class Node:
    def __init__(self, label, url, level=None, children=None):
        self.label = label
        self.url = url
        self.level = level
        self.children = children if children is not None else []

goalNodes = []
roots = []

def IDUBS(root, goals):
    global roots, goalNodes
    depth = 0
    roots.append(root)
    while roots:
        for node in roots[:]:
            print("$ Root:", node.label)
        for node in roots[:]:
            remaining = DFS(node, goals, depth - node.level)
            if not remaining:
                roots.remove(node)
        print("-----")
        depth += 1
    return goalNodes

def DFS(node, goals, depth):
    print("# Node: ", node.label + " Level: " + str(node.level) + " Depth: " + str(depth))
    global roots, goalNodes
    if depth == 0:
        if node.label in goals:
            goalNodes.append(node.label)
        return True

    elif depth > 0:
        anyRemaining = False
        for child in node.children:
            if child not in roots:
                child.level = node.level + 1
                if child.url != node.url:
                    print("- Add Root:", child.label)
                    roots.append(child)
                anyRemaining = DFS(child, goals, depth - 1)
        return anyRemaining

# Example usage:
if __name__ == "__main__":
    # Constructing the provided tree with labels and URLs
    nodeA = Node("A", "http://example.com/")
    nodeB = Node("B", "http://example.com/find")
    nodeC = Node("C", "http://example.com/vets")
    nodeD = Node("D", "http://example.com/new")
    nodeE = Node("E", "http://example.com/find")
    nodeF = Node("F", "http://example.com/new")
    nodeG = Node("G", "http://example.com/1")
    nodeH = Node("H", "http://example.com/pet/new")
    nodeI = Node("I", "http://example.com/edit")

    nodeA.level = 0
    nodeA.children = [nodeB, nodeC]
    nodeB.children = [nodeD, nodeE]
    nodeD.children = [nodeF]
    nodeE.children = [nodeG]
    nodeG.children = [nodeH, nodeI]

    # Define the root and goals with labels
    root = nodeA
    goals = ["C", "H"]

    # Call the IDUBS algorithm
    result = IDUBS(root, goals)
    print("Goal nodes found:", result)

