describe('Blindreview', () => {
  beforeEach(() => {
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element navbar-brand`, () => {
    const actualId = [`root`, `navbar-brand`];
    cy.clickIfExist(`[data-cy="navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element navbar-toggler`, () => {
    const actualId = [`root`, `navbar-toggler`];
    cy.clickIfExist(`[data-cy="navbar-toggler"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element home page`, () => {
    const actualId = [`root`, `home page`];
    cy.clickIfExist(`[data-cy="home page"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners`, () => {
    const actualId = [`root`, `find owners`];
    cy.clickIfExist(`[data-cy="find owners"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians`, () => {
    const actualId = [`root`, `veterinarians`];
    cy.clickIfExist(`[data-cy="veterinarians"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`];
    cy.visit('http://localhost:8080/owners/find');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="find-owner-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->add-owner-find-owners`, () => {
    const actualId = [`root`, `find owners`, `add-owner-find-owners`];
    cy.visit('http://localhost:8080/owners/find');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-owner-find-owners"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Filling values find owners->last-name-input-find-owners and submit`, () => {
    const actualId = [`root`, `find owners`, `last-name-input-find-owners`];
    cy.clickIfExist(`[data-cy="find owners"]`);
    cy.fillInput(`[data-cy="last-name-input-find-owners"] textarea`, `Generic Frozen Fish`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-pagination-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-pagination-button`];
    cy.visit('http://localhost:8080/vets.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-pagination-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-next-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-next-button`];
    cy.visit('http://localhost:8080/vets.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-next-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-last-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-last-button`];
    cy.visit('http://localhost:8080/vets.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-last-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->owner-link`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`];
    cy.visit('http://localhost:8080/owners?lastName=');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="owner-link"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->pagination-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `pagination-button`];
    cy.visit('http://localhost:8080/owners?lastName=');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pagination-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->next-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `next-button`];
    cy.visit('http://localhost:8080/owners?lastName=');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="next-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->last-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `last-button`];
    cy.visit('http://localhost:8080/owners?lastName=');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="last-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->add-owner-find-owners->add-owner`, () => {
    const actualId = [`root`, `find owners`, `add-owner-find-owners`, `add-owner`];
    cy.visit('http://localhost:8080/owners/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-owner"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Filling values find owners->add-owner-find-owners->firstName-lastName-address-city-telephone and submit`, () => {
    const actualId = [`root`, `find owners`, `add-owner-find-owners`, `firstName-lastName-address-city-telephone`];
    cy.visit('http://localhost:8080/owners/find');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-owner-find-owners"]`);
    cy.fillInput(`[data-cy="firstName"] textarea`, `payment`);
    cy.fillInput(`[data-cy="lastName"] textarea`, `Singapura`);
    cy.fillInput(`[data-cy="address"] textarea`, `JSON`);
    cy.fillInput(`[data-cy="city"] textarea`, `Borders`);
    cy.fillInput(`[data-cy="telephone"] textarea`, `opensource`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-pagination-button->vets-first-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-pagination-button`, `vets-first-button`];
    cy.visit('http://localhost:8080/vets.html?page=2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-first-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-pagination-button->vets-previous-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-pagination-button`, `vets-previous-button`];
    cy.visit('http://localhost:8080/vets.html?page=2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-previous-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->owner-link->edit-owner`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-owner`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="edit-owner"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->owner-link->add-new-pet`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-new-pet"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->owner-link->edit-pet-a`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="edit-pet-a"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->owner-link->add-visit-a`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-visit-a`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-visit-a"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->pagination-button->first-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `pagination-button`, `first-button`];
    cy.visit('http://localhost:8080/owners?page=2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="first-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element find owners->find-owner-button->pagination-button->previous-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `pagination-button`, `previous-button`];
    cy.visit('http://localhost:8080/owners?page=2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="previous-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-pagination-button->vets-first-button->vets-next-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-pagination-button`, `vets-first-button`, `vets-next-button`];
    cy.visit('http://localhost:8080/vets.html?page=1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-next-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it.skip(`Click on element veterinarians->vets-pagination-button->vets-first-button->vets-last-button`, () => {
    const actualId = [`root`, `veterinarians`, `vets-pagination-button`, `vets-first-button`, `vets-last-button`];
    cy.visit('http://localhost:8080/vets.html?page=1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="vets-last-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-owner->add-owner`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-owner`, `add-owner`];
    cy.visit('http://localhost:8080/owners/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-owner"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Filling values find owners->find-owner-button->owner-link->edit-owner->firstName-lastName-address-city-telephone and submit`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-owner`, `firstName-lastName-address-city-telephone`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="edit-owner"]`);
    cy.fillInput(`[data-cy="firstName"] textarea`, `Borders`);
    cy.fillInput(`[data-cy="lastName"] textarea`, `Associado`);
    cy.fillInput(`[data-cy="address"] textarea`, `Hryvnia`);
    cy.fillInput(`[data-cy="city"] textarea`, `sensor`);
    cy.fillInput(`[data-cy="telephone"] textarea`, `magenta`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->bird`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `bird`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="bird"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->cat`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `cat`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="cat"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->dog`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `dog`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="dog"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->hamster`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `hamster`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="hamster"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->lizard`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `lizard`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="lizard"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->snake`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `snake`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="snake"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-new-pet->add-pet`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `add-pet`];
    cy.visit('http://localhost:8080/owners/1/pets/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-pet"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Filling values find owners->find-owner-button->owner-link->add-new-pet->id-input-name-birthDate and submit`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-new-pet`, `id-input-name-birthDate`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-new-pet"]`);
    cy.fillInput(`[data-cy="id-input"] textarea`, `SMS`);
    cy.fillInput(`[data-cy="name"] textarea`, `virtual`);
    cy.clickIfExist('[data-cy="birthDate"]');
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->bird`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `bird`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="bird"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->cat`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `cat`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="cat"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->dog`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `dog`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="dog"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->hamster`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `hamster`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="hamster"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->lizard`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `lizard`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="lizard"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->snake`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `snake`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="snake"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->edit-pet-a->add-pet`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `add-pet`];
    cy.visit('http://localhost:8080/owners/1/pets/1/edit');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-pet"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Filling values find owners->find-owner-button->owner-link->edit-pet-a->id-input-name-birthDate and submit`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `edit-pet-a`, `id-input-name-birthDate`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="edit-pet-a"]`);
    cy.fillInput(`[data-cy="id-input"] textarea`, `invoice`);
    cy.fillInput(`[data-cy="name"] textarea`, `Amazonas`);
    cy.clickIfExist('[data-cy="birthDate"]');
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->owner-link->add-visit-a->add-visit-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-visit-a`, `add-visit-button`];
    cy.visit('http://localhost:8080/owners/1/pets/1/visits/new');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-visit-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Filling values find owners->find-owner-button->owner-link->add-visit-a->date-description-id-input and submit`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `owner-link`, `add-visit-a`, `date-description-id-input`];
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="add-visit-a"]`);
    cy.clickIfExist('[data-cy="date"]');
    cy.fillInput(`[data-cy="description"] textarea`, `Ponte`);
    cy.fillInput(`[data-cy="id-input"] textarea`, `JBOD`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->pagination-button->first-button->next-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `pagination-button`, `first-button`, `next-button`];
    cy.visit('http://localhost:8080/owners?page=1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="next-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  it(`Click on element find owners->find-owner-button->pagination-button->first-button->last-button`, () => {
    const actualId = [`root`, `find owners`, `find-owner-button`, `pagination-button`, `first-button`, `last-button`];
    cy.visit('http://localhost:8080/owners?page=1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="last-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
});
