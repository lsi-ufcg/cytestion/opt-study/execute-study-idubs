const fs = require('fs');
const csv = require('csv-parser');

const files = [];
for (let index = 0; index < 20; index++) {
  files.push(`results/A${index + 1}/states.csv`)
}

files.push(`results/bistro-restaurant/states.csv`)
files.push(`results/learn-educational/states.csv`)
files.push(`results/petclinic/states.csv`)
files.push(`results/school-educational/states.csv`)

let totalIdsOccurrences = 0;
let totalIdubsOccurrences = 0;

files.forEach(file => {
  if (file.endsWith('.csv')) {
    fs.createReadStream(file)
      .pipe(csv())
      .on('data', (row) => {
        totalIdsOccurrences += parseInt(row['IDS-OCCURRENCE']);
        totalIdubsOccurrences += parseInt(row['IDUBS-OCCURRENCE']);
      })
      .on('end', () => {
        console.log(`File ${file} processed.`);
      });
  }
});

process.on('exit', () => {
  console.log('Total IDS-OCCURRENCE:', totalIdsOccurrences);
  console.log('Total IDUBS-OCCURRENCE:', totalIdubsOccurrences);
  const percentage = calculatePercentage(totalIdubsOccurrences, totalIdsOccurrences);
  console.log("Reduction of " + (100.0 - percentage) + "%");
});


function calculatePercentage(value, total) {
  return (value / total) * 100;
}