const fs = require('fs');
const path = require('path');

const resultsDir = 'data';

function processLogFile(filePath) {
    if (fs.existsSync(filePath)) {
        const content = fs.readFileSync(filePath, 'utf8');

        // Extrair tempo de execução
        const executionTimeMatch = content.match(/Time execution: (\d+:\d+:\d+\.\d+|\d+:\d+\.\d+)/);
        const executionTime = executionTimeMatch ? executionTimeMatch[1] : 'N/A';

        // Extrair quantidade total de casos de teste
        const totalTestCasesMatch = content.match(/Total test cases: (\d+)/g);
        const totalTestCases = totalTestCasesMatch ? totalTestCasesMatch.pop().split(': ')[1] : 'N/A';

        // Verificar se existe a string "crash" no arquivo
        const hasCrash = content.includes('crash');

        //Extrair cobertura
        const coveragePath = filePath.replace('log.txt', 'coverage/lcov-report/index.html')
        const coverageInfo = extractCoverage(coveragePath);

        return {
            executionTime,
            totalTestCases,
            hasCrash,
            coverageInfo,
        };
    }
    return {}
}

function extractCoverage(path) {
    if (!fs.existsSync(path)) return {};

    const content = fs.readFileSync(path, "utf-8");

    const matches = content.match(/<span class="(.*?)">(.*?)<\/span>/g);
    const cleanValues = matches.map((match) => {
        return match.replace(/<span class="(.*?)">(.*?)<\/span>/g, (_, __, value) => value.trim());
    });

    const structuredValues = {}
    cleanValues.forEach((value, idx) => {
        return idx % 2 == 0 && (structuredValues[cleanValues[idx + 1]] = value);
    });

    return structuredValues;
}

function processModuleDirectory(moduleDir) {
    const directories = [
        'IDUBS',
        'IDS',
    ];

    const results = {};
    directories.forEach(directory => {
        const dirPath = path.join(moduleDir, directory);
        const logFilePath = path.join(dirPath, 'log.txt');
        results[directory] = processLogFile(logFilePath);
    });

    return results;
}

function processResultsDirectory(resultsDir) {
    const modules = fs.readdirSync(resultsDir);

    const result = {};

    for (const module of modules) {
        const moduleDir = path.join(resultsDir, module);
        if (fs.statSync(moduleDir).isDirectory()) {
            result[module] = processModuleDirectory(moduleDir);
        }
    }

    return result;
}

const collectedInfo = processResultsDirectory(resultsDir);
console.log(JSON.stringify(collectedInfo, null, 2));
fs.writeFileSync('info-results.json', JSON.stringify(collectedInfo, null, 2))
