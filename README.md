# Execute-study

### Cytestion tool could be found here: [cytestion.zip](./cytestion.zip). Inside the zip, you will found a **complete** README.

# Study of Iterative Deepening URL-Based Search

Study conducted to evaluate IDUBS compared to IDS in GUI testing. The study was conducted with twenty industrial web applications (A1 - A20) and four open-source applications.

The four open-source applications:

[petclinic](https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/spring-petclinic) a SpringBoot website for adding owners as well as their pets and manage scheduled visits to a veterinary

[bistro-restaurant](https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/website-bistro-restaurant) a website that serves as a portfolio template for your restaurant.

[learn-educational](https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/website-learn-educational) a website that serves as a platform to show informations about a learning institution.

[school-educational](https://gitlab.com/lsi-ufcg/cytestion/gui-testing-study/applications/website-school-educational) a website that serves as a template for school websites.

To compare IDUBS with IDS, we focused on four key aspects: test case execution time, number of revisited states, test suite coverage, and number of detected faults.

The data collected during the studies' execution can be found in the folder `results/$projectName`. Inside each folder, we find three files and two folders:
- `IDS`: Folder with the test files and metrics collected during the tool execution with the IDS approach
- `IDUBS`: Folder with the test files and metrics collected during the tool execution with the IDUBS approach
- `states.csv`: File generated with the collected information, indicating the count of states for both techniques
- `times.csv`: File generated with the collected information, indicating the times for each technique for the generated test cases
- `times-winsorization.csv`: File generated with the collected information, regarding the times of the techniques per test case after applying winsorization to remove outlier values.

## To execute

To collect the data, after executing the tool with both strategies, run the script `collect-info.js` with the command:

`node collect-info.js`.

To reproduce the results found, you can run the scripts contained in the following link:

[Link to scripts](https://colab.research.google.com/drive/1UcZbbEd0JI9NZ94ypHglOoQc-ohYSM68?usp=sharing)
