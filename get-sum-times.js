const fs = require('fs');
const csv = require('csv-parser');

const files = [];
for (let index = 0; index < 20; index++) {
  files.push(`results/A${index + 1}/times-winsorization.csv`)
}

files.push(`results/bistro-restaurant/times-winsorization.csv`)
files.push(`results/learn-educational/times-winsorization.csv`)
files.push(`results/petclinic/times-winsorization.csv`)
files.push(`results/school-educational/times-winsorization.csv`)

let totalIdsOccurrences = 0;
let totalIdubsOccurrences = 0;

files.forEach(file => {
  if (file.endsWith('.csv')) {
    fs.createReadStream(file)
      .pipe(csv())
      .on('data', (row) => {
        if (!isNaN(parseInt(row['IDS-TIME(MS)'])) && !isNaN(parseInt(row['IDUBS-TIME(MS)']))) {
          totalIdsOccurrences += parseInt(row['IDS-TIME(MS)']);
          totalIdubsOccurrences += parseInt(row['IDUBS-TIME(MS)']);
        }
      })
      .on('end', () => {
        console.log(`File ${file} processed.`);
      });
  }
});

process.on('exit', () => {
  console.log('Total IDS-TIME(MS):', totalIdsOccurrences);
  console.log('Total IDUBS-TIME(MS):', totalIdubsOccurrences);
  const percentage = calculatePercentage(totalIdubsOccurrences, totalIdsOccurrences);
  console.log("Reduction of " + (100.0 - percentage) + "%");
});


function calculatePercentage(value, total) {
  return (value / total) * 100;
}