describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar`, () => {
    cy.clickCauseExist(`[data-cy="reinf"]`);
    cy.clickCauseExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
    cy.clickCauseExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
    cy.clickCauseExist(`[data-cy="1716736008-novo"]`);
    cy.clickCauseExist(`[data-cy="699364833-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag and submit`, () => {
    cy.clickCauseExist(`[data-cy="reinf"]`);
    cy.clickCauseExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
    cy.clickCauseExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
    cy.clickCauseExist(`[data-cy="1716736008-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPfjCodigo"] input`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioCpfCnpj"] textarea`, `array`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioRazaoSocial"] textarea`, `Fatores`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndFisJur"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndNif"] input`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioCodigoNif"] textarea`, `Clothing`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioRelFontePag"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-salvar`, () => {
    cy.clickCauseExist(`[data-cy="reinf"]`);
    cy.clickCauseExist(`[data-cy="reinf/apuracoes"]`);
    cy.clickCauseExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
    cy.clickCauseExist(`[data-cy="3461523942-novo"]`);
    cy.clickCauseExist(`[data-cy="1127454403-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-salvar`, () => {
    cy.clickCauseExist(`[data-cy="reinf"]`);
    cy.clickCauseExist(`[data-cy="reinf/apuracoes"]`);
    cy.clickCauseExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
    cy.clickCauseExist(`[data-cy="1126801533-novo"]`);
    cy.clickCauseExist(`[data-cy="770844300-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo and submit`, () => {
    cy.clickCauseExist(`[data-cy="reinf"]`);
    cy.clickCauseExist(`[data-cy="reinf/apuracoes"]`);
    cy.clickCauseExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
    cy.clickCauseExist(`[data-cy="1126801533-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="770844300-powerselect-estCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-testar regra`, () => {
    cy.clickCauseExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickCauseExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
    cy.clickCauseExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
    cy.clickCauseExist(`[data-cy="3509287715-selectoutlined"]`);
    cy.clickCauseExist(`[data-cy="2512964284-limp"]`);
    cy.clickCauseExist(`[data-cy="2666930413-testar regra"]`);
    cy.checkErrorsWereDetected();
  });
});
