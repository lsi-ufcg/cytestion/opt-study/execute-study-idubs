describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
const actualId = [`root`,`tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
const actualId = [`root`,`tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes`, () => {
const actualId = [`root`,`integracoes`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf`, () => {
const actualId = [`root`,`reinf`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro`, () => {
const actualId = [`root`,`tributo-terceiro`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
const actualId = [`root`,`downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2585622888-r-2099 entrega dispensada.`, () => {
const actualId = [`root`,`2585622888-r-2099 entrega dispensada.`];
    cy.clickIfExist(`[data-cy="2585622888-r-2099 entrega dispensada."]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2585622888-atualizar pendências`, () => {
const actualId = [`root`,`2585622888-atualizar pendências`];
    cy.clickIfExist(`[data-cy="2585622888-atualizar pendências"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home->2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!`, () => {
const actualId = [`root`,`home`,`2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.clickIfExist(`[data-cy="2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/classificacao-servico`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/classificacao-servico`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/classificacao-servico"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/codigo-receita`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/codigo-receita`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/codigo-receita"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/natureza-rendimento`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/natureza-rendimento`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/natureza-rendimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/parametrizacao-geral`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametrizacao-geral"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pessoa-fisica-juridica`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pessoa-fisica-juridica`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pessoa-fisica-juridica"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/hierarquia-pessoa`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/hierarquia-pessoa`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/hierarquia-pessoa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/tipo-documento-nao-fiscal`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/tipo-documento-nao-fiscal`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/tipo-documento-nao-fiscal"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/natureza-operacao`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/natureza-operacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/natureza-operacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/prestacao`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/prestacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/prestacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/servicos-iss`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/servicos-iss`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/servicos-iss"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/processo`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/processo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/processo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/especie-dof`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/especie-dof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/especie-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/condicoes-pagamento-guia`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/condicoes-pagamento-guia`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/condicoes-pagamento-guia"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/tipos-impostos-da-apuracao`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/tipos-impostos-da-apuracao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/tipos-impostos-da-apuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/outras-remuneracoes-inss`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/dados-interface`, () => {
const actualId = [`root`,`integracoes`,`integracoes/dados-interface`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/dados-interface"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/execucao-interface`, () => {
const actualId = [`root`,`integracoes`,`integracoes/execucao-interface`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/execucao-interface"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/documentos-fiscais`, () => {
const actualId = [`root`,`transacoes`,`transacoes/documentos-fiscais`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/documentos-fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->385672452-power-search-button`, () => {
const actualId = [`root`,`downloads`,`385672452-power-search-button`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="385672452-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->385672452-download`, () => {
const actualId = [`root`,`downloads`,`385672452-download`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="385672452-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->385672452-detalhes`, () => {
const actualId = [`root`,`downloads`,`385672452-detalhes`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="385672452-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->385672452-excluir`, () => {
const actualId = [`root`,`downloads`,`385672452-excluir`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="385672452-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values 2585622888-atualizar pendências->2585622888-checkbox-checkGeracaoREINF-2585622888-checkbox-checkGeracaoESOCIAL and submit`, () => {
const actualId = [`root`,`2585622888-atualizar pendências`,`2585622888-checkbox-checkGeracaoREINF-2585622888-checkbox-checkGeracaoESOCIAL`];
    cy.clickIfExist(`[data-cy="2585622888-atualizar pendências"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2585622888-checkbox-checkGeracaoREINF"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="2585622888-checkbox-checkGeracaoESOCIAL"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home->2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!->2585622888-exportar dados`, () => {
const actualId = [`root`,`home`,`2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!`,`2585622888-exportar dados`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.clickIfExist(`[data-cy="2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!"]`);
      cy.clickIfExist(`[data-cy="2585622888-exportar dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values home->2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!->2585622888-checkbox-undefined and submit`, () => {
const actualId = [`root`,`home`,`2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!`,`2585622888-checkbox-undefined`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.clickIfExist(`[data-cy="2585622888-existem eventos de períodos anteriores a 03/2024 pendentes de geração!"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2585622888-checkbox-undefined"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/outras-remuneracoes-inss`,`308557383-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
      cy.clickIfExist(`[data-cy="308557383-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/outras-remuneracoes-inss`,`308557383-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
      cy.clickIfExist(`[data-cy="308557383-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/e-social`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/e-social`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/e-social"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-2000`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-2000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-2000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/comercializacao-rural`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/aquisicao-producao-rural`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/cprb`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/cprb`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/geracao-eventos`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/geracao-eventos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/geracao-eventos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/controle-eventos`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/controle-eventos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/controle-eventos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consulta-job`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consulta-job"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/execucao-processos`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/execucao-processos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/execucao-processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/automacao-pendencias`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/agendamento-processo`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/agendamento-processo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/agendamento-processo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/execucao-relatorios`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-2010-r-2020`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pagamentos-beneficiario-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/detalhes-eventos-criticados`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/apuracao-com-rural`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/apuracao-com-rural`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/apuracao-com-rural"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9011`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9015`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/guia-complementar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/guia-complementar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/guia-complementar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/pag-consolidados`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/pag-consolidados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/retencoes-notas-fiscais`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/retencoes-notas-fiscais`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/retencoes-notas-fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-analiticos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-analiticos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo->2360175536-salvar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-novo`,`2360175536-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-novo"]`);
      cy.clickIfExist(`[data-cy="2360175536-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo->2360175536-voltar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-novo`,`2360175536-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-novo"]`);
      cy.clickIfExist(`[data-cy="2360175536-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo->2360175536-powerselect-estCodigo-2360175536-input-rubricaId-2360175536-input-codigo-2360175536-powerselect-tipo-2360175536-input-descricao-2360175536-powerselect-indApuracaoIr-2360175536-checkbox-indRubricaDefault and submit`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-novo`,`2360175536-powerselect-estCodigo-2360175536-input-rubricaId-2360175536-input-codigo-2360175536-powerselect-tipo-2360175536-input-descricao-2360175536-powerselect-indApuracaoIr-2360175536-checkbox-indRubricaDefault`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2360175536-powerselect-estCodigo"] input`);
cy.fillInput(`[data-cy="2360175536-input-rubricaId"] textarea`, `bluetooth`);
cy.fillInput(`[data-cy="2360175536-input-codigo"] textarea`, `optimize`);
cy.fillInputPowerSelect(`[data-cy="2360175536-powerselect-tipo"] input`);
cy.fillInput(`[data-cy="2360175536-input-descricao"] textarea`, `Produtos`);
cy.fillInputPowerSelect(`[data-cy="2360175536-powerselect-indApuracaoIr"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="2360175536-checkbox-indRubricaDefault"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-remover item`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-salvar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-voltar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-novo`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-visualizar/editar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-visualizar/editar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-excluir`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-excluir`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-powerselect-estCodigo-2825312631-input-rubricaId-2825312631-input-codigo-2825312631-powerselect-tipo-2825312631-input-descricao-2825312631-powerselect-indApuracaoIr-2825312631-checkbox-indRubricaDefault and submit`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-powerselect-estCodigo-2825312631-input-rubricaId-2825312631-input-codigo-2825312631-powerselect-tipo-2825312631-input-descricao-2825312631-powerselect-indApuracaoIr-2825312631-checkbox-indRubricaDefault`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-estCodigo"] input`);
cy.fillInput(`[data-cy="2825312631-input-rubricaId"] textarea`, `generating`);
cy.fillInput(`[data-cy="2825312631-input-codigo"] textarea`, `Generic`);
cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-tipo"] input`);
cy.fillInput(`[data-cy="2825312631-input-descricao"] textarea`, `Mato Grosso do Sul`);
cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-indApuracaoIr"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="2825312631-checkbox-indRubricaDefault"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo->1841274626-salvar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/outras-remuneracoes-inss`,`308557383-novo`,`1841274626-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
      cy.clickIfExist(`[data-cy="308557383-novo"]`);
      cy.clickIfExist(`[data-cy="1841274626-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo->1841274626-voltar`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/outras-remuneracoes-inss`,`308557383-novo`,`1841274626-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
      cy.clickIfExist(`[data-cy="308557383-novo"]`);
      cy.clickIfExist(`[data-cy="1841274626-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo->1841274626-powerselect-trabalhadorPfjCodigo-1841274626-input-number-trabalhadorCategoria-1841274626-powerselect-tipoInscricao-1841274626-input-nomeOutraRetencao-1841274626-input-monetary-vlBaseCalculoInss-1841274626-input-monetary-vlRetencaoInss and submit`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/outras-remuneracoes-inss`,`308557383-novo`,`1841274626-powerselect-trabalhadorPfjCodigo-1841274626-input-number-trabalhadorCategoria-1841274626-powerselect-tipoInscricao-1841274626-input-nomeOutraRetencao-1841274626-input-monetary-vlBaseCalculoInss-1841274626-input-monetary-vlRetencaoInss`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
      cy.clickIfExist(`[data-cy="308557383-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1841274626-powerselect-trabalhadorPfjCodigo"] input`);
cy.fillInput(`[data-cy="1841274626-input-number-trabalhadorCategoria"] textarea`, `2`);
cy.fillInputPowerSelect(`[data-cy="1841274626-powerselect-tipoInscricao"] input`);
cy.fillInput(`[data-cy="1841274626-input-nomeOutraRetencao"] textarea`, `Zimbbue`);
cy.fillInput(`[data-cy="1841274626-input-monetary-vlBaseCalculoInss"] textarea`, `7`);
cy.fillInput(`[data-cy="1841274626-input-monetary-vlRetencaoInss"] textarea`, `7`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/e-social->1485690624-exibir dados`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/e-social`,`1485690624-exibir dados`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/e-social"]`);
      cy.clickIfExist(`[data-cy="1485690624-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-2000->1291494902-visualizar relatório`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-2000`,`1291494902-visualizar relatório`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-2000"]`);
      cy.clickIfExist(`[data-cy="1291494902-visualizar relatório"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000->1291554484-button`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-4000`,`1291554484-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-4000"]`);
      cy.clickIfExist(`[data-cy="1291554484-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000->1291554484-visualizar relatório`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-4000`,`1291554484-visualizar relatório`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-4000"]`);
      cy.clickIfExist(`[data-cy="1291554484-visualizar relatório"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-excluir`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-excluir`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-copyoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-deleteoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-deleteoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/comercializacao-rural`,`3461523942-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
      cy.clickIfExist(`[data-cy="3461523942-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/comercializacao-rural`,`3461523942-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
      cy.clickIfExist(`[data-cy="3461523942-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/aquisicao-producao-rural`,`1126801533-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
      cy.clickIfExist(`[data-cy="1126801533-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/aquisicao-producao-rural`,`1126801533-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
      cy.clickIfExist(`[data-cy="1126801533-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/cprb->3017108100-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/cprb`,`3017108100-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
      cy.clickIfExist(`[data-cy="3017108100-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/cprb->3017108100-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/cprb`,`3017108100-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
      cy.clickIfExist(`[data-cy="3017108100-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/geracao-eventos->2188111078-checkbox-utilizaDataFatoGerador-2188111078-checkbox-transmissaoAutomatica-2188111078-checkbox-geracaoMatrizFilial and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/geracao-eventos`,`2188111078-checkbox-utilizaDataFatoGerador-2188111078-checkbox-transmissaoAutomatica-2188111078-checkbox-geracaoMatrizFilial`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/geracao-eventos"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2188111078-checkbox-utilizaDataFatoGerador"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="2188111078-checkbox-transmissaoAutomatica"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="2188111078-checkbox-geracaoMatrizFilial"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/controle-eventos->3830594493-reloadoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/controle-eventos`,`3830594493-reloadoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/controle-eventos"]`);
      cy.clickIfExist(`[data-cy="3830594493-reloadoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/controle-eventos->3830594493-fechar período`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/controle-eventos`,`3830594493-fechar período`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/controle-eventos"]`);
      cy.clickIfExist(`[data-cy="3830594493-fechar período"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/controle-eventos->3830594493-powerselect-contribuinte-3830594493-checkbox-hierarquiaMatrizFilial and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/controle-eventos`,`3830594493-powerselect-contribuinte-3830594493-checkbox-hierarquiaMatrizFilial`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/controle-eventos"]`);
      cy.fillInputPowerSelect(`[data-cy="3830594493-powerselect-contribuinte"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="3830594493-checkbox-hierarquiaMatrizFilial"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job->3385006537-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consulta-job`,`3385006537-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consulta-job"]`);
      cy.clickIfExist(`[data-cy="3385006537-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job->3385006537-eyeoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consulta-job`,`3385006537-eyeoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consulta-job"]`);
      cy.clickIfExist(`[data-cy="3385006537-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/execucao-processos->1118927095-agendar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/execucao-processos`,`1118927095-agendar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/execucao-processos"]`);
      cy.clickIfExist(`[data-cy="1118927095-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/execucao-processos->1118927095-executar processo`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/execucao-processos`,`1118927095-executar processo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/execucao-processos"]`);
      cy.clickIfExist(`[data-cy="1118927095-executar processo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/execucao-processos->1118927095-powerselect-tipoExecucaoProcesso and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/execucao-processos`,`1118927095-powerselect-tipoExecucaoProcesso`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/execucao-processos"]`);
      cy.fillInputPowerSelect(`[data-cy="1118927095-powerselect-tipoExecucaoProcesso"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-detalhes do processo`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/automacao-pendencias`,`331885839-detalhes do processo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
      cy.clickIfExist(`[data-cy="331885839-detalhes do processo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-agendar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/automacao-pendencias`,`331885839-agendar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
      cy.clickIfExist(`[data-cy="331885839-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-executar automação`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/automacao-pendencias`,`331885839-executar automação`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
      cy.clickIfExist(`[data-cy="331885839-executar automação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-powerselect-serieEventos-331885839-checkbox-atualizarPendencias-331885839-checkbox-utilizarDataFatoGerador and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/automacao-pendencias`,`331885839-powerselect-serieEventos-331885839-checkbox-atualizarPendencias-331885839-checkbox-utilizarDataFatoGerador`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
      cy.fillInputPowerSelect(`[data-cy="331885839-powerselect-serieEventos"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="331885839-checkbox-atualizarPendencias"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="331885839-checkbox-utilizarDataFatoGerador"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/agendamento-processo->1126184012-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/agendamento-processo`,`1126184012-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/agendamento-processo"]`);
      cy.clickIfExist(`[data-cy="1126184012-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-executar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-agendamentos`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-agendamentos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-ajuda contextualizada`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-regerar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-regerar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-detalhes`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-detalhes`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-abrir visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-excluir`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-excluir`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-executar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-agendamentos`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-agendamentos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-requisitos para a exibição do processo`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-requisitos para a exibição do processo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-requisitos para a exibição do processo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-consultar job`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/execucao-relatorios`,`2455667123-consultar job`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
      cy.clickIfExist(`[data-cy="2455667123-consultar job"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-agendar`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/execucao-relatorios`,`2455667123-agendar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
      cy.clickIfExist(`[data-cy="2455667123-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-executar relatório`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/execucao-relatorios`,`2455667123-executar relatório`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
      cy.clickIfExist(`[data-cy="2455667123-executar relatório"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-powerselect-tipoExecucaoRelatorio-2455667123-checkbox-geracaoMatrizFilial and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/execucao-relatorios`,`2455667123-powerselect-tipoExecucaoRelatorio-2455667123-checkbox-geracaoMatrizFilial`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
      cy.fillInputPowerSelect(`[data-cy="2455667123-powerselect-tipoExecucaoRelatorio"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="2455667123-checkbox-geracaoMatrizFilial"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento-r4000`,`717893041-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
      cy.clickIfExist(`[data-cy="717893041-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-cenários de crítica de enquadramento de documentos fiscais r4000`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento-r4000`,`717893041-cenários de crítica de enquadramento de documentos fiscais r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
      cy.clickIfExist(`[data-cy="717893041-cenários de crítica de enquadramento de documentos fiscais r4000"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento-r4000`,`717893041-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
      cy.clickIfExist(`[data-cy="717893041-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento-r4000`,`717893041-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
      cy.fillInputPowerSearch(`[data-cy="717893041-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-2010-r-2020`,`3991017516-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
      cy.clickIfExist(`[data-cy="3991017516-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-requisitos para exibição de registros`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-2010-r-2020`,`3991017516-requisitos para exibição de registros`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
      cy.clickIfExist(`[data-cy="3991017516-requisitos para exibição de registros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-2010-r-2020`,`3991017516-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
      cy.clickIfExist(`[data-cy="3991017516-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-2010-r-2020`,`3991017516-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
      cy.fillInputPowerSearch(`[data-cy="3991017516-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-4000`,`2870315351-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
      cy.clickIfExist(`[data-cy="2870315351-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-requisitos para exibição de registros`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-4000`,`2870315351-requisitos para exibição de registros`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
      cy.clickIfExist(`[data-cy="2870315351-requisitos para exibição de registros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-4000`,`2870315351-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
      cy.clickIfExist(`[data-cy="2870315351-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-4000`,`2870315351-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
      cy.fillInputPowerSearch(`[data-cy="2870315351-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pagamentos-beneficiario-r4000`,`2161322512-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
      cy.clickIfExist(`[data-cy="2161322512-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-requisitos para exibição de registros`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pagamentos-beneficiario-r4000`,`2161322512-requisitos para exibição de registros`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
      cy.clickIfExist(`[data-cy="2161322512-requisitos para exibição de registros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pagamentos-beneficiario-r4000`,`2161322512-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
      cy.clickIfExist(`[data-cy="2161322512-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pagamentos-beneficiario-r4000`,`2161322512-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
      cy.fillInputPowerSearch(`[data-cy="2161322512-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/detalhes-eventos-criticados`,`3895730676-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
      cy.clickIfExist(`[data-cy="3895730676-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-ajuda`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/detalhes-eventos-criticados`,`3895730676-ajuda`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
      cy.clickIfExist(`[data-cy="3895730676-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/detalhes-eventos-criticados`,`3895730676-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
      cy.clickIfExist(`[data-cy="3895730676-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/detalhes-eventos-criticados`,`3895730676-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
      cy.fillInputPowerSearch(`[data-cy="3895730676-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9011`,`2785112454-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
      cy.clickIfExist(`[data-cy="2785112454-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-ajuda`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9011`,`2785112454-ajuda`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
      cy.clickIfExist(`[data-cy="2785112454-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9011`,`2785112454-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
      cy.clickIfExist(`[data-cy="2785112454-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9011`,`2785112454-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
      cy.fillInputPowerSearch(`[data-cy="2785112454-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9015`,`2785112458-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
      cy.clickIfExist(`[data-cy="2785112458-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-ajuda`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9015`,`2785112458-ajuda`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
      cy.clickIfExist(`[data-cy="2785112458-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-visualização`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9015`,`2785112458-visualização`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
      cy.clickIfExist(`[data-cy="2785112458-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-power-search-input and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9015`,`2785112458-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
      cy.fillInputPowerSearch(`[data-cy="2785112458-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-novo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-eyeoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-deleteoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-deleteoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-deleteoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-deleteoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-mais operações`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-mais operações`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-mais operações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-regerar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-regerar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-detalhes`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-detalhes`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-abrir visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-excluir`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-excluir`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/guia-complementar->2610911131-exibir dados`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/guia-complementar`,`2610911131-exibir dados`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/guia-complementar"]`);
      cy.clickIfExist(`[data-cy="2610911131-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-importar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-importar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-importar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-fileoutlined`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-fileoutlined`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-fileoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-deleteoutlined`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-regerar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-regerar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-detalhes`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-detalhes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-abrir visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-excluir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-excluir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/retencoes-notas-fiscais->2664595303-exibir dados`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/retencoes-notas-fiscais`,`2664595303-exibir dados`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/retencoes-notas-fiscais"]`);
      cy.clickIfExist(`[data-cy="2664595303-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-regerar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-regerar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-detalhes`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-detalhes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-abrir visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-excluir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-excluir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-regerar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-regerar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-detalhes`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-detalhes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-abrir visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-excluir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-excluir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-requisitos de geração`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-requisitos de geração`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-requisitos de geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-regerar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-regerar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-detalhes`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-detalhes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-abrir visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-excluir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-excluir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`,`682447114-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.clickIfExist(`[data-cy="682447114-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`,`682447114-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.clickIfExist(`[data-cy="682447114-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`,`682447114-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.clickIfExist(`[data-cy="682447114-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`,`682447114-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.clickIfExist(`[data-cy="682447114-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-executar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-gerenciar labels`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-gerenciar labels`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar parâmetros`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar/editar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-visualizar/editar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ir para todas as obrigações`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajuda`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajuda`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-nova solicitação`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-nova solicitação`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-atualizar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-atualizar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-gerar obrigação`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-gerar obrigação`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-gerar obrigação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-visualizar resultado da geração`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-protocolo transmissão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-protocolo transmissão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-excluir geração`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-excluir geração`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-excluir geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-abrir visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualizar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-visualizar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-novo`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-editar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-editar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-excluir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-excluir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-novo`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao-estabelecimento`,`2827831370-novo`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2827831370-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao-estabelecimento`,`2827831370-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2827831370-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-excluir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao-estabelecimento`,`2827831370-excluir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2827831370-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-novo->2825312631-powerselect-sissCodigo and submit`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/rubricas`,`2746791129-eyeoutlined`,`2825312631-novo`,`2825312631-powerselect-sissCodigo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
      cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2825312631-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-sissCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-2000->1291494902-visualizar relatório->2069757523-exibir dados`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-2000`,`1291494902-visualizar relatório`,`2069757523-exibir dados`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-2000"]`);
      cy.clickIfExist(`[data-cy="1291494902-visualizar relatório"]`);
      cy.clickIfExist(`[data-cy="2069757523-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000->1291554484-visualizar relatório->2069817105-exibir dados`, () => {
const actualId = [`root`,`reinf`,`reinf/paineis`,`reinf/paineis/serie-r-4000`,`1291554484-visualizar relatório`,`2069817105-exibir dados`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis"]`);
      cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-4000"]`);
      cy.clickIfExist(`[data-cy="1291554484-visualizar relatório"]`);
      cy.clickIfExist(`[data-cy="2069817105-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-novo`,`1165459227-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.clickIfExist(`[data-cy="1165459227-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-novo`,`1165459227-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.clickIfExist(`[data-cy="1165459227-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-powerselect-contribuintePfjCodigo-1165459227-powerselect-fontePagadoraPfjCodigo-1165459227-input-recImportNumero and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-novo`,`1165459227-powerselect-contribuintePfjCodigo-1165459227-powerselect-fontePagadoraPfjCodigo-1165459227-input-recImportNumero`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1165459227-powerselect-contribuintePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1165459227-powerselect-fontePagadoraPfjCodigo"] input`);
cy.fillInput(`[data-cy="1165459227-input-recImportNumero"] textarea`, `Bulgria`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-novo`,`3643260362-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.clickIfExist(`[data-cy="3643260362-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-novo`,`3643260362-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.clickIfExist(`[data-cy="3643260362-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-powerselect-contribuintePfjCodigo-3643260362-powerselect-beneficiarioPfjCodigo-3643260362-input-cnoCodigo-3643260362-input-pagImportNumero and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-novo`,`3643260362-powerselect-contribuintePfjCodigo-3643260362-powerselect-beneficiarioPfjCodigo-3643260362-input-cnoCodigo-3643260362-input-pagImportNumero`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="3643260362-powerselect-contribuintePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3643260362-powerselect-beneficiarioPfjCodigo"] input`);
cy.fillInput(`[data-cy="3643260362-input-cnoCodigo"] textarea`, `deposit`);
cy.fillInput(`[data-cy="3643260362-input-pagImportNumero"] textarea`, `teal`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.clickIfExist(`[data-cy="699364833-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.clickIfExist(`[data-cy="699364833-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPfjCodigo"] input`);
cy.fillInput(`[data-cy="699364833-input-beneficiarioCpfCnpj"] textarea`, `array`);
cy.fillInput(`[data-cy="699364833-input-beneficiarioRazaoSocial"] textarea`, `Fatores`);
cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndFisJur"] input`);
cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPais"] input`);
cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndNif"] input`);
cy.fillInput(`[data-cy="699364833-input-beneficiarioCodigoNif"] textarea`, `Clothing`);
cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioRelFontePag"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-remover item`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-remover item`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioPfjCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo->317393494-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-novo`,`317393494-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-novo"]`);
      cy.clickIfExist(`[data-cy="317393494-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo->317393494-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-novo`,`317393494-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-novo"]`);
      cy.clickIfExist(`[data-cy="317393494-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo->317393494-input-nome-317393494-powerselect-tipo-317393494-textarea-descricao and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-novo`,`317393494-input-nome-317393494-powerselect-tipo-317393494-textarea-descricao`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-novo"]`);
      cy.fillInput(`[data-cy="317393494-input-nome"] textarea`, `So Paulo`);
cy.fillInputPowerSelect(`[data-cy="317393494-powerselect-tipo"] input`);
cy.fillInput(`[data-cy="317393494-textarea-descricao"] input`, `Acre`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-remover item`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-remover item`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-adicionar categoria`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-adicionar categoria`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-adicionar categoria"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-eyeoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-eyeoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-deleteoutlined`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-deleteoutlined`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-input-nome-851469213-powerselect-indicadorDeduzaNcmAtividade-851469213-textarea-descricao and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-input-nome-851469213-powerselect-indicadorDeduzaNcmAtividade-851469213-textarea-descricao`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.fillInput(`[data-cy="851469213-input-nome"] textarea`, `harness`);
cy.fillInputPowerSelect(`[data-cy="851469213-powerselect-indicadorDeduzaNcmAtividade"] input`);
cy.fillInput(`[data-cy="851469213-textarea-descricao"] input`, `Fatores`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined->4012902003-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-copyoutlined`,`4012902003-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
      cy.clickIfExist(`[data-cy="4012902003-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined->4012902003-copiar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-copyoutlined`,`4012902003-copiar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
      cy.clickIfExist(`[data-cy="4012902003-copiar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined->4012902003-input-nome and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-copyoutlined`,`4012902003-input-nome`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
      cy.fillInput(`[data-cy="4012902003-input-nome"] textarea`, `Bulgria`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/comercializacao-rural`,`3461523942-novo`,`1127454403-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
      cy.clickIfExist(`[data-cy="3461523942-novo"]`);
      cy.clickIfExist(`[data-cy="1127454403-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/comercializacao-rural`,`3461523942-novo`,`1127454403-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
      cy.clickIfExist(`[data-cy="3461523942-novo"]`);
      cy.clickIfExist(`[data-cy="1127454403-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-powerselect-codigoEstabelecimento and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/comercializacao-rural`,`3461523942-novo`,`1127454403-powerselect-codigoEstabelecimento`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
      cy.clickIfExist(`[data-cy="3461523942-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1127454403-powerselect-codigoEstabelecimento"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/aquisicao-producao-rural`,`1126801533-novo`,`770844300-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
      cy.clickIfExist(`[data-cy="1126801533-novo"]`);
      cy.clickIfExist(`[data-cy="770844300-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/aquisicao-producao-rural`,`1126801533-novo`,`770844300-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
      cy.clickIfExist(`[data-cy="1126801533-novo"]`);
      cy.clickIfExist(`[data-cy="770844300-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/aquisicao-producao-rural`,`1126801533-novo`,`770844300-powerselect-estCodigo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
      cy.clickIfExist(`[data-cy="1126801533-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="770844300-powerselect-estCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/cprb->3017108100-novo->788957413-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/cprb`,`3017108100-novo`,`788957413-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
      cy.clickIfExist(`[data-cy="3017108100-novo"]`);
      cy.clickIfExist(`[data-cy="788957413-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/cprb->3017108100-novo->788957413-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/cprb`,`3017108100-novo`,`788957413-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
      cy.clickIfExist(`[data-cy="3017108100-novo"]`);
      cy.clickIfExist(`[data-cy="788957413-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/cprb->3017108100-novo->788957413-powerselect-codigoContribuinte-788957413-input-cnoCodigo-788957413-input-monetary-receitaBruta and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/apuracoes`,`reinf/apuracoes/cprb`,`3017108100-novo`,`788957413-powerselect-codigoContribuinte-788957413-input-cnoCodigo-788957413-input-monetary-receitaBruta`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
      cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
      cy.clickIfExist(`[data-cy="3017108100-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="788957413-powerselect-codigoContribuinte"] input`);
cy.fillInput(`[data-cy="788957413-input-cnoCodigo"] textarea`, `deliverables`);
cy.fillInput(`[data-cy="788957413-input-monetary-receitaBruta"] textarea`, `3`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar->338915745-múltipla seleção`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-executar`,`338915745-múltipla seleção`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-executar"]`);
      cy.clickIfExist(`[data-cy="338915745-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar->338915745-agendar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-executar`,`338915745-agendar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-executar"]`);
      cy.clickIfExist(`[data-cy="338915745-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-agendamentos->338915745-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-agendamentos`,`338915745-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-agendamentos"]`);
      cy.clickIfExist(`[data-cy="338915745-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-visualização->338915745-item- and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-visualização`,`338915745-item-`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="338915745-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-detalhes->338915745-dados disponíveis para impressão`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-detalhes`,`338915745-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-detalhes"]`);
      cy.clickIfExist(`[data-cy="338915745-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-aumentar o zoom`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-abrir visualização`,`338915745-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="338915745-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-diminuir o zoom`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-abrir visualização`,`338915745-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="338915745-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-expandir`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-abrir visualização`,`338915745-expandir`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="338915745-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-download`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-abrir visualização`,`338915745-download`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="338915745-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar->4056642876-múltipla seleção`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-executar`,`4056642876-múltipla seleção`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-executar"]`);
      cy.clickIfExist(`[data-cy="4056642876-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar->4056642876-agendar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-executar`,`4056642876-agendar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-executar"]`);
      cy.clickIfExist(`[data-cy="4056642876-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-agendamentos->4056642876-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-agendamentos`,`4056642876-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4056642876-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-visualização->4056642876-item- and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-visualização`,`4056642876-item-`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4056642876-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-visualização->717893041-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/enquadramento-r4000`,`717893041-visualização`,`717893041-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
      cy.clickIfExist(`[data-cy="717893041-visualização"]`);
      cy.clickIfExist(`[data-cy="717893041-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-visualização->3991017516-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-2010-r-2020`,`3991017516-visualização`,`3991017516-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
      cy.clickIfExist(`[data-cy="3991017516-visualização"]`);
      cy.clickIfExist(`[data-cy="3991017516-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-visualização->2870315351-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pend-geracao-r-4000`,`2870315351-visualização`,`2870315351-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
      cy.clickIfExist(`[data-cy="2870315351-visualização"]`);
      cy.clickIfExist(`[data-cy="2870315351-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-visualização->2161322512-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/pagamentos-beneficiario-r4000`,`2161322512-visualização`,`2161322512-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
      cy.clickIfExist(`[data-cy="2161322512-visualização"]`);
      cy.clickIfExist(`[data-cy="2161322512-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-visualização->3895730676-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/detalhes-eventos-criticados`,`3895730676-visualização`,`3895730676-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
      cy.clickIfExist(`[data-cy="3895730676-visualização"]`);
      cy.clickIfExist(`[data-cy="3895730676-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-visualização->2785112454-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9011`,`2785112454-visualização`,`2785112454-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
      cy.clickIfExist(`[data-cy="2785112454-visualização"]`);
      cy.clickIfExist(`[data-cy="2785112454-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-visualização->2785112458-salvar configuração`, () => {
const actualId = [`root`,`reinf`,`reinf/relatorios`,`reinf/relatorios/conciliacao-r9015`,`2785112458-visualização`,`2785112458-salvar configuração`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
      cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
      cy.clickIfExist(`[data-cy="2785112458-visualização"]`);
      cy.clickIfExist(`[data-cy="2785112458-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo->1414333350-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-novo`,`1414333350-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-novo"]`);
      cy.clickIfExist(`[data-cy="1414333350-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo->1414333350-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-novo`,`1414333350-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-novo"]`);
      cy.clickIfExist(`[data-cy="1414333350-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo->1414333350-input-rgliCodigo-1414333350-powerselect-indBaseLimp-1414333350-powerselect-indEntradaSaida-1414333350-powerselect-imposto-1414333350-textarea-obsRegra and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-novo`,`1414333350-input-rgliCodigo-1414333350-powerselect-indBaseLimp-1414333350-powerselect-indEntradaSaida-1414333350-powerselect-imposto-1414333350-textarea-obsRegra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-novo"]`);
      cy.fillInput(`[data-cy="1414333350-input-rgliCodigo"] textarea`, `navigate`);
cy.fillInputPowerSelect(`[data-cy="1414333350-powerselect-indBaseLimp"] input`);
cy.fillInputPowerSelect(`[data-cy="1414333350-powerselect-indEntradaSaida"] input`);
cy.fillInputPowerSelect(`[data-cy="1414333350-powerselect-imposto"] input`);
cy.fillInput(`[data-cy="1414333350-textarea-obsRegra"] input`, `Refined Rubber Mouse`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-novo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-eyeoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-deleteoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-deleteoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-pesquisar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`,`4001809800-pesquisar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.clickIfExist(`[data-cy="4001809800-pesquisar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`,`4001809800-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.clickIfExist(`[data-cy="4001809800-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`,`4001809800-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.clickIfExist(`[data-cy="4001809800-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-input-rgridCodigo-4001809800-input-nome-4001809800-powerselect-imposto-4001809800-input-monetary-vlMinRecolhimento-4001809800-powerselect-rgridCodigoSaldoCredor and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`,`4001809800-input-rgridCodigo-4001809800-input-nome-4001809800-powerselect-imposto-4001809800-input-monetary-vlMinRecolhimento-4001809800-powerselect-rgridCodigoSaldoCredor`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.fillInput(`[data-cy="4001809800-input-rgridCodigo"] textarea`, `Etipia`);
cy.fillInput(`[data-cy="4001809800-input-nome"] textarea`, `Uzbequisto`);
cy.fillInputPowerSelect(`[data-cy="4001809800-powerselect-imposto"] input`);
cy.fillInput(`[data-cy="4001809800-input-monetary-vlMinRecolhimento"] textarea`, `4,95`);
cy.fillInputPowerSelect(`[data-cy="4001809800-powerselect-rgridCodigoSaldoCredor"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-pesquisar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-pesquisar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-pesquisar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-remover item`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-remover item`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-copiar regra`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-copiar regra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-copiar regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-novo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-eyeoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-deleteoutlined`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-deleteoutlined`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-input-nome-2263500228-powerselect-imposto-2263500228-input-monetary-vlMinRecolhimento-2263500228-powerselect-rgridCodigoSaldoCredor and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-input-nome-2263500228-powerselect-imposto-2263500228-input-monetary-vlMinRecolhimento-2263500228-powerselect-rgridCodigoSaldoCredor`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.fillInput(`[data-cy="2263500228-input-nome"] textarea`, `Pernambuco`);
cy.fillInputPowerSelect(`[data-cy="2263500228-powerselect-imposto"] input`);
cy.fillInput(`[data-cy="2263500228-input-monetary-vlMinRecolhimento"] textarea`, `4,74`);
cy.fillInputPowerSelect(`[data-cy="2263500228-powerselect-rgridCodigoSaldoCredor"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-executar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-agendamentos`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-agendamentos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-visualização`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-visualização`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-regerar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-regerar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-detalhes`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-detalhes`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-excluir`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-excluir`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar->447335411-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-executar`,`447335411-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-executar"]`);
      cy.clickIfExist(`[data-cy="447335411-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar->447335411-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-executar`,`447335411-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-executar"]`);
      cy.clickIfExist(`[data-cy="447335411-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-agendamentos->447335411-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-agendamentos`,`447335411-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-agendamentos"]`);
      cy.clickIfExist(`[data-cy="447335411-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-visualização->447335411-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-visualização`,`447335411-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="447335411-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar->717809746-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-executar`,`717809746-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-executar"]`);
      cy.clickIfExist(`[data-cy="717809746-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar->717809746-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-executar`,`717809746-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-executar"]`);
      cy.clickIfExist(`[data-cy="717809746-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-agendamentos->717809746-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-agendamentos`,`717809746-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-agendamentos"]`);
      cy.clickIfExist(`[data-cy="717809746-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-visualização->717809746-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-visualização`,`717809746-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="717809746-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar->133362368-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-executar`,`133362368-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-executar"]`);
      cy.clickIfExist(`[data-cy="133362368-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar->133362368-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-executar`,`133362368-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-executar"]`);
      cy.clickIfExist(`[data-cy="133362368-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-agendamentos->133362368-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-agendamentos`,`133362368-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-agendamentos"]`);
      cy.clickIfExist(`[data-cy="133362368-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-visualização->133362368-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-visualização`,`133362368-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="133362368-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar->235713453-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-executar`,`235713453-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-executar"]`);
      cy.clickIfExist(`[data-cy="235713453-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar->235713453-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-executar`,`235713453-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-executar"]`);
      cy.clickIfExist(`[data-cy="235713453-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-agendamentos->235713453-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-agendamentos`,`235713453-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-agendamentos"]`);
      cy.clickIfExist(`[data-cy="235713453-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-visualização->235713453-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-visualização`,`235713453-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="235713453-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-detalhes->235713453-dados disponíveis para impressão`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-detalhes`,`235713453-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-detalhes"]`);
      cy.clickIfExist(`[data-cy="235713453-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-aumentar o zoom`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-abrir visualização`,`235713453-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="235713453-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-diminuir o zoom`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-abrir visualização`,`235713453-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="235713453-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-expandir`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-abrir visualização`,`235713453-expandir`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="235713453-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-download`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-abrir visualização`,`235713453-download`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="235713453-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar->2590522946-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-executar`,`2590522946-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-executar"]`);
      cy.clickIfExist(`[data-cy="2590522946-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar->2590522946-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-executar`,`2590522946-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-executar"]`);
      cy.clickIfExist(`[data-cy="2590522946-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-agendamentos->2590522946-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-agendamentos`,`2590522946-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2590522946-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-visualização->2590522946-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-visualização`,`2590522946-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2590522946-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar->2403317122-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-executar`,`2403317122-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-executar"]`);
      cy.clickIfExist(`[data-cy="2403317122-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar->2403317122-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-executar`,`2403317122-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-executar"]`);
      cy.clickIfExist(`[data-cy="2403317122-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-agendamentos->2403317122-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-agendamentos`,`2403317122-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2403317122-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-visualização->2403317122-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-visualização`,`2403317122-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2403317122-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar->3689771228-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-executar`,`3689771228-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-executar"]`);
      cy.clickIfExist(`[data-cy="3689771228-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar->3689771228-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-executar`,`3689771228-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-executar"]`);
      cy.clickIfExist(`[data-cy="3689771228-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-agendamentos->3689771228-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-agendamentos`,`3689771228-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3689771228-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-visualização->3689771228-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-visualização`,`3689771228-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3689771228-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar->1130815789-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-executar`,`1130815789-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-executar"]`);
      cy.clickIfExist(`[data-cy="1130815789-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar->1130815789-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-executar`,`1130815789-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-executar"]`);
      cy.clickIfExist(`[data-cy="1130815789-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-agendamentos->1130815789-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-agendamentos`,`1130815789-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1130815789-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-visualização->1130815789-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-visualização`,`1130815789-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1130815789-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar->3148305101-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-executar`,`3148305101-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-executar"]`);
      cy.clickIfExist(`[data-cy="3148305101-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar->3148305101-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-executar`,`3148305101-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-executar"]`);
      cy.clickIfExist(`[data-cy="3148305101-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-agendamentos->3148305101-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-agendamentos`,`3148305101-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3148305101-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-visualização->3148305101-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-visualização`,`3148305101-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3148305101-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-importar->307422372-powerselect-ano and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-importar`,`307422372-powerselect-ano`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-importar"]`);
      cy.fillInputPowerSelect(`[data-cy="307422372-powerselect-ano"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-fileoutlined->307422372-saveoutlined`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-fileoutlined`,`307422372-saveoutlined`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-fileoutlined"]`);
      cy.clickIfExist(`[data-cy="307422372-saveoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-fileoutlined->307422372-printeroutlined`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/dirf`,`tributo-terceiro/dirf/importacao`,`307422372-fileoutlined`,`307422372-printeroutlined`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
      cy.clickIfExist(`[data-cy="307422372-fileoutlined"]`);
      cy.clickIfExist(`[data-cy="307422372-printeroutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-executar->2527279051-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-executar`,`2527279051-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-executar"]`);
      cy.clickIfExist(`[data-cy="2527279051-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-executar->2527279051-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-executar`,`2527279051-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-executar"]`);
      cy.clickIfExist(`[data-cy="2527279051-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-executar->2527279051-input-P_ANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-executar`,`2527279051-input-P_ANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-executar"]`);
      cy.fillInput(`[data-cy="2527279051-input-P_ANO"] textarea`, `infrastructures`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-agendamentos->2527279051-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-agendamentos`,`2527279051-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2527279051-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-visualização->2527279051-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-visualização`,`2527279051-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2527279051-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-executar`,`1745767366-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-executar"]`);
      cy.clickIfExist(`[data-cy="1745767366-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-executar`,`1745767366-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-executar"]`);
      cy.clickIfExist(`[data-cy="1745767366-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-input-P_ANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-executar`,`1745767366-input-P_ANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-executar"]`);
      cy.fillInput(`[data-cy="1745767366-input-P_ANO"] textarea`, `Seychelles`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-agendamentos->1745767366-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-agendamentos`,`1745767366-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1745767366-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-visualização->1745767366-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-visualização`,`1745767366-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1745767366-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-executar`,`1673335588-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-executar"]`);
      cy.clickIfExist(`[data-cy="1673335588-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-executar`,`1673335588-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-executar"]`);
      cy.clickIfExist(`[data-cy="1673335588-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-input-P_ANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-executar`,`1673335588-input-P_ANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-executar"]`);
      cy.fillInput(`[data-cy="1673335588-input-P_ANO"] textarea`, `Industrial`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-agendamentos->1673335588-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-agendamentos`,`1673335588-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1673335588-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-visualização->1673335588-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-visualização`,`1673335588-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1673335588-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-detalhes->1673335588-dados disponíveis para impressão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-detalhes`,`1673335588-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-detalhes"]`);
      cy.clickIfExist(`[data-cy="1673335588-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-abrir visualização`,`1673335588-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1673335588-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-abrir visualização`,`1673335588-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1673335588-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-abrir visualização`,`1673335588-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1673335588-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-abrir visualização`,`1673335588-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1673335588-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-executar`,`3329339398-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-executar"]`);
      cy.clickIfExist(`[data-cy="3329339398-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-executar`,`3329339398-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-executar"]`);
      cy.clickIfExist(`[data-cy="3329339398-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-input-PFJ and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-executar`,`3329339398-input-PFJ`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-executar"]`);
      cy.fillInput(`[data-cy="3329339398-input-PFJ"] textarea`, `Borders`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-agendamentos->3329339398-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-agendamentos`,`3329339398-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3329339398-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-visualização->3329339398-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-visualização`,`3329339398-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3329339398-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-detalhes->3329339398-dados disponíveis para impressão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-detalhes`,`3329339398-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-detalhes"]`);
      cy.clickIfExist(`[data-cy="3329339398-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-abrir visualização`,`3329339398-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3329339398-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-abrir visualização`,`3329339398-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3329339398-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-abrir visualização`,`3329339398-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3329339398-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-abrir visualização`,`3329339398-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3329339398-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar->1917228366-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-executar`,`1917228366-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-executar"]`);
      cy.clickIfExist(`[data-cy="1917228366-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar->1917228366-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-executar`,`1917228366-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-executar"]`);
      cy.clickIfExist(`[data-cy="1917228366-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-agendamentos->1917228366-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-agendamentos`,`1917228366-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1917228366-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-visualização->1917228366-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-visualização`,`1917228366-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1917228366-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-executar`,`977623228-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-executar"]`);
      cy.clickIfExist(`[data-cy="977623228-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-executar`,`977623228-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-executar"]`);
      cy.clickIfExist(`[data-cy="977623228-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-input-P_CPF_CGC and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-executar`,`977623228-input-P_CPF_CGC`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-executar"]`);
      cy.fillInput(`[data-cy="977623228-input-P_CPF_CGC"] textarea`, `Tcnico`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-agendamentos->977623228-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-agendamentos`,`977623228-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-agendamentos"]`);
      cy.clickIfExist(`[data-cy="977623228-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-visualização->977623228-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-visualização`,`977623228-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="977623228-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar->3465203885-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-executar`,`3465203885-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-executar"]`);
      cy.clickIfExist(`[data-cy="3465203885-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar->3465203885-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-executar`,`3465203885-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-executar"]`);
      cy.clickIfExist(`[data-cy="3465203885-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-agendamentos->3465203885-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-agendamentos`,`3465203885-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-visualização->3465203885-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-visualização`,`3465203885-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3465203885-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-executar`,`1614719659-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-executar"]`);
      cy.clickIfExist(`[data-cy="1614719659-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-executar`,`1614719659-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-executar"]`);
      cy.clickIfExist(`[data-cy="1614719659-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-input-PFJ and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-executar`,`1614719659-input-PFJ`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-executar"]`);
      cy.fillInput(`[data-cy="1614719659-input-PFJ"] textarea`, `Orquestrador`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-agendamentos->1614719659-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-agendamentos`,`1614719659-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614719659-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-visualização->1614719659-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-visualização`,`1614719659-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1614719659-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-detalhes->1614719659-dados disponíveis para impressão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-detalhes`,`1614719659-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-detalhes"]`);
      cy.clickIfExist(`[data-cy="1614719659-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-abrir visualização`,`1614719659-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614719659-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-abrir visualização`,`1614719659-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614719659-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-abrir visualização`,`1614719659-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614719659-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-abrir visualização`,`1614719659-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614719659-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-executar`,`3047460406-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-executar"]`);
      cy.clickIfExist(`[data-cy="3047460406-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-executar`,`3047460406-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-executar"]`);
      cy.clickIfExist(`[data-cy="3047460406-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-input-PFJ_PRESTADORA and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-executar`,`3047460406-input-PFJ_PRESTADORA`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-executar"]`);
      cy.fillInput(`[data-cy="3047460406-input-PFJ_PRESTADORA"] textarea`, `Chips`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-agendamentos->3047460406-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-agendamentos`,`3047460406-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3047460406-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-visualização->3047460406-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-visualização`,`3047460406-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3047460406-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-detalhes->3047460406-dados disponíveis para impressão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-detalhes`,`3047460406-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-detalhes"]`);
      cy.clickIfExist(`[data-cy="3047460406-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-abrir visualização`,`3047460406-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3047460406-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-abrir visualização`,`3047460406-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3047460406-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-abrir visualização`,`3047460406-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3047460406-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-abrir visualização`,`3047460406-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3047460406-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-executar`,`3040111907-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-executar"]`);
      cy.clickIfExist(`[data-cy="3040111907-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-executar`,`3040111907-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-executar"]`);
      cy.clickIfExist(`[data-cy="3040111907-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-input-PRESTADOR and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-executar`,`3040111907-input-PRESTADOR`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-executar"]`);
      cy.fillInput(`[data-cy="3040111907-input-PRESTADOR"] textarea`, `Seychelles`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-agendamentos->3040111907-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-agendamentos`,`3040111907-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3040111907-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-visualização->3040111907-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-retidos`,`3040111907-visualização`,`3040111907-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
      cy.clickIfExist(`[data-cy="3040111907-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3040111907-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-executar`,`2966158800-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-executar"]`);
      cy.clickIfExist(`[data-cy="2966158800-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-executar`,`2966158800-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-executar"]`);
      cy.clickIfExist(`[data-cy="2966158800-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-input-CPF_CNPJ and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-executar`,`2966158800-input-CPF_CNPJ`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-executar"]`);
      cy.fillInput(`[data-cy="2966158800-input-CPF_CNPJ"] textarea`, `ROI`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-agendamentos->2966158800-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-agendamentos`,`2966158800-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2966158800-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-visualização->2966158800-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-visualização`,`2966158800-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2966158800-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-agendamentos->682447114-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`,`682447114-agendamentos`,`682447114-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.clickIfExist(`[data-cy="682447114-agendamentos"]`);
      cy.clickIfExist(`[data-cy="682447114-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-visualização->682447114-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-por-beneficiario`,`682447114-visualização`,`682447114-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
      cy.clickIfExist(`[data-cy="682447114-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="682447114-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-executar`,`2798312950-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-executar"]`);
      cy.clickIfExist(`[data-cy="2798312950-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-executar`,`2798312950-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-executar"]`);
      cy.clickIfExist(`[data-cy="2798312950-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-input-pANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-executar`,`2798312950-input-pANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-executar"]`);
      cy.fillInput(`[data-cy="2798312950-input-pANO"] textarea`, `Personal Loan Account`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-agendamentos->2798312950-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-agendamentos`,`2798312950-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2798312950-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-visualização->2798312950-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-visualização`,`2798312950-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2798312950-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-executar`,`533561082-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-executar"]`);
      cy.clickIfExist(`[data-cy="533561082-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-executar`,`533561082-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-executar"]`);
      cy.clickIfExist(`[data-cy="533561082-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-input-P_ANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-executar`,`533561082-input-P_ANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-executar"]`);
      cy.fillInput(`[data-cy="533561082-input-P_ANO"] textarea`, `Rustic Frozen Shoes`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-agendamentos->533561082-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-agendamentos`,`533561082-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-agendamentos"]`);
      cy.clickIfExist(`[data-cy="533561082-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-visualização->533561082-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-visualização`,`533561082-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="533561082-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-executar`,`716117882-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-executar"]`);
      cy.clickIfExist(`[data-cy="716117882-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-executar`,`716117882-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-executar"]`);
      cy.clickIfExist(`[data-cy="716117882-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-input-RESPONSAVEL and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-executar`,`716117882-input-RESPONSAVEL`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-executar"]`);
      cy.fillInput(`[data-cy="716117882-input-RESPONSAVEL"] textarea`, `Money Market Account`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-agendamentos->716117882-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-agendamentos`,`716117882-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-agendamentos"]`);
      cy.clickIfExist(`[data-cy="716117882-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-visualização->716117882-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-visualização`,`716117882-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="716117882-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-executar`,`1023520286-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-executar"]`);
      cy.clickIfExist(`[data-cy="1023520286-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-executar`,`1023520286-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-executar"]`);
      cy.clickIfExist(`[data-cy="1023520286-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-input-RESPONSAVEL and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-executar`,`1023520286-input-RESPONSAVEL`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-executar"]`);
      cy.fillInput(`[data-cy="1023520286-input-RESPONSAVEL"] textarea`, `Garden`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-agendamentos->1023520286-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-agendamentos`,`1023520286-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1023520286-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-visualização->1023520286-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-visualização`,`1023520286-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1023520286-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-executar`,`2587924048-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-executar"]`);
      cy.clickIfExist(`[data-cy="2587924048-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-executar`,`2587924048-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-executar"]`);
      cy.clickIfExist(`[data-cy="2587924048-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-input-CPF_CNPJ_BENEF-2587924048-input-DATA_ANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-executar`,`2587924048-input-CPF_CNPJ_BENEF-2587924048-input-DATA_ANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-executar"]`);
      cy.fillInput(`[data-cy="2587924048-input-CPF_CNPJ_BENEF"] textarea`, `circuit`);
cy.fillInput(`[data-cy="2587924048-input-DATA_ANO"] textarea`, `portals`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-agendamentos->2587924048-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-agendamentos`,`2587924048-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2587924048-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-visualização->2587924048-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-visualização`,`2587924048-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2587924048-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-executar`,`1339819272-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-executar"]`);
      cy.clickIfExist(`[data-cy="1339819272-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-executar`,`1339819272-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-executar"]`);
      cy.clickIfExist(`[data-cy="1339819272-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-input-CPF_CNPJ_BENEF-1339819272-input-DATA_ANO and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-executar`,`1339819272-input-CPF_CNPJ_BENEF-1339819272-input-DATA_ANO`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-executar"]`);
      cy.fillInput(`[data-cy="1339819272-input-CPF_CNPJ_BENEF"] textarea`, `gold`);
cy.fillInput(`[data-cy="1339819272-input-DATA_ANO"] textarea`, `USB`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-agendamentos->1339819272-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-agendamentos`,`1339819272-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1339819272-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-visualização->1339819272-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-visualização`,`1339819272-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1339819272-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-múltipla seleção`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-executar`,`1234427231-múltipla seleção`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-executar"]`);
      cy.clickIfExist(`[data-cy="1234427231-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-agendar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-executar`,`1234427231-agendar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-executar"]`);
      cy.clickIfExist(`[data-cy="1234427231-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-input-CPF_CNPJ_BENEF-1234427231-input-DATA_ANO-1234427231-input-RESPONSAVEL and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-executar`,`1234427231-input-CPF_CNPJ_BENEF-1234427231-input-DATA_ANO-1234427231-input-RESPONSAVEL`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-executar"]`);
      cy.fillInput(`[data-cy="1234427231-input-CPF_CNPJ_BENEF"] textarea`, `eenable`);
cy.fillInput(`[data-cy="1234427231-input-DATA_ANO"] textarea`, `morph`);
cy.fillInput(`[data-cy="1234427231-input-RESPONSAVEL"] textarea`, `Amazonas`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-agendamentos->1234427231-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-agendamentos`,`1234427231-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1234427231-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-visualização->1234427231-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-visualização`,`1234427231-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1234427231-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-gerenciar labels->1392456818-fechar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-gerenciar labels`,`1392456818-fechar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="1392456818-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar/editar->3216657679-salvar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-visualizar/editar`,`3216657679-salvar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3216657679-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar/editar->3216657679-voltar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao`,`1392456818-visualizar/editar`,`3216657679-voltar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="1392456818-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3216657679-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ir para todas as obrigações->611534337-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ir para todas as obrigações`,`611534337-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="611534337-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-nova solicitação->611534337-salvar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-nova solicitação`,`611534337-salvar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="611534337-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-nova solicitação->611534337-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-nova solicitação`,`611534337-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="611534337-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-abrir visualização`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualizar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-visualizar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-rightoutlined`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`,`1874405030-rightoutlined`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1874405030-rightoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-habilitar edição`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`,`1874405030-habilitar edição`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1874405030-habilitar edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-abrir janela de edição`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`,`1874405030-abrir janela de edição`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1874405030-abrir janela de edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-visualizar resultado da geração`,`611534337-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="611534337-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-visualizar resultado da geração`,`611534337-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="611534337-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-visualizar resultado da geração`,`611534337-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="611534337-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-visualizar resultado da geração`,`611534337-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="611534337-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão->3661377306-editar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-protocolo transmissão`,`3661377306-editar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="3661377306-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualização->52374570-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-visualização`,`52374570-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="52374570-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-abrir visualização`,`52374570-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-abrir visualização`,`52374570-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-abrir visualização`,`52374570-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-abrir visualização`,`52374570-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualizar->52374570-dados disponíveis para impressão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-visualizar`,`52374570-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
      cy.clickIfExist(`[data-cy="52374570-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo->3124702626-criar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-novo`,`3124702626-criar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-novo"]`);
      cy.clickIfExist(`[data-cy="3124702626-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo->3124702626-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-novo`,`3124702626-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-novo"]`);
      cy.clickIfExist(`[data-cy="3124702626-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo->3124702626-input-number-ano and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-novo`,`3124702626-input-number-ano`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-novo"]`);
      cy.fillInput(`[data-cy="3124702626-input-number-ano"] textarea`, `3`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-editar->3124702626-remover item`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-editar`,`3124702626-remover item`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-editar"]`);
      cy.clickIfExist(`[data-cy="3124702626-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-editar->3124702626-salvar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/periodicidade`,`3124702626-editar`,`3124702626-salvar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="3124702626-editar"]`);
      cy.clickIfExist(`[data-cy="3124702626-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-novo->2827831370-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/configuracao-estabelecimento`,`2827831370-novo`,`2827831370-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2827831370-novo"]`);
      cy.clickIfExist(`[data-cy="2827831370-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-voltar->4096060046-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-novo`,`1165459227-voltar`,`4096060046-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.clickIfExist(`[data-cy="1165459227-voltar"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-voltar->4096060046-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/recebimentos-diversos`,`4096060046-novo`,`1165459227-voltar`,`4096060046-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
      cy.clickIfExist(`[data-cy="4096060046-novo"]`);
      cy.clickIfExist(`[data-cy="1165459227-voltar"]`);
      cy.clickIfExist(`[data-cy="4096060046-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-voltar->2030434943-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-novo`,`3643260362-voltar`,`2030434943-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.clickIfExist(`[data-cy="3643260362-voltar"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-voltar->2030434943-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-diversos`,`2030434943-novo`,`3643260362-voltar`,`2030434943-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
      cy.clickIfExist(`[data-cy="2030434943-novo"]`);
      cy.clickIfExist(`[data-cy="3643260362-voltar"]`);
      cy.clickIfExist(`[data-cy="2030434943-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-voltar`,`1716736008-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.clickIfExist(`[data-cy="699364833-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-voltar`,`1716736008-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.clickIfExist(`[data-cy="699364833-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-visualizar/editar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-voltar`,`1716736008-visualizar/editar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.clickIfExist(`[data-cy="699364833-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-excluir`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-novo`,`699364833-voltar`,`1716736008-excluir`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.clickIfExist(`[data-cy="699364833-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-voltar->1716736008-novo`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-voltar`,`1716736008-novo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-voltar->1716736008-power-search-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-voltar`,`1716736008-power-search-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-voltar->1716736008-visualizar/editar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-voltar`,`1716736008-visualizar/editar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-voltar->1716736008-excluir`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-voltar`,`1716736008-excluir`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-voltar"]`);
      cy.clickIfExist(`[data-cy="1716736008-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-novo->1831468927-button`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-novo`,`1831468927-button`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-novo"]`);
      cy.clickIfExist(`[data-cy="1831468927-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-novo->1831468927-edição nat. rend.`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-novo`,`1831468927-edição nat. rend.`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-novo"]`);
      cy.clickIfExist(`[data-cy="1831468927-edição nat. rend."]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-novo->1831468927-salvar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-novo`,`1831468927-salvar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-novo"]`);
      cy.clickIfExist(`[data-cy="1831468927-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-novo->1831468927-voltar`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-novo`,`1831468927-voltar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-novo"]`);
      cy.clickIfExist(`[data-cy="1831468927-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-visualizar/editar->441105128-novo->1831468927-powerselect-naturezaRendimentoCodigo-1831468927-powerselect-indFiscalNaoFiscal-1831468927-powerselect-dofId-1831468927-input-eventoAdicionalId-1831468927-powerselect-indAutoRetencao-1831468927-input-descricao-1831468927-input-monetary-vlOperacao-1831468927-powerselect-ind13Salario-1831468927-powerselect-indRra-1831468927-powerselect-indRendDecisaoJudicial-1831468927-powerselect-tipoEntidadeAssociada-1831468927-input-cnpjEntidadeAssociada-1831468927-input-monetary-percentualParticipacao-1831468927-powerselect-formaTribRendimento and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/recebimentos-pagamentos`,`reinf/recebimentos-pagamentos/pagamentos-r4000`,`1716736008-visualizar/editar`,`441105128-novo`,`1831468927-powerselect-naturezaRendimentoCodigo-1831468927-powerselect-indFiscalNaoFiscal-1831468927-powerselect-dofId-1831468927-input-eventoAdicionalId-1831468927-powerselect-indAutoRetencao-1831468927-input-descricao-1831468927-input-monetary-vlOperacao-1831468927-powerselect-ind13Salario-1831468927-powerselect-indRra-1831468927-powerselect-indRendDecisaoJudicial-1831468927-powerselect-tipoEntidadeAssociada-1831468927-input-cnpjEntidadeAssociada-1831468927-input-monetary-percentualParticipacao-1831468927-powerselect-formaTribRendimento`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
      cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
      cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="441105128-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-naturezaRendimentoCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indFiscalNaoFiscal"] input`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-dofId"] input`);
cy.fillInput(`[data-cy="1831468927-input-eventoAdicionalId"] textarea`, `plum`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indAutoRetencao"] input`);
cy.fillInput(`[data-cy="1831468927-input-descricao"] textarea`, `opensource`);
cy.fillInput(`[data-cy="1831468927-input-monetary-vlOperacao"] textarea`, `3`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-ind13Salario"] input`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indRra"] input`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indRendDecisaoJudicial"] input`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-tipoEntidadeAssociada"] input`);
cy.fillInput(`[data-cy="1831468927-input-cnpjEntidadeAssociada"] textarea`, `35.061.134/7410-24`);
cy.fillInput(`[data-cy="1831468927-input-monetary-percentualParticipacao"] textarea`, `1`);
cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-formaTribRendimento"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-adicionar categoria->851469213-cancelar`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-adicionar categoria`,`851469213-cancelar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-adicionar categoria"]`);
      cy.clickIfExist(`[data-cy="851469213-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-adicionar categoria->851469213-powerselect-destino and submit`, () => {
const actualId = [`root`,`reinf`,`reinf/regras`,`reinf/regras/apuracao-cprb`,`4012902003-eyeoutlined`,`851469213-adicionar categoria`,`851469213-powerselect-destino`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/regras"]`);
      cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
      cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="851469213-adicionar categoria"]`);
      cy.fillInputPowerSelect(`[data-cy="851469213-powerselect-destino"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar->338915745-múltipla seleção->338915745-cancelar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-executar`,`338915745-múltipla seleção`,`338915745-cancelar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-executar"]`);
      cy.clickIfExist(`[data-cy="338915745-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="338915745-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-expandir->338915745-diminuir`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/apuracao-r4000`,`338915745-abrir visualização`,`338915745-expandir`,`338915745-diminuir`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
      cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="338915745-expandir"]`);
      cy.clickIfExist(`[data-cy="338915745-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar->4056642876-múltipla seleção->4056642876-cancelar`, () => {
const actualId = [`root`,`reinf`,`reinf/processos`,`reinf/processos/consolidacao-desconsolidacao-r4000`,`4056642876-executar`,`4056642876-múltipla seleção`,`4056642876-cancelar`];
    cy.clickIfExist(`[data-cy="reinf"]`);
      cy.clickIfExist(`[data-cy="reinf/processos"]`);
      cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
      cy.clickIfExist(`[data-cy="4056642876-executar"]`);
      cy.clickIfExist(`[data-cy="4056642876-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4056642876-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-selecionar critérios`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-selecionar critérios`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-selecionar critérios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-estabelecimentos da regra`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-estabelecimentos da regra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-itens da regra`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-itens da regra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-itens da regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-testar regra`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-testar regra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-testar regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-copiar regra`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-copiar regra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-copiar regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-remover item`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-remover item`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-powerselect-indEntradaSaida-2666930413-powerselect-imposto-2666930413-textarea-obsRegra and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-powerselect-indEntradaSaida-2666930413-powerselect-imposto-2666930413-textarea-obsRegra`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.fillInputPowerSelect(`[data-cy="2666930413-powerselect-indEntradaSaida"] input`);
cy.fillInputPowerSelect(`[data-cy="2666930413-powerselect-imposto"] input`);
cy.fillInput(`[data-cy="2666930413-textarea-obsRegra"] input`, `Personal Loan Account`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo->3183381421-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-novo`,`3183381421-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-novo"]`);
      cy.clickIfExist(`[data-cy="3183381421-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo->3183381421-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-novo`,`3183381421-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-novo"]`);
      cy.clickIfExist(`[data-cy="3183381421-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo->3183381421-checkbox-indConverteValor-3183381421-powerselect-ufCodigo-3183381421-powerselect-vlImposto-3183381421-powerselect-dtFatoGerador-3183381421-powerselect-estabApuracao-3183381421-powerselect-codReceita-3183381421-powerselect-timCodigo and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-novo`,`3183381421-checkbox-indConverteValor-3183381421-powerselect-ufCodigo-3183381421-powerselect-vlImposto-3183381421-powerselect-dtFatoGerador-3183381421-powerselect-estabApuracao-3183381421-powerselect-codReceita-3183381421-powerselect-timCodigo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-novo"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3183381421-checkbox-indConverteValor"] textarea`);
cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-ufCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-vlImposto"] input`);
cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-dtFatoGerador"] input`);
cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-estabApuracao"] input`);
cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-codReceita"] input`);
cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-timCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-remover item`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-eyeoutlined`,`1020274267-remover item`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1020274267-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-eyeoutlined`,`1020274267-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1020274267-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-eyeoutlined`,`1020274267-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1020274267-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-checkbox-indConverteValor-1020274267-powerselect-ufCodigo-1020274267-powerselect-vlImposto-1020274267-powerselect-dtFatoGerador-1020274267-powerselect-estabApuracao-1020274267-powerselect-codReceita-1020274267-powerselect-timCodigo and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-eyeoutlined`,`1020274267-checkbox-indConverteValor-1020274267-powerselect-ufCodigo-1020274267-powerselect-vlImposto-1020274267-powerselect-dtFatoGerador-1020274267-powerselect-estabApuracao-1020274267-powerselect-codReceita-1020274267-powerselect-timCodigo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1020274267-checkbox-indConverteValor"] textarea`);
cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-ufCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-vlImposto"] input`);
cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-dtFatoGerador"] input`);
cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-estabApuracao"] input`);
cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-codReceita"] input`);
cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-timCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-pesquisar->4001809800-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`,`4001809800-pesquisar`,`4001809800-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.clickIfExist(`[data-cy="4001809800-pesquisar"]`);
      cy.clickIfExist(`[data-cy="4001809800-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-pesquisar->4001809800-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-novo`,`4001809800-pesquisar`,`4001809800-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-novo"]`);
      cy.clickIfExist(`[data-cy="4001809800-pesquisar"]`);
      cy.clickIfExist(`[data-cy="4001809800-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-pesquisar->2263500228-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-pesquisar`,`2263500228-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-pesquisar"]`);
      cy.clickIfExist(`[data-cy="2263500228-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-copiar regra->2263500228-input-copiaFrom-2263500228-input-copiaTo and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-copiar regra`,`2263500228-input-copiaFrom-2263500228-input-copiaTo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-copiar regra"]`);
      cy.fillInput(`[data-cy="2263500228-input-copiaFrom"] textarea`, `ubiquitous`);
cy.fillInput(`[data-cy="2263500228-input-copiaTo"] textarea`, `purple`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo->2148999588-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-novo`,`2148999588-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-novo"]`);
      cy.clickIfExist(`[data-cy="2148999588-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo->2148999588-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-novo`,`2148999588-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-novo"]`);
      cy.clickIfExist(`[data-cy="2148999588-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo->2148999588-powerselect-timCodigo-2148999588-powerselect-periodicidade-2148999588-input-number-diaInicio-2148999588-input-number-diaFim-2148999588-checkbox-desmembraGuia and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-novo`,`2148999588-powerselect-timCodigo-2148999588-powerselect-periodicidade-2148999588-input-number-diaInicio-2148999588-input-number-diaFim-2148999588-checkbox-desmembraGuia`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2148999588-powerselect-timCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2148999588-powerselect-periodicidade"] input`);
cy.fillInput(`[data-cy="2148999588-input-number-diaInicio"] textarea`, `6`);
cy.fillInput(`[data-cy="2148999588-input-number-diaFim"] textarea`, `3`);
cy.fillInputCheckboxOrRadio(`[data-cy="2148999588-checkbox-desmembraGuia"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-remover item`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-eyeoutlined`,`3697625195-remover item`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="3697625195-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-salvar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-eyeoutlined`,`3697625195-salvar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="3697625195-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-eyeoutlined`,`3697625195-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="3697625195-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-powerselect-timCodigo-3697625195-powerselect-periodicidade-3697625195-input-number-diaInicio-3697625195-input-number-diaFim-3697625195-checkbox-desmembraGuia and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/apuracao-impostos`,`3551826945-eyeoutlined`,`2263500228-eyeoutlined`,`3697625195-powerselect-timCodigo-3697625195-powerselect-periodicidade-3697625195-input-number-diaInicio-3697625195-input-number-diaFim-3697625195-checkbox-desmembraGuia`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
      cy.fillInputPowerSelect(`[data-cy="3697625195-powerselect-timCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3697625195-powerselect-periodicidade"] input`);
cy.fillInput(`[data-cy="3697625195-input-number-diaInicio"] textarea`, `2`);
cy.fillInput(`[data-cy="3697625195-input-number-diaFim"] textarea`, `6`);
cy.fillInputCheckboxOrRadio(`[data-cy="3697625195-checkbox-desmembraGuia"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-executar->1906165251-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-executar`,`1906165251-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-executar"]`);
      cy.clickIfExist(`[data-cy="1906165251-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-executar->1906165251-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-executar`,`1906165251-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-executar"]`);
      cy.clickIfExist(`[data-cy="1906165251-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-agendamentos->1906165251-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-agendamentos`,`1906165251-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1906165251-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-visualização->1906165251-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-visualização`,`1906165251-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1906165251-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar->4218034749-múltipla seleção`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-executar`,`4218034749-múltipla seleção`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-executar"]`);
      cy.clickIfExist(`[data-cy="4218034749-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar->4218034749-agendar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-executar`,`4218034749-agendar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-executar"]`);
      cy.clickIfExist(`[data-cy="4218034749-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-agendamentos->4218034749-voltar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-agendamentos`,`4218034749-voltar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4218034749-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-visualização->4218034749-item- and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-visualização`,`4218034749-item-`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4218034749-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-detalhes->4218034749-não há dados disponíveis para impressão`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-detalhes`,`4218034749-não há dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-detalhes"]`);
      cy.clickIfExist(`[data-cy="4218034749-não há dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar->447335411-múltipla seleção->447335411-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`,`447335411-executar`,`447335411-múltipla seleção`,`447335411-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
      cy.clickIfExist(`[data-cy="447335411-executar"]`);
      cy.clickIfExist(`[data-cy="447335411-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="447335411-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar->717809746-múltipla seleção->717809746-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`,`717809746-executar`,`717809746-múltipla seleção`,`717809746-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
      cy.clickIfExist(`[data-cy="717809746-executar"]`);
      cy.clickIfExist(`[data-cy="717809746-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="717809746-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar->133362368-múltipla seleção->133362368-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`,`133362368-executar`,`133362368-múltipla seleção`,`133362368-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
      cy.clickIfExist(`[data-cy="133362368-executar"]`);
      cy.clickIfExist(`[data-cy="133362368-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="133362368-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar->235713453-múltipla seleção->235713453-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-executar`,`235713453-múltipla seleção`,`235713453-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-executar"]`);
      cy.clickIfExist(`[data-cy="235713453-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="235713453-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-expandir->235713453-diminuir`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`,`235713453-abrir visualização`,`235713453-expandir`,`235713453-diminuir`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
      cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="235713453-expandir"]`);
      cy.clickIfExist(`[data-cy="235713453-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar->2590522946-múltipla seleção->2590522946-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`,`2590522946-executar`,`2590522946-múltipla seleção`,`2590522946-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
      cy.clickIfExist(`[data-cy="2590522946-executar"]`);
      cy.clickIfExist(`[data-cy="2590522946-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2590522946-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar->2403317122-múltipla seleção->2403317122-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`,`2403317122-executar`,`2403317122-múltipla seleção`,`2403317122-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
      cy.clickIfExist(`[data-cy="2403317122-executar"]`);
      cy.clickIfExist(`[data-cy="2403317122-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2403317122-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar->3689771228-múltipla seleção->3689771228-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`,`3689771228-executar`,`3689771228-múltipla seleção`,`3689771228-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
      cy.clickIfExist(`[data-cy="3689771228-executar"]`);
      cy.clickIfExist(`[data-cy="3689771228-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3689771228-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar->1130815789-múltipla seleção->1130815789-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`,`1130815789-executar`,`1130815789-múltipla seleção`,`1130815789-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
      cy.clickIfExist(`[data-cy="1130815789-executar"]`);
      cy.clickIfExist(`[data-cy="1130815789-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1130815789-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar->3148305101-múltipla seleção->3148305101-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/relatorios`,`lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`,`3148305101-executar`,`3148305101-múltipla seleção`,`3148305101-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
      cy.clickIfExist(`[data-cy="3148305101-executar"]`);
      cy.clickIfExist(`[data-cy="3148305101-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3148305101-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf->2527279051-executar->2527279051-múltipla seleção->2527279051-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/preparacao-dirf`,`2527279051-executar`,`2527279051-múltipla seleção`,`2527279051-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
      cy.clickIfExist(`[data-cy="2527279051-executar"]`);
      cy.clickIfExist(`[data-cy="2527279051-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2527279051-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-múltipla seleção->1745767366-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/identificacao-retroativa`,`1745767366-executar`,`1745767366-múltipla seleção`,`1745767366-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
      cy.clickIfExist(`[data-cy="1745767366-executar"]`);
      cy.clickIfExist(`[data-cy="1745767366-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1745767366-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-múltipla seleção->1673335588-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-executar`,`1673335588-múltipla seleção`,`1673335588-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-executar"]`);
      cy.clickIfExist(`[data-cy="1673335588-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1673335588-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-expandir->1673335588-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/processos`,`tributo-terceiro/processos/consolid-desconsolid`,`1673335588-abrir visualização`,`1673335588-expandir`,`1673335588-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
      cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1673335588-expandir"]`);
      cy.clickIfExist(`[data-cy="1673335588-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-múltipla seleção->3329339398-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-executar`,`3329339398-múltipla seleção`,`3329339398-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-executar"]`);
      cy.clickIfExist(`[data-cy="3329339398-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3329339398-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-expandir->3329339398-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/servico-por-periodo`,`3329339398-abrir visualização`,`3329339398-expandir`,`3329339398-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
      cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3329339398-expandir"]`);
      cy.clickIfExist(`[data-cy="3329339398-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar->1917228366-múltipla seleção->1917228366-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dof-rpa-sem-limp`,`1917228366-executar`,`1917228366-múltipla seleção`,`1917228366-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
      cy.clickIfExist(`[data-cy="1917228366-executar"]`);
      cy.clickIfExist(`[data-cy="1917228366-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1917228366-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-múltipla seleção->977623228-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pgtos-consolidados`,`977623228-executar`,`977623228-múltipla seleção`,`977623228-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
      cy.clickIfExist(`[data-cy="977623228-executar"]`);
      cy.clickIfExist(`[data-cy="977623228-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="977623228-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar->3465203885-múltipla seleção->3465203885-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/critica-pagamentos`,`3465203885-executar`,`3465203885-múltipla seleção`,`3465203885-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
      cy.clickIfExist(`[data-cy="3465203885-executar"]`);
      cy.clickIfExist(`[data-cy="3465203885-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3465203885-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-múltipla seleção->1614719659-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-executar`,`1614719659-múltipla seleção`,`1614719659-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-executar"]`);
      cy.clickIfExist(`[data-cy="1614719659-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1614719659-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-expandir->1614719659-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/inss-recolhimento`,`1614719659-abrir visualização`,`1614719659-expandir`,`1614719659-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
      cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614719659-expandir"]`);
      cy.clickIfExist(`[data-cy="1614719659-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-múltipla seleção->3047460406-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-executar`,`3047460406-múltipla seleção`,`3047460406-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-executar"]`);
      cy.clickIfExist(`[data-cy="3047460406-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3047460406-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-expandir->3047460406-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/irrf-sem-servicos`,`3047460406-abrir visualização`,`3047460406-expandir`,`3047460406-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
      cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3047460406-expandir"]`);
      cy.clickIfExist(`[data-cy="3047460406-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-múltipla seleção->2966158800-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/pcc-residuo-parcelas`,`2966158800-executar`,`2966158800-múltipla seleção`,`2966158800-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
      cy.clickIfExist(`[data-cy="2966158800-executar"]`);
      cy.clickIfExist(`[data-cy="2966158800-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2966158800-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-múltipla seleção->2798312950-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/resumo-cod-receita`,`2798312950-executar`,`2798312950-múltipla seleção`,`2798312950-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
      cy.clickIfExist(`[data-cy="2798312950-executar"]`);
      cy.clickIfExist(`[data-cy="2798312950-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2798312950-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-múltipla seleção->533561082-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/relatorios`,`tributo-terceiro/relatorios/dirf-x-dctf`,`533561082-executar`,`533561082-múltipla seleção`,`533561082-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
      cy.clickIfExist(`[data-cy="533561082-executar"]`);
      cy.clickIfExist(`[data-cy="533561082-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="533561082-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-múltipla seleção->716117882-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento`,`716117882-executar`,`716117882-múltipla seleção`,`716117882-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
      cy.clickIfExist(`[data-cy="716117882-executar"]`);
      cy.clickIfExist(`[data-cy="716117882-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="716117882-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-múltipla seleção->1023520286-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/informe-rendimento-html`,`1023520286-executar`,`1023520286-múltipla seleção`,`1023520286-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
      cy.clickIfExist(`[data-cy="1023520286-executar"]`);
      cy.clickIfExist(`[data-cy="1023520286-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1023520286-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-múltipla seleção->2587924048-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento`,`2587924048-executar`,`2587924048-múltipla seleção`,`2587924048-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
      cy.clickIfExist(`[data-cy="2587924048-executar"]`);
      cy.clickIfExist(`[data-cy="2587924048-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2587924048-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-múltipla seleção->1339819272-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/etiquetas-enderecamento-html`,`1339819272-executar`,`1339819272-múltipla seleção`,`1339819272-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
      cy.clickIfExist(`[data-cy="1339819272-executar"]`);
      cy.clickIfExist(`[data-cy="1339819272-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1339819272-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-múltipla seleção->1234427231-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/comprovante`,`tributo-terceiro/comprovante/exportacao-txt`,`1234427231-executar`,`1234427231-múltipla seleção`,`1234427231-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
      cy.clickIfExist(`[data-cy="1234427231-executar"]`);
      cy.clickIfExist(`[data-cy="1234427231-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1234427231-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualização->52374570-item- and submit`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-visualização`,`52374570-item-`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="52374570-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-aumentar o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-abrir visualização`,`52374570-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-diminuir o zoom`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-abrir visualização`,`52374570-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-expandir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-abrir visualização`,`52374570-expandir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-download`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-abrir visualização`,`52374570-download`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualizar->52374570-dados disponíveis para impressão`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-visualizar`,`52374570-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
      cy.clickIfExist(`[data-cy="52374570-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-rightoutlined->1874405030-downoutlined`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`,`1874405030-rightoutlined`,`1874405030-downoutlined`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1874405030-rightoutlined"]`);
      cy.clickIfExist(`[data-cy="1874405030-downoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-habilitar edição->1874405030-confirmar  edição`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`,`1874405030-habilitar edição`,`1874405030-confirmar  edição`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1874405030-habilitar edição"]`);
      cy.clickIfExist(`[data-cy="1874405030-confirmar  edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-abrir janela de edição->1874405030-power-search-button`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-ajustar parâmetros da geração`,`1874405030-abrir janela de edição`,`1874405030-power-search-button`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1874405030-abrir janela de edição"]`);
      cy.clickIfExist(`[data-cy="1874405030-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-expandir->611534337-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-visualizar resultado da geração`,`611534337-expandir`,`611534337-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="611534337-expandir"]`);
      cy.clickIfExist(`[data-cy="611534337-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão->3661377306-editar->3661377306-salvar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-protocolo transmissão`,`3661377306-editar`,`3661377306-salvar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="3661377306-editar"]`);
      cy.clickIfExist(`[data-cy="3661377306-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão->3661377306-editar->3661377306-cancelar`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-protocolo transmissão`,`3661377306-editar`,`3661377306-cancelar`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="3661377306-editar"]`);
      cy.clickIfExist(`[data-cy="3661377306-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-expandir->52374570-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/obrigacoes-executadas`,`52374570-abrir visualização`,`52374570-expandir`,`52374570-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-expandir"]`);
      cy.clickIfExist(`[data-cy="52374570-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-estabelecimentos da regra->2666930413-power-search-button`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-estabelecimentos da regra`,`2666930413-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
      cy.clickIfExist(`[data-cy="2666930413-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-estabelecimentos da regra->2666930413-carregar mais`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-estabelecimentos da regra`,`2666930413-carregar mais`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
      cy.clickIfExist(`[data-cy="2666930413-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-estabelecimentos da regra->2666930413-fechar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-estabelecimentos da regra`,`2666930413-fechar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
      cy.clickIfExist(`[data-cy="2666930413-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-estabelecimentos da regra->2666930413-power-search-input and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-estabelecimentos da regra`,`2666930413-power-search-input`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
      cy.fillInputPowerSearch(`[data-cy="2666930413-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp->2666930413-copiar regra->2666930413-input-copiaFrom-2666930413-input-copiaTo and submit`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/regras`,`lancamento-e-guia-de-impostos/regras/lancamento-impostos`,`3509287715-selectoutlined`,`2512964284-limp`,`2666930413-copiar regra`,`2666930413-input-copiaFrom-2666930413-input-copiaTo`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2512964284-limp"]`);
      cy.clickIfExist(`[data-cy="2666930413-copiar regra"]`);
      cy.fillInput(`[data-cy="2666930413-input-copiaFrom"] textarea`, `parsing`);
cy.fillInput(`[data-cy="2666930413-input-copiaTo"] textarea`, `Martinica`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-executar->1906165251-múltipla seleção->1906165251-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`,`2728661096-gerar`,`1906165251-executar`,`1906165251-múltipla seleção`,`1906165251-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
      cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
      cy.clickIfExist(`[data-cy="1906165251-executar"]`);
      cy.clickIfExist(`[data-cy="1906165251-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1906165251-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar->4218034749-múltipla seleção->4218034749-cancelar`, () => {
const actualId = [`root`,`lancamento-e-guia-de-impostos`,`lancamento-e-guia-de-impostos/apuracoes`,`lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`,`856883396-gerar`,`4218034749-executar`,`4218034749-múltipla seleção`,`4218034749-cancelar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
      cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
      cy.clickIfExist(`[data-cy="856883396-gerar"]`);
      cy.clickIfExist(`[data-cy="4218034749-executar"]`);
      cy.clickIfExist(`[data-cy="4218034749-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4218034749-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-expandir->52374570-diminuir`, () => {
const actualId = [`root`,`tributo-terceiro`,`tributo-terceiro/obrigacoes`,`tributo-terceiro/obrigacoes/solicitacoes`,`611534337-agendamentos`,`52374570-abrir visualização`,`52374570-expandir`,`52374570-diminuir`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
      cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
      cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="52374570-expandir"]`);
      cy.clickIfExist(`[data-cy="52374570-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
