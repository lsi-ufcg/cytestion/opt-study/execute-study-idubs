describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-agendamentos->338915745-voltar`, () => {
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53985074D%7C%7C53985074&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="338915745-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-agendamentos->4056642876-voltar`, () => {
    cy.visit(
      'http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56458077D%7C%7C56458077&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4056642876-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-testar regra`, () => {
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2666930413-testar regra"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-agendamentos->447335411-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46703635D%7C%7C46703635&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="447335411-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-agendamentos->717809746-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972667D%7C%7C26972667&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="717809746-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-agendamentos->133362368-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970534D%7C%7C54970534&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="133362368-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-agendamentos->235713453-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972654D%7C%7C26972654&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="235713453-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-agendamentos->2590522946-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210440D%7C%7C210440&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2590522946-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-agendamentos->2403317122-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972694D%7C%7C26972694&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2403317122-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-agendamentos->3689771228-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~238454D%7C%7C238454&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3689771228-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-agendamentos->1130815789-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972681D%7C%7C26972681&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1130815789-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-agendamentos->3148305101-voltar`, () => {
    cy.visit(
      'http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970542D%7C%7C54970542&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3148305101-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-agendamentos->1745767366-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~247835D%7C%7C247835&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1745767366-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-agendamentos->1673335588-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47399253D%7C%7C47399253&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1673335588-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-agendamentos->3329339398-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970468D%7C%7C54970468&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3329339398-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-agendamentos->1917228366-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245578D%7C%7C245578&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1917228366-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-agendamentos->3840643021-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970496D%7C%7C54970496&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3840643021-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-agendamentos->977623228-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970502D%7C%7C54970502&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="977623228-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-agendamentos->3465203885-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245758D%7C%7C245758&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3465203885-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-agendamentos->1614719659-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970482D%7C%7C54970482&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1614719659-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-agendamentos->3047460406-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970475D%7C%7C54970475&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3047460406-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-agendamentos->3040111907-voltar`, () => {
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~6821028D%7C%7C6821028&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3040111907-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-agendamentos->2966158800-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12904528D%7C%7C12904528&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2966158800-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-agendamentos->682447114-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970509D%7C%7C54970509&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="682447114-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-agendamentos->2798312950-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970522D%7C%7C54970522&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2798312950-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-agendamentos->533561082-voltar`, () => {
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~248256D%7C%7C248256&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="533561082-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-agendamentos->716117882-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890954D%7C%7C47890954&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="716117882-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-agendamentos->1023520286-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890961D%7C%7C47890961&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1023520286-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-agendamentos->2587924048-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890975D%7C%7C47890975&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2587924048-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-agendamentos->1339819272-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890968D%7C%7C47890968&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1339819272-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-agendamentos->1234427231-voltar`, () => {
    cy.visit(
      'http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970489D%7C%7C54970489&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1234427231-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo-441105128-input-beneficiarioCpfCnpj-441105128-input-beneficiarioRazaoSocial-441105128-powerselect-beneficiarioIndFisJur-441105128-powerselect-beneficiarioPais-441105128-powerselect-beneficiarioIndNif-441105128-input-beneficiarioCodigoNif-441105128-powerselect-beneficiarioRelFontePag and submit`, () => {
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="699364833-salvar"]`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioPfjCodigo"] input`);
    cy.fillInput(`[data-cy="441105128-input-beneficiarioCpfCnpj"] textarea`, `Viela`);
    cy.fillInput(`[data-cy="441105128-input-beneficiarioRazaoSocial"] textarea`, `Standalone`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioIndFisJur"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioIndNif"] input`);
    cy.fillInput(`[data-cy="441105128-input-beneficiarioCodigoNif"] textarea`, `Uzbekistan Sum`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioRelFontePag"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-agendamentos->1906165251-voltar`, () => {
    cy.visit(
      'http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47670891D%7C%7C47670891&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1906165251-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-agendamentos->4218034749-voltar`, () => {
    cy.visit(
      'http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47781678D%7C%7C47781678&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4218034749-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-novo->3217114759-powerselect-estCodigo and submit`, () => {
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2415241101-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3217114759-powerselect-estCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-powerselect-indTributacaoFolha and submit`, () => {
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2661856238-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="123050857-powerselect-produtorPfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="123050857-powerselect-indTributacaoFolha"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.fillInputPowerSelect(`[data-cy="3918073255-powerselect-indTributacaoFolha"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
