describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
    const actualId = [`root`, `tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
    const actualId = [`root`, `tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes`, () => {
    const actualId = [`root`, `integracoes`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
    const actualId = [`root`, `transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf`, () => {
    const actualId = [`root`, `reinf`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro`, () => {
    const actualId = [`root`, `tributo-terceiro`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
    const actualId = [`root`, `processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
    const actualId = [`root`, `downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!`, () => {
    const actualId = [`root`, `2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!`];
    cy.clickIfExist(`[data-cy="2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2585622888-r-2099 entrega dispensada.`, () => {
    const actualId = [`root`, `2585622888-r-2099 entrega dispensada.`];
    cy.clickIfExist(`[data-cy="2585622888-r-2099 entrega dispensada."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2585622888-atualizar pendências`, () => {
    const actualId = [`root`, `2585622888-atualizar pendências`];
    cy.clickIfExist(`[data-cy="2585622888-atualizar pendências"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/classificacao-servico`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/classificacao-servico`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/classificacao-servico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/codigo-receita`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/codigo-receita`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/codigo-receita"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/natureza-rendimento`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/natureza-rendimento`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/natureza-rendimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/parametrizacao-geral`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/parametrizacao-geral"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pessoa-fisica-juridica`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pessoa-fisica-juridica`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pessoa-fisica-juridica"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/hierarquia-pessoa`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/hierarquia-pessoa`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/hierarquia-pessoa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/tipo-documento-nao-fiscal`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/tipo-documento-nao-fiscal`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/tipo-documento-nao-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/natureza-operacao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/natureza-operacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/natureza-operacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/prestacao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/prestacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/prestacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/servicos-iss`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/servicos-iss`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/servicos-iss"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/processo`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/processo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/especie-dof`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/especie-dof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/especie-dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/rubricas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/condicoes-pagamento-guia`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/condicoes-pagamento-guia`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/condicoes-pagamento-guia"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/tipos-impostos-da-apuracao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/tipos-impostos-da-apuracao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/tipos-impostos-da-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/outras-remuneracoes-inss`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/outras-remuneracoes-inss"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/dados-interface`, () => {
    const actualId = [`root`, `integracoes`, `integracoes/dados-interface`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
    cy.clickIfExist(`[data-cy="integracoes/dados-interface"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/execucao-interface`, () => {
    const actualId = [`root`, `integracoes`, `integracoes/execucao-interface`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
    cy.clickIfExist(`[data-cy="integracoes/execucao-interface"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/documentos-fiscais`, () => {
    const actualId = [`root`, `transacoes`, `transacoes/documentos-fiscais`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
    cy.clickIfExist(`[data-cy="transacoes/documentos-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->385672452-power-search-button`, () => {
    const actualId = [`root`, `downloads`, `385672452-power-search-button`];
    cy.visit('http://system-A6/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="385672452-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->385672452-download`, () => {
    const actualId = [`root`, `downloads`, `385672452-download`];
    cy.visit('http://system-A6/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="385672452-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->385672452-detalhes`, () => {
    const actualId = [`root`, `downloads`, `385672452-detalhes`];
    cy.visit('http://system-A6/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="385672452-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->385672452-excluir`, () => {
    const actualId = [`root`, `downloads`, `385672452-excluir`];
    cy.visit('http://system-A6/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="385672452-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element 2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!->2585622888-exportar dados`, () => {
    const actualId = [`root`, `2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!`, `2585622888-exportar dados`];
    cy.clickIfExist(`[data-cy="2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!"]`);
    cy.clickIfExist(`[data-cy="2585622888-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values 2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!->2585622888-checkbox-undefined and submit`, () => {
    const actualId = [`root`, `2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!`, `2585622888-checkbox-undefined`];
    cy.clickIfExist(`[data-cy="2585622888-existem eventos de períodos anteriores a 04/2024 pendentes de geração!"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2585622888-checkbox-undefined"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values 2585622888-atualizar pendências->2585622888-checkbox-checkGeracaoREINF-2585622888-checkbox-checkGeracaoESOCIAL and submit`, () => {
    const actualId = [`root`, `2585622888-atualizar pendências`, `2585622888-checkbox-checkGeracaoREINF-2585622888-checkbox-checkGeracaoESOCIAL`];
    cy.clickIfExist(`[data-cy="2585622888-atualizar pendências"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2585622888-checkbox-checkGeracaoREINF"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2585622888-checkbox-checkGeracaoESOCIAL"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-novo`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2746791129-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-power-search-button`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-power-search-button`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2746791129-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-deleteoutlined`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-deleteoutlined`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2746791129-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/outras-remuneracoes-inss`, `308557383-novo`];
    cy.visit('http://system-A6/outras-remuneracoes-inss?periodoCompetencia=~eq~04%2F2024%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="308557383-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-power-search-button`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/outras-remuneracoes-inss`, `308557383-power-search-button`];
    cy.visit('http://system-A6/outras-remuneracoes-inss?periodoCompetencia=~eq~04%2F2024%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="308557383-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/e-social`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/e-social`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis/e-social"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-2000`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-2000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-2000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis"]`);
    cy.clickIfExist(`[data-cy="reinf/paineis/serie-r-4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/recebimentos-diversos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-diversos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos"]`);
    cy.clickIfExist(`[data-cy="reinf/recebimentos-pagamentos/pagamentos-r4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/regras"]`);
    cy.clickIfExist(`[data-cy="reinf/regras/apuracao-cprb"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/comercializacao-rural`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes/comercializacao-rural"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes/aquisicao-producao-rural"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/cprb`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/cprb`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes"]`);
    cy.clickIfExist(`[data-cy="reinf/apuracoes/cprb"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/geracao-eventos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/geracao-eventos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/geracao-eventos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/controle-eventos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/controle-eventos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/controle-eventos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consulta-job`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/consulta-job"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/execucao-processos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/execucao-processos`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/execucao-processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/automacao-pendencias`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/agendamento-processo`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/agendamento-processo`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/agendamento-processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/apuracao-r4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/consolidacao-desconsolidacao-r4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/execucao-relatorios`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-2010-r-2020`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pagamentos-beneficiario-r4000`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/detalhes-eventos-criticados`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/apuracao-com-rural`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/apuracao-com-rural`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/apuracao-com-rural"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9011`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9015`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/lancamento-impostos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/regras/apuracao-impostos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/guia-complementar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/guia-complementar`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/guia-complementar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`];
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios"]`);
    cy.clickIfExist(`[data-cy="lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/pag-consolidados`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/pag-consolidados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/dirf"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/dirf/importacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/preparacao-dirf`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/preparacao-dirf`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos/preparacao-dirf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos/identificacao-retroativa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/processos/consolid-desconsolid"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/retencoes-notas-fiscais`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/retencoes-notas-fiscais`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/retencoes-notas-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/servico-por-periodo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dof-rpa-sem-limp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-analiticos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pgtos-consolidados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/critica-pagamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/inss-recolhimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/irrf-sem-servicos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-retidos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/pcc-residuo-parcelas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-por-beneficiario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/resumo-cod-receita"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/relatorios/dirf-x-dctf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/informe-rendimento-html"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/etiquetas-enderecamento-html"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/comprovante/exportacao-txt"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/solicitacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/obrigacoes-executadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/periodicidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="tributo-terceiro"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes"]`);
    cy.clickIfExist(`[data-cy="tributo-terceiro/obrigacoes/configuracao-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo->2360175536-salvar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-novo`, `2360175536-salvar`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2360175536-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo->2360175536-voltar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-novo`, `2360175536-voltar`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2360175536-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-novo->2360175536-powerselect-estCodigo-2360175536-input-rubricaId-2360175536-input-codigo-2360175536-powerselect-tipo-2360175536-input-descricao-2360175536-powerselect-indApuracaoIr-2360175536-checkbox-indRubricaDefault and submit`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-novo`, `2360175536-powerselect-estCodigo-2360175536-input-rubricaId-2360175536-input-codigo-2360175536-powerselect-tipo-2360175536-input-descricao-2360175536-powerselect-indApuracaoIr-2360175536-checkbox-indRubricaDefault`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2746791129-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2360175536-powerselect-estCodigo"] input`);
    cy.fillInput(`[data-cy="2360175536-input-rubricaId"] textarea`, `China`);
    cy.fillInput(`[data-cy="2360175536-input-codigo"] textarea`, `Designer`);
    cy.fillInputPowerSelect(`[data-cy="2360175536-powerselect-tipo"] input`);
    cy.fillInput(`[data-cy="2360175536-input-descricao"] textarea`, `Fantastic`);
    cy.fillInputPowerSelect(`[data-cy="2360175536-powerselect-indApuracaoIr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2360175536-checkbox-indRubricaDefault"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-remover item`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-remover item`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-salvar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-salvar`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-voltar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-voltar`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-novo`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-novo`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-power-search-button`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-power-search-button`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-visualizar/editar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-visualizar/editar`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-excluir`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-excluir`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-powerselect-estCodigo-2825312631-input-rubricaId-2825312631-input-codigo-2825312631-powerselect-tipo-2825312631-input-descricao-2825312631-powerselect-indApuracaoIr-2825312631-checkbox-indRubricaDefault and submit`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-powerselect-estCodigo-2825312631-input-rubricaId-2825312631-input-codigo-2825312631-powerselect-tipo-2825312631-input-descricao-2825312631-powerselect-indApuracaoIr-2825312631-checkbox-indRubricaDefault`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2746791129-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-estCodigo"] input`);
    cy.fillInput(`[data-cy="2825312631-input-rubricaId"] textarea`, `SQL`);
    cy.fillInput(`[data-cy="2825312631-input-codigo"] textarea`, `Phased`);
    cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-tipo"] input`);
    cy.fillInput(`[data-cy="2825312631-input-descricao"] textarea`, `orange`);
    cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-indApuracaoIr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2825312631-checkbox-indRubricaDefault"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo->1841274626-salvar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/outras-remuneracoes-inss`, `308557383-novo`, `1841274626-salvar`];
    cy.visit('http://system-A6/outras-remuneracoes-inss/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1841274626-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo->1841274626-voltar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/outras-remuneracoes-inss`, `308557383-novo`, `1841274626-voltar`];
    cy.visit('http://system-A6/outras-remuneracoes-inss/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1841274626-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/outras-remuneracoes-inss->308557383-novo->1841274626-powerselect-trabalhadorPfjCodigo-1841274626-input-number-trabalhadorCategoria-1841274626-powerselect-tipoInscricao-1841274626-input-nomeOutraRetencao-1841274626-input-monetary-vlBaseCalculoInss-1841274626-input-monetary-vlRetencaoInss and submit`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/outras-remuneracoes-inss`, `308557383-novo`, `1841274626-powerselect-trabalhadorPfjCodigo-1841274626-input-number-trabalhadorCategoria-1841274626-powerselect-tipoInscricao-1841274626-input-nomeOutraRetencao-1841274626-input-monetary-vlBaseCalculoInss-1841274626-input-monetary-vlRetencaoInss`];
    cy.visit('http://system-A6/outras-remuneracoes-inss?periodoCompetencia=~eq~04%2F2024%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="308557383-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1841274626-powerselect-trabalhadorPfjCodigo"] input`);
    cy.fillInput(`[data-cy="1841274626-input-number-trabalhadorCategoria"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="1841274626-powerselect-tipoInscricao"] input`);
    cy.fillInput(`[data-cy="1841274626-input-nomeOutraRetencao"] textarea`, `Practical`);
    cy.fillInput(`[data-cy="1841274626-input-monetary-vlBaseCalculoInss"] textarea`, `3`);
    cy.fillInput(`[data-cy="1841274626-input-monetary-vlRetencaoInss"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-2000->1291494902-visualizar relatório`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-2000`, `1291494902-visualizar relatório`];
    cy.visit('http://system-A6/painel/tributos/tributos-serie-r2000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1291494902-visualizar relatório"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000->1291554484-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-4000`, `1291554484-button`];
    cy.visit('http://system-A6/painel/tributos/tributos-serie-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1291554484-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000->1291554484-visualizar relatório`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-4000`, `1291554484-visualizar relatório`];
    cy.visit('http://system-A6/painel/tributos/tributos-serie-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1291554484-visualizar relatório"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtRecebimento=~mth~1712479469824D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4096060046-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtRecebimento=~mth~1712479469824D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4096060046-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtPagamento=~mth~1712479480041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2030434943-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtPagamento=~mth~1712479480041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2030434943-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712479490269D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712479490269D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-novo`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-power-search-button`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-copyoutlined`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-deleteoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-deleteoutlined`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/comercializacao-rural`, `3461523942-novo`];
    cy.visit('http://system-A6/apuracoes/comercializacao-rural');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3461523942-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/comercializacao-rural`, `3461523942-power-search-button`];
    cy.visit('http://system-A6/apuracoes/comercializacao-rural');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3461523942-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126801533-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-power-search-button`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126801533-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/geracao-eventos->2188111078-checkbox-utilizaDataFatoGerador-2188111078-checkbox-transmissaoAutomatica-2188111078-checkbox-geracaoMatrizFilial and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/geracao-eventos`, `2188111078-checkbox-utilizaDataFatoGerador-2188111078-checkbox-transmissaoAutomatica-2188111078-checkbox-geracaoMatrizFilial`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/geracao-eventos"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2188111078-checkbox-utilizaDataFatoGerador"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2188111078-checkbox-transmissaoAutomatica"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2188111078-checkbox-geracaoMatrizFilial"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/controle-eventos->3830594493-reloadoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/controle-eventos`, `3830594493-reloadoutlined`];
    cy.visit('http://system-A6/processos/reinf/controle-eventos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3830594493-reloadoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/controle-eventos->3830594493-fechar período`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/controle-eventos`, `3830594493-fechar período`];
    cy.visit('http://system-A6/processos/reinf/controle-eventos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3830594493-fechar período"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/controle-eventos->3830594493-powerselect-contribuinte-3830594493-checkbox-hierarquiaMatrizFilial and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/controle-eventos`, `3830594493-powerselect-contribuinte-3830594493-checkbox-hierarquiaMatrizFilial`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/controle-eventos"]`);
    cy.fillInputPowerSelect(`[data-cy="3830594493-powerselect-contribuinte"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3830594493-checkbox-hierarquiaMatrizFilial"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job->3385006537-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consulta-job`, `3385006537-power-search-button`];
    cy.visit('http://system-A6/processos/reinf/consulta-job');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3385006537-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job->3385006537-eyeoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consulta-job`, `3385006537-eyeoutlined`];
    cy.visit('http://system-A6/processos/reinf/consulta-job');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3385006537-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/execucao-processos->1118927095-agendar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/execucao-processos`, `1118927095-agendar`];
    cy.visit('http://system-A6/processos/reinf/execucao-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1118927095-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/execucao-processos->1118927095-executar processo`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/execucao-processos`, `1118927095-executar processo`];
    cy.visit('http://system-A6/processos/reinf/execucao-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1118927095-executar processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/execucao-processos->1118927095-powerselect-tipoExecucaoProcesso and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/execucao-processos`, `1118927095-powerselect-tipoExecucaoProcesso`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/execucao-processos"]`);
    cy.fillInputPowerSelect(`[data-cy="1118927095-powerselect-tipoExecucaoProcesso"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-detalhes do processo`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/automacao-pendencias`, `331885839-detalhes do processo`];
    cy.visit('http://system-A6/processos/reinf/automacao-pendencias');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="331885839-detalhes do processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-agendar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/automacao-pendencias`, `331885839-agendar`];
    cy.visit('http://system-A6/processos/reinf/automacao-pendencias');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="331885839-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-executar automação`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/automacao-pendencias`, `331885839-executar automação`];
    cy.visit('http://system-A6/processos/reinf/automacao-pendencias');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="331885839-executar automação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/automacao-pendencias->331885839-powerselect-serieEventos-331885839-checkbox-atualizarPendencias-331885839-checkbox-utilizarDataFatoGerador and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/automacao-pendencias`, `331885839-powerselect-serieEventos-331885839-checkbox-atualizarPendencias-331885839-checkbox-utilizarDataFatoGerador`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/processos"]`);
    cy.clickIfExist(`[data-cy="reinf/processos/automacao-pendencias"]`);
    cy.fillInputPowerSelect(`[data-cy="331885839-powerselect-serieEventos"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="331885839-checkbox-atualizarPendencias"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="331885839-checkbox-utilizarDataFatoGerador"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/agendamento-processo->1126184012-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/agendamento-processo`, `1126184012-power-search-button`];
    cy.visit('http://system-A6/processos/reinf/agendamento-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126184012-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/agendamento-processo->1126184012-eyeoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/agendamento-processo`, `1126184012-eyeoutlined`];
    cy.visit('http://system-A6/processos/reinf/agendamento-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126184012-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/agendamento-processo->1126184012-deleteoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/agendamento-processo`, `1126184012-deleteoutlined`];
    cy.visit('http://system-A6/processos/reinf/agendamento-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126184012-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-executar`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-agendamentos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-agendamentos`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-power-search-button`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-ajuda contextualizada`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-ajuda contextualizada`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-ajuda contextualizada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-visualização`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-regerar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-regerar`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-detalhes`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-detalhes`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-abrir visualização`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-excluir`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-excluir`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-executar`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-agendamentos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-agendamentos`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-power-search-button`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-requisitos para a exibição do processo`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-requisitos para a exibição do processo`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-requisitos para a exibição do processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-visualização`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-consultar job`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/execucao-relatorios`, `2455667123-consultar job`];
    cy.visit('http://system-A6/relatorios/reinf/execucao-relatorios');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2455667123-consultar job"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-agendar`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/execucao-relatorios`, `2455667123-agendar`];
    cy.visit('http://system-A6/relatorios/reinf/execucao-relatorios');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2455667123-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-executar relatório`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/execucao-relatorios`, `2455667123-executar relatório`];
    cy.visit('http://system-A6/relatorios/reinf/execucao-relatorios');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2455667123-executar relatório"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/execucao-relatorios->2455667123-powerselect-tipoExecucaoRelatorio-2455667123-checkbox-geracaoMatrizFilial and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/execucao-relatorios`, `2455667123-powerselect-tipoExecucaoRelatorio-2455667123-checkbox-geracaoMatrizFilial`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/execucao-relatorios"]`);
    cy.fillInputPowerSelect(`[data-cy="2455667123-powerselect-tipoExecucaoRelatorio"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2455667123-checkbox-geracaoMatrizFilial"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento->1743331536-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento`, `1743331536-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1743331536-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento->1743331536-cenários de crítica de enquadramento de documentos fiscais r2000`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento`, `1743331536-cenários de crítica de enquadramento de documentos fiscais r2000`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1743331536-cenários de crítica de enquadramento de documentos fiscais r2000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento->1743331536-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento`, `1743331536-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1743331536-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/enquadramento->1743331536-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento`, `1743331536-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento"]`);
    cy.fillInputPowerSearch(`[data-cy="1743331536-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento-r4000`, `717893041-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717893041-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-cenários de crítica de enquadramento de documentos fiscais r4000`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento-r4000`, `717893041-cenários de crítica de enquadramento de documentos fiscais r4000`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717893041-cenários de crítica de enquadramento de documentos fiscais r4000"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento-r4000`, `717893041-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717893041-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento-r4000`, `717893041-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/enquadramento-r4000"]`);
    cy.fillInputPowerSearch(`[data-cy="717893041-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-2010-r-2020`, `3991017516-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-2010-r-2020');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3991017516-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-requisitos para exibição de registros`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-2010-r-2020`, `3991017516-requisitos para exibição de registros`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-2010-r-2020');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3991017516-requisitos para exibição de registros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-2010-r-2020`, `3991017516-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-2010-r-2020');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3991017516-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-2010-r-2020`, `3991017516-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-2010-r-2020"]`);
    cy.fillInputPowerSearch(`[data-cy="3991017516-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-4000`, `2870315351-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2870315351-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-requisitos para exibição de registros`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-4000`, `2870315351-requisitos para exibição de registros`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2870315351-requisitos para exibição de registros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-4000`, `2870315351-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2870315351-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-4000`, `2870315351-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/pend-geracao-r-4000"]`);
    cy.fillInputPowerSearch(`[data-cy="2870315351-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pagamentos-beneficiario-r4000`, `2161322512-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/pagamentos-beneficiario-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2161322512-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-requisitos para exibição de registros`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pagamentos-beneficiario-r4000`, `2161322512-requisitos para exibição de registros`];
    cy.visit('http://system-A6/relatorios/reinf/pagamentos-beneficiario-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2161322512-requisitos para exibição de registros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pagamentos-beneficiario-r4000`, `2161322512-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/pagamentos-beneficiario-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2161322512-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pagamentos-beneficiario-r4000`, `2161322512-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/pagamentos-beneficiario-r4000"]`);
    cy.fillInputPowerSearch(`[data-cy="2161322512-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/detalhes-eventos-criticados`, `3895730676-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/detalhes-eventos-criticados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3895730676-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-ajuda`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/detalhes-eventos-criticados`, `3895730676-ajuda`];
    cy.visit('http://system-A6/relatorios/reinf/detalhes-eventos-criticados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3895730676-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/detalhes-eventos-criticados`, `3895730676-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/detalhes-eventos-criticados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3895730676-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/detalhes-eventos-criticados`, `3895730676-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/detalhes-eventos-criticados"]`);
    cy.fillInputPowerSearch(`[data-cy="3895730676-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9011`, `2785112454-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9011');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112454-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-ajuda`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9011`, `2785112454-ajuda`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9011');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112454-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9011`, `2785112454-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9011');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112454-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9011`, `2785112454-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9011"]`);
    cy.fillInputPowerSearch(`[data-cy="2785112454-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9015`, `2785112458-power-search-button`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9015');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112458-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-ajuda`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9015`, `2785112458-ajuda`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9015');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112458-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-visualização`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9015`, `2785112458-visualização`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9015');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112458-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-power-search-input and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9015`, `2785112458-power-search-input`];
    cy.clickIfExist(`[data-cy="reinf"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios"]`);
    cy.clickIfExist(`[data-cy="reinf/relatorios/conciliacao-r9015"]`);
    cy.fillInputPowerSearch(`[data-cy="2785112458-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-novo`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-power-search-button`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-selectoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-deleteoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-deleteoutlined`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`];
    cy.visit('http://system-A6/regras/apuracao-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3551826945-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-power-search-button`];
    cy.visit('http://system-A6/regras/apuracao-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3551826945-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`];
    cy.visit('http://system-A6/regras/apuracao-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-deleteoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-deleteoutlined`];
    cy.visit('http://system-A6/regras/apuracao-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3551826945-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/ger?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2728661096-gerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-power-search-button`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/ger?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2728661096-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`];
    cy.visit('http://system-A6/apuracoes/impostos/ger?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="856883396-gerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-mais operações`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-mais operações`];
    cy.visit('http://system-A6/apuracoes/impostos/ger?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="856883396-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-power-search-button`];
    cy.visit('http://system-A6/apuracoes/impostos/ger?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="856883396-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-regerar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-regerar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-detalhes`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-detalhes`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-abrir visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-excluir`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-excluir`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/guia-complementar->2610911131-exibir dados`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/guia-complementar`, `2610911131-exibir dados`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/guia-complementar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2610911131-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-executar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-agendamentos`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-power-search-button`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-visualização`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados->466686838-novo`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/pag-consolidados`, `466686838-novo`];
    cy.visit('http://system-A6/dirf/pag-consolidados?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&mesAno=~eq~04%2F2024%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="466686838-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados->466686838-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/pag-consolidados`, `466686838-power-search-button`];
    cy.visit('http://system-A6/dirf/pag-consolidados?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&mesAno=~eq~04%2F2024%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="466686838-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-importar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-importar`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-importar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-power-search-button`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-fileoutlined`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-fileoutlined`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-fileoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-deleteoutlined`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-deleteoutlined`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-executar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-executar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-regerar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-regerar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-detalhes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-detalhes`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-abrir visualização`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-excluir`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/retencoes-notas-fiscais->2664595303-exibir dados`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/retencoes-notas-fiscais`, `2664595303-exibir dados`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/retencoes-notas-fiscais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2664595303-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-regerar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-regerar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-detalhes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-detalhes`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-abrir visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-excluir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-regerar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-regerar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-detalhes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-detalhes`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-abrir visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-excluir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-regerar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-regerar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-detalhes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-detalhes`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-abrir visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-excluir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-requisitos de geração`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-requisitos de geração`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-requisitos de geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-regerar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-regerar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-detalhes`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-detalhes`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-abrir visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-excluir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-executar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-executar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-executar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-executar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-executar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-executar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-agendamentos`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-power-search-button`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-visualização`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-power-search-button`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1392456818-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-gerenciar labels`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-gerenciar labels`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1392456818-gerenciar labels"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar parâmetros`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-visualizar parâmetros`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1392456818-visualizar parâmetros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar/editar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-visualizar/editar`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1392456818-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ir para todas as obrigações`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ir para todas as obrigações`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajuda`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajuda`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-nova solicitação`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-nova solicitação`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-nova solicitação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-atualizar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-atualizar`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-gerar obrigação`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-gerar obrigação`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-gerar obrigação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-ajustar parâmetros da geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-visualizar resultado da geração`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-protocolo transmissão`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-protocolo transmissão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-excluir geração`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-excluir geração`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-excluir geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-power-search-button`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-visualização`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-abrir visualização`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualizar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-visualizar`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-novo`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-power-search-button`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-editar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-editar`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-excluir`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-novo`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao-estabelecimento`, `2827831370-novo`];
    cy.visit('http://system-A6/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2827831370-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao-estabelecimento`, `2827831370-power-search-button`];
    cy.visit('http://system-A6/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2827831370-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-excluir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao-estabelecimento`, `2827831370-excluir`];
    cy.visit('http://system-A6/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2827831370-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/rubricas->2746791129-eyeoutlined->2825312631-novo->2825312631-powerselect-sissCodigo and submit`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/rubricas`, `2746791129-eyeoutlined`, `2825312631-novo`, `2825312631-powerselect-sissCodigo`];
    cy.visit('http://system-A6/esocial-rubrica-vigencia/editar/66');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2825312631-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2825312631-powerselect-sissCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-2000->1291494902-visualizar relatório->2069757523-exibir dados`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-2000`, `1291494902-visualizar relatório`, `2069757523-exibir dados`];
    cy.visit('http://system-A6/painel/tributos/relatorios/tributos-serie-r2000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069757523-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/paineis->reinf/paineis/serie-r-4000->1291554484-visualizar relatório->2069817105-exibir dados`, () => {
    const actualId = [`root`, `reinf`, `reinf/paineis`, `reinf/paineis/serie-r-4000`, `1291554484-visualizar relatório`, `2069817105-exibir dados`];
    cy.visit('http://system-A6/painel/tributos/relatorios/tributos-serie-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069817105-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-novo`, `1165459227-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1165459227-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-novo`, `1165459227-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1165459227-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-powerselect-contribuintePfjCodigo-1165459227-powerselect-fontePagadoraPfjCodigo-1165459227-input-recImportNumero and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-novo`, `1165459227-powerselect-contribuintePfjCodigo-1165459227-powerselect-fontePagadoraPfjCodigo-1165459227-input-recImportNumero`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtRecebimento=~mth~1712479469824D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4096060046-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1165459227-powerselect-contribuintePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1165459227-powerselect-fontePagadoraPfjCodigo"] input`);
    cy.fillInput(`[data-cy="1165459227-input-recImportNumero"] textarea`, `Rubber`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-novo`, `3643260362-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3643260362-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-novo`, `3643260362-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3643260362-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-powerselect-contribuintePfjCodigo-3643260362-powerselect-beneficiarioPfjCodigo-3643260362-input-cnoCodigo-3643260362-input-pagImportNumero and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-novo`, `3643260362-powerselect-contribuintePfjCodigo-3643260362-powerselect-beneficiarioPfjCodigo-3643260362-input-cnoCodigo-3643260362-input-pagImportNumero`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtPagamento=~mth~1712479480041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2030434943-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3643260362-powerselect-contribuintePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3643260362-powerselect-beneficiarioPfjCodigo"] input`);
    cy.fillInput(`[data-cy="3643260362-input-cnoCodigo"] textarea`, `Checking Account`);
    cy.fillInput(`[data-cy="3643260362-input-pagImportNumero"] textarea`, `calculate`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="699364833-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="699364833-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712479490269D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPfjCodigo"] input`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioCpfCnpj"] textarea`, `Qualidade`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioRazaoSocial"] textarea`, `Qualidade`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndFisJur"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndNif"] input`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioCodigoNif"] textarea`, `Investidor`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioRelFontePag"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo->317393494-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-novo`, `317393494-salvar`];
    cy.visit('http://system-A6/regras/reinf-apuracao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="317393494-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo->317393494-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-novo`, `317393494-voltar`];
    cy.visit('http://system-A6/regras/reinf-apuracao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="317393494-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-novo->317393494-input-nome-317393494-powerselect-tipo-317393494-textarea-descricao and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-novo`, `317393494-input-nome-317393494-powerselect-tipo-317393494-textarea-descricao`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-novo"]`);
    cy.fillInput(`[data-cy="317393494-input-nome"] textarea`, `firewall`);
    cy.fillInputPowerSelect(`[data-cy="317393494-powerselect-tipo"] input`);
    cy.fillInput(`[data-cy="317393494-textarea-descricao"] input`, `white`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-remover item`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-remover item`];
    cy.visit('http://system-A6/regras/reinf-apuracao/editar/274');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="851469213-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-salvar`];
    cy.visit('http://system-A6/regras/reinf-apuracao/editar/274');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="851469213-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-voltar`];
    cy.visit('http://system-A6/regras/reinf-apuracao/editar/274');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="851469213-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-adicionar categoria`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-adicionar categoria`];
    cy.visit('http://system-A6/regras/reinf-apuracao/editar/274');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="851469213-adicionar categoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-input-nome-851469213-powerselect-indicadorDeduzaNcmAtividade-851469213-textarea-descricao and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-input-nome-851469213-powerselect-indicadorDeduzaNcmAtividade-851469213-textarea-descricao`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-eyeoutlined"]`);
    cy.fillInput(`[data-cy="851469213-input-nome"] textarea`, `grow`);
    cy.fillInputPowerSelect(`[data-cy="851469213-powerselect-indicadorDeduzaNcmAtividade"] input`);
    cy.fillInput(`[data-cy="851469213-textarea-descricao"] input`, `Uganda`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined->4012902003-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-copyoutlined`, `4012902003-voltar`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
    cy.clickIfExist(`[data-cy="4012902003-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined->4012902003-copiar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-copyoutlined`, `4012902003-copiar`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
    cy.clickIfExist(`[data-cy="4012902003-copiar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-copyoutlined->4012902003-input-nome and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-copyoutlined`, `4012902003-input-nome`];
    cy.visit('http://system-A6/regras/reinf-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4012902003-copyoutlined"]`);
    cy.fillInput(`[data-cy="4012902003-input-nome"] textarea`, `payment`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/comercializacao-rural`, `3461523942-novo`, `1127454403-salvar`];
    cy.visit('http://system-A6/apuracoes/comercializacao-rural/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1127454403-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/comercializacao-rural`, `3461523942-novo`, `1127454403-voltar`];
    cy.visit('http://system-A6/apuracoes/comercializacao-rural/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1127454403-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/comercializacao-rural->3461523942-novo->1127454403-powerselect-codigoEstabelecimento and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/comercializacao-rural`, `3461523942-novo`, `1127454403-powerselect-codigoEstabelecimento`];
    cy.visit('http://system-A6/apuracoes/comercializacao-rural');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3461523942-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1127454403-powerselect-codigoEstabelecimento"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-salvar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="770844300-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="770844300-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126801533-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="770844300-powerselect-estCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job->3385006537-eyeoutlined->3385006537-todos`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consulta-job`, `3385006537-eyeoutlined`, `3385006537-todos`];
    cy.visit('http://system-A6/processos/reinf/consulta-job');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3385006537-eyeoutlined"]`);
    cy.clickIfExist(`[data-cy="3385006537-todos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consulta-job->3385006537-eyeoutlined->3385006537-downloadoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consulta-job`, `3385006537-eyeoutlined`, `3385006537-downloadoutlined`];
    cy.visit('http://system-A6/processos/reinf/consulta-job');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3385006537-eyeoutlined"]`);
    cy.clickIfExist(`[data-cy="3385006537-downloadoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar->338915745-múltipla seleção`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-executar`, `338915745-múltipla seleção`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-executar"]`);
    cy.clickIfExist(`[data-cy="338915745-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-executar->338915745-agendar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-executar`, `338915745-agendar`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-executar"]`);
    cy.clickIfExist(`[data-cy="338915745-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-agendamentos->338915745-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-agendamentos`, `338915745-voltar`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53985074D%7C%7C53985074&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-visualização->338915745-item- and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-visualização`, `338915745-item-`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="338915745-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-detalhes->338915745-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-detalhes`, `338915745-dados disponíveis para impressão`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-detalhes"]`);
    cy.clickIfExist(`[data-cy="338915745-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-aumentar o zoom`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-abrir visualização`, `338915745-aumentar o zoom`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="338915745-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-diminuir o zoom`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-abrir visualização`, `338915745-diminuir o zoom`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="338915745-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-expandir`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-abrir visualização`, `338915745-expandir`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="338915745-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-download`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-abrir visualização`, `338915745-download`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="338915745-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar->4056642876-múltipla seleção`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-executar`, `4056642876-múltipla seleção`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-executar"]`);
    cy.clickIfExist(`[data-cy="4056642876-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar->4056642876-agendar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-executar`, `4056642876-agendar`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-executar"]`);
    cy.clickIfExist(`[data-cy="4056642876-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-agendamentos->4056642876-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-agendamentos`, `4056642876-voltar`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56458077D%7C%7C56458077&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-visualização->4056642876-item- and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-visualização`, `4056642876-item-`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4056642876-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento->1743331536-visualização->1743331536-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento`, `1743331536-visualização`, `1743331536-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1743331536-visualização"]`);
    cy.clickIfExist(`[data-cy="1743331536-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/enquadramento-r4000->717893041-visualização->717893041-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/enquadramento-r4000`, `717893041-visualização`, `717893041-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/enquadramento-documentos-fiscais-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717893041-visualização"]`);
    cy.clickIfExist(`[data-cy="717893041-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-2010-r-2020->3991017516-visualização->3991017516-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-2010-r-2020`, `3991017516-visualização`, `3991017516-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-2010-r-2020');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3991017516-visualização"]`);
    cy.clickIfExist(`[data-cy="3991017516-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pend-geracao-r-4000->2870315351-visualização->2870315351-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pend-geracao-r-4000`, `2870315351-visualização`, `2870315351-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/pend-geracao-r-4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2870315351-visualização"]`);
    cy.clickIfExist(`[data-cy="2870315351-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/pagamentos-beneficiario-r4000->2161322512-visualização->2161322512-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/pagamentos-beneficiario-r4000`, `2161322512-visualização`, `2161322512-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/pagamentos-beneficiario-r4000');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2161322512-visualização"]`);
    cy.clickIfExist(`[data-cy="2161322512-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/detalhes-eventos-criticados->3895730676-visualização->3895730676-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/detalhes-eventos-criticados`, `3895730676-visualização`, `3895730676-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/detalhes-eventos-criticados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3895730676-visualização"]`);
    cy.clickIfExist(`[data-cy="3895730676-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9011->2785112454-visualização->2785112454-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9011`, `2785112454-visualização`, `2785112454-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9011');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112454-visualização"]`);
    cy.clickIfExist(`[data-cy="2785112454-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/relatorios->reinf/relatorios/conciliacao-r9015->2785112458-visualização->2785112458-salvar configuração`, () => {
    const actualId = [`root`, `reinf`, `reinf/relatorios`, `reinf/relatorios/conciliacao-r9015`, `2785112458-visualização`, `2785112458-salvar configuração`];
    cy.visit('http://system-A6/relatorios/reinf/conciliacao-r9015');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2785112458-visualização"]`);
    cy.clickIfExist(`[data-cy="2785112458-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo->1414333350-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-novo`, `1414333350-salvar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1414333350-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo->1414333350-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-novo`, `1414333350-voltar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1414333350-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-novo->1414333350-input-rgliCodigo-1414333350-powerselect-indBaseLimp-1414333350-powerselect-indEntradaSaida-1414333350-powerselect-imposto-1414333350-textarea-obsRegra and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-novo`, `1414333350-input-rgliCodigo-1414333350-powerselect-indBaseLimp-1414333350-powerselect-indEntradaSaida-1414333350-powerselect-imposto-1414333350-textarea-obsRegra`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-novo"]`);
    cy.fillInput(`[data-cy="1414333350-input-rgliCodigo"] textarea`, `Innovative`);
    cy.fillInputPowerSelect(`[data-cy="1414333350-powerselect-indBaseLimp"] input`);
    cy.fillInputPowerSelect(`[data-cy="1414333350-powerselect-indEntradaSaida"] input`);
    cy.fillInputPowerSelect(`[data-cy="1414333350-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="1414333350-textarea-obsRegra"] input`, `Produtor`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-limp`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-limp`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-limp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-novo`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-power-search-button`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-eyeoutlined`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-deleteoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-deleteoutlined`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-selecionar critérios`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-selecionar critérios`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-selecionar critérios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-estabelecimentos da regra`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-estabelecimentos da regra`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-itens da regra`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-itens da regra`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-itens da regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-testar regra`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-testar regra`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-testar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-copiar regra`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-copiar regra`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-copiar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-remover item`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-remover item`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-salvar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-voltar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-powerselect-indEntradaSaida-2666930413-powerselect-imposto-2666930413-textarea-obsRegra and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-powerselect-indEntradaSaida-2666930413-powerselect-imposto-2666930413-textarea-obsRegra`];
    cy.visit('http://system-A6/regras/lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3509287715-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="2666930413-powerselect-indEntradaSaida"] input`);
    cy.fillInputPowerSelect(`[data-cy="2666930413-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="2666930413-textarea-obsRegra"] input`, `Ligao`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-pesquisar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`, `4001809800-pesquisar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4001809800-pesquisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`, `4001809800-salvar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4001809800-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`, `4001809800-voltar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4001809800-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-input-rgridCodigo-4001809800-input-nome-4001809800-powerselect-imposto-4001809800-input-monetary-vlMinRecolhimento-4001809800-powerselect-rgridCodigoSaldoCredor and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`, `4001809800-input-rgridCodigo-4001809800-input-nome-4001809800-powerselect-imposto-4001809800-input-monetary-vlMinRecolhimento-4001809800-powerselect-rgridCodigoSaldoCredor`];
    cy.visit('http://system-A6/regras/apuracao-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3551826945-novo"]`);
    cy.fillInput(`[data-cy="4001809800-input-rgridCodigo"] textarea`, `bandwidth`);
    cy.fillInput(`[data-cy="4001809800-input-nome"] textarea`, `Chair`);
    cy.fillInputPowerSelect(`[data-cy="4001809800-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="4001809800-input-monetary-vlMinRecolhimento"] textarea`, `2,53`);
    cy.fillInputPowerSelect(`[data-cy="4001809800-powerselect-rgridCodigoSaldoCredor"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-pesquisar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-pesquisar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-pesquisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-remover item`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-remover item`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-salvar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-copiar regra`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-copiar regra`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-copiar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-voltar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-novo`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-power-search-button`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-eyeoutlined`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-deleteoutlined`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-deleteoutlined`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-input-nome-2263500228-powerselect-imposto-2263500228-input-monetary-vlMinRecolhimento-2263500228-powerselect-rgridCodigoSaldoCredor and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-input-nome-2263500228-powerselect-imposto-2263500228-input-monetary-vlMinRecolhimento-2263500228-powerselect-rgridCodigoSaldoCredor`];
    cy.visit('http://system-A6/regras/apuracao-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3551826945-eyeoutlined"]`);
    cy.fillInput(`[data-cy="2263500228-input-nome"] textarea`, `programming`);
    cy.fillInputPowerSelect(`[data-cy="2263500228-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="2263500228-input-monetary-vlMinRecolhimento"] textarea`, `8,41`);
    cy.fillInputPowerSelect(`[data-cy="2263500228-powerselect-rgridCodigoSaldoCredor"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`, `1906165251-executar`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906165251-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`, `1906165251-agendamentos`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906165251-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`, `1906165251-power-search-button`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906165251-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`, `1906165251-visualização`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906165251-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-executar`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-agendamentos`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-agendamentos`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-power-search-button`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-visualização`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-visualização`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-regerar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-regerar`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-detalhes`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-detalhes`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-excluir`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-excluir`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar->447335411-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-executar`, `447335411-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-executar"]`);
    cy.clickIfExist(`[data-cy="447335411-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar->447335411-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-executar`, `447335411-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-executar"]`);
    cy.clickIfExist(`[data-cy="447335411-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-agendamentos->447335411-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-agendamentos`, `447335411-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46703635D%7C%7C46703635&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-visualização->447335411-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-visualização`, `447335411-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="447335411-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar->717809746-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-executar`, `717809746-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-executar"]`);
    cy.clickIfExist(`[data-cy="717809746-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar->717809746-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-executar`, `717809746-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-executar"]`);
    cy.clickIfExist(`[data-cy="717809746-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-agendamentos->717809746-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-agendamentos`, `717809746-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972667D%7C%7C26972667&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-visualização->717809746-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-visualização`, `717809746-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="717809746-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar->133362368-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-executar`, `133362368-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-executar"]`);
    cy.clickIfExist(`[data-cy="133362368-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar->133362368-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-executar`, `133362368-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-executar"]`);
    cy.clickIfExist(`[data-cy="133362368-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-agendamentos->133362368-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-agendamentos`, `133362368-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970534D%7C%7C54970534&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-visualização->133362368-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-visualização`, `133362368-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="133362368-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar->235713453-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-executar`, `235713453-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-executar"]`);
    cy.clickIfExist(`[data-cy="235713453-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar->235713453-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-executar`, `235713453-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-executar"]`);
    cy.clickIfExist(`[data-cy="235713453-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-agendamentos->235713453-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-agendamentos`, `235713453-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972654D%7C%7C26972654&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-visualização->235713453-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-visualização`, `235713453-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="235713453-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-detalhes->235713453-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-detalhes`, `235713453-dados disponíveis para impressão`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-detalhes"]`);
    cy.clickIfExist(`[data-cy="235713453-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-aumentar o zoom`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-abrir visualização`, `235713453-aumentar o zoom`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="235713453-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-diminuir o zoom`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-abrir visualização`, `235713453-diminuir o zoom`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="235713453-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-expandir`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-abrir visualização`, `235713453-expandir`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="235713453-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-download`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-abrir visualização`, `235713453-download`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="235713453-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar->2590522946-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-executar`, `2590522946-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-executar"]`);
    cy.clickIfExist(`[data-cy="2590522946-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar->2590522946-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-executar`, `2590522946-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-executar"]`);
    cy.clickIfExist(`[data-cy="2590522946-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-agendamentos->2590522946-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-agendamentos`, `2590522946-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210440D%7C%7C210440&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-visualização->2590522946-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-visualização`, `2590522946-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2590522946-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar->2403317122-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-executar`, `2403317122-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-executar"]`);
    cy.clickIfExist(`[data-cy="2403317122-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar->2403317122-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-executar`, `2403317122-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-executar"]`);
    cy.clickIfExist(`[data-cy="2403317122-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-agendamentos->2403317122-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-agendamentos`, `2403317122-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972694D%7C%7C26972694&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-visualização->2403317122-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-visualização`, `2403317122-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2403317122-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar->3689771228-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-executar`, `3689771228-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-executar"]`);
    cy.clickIfExist(`[data-cy="3689771228-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar->3689771228-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-executar`, `3689771228-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-executar"]`);
    cy.clickIfExist(`[data-cy="3689771228-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-agendamentos->3689771228-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-agendamentos`, `3689771228-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~238454D%7C%7C238454&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-visualização->3689771228-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-visualização`, `3689771228-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3689771228-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar->1130815789-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-executar`, `1130815789-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-executar"]`);
    cy.clickIfExist(`[data-cy="1130815789-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar->1130815789-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-executar`, `1130815789-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-executar"]`);
    cy.clickIfExist(`[data-cy="1130815789-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-agendamentos->1130815789-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-agendamentos`, `1130815789-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26972681D%7C%7C26972681&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-visualização->1130815789-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-visualização`, `1130815789-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1130815789-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar->3148305101-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-executar`, `3148305101-múltipla seleção`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-executar"]`);
    cy.clickIfExist(`[data-cy="3148305101-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar->3148305101-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-executar`, `3148305101-agendar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-executar"]`);
    cy.clickIfExist(`[data-cy="3148305101-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-agendamentos->3148305101-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-agendamentos`, `3148305101-voltar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970542D%7C%7C54970542&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-visualização->3148305101-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-visualização`, `3148305101-item-`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3148305101-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados->466686838-novo->3607668531-salvar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/pag-consolidados`, `466686838-novo`, `3607668531-salvar`];
    cy.visit('http://system-A6/dirf/pag-consolidados/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3607668531-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados->466686838-novo->3607668531-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/pag-consolidados`, `466686838-novo`, `3607668531-voltar`];
    cy.visit('http://system-A6/dirf/pag-consolidados/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3607668531-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/pag-consolidados->466686838-novo->3607668531-powerselect-estCodigo-3607668531-powerselect-indOrigemBeneficiario-3607668531-input-cpfCgcBeneficiario-3607668531-powerselect-tipoPessoaBeneficiario-3607668531-input-nomeBeneficiario-3607668531-powerselect-indScp-3607668531-powerselect-codigoRetencao-3607668531-input-monetary-vlRendimentoTributavel-3607668531-input-monetary-vlImpostoRetido-3607668531-input-monetary-vlRendimentoNaoTributavel-3607668531-input-monetary-rraVlRendIsentos-3607668531-input-monetary-rraVlDespesasAcaoJudicial-3607668531-input-monetary-vlInssRet-3607668531-input-monetary-vlPensaoAlimenticia-3607668531-input-monetary-vlDependentes-3607668531-input-monetary-vlDeducoes-3607668531-powerselect-paisCodigo-3607668531-powerselect-relPagadorBenef-3607668531-powerselect-tipoRendimento-3607668531-powerselect-formaTributacao-3607668531-input-nif-3607668531-powerselect-rraIdentPagante-3607668531-input-rraNumProcesso-3607668531-input-rraNatureza-3607668531-input-number-rraQtdMeses-3607668531-input-monetary-vlScp-3607668531-input-monetary-vlIsento-3607668531-input-monetary-vlImune and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/pag-consolidados`, `466686838-novo`, `3607668531-powerselect-estCodigo-3607668531-powerselect-indOrigemBeneficiario-3607668531-input-cpfCgcBeneficiario-3607668531-powerselect-tipoPessoaBeneficiario-3607668531-input-nomeBeneficiario-3607668531-powerselect-indScp-3607668531-powerselect-codigoRetencao-3607668531-input-monetary-vlRendimentoTributavel-3607668531-input-monetary-vlImpostoRetido-3607668531-input-monetary-vlRendimentoNaoTributavel-3607668531-input-monetary-rraVlRendIsentos-3607668531-input-monetary-rraVlDespesasAcaoJudicial-3607668531-input-monetary-vlInssRet-3607668531-input-monetary-vlPensaoAlimenticia-3607668531-input-monetary-vlDependentes-3607668531-input-monetary-vlDeducoes-3607668531-powerselect-paisCodigo-3607668531-powerselect-relPagadorBenef-3607668531-powerselect-tipoRendimento-3607668531-powerselect-formaTributacao-3607668531-input-nif-3607668531-powerselect-rraIdentPagante-3607668531-input-rraNumProcesso-3607668531-input-rraNatureza-3607668531-input-number-rraQtdMeses-3607668531-input-monetary-vlScp-3607668531-input-monetary-vlIsento-3607668531-input-monetary-vlImune`];
    cy.visit('http://system-A6/dirf/pag-consolidados?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&mesAno=~eq~04%2F2024%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="466686838-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-indOrigemBeneficiario"] input`);
    cy.fillInput(`[data-cy="3607668531-input-cpfCgcBeneficiario"] textarea`, `197.885.782-92`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-tipoPessoaBeneficiario"] input`);
    cy.fillInput(`[data-cy="3607668531-input-nomeBeneficiario"] textarea`, `Snior`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-indScp"] input`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-codigoRetencao"] input`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlRendimentoTributavel"] textarea`, `3,07`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlImpostoRetido"] textarea`, `7,51`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlRendimentoNaoTributavel"] textarea`, `9,63`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-rraVlRendIsentos"] textarea`, `5,56`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-rraVlDespesasAcaoJudicial"] textarea`, `4,03`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlInssRet"] textarea`, `7,24`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlPensaoAlimenticia"] textarea`, `6,2`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlDependentes"] textarea`, `2,15`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlDeducoes"] textarea`, `6,83`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-paisCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-relPagadorBenef"] input`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-tipoRendimento"] input`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-formaTributacao"] input`);
    cy.fillInput(`[data-cy="3607668531-input-nif"] textarea`, `Gorgeous`);
    cy.fillInputPowerSelect(`[data-cy="3607668531-powerselect-rraIdentPagante"] input`);
    cy.fillInput(`[data-cy="3607668531-input-rraNumProcesso"] textarea`, `bestofbreed`);
    cy.fillInput(`[data-cy="3607668531-input-rraNatureza"] textarea`, `payment`);
    cy.fillInput(`[data-cy="3607668531-input-number-rraQtdMeses"] textarea`, `5`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlScp"] textarea`, `7,05`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlIsento"] textarea`, `8,07`);
    cy.fillInput(`[data-cy="3607668531-input-monetary-vlImune"] textarea`, `8,9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-importar->307422372-powerselect-ano and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-importar`, `307422372-powerselect-ano`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-importar"]`);
    cy.fillInputPowerSelect(`[data-cy="307422372-powerselect-ano"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-fileoutlined->307422372-saveoutlined`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-fileoutlined`, `307422372-saveoutlined`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-fileoutlined"]`);
    cy.clickIfExist(`[data-cy="307422372-saveoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/dirf->tributo-terceiro/dirf/importacao->307422372-fileoutlined->307422372-printeroutlined`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/dirf`, `tributo-terceiro/dirf/importacao`, `307422372-fileoutlined`, `307422372-printeroutlined`];
    cy.visit('http://system-A6/dirf/importacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="307422372-fileoutlined"]`);
    cy.clickIfExist(`[data-cy="307422372-printeroutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-executar`, `1745767366-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-executar"]`);
    cy.clickIfExist(`[data-cy="1745767366-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-executar`, `1745767366-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-executar"]`);
    cy.clickIfExist(`[data-cy="1745767366-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-input-P_ANO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-executar`, `1745767366-input-P_ANO`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-executar"]`);
    cy.fillInput(`[data-cy="1745767366-input-P_ANO"] textarea`, `Wallis and Futuna`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-agendamentos->1745767366-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-agendamentos`, `1745767366-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~247835D%7C%7C247835&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-visualização->1745767366-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-visualização`, `1745767366-item-`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1745767366-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-executar`, `1673335588-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-executar"]`);
    cy.clickIfExist(`[data-cy="1673335588-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-executar`, `1673335588-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-executar"]`);
    cy.clickIfExist(`[data-cy="1673335588-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-input-P_ANO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-executar`, `1673335588-input-P_ANO`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-executar"]`);
    cy.fillInput(`[data-cy="1673335588-input-P_ANO"] textarea`, `Personal Loan Account`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-agendamentos->1673335588-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-agendamentos`, `1673335588-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47399253D%7C%7C47399253&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-visualização->1673335588-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-visualização`, `1673335588-item-`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1673335588-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-detalhes->1673335588-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-detalhes`, `1673335588-dados disponíveis para impressão`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-detalhes"]`);
    cy.clickIfExist(`[data-cy="1673335588-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-abrir visualização`, `1673335588-aumentar o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1673335588-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-abrir visualização`, `1673335588-diminuir o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1673335588-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-abrir visualização`, `1673335588-expandir`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1673335588-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-abrir visualização`, `1673335588-download`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1673335588-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-executar`, `3329339398-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-executar"]`);
    cy.clickIfExist(`[data-cy="3329339398-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-executar`, `3329339398-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-executar"]`);
    cy.clickIfExist(`[data-cy="3329339398-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-input-PFJ and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-executar`, `3329339398-input-PFJ`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-executar"]`);
    cy.fillInput(`[data-cy="3329339398-input-PFJ"] textarea`, `Kuwaiti Dinar`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-agendamentos->3329339398-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-agendamentos`, `3329339398-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970468D%7C%7C54970468&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-visualização->3329339398-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-visualização`, `3329339398-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3329339398-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-detalhes->3329339398-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-detalhes`, `3329339398-dados disponíveis para impressão`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-detalhes"]`);
    cy.clickIfExist(`[data-cy="3329339398-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-abrir visualização`, `3329339398-aumentar o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3329339398-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-abrir visualização`, `3329339398-diminuir o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3329339398-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-abrir visualização`, `3329339398-expandir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3329339398-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-abrir visualização`, `3329339398-download`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3329339398-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar->1917228366-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-executar`, `1917228366-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-executar"]`);
    cy.clickIfExist(`[data-cy="1917228366-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar->1917228366-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-executar`, `1917228366-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-executar"]`);
    cy.clickIfExist(`[data-cy="1917228366-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-agendamentos->1917228366-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-agendamentos`, `1917228366-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245578D%7C%7C245578&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-visualização->1917228366-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-visualização`, `1917228366-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1917228366-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-executar->3840643021-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-executar`, `3840643021-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-executar"]`);
    cy.clickIfExist(`[data-cy="3840643021-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-executar->3840643021-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-executar`, `3840643021-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-executar"]`);
    cy.clickIfExist(`[data-cy="3840643021-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-executar->3840643021-input-P_BENEFICIARIO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-executar`, `3840643021-input-P_BENEFICIARIO`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-executar"]`);
    cy.fillInput(`[data-cy="3840643021-input-P_BENEFICIARIO"] textarea`, `Refined`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-agendamentos->3840643021-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-agendamentos`, `3840643021-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970496D%7C%7C54970496&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-visualização->3840643021-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-visualização`, `3840643021-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3840643021-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-detalhes->3840643021-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-detalhes`, `3840643021-dados disponíveis para impressão`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-detalhes"]`);
    cy.clickIfExist(`[data-cy="3840643021-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-abrir visualização->3840643021-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-abrir visualização`, `3840643021-aumentar o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3840643021-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-abrir visualização->3840643021-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-abrir visualização`, `3840643021-diminuir o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3840643021-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-abrir visualização->3840643021-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-abrir visualização`, `3840643021-expandir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3840643021-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-abrir visualização->3840643021-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-abrir visualização`, `3840643021-download`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3840643021-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-executar`, `977623228-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-executar"]`);
    cy.clickIfExist(`[data-cy="977623228-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-executar`, `977623228-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-executar"]`);
    cy.clickIfExist(`[data-cy="977623228-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-input-P_CPF_CGC and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-executar`, `977623228-input-P_CPF_CGC`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-executar"]`);
    cy.fillInput(`[data-cy="977623228-input-P_CPF_CGC"] textarea`, `array`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-agendamentos->977623228-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-agendamentos`, `977623228-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970502D%7C%7C54970502&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-visualização->977623228-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-visualização`, `977623228-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="977623228-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar->3465203885-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-executar`, `3465203885-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-executar"]`);
    cy.clickIfExist(`[data-cy="3465203885-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar->3465203885-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-executar`, `3465203885-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-executar"]`);
    cy.clickIfExist(`[data-cy="3465203885-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-agendamentos->3465203885-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-agendamentos`, `3465203885-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245758D%7C%7C245758&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-visualização->3465203885-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-visualização`, `3465203885-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3465203885-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-executar`, `1614719659-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-executar"]`);
    cy.clickIfExist(`[data-cy="1614719659-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-executar`, `1614719659-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-executar"]`);
    cy.clickIfExist(`[data-cy="1614719659-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-input-PFJ and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-executar`, `1614719659-input-PFJ`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-executar"]`);
    cy.fillInput(`[data-cy="1614719659-input-PFJ"] textarea`, `Concrete`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-agendamentos->1614719659-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-agendamentos`, `1614719659-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970482D%7C%7C54970482&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-visualização->1614719659-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-visualização`, `1614719659-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1614719659-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-detalhes->1614719659-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-detalhes`, `1614719659-dados disponíveis para impressão`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-detalhes"]`);
    cy.clickIfExist(`[data-cy="1614719659-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-abrir visualização`, `1614719659-aumentar o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1614719659-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-abrir visualização`, `1614719659-diminuir o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1614719659-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-abrir visualização`, `1614719659-expandir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1614719659-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-abrir visualização`, `1614719659-download`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1614719659-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-executar`, `3047460406-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-executar"]`);
    cy.clickIfExist(`[data-cy="3047460406-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-executar`, `3047460406-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-executar"]`);
    cy.clickIfExist(`[data-cy="3047460406-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-input-PFJ_PRESTADORA and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-executar`, `3047460406-input-PFJ_PRESTADORA`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-executar"]`);
    cy.fillInput(`[data-cy="3047460406-input-PFJ_PRESTADORA"] textarea`, `Tuna`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-agendamentos->3047460406-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-agendamentos`, `3047460406-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970475D%7C%7C54970475&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-visualização->3047460406-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-visualização`, `3047460406-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3047460406-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-detalhes->3047460406-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-detalhes`, `3047460406-dados disponíveis para impressão`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-detalhes"]`);
    cy.clickIfExist(`[data-cy="3047460406-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-abrir visualização`, `3047460406-aumentar o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3047460406-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-abrir visualização`, `3047460406-diminuir o zoom`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3047460406-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-abrir visualização`, `3047460406-expandir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3047460406-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-abrir visualização`, `3047460406-download`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3047460406-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-executar`, `3040111907-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-executar"]`);
    cy.clickIfExist(`[data-cy="3040111907-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-executar`, `3040111907-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-executar"]`);
    cy.clickIfExist(`[data-cy="3040111907-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-input-PRESTADOR and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-executar`, `3040111907-input-PRESTADOR`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-executar"]`);
    cy.fillInput(`[data-cy="3040111907-input-PRESTADOR"] textarea`, `olive`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-agendamentos->3040111907-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-agendamentos`, `3040111907-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~6821028D%7C%7C6821028&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-visualização->3040111907-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-visualização`, `3040111907-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3040111907-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-executar`, `2966158800-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-executar"]`);
    cy.clickIfExist(`[data-cy="2966158800-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-executar`, `2966158800-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-executar"]`);
    cy.clickIfExist(`[data-cy="2966158800-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-input-CPF_CNPJ and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-executar`, `2966158800-input-CPF_CNPJ`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-executar"]`);
    cy.fillInput(`[data-cy="2966158800-input-CPF_CNPJ"] textarea`, `repurpose`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-agendamentos->2966158800-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-agendamentos`, `2966158800-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12904528D%7C%7C12904528&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-visualização->2966158800-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-visualização`, `2966158800-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2966158800-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-executar->682447114-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-executar`, `682447114-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-executar"]`);
    cy.clickIfExist(`[data-cy="682447114-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-executar->682447114-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-executar`, `682447114-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-executar"]`);
    cy.clickIfExist(`[data-cy="682447114-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-executar->682447114-input-pANO-682447114-input-pCPF_CNPJ and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-executar`, `682447114-input-pANO-682447114-input-pCPF_CNPJ`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-executar"]`);
    cy.fillInput(`[data-cy="682447114-input-pANO"] textarea`, `black`);
    cy.fillInput(`[data-cy="682447114-input-pCPF_CNPJ"] textarea`, `Sahara`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-agendamentos->682447114-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-agendamentos`, `682447114-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970509D%7C%7C54970509&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-visualização->682447114-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-visualização`, `682447114-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="682447114-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-executar`, `2798312950-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-executar"]`);
    cy.clickIfExist(`[data-cy="2798312950-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-executar`, `2798312950-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-executar"]`);
    cy.clickIfExist(`[data-cy="2798312950-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-input-pANO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-executar`, `2798312950-input-pANO`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-executar"]`);
    cy.fillInput(`[data-cy="2798312950-input-pANO"] textarea`, `haptic`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-agendamentos->2798312950-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-agendamentos`, `2798312950-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970522D%7C%7C54970522&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-visualização->2798312950-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-visualização`, `2798312950-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2798312950-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-executar`, `533561082-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-executar"]`);
    cy.clickIfExist(`[data-cy="533561082-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-executar`, `533561082-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-executar"]`);
    cy.clickIfExist(`[data-cy="533561082-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-input-P_ANO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-executar`, `533561082-input-P_ANO`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-executar"]`);
    cy.fillInput(`[data-cy="533561082-input-P_ANO"] textarea`, `homogeneous`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-agendamentos->533561082-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-agendamentos`, `533561082-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~248256D%7C%7C248256&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-visualização->533561082-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-visualização`, `533561082-item-`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="533561082-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-executar`, `716117882-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-executar"]`);
    cy.clickIfExist(`[data-cy="716117882-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-executar`, `716117882-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-executar"]`);
    cy.clickIfExist(`[data-cy="716117882-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-input-RESPONSAVEL and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-executar`, `716117882-input-RESPONSAVEL`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-executar"]`);
    cy.fillInput(`[data-cy="716117882-input-RESPONSAVEL"] textarea`, `bypass`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-agendamentos->716117882-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-agendamentos`, `716117882-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890954D%7C%7C47890954&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-visualização->716117882-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-visualização`, `716117882-item-`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="716117882-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-executar`, `1023520286-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-executar"]`);
    cy.clickIfExist(`[data-cy="1023520286-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-executar`, `1023520286-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-executar"]`);
    cy.clickIfExist(`[data-cy="1023520286-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-input-RESPONSAVEL and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-executar`, `1023520286-input-RESPONSAVEL`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-executar"]`);
    cy.fillInput(`[data-cy="1023520286-input-RESPONSAVEL"] textarea`, `Travessa`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-agendamentos->1023520286-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-agendamentos`, `1023520286-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890961D%7C%7C47890961&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-visualização->1023520286-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-visualização`, `1023520286-item-`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1023520286-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-executar`, `2587924048-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-executar"]`);
    cy.clickIfExist(`[data-cy="2587924048-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-executar`, `2587924048-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-executar"]`);
    cy.clickIfExist(`[data-cy="2587924048-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-input-CPF_CNPJ_BENEF-2587924048-input-DATA_ANO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-executar`, `2587924048-input-CPF_CNPJ_BENEF-2587924048-input-DATA_ANO`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-executar"]`);
    cy.fillInput(`[data-cy="2587924048-input-CPF_CNPJ_BENEF"] textarea`, `program`);
    cy.fillInput(`[data-cy="2587924048-input-DATA_ANO"] textarea`, `global`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-agendamentos->2587924048-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-agendamentos`, `2587924048-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890975D%7C%7C47890975&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-visualização->2587924048-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-visualização`, `2587924048-item-`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2587924048-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-executar`, `1339819272-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-executar"]`);
    cy.clickIfExist(`[data-cy="1339819272-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-executar`, `1339819272-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-executar"]`);
    cy.clickIfExist(`[data-cy="1339819272-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-input-CPF_CNPJ_BENEF-1339819272-input-DATA_ANO and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-executar`, `1339819272-input-CPF_CNPJ_BENEF-1339819272-input-DATA_ANO`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-executar"]`);
    cy.fillInput(`[data-cy="1339819272-input-CPF_CNPJ_BENEF"] textarea`, `protocol`);
    cy.fillInput(`[data-cy="1339819272-input-DATA_ANO"] textarea`, `ivory`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-agendamentos->1339819272-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-agendamentos`, `1339819272-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47890968D%7C%7C47890968&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-visualização->1339819272-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-visualização`, `1339819272-item-`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1339819272-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-múltipla seleção`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-executar`, `1234427231-múltipla seleção`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-executar"]`);
    cy.clickIfExist(`[data-cy="1234427231-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-agendar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-executar`, `1234427231-agendar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-executar"]`);
    cy.clickIfExist(`[data-cy="1234427231-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-input-CPF_CNPJ_BENEF-1234427231-input-DATA_ANO-1234427231-input-RESPONSAVEL and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-executar`, `1234427231-input-CPF_CNPJ_BENEF-1234427231-input-DATA_ANO-1234427231-input-RESPONSAVEL`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-executar"]`);
    cy.fillInput(`[data-cy="1234427231-input-CPF_CNPJ_BENEF"] textarea`, `FTP`);
    cy.fillInput(`[data-cy="1234427231-input-DATA_ANO"] textarea`, `Facilitador`);
    cy.fillInput(`[data-cy="1234427231-input-RESPONSAVEL"] textarea`, `transmit`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-agendamentos->1234427231-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-agendamentos`, `1234427231-voltar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54970489D%7C%7C54970489&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-visualização->1234427231-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-visualização`, `1234427231-item-`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1234427231-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-gerenciar labels->1392456818-fechar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-gerenciar labels`, `1392456818-fechar`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1392456818-gerenciar labels"]`);
    cy.clickIfExist(`[data-cy="1392456818-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar/editar->3216657679-salvar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-visualizar/editar`, `3216657679-salvar`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3216657679-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao->1392456818-visualizar/editar->3216657679-voltar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao`, `1392456818-visualizar/editar`, `3216657679-voltar`];
    cy.visit('http://system-A6/obrigacoes/configuracao-obrigacao-fiscal/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3216657679-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ir para todas as obrigações->611534337-voltar às obrigações do módulo`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ir para todas as obrigações`, `611534337-voltar às obrigações do módulo`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="611534337-voltar às obrigações do módulo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-nova solicitação->611534337-salvar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-nova solicitação`, `611534337-salvar`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="611534337-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-nova solicitação->611534337-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-nova solicitação`, `611534337-cancelar`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="611534337-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-power-search-button`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-visualização`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-abrir visualização`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualizar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-visualizar`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-rightoutlined`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`, `1874405030-rightoutlined`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1874405030-rightoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-habilitar edição`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`, `1874405030-habilitar edição`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1874405030-habilitar edição"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-abrir janela de edição`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`, `1874405030-abrir janela de edição`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1874405030-abrir janela de edição"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-visualizar resultado da geração`, `611534337-aumentar o zoom`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="611534337-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-visualizar resultado da geração`, `611534337-diminuir o zoom`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="611534337-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-visualizar resultado da geração`, `611534337-expandir`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="611534337-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-visualizar resultado da geração`, `611534337-download`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="611534337-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão->3661377306-editar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-protocolo transmissão`, `3661377306-editar`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914/protocolos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3661377306-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualização->52374570-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-visualização`, `52374570-item-`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="52374570-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-abrir visualização`, `52374570-aumentar o zoom`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-abrir visualização`, `52374570-diminuir o zoom`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-abrir visualização`, `52374570-expandir`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-abrir visualização`, `52374570-download`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-visualizar->52374570-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-visualizar`, `52374570-dados disponíveis para impressão`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
    cy.clickIfExist(`[data-cy="52374570-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo->3124702626-criar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-novo`, `3124702626-criar`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-novo"]`);
    cy.clickIfExist(`[data-cy="3124702626-criar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo->3124702626-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-novo`, `3124702626-cancelar`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-novo"]`);
    cy.clickIfExist(`[data-cy="3124702626-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-novo->3124702626-input-number-ano and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-novo`, `3124702626-input-number-ano`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-novo"]`);
    cy.fillInput(`[data-cy="3124702626-input-number-ano"] textarea`, `6`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-editar->3124702626-remover item`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-editar`, `3124702626-remover item`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-editar"]`);
    cy.clickIfExist(`[data-cy="3124702626-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/periodicidade->3124702626-editar->3124702626-salvar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/periodicidade`, `3124702626-editar`, `3124702626-salvar`];
    cy.visit('http://system-A6/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124702626-editar"]`);
    cy.clickIfExist(`[data-cy="3124702626-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/configuracao-estabelecimento->2827831370-novo->2827831370-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/configuracao-estabelecimento`, `2827831370-novo`, `2827831370-cancelar`];
    cy.visit('http://system-A6/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2827831370-novo"]`);
    cy.clickIfExist(`[data-cy="2827831370-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-voltar->4096060046-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-novo`, `1165459227-voltar`, `4096060046-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtRecebimento=~mth~1712482478112D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4096060046-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/recebimentos-diversos->4096060046-novo->1165459227-voltar->4096060046-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/recebimentos-diversos`, `4096060046-novo`, `1165459227-voltar`, `4096060046-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/recebimentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtRecebimento=~mth~1712482478112D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4096060046-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-voltar->2030434943-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-novo`, `3643260362-voltar`, `2030434943-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtPagamento=~mth~1712482504429D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2030434943-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-diversos->2030434943-novo->3643260362-voltar->2030434943-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-diversos`, `2030434943-novo`, `3643260362-voltar`, `2030434943-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-diversos?contribuintePfjCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtPagamento=~mth~1712482504429D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2030434943-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-remover item`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-remover item`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9093');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9093');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9093');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9093');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9093');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo-441105128-input-beneficiarioCpfCnpj-441105128-input-beneficiarioRazaoSocial-441105128-powerselect-beneficiarioIndFisJur-441105128-powerselect-beneficiarioPais-441105128-powerselect-beneficiarioIndNif-441105128-input-beneficiarioCodigoNif-441105128-powerselect-beneficiarioRelFontePag and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo-441105128-input-beneficiarioCpfCnpj-441105128-input-beneficiarioRazaoSocial-441105128-powerselect-beneficiarioIndFisJur-441105128-powerselect-beneficiarioPais-441105128-powerselect-beneficiarioIndNif-441105128-input-beneficiarioCodigoNif-441105128-powerselect-beneficiarioRelFontePag`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="699364833-salvar"]`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioPfjCodigo"] input`);
    cy.fillInput(`[data-cy="441105128-input-beneficiarioCpfCnpj"] textarea`, `Viela`);
    cy.fillInput(`[data-cy="441105128-input-beneficiarioRazaoSocial"] textarea`, `Standalone`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioIndFisJur"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioIndNif"] input`);
    cy.fillInput(`[data-cy="441105128-input-beneficiarioCodigoNif"] textarea`, `Uzbekistan Sum`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioRelFontePag"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-voltar`, `1716736008-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712482531324D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-voltar`, `1716736008-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712482531324D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-visualizar/editar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-voltar`, `1716736008-visualizar/editar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712482531324D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-voltar->1716736008-excluir`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-voltar`, `1716736008-excluir`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712482531324D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-remover item`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-remover item`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9094');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9094');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9094');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9094');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9094');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-powerselect-estCodigo-441105128-powerselect-beneficiarioPfjCodigo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712479490269D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPfjCodigo"] input`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioCpfCnpj"] textarea`, `Qualidade`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioRazaoSocial"] textarea`, `Qualidade`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndFisJur"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioIndNif"] input`);
    cy.fillInput(`[data-cy="699364833-input-beneficiarioCodigoNif"] textarea`, `Investidor`);
    cy.fillInputPowerSelect(`[data-cy="699364833-powerselect-beneficiarioRelFontePag"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="441105128-powerselect-beneficiarioPfjCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-adicionar categoria->851469213-cancelar`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-adicionar categoria`, `851469213-cancelar`];
    cy.visit('http://system-A6/regras/reinf-apuracao/editar/274');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="851469213-adicionar categoria"]`);
    cy.clickIfExist(`[data-cy="851469213-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/regras->reinf/regras/apuracao-cprb->4012902003-eyeoutlined->851469213-adicionar categoria->851469213-powerselect-destino and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/regras`, `reinf/regras/apuracao-cprb`, `4012902003-eyeoutlined`, `851469213-adicionar categoria`, `851469213-powerselect-destino`];
    cy.visit('http://system-A6/regras/reinf-apuracao/editar/274');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="851469213-adicionar categoria"]`);
    cy.fillInputPowerSelect(`[data-cy="851469213-powerselect-destino"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-power-search-button`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/apuracao-r4000->338915745-abrir visualização->338915745-expandir->338915745-diminuir`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/apuracao-r4000`, `338915745-abrir visualização`, `338915745-expandir`, `338915745-diminuir`];
    cy.visit('http://system-A6/processos/reinf/apuracao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="338915745-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="338915745-expandir"]`);
    cy.clickIfExist(`[data-cy="338915745-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/processos->reinf/processos/consolidacao-desconsolidacao-r4000->4056642876-executar->4056642876-múltipla seleção->4056642876-cancelar`, () => {
    const actualId = [`root`, `reinf`, `reinf/processos`, `reinf/processos/consolidacao-desconsolidacao-r4000`, `4056642876-executar`, `4056642876-múltipla seleção`, `4056642876-cancelar`];
    cy.visit('http://system-A6/processos/reinf/consolidacao-desconsolidacao-r4000?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056642876-executar"]`);
    cy.clickIfExist(`[data-cy="4056642876-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4056642876-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo->3183381421-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-novo`, `3183381421-salvar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3183381421-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo->3183381421-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-novo`, `3183381421-voltar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3183381421-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-novo->3183381421-checkbox-indConverteValor-3183381421-powerselect-ufCodigo-3183381421-powerselect-vlImposto-3183381421-powerselect-dtFatoGerador-3183381421-powerselect-estabApuracao-3183381421-powerselect-codReceita-3183381421-powerselect-timCodigo and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-novo`, `3183381421-checkbox-indConverteValor-3183381421-powerselect-ufCodigo-3183381421-powerselect-vlImposto-3183381421-powerselect-dtFatoGerador-3183381421-powerselect-estabApuracao-3183381421-powerselect-codReceita-3183381421-powerselect-timCodigo`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-novo"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3183381421-checkbox-indConverteValor"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-ufCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-vlImposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-dtFatoGerador"] input`);
    cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-estabApuracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-codReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="3183381421-powerselect-timCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-remover item`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-eyeoutlined`, `1020274267-remover item`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens/editar/2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1020274267-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-eyeoutlined`, `1020274267-salvar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens/editar/2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1020274267-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-eyeoutlined`, `1020274267-voltar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens/editar/2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1020274267-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-selectoutlined->2512964284-eyeoutlined->1020274267-checkbox-indConverteValor-1020274267-powerselect-ufCodigo-1020274267-powerselect-vlImposto-1020274267-powerselect-dtFatoGerador-1020274267-powerselect-estabApuracao-1020274267-powerselect-codReceita-1020274267-powerselect-timCodigo and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-selectoutlined`, `2512964284-eyeoutlined`, `1020274267-checkbox-indConverteValor-1020274267-powerselect-ufCodigo-1020274267-powerselect-vlImposto-1020274267-powerselect-dtFatoGerador-1020274267-powerselect-estabApuracao-1020274267-powerselect-codReceita-1020274267-powerselect-timCodigo`];
    cy.visit('http://system-A6/regras/lancamento-impostos/aaa_001/PARC/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2512964284-eyeoutlined"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1020274267-checkbox-indConverteValor"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-ufCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-vlImposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-dtFatoGerador"] input`);
    cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-estabApuracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-codReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="1020274267-powerselect-timCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-estabelecimentos da regra->2666930413-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-estabelecimentos da regra`, `2666930413-power-search-button`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="2666930413-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-estabelecimentos da regra->2666930413-carregar mais`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-estabelecimentos da regra`, `2666930413-carregar mais`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="2666930413-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-estabelecimentos da regra->2666930413-fechar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-estabelecimentos da regra`, `2666930413-fechar`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="2666930413-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-estabelecimentos da regra->2666930413-power-search-input and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-estabelecimentos da regra`, `2666930413-power-search-input`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-estabelecimentos da regra"]`);
    cy.fillInputPowerSearch(`[data-cy="2666930413-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/lancamento-impostos->3509287715-eyeoutlined->2666930413-copiar regra->2666930413-input-copiaFrom-2666930413-input-copiaTo and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/lancamento-impostos`, `3509287715-eyeoutlined`, `2666930413-copiar regra`, `2666930413-input-copiaFrom-2666930413-input-copiaTo`];
    cy.visit('http://system-A6/regras/lancamento-impostos/editar/aaa_001');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2666930413-copiar regra"]`);
    cy.fillInput(`[data-cy="2666930413-input-copiaFrom"] textarea`, `Cuban Peso Peso Convertible`);
    cy.fillInput(`[data-cy="2666930413-input-copiaTo"] textarea`, `Fresh`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-pesquisar->4001809800-power-search-button`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`, `4001809800-pesquisar`, `4001809800-power-search-button`];
    cy.visit('http://system-A6/regras/apuracao-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4001809800-pesquisar"]`);
    cy.clickIfExist(`[data-cy="4001809800-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-novo->4001809800-pesquisar->4001809800-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-novo`, `4001809800-pesquisar`, `4001809800-cancelar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4001809800-pesquisar"]`);
    cy.clickIfExist(`[data-cy="4001809800-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-pesquisar->2263500228-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-pesquisar`, `2263500228-cancelar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2263500228-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-copiar regra->2263500228-input-copiaFrom-2263500228-input-copiaTo and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-copiar regra`, `2263500228-input-copiaFrom-2263500228-input-copiaTo`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-copiar regra"]`);
    cy.fillInput(`[data-cy="2263500228-input-copiaFrom"] textarea`, `pricing structure`);
    cy.fillInput(`[data-cy="2263500228-input-copiaTo"] textarea`, `Distrito`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo->2148999588-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-novo`, `2148999588-salvar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/ACE_COFFAT/item/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2148999588-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo->2148999588-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-novo`, `2148999588-voltar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/ACE_COFFAT/item/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2148999588-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-novo->2148999588-powerselect-timCodigo-2148999588-powerselect-periodicidade-2148999588-input-number-diaInicio-2148999588-input-number-diaFim-2148999588-checkbox-desmembraGuia and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-novo`, `2148999588-powerselect-timCodigo-2148999588-powerselect-periodicidade-2148999588-input-number-diaInicio-2148999588-input-number-diaFim-2148999588-checkbox-desmembraGuia`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2148999588-powerselect-timCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2148999588-powerselect-periodicidade"] input`);
    cy.fillInput(`[data-cy="2148999588-input-number-diaInicio"] textarea`, `2`);
    cy.fillInput(`[data-cy="2148999588-input-number-diaFim"] textarea`, `4`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2148999588-checkbox-desmembraGuia"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-remover item`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-eyeoutlined`, `3697625195-remover item`];
    cy.visit('http://system-A6/regras/apuracao-impostos/ACE_COFFAT/item/editar/2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3697625195-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-salvar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-eyeoutlined`, `3697625195-salvar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/ACE_COFFAT/item/editar/2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3697625195-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-eyeoutlined`, `3697625195-voltar`];
    cy.visit('http://system-A6/regras/apuracao-impostos/ACE_COFFAT/item/editar/2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3697625195-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/regras->lancamento-e-guia-de-impostos/regras/apuracao-impostos->3551826945-eyeoutlined->2263500228-eyeoutlined->3697625195-powerselect-timCodigo-3697625195-powerselect-periodicidade-3697625195-input-number-diaInicio-3697625195-input-number-diaFim-3697625195-checkbox-desmembraGuia and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/regras`, `lancamento-e-guia-de-impostos/regras/apuracao-impostos`, `3551826945-eyeoutlined`, `2263500228-eyeoutlined`, `3697625195-powerselect-timCodigo-3697625195-powerselect-periodicidade-3697625195-input-number-diaInicio-3697625195-input-number-diaFim-3697625195-checkbox-desmembraGuia`];
    cy.visit('http://system-A6/regras/apuracao-impostos/editar/ACE_COFFAT/item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2263500228-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="3697625195-powerselect-timCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3697625195-powerselect-periodicidade"] input`);
    cy.fillInput(`[data-cy="3697625195-input-number-diaInicio"] textarea`, `10`);
    cy.fillInput(`[data-cy="3697625195-input-number-diaFim"] textarea`, `9`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3697625195-checkbox-desmembraGuia"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-agendamentos->1906165251-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`, `1906165251-agendamentos`, `1906165251-voltar`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47670891D%7C%7C47670891&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906165251-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos->2728661096-gerar->1906165251-visualização->1906165251-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/ger-man-lancamento-impostos`, `2728661096-gerar`, `1906165251-visualização`, `1906165251-item-`];
    cy.visit('http://system-A6/apuracoes/ger-man-limp/processos/lancamento-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906165251-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1906165251-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar->4218034749-múltipla seleção`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-executar`, `4218034749-múltipla seleção`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-executar"]`);
    cy.clickIfExist(`[data-cy="4218034749-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar->4218034749-agendar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-executar`, `4218034749-agendar`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-executar"]`);
    cy.clickIfExist(`[data-cy="4218034749-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-agendamentos->4218034749-voltar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-agendamentos`, `4218034749-voltar`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47781678D%7C%7C47781678&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-visualização->4218034749-item- and submit`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-visualização`, `4218034749-item-`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4218034749-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-detalhes->4218034749-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-detalhes`, `4218034749-não há dados disponíveis para impressão`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-detalhes"]`);
    cy.clickIfExist(`[data-cy="4218034749-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp->447335411-executar->447335411-múltipla seleção->447335411-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp`, `447335411-executar`, `447335411-múltipla seleção`, `447335411-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-rural-cide-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="447335411-executar"]`);
    cy.clickIfExist(`[data-cy="447335411-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="447335411-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp->717809746-executar->717809746-múltipla seleção->717809746-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp`, `717809746-executar`, `717809746-múltipla seleção`, `717809746-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="717809746-executar"]`);
    cy.clickIfExist(`[data-cy="717809746-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="717809746-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp->133362368-executar->133362368-múltipla seleção->133362368-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp`, `133362368-executar`, `133362368-múltipla seleção`, `133362368-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-composicao-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133362368-executar"]`);
    cy.clickIfExist(`[data-cy="133362368-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="133362368-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-executar->235713453-múltipla seleção->235713453-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-executar`, `235713453-múltipla seleção`, `235713453-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-executar"]`);
    cy.clickIfExist(`[data-cy="235713453-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="235713453-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids->235713453-abrir visualização->235713453-expandir->235713453-diminuir`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids`, `235713453-abrir visualização`, `235713453-expandir`, `235713453-diminuir`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/dofs-servico-grids?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="235713453-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="235713453-expandir"]`);
    cy.clickIfExist(`[data-cy="235713453-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos->2590522946-executar->2590522946-múltipla seleção->2590522946-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos`, `2590522946-executar`, `2590522946-múltipla seleção`, `2590522946-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-impostos-recolhidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2590522946-executar"]`);
    cy.clickIfExist(`[data-cy="2590522946-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2590522946-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid->2403317122-executar->2403317122-múltipla seleção->2403317122-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid`, `2403317122-executar`, `2403317122-múltipla seleção`, `2403317122-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-retidos-sem-grid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403317122-executar"]`);
    cy.clickIfExist(`[data-cy="2403317122-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2403317122-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos->3689771228-executar->3689771228-múltipla seleção->3689771228-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos`, `3689771228-executar`, `3689771228-múltipla seleção`, `3689771228-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/limps-apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3689771228-executar"]`);
    cy.clickIfExist(`[data-cy="3689771228-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3689771228-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico->1130815789-executar->1130815789-múltipla seleção->1130815789-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico`, `1130815789-executar`, `1130815789-múltipla seleção`, `1130815789-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/grids-dofs-servico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130815789-executar"]`);
    cy.clickIfExist(`[data-cy="1130815789-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1130815789-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/relatorios->lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos->3148305101-executar->3148305101-múltipla seleção->3148305101-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/relatorios`, `lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos`, `3148305101-executar`, `3148305101-múltipla seleção`, `3148305101-cancelar`];
    cy.visit('http://system-A6/lancamento-e-guia-de-impostos/relatorios/inss-sobre-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3148305101-executar"]`);
    cy.clickIfExist(`[data-cy="3148305101-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3148305101-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/identificacao-retroativa->1745767366-executar->1745767366-múltipla seleção->1745767366-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/identificacao-retroativa`, `1745767366-executar`, `1745767366-múltipla seleção`, `1745767366-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/identificacao-retroativa?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745767366-executar"]`);
    cy.clickIfExist(`[data-cy="1745767366-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1745767366-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-executar->1673335588-múltipla seleção->1673335588-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-executar`, `1673335588-múltipla seleção`, `1673335588-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-executar"]`);
    cy.clickIfExist(`[data-cy="1673335588-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1673335588-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/processos->tributo-terceiro/processos/consolid-desconsolid->1673335588-abrir visualização->1673335588-expandir->1673335588-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/processos`, `tributo-terceiro/processos/consolid-desconsolid`, `1673335588-abrir visualização`, `1673335588-expandir`, `1673335588-diminuir`];
    cy.visit('http://system-A6/tributo-terceiro/processos/consolid-desconsolid?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673335588-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1673335588-expandir"]`);
    cy.clickIfExist(`[data-cy="1673335588-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-executar->3329339398-múltipla seleção->3329339398-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-executar`, `3329339398-múltipla seleção`, `3329339398-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-executar"]`);
    cy.clickIfExist(`[data-cy="3329339398-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3329339398-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/servico-por-periodo->3329339398-abrir visualização->3329339398-expandir->3329339398-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/servico-por-periodo`, `3329339398-abrir visualização`, `3329339398-expandir`, `3329339398-diminuir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/servico-por-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3329339398-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3329339398-expandir"]`);
    cy.clickIfExist(`[data-cy="3329339398-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dof-rpa-sem-limp->1917228366-executar->1917228366-múltipla seleção->1917228366-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dof-rpa-sem-limp`, `1917228366-executar`, `1917228366-múltipla seleção`, `1917228366-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dof-rpa-sem-limp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1917228366-executar"]`);
    cy.clickIfExist(`[data-cy="1917228366-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1917228366-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-executar->3840643021-múltipla seleção->3840643021-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-executar`, `3840643021-múltipla seleção`, `3840643021-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-executar"]`);
    cy.clickIfExist(`[data-cy="3840643021-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3840643021-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-analiticos->3840643021-abrir visualização->3840643021-expandir->3840643021-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-analiticos`, `3840643021-abrir visualização`, `3840643021-expandir`, `3840643021-diminuir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-analiticos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840643021-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3840643021-expandir"]`);
    cy.clickIfExist(`[data-cy="3840643021-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pgtos-consolidados->977623228-executar->977623228-múltipla seleção->977623228-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pgtos-consolidados`, `977623228-executar`, `977623228-múltipla seleção`, `977623228-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pgtos-consolidados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="977623228-executar"]`);
    cy.clickIfExist(`[data-cy="977623228-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="977623228-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/critica-pagamentos->3465203885-executar->3465203885-múltipla seleção->3465203885-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/critica-pagamentos`, `3465203885-executar`, `3465203885-múltipla seleção`, `3465203885-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/critica-pagamentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3465203885-executar"]`);
    cy.clickIfExist(`[data-cy="3465203885-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3465203885-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-executar->1614719659-múltipla seleção->1614719659-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-executar`, `1614719659-múltipla seleção`, `1614719659-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-executar"]`);
    cy.clickIfExist(`[data-cy="1614719659-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1614719659-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/inss-recolhimento->1614719659-abrir visualização->1614719659-expandir->1614719659-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/inss-recolhimento`, `1614719659-abrir visualização`, `1614719659-expandir`, `1614719659-diminuir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/inss-recolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1614719659-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1614719659-expandir"]`);
    cy.clickIfExist(`[data-cy="1614719659-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-executar->3047460406-múltipla seleção->3047460406-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-executar`, `3047460406-múltipla seleção`, `3047460406-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-executar"]`);
    cy.clickIfExist(`[data-cy="3047460406-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3047460406-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/irrf-sem-servicos->3047460406-abrir visualização->3047460406-expandir->3047460406-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/irrf-sem-servicos`, `3047460406-abrir visualização`, `3047460406-expandir`, `3047460406-diminuir`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/irrf-sem-servicos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3047460406-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3047460406-expandir"]`);
    cy.clickIfExist(`[data-cy="3047460406-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-retidos->3040111907-executar->3040111907-múltipla seleção->3040111907-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-retidos`, `3040111907-executar`, `3040111907-múltipla seleção`, `3040111907-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-retidos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3040111907-executar"]`);
    cy.clickIfExist(`[data-cy="3040111907-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3040111907-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/pcc-residuo-parcelas->2966158800-executar->2966158800-múltipla seleção->2966158800-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/pcc-residuo-parcelas`, `2966158800-executar`, `2966158800-múltipla seleção`, `2966158800-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/pcc-residuo-parcelas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2966158800-executar"]`);
    cy.clickIfExist(`[data-cy="2966158800-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2966158800-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-por-beneficiario->682447114-executar->682447114-múltipla seleção->682447114-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-por-beneficiario`, `682447114-executar`, `682447114-múltipla seleção`, `682447114-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-por-beneficiario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="682447114-executar"]`);
    cy.clickIfExist(`[data-cy="682447114-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="682447114-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/resumo-cod-receita->2798312950-executar->2798312950-múltipla seleção->2798312950-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/resumo-cod-receita`, `2798312950-executar`, `2798312950-múltipla seleção`, `2798312950-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/resumo-cod-receita?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2798312950-executar"]`);
    cy.clickIfExist(`[data-cy="2798312950-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2798312950-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/relatorios->tributo-terceiro/relatorios/dirf-x-dctf->533561082-executar->533561082-múltipla seleção->533561082-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/relatorios`, `tributo-terceiro/relatorios/dirf-x-dctf`, `533561082-executar`, `533561082-múltipla seleção`, `533561082-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/relatorios/dirf-x-dctf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="533561082-executar"]`);
    cy.clickIfExist(`[data-cy="533561082-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="533561082-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento->716117882-executar->716117882-múltipla seleção->716117882-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento`, `716117882-executar`, `716117882-múltipla seleção`, `716117882-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="716117882-executar"]`);
    cy.clickIfExist(`[data-cy="716117882-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="716117882-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/informe-rendimento-html->1023520286-executar->1023520286-múltipla seleção->1023520286-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/informe-rendimento-html`, `1023520286-executar`, `1023520286-múltipla seleção`, `1023520286-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/informe-rendimento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1023520286-executar"]`);
    cy.clickIfExist(`[data-cy="1023520286-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1023520286-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento->2587924048-executar->2587924048-múltipla seleção->2587924048-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento`, `2587924048-executar`, `2587924048-múltipla seleção`, `2587924048-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2587924048-executar"]`);
    cy.clickIfExist(`[data-cy="2587924048-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2587924048-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/etiquetas-enderecamento-html->1339819272-executar->1339819272-múltipla seleção->1339819272-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/etiquetas-enderecamento-html`, `1339819272-executar`, `1339819272-múltipla seleção`, `1339819272-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/etiquetas-enderecamento-html?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1339819272-executar"]`);
    cy.clickIfExist(`[data-cy="1339819272-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1339819272-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/comprovante->tributo-terceiro/comprovante/exportacao-txt->1234427231-executar->1234427231-múltipla seleção->1234427231-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/comprovante`, `tributo-terceiro/comprovante/exportacao-txt`, `1234427231-executar`, `1234427231-múltipla seleção`, `1234427231-cancelar`];
    cy.visit('http://system-A6/tributo-terceiro/comprovante/exportacao-txt?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1234427231-executar"]`);
    cy.clickIfExist(`[data-cy="1234427231-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1234427231-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualização->52374570-item- and submit`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-visualização`, `52374570-item-`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="52374570-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-aumentar o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-abrir visualização`, `52374570-aumentar o zoom`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-diminuir o zoom`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-abrir visualização`, `52374570-diminuir o zoom`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-expandir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-abrir visualização`, `52374570-expandir`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-download`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-abrir visualização`, `52374570-download`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-visualizar->52374570-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-visualizar`, `52374570-dados disponíveis para impressão`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-visualizar"]`);
    cy.clickIfExist(`[data-cy="52374570-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-rightoutlined->1874405030-downoutlined`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`, `1874405030-rightoutlined`, `1874405030-downoutlined`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1874405030-rightoutlined"]`);
    cy.clickIfExist(`[data-cy="1874405030-downoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-habilitar edição->1874405030-confirmar  edição`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`, `1874405030-habilitar edição`, `1874405030-confirmar  edição`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1874405030-habilitar edição"]`);
    cy.clickIfExist(`[data-cy="1874405030-confirmar  edição"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-ajustar parâmetros da geração->1874405030-abrir janela de edição->1874405030-power-search-button`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-ajustar parâmetros da geração`, `1874405030-abrir janela de edição`, `1874405030-power-search-button`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1874405030-abrir janela de edição"]`);
    cy.clickIfExist(`[data-cy="1874405030-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-visualizar resultado da geração->611534337-expandir->611534337-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-visualizar resultado da geração`, `611534337-expandir`, `611534337-diminuir`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="611534337-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="611534337-expandir"]`);
    cy.clickIfExist(`[data-cy="611534337-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão->3661377306-editar->3661377306-salvar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-protocolo transmissão`, `3661377306-editar`, `3661377306-salvar`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914/protocolos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3661377306-editar"]`);
    cy.clickIfExist(`[data-cy="3661377306-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-protocolo transmissão->3661377306-editar->3661377306-cancelar`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-protocolo transmissão`, `3661377306-editar`, `3661377306-cancelar`];
    cy.visit('http://system-A6/obrigacoes/solicitacoes-resultados/361369/obrigacao-gerada/424914/protocolos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3661377306-editar"]`);
    cy.clickIfExist(`[data-cy="3661377306-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/obrigacoes-executadas->52374570-abrir visualização->52374570-expandir->52374570-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/obrigacoes-executadas`, `52374570-abrir visualização`, `52374570-expandir`, `52374570-diminuir`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-expandir"]`);
    cy.clickIfExist(`[data-cy="52374570-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-voltar->1716736008-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-voltar`, `1716736008-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484893538D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-voltar->1716736008-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-voltar`, `1716736008-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484893538D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-voltar->1716736008-visualizar/editar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-voltar`, `1716736008-visualizar/editar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484893538D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-voltar->1716736008-excluir`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-voltar`, `1716736008-excluir`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484893538D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-novo->1831468927-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-novo`, `1831468927-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9093/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-novo->1831468927-edição nat. rend.`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-novo`, `1831468927-edição nat. rend.`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9093/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-edição nat. rend."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-novo->1831468927-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-novo`, `1831468927-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9093/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-novo->1831468927-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-novo`, `1831468927-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9093/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-salvar->441105128-novo->1831468927-powerselect-naturezaRendimentoCodigo-1831468927-powerselect-indFiscalNaoFiscal-1831468927-powerselect-dofId-1831468927-input-eventoAdicionalId-1831468927-powerselect-indAutoRetencao-1831468927-input-descricao-1831468927-input-monetary-vlOperacao-1831468927-powerselect-ind13Salario-1831468927-powerselect-indRra-1831468927-powerselect-indRendDecisaoJudicial-1831468927-powerselect-tipoEntidadeAssociada-1831468927-input-cnpjEntidadeAssociada-1831468927-input-monetary-percentualParticipacao-1831468927-powerselect-formaTribRendimento and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-salvar`, `441105128-novo`, `1831468927-powerselect-naturezaRendimentoCodigo-1831468927-powerselect-indFiscalNaoFiscal-1831468927-powerselect-dofId-1831468927-input-eventoAdicionalId-1831468927-powerselect-indAutoRetencao-1831468927-input-descricao-1831468927-input-monetary-vlOperacao-1831468927-powerselect-ind13Salario-1831468927-powerselect-indRra-1831468927-powerselect-indRendDecisaoJudicial-1831468927-powerselect-tipoEntidadeAssociada-1831468927-input-cnpjEntidadeAssociada-1831468927-input-monetary-percentualParticipacao-1831468927-powerselect-formaTribRendimento`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9093');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-naturezaRendimentoCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indFiscalNaoFiscal"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-dofId"] input`);
    cy.fillInput(`[data-cy="1831468927-input-eventoAdicionalId"] textarea`, `Usabilidade`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indAutoRetencao"] input`);
    cy.fillInput(`[data-cy="1831468927-input-descricao"] textarea`, `Tools`);
    cy.fillInput(`[data-cy="1831468927-input-monetary-vlOperacao"] textarea`, `6`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-ind13Salario"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indRra"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indRendDecisaoJudicial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-tipoEntidadeAssociada"] input`);
    cy.fillInput(`[data-cy="1831468927-input-cnpjEntidadeAssociada"] textarea`, `77.010.805/1330-27`);
    cy.fillInput(`[data-cy="1831468927-input-monetary-percentualParticipacao"] textarea`, `1`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-formaTribRendimento"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-voltar->1716736008-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-voltar`, `1716736008-novo`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484969041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-voltar->1716736008-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-voltar`, `1716736008-power-search-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484969041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-voltar->1716736008-visualizar/editar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-voltar`, `1716736008-visualizar/editar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484969041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-voltar->1716736008-excluir`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-voltar`, `1716736008-excluir`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&periodo=~mth~1712484969041D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1716736008-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-novo->1831468927-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-novo`, `1831468927-button`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9094/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-novo->1831468927-edição nat. rend.`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-novo`, `1831468927-edição nat. rend.`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9094/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-edição nat. rend."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-novo->1831468927-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-novo`, `1831468927-salvar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9094/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-novo->1831468927-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-novo`, `1831468927-voltar`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/9094/retencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1831468927-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/recebimentos-pagamentos->reinf/recebimentos-pagamentos/pagamentos-r4000->1716736008-novo->699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag->441105128-novo->1831468927-powerselect-naturezaRendimentoCodigo-1831468927-powerselect-indFiscalNaoFiscal-1831468927-powerselect-dofId-1831468927-input-eventoAdicionalId-1831468927-powerselect-indAutoRetencao-1831468927-input-descricao-1831468927-input-monetary-vlOperacao-1831468927-powerselect-ind13Salario-1831468927-powerselect-indRra-1831468927-powerselect-indRendDecisaoJudicial-1831468927-powerselect-tipoEntidadeAssociada-1831468927-input-cnpjEntidadeAssociada-1831468927-input-monetary-percentualParticipacao-1831468927-powerselect-formaTribRendimento and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/recebimentos-pagamentos`, `reinf/recebimentos-pagamentos/pagamentos-r4000`, `1716736008-novo`, `699364833-powerselect-estCodigo-699364833-powerselect-beneficiarioPfjCodigo-699364833-input-beneficiarioCpfCnpj-699364833-input-beneficiarioRazaoSocial-699364833-powerselect-beneficiarioIndFisJur-699364833-powerselect-beneficiarioPais-699364833-powerselect-beneficiarioIndNif-699364833-input-beneficiarioCodigoNif-699364833-powerselect-beneficiarioRelFontePag`, `441105128-novo`, `1831468927-powerselect-naturezaRendimentoCodigo-1831468927-powerselect-indFiscalNaoFiscal-1831468927-powerselect-dofId-1831468927-input-eventoAdicionalId-1831468927-powerselect-indAutoRetencao-1831468927-input-descricao-1831468927-input-monetary-vlOperacao-1831468927-powerselect-ind13Salario-1831468927-powerselect-indRra-1831468927-powerselect-indRendDecisaoJudicial-1831468927-powerselect-tipoEntidadeAssociada-1831468927-input-cnpjEntidadeAssociada-1831468927-input-monetary-percentualParticipacao-1831468927-powerselect-formaTribRendimento`];
    cy.visit('http://system-A6/recebimentos-pagamentos/pagamentos-r4000/editar/9094');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="441105128-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-naturezaRendimentoCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indFiscalNaoFiscal"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-dofId"] input`);
    cy.fillInput(`[data-cy="1831468927-input-eventoAdicionalId"] textarea`, `So Paulo`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indAutoRetencao"] input`);
    cy.fillInput(`[data-cy="1831468927-input-descricao"] textarea`, `Ergonomic Frozen Soap`);
    cy.fillInput(`[data-cy="1831468927-input-monetary-vlOperacao"] textarea`, `4`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-ind13Salario"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indRra"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-indRendDecisaoJudicial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-tipoEntidadeAssociada"] input`);
    cy.fillInput(`[data-cy="1831468927-input-cnpjEntidadeAssociada"] textarea`, `32.305.151/8230-43`);
    cy.fillInput(`[data-cy="1831468927-input-monetary-percentualParticipacao"] textarea`, `6`);
    cy.fillInputPowerSelect(`[data-cy="1831468927-powerselect-formaTribRendimento"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2415241101-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-novo`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2415241101-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-power-search-button`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-power-search-button`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2415241101-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-eyeoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-eyeoutlined`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2415241101-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-deleteoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-deleteoutlined`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2415241101-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-salvar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="123050857-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="123050857-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="123050857-powerselect-produtorPfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="123050857-powerselect-indTributacaoFolha"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element lancamento-e-guia-de-impostos->lancamento-e-guia-de-impostos/apuracoes->lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos->856883396-gerar->4218034749-executar->4218034749-múltipla seleção->4218034749-cancelar`, () => {
    const actualId = [`root`, `lancamento-e-guia-de-impostos`, `lancamento-e-guia-de-impostos/apuracoes`, `lancamento-e-guia-de-impostos/apuracoes/apuracao-de-impostos`, `856883396-gerar`, `4218034749-executar`, `4218034749-múltipla seleção`, `4218034749-cancelar`];
    cy.visit('http://system-A6/apuracoes/impostos/processos/apuracao-impostos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4218034749-executar"]`);
    cy.clickIfExist(`[data-cy="4218034749-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4218034749-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tributo-terceiro->tributo-terceiro/obrigacoes->tributo-terceiro/obrigacoes/solicitacoes->611534337-agendamentos->52374570-abrir visualização->52374570-expandir->52374570-diminuir`, () => {
    const actualId = [`root`, `tributo-terceiro`, `tributo-terceiro/obrigacoes`, `tributo-terceiro/obrigacoes/solicitacoes`, `611534337-agendamentos`, `52374570-abrir visualização`, `52374570-expandir`, `52374570-diminuir`];
    cy.visit('http://system-A6/obrigacoes/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="52374570-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="52374570-expandir"]`);
    cy.clickIfExist(`[data-cy="52374570-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-novo->3217114759-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-novo`, `3217114759-salvar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3217114759-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-novo->3217114759-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-novo`, `3217114759-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3217114759-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-voltar->2415241101-novo->3217114759-powerselect-estCodigo and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-voltar`, `2415241101-novo`, `3217114759-powerselect-estCodigo`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2415241101-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3217114759-powerselect-estCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-salvar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/editar/4189');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3918073255-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/editar/4189');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3918073255-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-novo`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-novo`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/editar/4189');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3918073255-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-powerselect-indTributacaoFolha and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-powerselect-indTributacaoFolha`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="123050857-powerselect-produtorPfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="123050857-powerselect-indTributacaoFolha"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="3918073255-powerselect-indTributacaoFolha"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-voltar->2661856238-eyeoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-voltar`, `2661856238-eyeoutlined`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-voltar->2661856238-deleteoutlined`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-voltar`, `2661856238-deleteoutlined`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/editar/ACS001-MT/471');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2661856238-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-novo->1584196771-salvar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-novo`, `1584196771-salvar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/4189/tipo/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1584196771-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-novo->1584196771-voltar`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-novo`, `1584196771-voltar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/4189/tipo/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1584196771-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Filling values reinf->reinf/apuracoes->reinf/apuracoes/aquisicao-producao-rural->1126801533-novo->770844300-powerselect-estCodigo->2661856238-novo->123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha->3918073255-novo->1584196771-powerselect-tipoAquisicaoCodigo-1584196771-input-monetary-vlBruto-1584196771-input-monetary-aliquotaFunrural-1584196771-input-monetary-vlContribuicaoFunrural-1584196771-input-monetary-aliquotaGilrat-1584196771-input-monetary-vlContribuicaoGilrat-1584196771-input-monetary-aliquotaSenar-1584196771-input-monetary-vlContribuicaoSenar and submit`, () => {
    const actualId = [`root`, `reinf`, `reinf/apuracoes`, `reinf/apuracoes/aquisicao-producao-rural`, `1126801533-novo`, `770844300-powerselect-estCodigo`, `2661856238-novo`, `123050857-powerselect-produtorPfjCodigo-123050857-powerselect-indTributacaoFolha`, `3918073255-novo`, `1584196771-powerselect-tipoAquisicaoCodigo-1584196771-input-monetary-vlBruto-1584196771-input-monetary-aliquotaFunrural-1584196771-input-monetary-vlContribuicaoFunrural-1584196771-input-monetary-aliquotaGilrat-1584196771-input-monetary-vlContribuicaoGilrat-1584196771-input-monetary-aliquotaSenar-1584196771-input-monetary-vlContribuicaoSenar`];
    cy.visit('http://system-A6/apuracoes/aquisicao-producao-rural/empresa/ACS001-MT/periodo/202404/estabelecimento/ACS001-MT/471/produtor/editar/4189');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3918073255-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1584196771-powerselect-tipoAquisicaoCodigo"] input`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-vlBruto"] textarea`, `5`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-aliquotaFunrural"] textarea`, `9`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-vlContribuicaoFunrural"] textarea`, `6`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-aliquotaGilrat"] textarea`, `6`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-vlContribuicaoGilrat"] textarea`, `9`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-aliquotaSenar"] textarea`, `1`);
    cy.fillInput(`[data-cy="1584196771-input-monetary-vlContribuicaoSenar"] textarea`, `3`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
