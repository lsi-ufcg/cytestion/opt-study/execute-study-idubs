describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-agendamentos->3085244455-voltar`, () => {
    cy.visit(
      'http://system-A8/batimento-tributario/batimento-tributario-processo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47772887D%7C%7C47772887&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3085244455-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-agendamentos->363060266-voltar`, () => {
    cy.visit(
      'http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47785422D%7C%7C47785422&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="363060266-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-agendamentos->3796808049-voltar`, () => {
    cy.visit(
      'http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54958095D%7C%7C54958095&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3796808049-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-agendamentos->690119939-voltar`, () => {
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47353087D%7C%7C47353087&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="690119939-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/limpeza-cache-calculo->4292064370-agendamentos->4292064370-voltar`, () => {
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46526222D%7C%7C46526222&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4292064370-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/teste-regra-arvore->33832422-agendamentos->33832422-voltar`, () => {
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~237054D%7C%7C237054&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="33832422-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-agendamentos->4038971926-voltar`, () => {
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54958088D%7C%7C54958088&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4038971926-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-agendamentos->3014098245-voltar`, () => {
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210752D%7C%7C210752&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3014098245-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-agendamentos->993067788-voltar`, () => {
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~237469D%7C%7C237469&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="993067788-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-agendamentos->1591012837-voltar`, () => {
    cy.visit(
      'http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12454872D%7C%7C12454872&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1591012837-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-agendamentos->2275226589-voltar`, () => {
    cy.visit(
      'http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54958093D%7C%7C54958093&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2275226589-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-agendamentos->357684938-voltar`, () => {
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31990495D%7C%7C31990495&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="357684938-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-novo->103581300-powerselect-ordem-103581300-input-comentario-103581300-textarea-codigoVigente and submit`, () => {
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="902838972-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="103581300-powerselect-ordem"] input`);
    cy.fillInput(`[data-cy="103581300-input-comentario"] textarea`, `overriding`);
    cy.fillInput(`[data-cy="103581300-textarea-codigoVigente"] input`, `FTP`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
