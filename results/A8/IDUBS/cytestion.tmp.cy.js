describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo`, () => {
    const actualId = [`root`, `inteligencia-calculo`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares`, () => {
    const actualId = [`root`, `cadastros-complementares`];
    cy.clickIfExist(`[data-cy="cadastros-complementares"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario`, () => {
    const actualId = [`root`, `batimento-tributario`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element simulacoes-cenarios`, () => {
    const actualId = [`root`, `simulacoes-cenarios`];
    cy.clickIfExist(`[data-cy="simulacoes-cenarios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracao`, () => {
    const actualId = [`root`, `integracao`];
    cy.clickIfExist(`[data-cy="integracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
    const actualId = [`root`, `processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio`, () => {
    const actualId = [`root`, `relatorios-apoio`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
    const actualId = [`root`, `processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
    const actualId = [`root`, `download`];
    cy.clickIfExist(`[data-cy="download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/condicoes`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/condicoes`];
    cy.clickIfExist(`[data-cy="cadastros-complementares"]`);
    cy.clickIfExist(`[data-cy="cadastros-complementares/condicoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/meios-pagamento`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/meios-pagamento`];
    cy.clickIfExist(`[data-cy="cadastros-complementares"]`);
    cy.clickIfExist(`[data-cy="cadastros-complementares/meios-pagamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/contrato-fidelidade`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/contrato-fidelidade`];
    cy.clickIfExist(`[data-cy="cadastros-complementares"]`);
    cy.clickIfExist(`[data-cy="cadastros-complementares/contrato-fidelidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`];
    cy.clickIfExist(`[data-cy="cadastros-complementares"]`);
    cy.clickIfExist(`[data-cy="cadastros-complementares/operacoes-deducao-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/tabela-codigo`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/tabela-codigo`];
    cy.clickIfExist(`[data-cy="cadastros-complementares"]`);
    cy.clickIfExist(`[data-cy="cadastros-complementares/tabela-codigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-metadados`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-metadados`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.clickIfExist(`[data-cy="batimento-tributario/batimento-tributario-metadados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.clickIfExist(`[data-cy="batimento-tributario/batimento-tributario-processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/doc-divergentes`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/doc-divergentes`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.clickIfExist(`[data-cy="batimento-tributario/doc-divergentes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.clickIfExist(`[data-cy="batimento-tributario/batimento-tributario-atualiza"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.clickIfExist(`[data-cy="batimento-tributario/batimento-tributario-relatorio-divergentes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/simulacao`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/simulacao`];
    cy.clickIfExist(`[data-cy="simulacoes-cenarios"]`);
    cy.clickIfExist(`[data-cy="simulacoes-cenarios/simulacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`];
    cy.clickIfExist(`[data-cy="simulacoes-cenarios"]`);
    cy.clickIfExist(`[data-cy="simulacoes-cenarios/cenario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/execucao`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/execucao`];
    cy.clickIfExist(`[data-cy="simulacoes-cenarios"]`);
    cy.clickIfExist(`[data-cy="simulacoes-cenarios/execucao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracao->integracao/integracao-falcon`, () => {
    const actualId = [`root`, `integracao`, `integracao/integracao-falcon`];
    cy.clickIfExist(`[data-cy="integracao"]`);
    cy.clickIfExist(`[data-cy="integracao/integracao-falcon"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpeza-dados-tabela-calculo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpeza-cache-calculo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/teste-regra-arvore"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.clickIfExist(`[data-cy="relatorios-apoio/tributacao-vigente"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.clickIfExist(`[data-cy="relatorios-apoio/processo-calculo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.clickIfExist(`[data-cy="relatorios-apoio/processo-customizado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.clickIfExist(`[data-cy="relatorios-apoio/regras-recriacao-enquadramentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.clickIfExist(`[data-cy="relatorios-apoio/visualizacao-regras-arvore"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
    cy.clickIfExist(`[data-cy="relatorios-apoio/regras-mva"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->2270201794-power-search-button`, () => {
    const actualId = [`root`, `download`, `2270201794-power-search-button`];
    cy.visit('http://system-A8/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2270201794-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element download->2270201794-download`, () => {
    const actualId = [`root`, `download`, `2270201794-download`];
    cy.visit('http://system-A8/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2270201794-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element download->2270201794-detalhes`, () => {
    const actualId = [`root`, `download`, `2270201794-detalhes`];
    cy.visit('http://system-A8/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2270201794-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element download->2270201794-excluir`, () => {
    const actualId = [`root`, `download`, `2270201794-excluir`];
    cy.visit('http://system-A8/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2270201794-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/icms-aliquota"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-especifica`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-especifica`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/icms-aliquota-especifica"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-base`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-base`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/icms-base"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/icms-aliquota-adrem"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/mva`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/mva`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/mva"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/mva-cest`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/mva-cest`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/mva-cest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/mva-protocolos`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/mva-protocolos`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/mva-protocolos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/mva-ajustado`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/mva-ajustado`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/mva-ajustado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/ipi-pis`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/ipi-pis`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/ipi-pis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/pis-cofins`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/pis-cofins`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/iss-corpt`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/iss-corpt`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/inss`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/inss`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/inss"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/irrf-corpt`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/irrf-corpt`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/A10`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/A10`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/A10"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/arvore`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/arvore`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas/arvore"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas/desempate"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas/mensagens"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/deducao-nop-calculo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/contrato-fidelidade->2373227874-novo`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/contrato-fidelidade`, `2373227874-novo`];
    cy.visit('http://system-A8/cadastros-complementares/contrato-fidelidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2373227874-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/contrato-fidelidade->2373227874-power-search-button`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/contrato-fidelidade`, `2373227874-power-search-button`];
    cy.visit('http://system-A8/cadastros-complementares/contrato-fidelidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2373227874-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-novo`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-novo`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479390208-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-power-search-button`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-power-search-button`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479390208-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-visualizar/editar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-visualizar/editar`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479390208-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-excluir`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-excluir`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479390208-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-metadados->1182814559-power-search-button`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-metadados`, `1182814559-power-search-button`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-metadados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1182814559-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values batimento-tributario->batimento-tributario/batimento-tributario-metadados->1182814559-power-search-input and submit`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-metadados`, `1182814559-power-search-input`];
    cy.clickIfExist(`[data-cy="batimento-tributario"]`);
    cy.clickIfExist(`[data-cy="batimento-tributario/batimento-tributario-metadados"]`);
    cy.fillInputPowerSearch(`[data-cy="1182814559-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-executar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`, `3085244455-executar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-processo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3085244455-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-agendamentos`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`, `3085244455-agendamentos`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-processo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3085244455-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-power-search-button`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`, `3085244455-power-search-button`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-processo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3085244455-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-visualização`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`, `3085244455-visualização`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-processo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3085244455-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/doc-divergentes->2403781780-power-search-button`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/doc-divergentes`, `2403781780-power-search-button`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2403781780-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-executar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-executar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-agendamentos`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-agendamentos`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-power-search-button`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-power-search-button`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-visualização`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-visualização`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-executar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-executar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-agendamentos`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-agendamentos`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-power-search-button`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-power-search-button`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-visualização`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-visualização`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/simulacao->2532481961-novo`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/simulacao`, `2532481961-novo`];
    cy.visit('http://system-A8/simulacoes-cenarios/simula-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2532481961-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/simulacao->2532481961-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/simulacao`, `2532481961-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/simula-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2532481961-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-novo`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-novo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-moreoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-moreoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-moreoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-deleteoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-deleteoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/execucao->3724416881-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/execucao`, `3724416881-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/execucao-cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724416881-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element integracao->integracao/integracao-falcon->1966475492-power-search-button`, () => {
    const actualId = [`root`, `integracao`, `integracao/integracao-falcon`, `1966475492-power-search-button`];
    cy.visit('http://system-A8/integracao-falcon');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1966475492-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element integracao->integracao/integracao-falcon->1966475492-ver mais informações`, () => {
    const actualId = [`root`, `integracao`, `integracao/integracao-falcon`, `1966475492-ver mais informações`];
    cy.visit('http://system-A8/integracao-falcon');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1966475492-ver mais informações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-executar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-executar`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-agendamentos`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-power-search-button`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-visualização`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-executar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-executar`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-agendamentos`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-power-search-button`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-visualização`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-executar`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-executar`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-agendamentos`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-power-search-button`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-visualização`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-executar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-executar`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-agendamentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-agendamentos`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-power-search-button`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-power-search-button`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-visualização`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-executar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-executar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-agendamentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-agendamentos`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-power-search-button`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-power-search-button`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-visualização`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-executar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-executar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-agendamentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-agendamentos`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-power-search-button`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-power-search-button`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-visualização`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-executar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-executar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-agendamentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-agendamentos`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-power-search-button`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-power-search-button`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-visualização`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-executar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-executar`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-agendamentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-agendamentos`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-power-search-button`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-power-search-button`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-visualização`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-regerar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-regerar`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-detalhes`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-detalhes`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-abrir visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-abrir visualização`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-excluir`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-excluir`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-executar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-executar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-agendamentos`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-agendamentos`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-power-search-button`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-power-search-button`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-visualização`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-visualização`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-novo`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-power-search-button`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-unorderedlistoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-unorderedlistoutlined`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-unorderedlistoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-excluir`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-excluir`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/iss-corpt->parametrizacoes-tributacao/iss-corpt/iss-servico`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/iss-corpt`, `parametrizacoes-tributacao/iss-corpt/iss-servico`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt/iss-servico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/iss-corpt->parametrizacoes-tributacao/iss-corpt/iss-aliq-servico`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/iss-corpt`, `parametrizacoes-tributacao/iss-corpt/iss-aliq-servico`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt/iss-aliq-servico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/iss-corpt->parametrizacoes-tributacao/iss-corpt/iss-aliq-gen`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/iss-corpt`, `parametrizacoes-tributacao/iss-corpt/iss-aliq-gen`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/iss-corpt/iss-aliq-gen"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/irrf-corpt->parametrizacoes-tributacao/irrf-corpt/irrf-aliq-gen`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/irrf-corpt`, `parametrizacoes-tributacao/irrf-corpt/irrf-aliq-gen`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt/irrf-aliq-gen"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/irrf-corpt->parametrizacoes-tributacao/irrf-corpt/irrf-aliq-estab`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/irrf-corpt`, `parametrizacoes-tributacao/irrf-corpt/irrf-aliq-estab`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt/irrf-aliq-estab"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/irrf-corpt->parametrizacoes-tributacao/irrf-corpt/irrf-aliq-serv`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/irrf-corpt`, `parametrizacoes-tributacao/irrf-corpt/irrf-aliq-serv`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt/irrf-aliq-serv"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/irrf-corpt->parametrizacoes-tributacao/irrf-corpt/irrf-tabela-prog`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/irrf-corpt`, `parametrizacoes-tributacao/irrf-corpt/irrf-tabela-prog`];
    cy.clickIfExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickIfExist(`[data-cy="inteligencia-calculo/parametrizacoes-tributacao"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt"]`);
    cy.clickIfExist(`[data-cy="parametrizacoes-tributacao/irrf-corpt/irrf-tabela-prog"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/arvore->117702252-plusoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/arvore`, `117702252-plusoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/calculo-regras');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117702252-plusoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/arvore->117702252-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/arvore`, `117702252-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/calculo-regras');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117702252-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/arvore->117702252-recriar e efetivar enquadramentos`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/arvore`, `117702252-recriar e efetivar enquadramentos`];
    cy.visit('http://system-A8/inteligencia-calculo/calculo-regras');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117702252-recriar e efetivar enquadramentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/arvore->117702252-recriar enquadramentos sem efetivar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/arvore`, `117702252-recriar enquadramentos sem efetivar`];
    cy.visit('http://system-A8/inteligencia-calculo/calculo-regras');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117702252-recriar enquadramentos sem efetivar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-novo`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2198048127-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2198048127-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-visualizar/editar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-visualizar/editar`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2198048127-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-excluir`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-excluir`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2198048127-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-novo`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-playcircleoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-playcircleoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-playcircleoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-eyeoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-eyeoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-deleteoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-deleteoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-novo`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516430826-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516430826-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-eyeoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-eyeoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516430826-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-deleteoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-deleteoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516430826-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-novo`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038004463-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038004463-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038004463-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-excluir`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-excluir`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038004463-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3203512414-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-playcircleoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-playcircleoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3203512414-playcircleoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3203512414-unorderedlistoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3203512414-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/contrato-fidelidade->2373227874-novo->2804250823-salvar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/contrato-fidelidade`, `2373227874-novo`, `2804250823-salvar`];
    cy.visit('http://system-A8/cadastros-complementares/contrato-fidelidade/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2804250823-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/contrato-fidelidade->2373227874-novo->2804250823-cancelar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/contrato-fidelidade`, `2373227874-novo`, `2804250823-cancelar`];
    cy.visit('http://system-A8/cadastros-complementares/contrato-fidelidade/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2804250823-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values cadastros-complementares->cadastros-complementares/contrato-fidelidade->2373227874-novo->2804250823-powerselect-pfjCodigo and submit`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/contrato-fidelidade`, `2373227874-novo`, `2804250823-powerselect-pfjCodigo`];
    cy.visit('http://system-A8/cadastros-complementares/contrato-fidelidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2373227874-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2804250823-powerselect-pfjCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-novo->909359337-salvar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-novo`, `909359337-salvar`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="909359337-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-novo->909359337-cancelar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-novo`, `909359337-cancelar`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="909359337-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-novo->909359337-input-operCodigo-909359337-input-descricao and submit`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-novo`, `909359337-input-operCodigo-909359337-input-descricao`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479390208-novo"]`);
    cy.fillInput(`[data-cy="909359337-input-operCodigo"] textarea`, `Designer`);
    cy.fillInput(`[data-cy="909359337-input-descricao"] textarea`, `Personal Loan Account`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-visualizar/editar->135170140-remover item`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-visualizar/editar`, `135170140-remover item`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop/editar/CTE');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="135170140-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-visualizar/editar->135170140-salvar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-visualizar/editar`, `135170140-salvar`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop/editar/CTE');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="135170140-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-visualizar/editar->135170140-voltar`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-visualizar/editar`, `135170140-voltar`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop/editar/CTE');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="135170140-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values cadastros-complementares->cadastros-complementares/operacoes-deducao-nop->3479390208-visualizar/editar->135170140-input-descricao and submit`, () => {
    const actualId = [`root`, `cadastros-complementares`, `cadastros-complementares/operacoes-deducao-nop`, `3479390208-visualizar/editar`, `135170140-input-descricao`];
    cy.visit('http://system-A8/cadastros-complementares/operacao-deducao-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479390208-visualizar/editar"]`);
    cy.fillInput(`[data-cy="135170140-input-descricao"] textarea`, `Djibouti Franc`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-agendamentos->3085244455-voltar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`, `3085244455-agendamentos`, `3085244455-voltar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-processo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47772887D%7C%7C47772887&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3085244455-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values batimento-tributario->batimento-tributario/batimento-tributario-processo->3085244455-visualização->3085244455-item- and submit`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-processo`, `3085244455-visualização`, `3085244455-item-`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-processo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3085244455-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3085244455-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-executar->363060266-agendar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-executar`, `363060266-agendar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-executar"]`);
    cy.clickIfExist(`[data-cy="363060266-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-agendamentos->363060266-voltar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-agendamentos`, `363060266-voltar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47785422D%7C%7C47785422&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values batimento-tributario->batimento-tributario/batimento-tributario-atualiza->363060266-visualização->363060266-item- and submit`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-atualiza`, `363060266-visualização`, `363060266-item-`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-atualiza?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="363060266-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="363060266-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-executar->3796808049-múltipla seleção`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-executar`, `3796808049-múltipla seleção`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-executar"]`);
    cy.clickIfExist(`[data-cy="3796808049-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-executar->3796808049-agendar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-executar`, `3796808049-agendar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-executar"]`);
    cy.clickIfExist(`[data-cy="3796808049-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-agendamentos->3796808049-voltar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-agendamentos`, `3796808049-voltar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54958095D%7C%7C54958095&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-visualização->3796808049-item- and submit`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-visualização`, `3796808049-item-`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3796808049-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/simulacao->2532481961-novo->2460174048-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/simulacao`, `2532481961-novo`, `2460174048-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/simula-dof/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2460174048-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/simulacao->2532481961-novo->2460174048-cancelar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/simulacao`, `2532481961-novo`, `2460174048-cancelar`];
    cy.visit('http://system-A8/simulacoes-cenarios/simula-dof/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2460174048-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/simulacao->2532481961-novo->2460174048-input-numero-2460174048-powerselect-edofCodigo-2460174048-powerselect-indEntradaSaida-2460174048-powerselect-utilizaPfj-2460174048-powerselect-emitentePfjCodigo-2460174048-powerselect-emitenteLocCodigo-2460174048-powerselect-remetentePfjCodigo-2460174048-powerselect-remetenteLocCodigo-2460174048-powerselect-destinatarioPfjCodigo-2460174048-powerselect-destinatarioLocCodigo-2460174048-powerselect-ufCodigoEntregaSim-2460174048-powerselect-ufCodigoRetiradaSim-2460174048-powerselect-munPresServicoSim-2460174048-powerselect-cpagCodigo-2460174048-input-munCodOrigem-2460174048-input-munCodDestino-2460174048-input-monetary-vlFrete-2460174048-input-monetary-vlBaseCtStf-2460174048-input-monetary-vlIrrfAcum-2460174048-input-monetary-vlSeguro-2460174048-input-monetary-vlCtStf-2460174048-input-monetary-vlBaseInssRetAcum-2460174048-input-monetary-vlOutrasDespesas-2460174048-input-monetary-vlRendimentoIrrfAcum-2460174048-input-monetary-vlInssRetAcum and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/simulacao`, `2532481961-novo`, `2460174048-input-numero-2460174048-powerselect-edofCodigo-2460174048-powerselect-indEntradaSaida-2460174048-powerselect-utilizaPfj-2460174048-powerselect-emitentePfjCodigo-2460174048-powerselect-emitenteLocCodigo-2460174048-powerselect-remetentePfjCodigo-2460174048-powerselect-remetenteLocCodigo-2460174048-powerselect-destinatarioPfjCodigo-2460174048-powerselect-destinatarioLocCodigo-2460174048-powerselect-ufCodigoEntregaSim-2460174048-powerselect-ufCodigoRetiradaSim-2460174048-powerselect-munPresServicoSim-2460174048-powerselect-cpagCodigo-2460174048-input-munCodOrigem-2460174048-input-munCodDestino-2460174048-input-monetary-vlFrete-2460174048-input-monetary-vlBaseCtStf-2460174048-input-monetary-vlIrrfAcum-2460174048-input-monetary-vlSeguro-2460174048-input-monetary-vlCtStf-2460174048-input-monetary-vlBaseInssRetAcum-2460174048-input-monetary-vlOutrasDespesas-2460174048-input-monetary-vlRendimentoIrrfAcum-2460174048-input-monetary-vlInssRetAcum`];
    cy.visit('http://system-A8/simulacoes-cenarios/simula-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2532481961-novo"]`);
    cy.fillInput(`[data-cy="2460174048-input-numero"] textarea`, `plugandplay`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-indEntradaSaida"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-utilizaPfj"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-emitentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-emitenteLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-remetentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-remetenteLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-destinatarioPfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-ufCodigoEntregaSim"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-ufCodigoRetiradaSim"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-munPresServicoSim"] input`);
    cy.fillInputPowerSelect(`[data-cy="2460174048-powerselect-cpagCodigo"] input`);
    cy.fillInput(`[data-cy="2460174048-input-munCodOrigem"] textarea`, `Fresh`);
    cy.fillInput(`[data-cy="2460174048-input-munCodDestino"] textarea`, `override`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlFrete"] textarea`, `1,59`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlBaseCtStf"] textarea`, `1,12`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlIrrfAcum"] textarea`, `6,37`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlSeguro"] textarea`, `4,84`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlCtStf"] textarea`, `3,42`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlBaseInssRetAcum"] textarea`, `5,66`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlOutrasDespesas"] textarea`, `6,31`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlRendimentoIrrfAcum"] textarea`, `3,97`);
    cy.fillInput(`[data-cy="2460174048-input-monetary-vlInssRetAcum"] textarea`, `4,9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-novo->1723482664-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-novo`, `1723482664-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723482664-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-novo->1723482664-cancelar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-novo`, `1723482664-cancelar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723482664-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-novo->1723482664-input-cenCodigo-1723482664-input-descricao and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-novo`, `1723482664-input-cenCodigo-1723482664-input-descricao`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-novo"]`);
    cy.fillInput(`[data-cy="1723482664-input-cenCodigo"] textarea`, `Handmade`);
    cy.fillInput(`[data-cy="1723482664-input-descricao"] textarea`, `Auto Loan Account`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-moreoutlined->1764602721-item-`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-moreoutlined`, `1764602721-item-`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-moreoutlined"]`);
    cy.clickIfExist(`[data-cy="1764602721-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/editar/FVR_05');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2740471473-casos de teste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-remover item`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-remover item`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/editar/FVR_05');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2740471473-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/editar/FVR_05');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2740471473-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-voltar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-voltar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/editar/FVR_05');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2740471473-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-input-cenCodigo-2740471473-input-descricao and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-input-cenCodigo-2740471473-input-descricao`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1764602721-eyeoutlined"]`);
    cy.fillInput(`[data-cy="2740471473-input-cenCodigo"] textarea`, `Corporativo`);
    cy.fillInput(`[data-cy="2740471473-input-descricao"] textarea`, `Sleek Steel Soap`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-executar->690119939-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-executar`, `690119939-agendar`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-executar"]`);
    cy.clickIfExist(`[data-cy="690119939-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-dados-tabela-calculo->690119939-agendamentos->690119939-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-agendamentos`, `690119939-voltar`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47353087D%7C%7C47353087&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/limpeza-dados-tabela-calculo->690119939-visualização->690119939-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-dados-tabela-calculo`, `690119939-visualização`, `690119939-item-`];
    cy.visit('http://system-A8/processos/limpeza-dados-tabela-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="690119939-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="690119939-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-executar->4292064370-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-executar`, `4292064370-múltipla seleção`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-executar"]`);
    cy.clickIfExist(`[data-cy="4292064370-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-executar->4292064370-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-executar`, `4292064370-agendar`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-executar"]`);
    cy.clickIfExist(`[data-cy="4292064370-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-agendamentos->4292064370-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-agendamentos`, `4292064370-voltar`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46526222D%7C%7C46526222&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/limpeza-cache-calculo->4292064370-visualização->4292064370-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-visualização`, `4292064370-item-`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4292064370-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-executar->33832422-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-executar`, `33832422-múltipla seleção`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-executar"]`);
    cy.clickIfExist(`[data-cy="33832422-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-executar->33832422-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-executar`, `33832422-agendar`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-executar"]`);
    cy.clickIfExist(`[data-cy="33832422-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-agendamentos->33832422-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-agendamentos`, `33832422-voltar`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~237054D%7C%7C237054&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/teste-regra-arvore->33832422-visualização->33832422-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-visualização`, `33832422-item-`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="33832422-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-executar->4038971926-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-executar`, `4038971926-múltipla seleção`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-executar"]`);
    cy.clickIfExist(`[data-cy="4038971926-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-executar->4038971926-agendar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-executar`, `4038971926-agendar`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-executar"]`);
    cy.clickIfExist(`[data-cy="4038971926-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-executar->4038971926-input-PBASE_LEGA and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-executar`, `4038971926-input-PBASE_LEGA`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-executar"]`);
    cy.fillInput(`[data-cy="4038971926-input-PBASE_LEGA"] textarea`, `ebusiness`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-agendamentos->4038971926-voltar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-agendamentos`, `4038971926-voltar`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54958088D%7C%7C54958088&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-visualização->4038971926-item- and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-visualização`, `4038971926-item-`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4038971926-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-executar->3014098245-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-executar`, `3014098245-múltipla seleção`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-executar"]`);
    cy.clickIfExist(`[data-cy="3014098245-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-executar->3014098245-agendar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-executar`, `3014098245-agendar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-executar"]`);
    cy.clickIfExist(`[data-cy="3014098245-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-agendamentos->3014098245-voltar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-agendamentos`, `3014098245-voltar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210752D%7C%7C210752&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-visualização->3014098245-item- and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-visualização`, `3014098245-item-`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3014098245-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-executar->993067788-agendar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-executar`, `993067788-agendar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-executar"]`);
    cy.clickIfExist(`[data-cy="993067788-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-customizado->993067788-agendamentos->993067788-voltar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-agendamentos`, `993067788-voltar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~237469D%7C%7C237469&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/processo-customizado->993067788-visualização->993067788-item- and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-customizado`, `993067788-visualização`, `993067788-item-`];
    cy.visit('http://system-A8/relatorios-apoio/processo-customizado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="993067788-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="993067788-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-executar->1591012837-agendar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-executar`, `1591012837-agendar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-executar"]`);
    cy.clickIfExist(`[data-cy="1591012837-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-agendamentos->1591012837-voltar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-agendamentos`, `1591012837-voltar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12454872D%7C%7C12454872&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/regras-recriacao-enquadramentos->1591012837-visualização->1591012837-item- and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-recriacao-enquadramentos`, `1591012837-visualização`, `1591012837-item-`];
    cy.visit('http://system-A8/relatorios-apoio/regras-recriacao-enquadramentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1591012837-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1591012837-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-executar->2275226589-agendar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-executar`, `2275226589-agendar`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-executar"]`);
    cy.clickIfExist(`[data-cy="2275226589-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-agendamentos->2275226589-voltar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-agendamentos`, `2275226589-voltar`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54958093D%7C%7C54958093&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-visualização->2275226589-item- and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-visualização`, `2275226589-item-`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2275226589-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-detalhes->2275226589-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-detalhes`, `2275226589-dados disponíveis para impressão`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-detalhes"]`);
    cy.clickIfExist(`[data-cy="2275226589-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-abrir visualização->2275226589-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-abrir visualização`, `2275226589-aumentar o zoom`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2275226589-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-abrir visualização->2275226589-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-abrir visualização`, `2275226589-diminuir o zoom`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2275226589-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-abrir visualização->2275226589-expandir`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-abrir visualização`, `2275226589-expandir`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2275226589-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/visualizacao-regras-arvore->2275226589-abrir visualização->2275226589-download`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/visualizacao-regras-arvore`, `2275226589-abrir visualização`, `2275226589-download`];
    cy.visit('http://system-A8/relatorios-apoio/visualizacao-regras-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2275226589-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2275226589-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-executar->357684938-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-executar`, `357684938-múltipla seleção`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-executar"]`);
    cy.clickIfExist(`[data-cy="357684938-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-executar->357684938-agendar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-executar`, `357684938-agendar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-executar"]`);
    cy.clickIfExist(`[data-cy="357684938-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-agendamentos->357684938-voltar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-agendamentos`, `357684938-voltar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31990495D%7C%7C31990495&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/regras-mva->357684938-visualização->357684938-item- and submit`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-visualização`, `357684938-item-`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="357684938-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-novo->1754788590-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-novo`, `1754788590-salvar`];
    cy.visit('http://system-A8/aliq-icms-adrem/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1754788590-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-novo->1754788590-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-novo`, `1754788590-voltar`];
    cy.visit('http://system-A8/aliq-icms-adrem/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1754788590-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-novo->1754788590-input-codProdutoAnp-1754788590-powerselect-uniCodigo-1754788590-input-monetary-aliqAdremIcms-1754788590-input-baseLegal and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-novo`, `1754788590-input-codProdutoAnp-1754788590-powerselect-uniCodigo-1754788590-input-monetary-aliqAdremIcms-1754788590-input-baseLegal`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-novo"]`);
    cy.fillInput(`[data-cy="1754788590-input-codProdutoAnp"] textarea`, `neutral`);
    cy.fillInputPowerSelect(`[data-cy="1754788590-powerselect-uniCodigo"] input`);
    cy.fillInput(`[data-cy="1754788590-input-monetary-aliqAdremIcms"] textarea`, `2,05`);
    cy.fillInput(`[data-cy="1754788590-input-baseLegal"] textarea`, `TCP`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-unorderedlistoutlined->1737105115-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-unorderedlistoutlined`, `1737105115-voltar`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-unorderedlistoutlined"]`);
    cy.clickIfExist(`[data-cy="1737105115-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar->2782034485-selecionar critérios`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`, `2782034485-selecionar critérios`];
    cy.visit('http://system-A8/aliq-icms-adrem/editar/221');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2782034485-selecionar critérios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar->2782034485-remover item`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`, `2782034485-remover item`];
    cy.visit('http://system-A8/aliq-icms-adrem/editar/221');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2782034485-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar->2782034485-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`, `2782034485-salvar`];
    cy.visit('http://system-A8/aliq-icms-adrem/editar/221');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2782034485-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar->2782034485-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`, `2782034485-voltar`];
    cy.visit('http://system-A8/aliq-icms-adrem/editar/221');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2782034485-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar->2782034485-input-codProdutoAnp-2782034485-powerselect-uniCodigo-2782034485-input-monetary-aliqAdremIcms-2782034485-input-baseLegal and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`, `2782034485-input-codProdutoAnp-2782034485-powerselect-uniCodigo-2782034485-input-monetary-aliqAdremIcms-2782034485-input-baseLegal`];
    cy.visit('http://system-A8/aliq-icms-adrem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1737105115-visualizar/editar"]`);
    cy.fillInput(`[data-cy="2782034485-input-codProdutoAnp"] textarea`, `Metal`);
    cy.fillInputPowerSelect(`[data-cy="2782034485-powerselect-uniCodigo"] input`);
    cy.fillInput(`[data-cy="2782034485-input-monetary-aliqAdremIcms"] textarea`, `3,51`);
    cy.fillInput(`[data-cy="2782034485-input-baseLegal"] textarea`, `Liberian Dollar`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-novo->1571687114-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-novo`, `1571687114-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1571687114-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-novo->1571687114-cancelar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-novo`, `1571687114-cancelar`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1571687114-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-novo->1571687114-input-titulo-1571687114-powerselect-xrgrId-1571687114-powerselect-xrgrIdPreterida and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-novo`, `1571687114-input-titulo-1571687114-powerselect-xrgrId-1571687114-powerselect-xrgrIdPreterida`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2198048127-novo"]`);
    cy.fillInput(`[data-cy="1571687114-input-titulo"] textarea`, `haptic`);
    cy.fillInputPowerSelect(`[data-cy="1571687114-powerselect-xrgrId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1571687114-powerselect-xrgrIdPreterida"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-visualizar/editar->395722001-remover item`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-visualizar/editar`, `395722001-remover item`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view/editar/227170');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="395722001-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-visualizar/editar->395722001-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-visualizar/editar`, `395722001-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view/editar/227170');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="395722001-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-visualizar/editar->395722001-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-visualizar/editar`, `395722001-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view/editar/227170');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="395722001-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/desempate->2198048127-visualizar/editar->395722001-input-titulo-395722001-powerselect-xrgrId-395722001-powerselect-xrgrIdPreterida and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/desempate`, `2198048127-visualizar/editar`, `395722001-input-titulo-395722001-powerselect-xrgrId-395722001-powerselect-xrgrIdPreterida`];
    cy.visit('http://system-A8/inteligencia-calculo/desempate-regras-view');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2198048127-visualizar/editar"]`);
    cy.fillInput(`[data-cy="395722001-input-titulo"] textarea`, `Tasty`);
    cy.fillInputPowerSelect(`[data-cy="395722001-powerselect-xrgrId"] input`);
    cy.fillInputPowerSelect(`[data-cy="395722001-powerselect-xrgrIdPreterida"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-novo->4074578652-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-novo`, `4074578652-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4074578652-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-novo->4074578652-cancelar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-novo`, `4074578652-cancelar`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4074578652-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-novo->4074578652-powerselect-tipo-4074578652-textarea-titulo-4074578652-textarea-exemplo-4074578652-checkbox-indDof-4074578652-checkbox-indDinamica-4074578652-input-origem-4074578652-input-chaveOrigem-4074578652-input-incluidoPor-4074578652-input-alteradoPor and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-novo`, `4074578652-powerselect-tipo-4074578652-textarea-titulo-4074578652-textarea-exemplo-4074578652-checkbox-indDof-4074578652-checkbox-indDinamica-4074578652-input-origem-4074578652-input-chaveOrigem-4074578652-input-incluidoPor-4074578652-input-alteradoPor`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="4074578652-powerselect-tipo"] input`);
    cy.fillInput(`[data-cy="4074578652-textarea-titulo"] input`, `Sports`);
    cy.fillInput(`[data-cy="4074578652-textarea-exemplo"] input`, `parsing`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4074578652-checkbox-indDof"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4074578652-checkbox-indDinamica"] textarea`);
    cy.fillInput(`[data-cy="4074578652-input-origem"] textarea`, `Grocery`);
    cy.fillInput(`[data-cy="4074578652-input-chaveOrigem"] textarea`, `sensor`);
    cy.fillInput(`[data-cy="4074578652-input-incluidoPor"] textarea`, `Minas Gerais`);
    cy.fillInput(`[data-cy="4074578652-input-alteradoPor"] textarea`, `Beauty`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-eyeoutlined->457408419-remover item`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-eyeoutlined`, `457408419-remover item`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem/editar/1591');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="457408419-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-eyeoutlined->457408419-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-eyeoutlined`, `457408419-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem/editar/1591');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="457408419-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-eyeoutlined->457408419-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-eyeoutlined`, `457408419-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem/editar/1591');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="457408419-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens->2059809325-eyeoutlined->457408419-powerselect-tipo-457408419-textarea-titulo-457408419-textarea-exemplo-457408419-checkbox-indDof-457408419-checkbox-indDinamica-457408419-input-origem-457408419-input-chaveOrigem-457408419-input-incluidoPor-457408419-input-alteradoPor and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens`, `2059809325-eyeoutlined`, `457408419-powerselect-tipo-457408419-textarea-titulo-457408419-textarea-exemplo-457408419-checkbox-indDof-457408419-checkbox-indDinamica-457408419-input-origem-457408419-input-chaveOrigem-457408419-input-incluidoPor-457408419-input-alteradoPor`];
    cy.visit('http://system-A8/inteligencia-calculo/rgr-mensagem');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059809325-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="457408419-powerselect-tipo"] input`);
    cy.fillInput(`[data-cy="457408419-textarea-titulo"] input`, `Minas Gerais`);
    cy.fillInput(`[data-cy="457408419-textarea-exemplo"] input`, `Dinmico`);
    cy.fillInputCheckboxOrRadio(`[data-cy="457408419-checkbox-indDof"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="457408419-checkbox-indDinamica"] textarea`);
    cy.fillInput(`[data-cy="457408419-input-origem"] textarea`, `copying`);
    cy.fillInput(`[data-cy="457408419-input-chaveOrigem"] textarea`, `connecting`);
    cy.fillInput(`[data-cy="457408419-input-incluidoPor"] textarea`, `Sleek Steel Hat`);
    cy.fillInput(`[data-cy="457408419-input-alteradoPor"] textarea`, `withdrawal`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-novo->2044930367-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-novo`, `2044930367-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2044930367-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-novo->2044930367-cancelar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-novo`, `2044930367-cancelar`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2044930367-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-novo->2044930367-powerselect-ufCodigo-2044930367-radio-indUf-2044930367-powerselect-cfopCodigo-2044930367-powerselect-idMsg and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-novo`, `2044930367-powerselect-ufCodigo-2044930367-radio-indUf-2044930367-powerselect-cfopCodigo-2044930367-powerselect-idMsg`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516430826-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2044930367-powerselect-ufCodigo"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2044930367-radio-indUf"] input`);
    cy.fillInputPowerSelect(`[data-cy="2044930367-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2044930367-powerselect-idMsg"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-eyeoutlined->894859336-remover item`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-eyeoutlined`, `894859336-remover item`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf/editar/CL/6.101/D');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="894859336-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-eyeoutlined->894859336-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-eyeoutlined`, `894859336-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf/editar/CL/6.101/D');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="894859336-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-eyeoutlined->894859336-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-eyeoutlined`, `894859336-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf/editar/CL/6.101/D');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="894859336-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais->3516430826-eyeoutlined->894859336-powerselect-ufCodigo-894859336-radio-indUf-894859336-powerselect-cfopCodigo-894859336-powerselect-idMsg and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/mensagens-fiscais`, `3516430826-eyeoutlined`, `894859336-powerselect-ufCodigo-894859336-radio-indUf-894859336-powerselect-cfopCodigo-894859336-powerselect-idMsg`];
    cy.visit('http://system-A8/inteligencia-calculo/mensagem-cfop-uf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516430826-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="894859336-powerselect-ufCodigo"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="894859336-radio-indUf"] input`);
    cy.fillInputPowerSelect(`[data-cy="894859336-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="894859336-powerselect-idMsg"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-novo->2737434458-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-novo`, `2737434458-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2737434458-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-novo->2737434458-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-novo`, `2737434458-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2737434458-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-novo->2737434458-powerselect-nopCodigo-2737434458-powerselect-operCodigo-2737434458-powerselect-finCodigo-2737434458-powerselect-tipoProducao and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-novo`, `2737434458-powerselect-nopCodigo-2737434458-powerselect-operCodigo-2737434458-powerselect-finCodigo-2737434458-powerselect-tipoProducao`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038004463-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2737434458-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2737434458-powerselect-operCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2737434458-powerselect-finCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2737434458-powerselect-tipoProducao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-selecionar critério`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-selecionar critério`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-selecionar critério"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-remover item`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-remover item`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-powerselect-nopCodigo-3810031009-powerselect-operCodigo-3810031009-powerselect-finCodigo-3810031009-powerselect-tipoProducao and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-powerselect-nopCodigo-3810031009-powerselect-operCodigo-3810031009-powerselect-finCodigo-3810031009-powerselect-tipoProducao`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038004463-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="3810031009-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3810031009-powerselect-operCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3810031009-powerselect-finCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3810031009-powerselect-tipoProducao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-novo`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-playcircleoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-playcircleoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-playcircleoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-deleteoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-deleteoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined->3390621447-mais operações`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`, `3390621447-mais operações`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/editar/233162/codpadrao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390621447-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined->3390621447-eyeoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`, `3390621447-eyeoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/editar/233162/codpadrao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390621447-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined->3390621447-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`, `3390621447-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/editar/233162/codpadrao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390621447-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element batimento-tributario->batimento-tributario/batimento-tributario-relatorio-divergentes->3796808049-executar->3796808049-múltipla seleção->3796808049-cancelar`, () => {
    const actualId = [`root`, `batimento-tributario`, `batimento-tributario/batimento-tributario-relatorio-divergentes`, `3796808049-executar`, `3796808049-múltipla seleção`, `3796808049-cancelar`];
    cy.visit('http://system-A8/batimento-tributario/batimento-tributario-relatorio-divergentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3796808049-executar"]`);
    cy.clickIfExist(`[data-cy="3796808049-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3796808049-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-novo`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-novo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-moreoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-moreoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-moreoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-deleteoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-deleteoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/limpeza-cache-calculo->4292064370-executar->4292064370-múltipla seleção->4292064370-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-cache-calculo`, `4292064370-executar`, `4292064370-múltipla seleção`, `4292064370-cancelar`];
    cy.visit('http://system-A8/processos/limpeza-cache-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4292064370-executar"]`);
    cy.clickIfExist(`[data-cy="4292064370-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4292064370-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-executar->33832422-múltipla seleção->33832422-próximo`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-executar`, `33832422-múltipla seleção`, `33832422-próximo`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-executar"]`);
    cy.clickIfExist(`[data-cy="33832422-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="33832422-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/teste-regra-arvore->33832422-executar->33832422-múltipla seleção->33832422-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/teste-regra-arvore`, `33832422-executar`, `33832422-múltipla seleção`, `33832422-cancelar`];
    cy.visit('http://system-A8/processos/teste-regra-arvore?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="33832422-executar"]`);
    cy.clickIfExist(`[data-cy="33832422-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="33832422-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/tributacao-vigente->4038971926-executar->4038971926-múltipla seleção->4038971926-cancelar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/tributacao-vigente`, `4038971926-executar`, `4038971926-múltipla seleção`, `4038971926-cancelar`];
    cy.visit('http://system-A8/relatorios-apoio/tributacao-vigente?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4038971926-executar"]`);
    cy.clickIfExist(`[data-cy="4038971926-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4038971926-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-executar->3014098245-múltipla seleção->3014098245-próximo`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-executar`, `3014098245-múltipla seleção`, `3014098245-próximo`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-executar"]`);
    cy.clickIfExist(`[data-cy="3014098245-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3014098245-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/processo-calculo->3014098245-executar->3014098245-múltipla seleção->3014098245-cancelar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/processo-calculo`, `3014098245-executar`, `3014098245-múltipla seleção`, `3014098245-cancelar`];
    cy.visit('http://system-A8/relatorios-apoio/processo-calculo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3014098245-executar"]`);
    cy.clickIfExist(`[data-cy="3014098245-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3014098245-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-executar->357684938-múltipla seleção->357684938-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-executar`, `357684938-múltipla seleção`, `357684938-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-executar"]`);
    cy.clickIfExist(`[data-cy="357684938-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="357684938-este parâmetro possui mais de 100 valores. clique aqui para carregar todos."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-executar->357684938-múltipla seleção->357684938-próximo`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-executar`, `357684938-múltipla seleção`, `357684938-próximo`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-executar"]`);
    cy.clickIfExist(`[data-cy="357684938-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="357684938-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/regras-mva->357684938-executar->357684938-múltipla seleção->357684938-cancelar`, () => {
    const actualId = [`root`, `relatorios-apoio`, `relatorios-apoio/regras-mva`, `357684938-executar`, `357684938-múltipla seleção`, `357684938-cancelar`];
    cy.visit('http://system-A8/relatorios-apoio/regras-mva?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="357684938-executar"]`);
    cy.clickIfExist(`[data-cy="357684938-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="357684938-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-tributacao->parametrizacoes-tributacao/icms-aliquota-adrem->1737105115-visualizar/editar->2782034485-selecionar critérios->2782034485-item-undefined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-tributacao`, `parametrizacoes-tributacao/icms-aliquota-adrem`, `1737105115-visualizar/editar`, `2782034485-selecionar critérios`, `2782034485-item-undefined`];
    cy.visit('http://system-A8/aliq-icms-adrem/editar/221');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2782034485-selecionar critérios"]`);
    cy.clickIfExist(`[data-cy="2782034485-item-undefined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-selecionar critério->3810031009-item-undefined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-selecionar critério`, `3810031009-item-undefined`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-selecionar critério"]`);
    cy.clickIfExist(`[data-cy="3810031009-item-undefined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-novo->1186623104-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-novo`, `1186623104-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1186623104-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-novo->1186623104-cancelar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-novo`, `1186623104-cancelar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1186623104-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-novo->1186623104-input-titulo and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-novo`, `1186623104-input-titulo`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-novo"]`);
    cy.fillInput(`[data-cy="1186623104-input-titulo"] textarea`, `Baby`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-novo`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-novo`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-eyeoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-eyeoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-deleteoutlined`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-deleteoutlined`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-remover item`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-remover item`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-input-titulo and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-input-titulo`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3584634889-eyeoutlined"]`);
    cy.fillInput(`[data-cy="902838972-input-titulo"] textarea`, `Small`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined->3390621447-mais operações->3390621447-item-`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`, `3390621447-mais operações`, `3390621447-item-`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/editar/233162/codpadrao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390621447-mais operações"]`);
    cy.clickIfExist(`[data-cy="3390621447-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined->3390621447-eyeoutlined->885646985-voltar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`, `3390621447-eyeoutlined`, `885646985-voltar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/codpadrao/processo-padrao-estrutura/editar/253115');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="885646985-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-eyeoutlined->3390621447-eyeoutlined->885646985-input-ordem-885646985-textarea-comentario-885646985-textarea-codigoVigente and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-eyeoutlined`, `3390621447-eyeoutlined`, `885646985-input-ordem-885646985-textarea-comentario-885646985-textarea-codigoVigente`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/editar/233162/codpadrao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390621447-eyeoutlined"]`);
    cy.fillInput(`[data-cy="885646985-input-ordem"] textarea`, `PCI`);
    cy.fillInput(`[data-cy="885646985-textarea-comentario"] input`, `Salad`);
    cy.fillInput(`[data-cy="885646985-textarea-codigoVigente"] input`, `deposit`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-novo->1247472997-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-novo`, `1247472997-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1247472997-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-novo->1247472997-cancelar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-novo`, `1247472997-cancelar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1247472997-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-novo->1247472997-powerselect-informanteEstCodigo-1247472997-input-numero-1247472997-powerselect-edofCodigo-1247472997-powerselect-utilizaPfj-1247472997-powerselect-indCenarioAtivo-1247472997-powerselect-emitentePfjCodigo-1247472997-powerselect-emitenteLocCodigo-1247472997-powerselect-remetentePfjCodigo-1247472997-powerselect-remetenteLocCodigo-1247472997-powerselect-destinatarioPfjCodigo-1247472997-powerselect-destinatarioLocCodigo-1247472997-powerselect-munPresServico-1247472997-powerselect-ufCodigoEntrega-1247472997-powerselect-ufCodigoRetirada-1247472997-powerselect-cpagCodigo-1247472997-input-munCodOrigem-1247472997-input-munCodDestino-1247472997-input-number-vlFrete-1247472997-input-number-vlSeguro-1247472997-input-number-vlOutrasDespesas-1247472997-input-number-vlBaseCtStf-1247472997-input-number-vlCtStf and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-novo`, `1247472997-powerselect-informanteEstCodigo-1247472997-input-numero-1247472997-powerselect-edofCodigo-1247472997-powerselect-utilizaPfj-1247472997-powerselect-indCenarioAtivo-1247472997-powerselect-emitentePfjCodigo-1247472997-powerselect-emitenteLocCodigo-1247472997-powerselect-remetentePfjCodigo-1247472997-powerselect-remetenteLocCodigo-1247472997-powerselect-destinatarioPfjCodigo-1247472997-powerselect-destinatarioLocCodigo-1247472997-powerselect-munPresServico-1247472997-powerselect-ufCodigoEntrega-1247472997-powerselect-ufCodigoRetirada-1247472997-powerselect-cpagCodigo-1247472997-input-munCodOrigem-1247472997-input-munCodDestino-1247472997-input-number-vlFrete-1247472997-input-number-vlSeguro-1247472997-input-number-vlOutrasDespesas-1247472997-input-number-vlBaseCtStf-1247472997-input-number-vlCtStf`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-informanteEstCodigo"] input`);
    cy.fillInput(`[data-cy="1247472997-input-numero"] textarea`, `Administrador`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-utilizaPfj"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-indCenarioAtivo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-emitentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-emitenteLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-remetentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-remetenteLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-destinatarioPfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-munPresServico"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-ufCodigoEntrega"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-ufCodigoRetirada"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247472997-powerselect-cpagCodigo"] input`);
    cy.fillInput(`[data-cy="1247472997-input-munCodOrigem"] textarea`, `Mandatory`);
    cy.fillInput(`[data-cy="1247472997-input-munCodDestino"] textarea`, `hierarchy`);
    cy.fillInput(`[data-cy="1247472997-input-number-vlFrete"] textarea`, `9`);
    cy.fillInput(`[data-cy="1247472997-input-number-vlSeguro"] textarea`, `8`);
    cy.fillInput(`[data-cy="1247472997-input-number-vlOutrasDespesas"] textarea`, `8`);
    cy.fillInput(`[data-cy="1247472997-input-number-vlBaseCtStf"] textarea`, `3`);
    cy.fillInput(`[data-cy="1247472997-input-number-vlCtStf"] textarea`, `8`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-moreoutlined->762590724-item-`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-moreoutlined`, `762590724-item-`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-moreoutlined"]`);
    cy.clickIfExist(`[data-cy="762590724-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/editar/226');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3409041606-itens"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/editar/226');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3409041606-resultados do teste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-remover item`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-remover item`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/editar/226');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3409041606-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/editar/226');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3409041606-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-voltar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-voltar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/editar/226');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3409041606-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-powerselect-informanteEstCodigo-3409041606-input-numero-3409041606-powerselect-edofCodigo-3409041606-powerselect-indEntradaSaida-3409041606-powerselect-indCenarioAtivo-3409041606-powerselect-destinatarioPfjCodigo-3409041606-powerselect-destinatarioLocCodigo-3409041606-powerselect-munPresServico-3409041606-powerselect-ufCodigoEntrega-3409041606-powerselect-ufCodigoRetirada-3409041606-powerselect-cpagCodigo-3409041606-input-munCodOrigem-3409041606-input-munCodDestino-3409041606-input-number-vlFrete-3409041606-input-number-vlSeguro-3409041606-input-number-vlOutrasDespesas-3409041606-input-number-vlBaseCtStf-3409041606-input-number-vlCtStf and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-powerselect-informanteEstCodigo-3409041606-input-numero-3409041606-powerselect-edofCodigo-3409041606-powerselect-indEntradaSaida-3409041606-powerselect-indCenarioAtivo-3409041606-powerselect-destinatarioPfjCodigo-3409041606-powerselect-destinatarioLocCodigo-3409041606-powerselect-munPresServico-3409041606-powerselect-ufCodigoEntrega-3409041606-powerselect-ufCodigoRetirada-3409041606-powerselect-cpagCodigo-3409041606-input-munCodOrigem-3409041606-input-munCodDestino-3409041606-input-number-vlFrete-3409041606-input-number-vlSeguro-3409041606-input-number-vlOutrasDespesas-3409041606-input-number-vlBaseCtStf-3409041606-input-number-vlCtStf`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="762590724-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-informanteEstCodigo"] input`);
    cy.fillInput(`[data-cy="3409041606-input-numero"] textarea`, `deposit`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-indEntradaSaida"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-indCenarioAtivo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-destinatarioPfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-munPresServico"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-ufCodigoEntrega"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-ufCodigoRetirada"] input`);
    cy.fillInputPowerSelect(`[data-cy="3409041606-powerselect-cpagCodigo"] input`);
    cy.fillInput(`[data-cy="3409041606-input-munCodOrigem"] textarea`, `So Paulo`);
    cy.fillInput(`[data-cy="3409041606-input-munCodDestino"] textarea`, `Benin`);
    cy.fillInput(`[data-cy="3409041606-input-number-vlFrete"] textarea`, `5`);
    cy.fillInput(`[data-cy="3409041606-input-number-vlSeguro"] textarea`, `10`);
    cy.fillInput(`[data-cy="3409041606-input-number-vlOutrasDespesas"] textarea`, `9`);
    cy.fillInput(`[data-cy="3409041606-input-number-vlBaseCtStf"] textarea`, `7`);
    cy.fillInput(`[data-cy="3409041606-input-number-vlCtStf"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-selecionar critério->3810031009-item-undefined->3810031009-power-search-button`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-selecionar critério`, `3810031009-item-undefined`, `3810031009-power-search-button`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-selecionar critério"]`);
    cy.clickIfExist(`[data-cy="3810031009-item-undefined"]`);
    cy.clickIfExist(`[data-cy="3810031009-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/deducao-nop-calculo->4038004463-visualizar/editar->3810031009-selecionar critério->3810031009-item-undefined->3810031009-power-search-input and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/deducao-nop-calculo`, `4038004463-visualizar/editar`, `3810031009-selecionar critério`, `3810031009-item-undefined`, `3810031009-power-search-input`];
    cy.visit('http://system-A8/inteligencia-calculo/deducao-nop-calculo/pre-calculo/editar/901');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3810031009-selecionar critério"]`);
    cy.clickIfExist(`[data-cy="3810031009-item-undefined"]`);
    cy.fillInputPowerSearch(`[data-cy="3810031009-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-novo->103581300-salvar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-novo`, `103581300-salvar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/245112/tratamento/processo-tratamento-estrutura/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="103581300-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-novo->103581300-cancelar`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-novo`, `103581300-cancelar`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/245112/tratamento/processo-tratamento-estrutura/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="103581300-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-novo->103581300-powerselect-ordem-103581300-input-comentario-103581300-textarea-codigoVigente and submit`, () => {
    const actualId = [`root`, `inteligencia-calculo`, `inteligencia-calculo/parametrizacoes-avancadas`, `inteligencia-calculo/parametrizacoes-avancadas/processos`, `3203512414-unorderedlistoutlined`, `3584634889-eyeoutlined`, `902838972-novo`, `103581300-powerselect-ordem-103581300-input-comentario-103581300-textarea-codigoVigente`];
    cy.visit('http://system-A8/inteligencia-calculo/processo-padrao-calculo/233162/processo-padrao-tratamento/editar/245112/tratamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="902838972-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="103581300-powerselect-ordem"] input`);
    cy.fillInput(`[data-cy="103581300-input-comentario"] textarea`, `overriding`);
    cy.fillInput(`[data-cy="103581300-textarea-codigoVigente"] input`, `FTP`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-caso de teste`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-caso de teste`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-caso de teste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-eyeoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-eyeoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-deleteoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-deleteoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-novo`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-novo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-moreoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-eyeoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-eyeoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-deleteoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-deleteoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-classes de mercadorias`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-classes de mercadorias`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3741981877-classes de mercadorias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3741981877-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-cancelar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-cancelar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3741981877-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-powerselect-subclasseIdf-3741981877-powerselect-nbmCodigo-3741981877-powerselect-cestCodigo-3741981877-powerselect-entsaiUniCodigo-3741981877-powerselect-amCodigo-3741981877-powerselect-finCodigo-3741981877-powerselect-omCodigo-3741981877-powerselect-nopCodigo-3741981877-powerselect-cfopCodigo-3741981877-powerselect-operCodigo-3741981877-powerselect-tipoProducao-3741981877-input-number-qtd-3741981877-input-number-precoUnitario-3741981877-input-number-precoTotal-3741981877-powerselect-indVlIcmsNoPreco-3741981877-powerselect-indVlPisCofinsNoPreco-3741981877-input-number-vlAjustePrecoTotal-3741981877-input-number-pesoLiquidoKg-3741981877-input-number-pesoBrutoKg-3741981877-input-number-vlAbatLegalIrrf-3741981877-input-number-vlAbatLegalInssRet-3741981877-input-number-vlAbatLegalIss-3741981877-input-number-vlRateioFrete-3741981877-input-number-vlRateioSeguro-3741981877-input-number-vlRateioOda-3741981877-input-number-vlRateioBaseCtStf-3741981877-input-number-vlRateioCtStf-3741981877-input-number-aliqIcms-3741981877-input-number-aliqIss-3741981877-powerselect-stcCodigo-3741981877-powerselect-stpCodigo and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-powerselect-subclasseIdf-3741981877-powerselect-nbmCodigo-3741981877-powerselect-cestCodigo-3741981877-powerselect-entsaiUniCodigo-3741981877-powerselect-amCodigo-3741981877-powerselect-finCodigo-3741981877-powerselect-omCodigo-3741981877-powerselect-nopCodigo-3741981877-powerselect-cfopCodigo-3741981877-powerselect-operCodigo-3741981877-powerselect-tipoProducao-3741981877-input-number-qtd-3741981877-input-number-precoUnitario-3741981877-input-number-precoTotal-3741981877-powerselect-indVlIcmsNoPreco-3741981877-powerselect-indVlPisCofinsNoPreco-3741981877-input-number-vlAjustePrecoTotal-3741981877-input-number-pesoLiquidoKg-3741981877-input-number-pesoBrutoKg-3741981877-input-number-vlAbatLegalIrrf-3741981877-input-number-vlAbatLegalInssRet-3741981877-input-number-vlAbatLegalIss-3741981877-input-number-vlRateioFrete-3741981877-input-number-vlRateioSeguro-3741981877-input-number-vlRateioOda-3741981877-input-number-vlRateioBaseCtStf-3741981877-input-number-vlRateioCtStf-3741981877-input-number-aliqIcms-3741981877-input-number-aliqIss-3741981877-powerselect-stcCodigo-3741981877-powerselect-stpCodigo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-subclasseIdf"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-nbmCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-cestCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-entsaiUniCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-amCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-finCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-omCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-operCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-tipoProducao"] input`);
    cy.fillInput(`[data-cy="3741981877-input-number-qtd"] textarea`, `10`);
    cy.fillInput(`[data-cy="3741981877-input-number-precoUnitario"] textarea`, `5`);
    cy.fillInput(`[data-cy="3741981877-input-number-precoTotal"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-indVlIcmsNoPreco"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-indVlPisCofinsNoPreco"] input`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAjustePrecoTotal"] textarea`, `5`);
    cy.fillInput(`[data-cy="3741981877-input-number-pesoLiquidoKg"] textarea`, `9`);
    cy.fillInput(`[data-cy="3741981877-input-number-pesoBrutoKg"] textarea`, `8`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAbatLegalIrrf"] textarea`, `6`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAbatLegalInssRet"] textarea`, `3`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAbatLegalIss"] textarea`, `9`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioFrete"] textarea`, `3`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioSeguro"] textarea`, `2`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioOda"] textarea`, `9`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioBaseCtStf"] textarea`, `4`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioCtStf"] textarea`, `6`);
    cy.fillInput(`[data-cy="3741981877-input-number-aliqIcms"] textarea`, `7`);
    cy.fillInput(`[data-cy="3741981877-input-number-aliqIss"] textarea`, `8`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-stcCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-stpCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-novo->1294589336-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-novo`, `1294589336-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1294589336-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-novo->1294589336-cancelar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-novo`, `1294589336-cancelar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1294589336-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-novo->1294589336-powerselect-cfopCodigo-1294589336-powerselect-munCodigoIss-1294589336-input-number-precoTotalM-1294589336-input-number-vlFiscalM-1294589336-input-number-vlAjustePrecoTotalM-1294589336-input-number-precoTotalP-1294589336-input-number-vlFiscalP-1294589336-input-number-vlAjustePrecoTotalP-1294589336-input-number-precoTotalS-1294589336-input-number-vlFiscalS-1294589336-input-number-vlAjustePrecoTotalS-1294589336-input-number-precoTotalO-1294589336-input-number-vlFiscalO-1294589336-input-number-vlTotalContabil-1294589336-input-number-vlTotalFaturado-1294589336-input-number-vlTotalIi-1294589336-input-number-vlFrete-1294589336-input-number-vlSeguro-1294589336-input-number-vlPedagio-1294589336-input-number-vlOutrasDespesas-1294589336-input-number-vlAbatNt-1294589336-input-number-vlOutrosAbat-1294589336-input-number-vlTotalIsentoIcms-1294589336-input-number-vlTotalOutrosIcms-1294589336-input-number-vlTotalBaseIcms-1294589336-input-number-vlTotalIcms-1294589336-input-number-vlTotalBaseIcmsPartRem-1294589336-input-number-vlTotalIcmsPartRem-1294589336-input-number-vlTotalBaseStf-1294589336-input-number-vlTotalStf-1294589336-input-number-vlTotalBaseIcmsPartDest-1294589336-input-number-vlTotalIcmsPartDest-1294589336-input-number-vlTotalBaseStfFronteira-1294589336-input-number-vlTotalStfFronteira-1294589336-input-number-vlTotalBaseIpi-1294589336-input-number-vlTotalIpi-1294589336-input-number-vlTotalBaseStt-1294589336-input-number-vlTotalStt-1294589336-input-number-vlTotalBaseCofins-1294589336-input-number-vlTotalCofins-1294589336-input-number-vlTotalBaseIcmsFcp-1294589336-input-number-vlTotalFcp-1294589336-input-number-vlTotalBasePisPasep-1294589336-input-number-vlTotalPisPasep-1294589336-input-number-vlTotalBaseIcmsFcpSt-1294589336-input-number-vlTotalFcpSt-1294589336-input-number-vlTotalBaseStfIdo-1294589336-input-number-vlTotalStfIdo-1294589336-input-number-vlCofinsSt-1294589336-input-number-vlTotalBaseIcmsDifa-1294589336-input-number-vlTotalIcmsDifa-1294589336-input-number-vlPisSt-1294589336-input-number-vlTotalBaseIss-1294589336-input-number-vlTotalIss-1294589336-input-number-vlTotalBasePisRet-1294589336-input-number-vlTotalPisRet-1294589336-input-number-vlTotalBaseInss-1294589336-input-number-vlTotalInss-1294589336-input-number-vlTotalBaseSenat-1294589336-input-number-vlTotalSenat-1294589336-input-number-vlTotalBaseIrrf-1294589336-input-number-vlTotalIrrf-1294589336-input-number-vlTotalBaseSest-1294589336-input-number-vlTotalSest-1294589336-input-number-vlTotalBaseInssRet-1294589336-input-number-vlTotalInssRet-1294589336-input-number-vlServNt-1294589336-input-number-vlBaseInssRetPer-1294589336-input-number-vlTotalInssRetPer-1294589336-input-number-vlTotalMatTerc-1294589336-input-number-vlTotalBaseCofinsRet-1294589336-input-number-vlTotalCofinsRet-1294589336-input-number-vlSecCat-1294589336-input-number-vlTotalBaseCsllRet-1294589336-input-number-vlTotalCsllRet and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-novo`, `1294589336-powerselect-cfopCodigo-1294589336-powerselect-munCodigoIss-1294589336-input-number-precoTotalM-1294589336-input-number-vlFiscalM-1294589336-input-number-vlAjustePrecoTotalM-1294589336-input-number-precoTotalP-1294589336-input-number-vlFiscalP-1294589336-input-number-vlAjustePrecoTotalP-1294589336-input-number-precoTotalS-1294589336-input-number-vlFiscalS-1294589336-input-number-vlAjustePrecoTotalS-1294589336-input-number-precoTotalO-1294589336-input-number-vlFiscalO-1294589336-input-number-vlTotalContabil-1294589336-input-number-vlTotalFaturado-1294589336-input-number-vlTotalIi-1294589336-input-number-vlFrete-1294589336-input-number-vlSeguro-1294589336-input-number-vlPedagio-1294589336-input-number-vlOutrasDespesas-1294589336-input-number-vlAbatNt-1294589336-input-number-vlOutrosAbat-1294589336-input-number-vlTotalIsentoIcms-1294589336-input-number-vlTotalOutrosIcms-1294589336-input-number-vlTotalBaseIcms-1294589336-input-number-vlTotalIcms-1294589336-input-number-vlTotalBaseIcmsPartRem-1294589336-input-number-vlTotalIcmsPartRem-1294589336-input-number-vlTotalBaseStf-1294589336-input-number-vlTotalStf-1294589336-input-number-vlTotalBaseIcmsPartDest-1294589336-input-number-vlTotalIcmsPartDest-1294589336-input-number-vlTotalBaseStfFronteira-1294589336-input-number-vlTotalStfFronteira-1294589336-input-number-vlTotalBaseIpi-1294589336-input-number-vlTotalIpi-1294589336-input-number-vlTotalBaseStt-1294589336-input-number-vlTotalStt-1294589336-input-number-vlTotalBaseCofins-1294589336-input-number-vlTotalCofins-1294589336-input-number-vlTotalBaseIcmsFcp-1294589336-input-number-vlTotalFcp-1294589336-input-number-vlTotalBasePisPasep-1294589336-input-number-vlTotalPisPasep-1294589336-input-number-vlTotalBaseIcmsFcpSt-1294589336-input-number-vlTotalFcpSt-1294589336-input-number-vlTotalBaseStfIdo-1294589336-input-number-vlTotalStfIdo-1294589336-input-number-vlCofinsSt-1294589336-input-number-vlTotalBaseIcmsDifa-1294589336-input-number-vlTotalIcmsDifa-1294589336-input-number-vlPisSt-1294589336-input-number-vlTotalBaseIss-1294589336-input-number-vlTotalIss-1294589336-input-number-vlTotalBasePisRet-1294589336-input-number-vlTotalPisRet-1294589336-input-number-vlTotalBaseInss-1294589336-input-number-vlTotalInss-1294589336-input-number-vlTotalBaseSenat-1294589336-input-number-vlTotalSenat-1294589336-input-number-vlTotalBaseIrrf-1294589336-input-number-vlTotalIrrf-1294589336-input-number-vlTotalBaseSest-1294589336-input-number-vlTotalSest-1294589336-input-number-vlTotalBaseInssRet-1294589336-input-number-vlTotalInssRet-1294589336-input-number-vlServNt-1294589336-input-number-vlBaseInssRetPer-1294589336-input-number-vlTotalInssRetPer-1294589336-input-number-vlTotalMatTerc-1294589336-input-number-vlTotalBaseCofinsRet-1294589336-input-number-vlTotalCofinsRet-1294589336-input-number-vlSecCat-1294589336-input-number-vlTotalBaseCsllRet-1294589336-input-number-vlTotalCsllRet`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1294589336-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1294589336-powerselect-munCodigoIss"] input`);
    cy.fillInput(`[data-cy="1294589336-input-number-precoTotalM"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlFiscalM"] textarea`, `10`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlAjustePrecoTotalM"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-precoTotalP"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlFiscalP"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlAjustePrecoTotalP"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-precoTotalS"] textarea`, `5`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlFiscalS"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlAjustePrecoTotalS"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-precoTotalO"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlFiscalO"] textarea`, `5`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalContabil"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalFaturado"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIi"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlFrete"] textarea`, `2`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlSeguro"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlPedagio"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlOutrasDespesas"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlAbatNt"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlOutrosAbat"] textarea`, `5`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIsentoIcms"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalOutrosIcms"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIcms"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIcms"] textarea`, `3`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIcmsPartRem"] textarea`, `2`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIcmsPartRem"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseStf"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalStf"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIcmsPartDest"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIcmsPartDest"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseStfFronteira"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalStfFronteira"] textarea`, `2`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIpi"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIpi"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseStt"] textarea`, `5`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalStt"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseCofins"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalCofins"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIcmsFcp"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalFcp"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBasePisPasep"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalPisPasep"] textarea`, `3`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIcmsFcpSt"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalFcpSt"] textarea`, `5`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseStfIdo"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalStfIdo"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlCofinsSt"] textarea`, `10`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIcmsDifa"] textarea`, `5`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIcmsDifa"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlPisSt"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIss"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIss"] textarea`, `10`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBasePisRet"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalPisRet"] textarea`, `3`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseInss"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalInss"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseSenat"] textarea`, `2`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalSenat"] textarea`, `2`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseIrrf"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalIrrf"] textarea`, `6`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseSest"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalSest"] textarea`, `8`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseInssRet"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalInssRet"] textarea`, `3`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlServNt"] textarea`, `4`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlBaseInssRetPer"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalInssRetPer"] textarea`, `1`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalMatTerc"] textarea`, `2`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseCofinsRet"] textarea`, `7`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalCofinsRet"] textarea`, `10`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlSecCat"] textarea`, `3`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalBaseCsllRet"] textarea`, `9`);
    cy.fillInput(`[data-cy="1294589336-input-number-vlTotalCsllRet"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-moreoutlined"]`);
    cy.clickIfExist(`[data-cy="3932598769-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-eyeoutlined->2012294748-itens`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-eyeoutlined`, `2012294748-itens`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/editar/2846');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2012294748-itens"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-eyeoutlined->2012294748-remover item`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-eyeoutlined`, `2012294748-remover item`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/editar/2846');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2012294748-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-eyeoutlined->2012294748-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-eyeoutlined`, `2012294748-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/editar/2846');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2012294748-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-eyeoutlined->2012294748-voltar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-eyeoutlined`, `2012294748-voltar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/editar/2846');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2012294748-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-eyeoutlined->2012294748-powerselect-cfopCodigo-2012294748-powerselect-munCodigoIss-2012294748-input-number-precoTotalM-2012294748-input-number-vlFiscalM-2012294748-input-number-vlAjustePrecoTotalM-2012294748-input-number-precoTotalP-2012294748-input-number-vlFiscalP-2012294748-input-number-vlAjustePrecoTotalP-2012294748-input-number-precoTotalS-2012294748-input-number-vlFiscalS-2012294748-input-number-vlAjustePrecoTotalS-2012294748-input-number-precoTotalO-2012294748-input-number-vlFiscalO-2012294748-input-number-vlTotalContabil-2012294748-input-number-vlTotalFaturado-2012294748-input-number-vlTotalIi-2012294748-input-number-vlFrete-2012294748-input-number-vlSeguro-2012294748-input-number-vlPedagio-2012294748-input-number-vlOutrasDespesas-2012294748-input-number-vlAbatNt-2012294748-input-number-vlOutrosAbat-2012294748-input-number-vlTotalIsentoIcms-2012294748-input-number-vlTotalOutrosIcms-2012294748-input-number-vlTotalBaseIcms-2012294748-input-number-vlTotalIcms-2012294748-input-number-vlTotalBaseIcmsPartRem-2012294748-input-number-vlTotalIcmsPartRem-2012294748-input-number-vlTotalBaseStf-2012294748-input-number-vlTotalStf-2012294748-input-number-vlTotalBaseIcmsPartDest-2012294748-input-number-vlTotalIcmsPartDest-2012294748-input-number-vlTotalBaseStfFronteira-2012294748-input-number-vlTotalStfFronteira-2012294748-input-number-vlTotalBaseIpi-2012294748-input-number-vlTotalIpi-2012294748-input-number-vlTotalBaseStt-2012294748-input-number-vlTotalStt-2012294748-input-number-vlTotalBaseCofins-2012294748-input-number-vlTotalCofins-2012294748-input-number-vlTotalBaseIcmsFcp-2012294748-input-number-vlTotalFcp-2012294748-input-number-vlTotalBasePisPasep-2012294748-input-number-vlTotalPisPasep-2012294748-input-number-vlTotalBaseIcmsFcpSt-2012294748-input-number-vlTotalFcpSt-2012294748-input-number-vlTotalBaseStfIdo-2012294748-input-number-vlTotalStfIdo-2012294748-input-number-vlCofinsSt-2012294748-input-number-vlTotalBaseIcmsDifa-2012294748-input-number-vlTotalIcmsDifa-2012294748-input-number-vlPisSt-2012294748-input-number-vlTotalBaseIss-2012294748-input-number-vlTotalIss-2012294748-input-number-vlTotalBasePisRet-2012294748-input-number-vlTotalPisRet-2012294748-input-number-vlTotalBaseInss-2012294748-input-number-vlTotalInss-2012294748-input-number-vlTotalBaseSenat-2012294748-input-number-vlTotalSenat-2012294748-input-number-vlTotalBaseIrrf-2012294748-input-number-vlTotalIrrf-2012294748-input-number-vlTotalBaseSest-2012294748-input-number-vlTotalSest-2012294748-input-number-vlTotalBaseInssRet-2012294748-input-number-vlTotalInssRet-2012294748-input-number-vlServNt-2012294748-input-number-vlBaseInssRetPer-2012294748-input-number-vlTotalInssRetPer-2012294748-input-number-vlTotalMatTerc-2012294748-input-number-vlTotalBaseCofinsRet-2012294748-input-number-vlTotalCofinsRet-2012294748-input-number-vlSecCat-2012294748-input-number-vlTotalBaseCsllRet-2012294748-input-number-vlTotalCsllRet and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-eyeoutlined`, `2012294748-powerselect-cfopCodigo-2012294748-powerselect-munCodigoIss-2012294748-input-number-precoTotalM-2012294748-input-number-vlFiscalM-2012294748-input-number-vlAjustePrecoTotalM-2012294748-input-number-precoTotalP-2012294748-input-number-vlFiscalP-2012294748-input-number-vlAjustePrecoTotalP-2012294748-input-number-precoTotalS-2012294748-input-number-vlFiscalS-2012294748-input-number-vlAjustePrecoTotalS-2012294748-input-number-precoTotalO-2012294748-input-number-vlFiscalO-2012294748-input-number-vlTotalContabil-2012294748-input-number-vlTotalFaturado-2012294748-input-number-vlTotalIi-2012294748-input-number-vlFrete-2012294748-input-number-vlSeguro-2012294748-input-number-vlPedagio-2012294748-input-number-vlOutrasDespesas-2012294748-input-number-vlAbatNt-2012294748-input-number-vlOutrosAbat-2012294748-input-number-vlTotalIsentoIcms-2012294748-input-number-vlTotalOutrosIcms-2012294748-input-number-vlTotalBaseIcms-2012294748-input-number-vlTotalIcms-2012294748-input-number-vlTotalBaseIcmsPartRem-2012294748-input-number-vlTotalIcmsPartRem-2012294748-input-number-vlTotalBaseStf-2012294748-input-number-vlTotalStf-2012294748-input-number-vlTotalBaseIcmsPartDest-2012294748-input-number-vlTotalIcmsPartDest-2012294748-input-number-vlTotalBaseStfFronteira-2012294748-input-number-vlTotalStfFronteira-2012294748-input-number-vlTotalBaseIpi-2012294748-input-number-vlTotalIpi-2012294748-input-number-vlTotalBaseStt-2012294748-input-number-vlTotalStt-2012294748-input-number-vlTotalBaseCofins-2012294748-input-number-vlTotalCofins-2012294748-input-number-vlTotalBaseIcmsFcp-2012294748-input-number-vlTotalFcp-2012294748-input-number-vlTotalBasePisPasep-2012294748-input-number-vlTotalPisPasep-2012294748-input-number-vlTotalBaseIcmsFcpSt-2012294748-input-number-vlTotalFcpSt-2012294748-input-number-vlTotalBaseStfIdo-2012294748-input-number-vlTotalStfIdo-2012294748-input-number-vlCofinsSt-2012294748-input-number-vlTotalBaseIcmsDifa-2012294748-input-number-vlTotalIcmsDifa-2012294748-input-number-vlPisSt-2012294748-input-number-vlTotalBaseIss-2012294748-input-number-vlTotalIss-2012294748-input-number-vlTotalBasePisRet-2012294748-input-number-vlTotalPisRet-2012294748-input-number-vlTotalBaseInss-2012294748-input-number-vlTotalInss-2012294748-input-number-vlTotalBaseSenat-2012294748-input-number-vlTotalSenat-2012294748-input-number-vlTotalBaseIrrf-2012294748-input-number-vlTotalIrrf-2012294748-input-number-vlTotalBaseSest-2012294748-input-number-vlTotalSest-2012294748-input-number-vlTotalBaseInssRet-2012294748-input-number-vlTotalInssRet-2012294748-input-number-vlServNt-2012294748-input-number-vlBaseInssRetPer-2012294748-input-number-vlTotalInssRetPer-2012294748-input-number-vlTotalMatTerc-2012294748-input-number-vlTotalBaseCofinsRet-2012294748-input-number-vlTotalCofinsRet-2012294748-input-number-vlSecCat-2012294748-input-number-vlTotalBaseCsllRet-2012294748-input-number-vlTotalCsllRet`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3932598769-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="2012294748-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2012294748-powerselect-munCodigoIss"] input`);
    cy.fillInput(`[data-cy="2012294748-input-number-precoTotalM"] textarea`, `2`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlFiscalM"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlAjustePrecoTotalM"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-precoTotalP"] textarea`, `8`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlFiscalP"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlAjustePrecoTotalP"] textarea`, `2`);
    cy.fillInput(`[data-cy="2012294748-input-number-precoTotalS"] textarea`, `3`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlFiscalS"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlAjustePrecoTotalS"] textarea`, `8`);
    cy.fillInput(`[data-cy="2012294748-input-number-precoTotalO"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlFiscalO"] textarea`, `2`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalContabil"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalFaturado"] textarea`, `4`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIi"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlFrete"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlSeguro"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlPedagio"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlOutrasDespesas"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlAbatNt"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlOutrosAbat"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIsentoIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalOutrosIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIcms"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIcms"] textarea`, `3`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIcmsPartRem"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIcmsPartRem"] textarea`, `3`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseStf"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalStf"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIcmsPartDest"] textarea`, `4`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIcmsPartDest"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseStfFronteira"] textarea`, `8`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalStfFronteira"] textarea`, `2`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIpi"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIpi"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseStt"] textarea`, `8`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalStt"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseCofins"] textarea`, `1`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalCofins"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIcmsFcp"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalFcp"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBasePisPasep"] textarea`, `8`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalPisPasep"] textarea`, `3`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIcmsFcpSt"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalFcpSt"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseStfIdo"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalStfIdo"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlCofinsSt"] textarea`, `1`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIcmsDifa"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIcmsDifa"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlPisSt"] textarea`, `4`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIss"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIss"] textarea`, `2`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBasePisRet"] textarea`, `1`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalPisRet"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseInss"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalInss"] textarea`, `4`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseSenat"] textarea`, `2`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalSenat"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseIrrf"] textarea`, `3`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalIrrf"] textarea`, `10`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseSest"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalSest"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseInssRet"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalInssRet"] textarea`, `1`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlServNt"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlBaseInssRetPer"] textarea`, `4`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalInssRetPer"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalMatTerc"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseCofinsRet"] textarea`, `9`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalCofinsRet"] textarea`, `5`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlSecCat"] textarea`, `7`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalBaseCsllRet"] textarea`, `6`);
    cy.fillInput(`[data-cy="2012294748-input-number-vlTotalCsllRet"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-classes de mercadorias->3741981877-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-classes de mercadorias`, `3741981877-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3741981877-classes de mercadorias"]`);
    cy.clickIfExist(`[data-cy="3741981877-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-classes de mercadorias->3741981877-voltar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-classes de mercadorias`, `3741981877-voltar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3741981877-classes de mercadorias"]`);
    cy.clickIfExist(`[data-cy="3741981877-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-classes de mercadorias->3741981877-power-search-input and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-classes de mercadorias`, `3741981877-power-search-input`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3741981877-classes de mercadorias"]`);
    cy.fillInputPowerSearch(`[data-cy="3741981877-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-itens->574141620-novo->3741981877-powerselect-subclasseIdf-3741981877-powerselect-nbmCodigo-3741981877-powerselect-cestCodigo-3741981877-powerselect-entsaiUniCodigo-3741981877-powerselect-amCodigo-3741981877-powerselect-finCodigo-3741981877-powerselect-omCodigo-3741981877-powerselect-nopCodigo-3741981877-powerselect-cfopCodigo-3741981877-powerselect-operCodigo-3741981877-powerselect-tipoProducao-3741981877-input-number-qtd-3741981877-input-number-precoUnitario-3741981877-input-number-precoTotal-3741981877-powerselect-indVlIcmsNoPreco-3741981877-powerselect-indVlPisCofinsNoPreco-3741981877-input-number-vlAjustePrecoTotal-3741981877-input-number-pesoLiquidoKg-3741981877-input-number-pesoBrutoKg-3741981877-input-number-vlAbatLegalIrrf-3741981877-input-number-vlAbatLegalInssRet-3741981877-input-number-vlAbatLegalIss-3741981877-input-number-vlRateioFrete-3741981877-input-number-vlRateioSeguro-3741981877-input-number-vlRateioOda-3741981877-input-number-vlRateioBaseCtStf-3741981877-input-number-vlRateioCtStf-3741981877-input-number-aliqIcms-3741981877-input-number-aliqIss-3741981877-powerselect-stcCodigo-3741981877-powerselect-stpCodigo->3741981877-powerselect-presCodigo and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-itens`, `574141620-novo`, `3741981877-powerselect-subclasseIdf-3741981877-powerselect-nbmCodigo-3741981877-powerselect-cestCodigo-3741981877-powerselect-entsaiUniCodigo-3741981877-powerselect-amCodigo-3741981877-powerselect-finCodigo-3741981877-powerselect-omCodigo-3741981877-powerselect-nopCodigo-3741981877-powerselect-cfopCodigo-3741981877-powerselect-operCodigo-3741981877-powerselect-tipoProducao-3741981877-input-number-qtd-3741981877-input-number-precoUnitario-3741981877-input-number-precoTotal-3741981877-powerselect-indVlIcmsNoPreco-3741981877-powerselect-indVlPisCofinsNoPreco-3741981877-input-number-vlAjustePrecoTotal-3741981877-input-number-pesoLiquidoKg-3741981877-input-number-pesoBrutoKg-3741981877-input-number-vlAbatLegalIrrf-3741981877-input-number-vlAbatLegalInssRet-3741981877-input-number-vlAbatLegalIss-3741981877-input-number-vlRateioFrete-3741981877-input-number-vlRateioSeguro-3741981877-input-number-vlRateioOda-3741981877-input-number-vlRateioBaseCtStf-3741981877-input-number-vlRateioCtStf-3741981877-input-number-aliqIcms-3741981877-input-number-aliqIss-3741981877-powerselect-stcCodigo-3741981877-powerselect-stpCodigo`, `3741981877-powerselect-presCodigo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="574141620-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-subclasseIdf"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-nbmCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-cestCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-entsaiUniCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-amCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-finCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-omCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-operCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-tipoProducao"] input`);
    cy.fillInput(`[data-cy="3741981877-input-number-qtd"] textarea`, `10`);
    cy.fillInput(`[data-cy="3741981877-input-number-precoUnitario"] textarea`, `5`);
    cy.fillInput(`[data-cy="3741981877-input-number-precoTotal"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-indVlIcmsNoPreco"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-indVlPisCofinsNoPreco"] input`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAjustePrecoTotal"] textarea`, `5`);
    cy.fillInput(`[data-cy="3741981877-input-number-pesoLiquidoKg"] textarea`, `9`);
    cy.fillInput(`[data-cy="3741981877-input-number-pesoBrutoKg"] textarea`, `8`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAbatLegalIrrf"] textarea`, `6`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAbatLegalInssRet"] textarea`, `3`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlAbatLegalIss"] textarea`, `9`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioFrete"] textarea`, `3`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioSeguro"] textarea`, `2`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioOda"] textarea`, `9`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioBaseCtStf"] textarea`, `4`);
    cy.fillInput(`[data-cy="3741981877-input-number-vlRateioCtStf"] textarea`, `6`);
    cy.fillInput(`[data-cy="3741981877-input-number-aliqIcms"] textarea`, `7`);
    cy.fillInput(`[data-cy="3741981877-input-number-aliqIss"] textarea`, `8`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-stcCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-stpCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="3741981877-powerselect-presCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-resultado do teste`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-resultado do teste`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-resultado do teste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-novo`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-novo`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-power-search-button`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-power-search-button`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-eyeoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-eyeoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-deleteoutlined`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-deleteoutlined`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-novo->2004344997-salvar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-novo`, `2004344997-salvar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2004344997-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-novo->2004344997-cancelar`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-novo`, `2004344997-cancelar`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2004344997-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-novo->2004344997-powerselect-idfNum-2004344997-powerselect-nopCodigo-2004344997-powerselect-cfopCodigo-2004344997-powerselect-stcCodigo-2004344997-powerselect-stpCodigo-2004344997-powerselect-staCodigo-2004344997-powerselect-stnCodigo-2004344997-powerselect-enqIpiCodigo-2004344997-input-modBaseIcmsCodigo-2004344997-input-modBaseIcmsStCodigo-2004344997-input-codBeneficioFiscal-2004344997-input-number-qtd-2004344997-input-number-precoUnitario-2004344997-input-number-precoTotal-2004344997-input-number-vlAjustePrecoTotal-2004344997-input-number-vlFaturado-2004344997-input-number-vlContabil-2004344997-input-number-precoNetUnit-2004344997-input-number-precoNetUnitPisCofins-2004344997-input-number-vlRateioAjustePreco-2004344997-input-number-vlRateioFrete-2004344997-input-number-vlRateioSeguro-2004344997-input-number-vlRateioOda-2004344997-input-number-percIcmsPartRem-2004344997-input-number-percIcmsPartDest-2004344997-input-number-percTributavelIcms-2004344997-input-number-percTributavelIpi-2004344997-input-number-percTributavelStf-2004344997-input-number-percDiferimentoIcms-2004344997-input-number-vlIcmsOperacao-2004344997-input-number-vlIcmsDiferido-2004344997-input-number-vlIsentoIcms-2004344997-input-number-vlOutrosIcms-2004344997-input-number-vlOutrosAbat-2004344997-input-number-vlIsentoIpi-2004344997-input-number-vlOutrosIpi-2004344997-input-number-vlIcmsStRecup-2004344997-input-number-vlIcmsSimplesNac-2004344997-input-number-vlIof-2004344997-input-number-aliqIcmsPartDest-2004344997-input-number-custoAdicao-2004344997-input-number-custoReducao-2004344997-input-number-vlCustoIcms-2004344997-input-number-vlCustoIpi-2004344997-input-number-vlCustoStf-2004344997-input-number-vlCustoAntecipIcms-2004344997-input-number-vlCustoIcmsDescL-2004344997-input-number-vlCustoPis-2004344997-input-number-vlCustoCofins and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-novo`, `2004344997-powerselect-idfNum-2004344997-powerselect-nopCodigo-2004344997-powerselect-cfopCodigo-2004344997-powerselect-stcCodigo-2004344997-powerselect-stpCodigo-2004344997-powerselect-staCodigo-2004344997-powerselect-stnCodigo-2004344997-powerselect-enqIpiCodigo-2004344997-input-modBaseIcmsCodigo-2004344997-input-modBaseIcmsStCodigo-2004344997-input-codBeneficioFiscal-2004344997-input-number-qtd-2004344997-input-number-precoUnitario-2004344997-input-number-precoTotal-2004344997-input-number-vlAjustePrecoTotal-2004344997-input-number-vlFaturado-2004344997-input-number-vlContabil-2004344997-input-number-precoNetUnit-2004344997-input-number-precoNetUnitPisCofins-2004344997-input-number-vlRateioAjustePreco-2004344997-input-number-vlRateioFrete-2004344997-input-number-vlRateioSeguro-2004344997-input-number-vlRateioOda-2004344997-input-number-percIcmsPartRem-2004344997-input-number-percIcmsPartDest-2004344997-input-number-percTributavelIcms-2004344997-input-number-percTributavelIpi-2004344997-input-number-percTributavelStf-2004344997-input-number-percDiferimentoIcms-2004344997-input-number-vlIcmsOperacao-2004344997-input-number-vlIcmsDiferido-2004344997-input-number-vlIsentoIcms-2004344997-input-number-vlOutrosIcms-2004344997-input-number-vlOutrosAbat-2004344997-input-number-vlIsentoIpi-2004344997-input-number-vlOutrosIpi-2004344997-input-number-vlIcmsStRecup-2004344997-input-number-vlIcmsSimplesNac-2004344997-input-number-vlIof-2004344997-input-number-aliqIcmsPartDest-2004344997-input-number-custoAdicao-2004344997-input-number-custoReducao-2004344997-input-number-vlCustoIcms-2004344997-input-number-vlCustoIpi-2004344997-input-number-vlCustoStf-2004344997-input-number-vlCustoAntecipIcms-2004344997-input-number-vlCustoIcmsDescL-2004344997-input-number-vlCustoPis-2004344997-input-number-vlCustoCofins`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-idfNum"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-stcCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-stpCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-staCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-stnCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-enqIpiCodigo"] input`);
    cy.fillInput(`[data-cy="2004344997-input-modBaseIcmsCodigo"] textarea`, `port`);
    cy.fillInput(`[data-cy="2004344997-input-modBaseIcmsStCodigo"] textarea`, `reboot`);
    cy.fillInput(`[data-cy="2004344997-input-codBeneficioFiscal"] textarea`, `withdrawal`);
    cy.fillInput(`[data-cy="2004344997-input-number-qtd"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoUnitario"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoTotal"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlAjustePrecoTotal"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlFaturado"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlContabil"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoNetUnit"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoNetUnitPisCofins"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioAjustePreco"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioFrete"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioSeguro"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioOda"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-percIcmsPartRem"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-percIcmsPartDest"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-percTributavelIcms"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-percTributavelIpi"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-percTributavelStf"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-percDiferimentoIcms"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsOperacao"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsDiferido"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIsentoIcms"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlOutrosIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlOutrosAbat"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIsentoIpi"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlOutrosIpi"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsStRecup"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsSimplesNac"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIof"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsPartDest"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-custoAdicao"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-custoReducao"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoIpi"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoStf"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoAntecipIcms"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoIcmsDescL"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoPis"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoCofins"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Filling values simulacoes-cenarios->simulacoes-cenarios/cenario->1764602721-eyeoutlined->2740471473-casos de teste->762590724-eyeoutlined->3409041606-resultados do teste->3932598769-moreoutlined->3932598769-item-->744241860-novo->2004344997-powerselect-idfNum-2004344997-powerselect-nopCodigo-2004344997-powerselect-cfopCodigo-2004344997-powerselect-stcCodigo-2004344997-powerselect-stpCodigo-2004344997-powerselect-staCodigo-2004344997-powerselect-stnCodigo-2004344997-powerselect-enqIpiCodigo-2004344997-input-modBaseIcmsCodigo-2004344997-input-modBaseIcmsStCodigo-2004344997-input-codBeneficioFiscal-2004344997-input-number-qtd-2004344997-input-number-precoUnitario-2004344997-input-number-precoTotal-2004344997-input-number-vlAjustePrecoTotal-2004344997-input-number-vlFaturado-2004344997-input-number-vlContabil-2004344997-input-number-precoNetUnit-2004344997-input-number-precoNetUnitPisCofins-2004344997-input-number-vlRateioAjustePreco-2004344997-input-number-vlRateioFrete-2004344997-input-number-vlRateioSeguro-2004344997-input-number-vlRateioOda-2004344997-input-number-percIcmsPartRem-2004344997-input-number-percIcmsPartDest-2004344997-input-number-percTributavelIcms-2004344997-input-number-percTributavelIpi-2004344997-input-number-percTributavelStf-2004344997-input-number-percDiferimentoIcms-2004344997-input-number-vlIcmsOperacao-2004344997-input-number-vlIcmsDiferido-2004344997-input-number-vlIsentoIcms-2004344997-input-number-vlOutrosIcms-2004344997-input-number-vlOutrosAbat-2004344997-input-number-vlIsentoIpi-2004344997-input-number-vlOutrosIpi-2004344997-input-number-vlIcmsStRecup-2004344997-input-number-vlIcmsSimplesNac-2004344997-input-number-vlIof-2004344997-input-number-aliqIcmsPartDest-2004344997-input-number-custoAdicao-2004344997-input-number-custoReducao-2004344997-input-number-vlCustoIcms-2004344997-input-number-vlCustoIpi-2004344997-input-number-vlCustoStf-2004344997-input-number-vlCustoAntecipIcms-2004344997-input-number-vlCustoIcmsDescL-2004344997-input-number-vlCustoPis-2004344997-input-number-vlCustoCofins->2004344997-input-number-vlBaseIcms-2004344997-input-number-vlTributavelIcms-2004344997-input-number-aliqIcms-2004344997-input-number-vlIcms-2004344997-input-number-vlBaseIpi-2004344997-input-number-vlTributavelIpi-2004344997-input-number-aliqIpi-2004344997-input-number-vlIpi-2004344997-input-number-vlBaseStf-2004344997-input-number-vlTributavelStf-2004344997-input-number-aliqStf-2004344997-input-number-vlStf-2004344997-input-number-vlBaseStfFronteira-2004344997-input-number-vlStfFronteira-2004344997-input-number-vlBaseStfIdo-2004344997-input-number-vlStfIdo-2004344997-input-number-vlBaseStt-2004344997-input-number-vlTributavelStt-2004344997-input-number-aliqStt-2004344997-input-number-vlStt-2004344997-input-number-vlBaseIcmsDescL-2004344997-input-number-vlTributavelIcmsDescL-2004344997-input-number-aliqIcmsDescL-2004344997-input-number-vlIcmsDescL-2004344997-input-number-vlBaseSuframa-2004344997-input-number-vlTributavelSuframa-2004344997-input-number-vlSuframa-2004344997-input-number-vlBasePis-2004344997-input-number-vlAliqPis-2004344997-input-number-vlImpostoPis-2004344997-input-number-vlBaseCofins-2004344997-input-number-vlAliqCofins-2004344997-input-number-vlImpostoCofins-2004344997-input-number-vlTributavelDifa-2004344997-input-number-aliqDifa-2004344997-input-number-vlDifa-2004344997-input-number-vlBaseIi-2004344997-input-number-aliqIi-2004344997-input-number-vlIi-2004344997-input-number-vlBasePisSt-2004344997-input-number-aliqPisSt-2004344997-input-number-vlPisSt-2004344997-input-number-vlBaseCofinsSt-2004344997-input-number-aliqCofinsSt-2004344997-input-number-vlCofinsSt-2004344997-input-number-vlBaseIcmsEfetivo-2004344997-input-number-aliqIcmsEfetivo-2004344997-input-number-vlIcmsEfetivo-2004344997-input-number-vlBaseIcmsFcp-2004344997-input-number-aliqIcmsFcp-2004344997-input-number-vlIcmsFcp-2004344997-input-number-vlBaseIcmsFcpst-2004344997-input-number-aliqIcmsFcpst-2004344997-input-number-vlIcmsFcpst-2004344997-input-number-vlBaseIcmsPartRem-2004344997-input-number-aliqIcmsPartRem-2004344997-input-number-vlIcmsPartRem-2004344997-input-number-vlBaseIcmsPartDest-2004344997-input-number-vlIcmsPartDest-2004344997-input-number-aliqAntecipIcms-2004344997-input-number-vlAntecipIcms and submit`, () => {
    const actualId = [`root`, `simulacoes-cenarios`, `simulacoes-cenarios/cenario`, `1764602721-eyeoutlined`, `2740471473-casos de teste`, `762590724-eyeoutlined`, `3409041606-resultados do teste`, `3932598769-moreoutlined`, `3932598769-item-`, `744241860-novo`, `2004344997-powerselect-idfNum-2004344997-powerselect-nopCodigo-2004344997-powerselect-cfopCodigo-2004344997-powerselect-stcCodigo-2004344997-powerselect-stpCodigo-2004344997-powerselect-staCodigo-2004344997-powerselect-stnCodigo-2004344997-powerselect-enqIpiCodigo-2004344997-input-modBaseIcmsCodigo-2004344997-input-modBaseIcmsStCodigo-2004344997-input-codBeneficioFiscal-2004344997-input-number-qtd-2004344997-input-number-precoUnitario-2004344997-input-number-precoTotal-2004344997-input-number-vlAjustePrecoTotal-2004344997-input-number-vlFaturado-2004344997-input-number-vlContabil-2004344997-input-number-precoNetUnit-2004344997-input-number-precoNetUnitPisCofins-2004344997-input-number-vlRateioAjustePreco-2004344997-input-number-vlRateioFrete-2004344997-input-number-vlRateioSeguro-2004344997-input-number-vlRateioOda-2004344997-input-number-percIcmsPartRem-2004344997-input-number-percIcmsPartDest-2004344997-input-number-percTributavelIcms-2004344997-input-number-percTributavelIpi-2004344997-input-number-percTributavelStf-2004344997-input-number-percDiferimentoIcms-2004344997-input-number-vlIcmsOperacao-2004344997-input-number-vlIcmsDiferido-2004344997-input-number-vlIsentoIcms-2004344997-input-number-vlOutrosIcms-2004344997-input-number-vlOutrosAbat-2004344997-input-number-vlIsentoIpi-2004344997-input-number-vlOutrosIpi-2004344997-input-number-vlIcmsStRecup-2004344997-input-number-vlIcmsSimplesNac-2004344997-input-number-vlIof-2004344997-input-number-aliqIcmsPartDest-2004344997-input-number-custoAdicao-2004344997-input-number-custoReducao-2004344997-input-number-vlCustoIcms-2004344997-input-number-vlCustoIpi-2004344997-input-number-vlCustoStf-2004344997-input-number-vlCustoAntecipIcms-2004344997-input-number-vlCustoIcmsDescL-2004344997-input-number-vlCustoPis-2004344997-input-number-vlCustoCofins`, `2004344997-input-number-vlBaseIcms-2004344997-input-number-vlTributavelIcms-2004344997-input-number-aliqIcms-2004344997-input-number-vlIcms-2004344997-input-number-vlBaseIpi-2004344997-input-number-vlTributavelIpi-2004344997-input-number-aliqIpi-2004344997-input-number-vlIpi-2004344997-input-number-vlBaseStf-2004344997-input-number-vlTributavelStf-2004344997-input-number-aliqStf-2004344997-input-number-vlStf-2004344997-input-number-vlBaseStfFronteira-2004344997-input-number-vlStfFronteira-2004344997-input-number-vlBaseStfIdo-2004344997-input-number-vlStfIdo-2004344997-input-number-vlBaseStt-2004344997-input-number-vlTributavelStt-2004344997-input-number-aliqStt-2004344997-input-number-vlStt-2004344997-input-number-vlBaseIcmsDescL-2004344997-input-number-vlTributavelIcmsDescL-2004344997-input-number-aliqIcmsDescL-2004344997-input-number-vlIcmsDescL-2004344997-input-number-vlBaseSuframa-2004344997-input-number-vlTributavelSuframa-2004344997-input-number-vlSuframa-2004344997-input-number-vlBasePis-2004344997-input-number-vlAliqPis-2004344997-input-number-vlImpostoPis-2004344997-input-number-vlBaseCofins-2004344997-input-number-vlAliqCofins-2004344997-input-number-vlImpostoCofins-2004344997-input-number-vlTributavelDifa-2004344997-input-number-aliqDifa-2004344997-input-number-vlDifa-2004344997-input-number-vlBaseIi-2004344997-input-number-aliqIi-2004344997-input-number-vlIi-2004344997-input-number-vlBasePisSt-2004344997-input-number-aliqPisSt-2004344997-input-number-vlPisSt-2004344997-input-number-vlBaseCofinsSt-2004344997-input-number-aliqCofinsSt-2004344997-input-number-vlCofinsSt-2004344997-input-number-vlBaseIcmsEfetivo-2004344997-input-number-aliqIcmsEfetivo-2004344997-input-number-vlIcmsEfetivo-2004344997-input-number-vlBaseIcmsFcp-2004344997-input-number-aliqIcmsFcp-2004344997-input-number-vlIcmsFcp-2004344997-input-number-vlBaseIcmsFcpst-2004344997-input-number-aliqIcmsFcpst-2004344997-input-number-vlIcmsFcpst-2004344997-input-number-vlBaseIcmsPartRem-2004344997-input-number-aliqIcmsPartRem-2004344997-input-number-vlIcmsPartRem-2004344997-input-number-vlBaseIcmsPartDest-2004344997-input-number-vlIcmsPartDest-2004344997-input-number-aliqAntecipIcms-2004344997-input-number-vlAntecipIcms`];
    cy.visit('http://system-A8/simulacoes-cenarios/cenario-teste/FVR_05/caso-teste-dof/226/caso-teste-dof-result/2846/itens');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="744241860-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-idfNum"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-stcCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-stpCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-staCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-stnCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2004344997-powerselect-enqIpiCodigo"] input`);
    cy.fillInput(`[data-cy="2004344997-input-modBaseIcmsCodigo"] textarea`, `port`);
    cy.fillInput(`[data-cy="2004344997-input-modBaseIcmsStCodigo"] textarea`, `reboot`);
    cy.fillInput(`[data-cy="2004344997-input-codBeneficioFiscal"] textarea`, `withdrawal`);
    cy.fillInput(`[data-cy="2004344997-input-number-qtd"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoUnitario"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoTotal"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlAjustePrecoTotal"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlFaturado"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlContabil"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoNetUnit"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-precoNetUnitPisCofins"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioAjustePreco"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioFrete"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioSeguro"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlRateioOda"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-percIcmsPartRem"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-percIcmsPartDest"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-percTributavelIcms"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-percTributavelIpi"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-percTributavelStf"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-percDiferimentoIcms"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsOperacao"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsDiferido"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIsentoIcms"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlOutrosIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlOutrosAbat"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIsentoIpi"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlOutrosIpi"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsStRecup"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsSimplesNac"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIof"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsPartDest"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-custoAdicao"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-custoReducao"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoIpi"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoStf"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoAntecipIcms"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoIcmsDescL"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoPis"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCustoCofins"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcms"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelIcms"] textarea`, `4`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcms"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcms"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIpi"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelIpi"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIpi"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIpi"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseStf"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelStf"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqStf"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlStf"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseStfFronteira"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlStfFronteira"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseStfIdo"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlStfIdo"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseStt"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelStt"] textarea`, `4`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqStt"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlStt"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcmsDescL"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelIcmsDescL"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsDescL"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsDescL"] textarea`, `4`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseSuframa"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelSuframa"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlSuframa"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBasePis"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlAliqPis"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlImpostoPis"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseCofins"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlAliqCofins"] textarea`, `4`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlImpostoCofins"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlTributavelDifa"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqDifa"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlDifa"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIi"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIi"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIi"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBasePisSt"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqPisSt"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlPisSt"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseCofinsSt"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqCofinsSt"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlCofinsSt"] textarea`, `7`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcmsEfetivo"] textarea`, `4`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsEfetivo"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsEfetivo"] textarea`, `3`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcmsFcp"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsFcp"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsFcp"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcmsFcpst"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsFcpst"] textarea`, `9`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsFcpst"] textarea`, `8`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcmsPartRem"] textarea`, `10`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqIcmsPartRem"] textarea`, `2`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsPartRem"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlBaseIcmsPartDest"] textarea`, `1`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlIcmsPartDest"] textarea`, `6`);
    cy.fillInput(`[data-cy="2004344997-input-number-aliqAntecipIcms"] textarea`, `5`);
    cy.fillInput(`[data-cy="2004344997-input-number-vlAntecipIcms"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
