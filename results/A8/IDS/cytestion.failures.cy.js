describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values inteligencia-calculo->inteligencia-calculo/parametrizacoes-avancadas->inteligencia-calculo/parametrizacoes-avancadas/processos->3203512414-unorderedlistoutlined->3584634889-eyeoutlined->902838972-novo->103581300-powerselect-ordem-103581300-input-comentario-103581300-textarea-codigoVigente and submit`, () => {
    cy.clickCauseExist(`[data-cy="inteligencia-calculo"]`);
    cy.clickCauseExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas"]`);
    cy.clickCauseExist(`[data-cy="inteligencia-calculo/parametrizacoes-avancadas/processos"]`);
    cy.clickCauseExist(`[data-cy="3203512414-unorderedlistoutlined"]`);
    cy.clickCauseExist(`[data-cy="3584634889-eyeoutlined"]`);
    cy.clickCauseExist(`[data-cy="902838972-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="103581300-powerselect-ordem"] input`);
    cy.fillInput(`[data-cy="103581300-input-comentario"] textarea`, `indexing`);
    cy.fillInput(`[data-cy="103581300-textarea-codigoVigente"] input`, `Incredible Frozen Chicken`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
