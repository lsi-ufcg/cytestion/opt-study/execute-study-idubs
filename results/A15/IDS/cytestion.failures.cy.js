describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined->919376436-pesquisar pfj`, () => {
    cy.clickCauseExist(`[data-cy="operacoes-controladas"]`);
    cy.clickCauseExist(`[data-cy="criacao-documento-baixa"]`);
    cy.clickCauseExist(`[data-cy="1332631635-unorderedlistoutlined"]`);
    cy.clickCauseExist(`[data-cy="919376436-pesquisar pfj"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined->919376436-powerselect-remetentePfjCodigo-919376436-powerselect-destinatarioLocCodigo-919376436-powerselect-edofCodigo-919376436-input-serieSubserie-919376436-input-numero-919376436-powerselect-cfopCodigo-919376436-powerselect-nopCodigo and submit`, () => {
    cy.clickCauseExist(`[data-cy="operacoes-controladas"]`);
    cy.clickCauseExist(`[data-cy="criacao-documento-baixa"]`);
    cy.clickCauseExist(`[data-cy="1332631635-unorderedlistoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-remetentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-edofCodigo"] input`);
    cy.fillInput(`[data-cy="919376436-input-serieSubserie"] textarea`, `override`);
    cy.fillInput(`[data-cy="919376436-input-numero"] textarea`, `orchestrate`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-nopCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined->3349778643-pesquisar pfj`, () => {
    cy.clickCauseExist(`[data-cy="operacoes-controladas"]`);
    cy.clickCauseExist(`[data-cy="transferencia-controle"]`);
    cy.clickCauseExist(`[data-cy="4226799730-unorderedlistoutlined"]`);
    cy.clickCauseExist(`[data-cy="3349778643-pesquisar pfj"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined->3349778643-powerselect-remetentePfjCodigo-3349778643-powerselect-destinatarioLocCodigo-3349778643-powerselect-edofCodigo-3349778643-input-serieSubserie-3349778643-input-numero-3349778643-powerselect-cfopCodigo-3349778643-powerselect-nopCodigo and submit`, () => {
    cy.clickCauseExist(`[data-cy="operacoes-controladas"]`);
    cy.clickCauseExist(`[data-cy="transferencia-controle"]`);
    cy.clickCauseExist(`[data-cy="4226799730-unorderedlistoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-remetentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-edofCodigo"] input`);
    cy.fillInput(`[data-cy="3349778643-input-serieSubserie"] textarea`, `Bedfordshire`);
    cy.fillInput(`[data-cy="3349778643-input-numero"] textarea`, `unleash`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-nopCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
