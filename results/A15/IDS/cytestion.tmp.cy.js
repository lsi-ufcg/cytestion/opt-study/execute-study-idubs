describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas`, () => {
const actualId = [`root`,`tabela-corporativas`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas`, () => {
const actualId = [`root`,`operacoes-controladas`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros`, () => {
const actualId = [`root`,`parametros`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas->tabelas-corporativo/parametros-gerais`, () => {
const actualId = [`root`,`tabela-corporativas`,`tabelas-corporativo/parametros-gerais`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/parametros-gerais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas->tabelas-corporativo/pfj`, () => {
const actualId = [`root`,`tabela-corporativas`,`tabelas-corporativo/pfj`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas->tabelas-corporativo/hierarquia-pfj-cnpj`, () => {
const actualId = [`root`,`tabela-corporativas`,`tabelas-corporativo/hierarquia-pfj-cnpj`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/hierarquia-pfj-cnpj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas->tabelas-corporativo/cadastro-mercadorias`, () => {
const actualId = [`root`,`tabela-corporativas`,`tabelas-corporativo/cadastro-mercadorias`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastro-mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas->tabelas-corporativas/pfjmer`, () => {
const actualId = [`root`,`tabela-corporativas`,`tabelas-corporativas/pfjmer`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfjmer"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabela-corporativas->tabelas-corporativo/estruturas-mercadorias`, () => {
const actualId = [`root`,`tabela-corporativas`,`tabelas-corporativo/estruturas-mercadorias`];
    cy.clickIfExist(`[data-cy="tabela-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/estruturas-mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/documentos-fiscais`, () => {
const actualId = [`root`,`transacoes`,`transacoes/documentos-fiscais`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/documentos-fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->documento-controlado`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->criacao-documento-baixa`, () => {
const actualId = [`root`,`operacoes-controladas`,`criacao-documento-baixa`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="criacao-documento-baixa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->transferencia-controle`, () => {
const actualId = [`root`,`operacoes-controladas`,`transferencia-controle`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="transferencia-controle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/regras-geracao-documentos`, () => {
const actualId = [`root`,`parametros`,`parametros/regras-geracao-documentos`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/regras-geracao-documentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/documentos-controlados-baixa`, () => {
const actualId = [`root`,`relatorios`,`relatorios/documentos-controlados-baixa`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/documentos-controlados-baixa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1646856438-power-search-button`, () => {
const actualId = [`root`,`download`,`1646856438-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1646856438-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1646856438-download`, () => {
const actualId = [`root`,`download`,`1646856438-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1646856438-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1646856438-detalhes`, () => {
const actualId = [`root`,`download`,`1646856438-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1646856438-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1646856438-excluir`, () => {
const actualId = [`root`,`download`,`1646856438-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1646856438-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->documento-controlado->2931484940-novo documento`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`,`2931484940-novo documento`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.clickIfExist(`[data-cy="2931484940-novo documento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->documento-controlado->2931484940-power-search-button`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`,`2931484940-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.clickIfExist(`[data-cy="2931484940-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->criacao-documento-baixa->1332631635-power-search-button`, () => {
const actualId = [`root`,`operacoes-controladas`,`criacao-documento-baixa`,`1332631635-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="criacao-documento-baixa"]`);
      cy.clickIfExist(`[data-cy="1332631635-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined`, () => {
const actualId = [`root`,`operacoes-controladas`,`criacao-documento-baixa`,`1332631635-unorderedlistoutlined`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="criacao-documento-baixa"]`);
      cy.clickIfExist(`[data-cy="1332631635-unorderedlistoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->transferencia-controle->4226799730-power-search-button`, () => {
const actualId = [`root`,`operacoes-controladas`,`transferencia-controle`,`4226799730-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="transferencia-controle"]`);
      cy.clickIfExist(`[data-cy="4226799730-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined`, () => {
const actualId = [`root`,`operacoes-controladas`,`transferencia-controle`,`4226799730-unorderedlistoutlined`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="transferencia-controle"]`);
      cy.clickIfExist(`[data-cy="4226799730-unorderedlistoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-novo`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-novo`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-power-search-button`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-power-search-button`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-eyeoutlined`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-eyeoutlined`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-deleteoutlined`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-deleteoutlined`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/regras-geracao-documentos->4130768677-plusoutlined`, () => {
const actualId = [`root`,`parametros`,`parametros/regras-geracao-documentos`,`4130768677-plusoutlined`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/regras-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="4130768677-plusoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/regras-geracao-documentos->4130768677-power-search-button`, () => {
const actualId = [`root`,`parametros`,`parametros/regras-geracao-documentos`,`4130768677-power-search-button`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/regras-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="4130768677-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-executar`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-visualização`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-regerar`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-excluir`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-executar`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-visualização`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-regerar`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-excluir`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-executar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-visualização`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-executar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-visualização`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-executar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-visualização`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/documentos-controlados-baixa->4248471160-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/documentos-controlados-baixa`,`4248471160-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="4248471160-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/documentos-controlados-baixa->4248471160-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/documentos-controlados-baixa`,`4248471160-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="4248471160-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/documentos-controlados-baixa->4248471160-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/documentos-controlados-baixa`,`4248471160-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/documentos-controlados-baixa"]`);
      cy.fillInputPowerSearch(`[data-cy="4248471160-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->documento-controlado->2931484940-novo documento->636673885-mais ações`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`,`2931484940-novo documento`,`636673885-mais ações`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.clickIfExist(`[data-cy="2931484940-novo documento"]`);
      cy.clickIfExist(`[data-cy="636673885-mais ações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->documento-controlado->2931484940-novo documento->636673885-salvar`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`,`2931484940-novo documento`,`636673885-salvar`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.clickIfExist(`[data-cy="2931484940-novo documento"]`);
      cy.clickIfExist(`[data-cy="636673885-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->documento-controlado->2931484940-novo documento->636673885-voltar`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`,`2931484940-novo documento`,`636673885-voltar`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.clickIfExist(`[data-cy="2931484940-novo documento"]`);
      cy.clickIfExist(`[data-cy="636673885-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values operacoes-controladas->documento-controlado->2931484940-novo documento->636673885-powerselect-opcCodigo-636673885-powerselect-ctrlTipo-636673885-powerselect-edofCodigo-636673885-input-serieSubserie-636673885-input-numero-636673885-powerselect-indEntradaSaida-636673885-powerselect-pfjCodigoEmitente-636673885-powerselect-pfjCodigoRemetente-636673885-powerselect-pfjCodigoDestinatario-636673885-powerselect-cfopCodigo-636673885-powerselect-nopCodigo and submit`, () => {
const actualId = [`root`,`operacoes-controladas`,`documento-controlado`,`2931484940-novo documento`,`636673885-powerselect-opcCodigo-636673885-powerselect-ctrlTipo-636673885-powerselect-edofCodigo-636673885-input-serieSubserie-636673885-input-numero-636673885-powerselect-indEntradaSaida-636673885-powerselect-pfjCodigoEmitente-636673885-powerselect-pfjCodigoRemetente-636673885-powerselect-pfjCodigoDestinatario-636673885-powerselect-cfopCodigo-636673885-powerselect-nopCodigo`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="documento-controlado"]`);
      cy.clickIfExist(`[data-cy="2931484940-novo documento"]`);
      cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-opcCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-ctrlTipo"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="636673885-input-serieSubserie"] textarea`, `wireless`);
cy.fillInput(`[data-cy="636673885-input-numero"] textarea`, `Incredible Steel Sausages`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-indEntradaSaida"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-pfjCodigoEmitente"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-pfjCodigoRemetente"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-pfjCodigoDestinatario"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="636673885-powerselect-nopCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined->919376436-pesquisar pfj`, () => {
const actualId = [`root`,`operacoes-controladas`,`criacao-documento-baixa`,`1332631635-unorderedlistoutlined`,`919376436-pesquisar pfj`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="criacao-documento-baixa"]`);
      cy.clickIfExist(`[data-cy="1332631635-unorderedlistoutlined"]`);
      cy.clickIfExist(`[data-cy="919376436-pesquisar pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined->919376436-powerselect-remetentePfjCodigo-919376436-powerselect-destinatarioLocCodigo-919376436-powerselect-edofCodigo-919376436-input-serieSubserie-919376436-input-numero-919376436-powerselect-cfopCodigo-919376436-powerselect-nopCodigo and submit`, () => {
const actualId = [`root`,`operacoes-controladas`,`criacao-documento-baixa`,`1332631635-unorderedlistoutlined`,`919376436-powerselect-remetentePfjCodigo-919376436-powerselect-destinatarioLocCodigo-919376436-powerselect-edofCodigo-919376436-input-serieSubserie-919376436-input-numero-919376436-powerselect-cfopCodigo-919376436-powerselect-nopCodigo`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="criacao-documento-baixa"]`);
      cy.clickIfExist(`[data-cy="1332631635-unorderedlistoutlined"]`);
      cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-remetentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-destinatarioLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="919376436-input-serieSubserie"] textarea`, `override`);
cy.fillInput(`[data-cy="919376436-input-numero"] textarea`, `orchestrate`);
cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-nopCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined->3349778643-pesquisar pfj`, () => {
const actualId = [`root`,`operacoes-controladas`,`transferencia-controle`,`4226799730-unorderedlistoutlined`,`3349778643-pesquisar pfj`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="transferencia-controle"]`);
      cy.clickIfExist(`[data-cy="4226799730-unorderedlistoutlined"]`);
      cy.clickIfExist(`[data-cy="3349778643-pesquisar pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined->3349778643-powerselect-remetentePfjCodigo-3349778643-powerselect-destinatarioLocCodigo-3349778643-powerselect-edofCodigo-3349778643-input-serieSubserie-3349778643-input-numero-3349778643-powerselect-cfopCodigo-3349778643-powerselect-nopCodigo and submit`, () => {
const actualId = [`root`,`operacoes-controladas`,`transferencia-controle`,`4226799730-unorderedlistoutlined`,`3349778643-powerselect-remetentePfjCodigo-3349778643-powerselect-destinatarioLocCodigo-3349778643-powerselect-edofCodigo-3349778643-input-serieSubserie-3349778643-input-numero-3349778643-powerselect-cfopCodigo-3349778643-powerselect-nopCodigo`];
    cy.clickIfExist(`[data-cy="operacoes-controladas"]`);
      cy.clickIfExist(`[data-cy="transferencia-controle"]`);
      cy.clickIfExist(`[data-cy="4226799730-unorderedlistoutlined"]`);
      cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-remetentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-destinatarioLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="3349778643-input-serieSubserie"] textarea`, `Bedfordshire`);
cy.fillInput(`[data-cy="3349778643-input-numero"] textarea`, `unleash`);
cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-nopCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-novo->3050307865-salvar`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-novo`,`3050307865-salvar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-novo"]`);
      cy.clickIfExist(`[data-cy="3050307865-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-novo->3050307865-voltar`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-novo`,`3050307865-voltar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-novo"]`);
      cy.clickIfExist(`[data-cy="3050307865-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->parametros/operacao-controlada->3957760464-novo->3050307865-input-opcCodigo-3050307865-input-titulo-3050307865-powerselect-indEsControlado-3050307865-powerselect-indEsBaixa-3050307865-powerselect-ctrlComprovacao-3050307865-checkbox-indUsaHierPfjCnpj-3050307865-checkbox-indUsaPfj and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-novo`,`3050307865-input-opcCodigo-3050307865-input-titulo-3050307865-powerselect-indEsControlado-3050307865-powerselect-indEsBaixa-3050307865-powerselect-ctrlComprovacao-3050307865-checkbox-indUsaHierPfjCnpj-3050307865-checkbox-indUsaPfj`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-novo"]`);
      cy.fillInput(`[data-cy="3050307865-input-opcCodigo"] textarea`, `Forint`);
cy.fillInput(`[data-cy="3050307865-input-titulo"] textarea`, `Suriname`);
cy.fillInputPowerSelect(`[data-cy="3050307865-powerselect-indEsControlado"] input`);
cy.fillInputPowerSelect(`[data-cy="3050307865-powerselect-indEsBaixa"] input`);
cy.fillInputPowerSelect(`[data-cy="3050307865-powerselect-ctrlComprovacao"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="3050307865-checkbox-indUsaHierPfjCnpj"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="3050307865-checkbox-indUsaPfj"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-eyeoutlined->2733898784-remover item`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-eyeoutlined`,`2733898784-remover item`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2733898784-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-eyeoutlined->2733898784-salvar`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-eyeoutlined`,`2733898784-salvar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2733898784-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/operacao-controlada->3957760464-eyeoutlined->2733898784-voltar`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-eyeoutlined`,`2733898784-voltar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2733898784-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->parametros/operacao-controlada->3957760464-eyeoutlined->2733898784-input-titulo-2733898784-powerselect-indEsControlado-2733898784-powerselect-indEsBaixa-2733898784-powerselect-ctrlComprovacao-2733898784-checkbox-indUsaHierPfjCnpj-2733898784-checkbox-indUsaPfj and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/operacao-controlada`,`3957760464-eyeoutlined`,`2733898784-input-titulo-2733898784-powerselect-indEsControlado-2733898784-powerselect-indEsBaixa-2733898784-powerselect-ctrlComprovacao-2733898784-checkbox-indUsaHierPfjCnpj-2733898784-checkbox-indUsaPfj`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/operacao-controlada"]`);
      cy.clickIfExist(`[data-cy="3957760464-eyeoutlined"]`);
      cy.fillInput(`[data-cy="2733898784-input-titulo"] textarea`, `Cambridgeshire`);
cy.fillInputPowerSelect(`[data-cy="2733898784-powerselect-indEsControlado"] input`);
cy.fillInputPowerSelect(`[data-cy="2733898784-powerselect-indEsBaixa"] input`);
cy.fillInputPowerSelect(`[data-cy="2733898784-powerselect-ctrlComprovacao"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="2733898784-checkbox-indUsaHierPfjCnpj"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="2733898784-checkbox-indUsaPfj"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->parametros/regras-geracao-documentos->4130768677-plusoutlined->4130768677-input-titulo and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/regras-geracao-documentos`,`4130768677-plusoutlined`,`4130768677-input-titulo`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/regras-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="4130768677-plusoutlined"]`);
      cy.fillInput(`[data-cy="4130768677-input-titulo"] textarea`, `Produtor`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-executar->3971107661-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-executar`,`3971107661-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-executar"]`);
      cy.clickIfExist(`[data-cy="3971107661-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-executar->3971107661-agendar`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-executar`,`3971107661-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-executar"]`);
      cy.clickIfExist(`[data-cy="3971107661-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-agendamentos->3971107661-voltar`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-agendamentos`,`3971107661-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3971107661-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/documentos-controlados-baixa->3971107661-visualização->3971107661-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-visualização`,`3971107661-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3971107661-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-detalhes->3971107661-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-detalhes`,`3971107661-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-detalhes"]`);
      cy.clickIfExist(`[data-cy="3971107661-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-abrir visualização->3971107661-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-abrir visualização`,`3971107661-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3971107661-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-abrir visualização->3971107661-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-abrir visualização`,`3971107661-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3971107661-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-abrir visualização->3971107661-expandir`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-abrir visualização`,`3971107661-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3971107661-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/documentos-controlados-baixa->3971107661-abrir visualização->3971107661-download`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-abrir visualização`,`3971107661-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3971107661-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-executar->653385554-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-executar`,`653385554-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-executar"]`);
      cy.clickIfExist(`[data-cy="653385554-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-executar->653385554-agendar`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-executar`,`653385554-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-executar"]`);
      cy.clickIfExist(`[data-cy="653385554-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-agendamentos->653385554-voltar`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-agendamentos`,`653385554-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-agendamentos"]`);
      cy.clickIfExist(`[data-cy="653385554-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/comprovacao-automatica-documentos->653385554-visualização->653385554-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-visualização`,`653385554-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="653385554-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-detalhes->653385554-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-detalhes`,`653385554-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-detalhes"]`);
      cy.clickIfExist(`[data-cy="653385554-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-abrir visualização->653385554-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-abrir visualização`,`653385554-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="653385554-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-abrir visualização->653385554-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-abrir visualização`,`653385554-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="653385554-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-abrir visualização->653385554-expandir`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-abrir visualização`,`653385554-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="653385554-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-abrir visualização->653385554-download`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-abrir visualização`,`653385554-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="653385554-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-executar->2571081180-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-executar`,`2571081180-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-executar"]`);
      cy.clickIfExist(`[data-cy="2571081180-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-executar->2571081180-agendar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-executar`,`2571081180-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-executar"]`);
      cy.clickIfExist(`[data-cy="2571081180-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-geracao-documentos->2571081180-agendamentos->2571081180-voltar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-agendamentos`,`2571081180-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/estorno-geracao-documentos->2571081180-visualização->2571081180-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-visualização`,`2571081180-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2571081180-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-executar->4002148877-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-executar`,`4002148877-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-executar"]`);
      cy.clickIfExist(`[data-cy="4002148877-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-executar->4002148877-agendar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-executar`,`4002148877-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-executar"]`);
      cy.clickIfExist(`[data-cy="4002148877-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-agendamentos->4002148877-voltar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-agendamentos`,`4002148877-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4002148877-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/estorno-comprovacao-documento->4002148877-visualização->4002148877-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-visualização`,`4002148877-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4002148877-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-executar->3230736957-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-executar`,`3230736957-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-executar"]`);
      cy.clickIfExist(`[data-cy="3230736957-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-executar->3230736957-agendar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-executar`,`3230736957-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-executar"]`);
      cy.clickIfExist(`[data-cy="3230736957-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicadores->3230736957-agendamentos->3230736957-voltar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-agendamentos`,`3230736957-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3230736957-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/atualizacao-indicadores->3230736957-visualização->3230736957-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-visualização`,`3230736957-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3230736957-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-executar->2152697188-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-executar`,`2152697188-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-executar"]`);
      cy.clickIfExist(`[data-cy="2152697188-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-executar->2152697188-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-executar`,`2152697188-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-executar"]`);
      cy.clickIfExist(`[data-cy="2152697188-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/carta-confirmacao-saldo->2152697188-executar->2152697188-input-P_RESPONSAVEL-2152697188-input-P_PRAZO_RESPOSTA and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-executar`,`2152697188-input-P_RESPONSAVEL-2152697188-input-P_PRAZO_RESPOSTA`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-executar"]`);
      cy.fillInput(`[data-cy="2152697188-input-P_RESPONSAVEL"] textarea`, `Soap`);
cy.fillInput(`[data-cy="2152697188-input-P_PRAZO_RESPOSTA"] textarea`, `calculating`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-agendamentos->2152697188-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-agendamentos`,`2152697188-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2152697188-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/carta-confirmacao-saldo->2152697188-visualização->2152697188-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-visualização`,`2152697188-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2152697188-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/documentos-controlados-baixa->4248471160-visualização->4248471160-salvar configuração`, () => {
const actualId = [`root`,`relatorios`,`relatorios/documentos-controlados-baixa`,`4248471160-visualização`,`4248471160-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="4248471160-visualização"]`);
      cy.clickIfExist(`[data-cy="4248471160-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-executar->2972145925-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-executar`,`2972145925-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-executar"]`);
      cy.clickIfExist(`[data-cy="2972145925-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-executar->2972145925-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-executar`,`2972145925-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-executar"]`);
      cy.clickIfExist(`[data-cy="2972145925-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-executar->2972145925-input-P_SERIE_SUBS-2972145925-input-P_NUMERO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-executar`,`2972145925-input-P_SERIE_SUBS-2972145925-input-P_NUMERO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-executar"]`);
      cy.fillInput(`[data-cy="2972145925-input-P_SERIE_SUBS"] textarea`, `Licensed`);
cy.fillInput(`[data-cy="2972145925-input-P_NUMERO"] textarea`, `withdrawal`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-agendamentos->2972145925-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-agendamentos`,`2972145925-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-executar->4092472697-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-executar`,`4092472697-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-executar"]`);
      cy.clickIfExist(`[data-cy="4092472697-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-executar->4092472697-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-executar`,`4092472697-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-executar"]`);
      cy.clickIfExist(`[data-cy="4092472697-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lista-componentes-disponiveis->4092472697-executar->4092472697-input-P_QTD and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-executar`,`4092472697-input-P_QTD`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-executar"]`);
      cy.fillInput(`[data-cy="4092472697-input-P_QTD"] textarea`, `enhance`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-agendamentos->4092472697-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-agendamentos`,`4092472697-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4092472697-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lista-componentes-disponiveis->4092472697-visualização->4092472697-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-visualização`,`4092472697-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4092472697-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-executar->506385384-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-executar`,`506385384-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-executar"]`);
      cy.clickIfExist(`[data-cy="506385384-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-executar->506385384-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-executar`,`506385384-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-executar"]`);
      cy.clickIfExist(`[data-cy="506385384-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-agendamentos->506385384-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-agendamentos`,`506385384-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-agendamentos"]`);
      cy.clickIfExist(`[data-cy="506385384-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/relacao-movimento-produto->506385384-visualização->506385384-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-visualização`,`506385384-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="506385384-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/documentos-controlados-baixa->3971107661-executar->3971107661-múltipla seleção->3971107661-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-executar`,`3971107661-múltipla seleção`,`3971107661-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-executar"]`);
      cy.clickIfExist(`[data-cy="3971107661-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3971107661-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/documentos-controlados-baixa->3971107661-abrir visualização->3971107661-expandir->3971107661-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/documentos-controlados-baixa`,`3971107661-abrir visualização`,`3971107661-expandir`,`3971107661-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/documentos-controlados-baixa"]`);
      cy.clickIfExist(`[data-cy="3971107661-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3971107661-expandir"]`);
      cy.clickIfExist(`[data-cy="3971107661-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-executar->653385554-múltipla seleção->653385554-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-executar`,`653385554-múltipla seleção`,`653385554-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-executar"]`);
      cy.clickIfExist(`[data-cy="653385554-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="653385554-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-abrir visualização->653385554-expandir->653385554-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/comprovacao-automatica-documentos`,`653385554-abrir visualização`,`653385554-expandir`,`653385554-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/comprovacao-automatica-documentos"]`);
      cy.clickIfExist(`[data-cy="653385554-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="653385554-expandir"]`);
      cy.clickIfExist(`[data-cy="653385554-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/estorno-geracao-documentos->2571081180-executar->2571081180-múltipla seleção->2571081180-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-geracao-documentos`,`2571081180-executar`,`2571081180-múltipla seleção`,`2571081180-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-geracao-documentos"]`);
      cy.clickIfExist(`[data-cy="2571081180-executar"]`);
      cy.clickIfExist(`[data-cy="2571081180-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2571081180-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-executar->4002148877-múltipla seleção->4002148877-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/estorno-comprovacao-documento`,`4002148877-executar`,`4002148877-múltipla seleção`,`4002148877-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/estorno-comprovacao-documento"]`);
      cy.clickIfExist(`[data-cy="4002148877-executar"]`);
      cy.clickIfExist(`[data-cy="4002148877-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4002148877-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/atualizacao-indicadores->3230736957-executar->3230736957-múltipla seleção->3230736957-próximo`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-executar`,`3230736957-múltipla seleção`,`3230736957-próximo`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-executar"]`);
      cy.clickIfExist(`[data-cy="3230736957-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3230736957-próximo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/atualizacao-indicadores->3230736957-executar->3230736957-múltipla seleção->3230736957-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicadores`,`3230736957-executar`,`3230736957-múltipla seleção`,`3230736957-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicadores"]`);
      cy.clickIfExist(`[data-cy="3230736957-executar"]`);
      cy.clickIfExist(`[data-cy="3230736957-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3230736957-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-executar->2152697188-múltipla seleção->2152697188-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/carta-confirmacao-saldo`,`2152697188-executar`,`2152697188-múltipla seleção`,`2152697188-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/carta-confirmacao-saldo"]`);
      cy.clickIfExist(`[data-cy="2152697188-executar"]`);
      cy.clickIfExist(`[data-cy="2152697188-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2152697188-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-executar->2972145925-múltipla seleção->2972145925-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/movimentacao-consolidada-documentos`,`2972145925-executar`,`2972145925-múltipla seleção`,`2972145925-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/movimentacao-consolidada-documentos"]`);
      cy.clickIfExist(`[data-cy="2972145925-executar"]`);
      cy.clickIfExist(`[data-cy="2972145925-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2972145925-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-executar->4092472697-múltipla seleção->4092472697-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lista-componentes-disponiveis`,`4092472697-executar`,`4092472697-múltipla seleção`,`4092472697-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lista-componentes-disponiveis"]`);
      cy.clickIfExist(`[data-cy="4092472697-executar"]`);
      cy.clickIfExist(`[data-cy="4092472697-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4092472697-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-executar->506385384-múltipla seleção->506385384-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/relacao-movimento-produto`,`506385384-executar`,`506385384-múltipla seleção`,`506385384-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/relacao-movimento-produto"]`);
      cy.clickIfExist(`[data-cy="506385384-executar"]`);
      cy.clickIfExist(`[data-cy="506385384-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="506385384-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
