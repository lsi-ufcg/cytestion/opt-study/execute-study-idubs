describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined->919376436-pesquisar pfj`, () => {
    cy.visit('http://system-A15/documentos-controlados/criacao-documento-baixa/5005506/1/AAA/transferencia');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="919376436-pesquisar pfj"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values operacoes-controladas->criacao-documento-baixa->1332631635-unorderedlistoutlined->919376436-powerselect-remetentePfjCodigo-919376436-powerselect-destinatarioLocCodigo-919376436-powerselect-edofCodigo-919376436-input-serieSubserie-919376436-input-numero-919376436-powerselect-cfopCodigo-919376436-powerselect-nopCodigo and submit`, () => {
    cy.visit('http://system-A15/documentos-controlados/criacao-documento-baixa?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&opcCodigo=~eq~AAA%7C%7CAAA');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1332631635-unorderedlistoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-remetentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-edofCodigo"] input`);
    cy.fillInput(`[data-cy="919376436-input-serieSubserie"] textarea`, `Face to face`);
    cy.fillInput(`[data-cy="919376436-input-numero"] textarea`, `Frozen`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="919376436-powerselect-nopCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined->3349778643-pesquisar pfj`, () => {
    cy.visit('http://system-A15/documentos-controlados/transferencia-controle/5005507/1/AAA/transferencia');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3349778643-pesquisar pfj"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values operacoes-controladas->transferencia-controle->4226799730-unorderedlistoutlined->3349778643-powerselect-remetentePfjCodigo-3349778643-powerselect-destinatarioLocCodigo-3349778643-powerselect-edofCodigo-3349778643-input-serieSubserie-3349778643-input-numero-3349778643-powerselect-cfopCodigo-3349778643-powerselect-nopCodigo and submit`, () => {
    cy.visit('http://system-A15/documentos-controlados/transferencia-controle?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&opcCodigo=~eq~AAA%7C%7CAAA');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4226799730-unorderedlistoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-remetentePfjCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-destinatarioLocCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-edofCodigo"] input`);
    cy.fillInput(`[data-cy="3349778643-input-serieSubserie"] textarea`, `withdrawal`);
    cy.fillInput(`[data-cy="3349778643-input-numero"] textarea`, `Central`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3349778643-powerselect-nopCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/documentos-controlados-baixa->3971107661-agendamentos->3971107661-voltar`, () => {
    cy.visit('http://system-A15/processos/documentos-controlados-baixa?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~219549D%7C%7C219549&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3971107661-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/comprovacao-automatica-documentos->653385554-agendamentos->653385554-voltar`, () => {
    cy.visit('http://system-A15/processos/comprovacao-automatica-documentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~219560D%7C%7C219560&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="653385554-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/estorno-geracao-documentos->2571081180-agendamentos->2571081180-voltar`, () => {
    cy.visit('http://system-A15/processos/estorno-geracao-documentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~221448D%7C%7C221448&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2571081180-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/estorno-comprovacao-documento->4002148877-agendamentos->4002148877-voltar`, () => {
    cy.visit('http://system-A15/processos/estorno-comprovacao-documento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~219565D%7C%7C219565&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4002148877-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/atualizacao-indicadores->3230736957-agendamentos->3230736957-voltar`, () => {
    cy.visit('http://system-A15/processos/atualizacao-indicadores?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~240475D%7C%7C240475&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3230736957-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/carta-confirmacao-saldo->2152697188-agendamentos->2152697188-voltar`, () => {
    cy.visit('http://system-A15/relatorios/carta-confirmacao-saldo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~219501D%7C%7C219501&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2152697188-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/movimentacao-consolidada-documentos->2972145925-agendamentos->2972145925-voltar`, () => {
    cy.visit(
      'http://system-A15/relatorios/movimentacao-consolidada-documentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~235914D%7C%7C235914&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2972145925-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/lista-componentes-disponiveis->4092472697-agendamentos->4092472697-voltar`, () => {
    cy.visit('http://system-A15/relatorios/lista-componentes-disponiveis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~229474D%7C%7C229474&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4092472697-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/relacao-movimento-produto->506385384-agendamentos->506385384-voltar`, () => {
    cy.visit('http://system-A15/relatorios/relacao-movimento-produto?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~229295D%7C%7C229295&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="506385384-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
