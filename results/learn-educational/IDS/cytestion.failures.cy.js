describe('Cytestion', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Click on element index-about->about-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-about"]`);
    cy.clickCauseExist(`[data-cy="about-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-courses->courses-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-courses"]`);
    cy.clickCauseExist(`[data-cy="courses-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-portfolio->portfolio-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-portfolio"]`);
    cy.clickCauseExist(`[data-cy="portfolio-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-contact->contact-webthemez`, () => {
    cy.clickCauseExist(`[data-cy="index-contact"]`);
    cy.clickCauseExist(`[data-cy="contact-webthemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-about->about-navbar-brand->index-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-about"]`);
    cy.clickCauseExist(`[data-cy="about-navbar-brand"]`);
    cy.clickCauseExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
});
