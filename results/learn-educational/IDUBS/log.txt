yarn run v1.22.19
$ bash collectCPU.sh > executionMetrics.txt & NODE_ENV=generation EXECUTION_MODE=development node --max-old-space-size=12288 entrypoint.js
EXECUTION_MODE=development
NODE_ENV=generation
BASE_URL=http://localhost:9998/
BASE_URL_API=http://localhost:9998/
USER_LOGIN=undefined
USER_PASSWORD=undefined
E2E_FOLDER=../e2e/exploratory/learn-educational
ERROR_MESSAGE=Ocorreu um erro,ACESSO NEGADO,ERRO INESPERADO,Access is denied,Acesso Negado
CHECK_400=true
CHECK_500=true
ALLOWABLE_CODE_ERROR=422
[1;92mWelcome to Cytestion![0;0m Version: 1.7. A tool for automated GUI testing of web applications, using the cypress framework
[1;92mINFO:[0;0m Verifying existence of cypress config file and set environment variables
[1;92mINFO:[0;0m Checks if the tests have already been generated, inform it will be replaced.
[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 1
[1;92mINFO:[0;0m Total test cases without skip: 1
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 1
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/learn-educational/tmp/batches/**/*.cy.js'
It looks like this is your first time using Cypress: 13.6.2

[STARTED] Task without title.
[TITLE]  Verified Cypress!       /home/lsi/.cache/Cypress/13.6.2/Cypress
[SUCCESS]  Verified Cypress!       /home/lsi/.cache/Cypress/13.6.2/Cypress

Opening Cypress...

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/open-sources/cytestion-utilization/e2e/exploratory/learn-educati │
  │                 onal/tmp/batches/**/*.cy.js                                                    │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Visits index page (4079ms)


  1 passing (4s)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        1                                                                                │
  │ Passing:      1                                                                                │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     4 seconds                                                                        │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔  cytestion_1.cy.js                        00:04        1        1        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✔  All specs passed!                        00:04        1        1        -        -        -  

[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 33
[1;92mINFO:[0;0m Total test cases without skip: 32
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 32
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/learn-educational/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/open-sources/cytestion-utilization/e2e/exploratory/learn-educati │
  │                 onal/tmp/batches/**/*.cy.js                                                    │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Click on element index-navbar-toggle (4839ms)
    ✓ Click on element index-navbar-brand (6651ms)
    ✓ Click on element index-index (4409ms)
    ✓ Click on element index-about (4447ms)
    ✓ Click on element index-courses (4402ms)
    ✓ Click on element index-fees (4397ms)
    ✓ Click on element index-portfolio (4558ms)
    ✓ Click on element index-dropdown-toggle (4397ms)
    ✓ Click on element index-right-sidebar (4129ms)
    ✓ Click on element index-dummy-link-1 (4135ms)
    ✓ Click on element index-dummy-link-2 (4127ms)
    ✓ Click on element index-dummy-link-3 (4126ms)
    ✓ Click on element index-contact (4417ms)
    ✓ Click on element index-read-more (4406ms)
    ✓ Click on element index-text-1 (4415ms)
    ✓ Click on element index-text-2 (4404ms)
    ✓ Click on element index-text-3 (4398ms)
    ✓ Click on element index-text-4 (4402ms)
    ✓ Click on element index-text-5 (4408ms)
    ✓ Click on element index-text-6 (4391ms)
    ✓ Click on element index-twitter (4405ms)
    ✓ Click on element index-facebook (4407ms)
    ✓ Click on element index-dribbble (4394ms)
    ✓ Click on element index-flickr (4401ms)
    ✓ Click on element index-github (4394ms)
    ✓ Click on element index-index-footer (4392ms)
    ✓ Click on element index-about-footer (4401ms)
    ✓ Click on element index-courses-footer (4393ms)
    ✓ Click on element index-fees-footer (4396ms)
    ✓ Click on element index-portfolio-footer (4395ms)
    ✓ Click on element index-contact-footer (4398ms)
    1) Click on element index-WebThemez


  31 passing (3m)
  1 failing

  1) Cytestion
       Click on element index-WebThemez:
     AssertionError: Timed out retrying after 5000ms: Check requests with client error: expected 404 to satisfy [Function]
      at Context.eval (webpack://cytestion/./cypress/support/commands.js:301:80)




  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        32                                                                               │
  │ Passing:      31                                                                               │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     2 minutes, 36 seconds                                                            │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  /home/lsi/tcc/open-sources/cytestion-utilization/cytestion/cypress/screenshots/c     (1280x720)
     ytestion_1.cy.js/Cytestion -- Click on element index-WebThemez (failed).png                    


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✖  cytestion_1.cy.js                        02:36       32       31        1        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✖  1 of 1 failed (100%)                     02:36       32       31        1        -        -  

info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 187
[1;92mINFO:[0;0m Total test cases without skip: 154
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 50
[1;92mINFO:[0;0m Batch file created: cytestion_2.cy.js. Total test cases in batch file: 50
[1;92mINFO:[0;0m Batch file created: cytestion_3.cy.js. Total test cases in batch file: 50
[1;92mINFO:[0;0m Batch file created: cytestion_4.cy.js. Total test cases in batch file: 4
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/learn-educational/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          4 found (cytestion_1.cy.js, cytestion_2.cy.js, cytestion_3.cy.js, cytestion_4. │
  │                 cy.js)                                                                         │
  │ Searched:       /home/lsi/tcc/open-sources/cytestion-utilization/e2e/exploratory/learn-educati │
  │                 onal/tmp/batches/**/*.cy.js                                                    │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 4)


  Cytestion
    ✓ Click on element index-about->about-navbar-toggle (5032ms)
    ✓ Click on element index-about->about-navbar-brand (4547ms)
    ✓ Click on element index-about->about-index (4400ms)
    ✓ Click on element index-about->about-about (4418ms)
    ✓ Click on element index-about->about-courses (4412ms)
    ✓ Click on element index-about->about-fees (4411ms)
    ✓ Click on element index-about->about-portfolio (4551ms)
    ✓ Click on element index-about->about-dropdown-toggle (4394ms)
    ✓ Click on element index-about->about-right-sidebar (4133ms)
    ✓ Click on element index-about->about-dummy-link-1 (4122ms)
    ✓ Click on element index-about->about-dummy-link-2 (4120ms)
    ✓ Click on element index-about->about-dummy-link-3 (4120ms)
    ✓ Click on element index-about->about-contact (4403ms)
    ✓ Click on element index-about->about-Responsive (4400ms)
    ✓ Click on element index-about->about-HTML5 (4397ms)
    ✓ Click on element index-about->about-Bootstrap (4399ms)
    ✓ Click on element index-about->about-Clean (4398ms)
    ✓ Click on element index-about->about-Premium (4397ms)
    ✓ Click on element index-about->about-facebook-ceo (4400ms)
    ✓ Click on element index-about->about-google-ceo (4398ms)
    ✓ Click on element index-about->about-twitter-ceo (4392ms)
    ✓ Click on element index-about->about-dribbble-ceo (4393ms)
    ✓ Click on element index-about->about-github-ceo (4409ms)
    ✓ Click on element index-about->about-facebook-director (4400ms)
    ✓ Click on element index-about->about-google-director (4391ms)
    ✓ Click on element index-about->about-twitter-director (4388ms)
    ✓ Click on element index-about->about-dribbble-director (4390ms)
    ✓ Click on element index-about->about-github-director (4395ms)
    ✓ Click on element index-about->about-facebook-manager (4389ms)
    ✓ Click on element index-about->about-google-manager (4392ms)
    ✓ Click on element index-about->about-twitter-manager (4403ms)
    ✓ Click on element index-about->about-dribbble-manager (4402ms)
    ✓ Click on element index-about->about-github-manager (4397ms)
    ✓ Click on element index-about->about-facebook-creative (4394ms)
    ✓ Click on element index-about->about-google-creative (4393ms)
    ✓ Click on element index-about->about-twitter-creative (4405ms)
    ✓ Click on element index-about->about-dribbble-creative (4399ms)
    ✓ Click on element index-about->about-github-creative (4388ms)
    ✓ Click on element index-about->about-twitter (4393ms)
    ✓ Click on element index-about->about-facebook (4389ms)
    ✓ Click on element index-about->about-dribbble (4393ms)
    ✓ Click on element index-about->about-flickr (4391ms)
    ✓ Click on element index-about->about-github (4392ms)
    ✓ Click on element index-about->about-index-footer (4391ms)
    ✓ Click on element index-about->about-about-footer (4394ms)
    ✓ Click on element index-about->about-courses-footer (4392ms)
    ✓ Click on element index-about->about-fees-footer (4404ms)
    ✓ Click on element index-about->about-portfolio-footer (4404ms)
    ✓ Click on element index-about->about-contact-footer (4389ms)
    1) Click on element index-about->about-WebThemez


  49 passing (4m)
  1 failing

  1) Cytestion
       Click on element index-about->about-WebThemez:
     AssertionError: Timed out retrying after 5000ms: Check requests with client error: expected 404 to satisfy [Function]
      at Context.eval (webpack://cytestion/./cypress/support/commands.js:301:80)




  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        50                                                                               │
  │ Passing:      49                                                                               │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     3 minutes, 54 seconds                                                            │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  /home/lsi/tcc/open-sources/cytestion-utilization/cytestion/cypress/screenshots/c     (1280x720)
     ytestion_1.cy.js/Cytestion -- Click on element index-about-about-WebThemez (fail               
     ed).png                                                                                        


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_2.cy.js                                                               (2 of 4)


  Cytestion
    ✓ Click on element index-courses->courses-navbar-toggle (4531ms)
    ✓ Click on element index-courses->courses-navbar-brand (4559ms)
    ✓ Click on element index-courses->courses-index (4396ms)
    ✓ Click on element index-courses->courses-about (4448ms)
    ✓ Click on element index-courses->courses-courses (4400ms)
    ✓ Click on element index-courses->courses-fees (4400ms)
    ✓ Click on element index-courses->courses-portfolio (4536ms)
    ✓ Click on element index-courses->courses-dropdown-toggle (4391ms)
    ✓ Click on element index-courses->courses-right-sidebar (4117ms)
    ✓ Click on element index-courses->courses-dummy-link-1 (4130ms)
    ✓ Click on element index-courses->courses-dummy-link-2 (4109ms)
    ✓ Click on element index-courses->courses-dummy-link-3 (4122ms)
    ✓ Click on element index-courses->courses-contact (4407ms)
    ✓ Click on element index-courses->courses-twitter (4392ms)
    ✓ Click on element index-courses->courses-facebook (4390ms)
    ✓ Click on element index-courses->courses-dribbble (4387ms)
    ✓ Click on element index-courses->courses-flickr (4391ms)
    ✓ Click on element index-courses->courses-github (4390ms)
    ✓ Click on element index-courses->courses-index-footer (4407ms)
    ✓ Click on element index-courses->courses-about-footer (4398ms)
    ✓ Click on element index-courses->courses-courses-footer (4394ms)
    ✓ Click on element index-courses->courses-fees-footer (4410ms)
    ✓ Click on element index-courses->courses-portfolio-footer (4404ms)
    ✓ Click on element index-courses->courses-contact-footer (4393ms)
    1) Click on element index-courses->courses-WebThemez
    ✓ Click on element index-fees->fees-navbar-collapse (4202ms)
    ✓ Click on element index-fees->fees-navbar-brand (4410ms)
    ✓ Click on element index-fees->fees-index (4405ms)
    ✓ Click on element index-fees->fees-about (4405ms)
    ✓ Click on element index-fees->fees-courses (4401ms)
    ✓ Click on element index-fees->fees-fees (4422ms)
    ✓ Click on element index-fees->fees-portfolio (4422ms)
    ✓ Click on element index-fees->fees-dropdown (4399ms)
    ✓ Click on element index-fees->fees-right-sidebar (4137ms)
    ✓ Click on element index-fees->fees-dummy-link-1 (4128ms)
    ✓ Click on element index-fees->fees-dummy-link-2 (4134ms)
    ✓ Click on element index-fees->fees-dummy-link-3 (4135ms)
    ✓ Click on element index-fees->fees-contact (4414ms)
    ✓ Click on element index-fees->fees-signup-basic (4407ms)
    ✓ Click on element index-fees->fees-signup-standard (4404ms)
    ✓ Click on element index-fees->fees-signup-advanced (4414ms)
    ✓ Click on element index-fees->fees-signup-mighty (4410ms)
    ✓ Click on element index-fees->fees-twitter (4399ms)
    ✓ Click on element index-fees->fees-facebook (4414ms)
    ✓ Click on element index-fees->fees-dribbble (4401ms)
    ✓ Click on element index-fees->fees-flickr (4404ms)
    ✓ Click on element index-fees->fees-github (4396ms)
    ✓ Click on element index-fees->fees-WebThemez (12047ms)
    ✓ Click on element index-portfolio->portfolio-navbar-collapse (4187ms)
    ✓ Click on element index-portfolio->sidebar-navbar-brand (4423ms)


  49 passing (4m)
  1 failing

  1) Cytestion
       Click on element index-courses->courses-WebThemez:
     AssertionError: Timed out retrying after 5000ms: Check requests with client error: expected 404 to satisfy [Function]
      at Context.eval (webpack://cytestion/./cypress/support/commands.js:301:80)




  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        50                                                                               │
  │ Passing:      49                                                                               │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     3 minutes, 58 seconds                                                            │
  │ Spec Ran:     cytestion_2.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  /home/lsi/tcc/open-sources/cytestion-utilization/cytestion/cypress/screenshots/c     (1280x720)
     ytestion_2.cy.js/Cytestion -- Click on element index-courses-courses-WebThemez (               
     failed).png                                                                                    


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_3.cy.js                                                               (3 of 4)


  Cytestion
    ✓ Click on element index-portfolio->portfolio-index (5319ms)
    ✓ Click on element index-portfolio->portfolio-about (4471ms)
    ✓ Click on element index-portfolio->portfolio-courses (4404ms)
    ✓ Click on element index-portfolio->portfolio-fees (4408ms)
    ✓ Click on element index-portfolio->portfolio-portfolio (4407ms)
    ✓ Click on element index-portfolio->portfolio-dropdown (4400ms)
    ✓ Click on element index-portfolio->portfolio-right-sidebar (4149ms)
    ✓ Click on element index-portfolio->portfolio-dummy-link-1 (4131ms)
    ✓ Click on element index-portfolio->portfolio-dummy-link-2 (4133ms)
    ✓ Click on element index-portfolio->portfolio-dummy-link-3 (4144ms)
    ✓ Click on element index-portfolio->portfolio-contact (4413ms)
    ✓ Click on element index-portfolio->portfolio-all (4424ms)
    ✓ Click on element index-portfolio->portfolio-web-design (4423ms)
    ✓ Click on element index-portfolio->portfolio-photography (4408ms)
    ✓ Click on element index-portfolio->portfolio-print (4402ms)
    ✓ Click on element index-portfolio->portfolio-twitter (4406ms)
    ✓ Click on element index-portfolio->portfolio-facebook (4403ms)
    ✓ Click on element index-portfolio->portfolio-dribbble (4410ms)
    ✓ Click on element index-portfolio->portfolio-flickr (4402ms)
    ✓ Click on element index-portfolio->portfolio-github (4421ms)
    ✓ Click on element index-portfolio->portfolio-index-footer (4436ms)
    ✓ Click on element index-portfolio->portfolio-about-footer (4437ms)
    ✓ Click on element index-portfolio->portfolio-courses-footer (4414ms)
    ✓ Click on element index-portfolio->portfolio-fees-footer (4414ms)
    ✓ Click on element index-portfolio->portfolio-portfolio-footer (4395ms)
    ✓ Click on element index-portfolio->portfolio-contact-footer (4398ms)
    1) Click on element index-portfolio->portfolio-WebThemez
    ✓ Click on element index-contact->contact-navbar-toggle (4193ms)
    ✓ Click on element index-contact->contact-navbar-brand (4421ms)
    ✓ Click on element index-contact->contact-index (4412ms)
    ✓ Click on element index-contact->contact-about (4401ms)
    ✓ Click on element index-contact->contact-courses (4409ms)
    ✓ Click on element index-contact->contact-fees (4412ms)
    ✓ Click on element index-contact->contact-portfolio (4410ms)
    ✓ Click on element index-contact->contact-dropdown-toggle (4407ms)
    ✓ Click on element index-contact->contact-right-sidebar (4133ms)
    ✓ Click on element index-contact->contact-dummy-link-1 (4132ms)
    ✓ Click on element index-contact->contact-dummy-link-2 (4136ms)
    ✓ Click on element index-contact->contact-dummy-link-3 (4135ms)
    ✓ Click on element index-contact->contact-contact (4408ms)
    ✓ Click on element index-contact->send-message-button (4400ms)
    ✓ Click on element index-contact->courses-twitter (4400ms)
    ✓ Click on element index-contact->courses-facebook (4431ms)
    ✓ Click on element index-contact->courses-dribbble (4405ms)
    ✓ Click on element index-contact->courses-flickr (4408ms)
    ✓ Click on element index-contact->courses-github (4426ms)
    ✓ Click on element index-contact->contact-index-footer (4413ms)
    ✓ Click on element index-contact->contact-about-footer (4464ms)
    ✓ Click on element index-contact->contact-courses-footer (4393ms)
    ✓ Click on element index-contact->contact-fees-footer (4398ms)


  49 passing (4m)
  1 failing

  1) Cytestion
       Click on element index-portfolio->portfolio-WebThemez:
     AssertionError: Timed out retrying after 5000ms: Check requests with client error: expected 404 to satisfy [Function]
      at Context.eval (webpack://cytestion/./cypress/support/commands.js:301:80)




  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        50                                                                               │
  │ Passing:      49                                                                               │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     3 minutes, 52 seconds                                                            │
  │ Spec Ran:     cytestion_3.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  /home/lsi/tcc/open-sources/cytestion-utilization/cytestion/cypress/screenshots/c     (1280x720)
     ytestion_3.cy.js/Cytestion -- Click on element index-portfolio-portfolio-WebThem               
     ez (failed).png                                                                                


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_4.cy.js                                                               (4 of 4)


  Cytestion
    ✓ Click on element index-contact->contact-portfolio-footer (5033ms)
    ✓ Click on element index-contact->contact-contact-footer (4391ms)
    1) Click on element index-contact->contact-webthemez
    ✓ Filling values index-contact->email-name-input-email-address-input-phone-number-input-subject-input-message-textarea and submit (4636ms)


  3 passing (31s)
  1 failing

  1) Cytestion
       Click on element index-contact->contact-webthemez:
     AssertionError: Timed out retrying after 5000ms: Check requests with client error: expected 404 to satisfy [Function]
      at Context.eval (webpack://cytestion/./cypress/support/commands.js:301:80)




  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        4                                                                                │
  │ Passing:      3                                                                                │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     30 seconds                                                                       │
  │ Spec Ran:     cytestion_4.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  /home/lsi/tcc/open-sources/cytestion-utilization/cytestion/cypress/screenshots/c     (1280x720)
     ytestion_4.cy.js/Cytestion -- Click on element index-contact-contact-webthemez (               
     failed).png                                                                                    


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✖  cytestion_1.cy.js                        03:54       50       49        1        -        - │
  ├────────────────────────────────────────────────────────────────────────────────────────────────┤
  │ ✖  cytestion_2.cy.js                        03:58       50       49        1        -        - │
  ├────────────────────────────────────────────────────────────────────────────────────────────────┤
  │ ✖  cytestion_3.cy.js                        03:52       50       49        1        -        - │
  ├────────────────────────────────────────────────────────────────────────────────────────────────┤
  │ ✖  cytestion_4.cy.js                        00:30        4        3        1        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✖  4 of 4 failed (100%)                     12:17      154      150        4        -        -  

info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 225
[1;92mINFO:[0;0m Total test cases without skip: 38
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 38
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/learn-educational/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/open-sources/cytestion-utilization/e2e/exploratory/learn-educati │
  │                 onal/tmp/batches/**/*.cy.js                                                    │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Click on element index-about->about-navbar-brand->index-navbar-toggle (4993ms)
    ✓ Click on element index-about->about-navbar-brand->index-navbar-brand (4436ms)
    ✓ Click on element index-about->about-navbar-brand->index-index (4415ms)
    ✓ Click on element index-about->about-navbar-brand->index-about (4455ms)
    ✓ Click on element index-about->about-navbar-brand->index-courses (4404ms)
    ✓ Click on element index-about->about-navbar-brand->index-fees (4408ms)
    ✓ Click on element index-about->about-navbar-brand->index-portfolio (4576ms)
    ✓ Click on element index-about->about-navbar-brand->index-dropdown-toggle (4407ms)
    ✓ Click on element index-about->about-navbar-brand->index-right-sidebar (4130ms)
    ✓ Click on element index-about->about-navbar-brand->index-dummy-link-1 (4137ms)
    ✓ Click on element index-about->about-navbar-brand->index-dummy-link-2 (4142ms)
    ✓ Click on element index-about->about-navbar-brand->index-dummy-link-3 (4125ms)
    ✓ Click on element index-about->about-navbar-brand->index-contact (4407ms)
    ✓ Click on element index-about->about-navbar-brand->index-read-more (4403ms)
    ✓ Click on element index-about->about-navbar-brand->index-text-1 (4408ms)
    ✓ Click on element index-about->about-navbar-brand->index-text-2 (4395ms)
    ✓ Click on element index-about->about-navbar-brand->index-text-3 (4402ms)
    ✓ Click on element index-about->about-navbar-brand->index-text-4 (4394ms)
    ✓ Click on element index-about->about-navbar-brand->index-text-5 (4399ms)
    ✓ Click on element index-about->about-navbar-brand->index-text-6 (4410ms)
    ✓ Click on element index-about->about-navbar-brand->index-twitter (4408ms)
    ✓ Click on element index-about->about-navbar-brand->index-facebook (4407ms)
    ✓ Click on element index-about->about-navbar-brand->index-dribbble (4402ms)
    ✓ Click on element index-about->about-navbar-brand->index-flickr (4404ms)
    ✓ Click on element index-about->about-navbar-brand->index-github (4397ms)
    ✓ Click on element index-about->about-navbar-brand->index-index-footer (4400ms)
    ✓ Click on element index-about->about-navbar-brand->index-about-footer (4395ms)
    ✓ Click on element index-about->about-navbar-brand->index-courses-footer (4413ms)
    ✓ Click on element index-about->about-navbar-brand->index-fees-footer (4399ms)
    ✓ Click on element index-about->about-navbar-brand->index-portfolio-footer (4413ms)
    ✓ Click on element index-about->about-navbar-brand->index-contact-footer (4394ms)
    1) Click on element index-about->about-navbar-brand->index-WebThemez
    ✓ Click on element index-about->about-fees->index-index-footer (8650ms)
    ✓ Click on element index-about->about-fees->index-about-footer (4444ms)
    ✓ Click on element index-about->about-fees->index-courses-footer (4398ms)
    ✓ Click on element index-about->about-fees->index-fees-footer (4400ms)
    ✓ Click on element index-about->about-fees->index-portfolio-footer (4430ms)
    ✓ Click on element index-about->about-fees->index-contact-footer (4397ms)


  37 passing (3m)
  1 failing

  1) Cytestion
       Click on element index-about->about-navbar-brand->index-WebThemez:
     AssertionError: Timed out retrying after 5000ms: Check requests with client error: expected 404 to satisfy [Function]
      at Context.eval (webpack://cytestion/./cypress/support/commands.js:301:80)




  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        38                                                                               │
  │ Passing:      37                                                                               │
  │ Failing:      1                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  1                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     3 minutes, 6 seconds                                                             │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


  (Screenshots)

  -  /home/lsi/tcc/open-sources/cytestion-utilization/cytestion/cypress/screenshots/c     (1280x720)
     ytestion_1.cy.js/Cytestion -- Click on element index-about-about-navbar-brand-in               
     dex-WebThemez (failed).png                                                                     


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✖  cytestion_1.cy.js                        03:06       38       37        1        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✖  1 of 1 failed (100%)                     03:06       38       37        1        -        -  

info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
[1;92mINFO:[0;0m Generating tests
Test file generated! (../e2e/exploratory/learn-educational/cytestion.cy.js)
Time execution: 18:39.499 (m:ss.mmm)
[1;92mINFO:[0;0m Run failures tests
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
