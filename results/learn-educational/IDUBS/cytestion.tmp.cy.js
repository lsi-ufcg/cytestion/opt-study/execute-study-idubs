describe('Cytestion', () => {
  beforeEach(() => {
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-navbar-toggle`, () => {
const actualId = [`root`,`index-navbar-toggle`];
    cy.clickIfExist(`[data-cy="index-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-navbar-brand`, () => {
const actualId = [`root`,`index-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-index`, () => {
const actualId = [`root`,`index-index`];
    cy.clickIfExist(`[data-cy="index-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about`, () => {
const actualId = [`root`,`index-about`];
    cy.clickIfExist(`[data-cy="index-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses`, () => {
const actualId = [`root`,`index-courses`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-fees`, () => {
const actualId = [`root`,`index-fees`];
    cy.clickIfExist(`[data-cy="index-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-portfolio`, () => {
const actualId = [`root`,`index-portfolio`];
    cy.clickIfExist(`[data-cy="index-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-dropdown-toggle`, () => {
const actualId = [`root`,`index-dropdown-toggle`];
    cy.clickIfExist(`[data-cy="index-dropdown-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-right-sidebar`, () => {
const actualId = [`root`,`index-right-sidebar`];
    cy.clickIfExist(`[data-cy="index-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-dummy-link-1`, () => {
const actualId = [`root`,`index-dummy-link-1`];
    cy.clickIfExist(`[data-cy="index-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-dummy-link-2`, () => {
const actualId = [`root`,`index-dummy-link-2`];
    cy.clickIfExist(`[data-cy="index-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-dummy-link-3`, () => {
const actualId = [`root`,`index-dummy-link-3`];
    cy.clickIfExist(`[data-cy="index-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact`, () => {
const actualId = [`root`,`index-contact`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-read-more`, () => {
const actualId = [`root`,`index-read-more`];
    cy.clickIfExist(`[data-cy="index-read-more"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-1`, () => {
const actualId = [`root`,`index-text-1`];
    cy.clickIfExist(`[data-cy="index-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-2`, () => {
const actualId = [`root`,`index-text-2`];
    cy.clickIfExist(`[data-cy="index-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-3`, () => {
const actualId = [`root`,`index-text-3`];
    cy.clickIfExist(`[data-cy="index-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-4`, () => {
const actualId = [`root`,`index-text-4`];
    cy.clickIfExist(`[data-cy="index-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-5`, () => {
const actualId = [`root`,`index-text-5`];
    cy.clickIfExist(`[data-cy="index-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-6`, () => {
const actualId = [`root`,`index-text-6`];
    cy.clickIfExist(`[data-cy="index-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-twitter`, () => {
const actualId = [`root`,`index-twitter`];
    cy.clickIfExist(`[data-cy="index-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-facebook`, () => {
const actualId = [`root`,`index-facebook`];
    cy.clickIfExist(`[data-cy="index-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-dribbble`, () => {
const actualId = [`root`,`index-dribbble`];
    cy.clickIfExist(`[data-cy="index-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-flickr`, () => {
const actualId = [`root`,`index-flickr`];
    cy.clickIfExist(`[data-cy="index-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-github`, () => {
const actualId = [`root`,`index-github`];
    cy.clickIfExist(`[data-cy="index-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-index-footer`, () => {
const actualId = [`root`,`index-index-footer`];
    cy.clickIfExist(`[data-cy="index-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about-footer`, () => {
const actualId = [`root`,`index-about-footer`];
    cy.clickIfExist(`[data-cy="index-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses-footer`, () => {
const actualId = [`root`,`index-courses-footer`];
    cy.clickIfExist(`[data-cy="index-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-fees-footer`, () => {
const actualId = [`root`,`index-fees-footer`];
    cy.clickIfExist(`[data-cy="index-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-portfolio-footer`, () => {
const actualId = [`root`,`index-portfolio-footer`];
    cy.clickIfExist(`[data-cy="index-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact-footer`, () => {
const actualId = [`root`,`index-contact-footer`];
    cy.clickIfExist(`[data-cy="index-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-WebThemez`, () => {
const actualId = [`root`,`index-WebThemez`];
    cy.clickIfExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-navbar-toggle`, () => {
const actualId = [`root`,`index-about`,`about-navbar-toggle`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-navbar-brand`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-index`, () => {
const actualId = [`root`,`index-about`,`about-index`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-about`, () => {
const actualId = [`root`,`index-about`,`about-about`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-courses`, () => {
const actualId = [`root`,`index-about`,`about-courses`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-fees`, () => {
const actualId = [`root`,`index-about`,`about-fees`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-portfolio`, () => {
const actualId = [`root`,`index-about`,`about-portfolio`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dropdown-toggle`, () => {
const actualId = [`root`,`index-about`,`about-dropdown-toggle`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dropdown-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-right-sidebar`, () => {
const actualId = [`root`,`index-about`,`about-right-sidebar`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dummy-link-1`, () => {
const actualId = [`root`,`index-about`,`about-dummy-link-1`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dummy-link-2`, () => {
const actualId = [`root`,`index-about`,`about-dummy-link-2`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dummy-link-3`, () => {
const actualId = [`root`,`index-about`,`about-dummy-link-3`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact`, () => {
const actualId = [`root`,`index-about`,`about-contact`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-Responsive`, () => {
const actualId = [`root`,`index-about`,`about-Responsive`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-Responsive"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-HTML5`, () => {
const actualId = [`root`,`index-about`,`about-HTML5`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-HTML5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-Bootstrap`, () => {
const actualId = [`root`,`index-about`,`about-Bootstrap`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-Bootstrap"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-Clean`, () => {
const actualId = [`root`,`index-about`,`about-Clean`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-Clean"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-Premium`, () => {
const actualId = [`root`,`index-about`,`about-Premium`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-Premium"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-facebook-ceo`, () => {
const actualId = [`root`,`index-about`,`about-facebook-ceo`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-facebook-ceo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-google-ceo`, () => {
const actualId = [`root`,`index-about`,`about-google-ceo`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-google-ceo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-twitter-ceo`, () => {
const actualId = [`root`,`index-about`,`about-twitter-ceo`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-twitter-ceo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dribbble-ceo`, () => {
const actualId = [`root`,`index-about`,`about-dribbble-ceo`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dribbble-ceo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-github-ceo`, () => {
const actualId = [`root`,`index-about`,`about-github-ceo`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-github-ceo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-facebook-director`, () => {
const actualId = [`root`,`index-about`,`about-facebook-director`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-facebook-director"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-google-director`, () => {
const actualId = [`root`,`index-about`,`about-google-director`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-google-director"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-twitter-director`, () => {
const actualId = [`root`,`index-about`,`about-twitter-director`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-twitter-director"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dribbble-director`, () => {
const actualId = [`root`,`index-about`,`about-dribbble-director`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dribbble-director"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-github-director`, () => {
const actualId = [`root`,`index-about`,`about-github-director`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-github-director"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-facebook-manager`, () => {
const actualId = [`root`,`index-about`,`about-facebook-manager`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-facebook-manager"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-google-manager`, () => {
const actualId = [`root`,`index-about`,`about-google-manager`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-google-manager"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-twitter-manager`, () => {
const actualId = [`root`,`index-about`,`about-twitter-manager`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-twitter-manager"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dribbble-manager`, () => {
const actualId = [`root`,`index-about`,`about-dribbble-manager`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dribbble-manager"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-github-manager`, () => {
const actualId = [`root`,`index-about`,`about-github-manager`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-github-manager"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-facebook-creative`, () => {
const actualId = [`root`,`index-about`,`about-facebook-creative`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-facebook-creative"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-google-creative`, () => {
const actualId = [`root`,`index-about`,`about-google-creative`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-google-creative"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-twitter-creative`, () => {
const actualId = [`root`,`index-about`,`about-twitter-creative`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-twitter-creative"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dribbble-creative`, () => {
const actualId = [`root`,`index-about`,`about-dribbble-creative`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dribbble-creative"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-github-creative`, () => {
const actualId = [`root`,`index-about`,`about-github-creative`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-github-creative"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-twitter`, () => {
const actualId = [`root`,`index-about`,`about-twitter`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-facebook`, () => {
const actualId = [`root`,`index-about`,`about-facebook`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-dribbble`, () => {
const actualId = [`root`,`index-about`,`about-dribbble`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-flickr`, () => {
const actualId = [`root`,`index-about`,`about-flickr`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-github`, () => {
const actualId = [`root`,`index-about`,`about-github`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-index-footer`, () => {
const actualId = [`root`,`index-about`,`about-index-footer`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-about-footer`, () => {
const actualId = [`root`,`index-about`,`about-about-footer`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-courses-footer`, () => {
const actualId = [`root`,`index-about`,`about-courses-footer`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-fees-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees-footer`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-portfolio-footer`, () => {
const actualId = [`root`,`index-about`,`about-portfolio-footer`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact-footer`, () => {
const actualId = [`root`,`index-about`,`about-contact-footer`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-WebThemez`, () => {
const actualId = [`root`,`index-about`,`about-WebThemez`];
cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-navbar-toggle`, () => {
const actualId = [`root`,`index-courses`,`courses-navbar-toggle`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-navbar-brand`, () => {
const actualId = [`root`,`index-courses`,`courses-navbar-brand`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-index`, () => {
const actualId = [`root`,`index-courses`,`courses-index`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-about`, () => {
const actualId = [`root`,`index-courses`,`courses-about`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-courses`, () => {
const actualId = [`root`,`index-courses`,`courses-courses`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-fees`, () => {
const actualId = [`root`,`index-courses`,`courses-fees`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-portfolio`, () => {
const actualId = [`root`,`index-courses`,`courses-portfolio`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-dropdown-toggle`, () => {
const actualId = [`root`,`index-courses`,`courses-dropdown-toggle`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-dropdown-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-right-sidebar`, () => {
const actualId = [`root`,`index-courses`,`courses-right-sidebar`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-dummy-link-1`, () => {
const actualId = [`root`,`index-courses`,`courses-dummy-link-1`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-dummy-link-2`, () => {
const actualId = [`root`,`index-courses`,`courses-dummy-link-2`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-dummy-link-3`, () => {
const actualId = [`root`,`index-courses`,`courses-dummy-link-3`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-contact`, () => {
const actualId = [`root`,`index-courses`,`courses-contact`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-twitter`, () => {
const actualId = [`root`,`index-courses`,`courses-twitter`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-facebook`, () => {
const actualId = [`root`,`index-courses`,`courses-facebook`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-dribbble`, () => {
const actualId = [`root`,`index-courses`,`courses-dribbble`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-flickr`, () => {
const actualId = [`root`,`index-courses`,`courses-flickr`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-github`, () => {
const actualId = [`root`,`index-courses`,`courses-github`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-index-footer`, () => {
const actualId = [`root`,`index-courses`,`courses-index-footer`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-about-footer`, () => {
const actualId = [`root`,`index-courses`,`courses-about-footer`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-courses-footer`, () => {
const actualId = [`root`,`index-courses`,`courses-courses-footer`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-fees-footer`, () => {
const actualId = [`root`,`index-courses`,`courses-fees-footer`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-portfolio-footer`, () => {
const actualId = [`root`,`index-courses`,`courses-portfolio-footer`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-contact-footer`, () => {
const actualId = [`root`,`index-courses`,`courses-contact-footer`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-courses->courses-WebThemez`, () => {
const actualId = [`root`,`index-courses`,`courses-WebThemez`];
cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-navbar-collapse`, () => {
const actualId = [`root`,`index-fees`,`fees-navbar-collapse`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-navbar-collapse"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-navbar-brand`, () => {
const actualId = [`root`,`index-fees`,`fees-navbar-brand`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-index`, () => {
const actualId = [`root`,`index-fees`,`fees-index`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-about`, () => {
const actualId = [`root`,`index-fees`,`fees-about`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-courses`, () => {
const actualId = [`root`,`index-fees`,`fees-courses`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-fees`, () => {
const actualId = [`root`,`index-fees`,`fees-fees`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-portfolio`, () => {
const actualId = [`root`,`index-fees`,`fees-portfolio`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-dropdown`, () => {
const actualId = [`root`,`index-fees`,`fees-dropdown`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-dropdown"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-right-sidebar`, () => {
const actualId = [`root`,`index-fees`,`fees-right-sidebar`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-dummy-link-1`, () => {
const actualId = [`root`,`index-fees`,`fees-dummy-link-1`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-dummy-link-2`, () => {
const actualId = [`root`,`index-fees`,`fees-dummy-link-2`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-dummy-link-3`, () => {
const actualId = [`root`,`index-fees`,`fees-dummy-link-3`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-contact`, () => {
const actualId = [`root`,`index-fees`,`fees-contact`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-signup-basic`, () => {
const actualId = [`root`,`index-fees`,`fees-signup-basic`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-signup-basic"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-signup-standard`, () => {
const actualId = [`root`,`index-fees`,`fees-signup-standard`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-signup-standard"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-signup-advanced`, () => {
const actualId = [`root`,`index-fees`,`fees-signup-advanced`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-signup-advanced"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-signup-mighty`, () => {
const actualId = [`root`,`index-fees`,`fees-signup-mighty`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-signup-mighty"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-twitter`, () => {
const actualId = [`root`,`index-fees`,`fees-twitter`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-facebook`, () => {
const actualId = [`root`,`index-fees`,`fees-facebook`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-dribbble`, () => {
const actualId = [`root`,`index-fees`,`fees-dribbble`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-flickr`, () => {
const actualId = [`root`,`index-fees`,`fees-flickr`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-github`, () => {
const actualId = [`root`,`index-fees`,`fees-github`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-fees->fees-WebThemez`, () => {
const actualId = [`root`,`index-fees`,`fees-WebThemez`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="fees-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-navbar-collapse`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-navbar-collapse`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-navbar-collapse"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->sidebar-navbar-brand`, () => {
const actualId = [`root`,`index-portfolio`,`sidebar-navbar-brand`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="sidebar-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-index`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-index`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-about`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-about`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-courses`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-courses`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-fees`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-fees`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-portfolio`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-portfolio`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-dropdown`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-dropdown`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-dropdown"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-right-sidebar`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-right-sidebar`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-dummy-link-1`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-dummy-link-1`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-dummy-link-2`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-dummy-link-2`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-dummy-link-3`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-dummy-link-3`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-contact`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-contact`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-all`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-all`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-all"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-web-design`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-web-design`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-web-design"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-photography`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-photography`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-photography"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-print`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-print`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-print"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-twitter`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-twitter`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-facebook`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-facebook`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-dribbble`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-dribbble`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-flickr`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-flickr`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-github`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-github`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-index-footer`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-index-footer`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-about-footer`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-about-footer`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-courses-footer`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-courses-footer`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-fees-footer`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-fees-footer`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-portfolio-footer`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-portfolio-footer`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-contact-footer`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-contact-footer`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-portfolio->portfolio-WebThemez`, () => {
const actualId = [`root`,`index-portfolio`,`portfolio-WebThemez`];
cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="portfolio-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-navbar-toggle`, () => {
const actualId = [`root`,`index-contact`,`contact-navbar-toggle`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-navbar-brand`, () => {
const actualId = [`root`,`index-contact`,`contact-navbar-brand`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-index`, () => {
const actualId = [`root`,`index-contact`,`contact-index`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-about`, () => {
const actualId = [`root`,`index-contact`,`contact-about`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-courses`, () => {
const actualId = [`root`,`index-contact`,`contact-courses`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-fees`, () => {
const actualId = [`root`,`index-contact`,`contact-fees`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-portfolio`, () => {
const actualId = [`root`,`index-contact`,`contact-portfolio`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-dropdown-toggle`, () => {
const actualId = [`root`,`index-contact`,`contact-dropdown-toggle`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-dropdown-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-right-sidebar`, () => {
const actualId = [`root`,`index-contact`,`contact-right-sidebar`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-dummy-link-1`, () => {
const actualId = [`root`,`index-contact`,`contact-dummy-link-1`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-dummy-link-2`, () => {
const actualId = [`root`,`index-contact`,`contact-dummy-link-2`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-dummy-link-3`, () => {
const actualId = [`root`,`index-contact`,`contact-dummy-link-3`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-contact`, () => {
const actualId = [`root`,`index-contact`,`contact-contact`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->send-message-button`, () => {
const actualId = [`root`,`index-contact`,`send-message-button`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="send-message-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->courses-twitter`, () => {
const actualId = [`root`,`index-contact`,`courses-twitter`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->courses-facebook`, () => {
const actualId = [`root`,`index-contact`,`courses-facebook`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->courses-dribbble`, () => {
const actualId = [`root`,`index-contact`,`courses-dribbble`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->courses-flickr`, () => {
const actualId = [`root`,`index-contact`,`courses-flickr`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->courses-github`, () => {
const actualId = [`root`,`index-contact`,`courses-github`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="courses-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-index-footer`, () => {
const actualId = [`root`,`index-contact`,`contact-index-footer`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-about-footer`, () => {
const actualId = [`root`,`index-contact`,`contact-about-footer`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-courses-footer`, () => {
const actualId = [`root`,`index-contact`,`contact-courses-footer`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-fees-footer`, () => {
const actualId = [`root`,`index-contact`,`contact-fees-footer`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-portfolio-footer`, () => {
const actualId = [`root`,`index-contact`,`contact-portfolio-footer`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-contact-footer`, () => {
const actualId = [`root`,`index-contact`,`contact-contact-footer`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-webthemez`, () => {
const actualId = [`root`,`index-contact`,`contact-webthemez`];
cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-webthemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values index-contact->email-name-input-email-address-input-phone-number-input-subject-input-message-textarea and submit`, () => {
const actualId = [`root`,`index-contact`,`email-name-input-email-address-input-phone-number-input-subject-input-message-textarea`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
    cy.fillInput(`[data-cy="email-name-input"] textarea`, `initiatives`);
cy.fillInput(`[data-cy="email-address-input"] textarea`, `Identidade`);
cy.fillInput(`[data-cy="phone-number-input"] textarea`, `Argentina`);
cy.fillInput(`[data-cy="subject-input"] textarea`, `frontend`);
cy.fillInput(`[data-cy="message-textarea"] input`, `Metal`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-navbar-toggle`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-navbar-toggle`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-navbar-brand`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-navbar-brand`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-index`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-index`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-about`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-about`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-courses`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-courses`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-courses"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-fees`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-fees`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-fees"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-portfolio`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-portfolio`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-portfolio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-dropdown-toggle`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-dropdown-toggle`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-dropdown-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-right-sidebar`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-right-sidebar`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-right-sidebar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-dummy-link-1`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-dummy-link-1`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-dummy-link-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-dummy-link-2`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-dummy-link-2`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-dummy-link-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-dummy-link-3`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-dummy-link-3`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-dummy-link-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-contact`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-contact`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-read-more`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-read-more`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-read-more"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-1`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-1`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-2`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-2`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-3`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-3`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-4`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-4`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-5`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-5`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-6`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-6`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-twitter`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-twitter`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-facebook`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-facebook`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-dribbble`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-dribbble`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-dribbble"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-flickr`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-flickr`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-flickr"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-github`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-github`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-github"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-index-footer`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-index-footer`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-about-footer`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-about-footer`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-courses-footer`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-courses-footer`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-fees-footer`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-fees-footer`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-portfolio-footer`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-portfolio-footer`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-contact-footer`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-contact-footer`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-navbar-brand->index-WebThemez`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-WebThemez`];
cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-fees->index-index-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees`,`index-index-footer`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-index-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-fees->index-about-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees`,`index-about-footer`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-about-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-fees->index-courses-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees`,`index-courses-footer`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-courses-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-fees->index-fees-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees`,`index-fees-footer`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-fees-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-fees->index-portfolio-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees`,`index-portfolio-footer`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-portfolio-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-fees->index-contact-footer`, () => {
const actualId = [`root`,`index-about`,`about-fees`,`index-contact-footer`];
cy.visit('http://localhost:9998/fees.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-contact-footer"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
