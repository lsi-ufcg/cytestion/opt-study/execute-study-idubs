describe('Cytestion', () => {
  beforeEach(() => {
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Click on element index-about->about-WebThemez`, () => {
    cy.visit('http://localhost:9998/about.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="about-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-courses->courses-WebThemez`, () => {
    cy.visit('http://localhost:9998/courses.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="courses-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-portfolio->portfolio-WebThemez`, () => {
    cy.visit('http://localhost:9998/portfolio.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="portfolio-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-contact->contact-webthemez`, () => {
    cy.visit('http://localhost:9998/contact.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="contact-webthemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-about->about-navbar-brand->index-WebThemez`, () => {
    cy.visit('http://localhost:9998/index.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
});
