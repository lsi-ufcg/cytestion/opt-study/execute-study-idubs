describe('Cytestion', () => {
  beforeEach(() => {
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Filling values find owners->find-owner-button->owner-link->edit-owner->firstName-lastName-address-city-telephone and submit`, () => {
    cy.visit('http://localhost:8080/owners/1');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="edit-owner"]`);
    cy.fillInput(`[data-cy="firstName"] textarea`, `Borders`);
    cy.fillInput(`[data-cy="lastName"] textarea`, `Associado`);
    cy.fillInput(`[data-cy="address"] textarea`, `Hryvnia`);
    cy.fillInput(`[data-cy="city"] textarea`, `sensor`);
    cy.fillInput(`[data-cy="telephone"] textarea`, `magenta`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
