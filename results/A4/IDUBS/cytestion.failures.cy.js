describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-agendamentos->870786242-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/lancamento-saldos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~14555147D%7C%7C14555147&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="870786242-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-agendamentos->4285481604-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/lancamento-saldos-mf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31798273D%7C%7C31798273&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4285481604-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-agendamentos->3418140800-voltar`, () => {
    cy.visit(
      'http://system-A4/relatorios-apoio/diferencas-debito-credito?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46510613D%7C%7C46510613&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3418140800-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-agendamentos->3556153674-voltar`, () => {
    cy.visit(
      'http://system-A4/relatorios-apoio/balancete-contas-referenciais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~14555153D%7C%7C14555153&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3556153674-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-agendamentos->2582422381-voltar`, () => {
    cy.visit(
      'http://system-A4/relatorios-apoio/balancete-plano-estatutario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56537976D%7C%7C56537976&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2582422381-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-agendamentos->1836703078-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/balanco-patrimonial-mf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~32065000D%7C%7C32065000&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1836703078-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-agendamentos->1749891658-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/dre-mf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~32029312D%7C%7C32029312&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1749891658-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-agendamentos->3036592371-voltar`, () => {
    cy.visit(
      'http://system-A4/relatorios-apoio/conferencia-aglutinacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19786760D%7C%7C19786760&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3036592371-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-agendamentos->41723402-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/vigencia-aglutinadores?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~20037984D%7C%7C20037984&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="41723402-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-agendamentos->2943506080-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/livro-diario-geral?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12974456D%7C%7C12974456&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2943506080-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-agendamentos->3564810834-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/livro-diario-auxiliar?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12974464D%7C%7C12974464&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3564810834-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-agendamentos->787961799-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/livro-razao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12974472D%7C%7C12974472&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="787961799-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-agendamentos->4257475929-voltar`, () => {
    cy.visit('http://system-A4/relatorios-apoio/livro-razao-auxiliar?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12974479D%7C%7C12974479&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4257475929-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-agendamentos->1985649969-voltar`, () => {
    cy.visit(
      'http://system-A4/processos/geracao-lancamentos-encerramento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~34371945D%7C%7C34371945&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1985649969-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/saldos-mensais->1324586340-agendamentos->1324586340-voltar`, () => {
    cy.visit('http://system-A4/processos/saldos-mensais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~34372392D%7C%7C34372392&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1324586340-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/compilacao-aglutinadores->2882492716-agendamentos->2882492716-voltar`, () => {
    cy.visit('http://system-A4/processos/compilacao-aglutinadores?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15562255D%7C%7C15562255&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2882492716-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-agendamentos->2878135405-voltar`, () => {
    cy.visit(
      'http://system-A4/processos/limpeza-compilacao-aglutinadores?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15563155D%7C%7C15563155&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2878135405-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/dre->3534660222-agendamentos->3534660222-voltar`, () => {
    cy.visit('http://system-A4/processos/dre?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~14555161D%7C%7C14555161&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3534660222-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/dre-moeda-funcional->783159763-agendamentos->783159763-voltar`, () => {
    cy.visit('http://system-A4/processos/dre-moeda-funcional?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~32029312D%7C%7C32029312&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="783159763-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-agendamentos->4199836187-voltar`, () => {
    cy.visit('http://system-A4/processos/limpeza-tabelas-definitivas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~13002504D%7C%7C13002504&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4199836187-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/carga-estabelecimentos->3453673399-agendamentos->3453673399-voltar`, () => {
    cy.visit('http://system-A4/processos/carga-estabelecimentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12319204D%7C%7C12319204&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3453673399-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
