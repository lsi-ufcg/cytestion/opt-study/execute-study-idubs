describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo`, () => {
const actualId = [`root`,`tabelas-corporativo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil`, () => {
const actualId = [`root`,`movimentacao-contabil`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k`, () => {
const actualId = [`root`,`bloco-k`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio`, () => {
const actualId = [`root`,`relatorios-apoio`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
const actualId = [`root`,`downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/pfj`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/plano`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/plano`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/plano"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil-auxiliar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil-auxiliar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil-auxiliar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/saldo-mensal`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/notas`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/notas`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/notas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/razao-sub-conta-correlata`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/razao-sub-conta-correlata`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/razao-sub-conta-correlata"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/empresas`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/empresas`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/empresas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/saldo-contas`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/saldo-contas`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/saldo-contas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/plano-contas`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/plano-contas`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/plano-contas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre`, () => {
const actualId = [`root`,`processos`,`processos/dre`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1352459982-power-search-button`, () => {
const actualId = [`root`,`downloads`,`1352459982-power-search-button`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1352459982-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1352459982-download`, () => {
const actualId = [`root`,`downloads`,`1352459982-download`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1352459982-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1352459982-detalhes`, () => {
const actualId = [`root`,`downloads`,`1352459982-detalhes`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1352459982-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1352459982-excluir`, () => {
const actualId = [`root`,`downloads`,`1352459982-excluir`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1352459982-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/estabelecimentos`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/estabelecimentos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/estabelecimentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->3249208381-novo`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil`,`3249208381-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
      cy.clickIfExist(`[data-cy="3249208381-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->3249208381-power-search-button`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil`,`3249208381-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
      cy.clickIfExist(`[data-cy="3249208381-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil-auxiliar->1791159715-novo`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil-auxiliar`,`1791159715-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil-auxiliar"]`);
      cy.clickIfExist(`[data-cy="1791159715-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil-auxiliar->1791159715-power-search-button`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil-auxiliar`,`1791159715-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil-auxiliar"]`);
      cy.clickIfExist(`[data-cy="1791159715-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->1351642136-novo`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/saldo-mensal`,`1351642136-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
      cy.clickIfExist(`[data-cy="1351642136-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->1351642136-power-search-button`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/saldo-mensal`,`1351642136-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
      cy.clickIfExist(`[data-cy="1351642136-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/notas->103218009-novo`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/notas`,`103218009-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/notas"]`);
      cy.clickIfExist(`[data-cy="103218009-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/notas->103218009-power-search-button`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/notas`,`103218009-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/notas"]`);
      cy.clickIfExist(`[data-cy="103218009-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/razao-sub-conta-correlata->3642283728-novo`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/razao-sub-conta-correlata`,`3642283728-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/razao-sub-conta-correlata"]`);
      cy.clickIfExist(`[data-cy="3642283728-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/razao-sub-conta-correlata->3642283728-power-search-button`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/razao-sub-conta-correlata`,`3642283728-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/razao-sub-conta-correlata"]`);
      cy.clickIfExist(`[data-cy="3642283728-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/empresas->1357412324-novo`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/empresas`,`1357412324-novo`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/empresas"]`);
      cy.clickIfExist(`[data-cy="1357412324-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/empresas->1357412324-power-search-button`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/empresas`,`1357412324-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/empresas"]`);
      cy.clickIfExist(`[data-cy="1357412324-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/saldo-contas->3309622246-novo`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/saldo-contas`,`3309622246-novo`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/saldo-contas"]`);
      cy.clickIfExist(`[data-cy="3309622246-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/saldo-contas->3309622246-power-search-button`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/saldo-contas`,`3309622246-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/saldo-contas"]`);
      cy.clickIfExist(`[data-cy="3309622246-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1088714816-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1088714816-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1088714816-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1088714816-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1088714816-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1088714816-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1088714816-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1088714816-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1088714816-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/plano-contas->789851458-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/plano-contas`,`789851458-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/plano-contas"]`);
      cy.clickIfExist(`[data-cy="789851458-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/plano-contas->789851458-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/plano-contas`,`789851458-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/plano-contas"]`);
      cy.clickIfExist(`[data-cy="789851458-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/plano-contas->789851458-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/plano-contas`,`789851458-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/plano-contas"]`);
      cy.fillInputPowerSearch(`[data-cy="789851458-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete->1908665769-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete`,`1908665769-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete"]`);
      cy.clickIfExist(`[data-cy="1908665769-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete->1908665769-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete`,`1908665769-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete"]`);
      cy.clickIfExist(`[data-cy="1908665769-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/balancete->1908665769-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete`,`1908665769-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete"]`);
      cy.fillInputPowerSearch(`[data-cy="1908665769-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-regerar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-regerar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-detalhes`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-abrir visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-excluir`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-excluir`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial->2839368693-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial`,`2839368693-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial"]`);
      cy.clickIfExist(`[data-cy="2839368693-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial->2839368693-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial`,`2839368693-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial"]`);
      cy.clickIfExist(`[data-cy="2839368693-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/balanco-patrimonial->2839368693-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial`,`2839368693-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial"]`);
      cy.fillInputPowerSearch(`[data-cy="2839368693-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre->771669975-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre`,`771669975-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre"]`);
      cy.clickIfExist(`[data-cy="771669975-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre->771669975-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre`,`771669975-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre"]`);
      cy.clickIfExist(`[data-cy="771669975-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/dre->771669975-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre`,`771669975-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre"]`);
      cy.fillInputPowerSearch(`[data-cy="771669975-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-executar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`,`4257475929-executar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.clickIfExist(`[data-cy="4257475929-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-agendamentos`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`,`4257475929-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.clickIfExist(`[data-cy="4257475929-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-power-search-button`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`,`4257475929-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.clickIfExist(`[data-cy="4257475929-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-visualização`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`,`4257475929-visualização`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.clickIfExist(`[data-cy="4257475929-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-executar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-visualização`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-executar`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-visualização`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-regerar`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-excluir`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-executar`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-visualização`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-executar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-visualização`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-executar`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-visualização`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-regerar`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-excluir`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-executar`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-visualização`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-executar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-visualização`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-regerar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-excluir`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos->3453673399-executar`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos->3453673399-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos->3453673399-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos->3453673399-visualização`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-carregar mais`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-carregar mais`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-carregar mais`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-carregar mais`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-carregar mais`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-carregar mais`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-selecionar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-selecionar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-selecionar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->3249208381-novo->682894028-salvar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil`,`3249208381-novo`,`682894028-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
      cy.clickIfExist(`[data-cy="3249208381-novo"]`);
      cy.clickIfExist(`[data-cy="682894028-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->3249208381-novo->682894028-voltar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil`,`3249208381-novo`,`682894028-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
      cy.clickIfExist(`[data-cy="3249208381-novo"]`);
      cy.clickIfExist(`[data-cy="682894028-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/lancamento-contabil->3249208381-novo->682894028-input-numLancamento-682894028-input-numArquivamento-682894028-powerselect-icconIdAnalitica-682894028-powerselect-icodIdCusto-682894028-input-monetary-vlLancamento-682894028-input-monetary-vlLancamentoMf-682894028-powerselect-indDbCr-682894028-powerselect-icodIdFatoContabil-682894028-input-histComplementar-682894028-powerselect-icodIdHistorico-682894028-powerselect-tipoLcto-682894028-powerselect-codPart and submit`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil`,`3249208381-novo`,`682894028-input-numLancamento-682894028-input-numArquivamento-682894028-powerselect-icconIdAnalitica-682894028-powerselect-icodIdCusto-682894028-input-monetary-vlLancamento-682894028-input-monetary-vlLancamentoMf-682894028-powerselect-indDbCr-682894028-powerselect-icodIdFatoContabil-682894028-input-histComplementar-682894028-powerselect-icodIdHistorico-682894028-powerselect-tipoLcto-682894028-powerselect-codPart`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
      cy.clickIfExist(`[data-cy="3249208381-novo"]`);
      cy.fillInput(`[data-cy="682894028-input-numLancamento"] textarea`, `Fresh`);
cy.fillInput(`[data-cy="682894028-input-numArquivamento"] textarea`, `hack`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-icconIdAnalitica"] input`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-icodIdCusto"] input`);
cy.fillInput(`[data-cy="682894028-input-monetary-vlLancamento"] textarea`, `4,86`);
cy.fillInput(`[data-cy="682894028-input-monetary-vlLancamentoMf"] textarea`, `3,16`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-indDbCr"] input`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-icodIdFatoContabil"] input`);
cy.fillInput(`[data-cy="682894028-input-histComplementar"] textarea`, `alarm`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-icodIdHistorico"] input`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-tipoLcto"] input`);
cy.fillInputPowerSelect(`[data-cy="682894028-powerselect-codPart"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil-auxiliar->1791159715-novo->2214142246-salvar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil-auxiliar`,`1791159715-novo`,`2214142246-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil-auxiliar"]`);
      cy.clickIfExist(`[data-cy="1791159715-novo"]`);
      cy.clickIfExist(`[data-cy="2214142246-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil-auxiliar->1791159715-novo->2214142246-voltar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil-auxiliar`,`1791159715-novo`,`2214142246-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil-auxiliar"]`);
      cy.clickIfExist(`[data-cy="1791159715-novo"]`);
      cy.clickIfExist(`[data-cy="2214142246-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/lancamento-contabil-auxiliar->1791159715-novo->2214142246-input-numLancamento-2214142246-input-numArquivamento-2214142246-powerselect-icconIdAnalitica-2214142246-powerselect-icodIdCusto-2214142246-input-monetary-vlLancamento-2214142246-powerselect-indDbCr-2214142246-input-histComplementar-2214142246-powerselect-icodIdHistorico-2214142246-powerselect-tipoLcto-2214142246-powerselect-codPart-2214142246-powerselect-icodIdAux and submit`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/lancamento-contabil-auxiliar`,`1791159715-novo`,`2214142246-input-numLancamento-2214142246-input-numArquivamento-2214142246-powerselect-icconIdAnalitica-2214142246-powerselect-icodIdCusto-2214142246-input-monetary-vlLancamento-2214142246-powerselect-indDbCr-2214142246-input-histComplementar-2214142246-powerselect-icodIdHistorico-2214142246-powerselect-tipoLcto-2214142246-powerselect-codPart-2214142246-powerselect-icodIdAux`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil-auxiliar"]`);
      cy.clickIfExist(`[data-cy="1791159715-novo"]`);
      cy.fillInput(`[data-cy="2214142246-input-numLancamento"] textarea`, `Distrito Federal`);
cy.fillInput(`[data-cy="2214142246-input-numArquivamento"] textarea`, `Marginal`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-icconIdAnalitica"] input`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-icodIdCusto"] input`);
cy.fillInput(`[data-cy="2214142246-input-monetary-vlLancamento"] textarea`, `3,23`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-indDbCr"] input`);
cy.fillInput(`[data-cy="2214142246-input-histComplementar"] textarea`, `webenabled`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-icodIdHistorico"] input`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-tipoLcto"] input`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-codPart"] input`);
cy.fillInputPowerSelect(`[data-cy="2214142246-powerselect-icodIdAux"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->1351642136-novo->9528273-salvar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/saldo-mensal`,`1351642136-novo`,`9528273-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
      cy.clickIfExist(`[data-cy="1351642136-novo"]`);
      cy.clickIfExist(`[data-cy="9528273-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->1351642136-novo->9528273-voltar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/saldo-mensal`,`1351642136-novo`,`9528273-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
      cy.clickIfExist(`[data-cy="1351642136-novo"]`);
      cy.clickIfExist(`[data-cy="9528273-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/saldo-mensal->1351642136-novo->9528273-powerselect-icconIdAnalitica-9528273-input-monetary-vlInicial-9528273-powerselect-indDbCr-9528273-input-monetary-vlDebito-9528273-input-monetary-vlCredito-9528273-input-monetary-vlSaldoFinalAcumulado-9528273-powerselect-indDbCrSldFinal-9528273-powerselect-icodIdCcusto and submit`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/saldo-mensal`,`1351642136-novo`,`9528273-powerselect-icconIdAnalitica-9528273-input-monetary-vlInicial-9528273-powerselect-indDbCr-9528273-input-monetary-vlDebito-9528273-input-monetary-vlCredito-9528273-input-monetary-vlSaldoFinalAcumulado-9528273-powerselect-indDbCrSldFinal-9528273-powerselect-icodIdCcusto`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
      cy.clickIfExist(`[data-cy="1351642136-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="9528273-powerselect-icconIdAnalitica"] input`);
cy.fillInput(`[data-cy="9528273-input-monetary-vlInicial"] textarea`, `5,83`);
cy.fillInputPowerSelect(`[data-cy="9528273-powerselect-indDbCr"] input`);
cy.fillInput(`[data-cy="9528273-input-monetary-vlDebito"] textarea`, `5,99`);
cy.fillInput(`[data-cy="9528273-input-monetary-vlCredito"] textarea`, `7,35`);
cy.fillInput(`[data-cy="9528273-input-monetary-vlSaldoFinalAcumulado"] textarea`, `8,84`);
cy.fillInputPowerSelect(`[data-cy="9528273-powerselect-indDbCrSldFinal"] input`);
cy.fillInputPowerSelect(`[data-cy="9528273-powerselect-icodIdCcusto"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/notas->103218009-novo->3433510192-salvar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/notas`,`103218009-novo`,`3433510192-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/notas"]`);
      cy.clickIfExist(`[data-cy="103218009-novo"]`);
      cy.clickIfExist(`[data-cy="3433510192-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/notas->103218009-novo->3433510192-voltar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/notas`,`103218009-novo`,`3433510192-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/notas"]`);
      cy.clickIfExist(`[data-cy="103218009-novo"]`);
      cy.clickIfExist(`[data-cy="3433510192-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/notas->103218009-novo->3433510192-powerselect-origConta-3433510192-textarea-notaExplicativa and submit`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/notas`,`103218009-novo`,`3433510192-powerselect-origConta-3433510192-textarea-notaExplicativa`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/notas"]`);
      cy.clickIfExist(`[data-cy="103218009-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="3433510192-powerselect-origConta"] input`);
cy.fillInput(`[data-cy="3433510192-textarea-notaExplicativa"] input`, `override`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/razao-sub-conta-correlata->3642283728-novo->2830560281-salvar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/razao-sub-conta-correlata`,`3642283728-novo`,`2830560281-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/razao-sub-conta-correlata"]`);
      cy.clickIfExist(`[data-cy="3642283728-novo"]`);
      cy.clickIfExist(`[data-cy="2830560281-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/razao-sub-conta-correlata->3642283728-novo->2830560281-voltar`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/razao-sub-conta-correlata`,`3642283728-novo`,`2830560281-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/razao-sub-conta-correlata"]`);
      cy.clickIfExist(`[data-cy="3642283728-novo"]`);
      cy.clickIfExist(`[data-cy="2830560281-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/razao-sub-conta-correlata->3642283728-novo->2830560281-powerselect-codGrupo-2830560281-powerselect-icconIdSubconta-2830560281-powerselect-natSubCnt-2830560281-powerselect-icodIdCodCcus-2830560281-powerselect-icodIdCodPatrItem-2830560281-input-identItem-2830560281-input-cnpj-cnpjInvtd-2830560281-input-number-qtd-2830560281-input-monetary-sldItemIni-2830560281-powerselect-indSldItemIni-2830560281-input-monetary-realItem-2830560281-powerselect-indRealItem-2830560281-input-monetary-sldItemFin-2830560281-powerselect-indSldItemFin-2830560281-input-monetary-sldScntIni-2830560281-powerselect-indSldScntIni-2830560281-input-monetary-debScnt-2830560281-input-monetary-credScnt-2830560281-input-monetary-sldScntFin-2830560281-powerselect-indSldScntFin-2830560281-input-nrLcto-2830560281-input-monetary-vlrLcto-2830560281-powerselect-indVlrLcto-2830560281-powerselect-indAdocIni and submit`, () => {
const actualId = [`root`,`movimentacao-contabil`,`movimentacao-contabil/razao-sub-conta-correlata`,`3642283728-novo`,`2830560281-powerselect-codGrupo-2830560281-powerselect-icconIdSubconta-2830560281-powerselect-natSubCnt-2830560281-powerselect-icodIdCodCcus-2830560281-powerselect-icodIdCodPatrItem-2830560281-input-identItem-2830560281-input-cnpj-cnpjInvtd-2830560281-input-number-qtd-2830560281-input-monetary-sldItemIni-2830560281-powerselect-indSldItemIni-2830560281-input-monetary-realItem-2830560281-powerselect-indRealItem-2830560281-input-monetary-sldItemFin-2830560281-powerselect-indSldItemFin-2830560281-input-monetary-sldScntIni-2830560281-powerselect-indSldScntIni-2830560281-input-monetary-debScnt-2830560281-input-monetary-credScnt-2830560281-input-monetary-sldScntFin-2830560281-powerselect-indSldScntFin-2830560281-input-nrLcto-2830560281-input-monetary-vlrLcto-2830560281-powerselect-indVlrLcto-2830560281-powerselect-indAdocIni`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
      cy.clickIfExist(`[data-cy="movimentacao-contabil/razao-sub-conta-correlata"]`);
      cy.clickIfExist(`[data-cy="3642283728-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-codGrupo"] input`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-icconIdSubconta"] input`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-natSubCnt"] input`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-icodIdCodCcus"] input`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-icodIdCodPatrItem"] input`);
cy.fillInput(`[data-cy="2830560281-input-identItem"] textarea`, `B2C`);
cy.fillInput(`[data-cy="2830560281-input-cnpj-cnpjInvtd"] textarea`, `66.482.748/8737-75`);
cy.fillInput(`[data-cy="2830560281-input-number-qtd"] textarea`, `7`);
cy.fillInput(`[data-cy="2830560281-input-monetary-sldItemIni"] textarea`, `4,55`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indSldItemIni"] input`);
cy.fillInput(`[data-cy="2830560281-input-monetary-realItem"] textarea`, `9,51`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indRealItem"] input`);
cy.fillInput(`[data-cy="2830560281-input-monetary-sldItemFin"] textarea`, `3,55`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indSldItemFin"] input`);
cy.fillInput(`[data-cy="2830560281-input-monetary-sldScntIni"] textarea`, `2,88`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indSldScntIni"] input`);
cy.fillInput(`[data-cy="2830560281-input-monetary-debScnt"] textarea`, `6,42`);
cy.fillInput(`[data-cy="2830560281-input-monetary-credScnt"] textarea`, `7,17`);
cy.fillInput(`[data-cy="2830560281-input-monetary-sldScntFin"] textarea`, `2,87`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indSldScntFin"] input`);
cy.fillInput(`[data-cy="2830560281-input-nrLcto"] textarea`, `Viela`);
cy.fillInput(`[data-cy="2830560281-input-monetary-vlrLcto"] textarea`, `2,66`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indVlrLcto"] input`);
cy.fillInputPowerSelect(`[data-cy="2830560281-powerselect-indAdocIni"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/empresas->1357412324-novo->2560939909-salvar`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/empresas`,`1357412324-novo`,`2560939909-salvar`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/empresas"]`);
      cy.clickIfExist(`[data-cy="1357412324-novo"]`);
      cy.clickIfExist(`[data-cy="2560939909-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/empresas->1357412324-novo->2560939909-voltar`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/empresas`,`1357412324-novo`,`2560939909-voltar`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/empresas"]`);
      cy.clickIfExist(`[data-cy="1357412324-novo"]`);
      cy.clickIfExist(`[data-cy="2560939909-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-k->bloco-k/empresas->1357412324-novo->2560939909-powerselect-empPartId-2560939909-powerselect-evento-2560939909-input-monetary-perPart-2560939909-input-monetary-perCons and submit`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/empresas`,`1357412324-novo`,`2560939909-powerselect-empPartId-2560939909-powerselect-evento-2560939909-input-monetary-perPart-2560939909-input-monetary-perCons`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/empresas"]`);
      cy.clickIfExist(`[data-cy="1357412324-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2560939909-powerselect-empPartId"] input`);
cy.fillInputPowerSelect(`[data-cy="2560939909-powerselect-evento"] input`);
cy.fillInput(`[data-cy="2560939909-input-monetary-perPart"] textarea`, `9`);
cy.fillInput(`[data-cy="2560939909-input-monetary-perCons"] textarea`, `9`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/saldo-contas->3309622246-novo->584885854-salvar`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/saldo-contas`,`3309622246-novo`,`584885854-salvar`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/saldo-contas"]`);
      cy.clickIfExist(`[data-cy="3309622246-novo"]`);
      cy.clickIfExist(`[data-cy="584885854-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-k->bloco-k/saldo-contas->3309622246-novo->584885854-voltar`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/saldo-contas`,`3309622246-novo`,`584885854-voltar`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/saldo-contas"]`);
      cy.clickIfExist(`[data-cy="3309622246-novo"]`);
      cy.clickIfExist(`[data-cy="584885854-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-k->bloco-k/saldo-contas->3309622246-novo->584885854-powerselect-icconIdAnalitica-584885854-input-monetary-vlAglutinado-584885854-powerselect-indValAg-584885854-input-monetary-vlEliminado-584885854-powerselect-indValEl-584885854-input-monetary-vlConsolidado-584885854-powerselect-indValCs and submit`, () => {
const actualId = [`root`,`bloco-k`,`bloco-k/saldo-contas`,`3309622246-novo`,`584885854-powerselect-icconIdAnalitica-584885854-input-monetary-vlAglutinado-584885854-powerselect-indValAg-584885854-input-monetary-vlEliminado-584885854-powerselect-indValEl-584885854-input-monetary-vlConsolidado-584885854-powerselect-indValCs`];
    cy.clickIfExist(`[data-cy="bloco-k"]`);
      cy.clickIfExist(`[data-cy="bloco-k/saldo-contas"]`);
      cy.clickIfExist(`[data-cy="3309622246-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="584885854-powerselect-icconIdAnalitica"] input`);
cy.fillInput(`[data-cy="584885854-input-monetary-vlAglutinado"] textarea`, `9,99`);
cy.fillInputPowerSelect(`[data-cy="584885854-powerselect-indValAg"] input`);
cy.fillInput(`[data-cy="584885854-input-monetary-vlEliminado"] textarea`, `4,06`);
cy.fillInputPowerSelect(`[data-cy="584885854-powerselect-indValEl"] input`);
cy.fillInput(`[data-cy="584885854-input-monetary-vlConsolidado"] textarea`, `5,51`);
cy.fillInputPowerSelect(`[data-cy="584885854-powerselect-indValCs"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-gerenciar labels->2343663976-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-gerenciar labels`,`2343663976-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="2343663976-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-visualizar/editar->1738439429-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-visualizar/editar`,`1738439429-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1738439429-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->2343663976-visualizar/editar->1738439429-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-obrigacao-fiscal`,`2343663976-visualizar/editar`,`1738439429-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
      cy.clickIfExist(`[data-cy="2343663976-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1738439429-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-visualização->3494430496-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-visualização`,`3494430496-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3494430496-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-abrir visualização->3494430496-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-abrir visualização`,`3494430496-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3494430496-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-abrir visualização->3494430496-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-abrir visualização`,`3494430496-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3494430496-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-abrir visualização->3494430496-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-abrir visualização`,`3494430496-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3494430496-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-abrir visualização->3494430496-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-abrir visualização`,`3494430496-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3494430496-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-visualizar->3494430496-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-visualizar`,`3494430496-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-visualizar"]`);
      cy.clickIfExist(`[data-cy="3494430496-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-novo->2271791256-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-novo`,`2271791256-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-novo"]`);
      cy.clickIfExist(`[data-cy="2271791256-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-novo->2271791256-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-novo`,`2271791256-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-novo"]`);
      cy.clickIfExist(`[data-cy="2271791256-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->2271791256-novo->2271791256-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-novo`,`2271791256-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-novo"]`);
      cy.fillInput(`[data-cy="2271791256-input-number-ano"] textarea`, `7`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-editar->2271791256-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-editar`,`2271791256-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-editar"]`);
      cy.clickIfExist(`[data-cy="2271791256-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2271791256-editar->2271791256-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2271791256-editar`,`2271791256-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2271791256-editar"]`);
      cy.clickIfExist(`[data-cy="2271791256-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1088714816-novo->1088714816-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1088714816-novo`,`1088714816-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1088714816-novo"]`);
      cy.clickIfExist(`[data-cy="1088714816-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/plano-contas->789851458-visualização->789851458-salvar configuração`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/plano-contas`,`789851458-visualização`,`789851458-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/plano-contas"]`);
      cy.clickIfExist(`[data-cy="789851458-visualização"]`);
      cy.clickIfExist(`[data-cy="789851458-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-executar->870786242-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-executar`,`870786242-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-executar"]`);
      cy.clickIfExist(`[data-cy="870786242-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-executar->870786242-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-executar`,`870786242-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-executar"]`);
      cy.clickIfExist(`[data-cy="870786242-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-executar->870786242-input-P_CONTA and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-executar`,`870786242-input-P_CONTA`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-executar"]`);
      cy.fillInput(`[data-cy="870786242-input-P_CONTA"] textarea`, `cohesive`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-agendamentos->870786242-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-agendamentos`,`870786242-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-agendamentos"]`);
      cy.clickIfExist(`[data-cy="870786242-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-visualização->870786242-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-visualização`,`870786242-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="870786242-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-executar->4285481604-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-executar`,`4285481604-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-executar"]`);
      cy.clickIfExist(`[data-cy="4285481604-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-executar->4285481604-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-executar`,`4285481604-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-executar"]`);
      cy.clickIfExist(`[data-cy="4285481604-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-executar->4285481604-input-P_CONTA and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-executar`,`4285481604-input-P_CONTA`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-executar"]`);
      cy.fillInput(`[data-cy="4285481604-input-P_CONTA"] textarea`, `Marginal`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-agendamentos->4285481604-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-agendamentos`,`4285481604-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4285481604-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-visualização->4285481604-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-visualização`,`4285481604-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4285481604-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-executar->3418140800-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-executar`,`3418140800-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-executar"]`);
      cy.clickIfExist(`[data-cy="3418140800-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-executar->3418140800-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-executar`,`3418140800-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-executar"]`);
      cy.clickIfExist(`[data-cy="3418140800-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-agendamentos->3418140800-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-agendamentos`,`3418140800-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3418140800-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete->1908665769-visualização->1908665769-salvar configuração`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete`,`1908665769-visualização`,`1908665769-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete"]`);
      cy.clickIfExist(`[data-cy="1908665769-visualização"]`);
      cy.clickIfExist(`[data-cy="1908665769-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-executar->3556153674-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-executar`,`3556153674-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-executar"]`);
      cy.clickIfExist(`[data-cy="3556153674-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-executar->3556153674-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-executar`,`3556153674-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-executar"]`);
      cy.clickIfExist(`[data-cy="3556153674-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-agendamentos->3556153674-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-agendamentos`,`3556153674-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3556153674-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-visualização->3556153674-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-visualização`,`3556153674-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3556153674-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-executar->2582422381-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-executar`,`2582422381-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-executar"]`);
      cy.clickIfExist(`[data-cy="2582422381-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-executar->2582422381-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-executar`,`2582422381-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-executar"]`);
      cy.clickIfExist(`[data-cy="2582422381-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-executar->2582422381-input-PMESANO and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-executar`,`2582422381-input-PMESANO`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-executar"]`);
      cy.fillInput(`[data-cy="2582422381-input-PMESANO"] textarea`, `reintermediate`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-agendamentos->2582422381-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-agendamentos`,`2582422381-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2582422381-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-visualização->2582422381-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-visualização`,`2582422381-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2582422381-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-detalhes->2582422381-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-detalhes`,`2582422381-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-detalhes"]`);
      cy.clickIfExist(`[data-cy="2582422381-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-abrir visualização->2582422381-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-abrir visualização`,`2582422381-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2582422381-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-abrir visualização->2582422381-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-abrir visualização`,`2582422381-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2582422381-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-abrir visualização->2582422381-expandir`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-abrir visualização`,`2582422381-expandir`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2582422381-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-abrir visualização->2582422381-download`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-abrir visualização`,`2582422381-download`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2582422381-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-executar->1836703078-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-executar`,`1836703078-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-executar"]`);
      cy.clickIfExist(`[data-cy="1836703078-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-executar->1836703078-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-executar`,`1836703078-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-executar"]`);
      cy.clickIfExist(`[data-cy="1836703078-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-agendamentos->1836703078-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-agendamentos`,`1836703078-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1836703078-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-visualização->1836703078-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-visualização`,`1836703078-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1836703078-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre->771669975-visualização->771669975-salvar configuração`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre`,`771669975-visualização`,`771669975-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre"]`);
      cy.clickIfExist(`[data-cy="771669975-visualização"]`);
      cy.clickIfExist(`[data-cy="771669975-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-executar->1749891658-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-executar`,`1749891658-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-executar"]`);
      cy.clickIfExist(`[data-cy="1749891658-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-executar->1749891658-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-executar`,`1749891658-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-executar"]`);
      cy.clickIfExist(`[data-cy="1749891658-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-agendamentos->1749891658-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-agendamentos`,`1749891658-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1749891658-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/dre-mf->1749891658-visualização->1749891658-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-visualização`,`1749891658-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1749891658-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-executar->3036592371-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-executar`,`3036592371-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-executar"]`);
      cy.clickIfExist(`[data-cy="3036592371-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-executar->3036592371-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-executar`,`3036592371-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-executar"]`);
      cy.clickIfExist(`[data-cy="3036592371-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-executar->3036592371-input-P_DIR_EXCEL and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-executar`,`3036592371-input-P_DIR_EXCEL`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-executar"]`);
      cy.fillInput(`[data-cy="3036592371-input-P_DIR_EXCEL"] textarea`, `Rodovia`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-agendamentos->3036592371-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-agendamentos`,`3036592371-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3036592371-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-visualização->3036592371-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-visualização`,`3036592371-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3036592371-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-executar->41723402-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-executar`,`41723402-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-executar"]`);
      cy.clickIfExist(`[data-cy="41723402-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-executar->41723402-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-executar`,`41723402-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-executar"]`);
      cy.clickIfExist(`[data-cy="41723402-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-executar->41723402-input-PAno and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-executar`,`41723402-input-PAno`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-executar"]`);
      cy.fillInput(`[data-cy="41723402-input-PAno"] textarea`, `Cambridgeshire`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-agendamentos->41723402-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-agendamentos`,`41723402-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-agendamentos"]`);
      cy.clickIfExist(`[data-cy="41723402-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-visualização->41723402-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-visualização`,`41723402-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="41723402-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-executar->2943506080-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-executar`,`2943506080-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-executar"]`);
      cy.clickIfExist(`[data-cy="2943506080-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-executar->2943506080-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-executar`,`2943506080-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-executar"]`);
      cy.clickIfExist(`[data-cy="2943506080-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-executar->2943506080-input-p_ContaInicial-2943506080-input-p_ContaFinal-2943506080-input-p_NumLancto and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-executar`,`2943506080-input-p_ContaInicial-2943506080-input-p_ContaFinal-2943506080-input-p_NumLancto`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-executar"]`);
      cy.fillInput(`[data-cy="2943506080-input-p_ContaInicial"] textarea`, `Bsnia`);
cy.fillInput(`[data-cy="2943506080-input-p_ContaFinal"] textarea`, `Practical Cotton Bike`);
cy.fillInput(`[data-cy="2943506080-input-p_NumLancto"] textarea`, `Visionoriented`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-agendamentos->2943506080-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-agendamentos`,`2943506080-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2943506080-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-visualização->2943506080-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-visualização`,`2943506080-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2943506080-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-executar->3564810834-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-executar`,`3564810834-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-executar"]`);
      cy.clickIfExist(`[data-cy="3564810834-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-executar->3564810834-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-executar`,`3564810834-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-executar"]`);
      cy.clickIfExist(`[data-cy="3564810834-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-executar->3564810834-input-p_ContaInicial-3564810834-input-p_ContaFinal-3564810834-input-p_NumLancto and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-executar`,`3564810834-input-p_ContaInicial-3564810834-input-p_ContaFinal-3564810834-input-p_NumLancto`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-executar"]`);
      cy.fillInput(`[data-cy="3564810834-input-p_ContaInicial"] textarea`, `matrix`);
cy.fillInput(`[data-cy="3564810834-input-p_ContaFinal"] textarea`, `Estrategista`);
cy.fillInput(`[data-cy="3564810834-input-p_NumLancto"] textarea`, `white`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-agendamentos->3564810834-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-agendamentos`,`3564810834-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3564810834-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-visualização->3564810834-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-visualização`,`3564810834-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3564810834-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-executar->787961799-múltipla seleção`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-executar`,`787961799-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-executar"]`);
      cy.clickIfExist(`[data-cy="787961799-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-executar->787961799-agendar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-executar`,`787961799-agendar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-executar"]`);
      cy.clickIfExist(`[data-cy="787961799-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-razao->787961799-executar->787961799-input-CONTA_DE-787961799-input-CONTA_ATE and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-executar`,`787961799-input-CONTA_DE-787961799-input-CONTA_ATE`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-executar"]`);
      cy.fillInput(`[data-cy="787961799-input-CONTA_DE"] textarea`, `Interaes`);
cy.fillInput(`[data-cy="787961799-input-CONTA_ATE"] textarea`, `Djibouti Franc`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-agendamentos->787961799-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-agendamentos`,`787961799-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-agendamentos"]`);
      cy.clickIfExist(`[data-cy="787961799-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-razao->787961799-visualização->787961799-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-visualização`,`787961799-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="787961799-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-agendamentos->4257475929-voltar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`,`4257475929-agendamentos`,`4257475929-voltar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.clickIfExist(`[data-cy="4257475929-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4257475929-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios-apoio->relatorios-apoio/livro-razao-auxiliar->4257475929-visualização->4257475929-item- and submit`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao-auxiliar`,`4257475929-visualização`,`4257475929-item-`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao-auxiliar"]`);
      cy.clickIfExist(`[data-cy="4257475929-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4257475929-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-executar->1985649969-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-executar`,`1985649969-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-executar"]`);
      cy.clickIfExist(`[data-cy="1985649969-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-executar->1985649969-agendar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-executar`,`1985649969-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-executar"]`);
      cy.clickIfExist(`[data-cy="1985649969-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-agendamentos->1985649969-voltar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-agendamentos`,`1985649969-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1985649969-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/geracao-lancamentos-encerramento->1985649969-visualização->1985649969-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-visualização`,`1985649969-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1985649969-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-executar->1324586340-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-executar`,`1324586340-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-executar"]`);
      cy.clickIfExist(`[data-cy="1324586340-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-executar->1324586340-agendar`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-executar`,`1324586340-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-executar"]`);
      cy.clickIfExist(`[data-cy="1324586340-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-agendamentos->1324586340-voltar`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-agendamentos`,`1324586340-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1324586340-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/saldos-mensais->1324586340-visualização->1324586340-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-visualização`,`1324586340-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1324586340-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-detalhes->1324586340-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-detalhes`,`1324586340-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-detalhes"]`);
      cy.clickIfExist(`[data-cy="1324586340-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-abrir visualização->1324586340-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-abrir visualização`,`1324586340-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1324586340-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-abrir visualização->1324586340-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-abrir visualização`,`1324586340-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1324586340-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-abrir visualização->1324586340-expandir`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-abrir visualização`,`1324586340-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1324586340-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-abrir visualização->1324586340-download`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-abrir visualização`,`1324586340-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1324586340-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-executar->2882492716-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-executar`,`2882492716-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-executar"]`);
      cy.clickIfExist(`[data-cy="2882492716-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-executar->2882492716-agendar`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-executar`,`2882492716-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-executar"]`);
      cy.clickIfExist(`[data-cy="2882492716-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-agendamentos->2882492716-voltar`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-agendamentos`,`2882492716-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2882492716-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/compilacao-aglutinadores->2882492716-visualização->2882492716-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-visualização`,`2882492716-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2882492716-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-executar->2878135405-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-executar`,`2878135405-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-executar"]`);
      cy.clickIfExist(`[data-cy="2878135405-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-executar->2878135405-agendar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-executar`,`2878135405-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-executar"]`);
      cy.clickIfExist(`[data-cy="2878135405-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-agendamentos->2878135405-voltar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-agendamentos`,`2878135405-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2878135405-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-compilacao-aglutinadores->2878135405-visualização->2878135405-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-visualização`,`2878135405-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2878135405-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-executar->3534660222-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-executar`,`3534660222-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-executar"]`);
      cy.clickIfExist(`[data-cy="3534660222-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-executar->3534660222-agendar`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-executar`,`3534660222-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-executar"]`);
      cy.clickIfExist(`[data-cy="3534660222-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-agendamentos->3534660222-voltar`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-agendamentos`,`3534660222-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3534660222-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/dre->3534660222-visualização->3534660222-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-visualização`,`3534660222-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3534660222-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-detalhes->3534660222-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-detalhes`,`3534660222-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-detalhes"]`);
      cy.clickIfExist(`[data-cy="3534660222-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-abrir visualização->3534660222-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-abrir visualização`,`3534660222-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3534660222-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-abrir visualização->3534660222-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-abrir visualização`,`3534660222-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3534660222-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-abrir visualização->3534660222-expandir`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-abrir visualização`,`3534660222-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3534660222-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-abrir visualização->3534660222-download`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-abrir visualização`,`3534660222-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3534660222-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-executar->783159763-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-executar`,`783159763-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-executar"]`);
      cy.clickIfExist(`[data-cy="783159763-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-executar->783159763-agendar`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-executar`,`783159763-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-executar"]`);
      cy.clickIfExist(`[data-cy="783159763-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-agendamentos->783159763-voltar`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-agendamentos`,`783159763-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-agendamentos"]`);
      cy.clickIfExist(`[data-cy="783159763-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/dre-moeda-funcional->783159763-visualização->783159763-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-visualização`,`783159763-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="783159763-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-executar->4199836187-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-executar`,`4199836187-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-executar"]`);
      cy.clickIfExist(`[data-cy="4199836187-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-executar->4199836187-agendar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-executar`,`4199836187-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-executar"]`);
      cy.clickIfExist(`[data-cy="4199836187-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-agendamentos->4199836187-voltar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-agendamentos`,`4199836187-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4199836187-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-tabelas-definitivas->4199836187-visualização->4199836187-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-visualização`,`4199836187-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4199836187-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-detalhes->4199836187-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-detalhes`,`4199836187-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-detalhes"]`);
      cy.clickIfExist(`[data-cy="4199836187-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-abrir visualização->4199836187-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-abrir visualização`,`4199836187-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4199836187-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-abrir visualização->4199836187-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-abrir visualização`,`4199836187-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4199836187-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-abrir visualização->4199836187-expandir`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-abrir visualização`,`4199836187-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4199836187-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-abrir visualização->4199836187-download`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-abrir visualização`,`4199836187-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4199836187-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos->3453673399-executar->3453673399-agendar`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-executar`,`3453673399-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-executar"]`);
      cy.clickIfExist(`[data-cy="3453673399-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-estabelecimentos->3453673399-agendamentos->3453673399-voltar`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-agendamentos`,`3453673399-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/carga-estabelecimentos->3453673399-visualização->3453673399-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/carga-estabelecimentos`,`3453673399-visualização`,`3453673399-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="3453673399-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3453673399-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-remover item`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-novo->2438618037-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-novo`,`2438618037-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.clickIfExist(`[data-cy="2438618037-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-novo->2438618037-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-novo`,`2438618037-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.clickIfExist(`[data-cy="2438618037-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-novo->2438618037-powerselect-numero-2438618037-input-codigo-2438618037-textarea-ultimaDescricao and submit`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-novo`,`2438618037-powerselect-numero-2438618037-input-codigo-2438618037-textarea-ultimaDescricao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2438618037-powerselect-numero"] input`);
cy.fillInput(`[data-cy="2438618037-input-codigo"] textarea`, `solid state`);
cy.fillInput(`[data-cy="2438618037-textarea-ultimaDescricao"] input`, `payment`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-remover item`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-novo->2438618037-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-novo`,`2438618037-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.clickIfExist(`[data-cy="2438618037-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-novo->2438618037-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-novo`,`2438618037-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.clickIfExist(`[data-cy="2438618037-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-novo->2438618037-powerselect-numero-2438618037-input-codigo-2438618037-textarea-ultimaDescricao and submit`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-novo`,`2438618037-powerselect-numero-2438618037-input-codigo-2438618037-textarea-ultimaDescricao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2438618037-powerselect-numero"] input`);
cy.fillInput(`[data-cy="2438618037-input-codigo"] textarea`, `Health`);
cy.fillInput(`[data-cy="2438618037-textarea-ultimaDescricao"] input`, `monitor`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-remover item`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-eyeoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-eyeoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-deleteoutlined`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-deleteoutlined`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-selecionar->3063145326-novo`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-selecionar`,`3063145326-novo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-selecionar"]`);
      cy.clickIfExist(`[data-cy="3063145326-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-selecionar->3063145326-power-search-button`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-selecionar`,`3063145326-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-selecionar"]`);
      cy.clickIfExist(`[data-cy="3063145326-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-voltar às obrigações do módulo->1302461303-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-voltar às obrigações do módulo`,`1302461303-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-voltar às obrigações do módulo"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-nova solicitação->1302461303-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-nova solicitação`,`1302461303-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="1302461303-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-nova solicitação->1302461303-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-nova solicitação`,`1302461303-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="1302461303-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-agendamentos->3494430496-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-agendamentos`,`3494430496-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3494430496-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-agendamentos->3494430496-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-agendamentos`,`3494430496-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3494430496-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3494430496-abrir visualização->3494430496-expandir->3494430496-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3494430496-abrir visualização`,`3494430496-expandir`,`3494430496-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3494430496-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3494430496-expandir"]`);
      cy.clickIfExist(`[data-cy="3494430496-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos->870786242-executar->870786242-múltipla seleção->870786242-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos`,`870786242-executar`,`870786242-múltipla seleção`,`870786242-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos"]`);
      cy.clickIfExist(`[data-cy="870786242-executar"]`);
      cy.clickIfExist(`[data-cy="870786242-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="870786242-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/lancamento-saldos-mf->4285481604-executar->4285481604-múltipla seleção->4285481604-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/lancamento-saldos-mf`,`4285481604-executar`,`4285481604-múltipla seleção`,`4285481604-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/lancamento-saldos-mf"]`);
      cy.clickIfExist(`[data-cy="4285481604-executar"]`);
      cy.clickIfExist(`[data-cy="4285481604-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4285481604-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/diferencas-debito-credito->3418140800-executar->3418140800-múltipla seleção->3418140800-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/diferencas-debito-credito`,`3418140800-executar`,`3418140800-múltipla seleção`,`3418140800-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/diferencas-debito-credito"]`);
      cy.clickIfExist(`[data-cy="3418140800-executar"]`);
      cy.clickIfExist(`[data-cy="3418140800-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3418140800-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-contas-referenciais->3556153674-executar->3556153674-múltipla seleção->3556153674-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-contas-referenciais`,`3556153674-executar`,`3556153674-múltipla seleção`,`3556153674-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-contas-referenciais"]`);
      cy.clickIfExist(`[data-cy="3556153674-executar"]`);
      cy.clickIfExist(`[data-cy="3556153674-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3556153674-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-executar->2582422381-múltipla seleção->2582422381-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-executar`,`2582422381-múltipla seleção`,`2582422381-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-executar"]`);
      cy.clickIfExist(`[data-cy="2582422381-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2582422381-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balancete-plano-estatutario->2582422381-abrir visualização->2582422381-expandir->2582422381-diminuir`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balancete-plano-estatutario`,`2582422381-abrir visualização`,`2582422381-expandir`,`2582422381-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balancete-plano-estatutario"]`);
      cy.clickIfExist(`[data-cy="2582422381-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2582422381-expandir"]`);
      cy.clickIfExist(`[data-cy="2582422381-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/balanco-patrimonial-mf->1836703078-executar->1836703078-múltipla seleção->1836703078-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/balanco-patrimonial-mf`,`1836703078-executar`,`1836703078-múltipla seleção`,`1836703078-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/balanco-patrimonial-mf"]`);
      cy.clickIfExist(`[data-cy="1836703078-executar"]`);
      cy.clickIfExist(`[data-cy="1836703078-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1836703078-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/dre-mf->1749891658-executar->1749891658-múltipla seleção->1749891658-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/dre-mf`,`1749891658-executar`,`1749891658-múltipla seleção`,`1749891658-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/dre-mf"]`);
      cy.clickIfExist(`[data-cy="1749891658-executar"]`);
      cy.clickIfExist(`[data-cy="1749891658-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1749891658-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/conferencia-aglutinacao->3036592371-executar->3036592371-múltipla seleção->3036592371-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/conferencia-aglutinacao`,`3036592371-executar`,`3036592371-múltipla seleção`,`3036592371-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/conferencia-aglutinacao"]`);
      cy.clickIfExist(`[data-cy="3036592371-executar"]`);
      cy.clickIfExist(`[data-cy="3036592371-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3036592371-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/vigencia-aglutinadores->41723402-executar->41723402-múltipla seleção->41723402-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/vigencia-aglutinadores`,`41723402-executar`,`41723402-múltipla seleção`,`41723402-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/vigencia-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="41723402-executar"]`);
      cy.clickIfExist(`[data-cy="41723402-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="41723402-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-geral->2943506080-executar->2943506080-múltipla seleção->2943506080-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-geral`,`2943506080-executar`,`2943506080-múltipla seleção`,`2943506080-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-geral"]`);
      cy.clickIfExist(`[data-cy="2943506080-executar"]`);
      cy.clickIfExist(`[data-cy="2943506080-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2943506080-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-diario-auxiliar->3564810834-executar->3564810834-múltipla seleção->3564810834-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-diario-auxiliar`,`3564810834-executar`,`3564810834-múltipla seleção`,`3564810834-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-diario-auxiliar"]`);
      cy.clickIfExist(`[data-cy="3564810834-executar"]`);
      cy.clickIfExist(`[data-cy="3564810834-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3564810834-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios-apoio->relatorios-apoio/livro-razao->787961799-executar->787961799-múltipla seleção->787961799-cancelar`, () => {
const actualId = [`root`,`relatorios-apoio`,`relatorios-apoio/livro-razao`,`787961799-executar`,`787961799-múltipla seleção`,`787961799-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios-apoio/livro-razao"]`);
      cy.clickIfExist(`[data-cy="787961799-executar"]`);
      cy.clickIfExist(`[data-cy="787961799-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="787961799-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-lancamentos-encerramento->1985649969-executar->1985649969-múltipla seleção->1985649969-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-lancamentos-encerramento`,`1985649969-executar`,`1985649969-múltipla seleção`,`1985649969-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-lancamentos-encerramento"]`);
      cy.clickIfExist(`[data-cy="1985649969-executar"]`);
      cy.clickIfExist(`[data-cy="1985649969-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1985649969-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-executar->1324586340-múltipla seleção->1324586340-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-executar`,`1324586340-múltipla seleção`,`1324586340-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-executar"]`);
      cy.clickIfExist(`[data-cy="1324586340-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1324586340-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldos-mensais->1324586340-abrir visualização->1324586340-expandir->1324586340-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/saldos-mensais`,`1324586340-abrir visualização`,`1324586340-expandir`,`1324586340-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/saldos-mensais"]`);
      cy.clickIfExist(`[data-cy="1324586340-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1324586340-expandir"]`);
      cy.clickIfExist(`[data-cy="1324586340-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/compilacao-aglutinadores->2882492716-executar->2882492716-múltipla seleção->2882492716-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/compilacao-aglutinadores`,`2882492716-executar`,`2882492716-múltipla seleção`,`2882492716-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2882492716-executar"]`);
      cy.clickIfExist(`[data-cy="2882492716-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2882492716-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-compilacao-aglutinadores->2878135405-executar->2878135405-múltipla seleção->2878135405-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-compilacao-aglutinadores`,`2878135405-executar`,`2878135405-múltipla seleção`,`2878135405-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-compilacao-aglutinadores"]`);
      cy.clickIfExist(`[data-cy="2878135405-executar"]`);
      cy.clickIfExist(`[data-cy="2878135405-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2878135405-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-executar->3534660222-múltipla seleção->3534660222-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-executar`,`3534660222-múltipla seleção`,`3534660222-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-executar"]`);
      cy.clickIfExist(`[data-cy="3534660222-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3534660222-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre->3534660222-abrir visualização->3534660222-expandir->3534660222-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/dre`,`3534660222-abrir visualização`,`3534660222-expandir`,`3534660222-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre"]`);
      cy.clickIfExist(`[data-cy="3534660222-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3534660222-expandir"]`);
      cy.clickIfExist(`[data-cy="3534660222-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dre-moeda-funcional->783159763-executar->783159763-múltipla seleção->783159763-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/dre-moeda-funcional`,`783159763-executar`,`783159763-múltipla seleção`,`783159763-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dre-moeda-funcional"]`);
      cy.clickIfExist(`[data-cy="783159763-executar"]`);
      cy.clickIfExist(`[data-cy="783159763-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="783159763-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-executar->4199836187-múltipla seleção->4199836187-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-executar`,`4199836187-múltipla seleção`,`4199836187-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-executar"]`);
      cy.clickIfExist(`[data-cy="4199836187-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4199836187-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-definitivas->4199836187-abrir visualização->4199836187-expandir->4199836187-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-definitivas`,`4199836187-abrir visualização`,`4199836187-expandir`,`4199836187-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-definitivas"]`);
      cy.clickIfExist(`[data-cy="4199836187-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4199836187-expandir"]`);
      cy.clickIfExist(`[data-cy="4199836187-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-novo->2814644613-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.clickIfExist(`[data-cy="2814644613-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-novo->2814644613-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.clickIfExist(`[data-cy="2814644613-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/tabelas-codigos->1081939380-eyeoutlined->1545531836-novo->2814644613-input-descricao and submit`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/tabelas-codigos`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-input-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/tabelas-codigos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.fillInput(`[data-cy="2814644613-input-descricao"] textarea`, `Produtor`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-novo->2814644613-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.clickIfExist(`[data-cy="2814644613-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-novo->2814644613-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.clickIfExist(`[data-cy="2814644613-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/centro-custos->1081939380-eyeoutlined->1545531836-novo->2814644613-input-descricao and submit`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/centro-custos`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-input-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/centro-custos"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.fillInput(`[data-cy="2814644613-input-descricao"] textarea`, `Practical Frozen Pants`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-novo->2814644613-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.clickIfExist(`[data-cy="2814644613-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-novo->2814644613-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.clickIfExist(`[data-cy="2814644613-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/plano-contas-referencial->1081939380-eyeoutlined->1545531836-novo->2814644613-input-descricao and submit`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/plano-contas-referencial`,`1081939380-eyeoutlined`,`1545531836-novo`,`2814644613-input-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/plano-contas-referencial"]`);
      cy.clickIfExist(`[data-cy="1081939380-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="1545531836-novo"]`);
      cy.fillInput(`[data-cy="2814644613-input-descricao"] textarea`, `Granite`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-selecionar->3063145326-novo->2279674427-salvar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-selecionar`,`3063145326-novo`,`2279674427-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-selecionar"]`);
      cy.clickIfExist(`[data-cy="3063145326-novo"]`);
      cy.clickIfExist(`[data-cy="2279674427-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-selecionar->3063145326-novo->2279674427-voltar`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-selecionar`,`3063145326-novo`,`2279674427-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-selecionar"]`);
      cy.clickIfExist(`[data-cy="3063145326-novo"]`);
      cy.clickIfExist(`[data-cy="2279674427-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values tabelas-corporativo->tabelas-corporativo/cadastros-contabeis->tabelas-corporativo/cadastros-contabeis/livros-contabeis->3311812367-selecionar->3063145326-novo->2279674427-input-number-numeroLivro-2279674427-input-codHashAux-2279674427-powerselect-icodIdModLivro and submit`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativo/cadastros-contabeis`,`tabelas-corporativo/cadastros-contabeis/livros-contabeis`,`3311812367-selecionar`,`3063145326-novo`,`2279674427-input-number-numeroLivro-2279674427-input-codHashAux-2279674427-powerselect-icodIdModLivro`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativo/cadastros-contabeis/livros-contabeis"]`);
      cy.clickIfExist(`[data-cy="3311812367-selecionar"]`);
      cy.clickIfExist(`[data-cy="3063145326-novo"]`);
      cy.fillInput(`[data-cy="2279674427-input-number-numeroLivro"] textarea`, `6`);
cy.fillInput(`[data-cy="2279674427-input-codHashAux"] textarea`, `Oficial`);
cy.fillInputPowerSelect(`[data-cy="2279674427-powerselect-icodIdModLivro"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values obrigacoes->obrigacoes/solicitacoes-resultados->1302461303-ir para todas as obrigações->1302461303-agendamentos->3494430496-visualização->3494430496-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`1302461303-ir para todas as obrigações`,`1302461303-agendamentos`,`3494430496-visualização`,`3494430496-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="1302461303-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="1302461303-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3494430496-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3494430496-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
