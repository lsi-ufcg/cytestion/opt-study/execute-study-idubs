describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element movimentacao-contabil->movimentacao-contabil/notas->103218009-novo->3433510192-salvar`, () => {
    cy.clickCauseExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickCauseExist(`[data-cy="movimentacao-contabil/notas"]`);
    cy.clickCauseExist(`[data-cy="103218009-novo"]`);
    cy.clickCauseExist(`[data-cy="3433510192-salvar"]`);
    cy.checkErrorsWereDetected();
  });
});
