describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario`, () => {
const actualId = [`root`,`dicionario`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces`, () => {
const actualId = [`root`,`intefaces`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element open`, () => {
const actualId = [`root`,`open`];
    cy.clickIfExist(`[data-cy="open"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap`, () => {
const actualId = [`root`,`sap`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-r12`, () => {
const actualId = [`root`,`oracle-r12`];
    cy.clickIfExist(`[data-cy="oracle-r12"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-erp-cloud`, () => {
const actualId = [`root`,`oracle-erp-cloud`];
    cy.clickIfExist(`[data-cy="oracle-erp-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw`, () => {
const actualId = [`root`,`sfw`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios/processos-customizados`, () => {
const actualId = [`root`,`relatorios/processos-customizados`];
    cy.clickIfExist(`[data-cy="relatorios/processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
const actualId = [`root`,`downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/repositorio`, () => {
const actualId = [`root`,`dicionario`,`dicionario/repositorio`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/repositorio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes`, () => {
const actualId = [`root`,`dicionario`,`dicionario/visoes`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/visoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos`, () => {
const actualId = [`root`,`dicionario`,`dicionario/relacionamentos`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/relacionamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios`, () => {
const actualId = [`root`,`dicionario`,`dicionario/dominios`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/dominios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis`, () => {
const actualId = [`root`,`dicionario`,`dicionario/variaveis`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/variaveis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export`, () => {
const actualId = [`root`,`intefaces`,`intefaces/import-export`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/import-export"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao`, () => {
const actualId = [`root`,`intefaces`,`intefaces/manutencao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo`, () => {
const actualId = [`root`,`intefaces`,`intefaces/grupo`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/grupo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element open->open/equivalencia-codigo`, () => {
const actualId = [`root`,`open`,`open/equivalencia-codigo`];
    cy.clickIfExist(`[data-cy="open"]`);
    cy.clickIfExist(`[data-cy="open/equivalencia-codigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc`, () => {
const actualId = [`root`,`sap`,`sap/parametro-idoc`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/parametro-idoc"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos`, () => {
const actualId = [`root`,`sap`,`sap/consulta-codigos`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/consulta-codigos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/parametro-integração`, () => {
const actualId = [`root`,`sap`,`sap/parametro-integração`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/parametro-integração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/equivalencia-codigo`, () => {
const actualId = [`root`,`sap`,`sap/equivalencia-codigo`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/equivalencia-codigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/msg-pendente`, () => {
const actualId = [`root`,`sap`,`sap/msg-pendente`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/msg-pendente"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-r12->oracle-r12/r12-equivalencia-cod`, () => {
const actualId = [`root`,`oracle-r12`,`oracle-r12/r12-equivalencia-cod`];
    cy.clickIfExist(`[data-cy="oracle-r12"]`);
    cy.clickIfExist(`[data-cy="oracle-r12/r12-equivalencia-cod"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/carga-oracle-cloud`, () => {
const actualId = [`root`,`oracle-erp-cloud`,`oracle-erp-cloud/carga-oracle-cloud`];
    cy.clickIfExist(`[data-cy="oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="oracle-erp-cloud/carga-oracle-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/emissao-oracle-cloud`, () => {
const actualId = [`root`,`oracle-erp-cloud`,`oracle-erp-cloud/emissao-oracle-cloud`];
    cy.clickIfExist(`[data-cy="oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="oracle-erp-cloud/emissao-oracle-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/execucao-interface`, () => {
const actualId = [`root`,`processos`,`processos/execucao-interface`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/execucao-interface"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl`, () => {
const actualId = [`root`,`processos`,`processos/geracao-loader-ctl`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/geracao-loader-ctl"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-intermediarias`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-tabelas-intermediarias`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-intermediarias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces`, () => {
const actualId = [`root`,`processos`,`processos/testes-interfaces`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/testes-interfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/sap`, () => {
const actualId = [`root`,`processos`,`processos/sap`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/sap"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen`, () => {
const actualId = [`root`,`relatorios`,`relatorios/listaRegistrosDeOpen`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/listaRegistrosDeOpen"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase`, () => {
const actualId = [`root`,`relatorios`,`relatorios/listaDofsExistentesBase`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/listaDofsExistentesBase"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces`, () => {
const actualId = [`root`,`relatorios`,`relatorios/documentacaoOpenInterfaces`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/documentacaoOpenInterfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces`, () => {
const actualId = [`root`,`relatorios`,`relatorios/testesInterfaces`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/testesInterfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw`, () => {
const actualId = [`root`,`sfw`,`sfw/integracao-com-sfw`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/processos`, () => {
const actualId = [`root`,`sfw`,`sfw/processos`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop`, () => {
const actualId = [`root`,`sfw`,`sfw/regra-coop`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/regra-coop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao`, () => {
const actualId = [`root`,`intefaces`,`intefaces/manutencao`,`intefaces/manutencao/edicao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/importacao`, () => {
const actualId = [`root`,`intefaces`,`intefaces/manutencao`,`intefaces/manutencao/importacao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/importacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao`, () => {
const actualId = [`root`,`intefaces`,`intefaces/manutencao`,`intefaces/manutencao/exportacao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro`, () => {
const actualId = [`root`,`processos`,`processos/sap`,`processos/sap/limpeza-log-sap-x-synchro`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/sap"]`);
    cy.clickIfExist(`[data-cy="processos/sap/limpeza-log-sap-x-synchro"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-inicial`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-fiscal`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/retorno-guia`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-integra`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-dados-contabeis`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-dados-contabeis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/carga-dados-fiscais`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/carga-dados-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/carga-dados-contabeis`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/carga-dados-contabeis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/dofs-rejeitados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/dofs-rejeitados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/registros-rejeitados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/registros-rejeitados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/valores-parametros`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/valores-parametros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/exec-cargas-solicitadas`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/exec-cargas-solicitadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario`, () => {
const actualId = [`root`,`relatorios`,`relatorios/integracao-oracle-r12`,`relatorios/integracao-oracle-r12/processo-limpeza-inventario`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/processo-limpeza-inventario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades`, () => {
const actualId = [`root`,`sfw`,`sfw/integracao-com-sfw`,`sfw/integracao-com-sfw/propriedades`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/propriedades"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes`, () => {
const actualId = [`root`,`sfw`,`sfw/integracao-com-sfw`,`sfw/integracao-com-sfw/solicitacoes`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/solicitacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar`, () => {
const actualId = [`root`,`sfw`,`sfw/integracao-com-sfw`,`sfw/integracao-com-sfw/consultar`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/consultar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade`, () => {
const actualId = [`root`,`sfw`,`sfw/integracao-com-sfw`,`sfw/integracao-com-sfw/periodicidade`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/periodicidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento`, () => {
const actualId = [`root`,`sfw`,`sfw/integracao-com-sfw`,`sfw/integracao-com-sfw/por-estabelecimento`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/por-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-nfe"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-ecf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/relatorio-pis-cofins`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/relatorio-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/contrib-social-retida-fonte`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/contrib-social-retida-fonte"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/indice-financeiro-sintetico`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/indice-financeiro-sintetico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico`, () => {
const actualId = [`root`,`sfw`,`sfw/relatorios-de-apoio`,`sfw/relatorios-de-apoio/indice-financeiro-analitico`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/indice-financeiro-analitico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins`, () => {
const actualId = [`root`,`sfw`,`sfw/processos`,`sfw/processos/exclusao-icms-base-calculo-pis-cofins`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/processos"]`);
    cy.clickIfExist(`[data-cy="sfw/processos/exclusao-icms-base-calculo-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos`, () => {
const actualId = [`root`,`sfw`,`sfw/processos`,`sfw/processos/carga-estabelecimentos`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/processos"]`);
    cy.clickIfExist(`[data-cy="sfw/processos/carga-estabelecimentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-inicial`,`processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-inicial`,`processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-fiscal`,`processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-fiscal`,`processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-fiscal`,`processos/oracle-erp-cloud/carga-fiscal/carga-programada`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal/carga-programada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/retorno-guia`,`processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/retorno-guia`,`processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/retorno-guia`,`processos/oracle-erp-cloud/retorno-guia/enviar-gnre`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/enviar-gnre"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/retorno-guia`,`processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-integra`,`processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, () => {
const actualId = [`root`,`processos`,`processos/oracle-erp-cloud`,`processos/oracle-erp-cloud/carga-integra`,`processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
