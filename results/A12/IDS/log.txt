yarn run v1.22.19
$ bash collectCPU.sh > executionMetrics.txt & NODE_ENV=generation EXECUTION_MODE=production node --max-old-space-size=12288 entrypoint.js
EXECUTION_MODE=production
NODE_ENV=generation
BASE_URL=/http://system-A12
BASE_URL_API=/http://system-A12-api
USER_LOGIN=XXXXXXXXXXXXXXXXX
USER_PASSWORD=XXXXXXXXXXXXX
KEYCLOAK_URL=XXXXXXXXXXXXXXXXX
CLIENT_SECRET=**********
E2E_FOLDER=../e2e/exploratory/A12/squadgqs-teste
ERROR_MESSAGE=Ocorreu um erro inesperado
CHECK_400=true
CHECK_500=true
IGNORE_ENDPOINT_ERROR=corporativo/syn-revisao-kit/all:503,corporativo/identidade-visual/logo/modulo:404
[1;92mWelcome to Cytestion![0;0m Version: 1.7. A tool for automated GUI testing of web applications, using the cypress framework
[1;92mINFO:[0;0m Verifying existence of cypress config file and set environment variables
[1;92mINFO:[0;0m Checks if the tests have already been generated, inform it will be replaced.
[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 1
[1;92mINFO:[0;0m Total test cases without skip: 1
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 1
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/A12/squadgqs-teste/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/synchro/cytestion-utilization/e2e/exploratory/A12/sq │
  │                 uadgqs-teste/tmp/batches/**/*.cy.js                                            │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Visits index page (9949ms)


  1 passing (10s)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        1                                                                                │
  │ Passing:      1                                                                                │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     10 seconds                                                                       │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔  cytestion_1.cy.js                        00:10        1        1        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✔  All specs passed!                        00:10        1        1        -        -        -  

[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 15
[1;92mINFO:[0;0m Total test cases without skip: 14
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 14
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/A12/squadgqs-teste/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/synchro/cytestion-utilization/e2e/exploratory/A12/sq │
  │                 uadgqs-teste/tmp/batches/**/*.cy.js                                            │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Click on element home (15539ms)
    ✓ Click on element dicionario (10153ms)
    ✓ Click on element intefaces (10058ms)
    ✓ Click on element open (10022ms)
    ✓ Click on element sap (10019ms)
    ✓ Click on element oracle-r12 (10002ms)
    ✓ Click on element oracle-erp-cloud (10131ms)
    ✓ Click on element processos (10094ms)
    ✓ Click on element relatorios (10070ms)
    ✓ Click on element sfw (10030ms)
    ✓ Click on element relatorios/processos-customizados (14519ms)
    ✓ Click on element downloads (14362ms)
    ✓ Click on element collapse-menu (10044ms)
    ✓ Click on element modules-menu (14610ms)


  14 passing (3m)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        14                                                                               │
  │ Passing:      14                                                                               │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     2 minutes, 41 seconds                                                            │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔  cytestion_1.cy.js                        02:41       14       14        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✔  All specs passed!                        02:41       14       14        -        -        -  

[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 47
[1;92mINFO:[0;0m Total test cases without skip: 32
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 32
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/A12/squadgqs-teste/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/synchro/cytestion-utilization/e2e/exploratory/A12/sq │
  │                 uadgqs-teste/tmp/batches/**/*.cy.js                                            │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Click on element dicionario->dicionario/repositorio (17090ms)
    ✓ Click on element dicionario->dicionario/visoes (15748ms)
    ✓ Click on element dicionario->dicionario/relacionamentos (15830ms)
    ✓ Click on element dicionario->dicionario/dominios (15767ms)
    ✓ Click on element dicionario->dicionario/variaveis (15823ms)
    ✓ Click on element intefaces->intefaces/import-export (15835ms)
    ✓ Click on element intefaces->intefaces/manutencao (11424ms)
    ✓ Click on element intefaces->intefaces/grupo (15742ms)
    ✓ Click on element open->open/equivalencia-codigo (15697ms)
    ✓ Click on element sap->sap/parametro-idoc (15688ms)
    ✓ Click on element sap->sap/consulta-codigos (15734ms)
    ✓ Click on element sap->sap/parametro-integração (15762ms)
    ✓ Click on element sap->sap/equivalencia-codigo (15684ms)
    ✓ Click on element sap->sap/msg-pendente (15766ms)
    ✓ Click on element oracle-r12->oracle-r12/r12-equivalencia-cod (15729ms)
    ✓ Click on element oracle-erp-cloud->oracle-erp-cloud/carga-oracle-cloud (15910ms)
    ✓ Click on element oracle-erp-cloud->oracle-erp-cloud/emissao-oracle-cloud (15866ms)
    ✓ Click on element processos->processos/execucao-interface (15841ms)
    ✓ Click on element processos->processos/geracao-loader-ctl (15856ms)
    ✓ Click on element processos->processos/limpeza-tabelas-intermediarias (15821ms)
    ✓ Click on element processos->processos/testes-interfaces (15699ms)
    ✓ Click on element processos->processos/sap (11302ms)
    ✓ Click on element processos->processos/oracle-erp-cloud (11313ms)
    ✓ Click on element relatorios->relatorios/listaRegistrosDeOpen (15640ms)
    ✓ Click on element relatorios->relatorios/listaDofsExistentesBase (15922ms)
    ✓ Click on element relatorios->relatorios/documentacaoOpenInterfaces (15831ms)
    ✓ Click on element relatorios->relatorios/testesInterfaces (15717ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12 (11416ms)
    ✓ Click on element sfw->sfw/integracao-com-sfw (11332ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio (11401ms)
    ✓ Click on element sfw->sfw/processos (11392ms)
    ✓ Click on element sfw->sfw/regra-coop (15865ms)


  32 passing (8m)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        32                                                                               │
  │ Passing:      32                                                                               │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     7 minutes, 59 seconds                                                            │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔  cytestion_1.cy.js                        07:59       32       32        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✔  All specs passed!                        07:59       32       32        -        -        -  

[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 79
[1;92mINFO:[0;0m Total test cases without skip: 32
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 32
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/A12/squadgqs-teste/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/synchro/cytestion-utilization/e2e/exploratory/A12/sq │
  │                 uadgqs-teste/tmp/batches/**/*.cy.js                                            │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao (13255ms)
    ✓ Click on element intefaces->intefaces/manutencao->intefaces/manutencao/importacao (17121ms)
    ✓ Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao (17237ms)
    ✓ Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro (17213ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial (12696ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal (12926ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia (12767ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra (12841ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis (17085ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais (17155ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis (17153ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados (17169ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados (17130ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros (17221ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas (17143ms)
    ✓ Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario (17171ms)
    ✓ Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades (17143ms)
    ✓ Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes (17208ms)
    ✓ Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar (17271ms)
    ✓ Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade (17264ms)
    ✓ Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento (17330ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe (17088ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf (17138ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom (17141ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites (17190ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins (16977ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins (17176ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte (17082ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico (17308ms)
    ✓ Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico (17165ms)
    ✓ Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins (17300ms)
    ✓ Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos (17218ms)


  32 passing (9m)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        32                                                                               │
  │ Passing:      32                                                                               │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     8 minutes, 53 seconds                                                            │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔  cytestion_1.cy.js                        08:53       32       32        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✔  All specs passed!                        08:53       32       32        -        -        -  

[1;92mINFO:[0;0m Generating tests
[1;92mINFO:[0;0m Total test cases: 90
[1;92mINFO:[0;0m Total test cases without skip: 11
[1;92mINFO:[0;0m Batch file created: cytestion_1.cy.js. Total test cases in batch file: 11
$ ./node_modules/.bin/cypress run -s '../e2e/exploratory/A12/squadgqs-teste/tmp/batches/**/*.cy.js'

================================================================================

  (Run Starting)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Cypress:        13.6.2                                                                         │
  │ Browser:        Electron 114 (headless)                                                        │
  │ Node Version:   v16.20.0 (/home/lsi/.nvm/versions/node/v16.20.0/bin/node)                      │
  │ Specs:          1 found (cytestion_1.cy.js)                                                    │
  │ Searched:       /home/lsi/tcc/synchro/cytestion-utilization/e2e/exploratory/A12/sq │
  │                 uadgqs-teste/tmp/batches/**/*.cy.js                                            │
  │ Experiments:    experimentalMemoryManagement=true                                              │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


────────────────────────────────────────────────────────────────────────────────────────────────────
                                                                                                    
  Running:  cytestion_1.cy.js                                                               (1 of 1)


  Cytestion
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros (14606ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority (18514ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais (14882ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico (18653ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada (18414ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento (18452ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias (18597ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre (18506ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre (18643ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw (18449ms)
    ✓ Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw (18582ms)


  11 passing (3m)


  (Results)

  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ Tests:        11                                                                               │
  │ Passing:      11                                                                               │
  │ Failing:      0                                                                                │
  │ Pending:      0                                                                                │
  │ Skipped:      0                                                                                │
  │ Screenshots:  0                                                                                │
  │ Video:        false                                                                            │
  │ Duration:     3 minutes, 18 seconds                                                            │
  │ Spec Ran:     cytestion_1.cy.js                                                                │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘


================================================================================

  (Run Finished)


       Spec                                              Tests  Passing  Failing  Pending  Skipped  
  ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
  │ ✔  cytestion_1.cy.js                        03:18       11       11        -        -        - │
  └────────────────────────────────────────────────────────────────────────────────────────────────┘
    ✔  All specs passed!                        03:18       11       11        -        -        -  

[1;92mINFO:[0;0m Generating tests
Test file generated! (../e2e/exploratory/A12/squadgqs-teste/cytestion.cy.js)
Time execution: 23:39.259 (m:ss.mmm)
Done in 1419.36s.
