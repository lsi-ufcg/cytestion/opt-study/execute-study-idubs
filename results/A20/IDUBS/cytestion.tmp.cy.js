describe('Cytestion', () => {
  beforeEach(() => {
    cy.login(Cypress.env('userLogin'), Cypress.env('userPassword'), Cypress.env('loginUrl'));
    cy.selectEntity();
    cy.closeAlerts();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tce-logo-button`, () => {
    const actualId = [`root`, `tce-logo-button`];
    cy.clickIfExist(`[data-cy="tce-logo-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu-button`, () => {
    const actualId = [`root`, `collapse-menu-button`];
    cy.clickIfExist(`[data-cy="collapse-menu-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element entity-button`, () => {
    const actualId = [`root`, `entity-button`];
    cy.clickIfExist(`[data-cy="entity-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element scale-plus-button`, () => {
    const actualId = [`root`, `scale-plus-button`];
    cy.clickIfExist(`[data-cy="scale-plus-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element scale-standard-button`, () => {
    const actualId = [`root`, `scale-standard-button`];
    cy.clickIfExist(`[data-cy="scale-standard-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element scale-minus-button`, () => {
    const actualId = [`root`, `scale-minus-button`];
    cy.clickIfExist(`[data-cy="scale-minus-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element layout-button`, () => {
    const actualId = [`root`, `layout-button`];
    cy.clickIfExist(`[data-cy="layout-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element notification-button`, () => {
    const actualId = [`root`, `notification-button`];
    cy.clickIfExist(`[data-cy="notification-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element profile-button`, () => {
    const actualId = [`root`, `profile-button`];
    cy.clickIfExist(`[data-cy="profile-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element catalogo de produtos`, () => {
    const actualId = [`root`, `catalogo de produtos`];
    cy.clickIfExist(`[data-cy="catalogo de produtos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element termo de referencia`, () => {
    const actualId = [`root`, `termo de referencia`];
    cy.clickIfExist(`[data-cy="termo de referencia"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas`, () => {
    const actualId = [`root`, `cadastros e consultas`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria`, () => {
    const actualId = [`root`, `auditoria`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element alertas`, () => {
    const actualId = [`root`, `alertas`];
    cy.clickIfExist(`[data-cy="alertas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao`, () => {
    const actualId = [`root`, `administracao`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element seguranca`, () => {
    const actualId = [`root`, `seguranca`];
    cy.clickIfExist(`[data-cy="seguranca"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obras`, () => {
    const actualId = [`root`, `obras`];
    cy.clickIfExist(`[data-cy="obras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-3 nova(s) no ultimo ano`, () => {
    const actualId = [`root`, `2147483695-3 nova(s) no ultimo ano`];
    cy.clickIfExist(`[data-cy="2147483695-3 nova(s) no ultimo ano"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-5 nova(s) no ultimo ano`, () => {
    const actualId = [`root`, `2147483695-5 nova(s) no ultimo ano`];
    cy.clickIfExist(`[data-cy="2147483695-5 nova(s) no ultimo ano"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-2 nova(s) no ultimo ano`, () => {
    const actualId = [`root`, `2147483695-2 nova(s) no ultimo ano`];
    cy.clickIfExist(`[data-cy="2147483695-2 nova(s) no ultimo ano"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-10 novo(s) no ultimo ano`, () => {
    const actualId = [`root`, `2147483695-10 novo(s) no ultimo ano`];
    cy.clickIfExist(`[data-cy="2147483695-10 novo(s) no ultimo ano"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-15 novo(s) no ultimo ano`, () => {
    const actualId = [`root`, `2147483695-15 novo(s) no ultimo ano`];
    cy.clickIfExist(`[data-cy="2147483695-15 novo(s) no ultimo ano"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-exibir todos os alertas...`, () => {
    const actualId = [`root`, `2147483695-exibir todos os alertas...`];
    cy.clickIfExist(`[data-cy="2147483695-exibir todos os alertas..."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 2147483695-visualizar`, () => {
    const actualId = [`root`, `2147483695-visualizar`];
    cy.clickIfExist(`[data-cy="2147483695-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element entity-button->2147483695-cancelar`, () => {
    const actualId = [`root`, `entity-button`, `2147483695-cancelar`];
    cy.clickIfExist(`[data-cy="entity-button"]`);
    cy.clickIfExist(`[data-cy="2147483695-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element profile-button->recarregar-permissoes-button`, () => {
    const actualId = [`root`, `profile-button`, `recarregar-permissoes-button`];
    cy.clickIfExist(`[data-cy="profile-button"]`);
    cy.clickIfExist(`[data-cy="recarregar-permissoes-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element profile-button->configuracoes-button`, () => {
    const actualId = [`root`, `profile-button`, `configuracoes-button`];
    cy.clickIfExist(`[data-cy="profile-button"]`);
    cy.clickIfExist(`[data-cy="configuracoes-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element profile-button->sobre-button`, () => {
    const actualId = [`root`, `profile-button`, `sobre-button`];
    cy.clickIfExist(`[data-cy="profile-button"]`);
    cy.clickIfExist(`[data-cy="sobre-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element catalogo de produtos->4245688225-pi pi-search`, () => {
    const actualId = [`root`, `catalogo de produtos`, `4245688225-pi pi-search`];
    cy.visit('http://system-A20catalogo/consulta-materiais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4245688225-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element catalogo de produtos->4245688225-filtros`, () => {
    const actualId = [`root`, `catalogo de produtos`, `4245688225-filtros`];
    cy.visit('http://system-A20catalogo/consulta-materiais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4245688225-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element catalogo de produtos->4245688225-pi pi-times`, () => {
    const actualId = [`root`, `catalogo de produtos`, `4245688225-pi pi-times`];
    cy.visit('http://system-A20catalogo/consulta-materiais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4245688225-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`];
    cy.clickIfExist(`[data-cy="termo de referencia"]`);
    cy.clickIfExist(`[data-cy="secoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element termo de referencia->gerenciamento de termos`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`];
    cy.clickIfExist(`[data-cy="termo de referencia"]`);
    cy.clickIfExist(`[data-cy="gerenciamento de termos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="adesao/carona"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="dispensa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="inexigibilidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="contrato/aditivo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->comissao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="comissao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->requisicoes de modificacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="requisicoes de modificacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`];
    cy.clickIfExist(`[data-cy="cadastros e consultas"]`);
    cy.clickIfExist(`[data-cy="consultar processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->alertas dafo`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="alertas dafo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->analisar processos`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="analisar processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->secoes checklist`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="secoes checklist"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="checklist de verificacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->analise automatica`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="analise automatica"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->historico processos`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="historico processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->analise de editais`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="analise de editais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->monitoramento atos diario oficial`, () => {
    const actualId = [`root`, `auditoria`, `monitoramento atos diario oficial`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="monitoramento atos diario oficial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element auditoria->mapeamento de atos com licitacoes`, () => {
    const actualId = [`root`, `auditoria`, `mapeamento de atos com licitacoes`];
    cy.clickIfExist(`[data-cy="auditoria"]`);
    cy.clickIfExist(`[data-cy="mapeamento de atos com licitacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element alertas->1537469311-pi pi-search`, () => {
    const actualId = [`root`, `alertas`, `1537469311-pi pi-search`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-filtros`, () => {
    const actualId = [`root`, `alertas`, `1537469311-filtros`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-detalhes do processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-visualizar alerta`, () => {
    const actualId = [`root`, `alertas`, `1537469311-visualizar alerta`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-visualizar alerta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-arquivar alerta`, () => {
    const actualId = [`root`, `alertas`, `1537469311-arquivar alerta`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-arquivar alerta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-pi pi-times`, () => {
    const actualId = [`root`, `alertas`, `1537469311-pi pi-times`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->elementos de despesa`, () => {
    const actualId = [`root`, `administracao`, `elementos de despesa`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="elementos de despesa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->falhas`, () => {
    const actualId = [`root`, `administracao`, `falhas`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="falhas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->feriados`, () => {
    const actualId = [`root`, `administracao`, `feriados`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="feriados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->fontes de recurso`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="fontes de recurso"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->formas de licitacao`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="formas de licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->formas de publicacao`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="formas de publicacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->fundamentacao legal`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="fundamentacao legal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->licitantes`, () => {
    const actualId = [`root`, `administracao`, `licitantes`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="licitantes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="modalidades da licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->criterios de julgamento`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="criterios de julgamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->gerador de links`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="gerador de links"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->variaveis de controle`, () => {
    const actualId = [`root`, `administracao`, `variaveis de controle`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="variaveis de controle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->parametrizacao dos arquivos`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="parametrizacao dos arquivos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->entidade externa`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="entidade externa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element administracao->responsavel ente`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`];
    cy.clickIfExist(`[data-cy="administracao"]`);
    cy.clickIfExist(`[data-cy="responsavel ente"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element seguranca->grupos`, () => {
    const actualId = [`root`, `seguranca`, `grupos`];
    cy.clickIfExist(`[data-cy="seguranca"]`);
    cy.clickIfExist(`[data-cy="grupos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element seguranca->usuarios`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`];
    cy.clickIfExist(`[data-cy="seguranca"]`);
    cy.clickIfExist(`[data-cy="usuarios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element seguranca->auditores atribuidores`, () => {
    const actualId = [`root`, `seguranca`, `auditores atribuidores`];
    cy.clickIfExist(`[data-cy="seguranca"]`);
    cy.clickIfExist(`[data-cy="auditores atribuidores"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element seguranca->funcoes de risco`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`];
    cy.clickIfExist(`[data-cy="seguranca"]`);
    cy.clickIfExist(`[data-cy="funcoes de risco"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element seguranca->configuracoes`, () => {
    const actualId = [`root`, `seguranca`, `configuracoes`];
    cy.clickIfExist(`[data-cy="seguranca"]`);
    cy.clickIfExist(`[data-cy="configuracoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obras->medicao`, () => {
    const actualId = [`root`, `obras`, `medicao`];
    cy.clickIfExist(`[data-cy="obras"]`);
    cy.clickIfExist(`[data-cy="medicao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-pi pi-search`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-search`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-pi pi-filter-slash`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-filter-slash`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-pi pi-filter-slash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-filtros`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-filtros`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-novo`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-novo`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-pi pi-pencil`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-pencil`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-pi pi-trash`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-trash`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->secoes->2718408923-pi pi-times`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-times`];
    cy.visit('http://system-A20termo-referencia/secao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2718408923-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->gerenciamento de termos->1834015054-pi pi-search`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-pi pi-search`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->gerenciamento de termos->1834015054-filtros`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-filtros`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->gerenciamento de termos->1834015054-novo`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-novo`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->gerenciamento de termos->1834015054-exportar dados`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-exportar dados`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element termo de referencia->gerenciamento de termos->1834015054-pi pi-times`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-pi pi-times`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao->1555221225-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao->1555221225-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-filtros`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao->1555221225-nova licitacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-nova licitacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-nova licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao->1555221225-pi pi-angle-right`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-pi pi-angle-right`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-pi pi-angle-right"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao->1555221225-pi pi-sort-alt`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-pi pi-sort-alt`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-pi pi-sort-alt"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->licitacao->1555221225-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-filtros`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-novo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-novo`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-exportar dados`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-exportar dados`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-entidade de origem`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-entidade de origem`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-entidade de origem"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-numero do processo administrativo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-numero do processo administrativo`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-numero do processo administrativo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-valor`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-valor`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-valor"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-objeto`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-objeto`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-objeto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-pi pi-ellipsis-v`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-ver mais`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-ver mais`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-ver mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->adesao/carona->1678432678-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `adesao/carona`, `1678432678-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/carona');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1678432678-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-filtros`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-novo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-novo`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-exportar dados`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-exportar dados`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-numero do processo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-numero do processo`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-numero do processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-responsavel`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-responsavel`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-responsavel"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-objeto`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-objeto`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-objeto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-data de ratificacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-data de ratificacao`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-data de ratificacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-valor`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-valor`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-valor"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-pi pi-ellipsis-v`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->dispensa->2643273457-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `dispensa`, `2643273457-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/dispensa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643273457-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-filtros`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-novo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-novo`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-exportar dados`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-exportar dados`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-numero do processo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-numero do processo`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-numero do processo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-responsavel pela ratificacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-responsavel pela ratificacao`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-responsavel pela ratificacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-objeto`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-objeto`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-objeto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-data da assinatura`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-data da assinatura`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-data da assinatura"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-valor`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-valor`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-valor"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-pi pi-ellipsis-v`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->inexigibilidade->3253598109-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `inexigibilidade`, `3253598109-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/inexigibilidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3253598109-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-filtros`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-novo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-novo`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-exportar dados`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-exportar dados`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-numero do contrato`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-numero do contrato`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-numero do contrato"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-licitante`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-licitante`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-licitante"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-objeto`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-objeto`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-objeto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-modalidade`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-modalidade`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-modalidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-valor`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-valor`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-valor"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-forma contrato`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-forma contrato`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-forma contrato"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-entidade externa`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-entidade externa`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-entidade externa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-pi pi-ellipsis-v`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->contrato/aditivo->1791441076-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `contrato/aditivo`, `1791441076-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/contrato');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1791441076-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->comissao->1752699694-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`, `1752699694-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/comissao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1752699694-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->comissao->1752699694-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`, `1752699694-filtros`];
    cy.visit('http://system-A20cadastros-consulta/comissao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1752699694-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->comissao->1752699694-novo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`, `1752699694-novo`];
    cy.visit('http://system-A20cadastros-consulta/comissao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1752699694-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->comissao->1752699694-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`, `1752699694-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/comissao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1752699694-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/requisicao-modificacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906747741-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-filtros`];
    cy.visit('http://system-A20cadastros-consulta/requisicao-modificacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906747741-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`];
    cy.visit('http://system-A20cadastros-consulta/requisicao-modificacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906747741-pi pi-user-edit"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/requisicao-modificacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1906747741-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-filtros`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-filtros`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-nova licitacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-nova licitacao`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-nova licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-exportar dados`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-exportar dados`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-numero`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-numero`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-numero"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-entidade/orgao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-entidade/orgao`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-entidade/orgao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-objeto`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-objeto`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-objeto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-data de abertura`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-data de abertura`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-data de abertura"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-modalidade`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-modalidade`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-modalidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-valor estimado`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-valor estimado`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-valor estimado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-pi pi-ellipsis-v`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element cadastros e consultas->consultar processos->1353241976-pi pi-times`, () => {
    const actualId = [`root`, `cadastros e consultas`, `consultar processos`, `1353241976-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1353241976-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->alertas dafo->775637628-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-pi pi-search`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->alertas dafo->775637628-filtros`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-filtros`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->alertas dafo->775637628-pi pi-eye`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-pi pi-eye`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-pi pi-eye"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->alertas dafo->775637628-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-pi pi-times`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analisar processos->3240378834-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-pi pi-search`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analisar processos->3240378834-filtros`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-filtros`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analisar processos->3240378834-pi pi-angle-down`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-pi pi-angle-down`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-pi pi-angle-down"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analisar processos->3240378834-pi pi-angle-right`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-pi pi-angle-right`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-pi pi-angle-right"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analisar processos->3240378834-pi pi-sort-alt`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-pi pi-sort-alt`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-pi pi-sort-alt"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analisar processos->3240378834-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-pi pi-times`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->secoes checklist->3479445712-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-pi pi-search`];
    cy.visit('http://system-A20auditoria/secao-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479445712-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->secoes checklist->3479445712-filtros`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-filtros`];
    cy.visit('http://system-A20auditoria/secao-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479445712-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->secoes checklist->3479445712-novo`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-novo`];
    cy.visit('http://system-A20auditoria/secao-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479445712-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->secoes checklist->3479445712-pi pi-pencil`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-pi pi-pencil`];
    cy.visit('http://system-A20auditoria/secao-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479445712-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->secoes checklist->3479445712-pi pi-trash`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-pi pi-trash`];
    cy.visit('http://system-A20auditoria/secao-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479445712-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->secoes checklist->3479445712-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-pi pi-times`];
    cy.visit('http://system-A20auditoria/secao-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3479445712-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao->3355604276-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`, `3355604276-pi pi-search`];
    cy.visit('http://system-A20auditoria/item-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3355604276-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao->3355604276-filtros`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`, `3355604276-filtros`];
    cy.visit('http://system-A20auditoria/item-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3355604276-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao->3355604276-novo`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`, `3355604276-novo`];
    cy.visit('http://system-A20auditoria/item-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3355604276-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao->3355604276-pi pi-pencil`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`, `3355604276-pi pi-pencil`];
    cy.visit('http://system-A20auditoria/item-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3355604276-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao->3355604276-pi pi-trash`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`, `3355604276-pi pi-trash`];
    cy.visit('http://system-A20auditoria/item-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3355604276-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->checklist de verificacao->3355604276-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `checklist de verificacao`, `3355604276-pi pi-times`];
    cy.visit('http://system-A20auditoria/item-checklist');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3355604276-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise automatica->74772744-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-pi pi-search`];
    cy.visit('http://system-A20auditoria/analise-automatica');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74772744-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise automatica->74772744-filtros`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-filtros`];
    cy.visit('http://system-A20auditoria/analise-automatica');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74772744-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise automatica->74772744-novo`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-novo`];
    cy.visit('http://system-A20auditoria/analise-automatica');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74772744-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise automatica->74772744-pi pi-pencil`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-pi pi-pencil`];
    cy.visit('http://system-A20auditoria/analise-automatica');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74772744-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise automatica->74772744-pi pi-trash`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-pi pi-trash`];
    cy.visit('http://system-A20auditoria/analise-automatica');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74772744-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise automatica->74772744-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-pi pi-times`];
    cy.visit('http://system-A20auditoria/analise-automatica');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74772744-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->historico processos->1628962350-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`, `1628962350-pi pi-search`];
    cy.visit('http://system-A20auditoria/historico-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1628962350-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->historico processos->1628962350-pi pi-filter-slash`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`, `1628962350-pi pi-filter-slash`];
    cy.visit('http://system-A20auditoria/historico-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1628962350-pi pi-filter-slash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->historico processos->1628962350-filtros`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`, `1628962350-filtros`];
    cy.visit('http://system-A20auditoria/historico-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1628962350-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->historico processos->1628962350-pi pi-eye`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`, `1628962350-pi pi-eye`];
    cy.visit('http://system-A20auditoria/historico-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1628962350-pi pi-eye"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->historico processos->1628962350-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`, `1628962350-pi pi-times`];
    cy.visit('http://system-A20auditoria/historico-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1628962350-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise de editais->4100354799-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`, `4100354799-pi pi-search`];
    cy.visit('http://system-A20auditoria/analisar-editais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4100354799-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise de editais->4100354799-filtros`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`, `4100354799-filtros`];
    cy.visit('http://system-A20auditoria/analisar-editais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4100354799-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise de editais->4100354799-novo edital`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`, `4100354799-novo edital`];
    cy.visit('http://system-A20auditoria/analisar-editais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4100354799-novo edital"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->analise de editais->4100354799-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`, `4100354799-pi pi-times`];
    cy.visit('http://system-A20auditoria/analisar-editais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4100354799-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->monitoramento atos diario oficial->3575258333-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `monitoramento atos diario oficial`, `3575258333-pi pi-search`];
    cy.visit('http://system-A20auditoria/monitoramento-atos-diario-oficial');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3575258333-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->monitoramento atos diario oficial->3575258333-filtros`, () => {
    const actualId = [`root`, `auditoria`, `monitoramento atos diario oficial`, `3575258333-filtros`];
    cy.visit('http://system-A20auditoria/monitoramento-atos-diario-oficial');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3575258333-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->monitoramento atos diario oficial->3575258333-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `monitoramento atos diario oficial`, `3575258333-pi pi-times`];
    cy.visit('http://system-A20auditoria/monitoramento-atos-diario-oficial');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3575258333-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->mapeamento de atos com licitacoes->2601858932-pi pi-search`, () => {
    const actualId = [`root`, `auditoria`, `mapeamento de atos com licitacoes`, `2601858932-pi pi-search`];
    cy.visit('http://system-A20auditoria/mapeamento-atos-licitacoes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2601858932-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->mapeamento de atos com licitacoes->2601858932-filtros`, () => {
    const actualId = [`root`, `auditoria`, `mapeamento de atos com licitacoes`, `2601858932-filtros`];
    cy.visit('http://system-A20auditoria/mapeamento-atos-licitacoes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2601858932-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->mapeamento de atos com licitacoes->2601858932-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `auditoria`, `mapeamento de atos com licitacoes`, `2601858932-pi pi-ellipsis-v`];
    cy.visit('http://system-A20auditoria/mapeamento-atos-licitacoes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2601858932-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element auditoria->mapeamento de atos com licitacoes->2601858932-pi pi-times`, () => {
    const actualId = [`root`, `auditoria`, `mapeamento de atos com licitacoes`, `2601858932-pi pi-times`];
    cy.visit('http://system-A20auditoria/mapeamento-atos-licitacoes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2601858932-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->licitacao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `licitacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->adesao/carona`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `adesao/carona`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="adesao/carona"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->dispensa`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `dispensa`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="dispensa"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->inexigibilidade`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `inexigibilidade`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="inexigibilidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->contrato/aditivo`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `contrato/aditivo`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contrato/aditivo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->comissao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `comissao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="comissao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->requisicoes de modificacao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `requisicoes de modificacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="requisicoes de modificacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->consultar processos`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `consultar processos`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="consultar processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-pi pi-search`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-filtros`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-filtros`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-nova licitacao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-nova licitacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-nova licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-pi pi-angle-right`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-pi pi-angle-right`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-pi pi-angle-right"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-pi pi-sort-alt`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-pi pi-sort-alt`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-pi pi-sort-alt"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-preparatoria`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-preparatoria`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-preparatoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-divulgacao e publicacao da licitacao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-divulgacao e publicacao da licitacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-divulgacao e publicacao da licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-proxima fase`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-proxima fase`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-proxima fase"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-suspender licitacao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-suspender licitacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-suspender licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-finalizar licitacao`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-finalizar licitacao`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-finalizar licitacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-excluir`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-excluir`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-pi pi-list`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-pi pi-list`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-pi pi-list"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-visualizar`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-visualizar`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-download`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-download`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-detalhes do processo->1700861693-pi pi-times`, () => {
    const actualId = [`root`, `alertas`, `1537469311-detalhes do processo`, `1700861693-pi pi-times`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/detail/27171');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700861693-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-visualizar alerta->3585023761-visualizar arquivos`, () => {
    const actualId = [`root`, `alertas`, `1537469311-visualizar alerta`, `3585023761-visualizar arquivos`];
    cy.visit('http://system-A20alertas/editar/1398');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585023761-visualizar arquivos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-visualizar alerta->3585023761-voltar`, () => {
    const actualId = [`root`, `alertas`, `1537469311-visualizar alerta`, `3585023761-voltar`];
    cy.visit('http://system-A20alertas/editar/1398');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585023761-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-visualizar alerta->3585023761-arquivar`, () => {
    const actualId = [`root`, `alertas`, `1537469311-visualizar alerta`, `3585023761-arquivar`];
    cy.visit('http://system-A20alertas/editar/1398');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585023761-arquivar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-visualizar alerta->3585023761-responder`, () => {
    const actualId = [`root`, `alertas`, `1537469311-visualizar alerta`, `3585023761-responder`];
    cy.visit('http://system-A20alertas/editar/1398');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585023761-responder"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-arquivar alerta->1537469311-cancelar`, () => {
    const actualId = [`root`, `alertas`, `1537469311-arquivar alerta`, `1537469311-cancelar`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-arquivar alerta"]`);
    cy.clickIfExist(`[data-cy="1537469311-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element alertas->1537469311-arquivar alerta->1537469311-arquivar`, () => {
    const actualId = [`root`, `alertas`, `1537469311-arquivar alerta`, `1537469311-arquivar`];
    cy.visit('http://system-A20alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1537469311-arquivar alerta"]`);
    cy.clickIfExist(`[data-cy="1537469311-arquivar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-pi pi-search`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-filtros`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-filtros`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-novo`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-novo`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-exportar dados`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-pi pi-pencil`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-pi pi-trash`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->falhas->1380608115-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-pi pi-times`];
    cy.visit('http://system-A20administracao/falha');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1380608115-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-pi pi-search`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-filtros`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-filtros`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-novo`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-novo`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-exportar dados`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-pi pi-pencil`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-pi pi-trash`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->feriados->4034562079-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-pi pi-times`];
    cy.visit('http://system-A20administracao/feriado');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034562079-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-pi pi-search`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-filtros`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-filtros`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-novo`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-novo`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-exportar dados`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-pi pi-pencil`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-pi pi-trash`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fontes de recurso->2906815771-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-pi pi-times`];
    cy.visit('http://system-A20administracao/fonte-recurso');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2906815771-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-pi pi-search`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-filtros`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-filtros`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-novo`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-novo`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-exportar dados`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-pi pi-pencil`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-pi pi-trash`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de licitacao->2830712122-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-pi pi-times`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-pi pi-search`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-filtros`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-filtros`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-novo`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-novo`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-exportar dados`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-pi pi-pencil`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-pi pi-trash`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->formas de publicacao->1458627872-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-pi pi-times`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fundamentacao legal->275348607-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`, `275348607-pi pi-search`];
    cy.visit('http://system-A20administracao/fundamentacao-legal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="275348607-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fundamentacao legal->275348607-filtros`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`, `275348607-filtros`];
    cy.visit('http://system-A20administracao/fundamentacao-legal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="275348607-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fundamentacao legal->275348607-novo`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`, `275348607-novo`];
    cy.visit('http://system-A20administracao/fundamentacao-legal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="275348607-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fundamentacao legal->275348607-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`, `275348607-pi pi-pencil`];
    cy.visit('http://system-A20administracao/fundamentacao-legal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="275348607-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fundamentacao legal->275348607-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`, `275348607-pi pi-trash`];
    cy.visit('http://system-A20administracao/fundamentacao-legal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="275348607-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->fundamentacao legal->275348607-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `fundamentacao legal`, `275348607-pi pi-times`];
    cy.visit('http://system-A20administracao/fundamentacao-legal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="275348607-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-search`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-filtros`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-filtros`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-novo`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-novo`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-exportar dados`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-pi pi-list`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-list`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-pi pi-list"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-pencil`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-trash`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->licitantes->3164945720-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-times`];
    cy.visit('http://system-A20administracao/licitante');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3164945720-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-pi pi-search`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-filtros`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-filtros`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-novo`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-novo`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-exportar dados`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-pi pi-pencil`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-pi pi-trash`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->modalidades da licitacao->2428299973-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-pi pi-times`];
    cy.visit('http://system-A20administracao/modalidade-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428299973-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-pi pi-search`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-filtros`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-filtros`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-novo`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-novo`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-exportar dados`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-pi pi-pencil`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-pi pi-trash`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->criterios de julgamento->3946733471-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-pi pi-times`];
    cy.visit('http://system-A20administracao/tipo-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3946733471-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->gerador de links->1437041263-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`, `1437041263-pi pi-search`];
    cy.visit('http://system-A20administracao/gerador-links');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1437041263-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->gerador de links->1437041263-filtros`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`, `1437041263-filtros`];
    cy.visit('http://system-A20administracao/gerador-links');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1437041263-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->gerador de links->1437041263-novo`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`, `1437041263-novo`];
    cy.visit('http://system-A20administracao/gerador-links');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1437041263-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->gerador de links->1437041263-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`, `1437041263-pi pi-times`];
    cy.visit('http://system-A20administracao/gerador-links');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1437041263-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->variaveis de controle->3787439616-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `variaveis de controle`, `3787439616-pi pi-search`];
    cy.visit('http://system-A20administracao/variavel-controle');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3787439616-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->variaveis de controle->3787439616-filtros`, () => {
    const actualId = [`root`, `administracao`, `variaveis de controle`, `3787439616-filtros`];
    cy.visit('http://system-A20administracao/variavel-controle');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3787439616-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->variaveis de controle->3787439616-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `variaveis de controle`, `3787439616-pi pi-times`];
    cy.visit('http://system-A20administracao/variavel-controle');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3787439616-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->parametrizacao dos arquivos->3706103062-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-pi pi-search`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3706103062-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->parametrizacao dos arquivos->3706103062-filtros`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-filtros`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3706103062-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->parametrizacao dos arquivos->3706103062-pi pi-list`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-pi pi-list`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3706103062-pi pi-list"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->parametrizacao dos arquivos->3706103062-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-pi pi-pencil`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3706103062-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->parametrizacao dos arquivos->3706103062-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-pi pi-times`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3706103062-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-pi pi-search`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-filtros`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-filtros`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-novo`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-novo`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-exportar dados`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-exportar dados`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-exportar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-pi pi-pencil`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-pi pi-trash`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->entidade externa->3919870791-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-pi pi-times`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->responsavel ente->1130073086-pi pi-search`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-pi pi-search`];
    cy.visit('http://system-A20administracao/responsavel-ente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130073086-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->responsavel ente->1130073086-filtros`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-filtros`];
    cy.visit('http://system-A20administracao/responsavel-ente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130073086-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->responsavel ente->1130073086-novo`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-novo`];
    cy.visit('http://system-A20administracao/responsavel-ente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130073086-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->responsavel ente->1130073086-pi pi-pencil`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-pi pi-pencil`];
    cy.visit('http://system-A20administracao/responsavel-ente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130073086-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->responsavel ente->1130073086-pi pi-trash`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-pi pi-trash`];
    cy.visit('http://system-A20administracao/responsavel-ente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130073086-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element administracao->responsavel ente->1130073086-pi pi-times`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-pi pi-times`];
    cy.visit('http://system-A20administracao/responsavel-ente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1130073086-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-pi pi-search`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-pi pi-search`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-filtros`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-filtros`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-novo`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-novo`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-permissoes`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-permissoes`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-permissoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-usuarios`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-usuarios`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-usuarios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-pi pi-pencil`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-pi pi-pencil`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-pi pi-trash`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-pi pi-trash`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->grupos->1745718313-pi pi-times`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-pi pi-times`];
    cy.visit('http://system-A20seguranca/grupo-usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745718313-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->usuarios->4277602509-pi pi-search`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-pi pi-search`];
    cy.visit('http://system-A20seguranca/usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4277602509-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->usuarios->4277602509-filtros`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-filtros`];
    cy.visit('http://system-A20seguranca/usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4277602509-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->usuarios->4277602509-grupos`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-grupos`];
    cy.visit('http://system-A20seguranca/usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4277602509-grupos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->usuarios->4277602509-pi pi-pencil`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-pi pi-pencil`];
    cy.visit('http://system-A20seguranca/usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4277602509-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->usuarios->4277602509-pi pi-times`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-pi pi-times`];
    cy.visit('http://system-A20seguranca/usuario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4277602509-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->auditores atribuidores->2252475832-pi pi-search`, () => {
    const actualId = [`root`, `seguranca`, `auditores atribuidores`, `2252475832-pi pi-search`];
    cy.visit('http://system-A20seguranca/auditorAtribuidor');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2252475832-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->auditores atribuidores->2252475832-filtros`, () => {
    const actualId = [`root`, `seguranca`, `auditores atribuidores`, `2252475832-filtros`];
    cy.visit('http://system-A20seguranca/auditorAtribuidor');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2252475832-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->auditores atribuidores->2252475832-novo`, () => {
    const actualId = [`root`, `seguranca`, `auditores atribuidores`, `2252475832-novo`];
    cy.visit('http://system-A20seguranca/auditorAtribuidor');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2252475832-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->auditores atribuidores->2252475832-pi pi-times`, () => {
    const actualId = [`root`, `seguranca`, `auditores atribuidores`, `2252475832-pi pi-times`];
    cy.visit('http://system-A20seguranca/auditorAtribuidor');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2252475832-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-pi pi-search`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-search`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-filtros`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-filtros`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-novo`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-novo`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-pi pi-eye`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-eye`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-pi pi-eye"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-pi pi-pencil`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-pencil`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-pi pi-pencil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-pi pi-trash`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-trash`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->funcoes de risco->3861932206-pi pi-times`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-times`];
    cy.visit('http://system-A20seguranca/funcao-risco');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3861932206-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->configuracoes->2791227887-novo`, () => {
    const actualId = [`root`, `seguranca`, `configuracoes`, `2791227887-novo`];
    cy.visit('http://system-A20seguranca/configuracoes/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2791227887-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->configuracoes->2791227887-voltar`, () => {
    const actualId = [`root`, `seguranca`, `configuracoes`, `2791227887-voltar`];
    cy.visit('http://system-A20seguranca/configuracoes/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2791227887-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element seguranca->configuracoes->2791227887-salvar`, () => {
    const actualId = [`root`, `seguranca`, `configuracoes`, `2791227887-salvar`];
    cy.visit('http://system-A20seguranca/configuracoes/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2791227887-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obras->medicao->391958567-pi pi-chevron-right`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-pi pi-chevron-right`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-pi pi-chevron-right"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obras->medicao->391958567-pi pi-times`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-pi pi-times`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-pi pi-times"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obras->medicao->391958567-pi pi-search`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-pi pi-search`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obras->medicao->391958567-filtros`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-filtros`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-filtros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obras->medicao->391958567-pi pi-ellipsis-v`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-pi pi-ellipsis-v`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-pi pi-ellipsis-v"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obras->medicao->391958567-ver mais`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-ver mais`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-ver mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->secoes->2718408923-novo->171684078-voltar`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-novo`, `171684078-voltar`];
    cy.visit('http://system-A20termo-referencia/secao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="171684078-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->secoes->2718408923-novo->171684078-salvar`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-novo`, `171684078-salvar`];
    cy.visit('http://system-A20termo-referencia/secao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="171684078-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->secoes->2718408923-pi pi-pencil->556775477-voltar`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-pencil`, `556775477-voltar`];
    cy.visit('http://system-A20termo-referencia/secao/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="556775477-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->secoes->2718408923-pi pi-pencil->556775477-salvar`, () => {
    const actualId = [`root`, `termo de referencia`, `secoes`, `2718408923-pi pi-pencil`, `556775477-salvar`];
    cy.visit('http://system-A20termo-referencia/secao/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="556775477-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->gerenciamento de termos->1834015054-novo->942852187-exportar`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-novo`, `942852187-exportar`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="942852187-exportar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->gerenciamento de termos->1834015054-novo->942852187-voltar`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-novo`, `942852187-voltar`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="942852187-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->gerenciamento de termos->1834015054-exportar dados->1834015054-csv`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-exportar dados`, `1834015054-csv`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-exportar dados"]`);
    cy.clickIfExist(`[data-cy="1834015054-csv"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->gerenciamento de termos->1834015054-exportar dados->1834015054-xls`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-exportar dados`, `1834015054-xls`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-exportar dados"]`);
    cy.clickIfExist(`[data-cy="1834015054-xls"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element termo de referencia->gerenciamento de termos->1834015054-exportar dados->1834015054-pdf`, () => {
    const actualId = [`root`, `termo de referencia`, `gerenciamento de termos`, `1834015054-exportar dados`, `1834015054-pdf`];
    cy.visit('http://system-A20termo-referencia/gerenciamento-termos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1834015054-exportar dados"]`);
    cy.clickIfExist(`[data-cy="1834015054-pdf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->licitacao->1555221225-nova licitacao->98475936-voltar`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-nova licitacao`, `98475936-voltar`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="98475936-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->licitacao->1555221225-nova licitacao->98475936-pi pi-search`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-nova licitacao`, `98475936-pi pi-search`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="98475936-pi pi-search"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->licitacao->1555221225-nova licitacao->98475936-salvar`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-nova licitacao`, `98475936-salvar`];
    cy.visit('http://system-A20cadastros-consulta/licitacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="98475936-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->licitacao->1555221225-pi pi-angle-right->1555221225-pi pi-angle-down`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-pi pi-angle-right`, `1555221225-pi pi-angle-down`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-pi pi-angle-right"]`);
    cy.clickIfExist(`[data-cy="1555221225-pi pi-angle-down"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->licitacao->1555221225-pi pi-sort-alt->1555221225-aplicar`, () => {
    const actualId = [`root`, `cadastros e consultas`, `licitacao`, `1555221225-pi pi-sort-alt`, `1555221225-aplicar`];
    cy.visit('http://system-A20cadastros-consulta/licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1555221225-pi pi-sort-alt"]`);
    cy.clickIfExist(`[data-cy="1555221225-aplicar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->comissao->1752699694-novo->3756309115-voltar`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`, `1752699694-novo`, `3756309115-voltar`];
    cy.visit('http://system-A20cadastros-consulta/comissao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3756309115-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->comissao->1752699694-novo->3756309115-salvar`, () => {
    const actualId = [`root`, `cadastros e consultas`, `comissao`, `1752699694-novo`, `3756309115-salvar`];
    cy.visit('http://system-A20cadastros-consulta/comissao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3756309115-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->alertas dafo`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `alertas dafo`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="alertas dafo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->analisar processos`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `analisar processos`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="analisar processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->secoes checklist`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `secoes checklist`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="secoes checklist"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->checklist de verificacao`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `checklist de verificacao`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="checklist de verificacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->analise automatica`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `analise automatica`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="analise automatica"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->historico processos`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `historico processos`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="historico processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->analise de editais`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `analise de editais`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="analise de editais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->monitoramento atos diario oficial`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `monitoramento atos diario oficial`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="monitoramento atos diario oficial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element cadastros e consultas->requisicoes de modificacao->1906747741-pi pi-user-edit->mapeamento de atos com licitacoes`, () => {
    const actualId = [`root`, `cadastros e consultas`, `requisicoes de modificacao`, `1906747741-pi pi-user-edit`, `mapeamento de atos com licitacoes`];
    cy.visit('http://system-A20auditoria/requisicao-modificacao/julgamento/13332');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="mapeamento de atos com licitacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->alertas dafo->775637628-pi pi-eye->775637628-cancelar`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-pi pi-eye`, `775637628-cancelar`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-pi pi-eye"]`);
    cy.clickIfExist(`[data-cy="775637628-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->alertas dafo->775637628-pi pi-eye->775637628-rejeitar alerta`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-pi pi-eye`, `775637628-rejeitar alerta`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-pi pi-eye"]`);
    cy.clickIfExist(`[data-cy="775637628-rejeitar alerta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->alertas dafo->775637628-pi pi-eye->775637628-enviar para jurisdicionado`, () => {
    const actualId = [`root`, `auditoria`, `alertas dafo`, `775637628-pi pi-eye`, `775637628-enviar para jurisdicionado`];
    cy.visit('http://system-A20auditoria/analisar-alertas');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="775637628-pi pi-eye"]`);
    cy.clickIfExist(`[data-cy="775637628-enviar para jurisdicionado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->analisar processos->3240378834-pi pi-sort-alt->3240378834-aplicar`, () => {
    const actualId = [`root`, `auditoria`, `analisar processos`, `3240378834-pi pi-sort-alt`, `3240378834-aplicar`];
    cy.visit('http://system-A20auditoria/analise-processo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3240378834-pi pi-sort-alt"]`);
    cy.clickIfExist(`[data-cy="3240378834-aplicar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->secoes checklist->3479445712-novo->803856921-voltar`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-novo`, `803856921-voltar`];
    cy.visit('http://system-A20auditoria/secao-checklist/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="803856921-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->secoes checklist->3479445712-novo->803856921-salvar`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-novo`, `803856921-salvar`];
    cy.visit('http://system-A20auditoria/secao-checklist/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="803856921-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->secoes checklist->3479445712-pi pi-pencil->3227589920-voltar`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-pi pi-pencil`, `3227589920-voltar`];
    cy.visit('http://system-A20auditoria/secao-checklist/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3227589920-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->secoes checklist->3479445712-pi pi-pencil->3227589920-salvar`, () => {
    const actualId = [`root`, `auditoria`, `secoes checklist`, `3479445712-pi pi-pencil`, `3227589920-salvar`];
    cy.visit('http://system-A20auditoria/secao-checklist/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3227589920-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->analise automatica->74772744-pi pi-pencil->3330963944-voltar`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-pi pi-pencil`, `3330963944-voltar`];
    cy.visit('http://system-A20auditoria/analise-automatica/editar/3');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3330963944-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->analise automatica->74772744-pi pi-pencil->3330963944-salvar`, () => {
    const actualId = [`root`, `auditoria`, `analise automatica`, `74772744-pi pi-pencil`, `3330963944-salvar`];
    cy.visit('http://system-A20auditoria/analise-automatica/editar/3');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3330963944-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->historico processos->1628962350-pi pi-eye->1628962350-fechar`, () => {
    const actualId = [`root`, `auditoria`, `historico processos`, `1628962350-pi pi-eye`, `1628962350-fechar`];
    cy.visit('http://system-A20auditoria/historico-processos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1628962350-pi pi-eye"]`);
    cy.clickIfExist(`[data-cy="1628962350-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->analise de editais->4100354799-novo edital->4100354799-voltar`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`, `4100354799-novo edital`, `4100354799-voltar`];
    cy.visit('http://system-A20auditoria/analisar-editais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4100354799-novo edital"]`);
    cy.clickIfExist(`[data-cy="4100354799-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element auditoria->analise de editais->4100354799-novo edital->4100354799-analisar`, () => {
    const actualId = [`root`, `auditoria`, `analise de editais`, `4100354799-novo edital`, `4100354799-analisar`];
    cy.visit('http://system-A20auditoria/analisar-editais');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4100354799-novo edital"]`);
    cy.clickIfExist(`[data-cy="4100354799-analisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element alertas->1537469311-visualizar alerta->3585023761-arquivar->3585023761-cancelar`, () => {
    const actualId = [`root`, `alertas`, `1537469311-visualizar alerta`, `3585023761-arquivar`, `3585023761-cancelar`];
    cy.visit('http://system-A20alertas/editar/1398');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585023761-arquivar"]`);
    cy.clickIfExist(`[data-cy="3585023761-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->falhas->1380608115-pi pi-pencil->3165962653-voltar`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-pi pi-pencil`, `3165962653-voltar`];
    cy.visit('http://system-A20administracao/falha/editar/12');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3165962653-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->falhas->1380608115-pi pi-pencil->3165962653-salvar`, () => {
    const actualId = [`root`, `administracao`, `falhas`, `1380608115-pi pi-pencil`, `3165962653-salvar`];
    cy.visit('http://system-A20administracao/falha/editar/12');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3165962653-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->feriados->4034562079-pi pi-pencil->249978481-voltar`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-pi pi-pencil`, `249978481-voltar`];
    cy.visit('http://system-A20administracao/feriado/editar/4');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="249978481-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->feriados->4034562079-pi pi-pencil->249978481-salvar`, () => {
    const actualId = [`root`, `administracao`, `feriados`, `4034562079-pi pi-pencil`, `249978481-salvar`];
    cy.visit('http://system-A20administracao/feriado/editar/4');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="249978481-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->fontes de recurso->2906815771-pi pi-pencil->1144828917-voltar`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-pi pi-pencil`, `1144828917-voltar`];
    cy.visit('http://system-A20administracao/fonte-recurso/editar/29');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1144828917-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->fontes de recurso->2906815771-pi pi-pencil->1144828917-salvar`, () => {
    const actualId = [`root`, `administracao`, `fontes de recurso`, `2906815771-pi pi-pencil`, `1144828917-salvar`];
    cy.visit('http://system-A20administracao/fonte-recurso/editar/29');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1144828917-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de licitacao->2830712122-novo->1615329263-voltar`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-novo`, `1615329263-voltar`];
    cy.visit('http://system-A20administracao/forma-licitacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1615329263-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de licitacao->2830712122-novo->1615329263-salvar`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-novo`, `1615329263-salvar`];
    cy.visit('http://system-A20administracao/forma-licitacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1615329263-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de licitacao->2830712122-exportar dados->2830712122-csv`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-exportar dados`, `2830712122-csv`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-exportar dados"]`);
    cy.clickIfExist(`[data-cy="2830712122-csv"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de licitacao->2830712122-exportar dados->2830712122-xls`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-exportar dados`, `2830712122-xls`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-exportar dados"]`);
    cy.clickIfExist(`[data-cy="2830712122-xls"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de licitacao->2830712122-exportar dados->2830712122-pdf`, () => {
    const actualId = [`root`, `administracao`, `formas de licitacao`, `2830712122-exportar dados`, `2830712122-pdf`];
    cy.visit('http://system-A20administracao/forma-licitacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2830712122-exportar dados"]`);
    cy.clickIfExist(`[data-cy="2830712122-pdf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-novo->2117816265-voltar`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-novo`, `2117816265-voltar`];
    cy.visit('http://system-A20administracao/forma-publicacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2117816265-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-novo->2117816265-salvar`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-novo`, `2117816265-salvar`];
    cy.visit('http://system-A20administracao/forma-publicacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2117816265-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-exportar dados->1458627872-csv`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-exportar dados`, `1458627872-csv`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-exportar dados"]`);
    cy.clickIfExist(`[data-cy="1458627872-csv"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-exportar dados->1458627872-xls`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-exportar dados`, `1458627872-xls`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-exportar dados"]`);
    cy.clickIfExist(`[data-cy="1458627872-xls"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-exportar dados->1458627872-pdf`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-exportar dados`, `1458627872-pdf`];
    cy.visit('http://system-A20administracao/forma-publicacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1458627872-exportar dados"]`);
    cy.clickIfExist(`[data-cy="1458627872-pdf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-pi pi-pencil->3010823888-voltar`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-pi pi-pencil`, `3010823888-voltar`];
    cy.visit('http://system-A20administracao/forma-publicacao/editar/8');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3010823888-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->formas de publicacao->1458627872-pi pi-pencil->3010823888-salvar`, () => {
    const actualId = [`root`, `administracao`, `formas de publicacao`, `1458627872-pi pi-pencil`, `3010823888-salvar`];
    cy.visit('http://system-A20administracao/forma-publicacao/editar/8');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3010823888-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->licitantes->3164945720-pi pi-pencil->1052880824-voltar`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-pencil`, `1052880824-voltar`];
    cy.visit('http://system-A20administracao/licitante/editar/36972');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1052880824-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->licitantes->3164945720-pi pi-pencil->1052880824-salvar`, () => {
    const actualId = [`root`, `administracao`, `licitantes`, `3164945720-pi pi-pencil`, `1052880824-salvar`];
    cy.visit('http://system-A20administracao/licitante/editar/36972');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1052880824-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->modalidades da licitacao->2428299973-pi pi-pencil->266598411-voltar`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-pi pi-pencil`, `266598411-voltar`];
    cy.visit('http://system-A20administracao/modalidade-licitacao/editar/9');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="266598411-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->modalidades da licitacao->2428299973-pi pi-pencil->266598411-salvar`, () => {
    const actualId = [`root`, `administracao`, `modalidades da licitacao`, `2428299973-pi pi-pencil`, `266598411-salvar`];
    cy.visit('http://system-A20administracao/modalidade-licitacao/editar/9');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="266598411-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->criterios de julgamento->3946733471-pi pi-pencil->3606632177-voltar`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-pi pi-pencil`, `3606632177-voltar`];
    cy.visit('http://system-A20administracao/tipo-licitacao/editar/5');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3606632177-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->criterios de julgamento->3946733471-pi pi-pencil->3606632177-salvar`, () => {
    const actualId = [`root`, `administracao`, `criterios de julgamento`, `3946733471-pi pi-pencil`, `3606632177-salvar`];
    cy.visit('http://system-A20administracao/tipo-licitacao/editar/5');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3606632177-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->gerador de links->1437041263-novo->2968366042-voltar`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`, `1437041263-novo`, `2968366042-voltar`];
    cy.visit('http://system-A20administracao/gerador-links/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2968366042-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->gerador de links->1437041263-novo->2968366042-salvar`, () => {
    const actualId = [`root`, `administracao`, `gerador de links`, `1437041263-novo`, `2968366042-salvar`];
    cy.visit('http://system-A20administracao/gerador-links/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2968366042-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->parametrizacao dos arquivos->3706103062-pi pi-pencil->1098338586-voltar`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-pi pi-pencil`, `1098338586-voltar`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1098338586-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->parametrizacao dos arquivos->3706103062-pi pi-pencil->1098338586-salvar`, () => {
    const actualId = [`root`, `administracao`, `parametrizacao dos arquivos`, `3706103062-pi pi-pencil`, `1098338586-salvar`];
    cy.visit('http://system-A20administracao/obrigatoriedade-arquivo/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1098338586-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-novo->3705829890-voltar`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-novo`, `3705829890-voltar`];
    cy.visit('http://system-A20administracao/entidade-externa/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3705829890-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-novo->3705829890-salvar`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-novo`, `3705829890-salvar`];
    cy.visit('http://system-A20administracao/entidade-externa/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3705829890-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-exportar dados->3919870791-csv`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-exportar dados`, `3919870791-csv`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-exportar dados"]`);
    cy.clickIfExist(`[data-cy="3919870791-csv"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-exportar dados->3919870791-xls`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-exportar dados`, `3919870791-xls`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-exportar dados"]`);
    cy.clickIfExist(`[data-cy="3919870791-xls"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-exportar dados->3919870791-pdf`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-exportar dados`, `3919870791-pdf`];
    cy.visit('http://system-A20administracao/entidade-externa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3919870791-exportar dados"]`);
    cy.clickIfExist(`[data-cy="3919870791-pdf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-pi pi-pencil->3319292489-voltar`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-pi pi-pencil`, `3319292489-voltar`];
    cy.visit('http://system-A20administracao/entidade-externa/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3319292489-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->entidade externa->3919870791-pi pi-pencil->3319292489-salvar`, () => {
    const actualId = [`root`, `administracao`, `entidade externa`, `3919870791-pi pi-pencil`, `3319292489-salvar`];
    cy.visit('http://system-A20administracao/entidade-externa/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3319292489-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->responsavel ente->1130073086-pi pi-pencil->2652145970-voltar`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-pi pi-pencil`, `2652145970-voltar`];
    cy.visit('http://system-A20administracao/responsavel-ente/editar/30');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2652145970-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element administracao->responsavel ente->1130073086-pi pi-pencil->2652145970-salvar`, () => {
    const actualId = [`root`, `administracao`, `responsavel ente`, `1130073086-pi pi-pencil`, `2652145970-salvar`];
    cy.visit('http://system-A20administracao/responsavel-ente/editar/30');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2652145970-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->grupos->1745718313-pi pi-pencil->1090080807-voltar`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-pi pi-pencil`, `1090080807-voltar`];
    cy.visit('http://system-A20seguranca/grupo-usuario/editar/3');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1090080807-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->grupos->1745718313-pi pi-pencil->1090080807-salvar`, () => {
    const actualId = [`root`, `seguranca`, `grupos`, `1745718313-pi pi-pencil`, `1090080807-salvar`];
    cy.visit('http://system-A20seguranca/grupo-usuario/editar/3');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1090080807-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->usuarios->4277602509-pi pi-pencil->1750615299-voltar`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-pi pi-pencil`, `1750615299-voltar`];
    cy.visit('http://system-A20seguranca/usuario/editar/21720');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1750615299-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->usuarios->4277602509-pi pi-pencil->1750615299-salvar`, () => {
    const actualId = [`root`, `seguranca`, `usuarios`, `4277602509-pi pi-pencil`, `1750615299-salvar`];
    cy.visit('http://system-A20seguranca/usuario/editar/21720');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1750615299-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->auditores atribuidores->2252475832-novo->2252475832-cancelar`, () => {
    const actualId = [`root`, `seguranca`, `auditores atribuidores`, `2252475832-novo`, `2252475832-cancelar`];
    cy.visit('http://system-A20seguranca/auditorAtribuidor');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2252475832-novo"]`);
    cy.clickIfExist(`[data-cy="2252475832-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->funcoes de risco->3861932206-novo->461755643-voltar`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-novo`, `461755643-voltar`];
    cy.visit('http://system-A20seguranca/funcao-risco/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="461755643-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->funcoes de risco->3861932206-novo->461755643-salvar`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-novo`, `461755643-salvar`];
    cy.visit('http://system-A20seguranca/funcao-risco/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="461755643-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->funcoes de risco->3861932206-pi pi-pencil->2734677634-novo`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-pencil`, `2734677634-novo`];
    cy.visit('http://system-A20seguranca/funcao-risco/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2734677634-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->funcoes de risco->3861932206-pi pi-pencil->2734677634-voltar`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-pencil`, `2734677634-voltar`];
    cy.visit('http://system-A20seguranca/funcao-risco/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2734677634-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->funcoes de risco->3861932206-pi pi-pencil->2734677634-salvar`, () => {
    const actualId = [`root`, `seguranca`, `funcoes de risco`, `3861932206-pi pi-pencil`, `2734677634-salvar`];
    cy.visit('http://system-A20seguranca/funcao-risco/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2734677634-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element seguranca->configuracoes->2791227887-novo->2791227887-pi pi-trash`, () => {
    const actualId = [`root`, `seguranca`, `configuracoes`, `2791227887-novo`, `2791227887-pi pi-trash`];
    cy.visit('http://system-A20seguranca/configuracoes/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2791227887-novo"]`);
    cy.clickIfExist(`[data-cy="2791227887-pi pi-trash"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element obras->medicao->391958567-ver mais->391958567-ver menos`, () => {
    const actualId = [`root`, `obras`, `medicao`, `391958567-ver mais`, `391958567-ver menos`];
    cy.visit('http://system-A20obras/medicao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="391958567-ver mais"]`);
    cy.clickIfExist(`[data-cy="391958567-ver menos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
