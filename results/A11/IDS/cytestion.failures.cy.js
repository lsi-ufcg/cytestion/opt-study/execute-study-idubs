describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element sap->sap/msg-pendente->2069071305-reloadoutlined`, () => {
    cy.clickCauseExist(`[data-cy="sap"]`);
    cy.clickCauseExist(`[data-cy="sap/msg-pendente"]`);
    cy.clickCauseExist(`[data-cy="2069071305-reloadoutlined"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element intefaces->intefaces/grupo->233875364-novo->1006927813-salvar`, () => {
    cy.clickCauseExist(`[data-cy="intefaces"]`);
    cy.clickCauseExist(`[data-cy="intefaces/grupo"]`);
    cy.clickCauseExist(`[data-cy="233875364-novo"]`);
    cy.clickCauseExist(`[data-cy="1006927813-salvar"]`);
    cy.checkErrorsWereDetected();
  });
});
