describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element intefaces->intefaces/grupo->233875364-novo->1006927813-salvar`, () => {
    cy.visit('/http://system-A11/grupo-interface/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1006927813-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-remover item`, () => {
    cy.visit('/http://system-A11/parametros-integracao-sap/editar/%23%24%23%%%%ABC');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1782392296-remover item"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-salvar`, () => {
    cy.visit('/http://system-A11/parametros-integracao-sap/editar/%23%24%23%%%%ABC');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1782392296-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-voltar`, () => {
    cy.visit('/http://system-A11/parametros-integracao-sap/editar/%23%24%23%%%%ABC');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1782392296-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/geracao-loader-ctl->1476829751-agendamentos->1476829751-voltar`, () => {
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~239838D%7C%7C239838&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1476829751-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/testes-interfaces->1613817690-agendamentos->1613817690-voltar`, () => {
    cy.visit('/http://system-A11/processos/testes-interfaces?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56821999D%7C%7C56821999&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1613817690-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-agendamentos->578205992-voltar`, () => {
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47379367D%7C%7C47379367&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="578205992-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-agendamentos->578205992-visualizar`, () => {
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47379367D%7C%7C47379367&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="578205992-visualizar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-agendamentos->459120590-voltar`, () => {
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~52196329D%7C%7C52196329&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="459120590-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-agendamentos->1466872463-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/documentacaoOpenInterfaces?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53794479D%7C%7C53794479&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1466872463-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/testesInterfaces->1676676410-agendamentos->1676676410-voltar`, () => {
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56822003D%7C%7C56822003&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1676676410-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-agendamentos->4055249190-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~4771512D%7C%7C4771512&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4055249190-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-agendamentos->3774328533-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456159D%7C%7C47456159&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3774328533-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-agendamentos->1808761608-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784718D%7C%7C53784718&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1808761608-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-agendamentos->2096399756-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143342D%7C%7C19143342&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2096399756-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-agendamentos->1359902950-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784761D%7C%7C53784761&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1359902950-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-agendamentos->2057874332-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784962D%7C%7C53784962&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2057874332-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-agendamentos->3213345668-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784762D%7C%7C53784762&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3213345668-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-agendamentos->200605263-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784764D%7C%7C53784764&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="200605263-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-agendamentos->1237903472-voltar`, () => {
    cy.visit(
      '/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784998D%7C%7C53784998&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1237903472-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-agendamentos->140334162-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15931467D%7C%7C15931467&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="140334162-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-agendamentos->140325421-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19109687D%7C%7C19109687&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="140325421-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-agendamentos->2224687352-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19114072D%7C%7C19114072&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2224687352-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-agendamentos->172569653-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~20059071D%7C%7C20059071&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="172569653-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-agendamentos->2126595364-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31795855D%7C%7C31795855&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2126595364-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-agendamentos->1374054689-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33923506D%7C%7C33923506&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1374054689-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-agendamentos->1725052440-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54222872D%7C%7C54222872&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1725052440-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-agendamentos->3043091304-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56333929D%7C%7C56333929&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3043091304-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-agendamentos->2463665264-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56333925D%7C%7C56333925&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2463665264-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-agendamentos->414222541-voltar`, () => {
    cy.visit(
      '/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~48767671D%7C%7C48767671&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="414222541-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-agendamentos->2390360502-voltar`, () => {
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15931482D%7C%7C15931482&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2390360502-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-agendamentos->3724044816-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456196D%7C%7C47456196&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3724044816-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-agendamentos->4087847603-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456218D%7C%7C47456218&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4087847603-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-agendamentos->2909235083-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456197D%7C%7C47456197&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2909235083-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-agendamentos->2577835410-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456175D%7C%7C47456175&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2577835410-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-agendamentos->1466791461-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456249D%7C%7C47456249&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1466791461-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-agendamentos->3811599192-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456208D%7C%7C47456208&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3811599192-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-agendamentos->3121846514-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~50155256D%7C%7C50155256&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3121846514-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-agendamentos->65776735-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456183D%7C%7C47456183&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="65776735-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-agendamentos->277761785-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456154D%7C%7C47456154&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="277761785-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-agendamentos->1126770727-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456229D%7C%7C47456229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1126770727-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-agendamentos->3383681734-voltar`, () => {
    cy.visit(
      '/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456169D%7C%7C47456169&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3383681734-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
