describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario`, () => {
    const actualId = [`root`, `dicionario`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces`, () => {
    const actualId = [`root`, `intefaces`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element open`, () => {
    const actualId = [`root`, `open`];
    cy.clickIfExist(`[data-cy="open"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap`, () => {
    const actualId = [`root`, `sap`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-r12`, () => {
    const actualId = [`root`, `oracle-r12`];
    cy.clickIfExist(`[data-cy="oracle-r12"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-erp-cloud`, () => {
    const actualId = [`root`, `oracle-erp-cloud`];
    cy.clickIfExist(`[data-cy="oracle-erp-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
    const actualId = [`root`, `processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
    const actualId = [`root`, `relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw`, () => {
    const actualId = [`root`, `sfw`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios/processos-customizados`, () => {
    const actualId = [`root`, `relatorios/processos-customizados`];
    cy.clickIfExist(`[data-cy="relatorios/processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
    const actualId = [`root`, `downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 1273019102-atualizar`, () => {
    const actualId = [`root`, `1273019102-atualizar`];
    cy.clickIfExist(`[data-cy="1273019102-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/repositorio`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/repositorio`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/repositorio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/visoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/relacionamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/dominios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`];
    cy.clickIfExist(`[data-cy="dicionario"]`);
    cy.clickIfExist(`[data-cy="dicionario/variaveis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/import-export"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/grupo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element open->open/equivalencia-codigo`, () => {
    const actualId = [`root`, `open`, `open/equivalencia-codigo`];
    cy.clickIfExist(`[data-cy="open"]`);
    cy.clickIfExist(`[data-cy="open/equivalencia-codigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/parametro-idoc"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/consulta-codigos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/parametro-integração`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/parametro-integração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/equivalencia-codigo`, () => {
    const actualId = [`root`, `sap`, `sap/equivalencia-codigo`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/equivalencia-codigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/msg-pendente`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/msg-pendente"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-r12->oracle-r12/r12-equivalencia-cod`, () => {
    const actualId = [`root`, `oracle-r12`, `oracle-r12/r12-equivalencia-cod`];
    cy.clickIfExist(`[data-cy="oracle-r12"]`);
    cy.clickIfExist(`[data-cy="oracle-r12/r12-equivalencia-cod"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/carga-oracle-cloud`, () => {
    const actualId = [`root`, `oracle-erp-cloud`, `oracle-erp-cloud/carga-oracle-cloud`];
    cy.clickIfExist(`[data-cy="oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="oracle-erp-cloud/carga-oracle-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/emissao-oracle-cloud`, () => {
    const actualId = [`root`, `oracle-erp-cloud`, `oracle-erp-cloud/emissao-oracle-cloud`];
    cy.clickIfExist(`[data-cy="oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="oracle-erp-cloud/emissao-oracle-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/execucao-interface`, () => {
    const actualId = [`root`, `processos`, `processos/execucao-interface`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/execucao-interface"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/geracao-loader-ctl"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-tabelas-intermediarias`, () => {
    const actualId = [`root`, `processos`, `processos/limpeza-tabelas-intermediarias`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpeza-tabelas-intermediarias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/testes-interfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/sap`, () => {
    const actualId = [`root`, `processos`, `processos/sap`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/sap"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/listaRegistrosDeOpen"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/listaDofsExistentesBase"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/documentacaoOpenInterfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/testesInterfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/processos`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/regra-coop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1674398458-power-search-button`, () => {
    const actualId = [`root`, `downloads`, `1674398458-power-search-button`];
    cy.visit('/http://system-A11/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1674398458-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->1674398458-download`, () => {
    const actualId = [`root`, `downloads`, `1674398458-download`];
    cy.visit('/http://system-A11/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1674398458-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->1674398458-detalhes`, () => {
    const actualId = [`root`, `downloads`, `1674398458-detalhes`];
    cy.visit('/http://system-A11/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1674398458-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->1674398458-excluir`, () => {
    const actualId = [`root`, `downloads`, `1674398458-excluir`];
    cy.visit('/http://system-A11/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1674398458-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-novo`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-novo`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-power-search-button`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-power-search-button`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-selectoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-deleteoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-deleteoutlined`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-novo`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-novo`];
    cy.visit('/http://system-A11/dic-relacionamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515562749-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-power-search-button`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-power-search-button`];
    cy.visit('/http://system-A11/dic-relacionamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515562749-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-eyeoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-eyeoutlined`];
    cy.visit('/http://system-A11/dic-relacionamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515562749-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-deleteoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-deleteoutlined`];
    cy.visit('/http://system-A11/dic-relacionamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515562749-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-novo`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-novo`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-power-search-button`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-power-search-button`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-selectoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-eyeoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-eyeoutlined`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-deleteoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-deleteoutlined`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-novo`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-novo`];
    cy.visit('/http://system-A11/dic-variavel-plsql');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="107676803-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-power-search-button`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-power-search-button`];
    cy.visit('/http://system-A11/dic-variavel-plsql');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="107676803-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-eyeoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-eyeoutlined`];
    cy.visit('/http://system-A11/dic-variavel-plsql');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="107676803-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-deleteoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-deleteoutlined`];
    cy.visit('/http://system-A11/dic-variavel-plsql');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="107676803-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-novo`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-novo`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-power-search-button`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-dashoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-dashoutlined`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-dashoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-deleteoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-deleteoutlined`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/importacao`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/importacao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/importacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-novo`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-novo`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-power-search-button`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-copyoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-copyoutlined`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-copyoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-deleteoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-deleteoutlined`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values open->open/equivalencia-codigo->2975773813-powerselect-sistemaSelected and submit`, () => {
    const actualId = [`root`, `open`, `open/equivalencia-codigo`, `2975773813-powerselect-sistemaSelected`];
    cy.clickIfExist(`[data-cy="open"]`);
    cy.clickIfExist(`[data-cy="open/equivalencia-codigo"]`);
    cy.fillInputPowerSelect(`[data-cy="2975773813-powerselect-sistemaSelected"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-novo`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-novo`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-power-search-button`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-power-search-button`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-dashoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-dashoutlined`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-dashoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-eyeoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-eyeoutlined`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-deleteoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-deleteoutlined`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-novo`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-novo`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2359284755-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-power-search-button`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-power-search-button`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2359284755-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-eyeoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-eyeoutlined`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2359284755-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2359284755-unorderedlistoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-deleteoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-deleteoutlined`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2359284755-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-novo`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-novo`];
    cy.visit('/http://system-A11/parametros-integracao-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="901104904-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-power-search-button`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-power-search-button`];
    cy.visit('/http://system-A11/parametros-integracao-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="901104904-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-eyeoutlined`];
    cy.visit('/http://system-A11/parametros-integracao-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="901104904-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-deleteoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-deleteoutlined`];
    cy.visit('/http://system-A11/parametros-integracao-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="901104904-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/equivalencia-codigo->1065827415-power-search-button`, () => {
    const actualId = [`root`, `sap`, `sap/equivalencia-codigo`, `1065827415-power-search-button`];
    cy.visit('/http://system-A11/equivalencia-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1065827415-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/equivalencia-codigo->1065827415-powerselect-applsynId-1065827415-power-search-input and submit`, () => {
    const actualId = [`root`, `sap`, `sap/equivalencia-codigo`, `1065827415-powerselect-applsynId-1065827415-power-search-input`];
    cy.clickIfExist(`[data-cy="sap"]`);
    cy.clickIfExist(`[data-cy="sap/equivalencia-codigo"]`);
    cy.fillInputPowerSelect(`[data-cy="1065827415-powerselect-applsynId"] input`);
    cy.fillInputPowerSearch(`[data-cy="1065827415-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-power-search-button`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-power-search-button`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-reloadoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-reloadoutlined`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-reloadoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-eyeoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-eyeoutlined`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-deleteoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-deleteoutlined`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/carga-oracle-cloud->3724795830-power-search-button`, () => {
    const actualId = [`root`, `oracle-erp-cloud`, `oracle-erp-cloud/carga-oracle-cloud`, `3724795830-power-search-button`];
    cy.visit('/http://system-A11/carga-oracle-cloud');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724795830-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/emissao-oracle-cloud->2643980789-power-search-button`, () => {
    const actualId = [`root`, `oracle-erp-cloud`, `oracle-erp-cloud/emissao-oracle-cloud`, `2643980789-power-search-button`];
    cy.visit('/http://system-A11/emissao-oracle-cloud');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643980789-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element oracle-erp-cloud->oracle-erp-cloud/emissao-oracle-cloud->2643980789-downloadoutlined`, () => {
    const actualId = [`root`, `oracle-erp-cloud`, `oracle-erp-cloud/emissao-oracle-cloud`, `2643980789-downloadoutlined`];
    cy.visit('/http://system-A11/emissao-oracle-cloud');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2643980789-downloadoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-executar`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-executar`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-agendamentos`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-power-search-button`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-visualização`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-regerar`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-detalhes`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-abrir visualização`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-excluir`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-executar`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-executar`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-agendamentos`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-power-search-button`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-visualização`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-regerar`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-detalhes`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-abrir visualização`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-excluir`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/sap"]`);
    cy.clickIfExist(`[data-cy="processos/sap/limpeza-log-sap-x-synchro"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-dados-contabeis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-executar`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-agendamentos`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-power-search-button`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-visualização`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-regerar`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-detalhes`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-excluir`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-abrir visualização`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-executar`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-agendamentos`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-power-search-button`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-visualização`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-executar`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-agendamentos`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-power-search-button`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-visualização`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-regerar`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-detalhes`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-abrir visualização`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-excluir`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-executar`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-agendamentos`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-power-search-button`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-visualização`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-regerar`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-detalhes`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-abrir visualização`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-excluir`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/carga-dados-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/carga-dados-contabeis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/dofs-rejeitados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/registros-rejeitados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/valores-parametros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/exec-cargas-solicitadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12"]`);
    cy.clickIfExist(`[data-cy="relatorios/integracao-oracle-r12/processo-limpeza-inventario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/propriedades"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/solicitacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/consultar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/periodicidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/por-estabelecimento`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/integracao-com-sfw/por-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-nfe"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-ecf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/relatorio-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/contrib-social-retida-fonte"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/indice-financeiro-sintetico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio"]`);
    cy.clickIfExist(`[data-cy="sfw/relatorios-de-apoio/indice-financeiro-analitico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/processos"]`);
    cy.clickIfExist(`[data-cy="sfw/processos/exclusao-icms-base-calculo-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`];
    cy.clickIfExist(`[data-cy="sfw"]`);
    cy.clickIfExist(`[data-cy="sfw/processos"]`);
    cy.clickIfExist(`[data-cy="sfw/processos/carga-estabelecimentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-novo`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-novo`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-power-search-button`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-copiar regra`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-copiar regra`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-copiar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-visualizar/editar`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-visualizar/editar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-excluir`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-excluir`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-novo->2732912987-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-novo`, `2732912987-salvar`];
    cy.visit('/http://system-A11/dic-visao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2732912987-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-novo->2732912987-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-novo`, `2732912987-voltar`];
    cy.visit('/http://system-A11/dic-visao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2732912987-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/visoes->1680412750-novo->2732912987-input-titulo-2732912987-powerselect-repId-2732912987-checkbox-indDistinct-2732912987-checkbox-indInvalido-2732912987-checkbox-indDesativado-2732912987-textarea-descricao-2732912987-textarea-sqlOptimizerHints and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-novo`, `2732912987-input-titulo-2732912987-powerselect-repId-2732912987-checkbox-indDistinct-2732912987-checkbox-indInvalido-2732912987-checkbox-indDesativado-2732912987-textarea-descricao-2732912987-textarea-sqlOptimizerHints`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-novo"]`);
    cy.fillInput(`[data-cy="2732912987-input-titulo"] textarea`, `Berkshire`);
    cy.fillInputPowerSelect(`[data-cy="2732912987-powerselect-repId"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2732912987-checkbox-indDistinct"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2732912987-checkbox-indInvalido"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2732912987-checkbox-indDesativado"] textarea`);
    cy.fillInput(`[data-cy="2732912987-textarea-descricao"] input`, `ivory`);
    cy.fillInput(`[data-cy="2732912987-textarea-sqlOptimizerHints"] input`, `Avenida`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-novo`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-novo`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-mais operações`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-mais operações`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-power-search-button`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-power-search-button`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-eyeoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-eyeoutlined`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-deleteoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-deleteoutlined`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-mais operações`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-mais operações`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-remover item`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-remover item`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-salvar`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-voltar`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-input-titulo-3958387426-powerselect-repId-3958387426-checkbox-indDistinct-3958387426-checkbox-indInvalido-3958387426-checkbox-indDesativado-3958387426-textarea-descricao-3958387426-textarea-sqlOptimizerHints and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-input-titulo-3958387426-powerselect-repId-3958387426-checkbox-indDistinct-3958387426-checkbox-indInvalido-3958387426-checkbox-indDesativado-3958387426-textarea-descricao-3958387426-textarea-sqlOptimizerHints`];
    cy.visit('/http://system-A11/dic-visao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680412750-eyeoutlined"]`);
    cy.fillInput(`[data-cy="3958387426-input-titulo"] textarea`, `leading edge`);
    cy.fillInputPowerSelect(`[data-cy="3958387426-powerselect-repId"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3958387426-checkbox-indDistinct"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3958387426-checkbox-indInvalido"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3958387426-checkbox-indDesativado"] textarea`);
    cy.fillInput(`[data-cy="3958387426-textarea-descricao"] input`, `Graphical User Interface`);
    cy.fillInput(`[data-cy="3958387426-textarea-sqlOptimizerHints"] input`, `hack`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-novo->1901700620-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-novo`, `1901700620-salvar`];
    cy.visit('/http://system-A11/dic-relacionamento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1901700620-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-novo->1901700620-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-novo`, `1901700620-voltar`];
    cy.visit('/http://system-A11/dic-relacionamento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1901700620-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/relacionamentos->2515562749-novo->1901700620-powerselect-idTabelaPai-1901700620-powerselect-idTabelaFilha-1901700620-input-nomeRelacionamento-1901700620-input-chavePai-1901700620-input-chaveFilha-1901700620-powerselect-cardinalidadeFilha and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-novo`, `1901700620-powerselect-idTabelaPai-1901700620-powerselect-idTabelaFilha-1901700620-input-nomeRelacionamento-1901700620-input-chavePai-1901700620-input-chaveFilha-1901700620-powerselect-cardinalidadeFilha`];
    cy.visit('/http://system-A11/dic-relacionamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515562749-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1901700620-powerselect-idTabelaPai"] input`);
    cy.fillInputPowerSelect(`[data-cy="1901700620-powerselect-idTabelaFilha"] input`);
    cy.fillInput(`[data-cy="1901700620-input-nomeRelacionamento"] textarea`, `back up`);
    cy.fillInput(`[data-cy="1901700620-input-chavePai"] textarea`, `Tuna`);
    cy.fillInput(`[data-cy="1901700620-input-chaveFilha"] textarea`, `quantifying`);
    cy.fillInputPowerSelect(`[data-cy="1901700620-powerselect-cardinalidadeFilha"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-eyeoutlined->2285690579-remover item`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-eyeoutlined`, `2285690579-remover item`];
    cy.visit('/http://system-A11/dic-relacionamento/editar/111583');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2285690579-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-eyeoutlined->2285690579-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-eyeoutlined`, `2285690579-salvar`];
    cy.visit('/http://system-A11/dic-relacionamento/editar/111583');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2285690579-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/relacionamentos->2515562749-eyeoutlined->2285690579-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-eyeoutlined`, `2285690579-voltar`];
    cy.visit('/http://system-A11/dic-relacionamento/editar/111583');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2285690579-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/relacionamentos->2515562749-eyeoutlined->2285690579-input-nomeRelacionamento-2285690579-input-chavePai-2285690579-input-chaveFilha-2285690579-powerselect-cardinalidadeFilha and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/relacionamentos`, `2515562749-eyeoutlined`, `2285690579-input-nomeRelacionamento-2285690579-input-chavePai-2285690579-input-chaveFilha-2285690579-powerselect-cardinalidadeFilha`];
    cy.visit('/http://system-A11/dic-relacionamento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515562749-eyeoutlined"]`);
    cy.fillInput(`[data-cy="2285690579-input-nomeRelacionamento"] textarea`, `virtual`);
    cy.fillInput(`[data-cy="2285690579-input-chavePai"] textarea`, `transparent`);
    cy.fillInput(`[data-cy="2285690579-input-chaveFilha"] textarea`, `synthesize`);
    cy.fillInputPowerSelect(`[data-cy="2285690579-powerselect-cardinalidadeFilha"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-novo->3945466812-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-novo`, `3945466812-salvar`];
    cy.visit('/http://system-A11/dic-dominio/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3945466812-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-novo->3945466812-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-novo`, `3945466812-voltar`];
    cy.visit('/http://system-A11/dic-dominio/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3945466812-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/dominios->1340226893-novo->3945466812-input-nome-3945466812-checkbox-indDesativado-3945466812-textarea-descricao-3945466812-powerselect-tipoDado-3945466812-input-number-tamanho-3945466812-textarea-checkConstraint-3945466812-textarea-selectLov-3945466812-checkbox-indNull-3945466812-checkbox-indMaiuscula-3945466812-checkbox-indEditavel and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-novo`, `3945466812-input-nome-3945466812-checkbox-indDesativado-3945466812-textarea-descricao-3945466812-powerselect-tipoDado-3945466812-input-number-tamanho-3945466812-textarea-checkConstraint-3945466812-textarea-selectLov-3945466812-checkbox-indNull-3945466812-checkbox-indMaiuscula-3945466812-checkbox-indEditavel`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-novo"]`);
    cy.fillInput(`[data-cy="3945466812-input-nome"] textarea`, `Produtos`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3945466812-checkbox-indDesativado"] textarea`);
    cy.fillInput(`[data-cy="3945466812-textarea-descricao"] input`, `Licensed Rubber Car`);
    cy.fillInputPowerSelect(`[data-cy="3945466812-powerselect-tipoDado"] input`);
    cy.fillInput(`[data-cy="3945466812-input-number-tamanho"] textarea`, `6`);
    cy.fillInput(`[data-cy="3945466812-textarea-checkConstraint"] input`, `Viela`);
    cy.fillInput(`[data-cy="3945466812-textarea-selectLov"] input`, `enhance`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3945466812-checkbox-indNull"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3945466812-checkbox-indMaiuscula"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3945466812-checkbox-indEditavel"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-novo`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-novo`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2424881512-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-power-search-button`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-power-search-button`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2424881512-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-eyeoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-eyeoutlined`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2424881512-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-deleteoutlined`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-deleteoutlined`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2424881512-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-eyeoutlined->3572973699-remover item`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-eyeoutlined`, `3572973699-remover item`];
    cy.visit('/http://system-A11/dic-dominio/editar/8464');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3572973699-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-eyeoutlined->3572973699-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-eyeoutlined`, `3572973699-salvar`];
    cy.visit('/http://system-A11/dic-dominio/editar/8464');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3572973699-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-eyeoutlined->3572973699-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-eyeoutlined`, `3572973699-voltar`];
    cy.visit('/http://system-A11/dic-dominio/editar/8464');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3572973699-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/dominios->1340226893-eyeoutlined->3572973699-input-nome-3572973699-checkbox-indDesativado-3572973699-textarea-descricao-3572973699-powerselect-tipoDado-3572973699-input-number-tamanho-3572973699-textarea-checkConstraint-3572973699-textarea-selectLov-3572973699-checkbox-indNull-3572973699-checkbox-indMaiuscula-3572973699-checkbox-indEditavel and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-eyeoutlined`, `3572973699-input-nome-3572973699-checkbox-indDesativado-3572973699-textarea-descricao-3572973699-powerselect-tipoDado-3572973699-input-number-tamanho-3572973699-textarea-checkConstraint-3572973699-textarea-selectLov-3572973699-checkbox-indNull-3572973699-checkbox-indMaiuscula-3572973699-checkbox-indEditavel`];
    cy.visit('/http://system-A11/dic-dominio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1340226893-eyeoutlined"]`);
    cy.fillInput(`[data-cy="3572973699-input-nome"] textarea`, `hack`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3572973699-checkbox-indDesativado"] textarea`);
    cy.fillInput(`[data-cy="3572973699-textarea-descricao"] input`, `Oficial`);
    cy.fillInputPowerSelect(`[data-cy="3572973699-powerselect-tipoDado"] input`);
    cy.fillInput(`[data-cy="3572973699-input-number-tamanho"] textarea`, `9`);
    cy.fillInput(`[data-cy="3572973699-textarea-checkConstraint"] input`, `morph`);
    cy.fillInput(`[data-cy="3572973699-textarea-selectLov"] input`, `Diretor`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3572973699-checkbox-indNull"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3572973699-checkbox-indMaiuscula"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3572973699-checkbox-indEditavel"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-novo->4197209670-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-novo`, `4197209670-salvar`];
    cy.visit('/http://system-A11/dic-variavel-plsql/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4197209670-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-novo->4197209670-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-novo`, `4197209670-voltar`];
    cy.visit('/http://system-A11/dic-variavel-plsql/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4197209670-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/variaveis->107676803-novo->4197209670-input-nome-4197209670-powerselect-tipoDado-4197209670-textarea-descricao-4197209670-checkbox-indDesativado and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-novo`, `4197209670-input-nome-4197209670-powerselect-tipoDado-4197209670-textarea-descricao-4197209670-checkbox-indDesativado`];
    cy.visit('/http://system-A11/dic-variavel-plsql');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="107676803-novo"]`);
    cy.fillInput(`[data-cy="4197209670-input-nome"] textarea`, `Music`);
    cy.fillInputPowerSelect(`[data-cy="4197209670-powerselect-tipoDado"] input`);
    cy.fillInput(`[data-cy="4197209670-textarea-descricao"] input`, `Handmade`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4197209670-checkbox-indDesativado"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-eyeoutlined->4034198413-remover item`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-eyeoutlined`, `4034198413-remover item`];
    cy.visit('/http://system-A11/dic-variavel-plsql/editar/169572');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034198413-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-eyeoutlined->4034198413-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-eyeoutlined`, `4034198413-salvar`];
    cy.visit('/http://system-A11/dic-variavel-plsql/editar/169572');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034198413-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/variaveis->107676803-eyeoutlined->4034198413-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-eyeoutlined`, `4034198413-voltar`];
    cy.visit('/http://system-A11/dic-variavel-plsql/editar/169572');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4034198413-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/variaveis->107676803-eyeoutlined->4034198413-input-nome-4034198413-powerselect-tipoDado-4034198413-textarea-descricao-4034198413-checkbox-indDesativado and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/variaveis`, `107676803-eyeoutlined`, `4034198413-input-nome-4034198413-powerselect-tipoDado-4034198413-textarea-descricao-4034198413-checkbox-indDesativado`];
    cy.visit('/http://system-A11/dic-variavel-plsql');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="107676803-eyeoutlined"]`);
    cy.fillInput(`[data-cy="4034198413-input-nome"] textarea`, `optical`);
    cy.fillInputPowerSelect(`[data-cy="4034198413-powerselect-tipoDado"] input`);
    cy.fillInput(`[data-cy="4034198413-textarea-descricao"] input`, `RAM`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4034198413-checkbox-indDesativado"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-novo->1183833761-salvar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-novo`, `1183833761-salvar`];
    cy.visit('/http://system-A11/interface/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1183833761-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-novo->1183833761-voltar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-novo`, `1183833761-voltar`];
    cy.visit('/http://system-A11/interface/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1183833761-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values intefaces->intefaces/import-export->864317256-novo->1183833761-input-codigo-1183833761-input-titulo-1183833761-powerselect-indDesativado-1183833761-powerselect-indProcParalelo-1183833761-powerselect-sisCodigo-1183833761-powerselect-visId-1183833761-powerselect-ctrlCommit-1183833761-input-number-numRegCommit and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-novo`, `1183833761-input-codigo-1183833761-input-titulo-1183833761-powerselect-indDesativado-1183833761-powerselect-indProcParalelo-1183833761-powerselect-sisCodigo-1183833761-powerselect-visId-1183833761-powerselect-ctrlCommit-1183833761-input-number-numRegCommit`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-novo"]`);
    cy.fillInput(`[data-cy="1183833761-input-codigo"] textarea`, `redundant`);
    cy.fillInput(`[data-cy="1183833761-input-titulo"] textarea`, `Direto`);
    cy.fillInputPowerSelect(`[data-cy="1183833761-powerselect-indDesativado"] input`);
    cy.fillInputPowerSelect(`[data-cy="1183833761-powerselect-indProcParalelo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1183833761-powerselect-sisCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1183833761-powerselect-visId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1183833761-powerselect-ctrlCommit"] input`);
    cy.fillInput(`[data-cy="1183833761-input-number-numRegCommit"] textarea`, `9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-dashoutlined->864317256-item-`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-dashoutlined`, `864317256-item-`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-dashoutlined"]`);
    cy.clickIfExist(`[data-cy="864317256-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-remover item`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-remover item`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-salvar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-salvar`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-voltar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-voltar`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-novo`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-novo`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-power-search-button`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-eyeoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-eyeoutlined`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-deleteoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-deleteoutlined`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-input-codigo-1139381160-input-titulo-1139381160-powerselect-indDesativado-1139381160-powerselect-indProcParalelo-1139381160-powerselect-sisCodigo-1139381160-powerselect-visId-1139381160-powerselect-ctrlCommit-1139381160-input-number-numRegCommit and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-input-codigo-1139381160-input-titulo-1139381160-powerselect-indDesativado-1139381160-powerselect-indProcParalelo-1139381160-powerselect-sisCodigo-1139381160-powerselect-visId-1139381160-powerselect-ctrlCommit-1139381160-input-number-numRegCommit`];
    cy.visit('/http://system-A11/interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="864317256-eyeoutlined"]`);
    cy.fillInput(`[data-cy="1139381160-input-codigo"] textarea`, `outofthebox`);
    cy.fillInput(`[data-cy="1139381160-input-titulo"] textarea`, `Coordenador`);
    cy.fillInputPowerSelect(`[data-cy="1139381160-powerselect-indDesativado"] input`);
    cy.fillInputPowerSelect(`[data-cy="1139381160-powerselect-indProcParalelo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1139381160-powerselect-sisCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1139381160-powerselect-visId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1139381160-powerselect-ctrlCommit"] input`);
    cy.fillInput(`[data-cy="1139381160-input-number-numRegCommit"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-executar interfaces`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-executar interfaces`];
    cy.visit('/http://system-A11/manutencao-interface/editar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2299773694-executar interfaces"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/importacao->444533017-exportar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/importacao`, `444533017-exportar`];
    cy.visit('/http://system-A11/manutencao-interface/importar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="444533017-exportar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/importacao->444533017-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/importacao`, `444533017-power-search-button`];
    cy.visit('/http://system-A11/manutencao-interface/importar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="444533017-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/importacao->444533017-eyeoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/importacao`, `444533017-eyeoutlined`];
    cy.visit('/http://system-A11/manutencao-interface/importar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="444533017-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-downloads`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-downloads`];
    cy.visit('/http://system-A11/manutencao-interface/exportar');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3235832168-downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-powerselect-exportacao and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-powerselect-exportacao`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.fillInputPowerSelect(`[data-cy="3235832168-powerselect-exportacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-novo->1006927813-salvar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-novo`, `1006927813-salvar`];
    cy.visit('/http://system-A11/grupo-interface/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1006927813-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-novo->1006927813-voltar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-novo`, `1006927813-voltar`];
    cy.visit('/http://system-A11/grupo-interface/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1006927813-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values intefaces->intefaces/grupo->233875364-novo->1006927813-input-codigo-1006927813-powerselect-indAtivo-1006927813-powerselect-indUsaApi-1006927813-textarea-descricao and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-novo`, `1006927813-input-codigo-1006927813-powerselect-indAtivo-1006927813-powerselect-indUsaApi-1006927813-textarea-descricao`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-novo"]`);
    cy.fillInput(`[data-cy="1006927813-input-codigo"] textarea`, `Ouguiya`);
    cy.fillInputPowerSelect(`[data-cy="1006927813-powerselect-indAtivo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1006927813-powerselect-indUsaApi"] input`);
    cy.fillInput(`[data-cy="1006927813-textarea-descricao"] input`, `Diretor`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-remover item`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-remover item`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-salvar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-salvar`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-voltar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-voltar`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-power-search-button`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-ordenar grupos de interface`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-ordenar grupos de interface`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-ordenar grupos de interface"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-input-codigo-2382481868-powerselect-indAtivo-2382481868-powerselect-indUsaApi-2382481868-textarea-descricao-2382481868-power-search-input and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-input-codigo-2382481868-powerselect-indAtivo-2382481868-powerselect-indUsaApi-2382481868-textarea-descricao-2382481868-power-search-input`];
    cy.visit('/http://system-A11/grupo-interface');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="233875364-eyeoutlined"]`);
    cy.fillInput(`[data-cy="2382481868-input-codigo"] textarea`, `deposit`);
    cy.fillInputPowerSelect(`[data-cy="2382481868-powerselect-indAtivo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2382481868-powerselect-indUsaApi"] input`);
    cy.fillInput(`[data-cy="2382481868-textarea-descricao"] input`, `supplychains`);
    cy.fillInputPowerSearch(`[data-cy="2382481868-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-novo->4123311061-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-novo`, `4123311061-salvar`];
    cy.visit('/http://system-A11/parametrizacao-idoc/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4123311061-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-novo->4123311061-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-novo`, `4123311061-voltar`];
    cy.visit('/http://system-A11/parametrizacao-idoc/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4123311061-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/parametro-idoc->2363433364-novo->4123311061-input-idocCode-4123311061-powerselect-enabled-4123311061-powerselect-online-4123311061-powerselect-indEntradaSaida-4123311061-input-comments and submit`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-novo`, `4123311061-input-idocCode-4123311061-powerselect-enabled-4123311061-powerselect-online-4123311061-powerselect-indEntradaSaida-4123311061-input-comments`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-novo"]`);
    cy.fillInput(`[data-cy="4123311061-input-idocCode"] textarea`, `grey`);
    cy.fillInputPowerSelect(`[data-cy="4123311061-powerselect-enabled"] input`);
    cy.fillInputPowerSelect(`[data-cy="4123311061-powerselect-online"] input`);
    cy.fillInputPowerSelect(`[data-cy="4123311061-powerselect-indEntradaSaida"] input`);
    cy.fillInput(`[data-cy="4123311061-input-comments"] textarea`, `indigo`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-dashoutlined->2363433364-item-`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-dashoutlined`, `2363433364-item-`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-dashoutlined"]`);
    cy.clickIfExist(`[data-cy="2363433364-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-eyeoutlined->1007204828-remover item`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-eyeoutlined`, `1007204828-remover item`];
    cy.visit('/http://system-A11/parametrizacao-idoc/editar/415');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1007204828-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-eyeoutlined->1007204828-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-eyeoutlined`, `1007204828-salvar`];
    cy.visit('/http://system-A11/parametrizacao-idoc/editar/415');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1007204828-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-idoc->2363433364-eyeoutlined->1007204828-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-eyeoutlined`, `1007204828-voltar`];
    cy.visit('/http://system-A11/parametrizacao-idoc/editar/415');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1007204828-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/parametro-idoc->2363433364-eyeoutlined->1007204828-input-idocCode-1007204828-powerselect-enabled-1007204828-powerselect-online-1007204828-input-comments and submit`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-idoc`, `2363433364-eyeoutlined`, `1007204828-input-idocCode-1007204828-powerselect-enabled-1007204828-powerselect-online-1007204828-input-comments`];
    cy.visit('/http://system-A11/parametrizacao-idoc');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2363433364-eyeoutlined"]`);
    cy.fillInput(`[data-cy="1007204828-input-idocCode"] textarea`, `program`);
    cy.fillInputPowerSelect(`[data-cy="1007204828-powerselect-enabled"] input`);
    cy.fillInputPowerSelect(`[data-cy="1007204828-powerselect-online"] input`);
    cy.fillInput(`[data-cy="1007204828-input-comments"] textarea`, `synthesizing`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-novo->1700446390-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-novo`, `1700446390-salvar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700446390-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-novo->1700446390-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-novo`, `1700446390-voltar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1700446390-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/consulta-codigos->2359284755-novo->1700446390-powerselect-applsynId-1700446390-input-parCodigo-1700446390-textarea-descricao-1700446390-input-tabelaSyn-1700446390-input-colunaSyn-1700446390-input-colunaDescSyn-1700446390-input-condicaoSyn and submit`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-novo`, `1700446390-powerselect-applsynId-1700446390-input-parCodigo-1700446390-textarea-descricao-1700446390-input-tabelaSyn-1700446390-input-colunaSyn-1700446390-input-colunaDescSyn-1700446390-input-condicaoSyn`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2359284755-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1700446390-powerselect-applsynId"] input`);
    cy.fillInput(`[data-cy="1700446390-input-parCodigo"] textarea`, `Exclusive`);
    cy.fillInput(`[data-cy="1700446390-textarea-descricao"] input`, `invoice`);
    cy.fillInput(`[data-cy="1700446390-input-tabelaSyn"] textarea`, `Executivo`);
    cy.fillInput(`[data-cy="1700446390-input-colunaSyn"] textarea`, `orange`);
    cy.fillInput(`[data-cy="1700446390-input-colunaDescSyn"] textarea`, `Rustic`);
    cy.fillInput(`[data-cy="1700446390-input-condicaoSyn"] textarea`, `Produtor`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-eyeoutlined->979751421-remover item`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-eyeoutlined`, `979751421-remover item`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/editar/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="979751421-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-eyeoutlined->979751421-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-eyeoutlined`, `979751421-salvar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/editar/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="979751421-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-eyeoutlined->979751421-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-eyeoutlined`, `979751421-voltar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/editar/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="979751421-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-novo`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-novo`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1017782551-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-power-search-button`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-power-search-button`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1017782551-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-eyeoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-eyeoutlined`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1017782551-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-deleteoutlined`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-deleteoutlined`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1017782551-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-novo->1317937377-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-novo`, `1317937377-salvar`];
    cy.visit('/http://system-A11/parametros-integracao-sap/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1317937377-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-novo->1317937377-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-novo`, `1317937377-voltar`];
    cy.visit('/http://system-A11/parametros-integracao-sap/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1317937377-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/parametro-integração->901104904-novo->1317937377-powerselect-applsynId-1317937377-input-parCodigo-1317937377-input-definicao-1317937377-input-valor-1317937377-input-valorDefault and submit`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-novo`, `1317937377-powerselect-applsynId-1317937377-input-parCodigo-1317937377-input-definicao-1317937377-input-valor-1317937377-input-valorDefault`];
    cy.visit('/http://system-A11/parametros-integracao-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="901104904-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1317937377-powerselect-applsynId"] input`);
    cy.fillInput(`[data-cy="1317937377-input-parCodigo"] textarea`, `Movies`);
    cy.fillInput(`[data-cy="1317937377-input-definicao"] textarea`, `Savings Account`);
    cy.fillInput(`[data-cy="1317937377-input-valor"] textarea`, `Steel`);
    cy.fillInput(`[data-cy="1317937377-input-valorDefault"] textarea`, `Amap`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-remover item`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-eyeoutlined`, `1782392296-remover item`];
    cy.visit('/http://system-A11/parametros-integracao-sap/editar/%23%24%23%%%%ABC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1782392296-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-eyeoutlined`, `1782392296-salvar`];
    cy.visit('/http://system-A11/parametros-integracao-sap/editar/%23%24%23%%%%ABC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1782392296-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-eyeoutlined`, `1782392296-voltar`];
    cy.visit('/http://system-A11/parametros-integracao-sap/editar/%23%24%23%%%%ABC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1782392296-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/parametro-integração->901104904-eyeoutlined->1782392296-input-valor-1782392296-input-valorDefault and submit`, () => {
    const actualId = [`root`, `sap`, `sap/parametro-integração`, `901104904-eyeoutlined`, `1782392296-input-valor-1782392296-input-valorDefault`];
    cy.visit('/http://system-A11/parametros-integracao-sap');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="901104904-eyeoutlined"]`);
    cy.fillInput(`[data-cy="1782392296-input-valor"] textarea`, `Practical`);
    cy.fillInput(`[data-cy="1782392296-input-valorDefault"] textarea`, `bluetooth`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-eyeoutlined->2069071305-cancelar`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-eyeoutlined`, `2069071305-cancelar`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-eyeoutlined"]`);
    cy.clickIfExist(`[data-cy="2069071305-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-eyeoutlined->2069071305-aplicar`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-eyeoutlined`, `2069071305-aplicar`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-eyeoutlined"]`);
    cy.clickIfExist(`[data-cy="2069071305-aplicar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/msg-pendente->2069071305-eyeoutlined->2069071305-ok`, () => {
    const actualId = [`root`, `sap`, `sap/msg-pendente`, `2069071305-eyeoutlined`, `2069071305-ok`];
    cy.visit('/http://system-A11/sap/mensagem-pendente');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2069071305-eyeoutlined"]`);
    cy.clickIfExist(`[data-cy="2069071305-ok"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-agendamentos->1476829751-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-agendamentos`, `1476829751-voltar`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~239838D%7C%7C239838&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/geracao-loader-ctl->1476829751-visualização->1476829751-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-visualização`, `1476829751-item-`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1476829751-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-detalhes->1476829751-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-detalhes`, `1476829751-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-detalhes"]`);
    cy.clickIfExist(`[data-cy="1476829751-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-abrir visualização->1476829751-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-abrir visualização`, `1476829751-aumentar o zoom`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1476829751-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-abrir visualização->1476829751-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-abrir visualização`, `1476829751-diminuir o zoom`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1476829751-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-abrir visualização->1476829751-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-abrir visualização`, `1476829751-expandir`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1476829751-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-abrir visualização->1476829751-download`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-abrir visualização`, `1476829751-download`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1476829751-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-executar->1613817690-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-executar`, `1613817690-múltipla seleção`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-executar"]`);
    cy.clickIfExist(`[data-cy="1613817690-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-executar->1613817690-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-executar`, `1613817690-agendar`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-executar"]`);
    cy.clickIfExist(`[data-cy="1613817690-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-agendamentos->1613817690-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-agendamentos`, `1613817690-voltar`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56821999D%7C%7C56821999&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/testes-interfaces->1613817690-visualização->1613817690-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-visualização`, `1613817690-item-`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1613817690-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-detalhes->1613817690-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-detalhes`, `1613817690-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-detalhes"]`);
    cy.clickIfExist(`[data-cy="1613817690-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-abrir visualização->1613817690-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-abrir visualização`, `1613817690-aumentar o zoom`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1613817690-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-abrir visualização->1613817690-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-abrir visualização`, `1613817690-diminuir o zoom`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1613817690-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-abrir visualização->1613817690-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-abrir visualização`, `1613817690-expandir`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1613817690-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-abrir visualização->1613817690-download`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-abrir visualização`, `1613817690-download`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1613817690-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-executar`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-executar`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-agendamentos`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-power-search-button`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-visualização`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-regerar`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-detalhes`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-abrir visualização`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-excluir`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-fiscal/carga-programada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/enviar-gnre"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra"]`);
    cy.clickIfExist(`[data-cy="processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-executar->578205992-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-executar`, `578205992-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-executar"]`);
    cy.clickIfExist(`[data-cy="578205992-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-executar->578205992-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-executar`, `578205992-agendar`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-executar"]`);
    cy.clickIfExist(`[data-cy="578205992-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-agendamentos->578205992-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-agendamentos`, `578205992-voltar`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47379367D%7C%7C47379367&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-agendamentos->578205992-visualizar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-agendamentos`, `578205992-visualizar`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47379367D%7C%7C47379367&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/listaRegistrosDeOpen->578205992-visualização->578205992-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-visualização`, `578205992-item-`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="578205992-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-detalhes->578205992-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-detalhes`, `578205992-não há dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-detalhes"]`);
    cy.clickIfExist(`[data-cy="578205992-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-abrir visualização->578205992-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-abrir visualização`, `578205992-aumentar o zoom`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="578205992-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-abrir visualização->578205992-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-abrir visualização`, `578205992-diminuir o zoom`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="578205992-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-abrir visualização->578205992-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-abrir visualização`, `578205992-expandir`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="578205992-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-abrir visualização->578205992-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-abrir visualização`, `578205992-download`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="578205992-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-executar->459120590-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-executar`, `459120590-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-executar"]`);
    cy.clickIfExist(`[data-cy="459120590-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-executar->459120590-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-executar`, `459120590-agendar`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-executar"]`);
    cy.clickIfExist(`[data-cy="459120590-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-agendamentos->459120590-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-agendamentos`, `459120590-voltar`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~52196329D%7C%7C52196329&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/listaDofsExistentesBase->459120590-visualização->459120590-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-visualização`, `459120590-item-`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="459120590-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-executar->1466872463-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-executar`, `1466872463-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-executar"]`);
    cy.clickIfExist(`[data-cy="1466872463-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-executar->1466872463-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-executar`, `1466872463-agendar`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-executar"]`);
    cy.clickIfExist(`[data-cy="1466872463-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-agendamentos->1466872463-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-agendamentos`, `1466872463-voltar`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53794479D%7C%7C53794479&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/documentacaoOpenInterfaces->1466872463-visualização->1466872463-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-visualização`, `1466872463-item-`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1466872463-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-detalhes->1466872463-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-detalhes`, `1466872463-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-detalhes"]`);
    cy.clickIfExist(`[data-cy="1466872463-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-abrir visualização->1466872463-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-abrir visualização`, `1466872463-aumentar o zoom`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1466872463-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-abrir visualização->1466872463-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-abrir visualização`, `1466872463-diminuir o zoom`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1466872463-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-abrir visualização->1466872463-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-abrir visualização`, `1466872463-expandir`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1466872463-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-abrir visualização->1466872463-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-abrir visualização`, `1466872463-download`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1466872463-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-executar->1676676410-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-executar`, `1676676410-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-executar"]`);
    cy.clickIfExist(`[data-cy="1676676410-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-executar->1676676410-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-executar`, `1676676410-agendar`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-executar"]`);
    cy.clickIfExist(`[data-cy="1676676410-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-agendamentos->1676676410-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-agendamentos`, `1676676410-voltar`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56822003D%7C%7C56822003&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/testesInterfaces->1676676410-visualização->1676676410-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-visualização`, `1676676410-item-`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1676676410-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-detalhes->1676676410-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-detalhes`, `1676676410-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-detalhes"]`);
    cy.clickIfExist(`[data-cy="1676676410-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-abrir visualização->1676676410-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-abrir visualização`, `1676676410-aumentar o zoom`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1676676410-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-abrir visualização->1676676410-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-abrir visualização`, `1676676410-diminuir o zoom`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1676676410-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-abrir visualização->1676676410-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-abrir visualização`, `1676676410-expandir`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1676676410-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-abrir visualização->1676676410-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-abrir visualização`, `1676676410-download`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1676676410-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-regerar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-detalhes`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-excluir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-regerar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-detalhes`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-excluir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-regerar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-detalhes`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-abrir visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-excluir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-regerar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-detalhes`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-abrir visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-excluir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-regerar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-detalhes`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-excluir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-executar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-agendamentos`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-power-search-button`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-visualização`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-regerar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-detalhes`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-excluir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-power-search-button`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428971009-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-gerenciar labels`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-gerenciar labels`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428971009-gerenciar labels"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-visualizar parâmetros`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-visualizar parâmetros`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428971009-visualizar parâmetros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-visualizar/editar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-visualizar/editar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428971009-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ir para todas as obrigações`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ir para todas as obrigações`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ajuda`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ajuda`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-power-search-button`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-visualização`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-abrir visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-abrir visualização`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-visualizar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-visualizar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-novo`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-novo`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-power-search-button`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-editar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-editar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-excluir`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-excluir`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento->2977090664-novo`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/por-estabelecimento`, `2977090664-novo`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/por-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2977090664-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento->2977090664-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/por-estabelecimento`, `2977090664-power-search-button`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/por-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2977090664-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento->2977090664-excluir`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/por-estabelecimento`, `2977090664-excluir`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/por-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2977090664-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-regerar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-regerar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-detalhes`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-detalhes`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-abrir visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-abrir visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-excluir`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-excluir`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-executar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-agendamentos`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-power-search-button`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-regerar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-regerar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-detalhes`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-detalhes`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-abrir visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-abrir visualização`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-excluir`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-excluir`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-executar`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-agendamentos`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-power-search-button`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-visualização`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-regerar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-regerar`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-detalhes`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-detalhes`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-abrir visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-abrir visualização`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-excluir`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-excluir`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-executar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-executar`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-agendamentos`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-power-search-button`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-visualização`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-visualização`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-novo->405948803-salvar`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-novo`, `405948803-salvar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="405948803-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-novo->405948803-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-novo`, `405948803-voltar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="405948803-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/regra-coop->3894361894-novo->405948803-input-rgrCodigo-405948803-powerselect-ipcId-405948803-input-descricao and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-novo`, `405948803-input-rgrCodigo-405948803-powerselect-ipcId-405948803-input-descricao`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-novo"]`);
    cy.fillInput(`[data-cy="405948803-input-rgrCodigo"] textarea`, `deposit`);
    cy.fillInputPowerSelect(`[data-cy="405948803-powerselect-ipcId"] input`);
    cy.fillInput(`[data-cy="405948803-input-descricao"] textarea`, `Objectbased`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/regra-coop->3894361894-copiar regra->3894361894-input-copiaTo and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-copiar regra`, `3894361894-input-copiaTo`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-copiar regra"]`);
    cy.fillInput(`[data-cy="3894361894-input-copiaTo"] textarea`, `Cear`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-visualizar/editar->3688156426-remover item`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-visualizar/editar`, `3688156426-remover item`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa/editar/DRC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3688156426-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-visualizar/editar->3688156426-salvar`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-visualizar/editar`, `3688156426-salvar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa/editar/DRC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3688156426-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-visualizar/editar->3688156426-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-visualizar/editar`, `3688156426-voltar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa/editar/DRC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3688156426-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/regra-coop->3894361894-visualizar/editar->3688156426-power-search-button`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-visualizar/editar`, `3688156426-power-search-button`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa/editar/DRC');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3688156426-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/regra-coop->3894361894-visualizar/editar->3688156426-input-descricao-3688156426-power-search-input and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/regra-coop`, `3894361894-visualizar/editar`, `3688156426-input-descricao-3688156426-power-search-input`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/regra-cooperativa');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3894361894-visualizar/editar"]`);
    cy.fillInput(`[data-cy="3688156426-input-descricao"] textarea`, `Distrito Federal`);
    cy.fillInputPowerSearch(`[data-cy="3688156426-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-novo->4009397614-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-novo`, `4009397614-salvar`];
    cy.visit('/http://system-A11/dic-visao/145300/info/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4009397614-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-novo->4009397614-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-novo`, `4009397614-voltar`];
    cy.visit('/http://system-A11/dic-visao/145300/info/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4009397614-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-novo->4009397614-input-number-ordem-4009397614-input-nome-4009397614-powerselect-repjunId-4009397614-powerselect-dadId-4009397614-checkbox-indFromDual-4009397614-checkbox-indInvalido-4009397614-checkbox-indDesativado-4009397614-input-sqlSelect and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-novo`, `4009397614-input-number-ordem-4009397614-input-nome-4009397614-powerselect-repjunId-4009397614-powerselect-dadId-4009397614-checkbox-indFromDual-4009397614-checkbox-indInvalido-4009397614-checkbox-indDesativado-4009397614-input-sqlSelect`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-novo"]`);
    cy.fillInput(`[data-cy="4009397614-input-number-ordem"] textarea`, `4`);
    cy.fillInput(`[data-cy="4009397614-input-nome"] textarea`, `Distrito`);
    cy.fillInputPowerSelect(`[data-cy="4009397614-powerselect-repjunId"] input`);
    cy.fillInputPowerSelect(`[data-cy="4009397614-powerselect-dadId"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4009397614-checkbox-indFromDual"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4009397614-checkbox-indInvalido"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4009397614-checkbox-indDesativado"] textarea`);
    cy.fillInput(`[data-cy="4009397614-input-sqlSelect"] textarea`, `Sri Lanka`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-mais operações->1200682075-item-campos`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-mais operações`, `1200682075-item-campos`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-mais operações"]`);
    cy.clickIfExist(`[data-cy="1200682075-item-campos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-eyeoutlined->1299657909-remover item`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-eyeoutlined`, `1299657909-remover item`];
    cy.visit('/http://system-A11/dic-visao/145300/info/editar/976916');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1299657909-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-eyeoutlined->1299657909-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-eyeoutlined`, `1299657909-salvar`];
    cy.visit('/http://system-A11/dic-visao/145300/info/editar/976916');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1299657909-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-eyeoutlined->1299657909-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-eyeoutlined`, `1299657909-voltar`];
    cy.visit('/http://system-A11/dic-visao/145300/info/editar/976916');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1299657909-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-eyeoutlined->1299657909-input-number-ordem-1299657909-input-nome-1299657909-powerselect-repjunId-1299657909-powerselect-dadId-1299657909-checkbox-indFromDual-1299657909-checkbox-indInvalido-1299657909-checkbox-indDesativado and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-eyeoutlined`, `1299657909-input-number-ordem-1299657909-input-nome-1299657909-powerselect-repjunId-1299657909-powerselect-dadId-1299657909-checkbox-indFromDual-1299657909-checkbox-indInvalido-1299657909-checkbox-indDesativado`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-eyeoutlined"]`);
    cy.fillInput(`[data-cy="1299657909-input-number-ordem"] textarea`, `10`);
    cy.fillInput(`[data-cy="1299657909-input-nome"] textarea`, `Visionoriented`);
    cy.fillInputPowerSelect(`[data-cy="1299657909-powerselect-repjunId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1299657909-powerselect-dadId"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1299657909-checkbox-indFromDual"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1299657909-checkbox-indInvalido"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1299657909-checkbox-indDesativado"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-mais operações->3958387426-item-`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-mais operações`, `3958387426-item-`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-mais operações"]`);
    cy.clickIfExist(`[data-cy="3958387426-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-novo->2416566401-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-novo`, `2416566401-salvar`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2416566401-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-novo->2416566401-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-novo`, `2416566401-voltar`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2416566401-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-novo->2416566401-input-ordem-2416566401-textarea-valor-2416566401-textarea-significado and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-novo`, `2416566401-input-ordem-2416566401-textarea-valor-2416566401-textarea-significado`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2424881512-novo"]`);
    cy.fillInput(`[data-cy="2416566401-input-ordem"] textarea`, `Pizza`);
    cy.fillInput(`[data-cy="2416566401-textarea-valor"] input`, `Avenida`);
    cy.fillInput(`[data-cy="2416566401-textarea-significado"] input`, `Won`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-eyeoutlined->965913992-remover item`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-eyeoutlined`, `965913992-remover item`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores/editar/8465');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="965913992-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-eyeoutlined->965913992-salvar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-eyeoutlined`, `965913992-salvar`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores/editar/8465');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="965913992-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-eyeoutlined->965913992-voltar`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-eyeoutlined`, `965913992-voltar`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores/editar/8465');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="965913992-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values dicionario->dicionario/dominios->1340226893-selectoutlined->2424881512-eyeoutlined->965913992-input-ordem-965913992-textarea-valor-965913992-textarea-significado and submit`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/dominios`, `1340226893-selectoutlined`, `2424881512-eyeoutlined`, `965913992-input-ordem-965913992-textarea-valor-965913992-textarea-significado`];
    cy.visit('/http://system-A11/dic-dominio/8464/valores');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2424881512-eyeoutlined"]`);
    cy.fillInput(`[data-cy="965913992-input-ordem"] textarea`, `digital`);
    cy.fillInput(`[data-cy="965913992-textarea-valor"] input`, `Avon`);
    cy.fillInput(`[data-cy="965913992-textarea-significado"] input`, `withdrawal`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-novo->1139381160-cancelar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-novo`, `1139381160-cancelar`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-novo"]`);
    cy.clickIfExist(`[data-cy="1139381160-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/import-export->864317256-eyeoutlined->1139381160-novo->1139381160-adicionar`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/import-export`, `864317256-eyeoutlined`, `1139381160-novo`, `1139381160-adicionar`];
    cy.visit('/http://system-A11/interface/editar/258722');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1139381160-novo"]`);
    cy.clickIfExist(`[data-cy="1139381160-adicionar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao->2299773694-operações em lote`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`, `2299773694-operações em lote`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="2299773694-operações em lote"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao->2299773694-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`, `2299773694-power-search-button`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="2299773694-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao->2299773694-visualização`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`, `2299773694-visualização`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="2299773694-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao->2299773694-power-search-input and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`, `2299773694-power-search-input`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSearch(`[data-cy="2299773694-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-powerselect-exportacao->3235832168-power-search-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-powerselect-exportacao`, `3235832168-power-search-button`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.fillInputPowerSelect(`[data-cy="3235832168-powerselect-exportacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3235832168-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-powerselect-exportacao->3235832168-visualização`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-powerselect-exportacao`, `3235832168-visualização`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.fillInputPowerSelect(`[data-cy="3235832168-powerselect-exportacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3235832168-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-powerselect-exportacao->3235832168-power-search-input and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-powerselect-exportacao`, `3235832168-power-search-input`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.fillInputPowerSelect(`[data-cy="3235832168-powerselect-exportacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSearch(`[data-cy="3235832168-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-ordenar grupos de interface->2382481868-button`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-ordenar grupos de interface`, `2382481868-button`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-ordenar grupos de interface"]`);
    cy.clickIfExist(`[data-cy="2382481868-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-novo->2542532146-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-novo`, `2542532146-salvar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2542532146-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-novo->2542532146-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-novo`, `2542532146-voltar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2542532146-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-novo->2542532146-input-valCodigo-2542532146-input-descricao-2542532146-checkbox-indSyn and submit`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-novo`, `2542532146-input-valCodigo-2542532146-input-descricao-2542532146-checkbox-indSyn`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1017782551-novo"]`);
    cy.fillInput(`[data-cy="2542532146-input-valCodigo"] textarea`, `Awesome Frozen Pants`);
    cy.fillInput(`[data-cy="2542532146-input-descricao"] textarea`, `reinvent`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2542532146-checkbox-indSyn"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-eyeoutlined->2376071801-remover item`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-eyeoutlined`, `2376071801-remover item`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819/editar/1809');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2376071801-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-eyeoutlined->2376071801-salvar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-eyeoutlined`, `2376071801-salvar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819/editar/1809');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2376071801-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-eyeoutlined->2376071801-voltar`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-eyeoutlined`, `2376071801-voltar`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819/editar/1809');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2376071801-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sap->sap/consulta-codigos->2359284755-unorderedlistoutlined->1017782551-eyeoutlined->2376071801-input-valCodigo-2376071801-input-descricao-2376071801-checkbox-indSyn and submit`, () => {
    const actualId = [`root`, `sap`, `sap/consulta-codigos`, `2359284755-unorderedlistoutlined`, `1017782551-eyeoutlined`, `2376071801-input-valCodigo-2376071801-input-descricao-2376071801-checkbox-indSyn`];
    cy.visit('/http://system-A11/parametros-equivalencia-codigo/parametros-lista-valor/56819');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1017782551-eyeoutlined"]`);
    cy.fillInput(`[data-cy="2376071801-input-valCodigo"] textarea`, `connecting`);
    cy.fillInput(`[data-cy="2376071801-input-descricao"] textarea`, `Central`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2376071801-checkbox-indSyn"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/geracao-loader-ctl->1476829751-abrir visualização->1476829751-expandir->1476829751-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/geracao-loader-ctl`, `1476829751-abrir visualização`, `1476829751-expandir`, `1476829751-diminuir`];
    cy.visit('/http://system-A11/processos/geracao-loader-ctl?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1476829751-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1476829751-expandir"]`);
    cy.clickIfExist(`[data-cy="1476829751-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-executar->1613817690-múltipla seleção->1613817690-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-executar`, `1613817690-múltipla seleção`, `1613817690-cancelar`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-executar"]`);
    cy.clickIfExist(`[data-cy="1613817690-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1613817690-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/testes-interfaces->1613817690-abrir visualização->1613817690-expandir->1613817690-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/testes-interfaces`, `1613817690-abrir visualização`, `1613817690-expandir`, `1613817690-diminuir`];
    cy.visit('/http://system-A11/processos/testes-interfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1613817690-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1613817690-expandir"]`);
    cy.clickIfExist(`[data-cy="1613817690-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-executar->4055249190-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-executar`, `4055249190-múltipla seleção`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-executar"]`);
    cy.clickIfExist(`[data-cy="4055249190-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-executar->4055249190-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-executar`, `4055249190-agendar`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-executar"]`);
    cy.clickIfExist(`[data-cy="4055249190-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-executar->4055249190-input-pdias and submit`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-executar`, `4055249190-input-pdias`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-executar"]`);
    cy.fillInput(`[data-cy="4055249190-input-pdias"] textarea`, `productize`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-agendamentos->4055249190-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-agendamentos`, `4055249190-voltar`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~4771512D%7C%7C4771512&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-visualização->4055249190-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-visualização`, `4055249190-item-`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4055249190-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-detalhes->4055249190-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-detalhes`, `4055249190-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-detalhes"]`);
    cy.clickIfExist(`[data-cy="4055249190-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-abrir visualização->4055249190-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-abrir visualização`, `4055249190-aumentar o zoom`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4055249190-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-abrir visualização->4055249190-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-abrir visualização`, `4055249190-diminuir o zoom`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4055249190-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-abrir visualização->4055249190-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-abrir visualização`, `4055249190-expandir`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4055249190-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-abrir visualização->4055249190-download`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-abrir visualização`, `4055249190-download`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4055249190-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-regerar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-detalhes`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-excluir`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-executar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-executar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-agendamentos`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-power-search-button`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-visualização`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-executar->3774328533-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-executar`, `3774328533-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-executar"]`);
    cy.clickIfExist(`[data-cy="3774328533-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-executar->3774328533-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-executar`, `3774328533-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-executar"]`);
    cy.clickIfExist(`[data-cy="3774328533-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-agendamentos->3774328533-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-agendamentos`, `3774328533-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456159D%7C%7C47456159&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-dados-contabeis->3774328533-visualização->3774328533-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-dados-contabeis`, `3774328533-visualização`, `3774328533-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3774328533-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3774328533-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-executar->578205992-múltipla seleção->578205992-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-executar`, `578205992-múltipla seleção`, `578205992-cancelar`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-executar"]`);
    cy.clickIfExist(`[data-cy="578205992-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="578205992-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaRegistrosDeOpen->578205992-abrir visualização->578205992-expandir->578205992-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaRegistrosDeOpen`, `578205992-abrir visualização`, `578205992-expandir`, `578205992-diminuir`];
    cy.visit('/http://system-A11/relatorios/listaRegistrosDeOpen?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="578205992-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="578205992-expandir"]`);
    cy.clickIfExist(`[data-cy="578205992-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/listaDofsExistentesBase->459120590-executar->459120590-múltipla seleção->459120590-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/listaDofsExistentesBase`, `459120590-executar`, `459120590-múltipla seleção`, `459120590-cancelar`];
    cy.visit('/http://system-A11/relatorios/listaDofsExistentesBase?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="459120590-executar"]`);
    cy.clickIfExist(`[data-cy="459120590-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="459120590-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-executar->1466872463-múltipla seleção->1466872463-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-executar`, `1466872463-múltipla seleção`, `1466872463-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-executar"]`);
    cy.clickIfExist(`[data-cy="1466872463-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1466872463-este parâmetro possui mais de 100 valores. clique aqui para carregar todos."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-executar->1466872463-múltipla seleção->1466872463-próximo`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-executar`, `1466872463-múltipla seleção`, `1466872463-próximo`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-executar"]`);
    cy.clickIfExist(`[data-cy="1466872463-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1466872463-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-executar->1466872463-múltipla seleção->1466872463-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-executar`, `1466872463-múltipla seleção`, `1466872463-cancelar`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-executar"]`);
    cy.clickIfExist(`[data-cy="1466872463-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1466872463-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/documentacaoOpenInterfaces->1466872463-abrir visualização->1466872463-expandir->1466872463-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/documentacaoOpenInterfaces`, `1466872463-abrir visualização`, `1466872463-expandir`, `1466872463-diminuir`];
    cy.visit('/http://system-A11/relatorios/documentacaoOpenInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466872463-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1466872463-expandir"]`);
    cy.clickIfExist(`[data-cy="1466872463-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-executar->1676676410-múltipla seleção->1676676410-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-executar`, `1676676410-múltipla seleção`, `1676676410-cancelar`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-executar"]`);
    cy.clickIfExist(`[data-cy="1676676410-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1676676410-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/testesInterfaces->1676676410-abrir visualização->1676676410-expandir->1676676410-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/testesInterfaces`, `1676676410-abrir visualização`, `1676676410-expandir`, `1676676410-diminuir`];
    cy.visit('/http://system-A11/relatorios/testesInterfaces?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1676676410-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1676676410-expandir"]`);
    cy.clickIfExist(`[data-cy="1676676410-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-executar->1808761608-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-executar`, `1808761608-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-executar"]`);
    cy.clickIfExist(`[data-cy="1808761608-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-executar->1808761608-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-executar`, `1808761608-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-executar"]`);
    cy.clickIfExist(`[data-cy="1808761608-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-executar->1808761608-input-P_Hora_Inicial-1808761608-input-P_Hora_Final and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-executar`, `1808761608-input-P_Hora_Inicial-1808761608-input-P_Hora_Final`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-executar"]`);
    cy.fillInput(`[data-cy="1808761608-input-P_Hora_Inicial"] textarea`, `Metal`);
    cy.fillInput(`[data-cy="1808761608-input-P_Hora_Final"] textarea`, `solutions`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-agendamentos->1808761608-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-agendamentos`, `1808761608-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784718D%7C%7C53784718&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-visualização->1808761608-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-visualização`, `1808761608-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1808761608-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-detalhes->1808761608-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-detalhes`, `1808761608-não há dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-detalhes"]`);
    cy.clickIfExist(`[data-cy="1808761608-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-executar->2096399756-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-executar`, `2096399756-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-executar"]`);
    cy.clickIfExist(`[data-cy="2096399756-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-executar->2096399756-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-executar`, `2096399756-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-executar"]`);
    cy.clickIfExist(`[data-cy="2096399756-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-agendamentos->2096399756-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-agendamentos`, `2096399756-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143342D%7C%7C19143342&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-visualização->2096399756-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-visualização`, `2096399756-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2096399756-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-detalhes->2096399756-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-detalhes`, `2096399756-não há dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-detalhes"]`);
    cy.clickIfExist(`[data-cy="2096399756-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-executar->1359902950-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-executar`, `1359902950-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-executar"]`);
    cy.clickIfExist(`[data-cy="1359902950-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-agendamentos->1359902950-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-agendamentos`, `1359902950-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784761D%7C%7C53784761&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-visualização->1359902950-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-visualização`, `1359902950-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1359902950-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-detalhes->1359902950-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-detalhes`, `1359902950-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-detalhes"]`);
    cy.clickIfExist(`[data-cy="1359902950-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-abrir visualização->1359902950-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-abrir visualização`, `1359902950-aumentar o zoom`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1359902950-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-abrir visualização->1359902950-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-abrir visualização`, `1359902950-diminuir o zoom`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1359902950-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-abrir visualização->1359902950-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-abrir visualização`, `1359902950-expandir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1359902950-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-abrir visualização->1359902950-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-abrir visualização`, `1359902950-download`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1359902950-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-executar->2057874332-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-executar`, `2057874332-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-executar"]`);
    cy.clickIfExist(`[data-cy="2057874332-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-executar->2057874332-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-executar`, `2057874332-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-executar"]`);
    cy.clickIfExist(`[data-cy="2057874332-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-agendamentos->2057874332-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-agendamentos`, `2057874332-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784962D%7C%7C53784962&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-visualização->2057874332-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-visualização`, `2057874332-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2057874332-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-detalhes->2057874332-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-detalhes`, `2057874332-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-detalhes"]`);
    cy.clickIfExist(`[data-cy="2057874332-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-abrir visualização->2057874332-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-abrir visualização`, `2057874332-aumentar o zoom`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2057874332-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-abrir visualização->2057874332-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-abrir visualização`, `2057874332-diminuir o zoom`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2057874332-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-abrir visualização->2057874332-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-abrir visualização`, `2057874332-expandir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2057874332-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-abrir visualização->2057874332-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-abrir visualização`, `2057874332-download`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2057874332-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-executar->3213345668-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-executar`, `3213345668-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-executar"]`);
    cy.clickIfExist(`[data-cy="3213345668-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-executar->3213345668-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-executar`, `3213345668-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-executar"]`);
    cy.clickIfExist(`[data-cy="3213345668-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-agendamentos->3213345668-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-agendamentos`, `3213345668-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784762D%7C%7C53784762&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-visualização->3213345668-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-visualização`, `3213345668-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3213345668-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-executar->200605263-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-executar`, `200605263-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-executar"]`);
    cy.clickIfExist(`[data-cy="200605263-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-executar->200605263-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-executar`, `200605263-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-executar"]`);
    cy.clickIfExist(`[data-cy="200605263-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-agendamentos->200605263-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-agendamentos`, `200605263-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784764D%7C%7C53784764&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-visualização->200605263-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-visualização`, `200605263-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="200605263-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-detalhes->200605263-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-detalhes`, `200605263-não há dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-detalhes"]`);
    cy.clickIfExist(`[data-cy="200605263-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-executar->1237903472-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-executar`, `1237903472-múltipla seleção`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-executar"]`);
    cy.clickIfExist(`[data-cy="1237903472-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-executar->1237903472-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-executar`, `1237903472-agendar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-executar"]`);
    cy.clickIfExist(`[data-cy="1237903472-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-agendamentos->1237903472-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-agendamentos`, `1237903472-voltar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53784998D%7C%7C53784998&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-visualização->1237903472-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-visualização`, `1237903472-item-`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1237903472-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-detalhes->1237903472-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-detalhes`, `1237903472-não há dados disponíveis para impressão`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-detalhes"]`);
    cy.clickIfExist(`[data-cy="1237903472-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-gerenciar labels->2428971009-fechar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-gerenciar labels`, `2428971009-fechar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2428971009-gerenciar labels"]`);
    cy.clickIfExist(`[data-cy="2428971009-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-visualizar/editar->2390483742-salvar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-visualizar/editar`, `2390483742-salvar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390483742-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/propriedades->2428971009-visualizar/editar->2390483742-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/propriedades`, `2428971009-visualizar/editar`, `2390483742-voltar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/propriedades/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390483742-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ir para todas as obrigações->282198900-voltar às obrigações do módulo`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ir para todas as obrigações`, `282198900-voltar às obrigações do módulo`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-voltar às obrigações do módulo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ir para todas as obrigações->282198900-nova solicitação`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ir para todas as obrigações`, `282198900-nova solicitação`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-nova solicitação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ir para todas as obrigações->282198900-agendamentos`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ir para todas as obrigações`, `282198900-agendamentos`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ir para todas as obrigações->282198900-atualizar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ir para todas as obrigações`, `282198900-atualizar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-visualização->2504686342-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-visualização`, `2504686342-item-`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2504686342-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-abrir visualização->2504686342-aumentar o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-abrir visualização`, `2504686342-aumentar o zoom`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2504686342-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-abrir visualização->2504686342-diminuir o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-abrir visualização`, `2504686342-diminuir o zoom`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2504686342-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-abrir visualização->2504686342-expandir`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-abrir visualização`, `2504686342-expandir`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2504686342-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-abrir visualização->2504686342-download`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-abrir visualização`, `2504686342-download`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2504686342-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/consultar->2504686342-visualizar->2504686342-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/consultar`, `2504686342-visualizar`, `2504686342-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/consultar?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2504686342-visualizar"]`);
    cy.clickIfExist(`[data-cy="2504686342-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-novo->2181344213-criar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-novo`, `2181344213-criar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-novo"]`);
    cy.clickIfExist(`[data-cy="2181344213-criar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-novo->2181344213-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-novo`, `2181344213-cancelar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-novo"]`);
    cy.clickIfExist(`[data-cy="2181344213-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-novo->2181344213-input-number-ano and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-novo`, `2181344213-input-number-ano`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-novo"]`);
    cy.fillInput(`[data-cy="2181344213-input-number-ano"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-editar->2181344213-remover item`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-editar`, `2181344213-remover item`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-editar"]`);
    cy.clickIfExist(`[data-cy="2181344213-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/periodicidade->2181344213-editar->2181344213-salvar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/periodicidade`, `2181344213-editar`, `2181344213-salvar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181344213-editar"]`);
    cy.clickIfExist(`[data-cy="2181344213-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/por-estabelecimento->2977090664-novo->2977090664-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/por-estabelecimento`, `2977090664-novo`, `2977090664-cancelar`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/por-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2977090664-novo"]`);
    cy.clickIfExist(`[data-cy="2977090664-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-executar->140334162-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-executar`, `140334162-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-executar"]`);
    cy.clickIfExist(`[data-cy="140334162-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-executar->140334162-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-executar`, `140334162-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-executar"]`);
    cy.clickIfExist(`[data-cy="140334162-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-executar->140334162-input-MERC and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-executar`, `140334162-input-MERC`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-executar"]`);
    cy.fillInput(`[data-cy="140334162-input-MERC"] textarea`, `Plastic`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-agendamentos->140334162-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-agendamentos`, `140334162-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15931467D%7C%7C15931467&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-visualização->140334162-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-visualização`, `140334162-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="140334162-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-executar->140325421-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-executar`, `140325421-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-executar"]`);
    cy.clickIfExist(`[data-cy="140325421-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-executar->140325421-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-executar`, `140325421-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-executar"]`);
    cy.clickIfExist(`[data-cy="140325421-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-agendamentos->140325421-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-agendamentos`, `140325421-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19109687D%7C%7C19109687&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-visualização->140325421-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-visualização`, `140325421-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="140325421-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-executar->2224687352-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-executar`, `2224687352-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-executar"]`);
    cy.clickIfExist(`[data-cy="2224687352-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-executar->2224687352-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-executar`, `2224687352-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-executar"]`);
    cy.clickIfExist(`[data-cy="2224687352-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-agendamentos->2224687352-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-agendamentos`, `2224687352-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19114072D%7C%7C19114072&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-visualização->2224687352-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-visualização`, `2224687352-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2224687352-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-executar->172569653-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-executar`, `172569653-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-executar"]`);
    cy.clickIfExist(`[data-cy="172569653-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-executar->172569653-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-executar`, `172569653-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-executar"]`);
    cy.clickIfExist(`[data-cy="172569653-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-agendamentos->172569653-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-agendamentos`, `172569653-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~20059071D%7C%7C20059071&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-visualização->172569653-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-visualização`, `172569653-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="172569653-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-executar->2126595364-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-executar`, `2126595364-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-executar"]`);
    cy.clickIfExist(`[data-cy="2126595364-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-executar->2126595364-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-executar`, `2126595364-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-executar"]`);
    cy.clickIfExist(`[data-cy="2126595364-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-agendamentos->2126595364-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-agendamentos`, `2126595364-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31795855D%7C%7C31795855&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-visualização->2126595364-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-visualização`, `2126595364-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2126595364-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-detalhes->2126595364-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-detalhes`, `2126595364-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-detalhes"]`);
    cy.clickIfExist(`[data-cy="2126595364-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-abrir visualização->2126595364-aumentar o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-abrir visualização`, `2126595364-aumentar o zoom`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2126595364-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-abrir visualização->2126595364-diminuir o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-abrir visualização`, `2126595364-diminuir o zoom`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2126595364-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-abrir visualização->2126595364-expandir`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-abrir visualização`, `2126595364-expandir`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2126595364-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-abrir visualização->2126595364-download`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-abrir visualização`, `2126595364-download`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2126595364-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-executar->1374054689-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-executar`, `1374054689-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-executar"]`);
    cy.clickIfExist(`[data-cy="1374054689-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-executar->1374054689-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-executar`, `1374054689-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-executar"]`);
    cy.clickIfExist(`[data-cy="1374054689-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-executar->1374054689-input-P_DIR_EXCEL and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-executar`, `1374054689-input-P_DIR_EXCEL`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-executar"]`);
    cy.fillInput(`[data-cy="1374054689-input-P_DIR_EXCEL"] textarea`, `synthesizing`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-agendamentos->1374054689-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-agendamentos`, `1374054689-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33923506D%7C%7C33923506&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-visualização->1374054689-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-visualização`, `1374054689-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1374054689-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-executar->1725052440-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-executar`, `1725052440-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-executar"]`);
    cy.clickIfExist(`[data-cy="1725052440-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-executar->1725052440-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-executar`, `1725052440-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-executar"]`);
    cy.clickIfExist(`[data-cy="1725052440-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-executar->1725052440-input-P_ID_LOTE and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-executar`, `1725052440-input-P_ID_LOTE`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-executar"]`);
    cy.fillInput(`[data-cy="1725052440-input-P_ID_LOTE"] textarea`, `alarm`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-agendamentos->1725052440-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-agendamentos`, `1725052440-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54222872D%7C%7C54222872&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-visualização->1725052440-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-visualização`, `1725052440-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1725052440-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-executar->3043091304-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-executar`, `3043091304-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-executar"]`);
    cy.clickIfExist(`[data-cy="3043091304-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-executar->3043091304-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-executar`, `3043091304-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-executar"]`);
    cy.clickIfExist(`[data-cy="3043091304-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-agendamentos->3043091304-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-agendamentos`, `3043091304-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56333929D%7C%7C56333929&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-visualização->3043091304-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-visualização`, `3043091304-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3043091304-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-executar->2463665264-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-executar`, `2463665264-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-executar"]`);
    cy.clickIfExist(`[data-cy="2463665264-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-executar->2463665264-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-executar`, `2463665264-agendar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-executar"]`);
    cy.clickIfExist(`[data-cy="2463665264-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-agendamentos->2463665264-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-agendamentos`, `2463665264-voltar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~56333925D%7C%7C56333925&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-visualização->2463665264-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-visualização`, `2463665264-item-`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2463665264-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-detalhes->2463665264-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-detalhes`, `2463665264-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-detalhes"]`);
    cy.clickIfExist(`[data-cy="2463665264-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-abrir visualização->2463665264-aumentar o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-abrir visualização`, `2463665264-aumentar o zoom`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2463665264-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-abrir visualização->2463665264-diminuir o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-abrir visualização`, `2463665264-diminuir o zoom`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2463665264-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-abrir visualização->2463665264-expandir`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-abrir visualização`, `2463665264-expandir`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2463665264-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-abrir visualização->2463665264-download`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-abrir visualização`, `2463665264-download`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2463665264-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-executar->414222541-múltipla seleção`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-executar`, `414222541-múltipla seleção`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-executar"]`);
    cy.clickIfExist(`[data-cy="414222541-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-executar->414222541-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-executar`, `414222541-agendar`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-executar"]`);
    cy.clickIfExist(`[data-cy="414222541-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-agendamentos->414222541-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-agendamentos`, `414222541-voltar`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~48767671D%7C%7C48767671&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-visualização->414222541-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-visualização`, `414222541-item-`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="414222541-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-detalhes->414222541-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-detalhes`, `414222541-dados disponíveis para impressão`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-detalhes"]`);
    cy.clickIfExist(`[data-cy="414222541-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-abrir visualização->414222541-aumentar o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-abrir visualização`, `414222541-aumentar o zoom`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="414222541-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-abrir visualização->414222541-diminuir o zoom`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-abrir visualização`, `414222541-diminuir o zoom`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="414222541-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-abrir visualização->414222541-expandir`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-abrir visualização`, `414222541-expandir`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="414222541-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-abrir visualização->414222541-download`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-abrir visualização`, `414222541-download`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="414222541-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-executar->2390360502-agendar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-executar`, `2390360502-agendar`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-executar"]`);
    cy.clickIfExist(`[data-cy="2390360502-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-agendamentos->2390360502-voltar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-agendamentos`, `2390360502-voltar`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15931482D%7C%7C15931482&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sfw->sfw/processos->sfw/processos/carga-estabelecimentos->2390360502-visualização->2390360502-item- and submit`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/carga-estabelecimentos`, `2390360502-visualização`, `2390360502-item-`];
    cy.visit('/http://system-A11/sfw/processos/carga-estabelecimentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2390360502-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2390360502-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-selectoutlined->1200682075-mais operações->1200682075-item-campos->1200682075-ok`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-selectoutlined`, `1200682075-mais operações`, `1200682075-item-campos`, `1200682075-ok`];
    cy.visit('/http://system-A11/dic-visao/145300/info');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1200682075-mais operações"]`);
    cy.clickIfExist(`[data-cy="1200682075-item-campos"]`);
    cy.clickIfExist(`[data-cy="1200682075-ok"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-mais operações->3958387426-item-->3958387426-não`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-mais operações`, `3958387426-item-`, `3958387426-não`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-mais operações"]`);
    cy.clickIfExist(`[data-cy="3958387426-item-"]`);
    cy.clickIfExist(`[data-cy="3958387426-não"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element dicionario->dicionario/visoes->1680412750-eyeoutlined->3958387426-mais operações->3958387426-item-->3958387426-sim`, () => {
    const actualId = [`root`, `dicionario`, `dicionario/visoes`, `1680412750-eyeoutlined`, `3958387426-mais operações`, `3958387426-item-`, `3958387426-sim`];
    cy.visit('/http://system-A11/dic-visao/editar/145300');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3958387426-mais operações"]`);
    cy.clickIfExist(`[data-cy="3958387426-item-"]`);
    cy.clickIfExist(`[data-cy="3958387426-sim"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao->2299773694-visualização->2299773694-swapoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`, `2299773694-visualização`, `2299773694-swapoutlined`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="2299773694-visualização"]`);
    cy.clickIfExist(`[data-cy="2299773694-swapoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values intefaces->intefaces/manutencao->intefaces/manutencao/edicao->2299773694-powerselect-manutencao->2299773694-visualização->2299773694-item- and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/edicao`, `2299773694-powerselect-manutencao`, `2299773694-visualização`, `2299773694-item-`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/edicao"]`);
    cy.fillInputPowerSelect(`[data-cy="2299773694-powerselect-manutencao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="2299773694-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2299773694-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-powerselect-exportacao->3235832168-visualização->3235832168-swapoutlined`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-powerselect-exportacao`, `3235832168-visualização`, `3235832168-swapoutlined`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.fillInputPowerSelect(`[data-cy="3235832168-powerselect-exportacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3235832168-visualização"]`);
    cy.clickIfExist(`[data-cy="3235832168-swapoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values intefaces->intefaces/manutencao->intefaces/manutencao/exportacao->3235832168-powerselect-exportacao->3235832168-visualização->3235832168-item- and submit`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/manutencao`, `intefaces/manutencao/exportacao`, `3235832168-powerselect-exportacao`, `3235832168-visualização`, `3235832168-item-`];
    cy.clickIfExist(`[data-cy="intefaces"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao"]`);
    cy.clickIfExist(`[data-cy="intefaces/manutencao/exportacao"]`);
    cy.fillInputPowerSelect(`[data-cy="3235832168-powerselect-exportacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3235832168-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3235832168-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element intefaces->intefaces/grupo->233875364-eyeoutlined->2382481868-ordenar grupos de interface->2382481868-button->2382481868-item-`, () => {
    const actualId = [`root`, `intefaces`, `intefaces/grupo`, `233875364-eyeoutlined`, `2382481868-ordenar grupos de interface`, `2382481868-button`, `2382481868-item-`];
    cy.visit('/http://system-A11/grupo-interface/editar/11');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2382481868-ordenar grupos de interface"]`);
    cy.clickIfExist(`[data-cy="2382481868-button"]`);
    cy.clickIfExist(`[data-cy="2382481868-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-executar->4055249190-múltipla seleção->4055249190-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-executar`, `4055249190-múltipla seleção`, `4055249190-cancelar`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-executar"]`);
    cy.clickIfExist(`[data-cy="4055249190-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4055249190-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/sap->processos/sap/limpeza-log-sap-x-synchro->4055249190-abrir visualização->4055249190-expandir->4055249190-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/sap`, `processos/sap/limpeza-log-sap-x-synchro`, `4055249190-abrir visualização`, `4055249190-expandir`, `4055249190-diminuir`];
    cy.visit('/http://system-A11/processos/sap/limpeza-log-sap-x-synchro?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4055249190-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4055249190-expandir"]`);
    cy.clickIfExist(`[data-cy="4055249190-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-executar->3724044816-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-executar`, `3724044816-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-executar"]`);
    cy.clickIfExist(`[data-cy="3724044816-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-agendamentos->3724044816-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-agendamentos`, `3724044816-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456196D%7C%7C47456196&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros->3724044816-visualização->3724044816-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros`, `3724044816-visualização`, `3724044816-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-inicial-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3724044816-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3724044816-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-executar->4087847603-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-executar`, `4087847603-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-executar"]`);
    cy.clickIfExist(`[data-cy="4087847603-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-agendamentos->4087847603-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-agendamentos`, `4087847603-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456218D%7C%7C47456218&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-inicial->processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority->4087847603-visualização->4087847603-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-inicial`, `processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority`, `4087847603-visualização`, `4087847603-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-inicial/carga-de-tax-authority?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4087847603-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4087847603-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-executar->2909235083-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-executar`, `2909235083-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-executar"]`);
    cy.clickIfExist(`[data-cy="2909235083-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-executar->2909235083-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-executar`, `2909235083-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-executar"]`);
    cy.clickIfExist(`[data-cy="2909235083-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-executar->2909235083-input-P_NUMERO and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-executar`, `2909235083-input-P_NUMERO`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-executar"]`);
    cy.fillInput(`[data-cy="2909235083-input-P_NUMERO"] textarea`, `Computer`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-agendamentos->2909235083-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-agendamentos`, `2909235083-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456197D%7C%7C47456197&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-visualização->2909235083-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-visualização`, `2909235083-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2909235083-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-detalhes->2909235083-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-detalhes`, `2909235083-não há dados disponíveis para impressão`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-detalhes"]`);
    cy.clickIfExist(`[data-cy="2909235083-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-executar->2577835410-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-executar`, `2577835410-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-executar"]`);
    cy.clickIfExist(`[data-cy="2577835410-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-executar->2577835410-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-executar`, `2577835410-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-executar"]`);
    cy.clickIfExist(`[data-cy="2577835410-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-executar->2577835410-input-P_NUMERO and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-executar`, `2577835410-input-P_NUMERO`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-executar"]`);
    cy.fillInput(`[data-cy="2577835410-input-P_NUMERO"] textarea`, `Awesome Steel Ball`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-agendamentos->2577835410-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-agendamentos`, `2577835410-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456175D%7C%7C47456175&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-visualização->2577835410-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-visualização`, `2577835410-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2577835410-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-executar->1466791461-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-executar`, `1466791461-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-executar"]`);
    cy.clickIfExist(`[data-cy="1466791461-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-executar->1466791461-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-executar`, `1466791461-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-executar"]`);
    cy.clickIfExist(`[data-cy="1466791461-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-agendamentos->1466791461-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-agendamentos`, `1466791461-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456249D%7C%7C47456249&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-visualização->1466791461-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-visualização`, `1466791461-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1466791461-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-executar->3811599192-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-executar`, `3811599192-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-executar"]`);
    cy.clickIfExist(`[data-cy="3811599192-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-executar->3811599192-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-executar`, `3811599192-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-executar"]`);
    cy.clickIfExist(`[data-cy="3811599192-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-executar->3811599192-input-P_NUMERO_GRI and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-executar`, `3811599192-input-P_NUMERO_GRI`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-executar"]`);
    cy.fillInput(`[data-cy="3811599192-input-P_NUMERO_GRI"] textarea`, `Gloves`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-agendamentos->3811599192-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-agendamentos`, `3811599192-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456208D%7C%7C47456208&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-visualização->3811599192-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-visualização`, `3811599192-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3811599192-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-executar->3121846514-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-executar`, `3121846514-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-executar"]`);
    cy.clickIfExist(`[data-cy="3121846514-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-executar->3121846514-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-executar`, `3121846514-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-executar"]`);
    cy.clickIfExist(`[data-cy="3121846514-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-executar->3121846514-input-P_DIR_EXCEL and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-executar`, `3121846514-input-P_DIR_EXCEL`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-executar"]`);
    cy.fillInput(`[data-cy="3121846514-input-P_DIR_EXCEL"] textarea`, `panel`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-agendamentos->3121846514-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-agendamentos`, `3121846514-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~50155256D%7C%7C50155256&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-visualização->3121846514-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-visualização`, `3121846514-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3121846514-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-executar->65776735-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-executar`, `65776735-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-executar"]`);
    cy.clickIfExist(`[data-cy="65776735-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-executar->65776735-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-executar`, `65776735-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-executar"]`);
    cy.clickIfExist(`[data-cy="65776735-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-agendamentos->65776735-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-agendamentos`, `65776735-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456183D%7C%7C47456183&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-visualização->65776735-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-visualização`, `65776735-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="65776735-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-executar->277761785-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-executar`, `277761785-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-executar"]`);
    cy.clickIfExist(`[data-cy="277761785-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-executar->277761785-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-executar`, `277761785-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-executar"]`);
    cy.clickIfExist(`[data-cy="277761785-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-executar->277761785-input-pNotaFiscal and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-executar`, `277761785-input-pNotaFiscal`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-executar"]`);
    cy.fillInput(`[data-cy="277761785-input-pNotaFiscal"] textarea`, `Small Metal Keyboard`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-agendamentos->277761785-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-agendamentos`, `277761785-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456154D%7C%7C47456154&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-visualização->277761785-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-visualização`, `277761785-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="277761785-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-executar->1126770727-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-executar`, `1126770727-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-executar"]`);
    cy.clickIfExist(`[data-cy="1126770727-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-executar->1126770727-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-executar`, `1126770727-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-executar"]`);
    cy.clickIfExist(`[data-cy="1126770727-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-executar->1126770727-input-P_NUM_ATIVO and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-executar`, `1126770727-input-P_NUM_ATIVO`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-executar"]`);
    cy.fillInput(`[data-cy="1126770727-input-P_NUM_ATIVO"] textarea`, `Grupo`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-agendamentos->1126770727-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-agendamentos`, `1126770727-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456229D%7C%7C47456229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw->1126770727-visualização->1126770727-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw`, `1126770727-visualização`, `1126770727-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f120-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126770727-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1126770727-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-executar->3383681734-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-executar`, `3383681734-múltipla seleção`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-executar"]`);
    cy.clickIfExist(`[data-cy="3383681734-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-executar->3383681734-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-executar`, `3383681734-agendar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-executar"]`);
    cy.clickIfExist(`[data-cy="3383681734-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-executar->3383681734-input-P_NUM_ATIVO and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-executar`, `3383681734-input-P_NUM_ATIVO`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-executar"]`);
    cy.fillInput(`[data-cy="3383681734-input-P_NUM_ATIVO"] textarea`, `frontend`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-agendamentos->3383681734-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-agendamentos`, `3383681734-voltar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47456169D%7C%7C47456169&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-visualização->3383681734-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-visualização`, `3383681734-item-`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3383681734-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-fiscais->1808761608-executar->1808761608-múltipla seleção->1808761608-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-fiscais`, `1808761608-executar`, `1808761608-múltipla seleção`, `1808761608-cancelar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1808761608-executar"]`);
    cy.clickIfExist(`[data-cy="1808761608-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1808761608-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-executar->2096399756-múltipla seleção->2096399756-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-executar`, `2096399756-múltipla seleção`, `2096399756-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-executar"]`);
    cy.clickIfExist(`[data-cy="2096399756-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2096399756-este parâmetro possui mais de 100 valores. clique aqui para carregar todos."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-executar->2096399756-múltipla seleção->2096399756-próximo`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-executar`, `2096399756-múltipla seleção`, `2096399756-próximo`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-executar"]`);
    cy.clickIfExist(`[data-cy="2096399756-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2096399756-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/carga-dados-contabeis->2096399756-executar->2096399756-múltipla seleção->2096399756-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/carga-dados-contabeis`, `2096399756-executar`, `2096399756-múltipla seleção`, `2096399756-cancelar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/carga-dados-contabeis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2096399756-executar"]`);
    cy.clickIfExist(`[data-cy="2096399756-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2096399756-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/dofs-rejeitados->1359902950-abrir visualização->1359902950-expandir->1359902950-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/dofs-rejeitados`, `1359902950-abrir visualização`, `1359902950-expandir`, `1359902950-diminuir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/dofs-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1359902950-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1359902950-expandir"]`);
    cy.clickIfExist(`[data-cy="1359902950-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-executar->2057874332-múltipla seleção->2057874332-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-executar`, `2057874332-múltipla seleção`, `2057874332-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-executar"]`);
    cy.clickIfExist(`[data-cy="2057874332-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2057874332-este parâmetro possui mais de 100 valores. clique aqui para carregar todos."]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-executar->2057874332-múltipla seleção->2057874332-próximo`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-executar`, `2057874332-múltipla seleção`, `2057874332-próximo`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-executar"]`);
    cy.clickIfExist(`[data-cy="2057874332-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2057874332-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-executar->2057874332-múltipla seleção->2057874332-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-executar`, `2057874332-múltipla seleção`, `2057874332-cancelar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-executar"]`);
    cy.clickIfExist(`[data-cy="2057874332-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2057874332-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/registros-rejeitados->2057874332-abrir visualização->2057874332-expandir->2057874332-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/registros-rejeitados`, `2057874332-abrir visualização`, `2057874332-expandir`, `2057874332-diminuir`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/registros-rejeitados?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2057874332-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2057874332-expandir"]`);
    cy.clickIfExist(`[data-cy="2057874332-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/valores-parametros->3213345668-executar->3213345668-múltipla seleção->3213345668-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/valores-parametros`, `3213345668-executar`, `3213345668-múltipla seleção`, `3213345668-cancelar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/valores-parametros?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3213345668-executar"]`);
    cy.clickIfExist(`[data-cy="3213345668-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3213345668-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/exec-cargas-solicitadas->200605263-executar->200605263-múltipla seleção->200605263-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/exec-cargas-solicitadas`, `200605263-executar`, `200605263-múltipla seleção`, `200605263-cancelar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/exec-cargas-solicitadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="200605263-executar"]`);
    cy.clickIfExist(`[data-cy="200605263-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="200605263-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/integracao-oracle-r12->relatorios/integracao-oracle-r12/processo-limpeza-inventario->1237903472-executar->1237903472-múltipla seleção->1237903472-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/integracao-oracle-r12`, `relatorios/integracao-oracle-r12/processo-limpeza-inventario`, `1237903472-executar`, `1237903472-múltipla seleção`, `1237903472-cancelar`];
    cy.visit('/http://system-A11/relatorios/integracao-oracle-r12/processo-limpeza-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1237903472-executar"]`);
    cy.clickIfExist(`[data-cy="1237903472-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1237903472-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/integracao-com-sfw->sfw/integracao-com-sfw/solicitacoes->282198900-ir para todas as obrigações->282198900-voltar às obrigações do módulo->282198900-ir para todas as obrigações`, () => {
    const actualId = [`root`, `sfw`, `sfw/integracao-com-sfw`, `sfw/integracao-com-sfw/solicitacoes`, `282198900-ir para todas as obrigações`, `282198900-voltar às obrigações do módulo`, `282198900-ir para todas as obrigações`];
    cy.visit('/http://system-A11/sfw/integracao-com-sfw/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="282198900-voltar às obrigações do módulo"]`);
    cy.clickIfExist(`[data-cy="282198900-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfe->140334162-executar->140334162-múltipla seleção->140334162-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfe`, `140334162-executar`, `140334162-múltipla seleção`, `140334162-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfe?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140334162-executar"]`);
    cy.clickIfExist(`[data-cy="140334162-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="140334162-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-ecf->140325421-executar->140325421-múltipla seleção->140325421-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-ecf`, `140325421-executar`, `140325421-múltipla seleção`, `140325421-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-ecf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="140325421-executar"]`);
    cy.clickIfExist(`[data-cy="140325421-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="140325421-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom->2224687352-executar->2224687352-múltipla seleção->2224687352-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom`, `2224687352-executar`, `2224687352-múltipla seleção`, `2224687352-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nfcom-telecom?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2224687352-executar"]`);
    cy.clickIfExist(`[data-cy="2224687352-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2224687352-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites->172569653-executar->172569653-múltipla seleção->172569653-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites`, `172569653-executar`, `172569653-múltipla seleção`, `172569653-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-consolidado-nf-utilites?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="172569653-executar"]`);
    cy.clickIfExist(`[data-cy="172569653-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="172569653-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-executar->2126595364-múltipla seleção->2126595364-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-executar`, `2126595364-múltipla seleção`, `2126595364-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-executar"]`);
    cy.clickIfExist(`[data-cy="2126595364-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2126595364-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins->2126595364-abrir visualização->2126595364-expandir->2126595364-diminuir`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins`, `2126595364-abrir visualização`, `2126595364-expandir`, `2126595364-diminuir`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/detalhamento-conferencia-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2126595364-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2126595364-expandir"]`);
    cy.clickIfExist(`[data-cy="2126595364-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/relatorio-pis-cofins->1374054689-executar->1374054689-múltipla seleção->1374054689-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/relatorio-pis-cofins`, `1374054689-executar`, `1374054689-múltipla seleção`, `1374054689-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/relatorio-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1374054689-executar"]`);
    cy.clickIfExist(`[data-cy="1374054689-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1374054689-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/contrib-social-retida-fonte->1725052440-executar->1725052440-múltipla seleção->1725052440-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/contrib-social-retida-fonte`, `1725052440-executar`, `1725052440-múltipla seleção`, `1725052440-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/contrib-social-retida-fonte?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1725052440-executar"]`);
    cy.clickIfExist(`[data-cy="1725052440-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1725052440-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-sintetico->3043091304-executar->3043091304-múltipla seleção->3043091304-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-sintetico`, `3043091304-executar`, `3043091304-múltipla seleção`, `3043091304-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-sintetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3043091304-executar"]`);
    cy.clickIfExist(`[data-cy="3043091304-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3043091304-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-executar->2463665264-múltipla seleção->2463665264-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-executar`, `2463665264-múltipla seleção`, `2463665264-cancelar`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-executar"]`);
    cy.clickIfExist(`[data-cy="2463665264-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2463665264-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/relatorios-de-apoio->sfw/relatorios-de-apoio/indice-financeiro-analitico->2463665264-abrir visualização->2463665264-expandir->2463665264-diminuir`, () => {
    const actualId = [`root`, `sfw`, `sfw/relatorios-de-apoio`, `sfw/relatorios-de-apoio/indice-financeiro-analitico`, `2463665264-abrir visualização`, `2463665264-expandir`, `2463665264-diminuir`];
    cy.visit('/http://system-A11/sfw/relatorios-de-apoio/indice-financeiro-analitico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463665264-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2463665264-expandir"]`);
    cy.clickIfExist(`[data-cy="2463665264-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-executar->414222541-múltipla seleção->414222541-cancelar`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-executar`, `414222541-múltipla seleção`, `414222541-cancelar`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-executar"]`);
    cy.clickIfExist(`[data-cy="414222541-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="414222541-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sfw->sfw/processos->sfw/processos/exclusao-icms-base-calculo-pis-cofins->414222541-abrir visualização->414222541-expandir->414222541-diminuir`, () => {
    const actualId = [`root`, `sfw`, `sfw/processos`, `sfw/processos/exclusao-icms-base-calculo-pis-cofins`, `414222541-abrir visualização`, `414222541-expandir`, `414222541-diminuir`];
    cy.visit('/http://system-A11/sfw/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="414222541-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="414222541-expandir"]`);
    cy.clickIfExist(`[data-cy="414222541-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais->2909235083-executar->2909235083-múltipla seleção->2909235083-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais`, `2909235083-executar`, `2909235083-múltipla seleção`, `2909235083-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-dados-fiscais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2909235083-executar"]`);
    cy.clickIfExist(`[data-cy="2909235083-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2909235083-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico->2577835410-executar->2577835410-múltipla seleção->2577835410-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico`, `2577835410-executar`, `2577835410-múltipla seleção`, `2577835410-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/emissao-nfse-arquivo-magnetico?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2577835410-executar"]`);
    cy.clickIfExist(`[data-cy="2577835410-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2577835410-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-fiscal->processos/oracle-erp-cloud/carga-fiscal/carga-programada->1466791461-executar->1466791461-múltipla seleção->1466791461-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-fiscal`, `processos/oracle-erp-cloud/carga-fiscal/carga-programada`, `1466791461-executar`, `1466791461-múltipla seleção`, `1466791461-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-fiscal/carga-programada?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1466791461-executar"]`);
    cy.clickIfExist(`[data-cy="1466791461-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1466791461-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento->3811599192-executar->3811599192-múltipla seleção->3811599192-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento`, `3811599192-executar`, `3811599192-múltipla seleção`, `3811599192-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/retorno-de-guia-de-pagamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3811599192-executar"]`);
    cy.clickIfExist(`[data-cy="3811599192-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3811599192-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias->3121846514-executar->3121846514-múltipla seleção->3121846514-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias`, `3121846514-executar`, `3121846514-múltipla seleção`, `3121846514-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/relatorio-retorno-guias?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3121846514-executar"]`);
    cy.clickIfExist(`[data-cy="3121846514-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3121846514-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/enviar-gnre->65776735-executar->65776735-múltipla seleção->65776735-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/enviar-gnre`, `65776735-executar`, `65776735-múltipla seleção`, `65776735-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/enviar-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65776735-executar"]`);
    cy.clickIfExist(`[data-cy="65776735-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="65776735-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/retorno-guia->processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre->277761785-executar->277761785-múltipla seleção->277761785-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/retorno-guia`, `processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre`, `277761785-executar`, `277761785-múltipla seleção`, `277761785-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/retorno-guia/reenvio-guia-gnre?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="277761785-executar"]`);
    cy.clickIfExist(`[data-cy="277761785-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="277761785-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element processos->processos/oracle-erp-cloud->processos/oracle-erp-cloud/carga-integra->processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw->3383681734-executar->3383681734-múltipla seleção->3383681734-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/oracle-erp-cloud`, `processos/oracle-erp-cloud/carga-integra`, `processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw`, `3383681734-executar`, `3383681734-múltipla seleção`, `3383681734-cancelar`];
    cy.visit('/http://system-A11/processos/oracle-erp-cloud/carga-integra/carga-de-reg-integrar-registro-f130-com-efd-contrib-sfw?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3383681734-executar"]`);
    cy.clickIfExist(`[data-cy="3383681734-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3383681734-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
