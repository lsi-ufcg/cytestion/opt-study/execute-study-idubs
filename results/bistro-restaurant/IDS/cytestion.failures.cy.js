describe('Cytestion', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Click on element index-about->about-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-about"]`);
    cy.clickCauseExist(`[data-cy="about-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-services->services-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-services"]`);
    cy.clickCauseExist(`[data-cy="services-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-pricing->pricing-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-pricing"]`);
    cy.clickCauseExist(`[data-cy="pricing-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-about->about-home->index-WebThemez`, () => {
    cy.clickCauseExist(`[data-cy="index-about"]`);
    cy.clickCauseExist(`[data-cy="about-home"]`);
    cy.clickCauseExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
});
