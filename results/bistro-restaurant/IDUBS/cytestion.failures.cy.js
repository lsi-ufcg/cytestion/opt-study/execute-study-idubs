describe('Cytestion', () => {
  beforeEach(() => {
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Click on element index-about->about-WebThemez`, () => {
    cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="about-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-services->services-WebThemez`, () => {
    cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="services-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-pricing->pricing-WebThemez`, () => {
    cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="pricing-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element index-about->about-home->index-WebThemez`, () => {
    cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
  });
});
