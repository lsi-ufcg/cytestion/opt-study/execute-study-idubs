describe('Cytestion', () => {
  beforeEach(() => {
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-navbar-toggle`, () => {
const actualId = [`root`,`index-navbar-toggle`];
    cy.clickIfExist(`[data-cy="index-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-navbar-brand`, () => {
const actualId = [`root`,`index-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-index`, () => {
const actualId = [`root`,`index-index`];
    cy.clickIfExist(`[data-cy="index-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about`, () => {
const actualId = [`root`,`index-about`];
    cy.clickIfExist(`[data-cy="index-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services`, () => {
const actualId = [`root`,`index-services`];
    cy.clickIfExist(`[data-cy="index-services"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-gallery`, () => {
const actualId = [`root`,`index-gallery`];
    cy.clickIfExist(`[data-cy="index-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing`, () => {
const actualId = [`root`,`index-pricing`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact`, () => {
const actualId = [`root`,`index-contact`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-img-1`, () => {
const actualId = [`root`,`index-img-1`];
    cy.clickIfExist(`[data-cy="index-img-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-img-2`, () => {
const actualId = [`root`,`index-img-2`];
    cy.clickIfExist(`[data-cy="index-img-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-img-3`, () => {
const actualId = [`root`,`index-img-3`];
    cy.clickIfExist(`[data-cy="index-img-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-events`, () => {
const actualId = [`root`,`index-events`];
    cy.clickIfExist(`[data-cy="index-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-terms`, () => {
const actualId = [`root`,`index-terms`];
    cy.clickIfExist(`[data-cy="index-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-privacy`, () => {
const actualId = [`root`,`index-privacy`];
    cy.clickIfExist(`[data-cy="index-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-career`, () => {
const actualId = [`root`,`index-career`];
    cy.clickIfExist(`[data-cy="index-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact-us`, () => {
const actualId = [`root`,`index-contact-us`];
    cy.clickIfExist(`[data-cy="index-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-1`, () => {
const actualId = [`root`,`index-text-1`];
    cy.clickIfExist(`[data-cy="index-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-2`, () => {
const actualId = [`root`,`index-text-2`];
    cy.clickIfExist(`[data-cy="index-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-3`, () => {
const actualId = [`root`,`index-text-3`];
    cy.clickIfExist(`[data-cy="index-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-4`, () => {
const actualId = [`root`,`index-text-4`];
    cy.clickIfExist(`[data-cy="index-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-5`, () => {
const actualId = [`root`,`index-text-5`];
    cy.clickIfExist(`[data-cy="index-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-6`, () => {
const actualId = [`root`,`index-text-6`];
    cy.clickIfExist(`[data-cy="index-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-WebThemez`, () => {
const actualId = [`root`,`index-WebThemez`];
    cy.clickIfExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-facebook`, () => {
const actualId = [`root`,`index-facebook`];
    cy.clickIfExist(`[data-cy="index-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-twitter`, () => {
const actualId = [`root`,`index-twitter`];
    cy.clickIfExist(`[data-cy="index-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-linkedin`, () => {
const actualId = [`root`,`index-linkedin`];
    cy.clickIfExist(`[data-cy="index-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pinterest`, () => {
const actualId = [`root`,`index-pinterest`];
    cy.clickIfExist(`[data-cy="index-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-google`, () => {
const actualId = [`root`,`index-google`];
    cy.clickIfExist(`[data-cy="index-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-angle-up`, () => {
const actualId = [`root`,`index-angle-up`];
    cy.clickIfExist(`[data-cy="index-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-navbar-collapse`, () => {
const actualId = [`root`,`index-about`,`about-navbar-collapse`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-navbar-collapse"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-navbar-brand`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-home`, () => {
const actualId = [`root`,`index-about`,`about-home`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-about`, () => {
const actualId = [`root`,`index-about`,`about-about`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-service`, () => {
const actualId = [`root`,`index-about`,`about-service`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-service"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-gallery`, () => {
const actualId = [`root`,`index-about`,`about-gallery`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-pricing`, () => {
const actualId = [`root`,`index-about`,`about-pricing`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact`, () => {
const actualId = [`root`,`index-about`,`about-contact`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-read-more`, () => {
const actualId = [`root`,`index-about`,`about-read-more`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-read-more"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-collapse-1`, () => {
const actualId = [`root`,`index-about`,`about-collapse-1`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-collapse-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact-2`, () => {
const actualId = [`root`,`index-about`,`about-contact-2`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact-3`, () => {
const actualId = [`root`,`index-about`,`about-contact-3`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact-4`, () => {
const actualId = [`root`,`index-about`,`about-contact-4`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-text-1`, () => {
const actualId = [`root`,`index-about`,`about-text-1`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-text-2`, () => {
const actualId = [`root`,`index-about`,`about-text-2`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-text-3`, () => {
const actualId = [`root`,`index-about`,`about-text-3`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-text-4`, () => {
const actualId = [`root`,`index-about`,`about-text-4`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-text-5`, () => {
const actualId = [`root`,`index-about`,`about-text-5`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-text-6`, () => {
const actualId = [`root`,`index-about`,`about-text-6`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-events`, () => {
const actualId = [`root`,`index-about`,`about-events`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-terms`, () => {
const actualId = [`root`,`index-about`,`about-terms`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-privacy`, () => {
const actualId = [`root`,`index-about`,`about-privacy`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-career`, () => {
const actualId = [`root`,`index-about`,`about-career`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-contact-us`, () => {
const actualId = [`root`,`index-about`,`about-contact-us`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-WebThemez`, () => {
const actualId = [`root`,`index-about`,`about-WebThemez`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-facebook`, () => {
const actualId = [`root`,`index-about`,`about-facebook`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-twitter`, () => {
const actualId = [`root`,`index-about`,`about-twitter`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-linkedin`, () => {
const actualId = [`root`,`index-about`,`about-linkedin`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-pinterest`, () => {
const actualId = [`root`,`index-about`,`about-pinterest`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-google`, () => {
const actualId = [`root`,`index-about`,`about-google`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-about->about-angle-up`, () => {
const actualId = [`root`,`index-about`,`about-angle-up`];
cy.visit('http://localhost:9999/about.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="about-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-navbar-toggle`, () => {
const actualId = [`root`,`index-services`,`services-navbar-toggle`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-navbar-brand`, () => {
const actualId = [`root`,`index-services`,`services-navbar-brand`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-index`, () => {
const actualId = [`root`,`index-services`,`services-index`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-about`, () => {
const actualId = [`root`,`index-services`,`services-about`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-services`, () => {
const actualId = [`root`,`index-services`,`services-services`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-services"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-gallery`, () => {
const actualId = [`root`,`index-services`,`services-gallery`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-pricing`, () => {
const actualId = [`root`,`index-services`,`services-pricing`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-contact`, () => {
const actualId = [`root`,`index-services`,`services-contact`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-events`, () => {
const actualId = [`root`,`index-services`,`services-events`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-terms`, () => {
const actualId = [`root`,`index-services`,`services-terms`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-privacy`, () => {
const actualId = [`root`,`index-services`,`services-privacy`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-career`, () => {
const actualId = [`root`,`index-services`,`services-career`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-contact-us`, () => {
const actualId = [`root`,`index-services`,`services-contact-us`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-text-1`, () => {
const actualId = [`root`,`index-services`,`services-text-1`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-text-2`, () => {
const actualId = [`root`,`index-services`,`services-text-2`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-text-3`, () => {
const actualId = [`root`,`index-services`,`services-text-3`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-text-4`, () => {
const actualId = [`root`,`index-services`,`services-text-4`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-text-5`, () => {
const actualId = [`root`,`index-services`,`services-text-5`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-text-6`, () => {
const actualId = [`root`,`index-services`,`services-text-6`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-WebThemez`, () => {
const actualId = [`root`,`index-services`,`services-WebThemez`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-facebook`, () => {
const actualId = [`root`,`index-services`,`services-facebook`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-twitter`, () => {
const actualId = [`root`,`index-services`,`services-twitter`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-linkedin`, () => {
const actualId = [`root`,`index-services`,`services-linkedin`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-pinterest`, () => {
const actualId = [`root`,`index-services`,`services-pinterest`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-google`, () => {
const actualId = [`root`,`index-services`,`services-google`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-services->services-angle-up`, () => {
const actualId = [`root`,`index-services`,`services-angle-up`];
cy.visit('http://localhost:9999/services.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="services-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-navbar-collapse`, () => {
const actualId = [`root`,`index-gallery`,`gallery-navbar-collapse`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-navbar-collapse"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-navbar-brand`, () => {
const actualId = [`root`,`index-gallery`,`gallery-navbar-brand`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-home`, () => {
const actualId = [`root`,`index-gallery`,`gallery-home`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-about`, () => {
const actualId = [`root`,`index-gallery`,`gallery-about`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-service`, () => {
const actualId = [`root`,`index-gallery`,`gallery-service`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-service"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-gallery`, () => {
const actualId = [`root`,`index-gallery`,`gallery-gallery`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-pricing`, () => {
const actualId = [`root`,`index-gallery`,`gallery-pricing`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-contact`, () => {
const actualId = [`root`,`index-gallery`,`gallery-contact`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-all`, () => {
const actualId = [`root`,`index-gallery`,`gallery-all`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-all"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-continental`, () => {
const actualId = [`root`,`index-gallery`,`gallery-continental`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-continental"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-asian-food`, () => {
const actualId = [`root`,`index-gallery`,`gallery-asian-food`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-asian-food"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-italian-food`, () => {
const actualId = [`root`,`index-gallery`,`gallery-italian-food`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-italian-food"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-1`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-1`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-2`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-2`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-3`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-3`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-4`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-4`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-5`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-5`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-6`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-6`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-7`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-7`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-7"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-works-8`, () => {
const actualId = [`root`,`index-gallery`,`gallery-works-8`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-works-8"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-events`, () => {
const actualId = [`root`,`index-gallery`,`gallery-events`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-terms`, () => {
const actualId = [`root`,`index-gallery`,`gallery-terms`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-privacy`, () => {
const actualId = [`root`,`index-gallery`,`gallery-privacy`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-career`, () => {
const actualId = [`root`,`index-gallery`,`gallery-career`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-contact-us`, () => {
const actualId = [`root`,`index-gallery`,`gallery-contact-us`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-text-1`, () => {
const actualId = [`root`,`index-gallery`,`gallery-text-1`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-text-2`, () => {
const actualId = [`root`,`index-gallery`,`gallery-text-2`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-text-3`, () => {
const actualId = [`root`,`index-gallery`,`gallery-text-3`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-text-4`, () => {
const actualId = [`root`,`index-gallery`,`gallery-text-4`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-text-5`, () => {
const actualId = [`root`,`index-gallery`,`gallery-text-5`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-text-6`, () => {
const actualId = [`root`,`index-gallery`,`gallery-text-6`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-WebThemez`, () => {
const actualId = [`root`,`index-gallery`,`gallery-WebThemez`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-facebook`, () => {
const actualId = [`root`,`index-gallery`,`gallery-facebook`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-twitter`, () => {
const actualId = [`root`,`index-gallery`,`gallery-twitter`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-linkedin`, () => {
const actualId = [`root`,`index-gallery`,`gallery-linkedin`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-pinterest`, () => {
const actualId = [`root`,`index-gallery`,`gallery-pinterest`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-google`, () => {
const actualId = [`root`,`index-gallery`,`gallery-google`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-gallery->gallery-angle-up`, () => {
const actualId = [`root`,`index-gallery`,`gallery-angle-up`];
cy.visit('http://localhost:9999/gallery.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="gallery-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-navbar-collapse`, () => {
const actualId = [`root`,`index-pricing`,`pricing-navbar-collapse`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-navbar-collapse"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-navbar-brand`, () => {
const actualId = [`root`,`index-pricing`,`pricing-navbar-brand`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-home`, () => {
const actualId = [`root`,`index-pricing`,`pricing-home`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-about`, () => {
const actualId = [`root`,`index-pricing`,`pricing-about`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-service`, () => {
const actualId = [`root`,`index-pricing`,`pricing-service`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-service"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-gallery`, () => {
const actualId = [`root`,`index-pricing`,`pricing-gallery`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-pricing`, () => {
const actualId = [`root`,`index-pricing`,`pricing-pricing`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-contact`, () => {
const actualId = [`root`,`index-pricing`,`pricing-contact`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-1`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-1`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-get-now-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-2`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-2`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-get-now-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-3`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-3`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-get-now-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-4`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-4`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-get-now-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-events`, () => {
const actualId = [`root`,`index-pricing`,`pricing-events`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-terms`, () => {
const actualId = [`root`,`index-pricing`,`pricing-terms`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-privacy`, () => {
const actualId = [`root`,`index-pricing`,`pricing-privacy`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-career`, () => {
const actualId = [`root`,`index-pricing`,`pricing-career`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-contact-us`, () => {
const actualId = [`root`,`index-pricing`,`pricing-contact-us`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-text-1`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-1`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-text-2`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-2`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-text-3`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-3`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-text-4`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-4`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-text-5`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-5`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-text-6`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-6`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-WebThemez`, () => {
const actualId = [`root`,`index-pricing`,`pricing-WebThemez`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-facebook`, () => {
const actualId = [`root`,`index-pricing`,`pricing-facebook`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-twitter`, () => {
const actualId = [`root`,`index-pricing`,`pricing-twitter`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-linkedin`, () => {
const actualId = [`root`,`index-pricing`,`pricing-linkedin`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-pinterest`, () => {
const actualId = [`root`,`index-pricing`,`pricing-pinterest`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-google`, () => {
const actualId = [`root`,`index-pricing`,`pricing-google`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-pricing->pricing-angle-up`, () => {
const actualId = [`root`,`index-pricing`,`pricing-angle-up`];
cy.visit('http://localhost:9999/pricing.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="pricing-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-navbar-collapse`, () => {
const actualId = [`root`,`index-contact`,`contact-navbar-collapse`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-navbar-collapse"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-navbar-brand`, () => {
const actualId = [`root`,`index-contact`,`contact-navbar-brand`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-home`, () => {
const actualId = [`root`,`index-contact`,`contact-home`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-about`, () => {
const actualId = [`root`,`index-contact`,`contact-about`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-services`, () => {
const actualId = [`root`,`index-contact`,`contact-services`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-services"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-gallery`, () => {
const actualId = [`root`,`index-contact`,`contact-gallery`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-pricing`, () => {
const actualId = [`root`,`index-contact`,`contact-pricing`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-contact`, () => {
const actualId = [`root`,`index-contact`,`contact-contact`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->close-button`, () => {
const actualId = [`root`,`index-contact`,`close-button`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="close-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-events`, () => {
const actualId = [`root`,`index-contact`,`contact-events`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-terms`, () => {
const actualId = [`root`,`index-contact`,`contact-terms`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-privacy`, () => {
const actualId = [`root`,`index-contact`,`contact-privacy`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-career`, () => {
const actualId = [`root`,`index-contact`,`contact-career`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-contact-us`, () => {
const actualId = [`root`,`index-contact`,`contact-contact-us`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-text-1`, () => {
const actualId = [`root`,`index-contact`,`contact-text-1`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-text-2`, () => {
const actualId = [`root`,`index-contact`,`contact-text-2`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-text-3`, () => {
const actualId = [`root`,`index-contact`,`contact-text-3`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-text-4`, () => {
const actualId = [`root`,`index-contact`,`contact-text-4`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-text-5`, () => {
const actualId = [`root`,`index-contact`,`contact-text-5`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-text-6`, () => {
const actualId = [`root`,`index-contact`,`contact-text-6`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-WebThemez`, () => {
const actualId = [`root`,`index-contact`,`contact-WebThemez`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-facebook`, () => {
const actualId = [`root`,`index-contact`,`contact-facebook`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-twitter`, () => {
const actualId = [`root`,`index-contact`,`contact-twitter`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-linkedin`, () => {
const actualId = [`root`,`index-contact`,`contact-linkedin`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-pinterest`, () => {
const actualId = [`root`,`index-contact`,`contact-pinterest`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-google`, () => {
const actualId = [`root`,`index-contact`,`contact-google`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element index-contact->contact-angle-up`, () => {
const actualId = [`root`,`index-contact`,`contact-angle-up`];
cy.visit('http://localhost:9999/contact.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="contact-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values index-contact->name-input-email-input-message-textarea-submit-input and submit`, () => {
const actualId = [`root`,`index-contact`,`name-input-email-input-message-textarea-submit-input`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
    cy.fillInput(`[data-cy="name-input"] textarea`, `transmit`);
cy.fillInput(`[data-cy="email-input"] textarea`, `Rio Grande do Sul`);
cy.fillInput(`[data-cy="message-textarea"] input`, `salmon`);
cy.fillInput(`[data-cy="submit-input"] textarea`, `Table`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-home->index-navbar-toggle`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-navbar-toggle`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-navbar-toggle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-navbar-brand`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-navbar-brand`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-navbar-brand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-index`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-index`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-index"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-about`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-about`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-about"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-services`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-services`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-services"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-gallery`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-gallery`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-gallery"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-pricing`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-pricing`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-pricing"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-contact`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-contact`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-contact"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-img-1`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-img-1`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-img-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-img-2`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-img-2`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-img-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-img-3`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-img-3`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-img-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-events`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-events`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-events"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-terms`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-terms`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-terms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-privacy`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-privacy`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-privacy"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-career`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-career`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-career"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-contact-us`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-contact-us`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-contact-us"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-text-1`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-text-1`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-text-2`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-text-2`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-2"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-text-3`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-text-3`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-3"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-text-4`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-text-4`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-4"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-text-5`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-text-5`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-5"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-text-6`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-text-6`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-text-6"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-WebThemez`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-WebThemez`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-WebThemez"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-facebook`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-facebook`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-facebook"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-twitter`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-twitter`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-twitter"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-linkedin`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-linkedin`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-linkedin"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-pinterest`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-pinterest`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-pinterest"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-google`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-google`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-google"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element index-about->about-home->index-angle-up`, () => {
const actualId = [`root`,`index-about`,`about-home`,`index-angle-up`];
cy.visit('http://localhost:9999/index.html');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="index-angle-up"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
