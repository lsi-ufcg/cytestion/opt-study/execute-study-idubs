describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element operacoes->operacoes/ativacao`, () => {
    cy.clickCauseExist(`[data-cy="operacoes"]`);
    cy.clickCauseExist(`[data-cy="operacoes/ativacao"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-visualizar/editar`, () => {
    cy.visit('http://system-A2/escrituracao-apuracao/parametrizacao-regras/lancamento-ajustes/itens/auxiliary');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2051661756-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-voltar`, () => {
    cy.visit('http://system-A2/processos/geracao-controles?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210783D%7C%7C210783&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1614615400-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar`, () => {
    cy.visit('http://system-A2/processos/geracao-controles?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210783D%7C%7C210783&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1614615400-visualizar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-excluir`, () => {
    cy.visit('http://system-A2/processos/geracao-controles?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210783D%7C%7C210783&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1614615400-excluir"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/exclusao-controles->287994784-agendamentos->287994784-voltar`, () => {
    cy.visit('http://system-A2/processos/exclusao-controles?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210786D%7C%7C210786&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="287994784-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-agendamentos->1629306443-voltar`, () => {
    cy.visit('http://system-A2/processos/exclusao-controle-dof-aquisicao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~242813D%7C%7C242813&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1629306443-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/totalizacao-saidas->633719453-agendamentos->633719453-voltar`, () => {
    cy.visit('http://system-A2/processos/totalizacao-saidas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210784D%7C%7C210784&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="633719453-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/exclusao-creditos->2814741288-agendamentos->2814741288-voltar`, () => {
    cy.visit('http://system-A2/processos/exclusao-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210787D%7C%7C210787&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2814741288-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-agendamentos->298565727-voltar`, () => {
    cy.visit('http://system-A2/processos/apropriacao-credito-extemporaneos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~16214247D%7C%7C16214247&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="298565727-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/atualizacao-indicador->2224043782-agendamentos->2224043782-voltar`, () => {
    cy.visit('http://system-A2/processos/atualizacao-indicador?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210788D%7C%7C210788&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2224043782-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/exclusao-op-saida->3183959863-agendamentos->3183959863-voltar`, () => {
    cy.visit('http://system-A2/processos/exclusao-op-saida?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27728126D%7C%7C27728126&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3183959863-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/suspencao-retomada-creditos->985668109-agendamentos->985668109-voltar`, () => {
    cy.visit('http://system-A2/processos/suspencao-retomada-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505601D%7C%7C46505601&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="985668109-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/transferencia-inc-societaria->1642007593-agendamentos->1642007593-voltar`, () => {
    cy.visit('http://system-A2/processos/transferencia-inc-societaria?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~20171692D%7C%7C20171692&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1642007593-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/controles-ciap->2680178634-agendamentos->2680178634-voltar`, () => {
    cy.visit('http://system-A2/relatorios/controles-ciap?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210774D%7C%7C210774&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2680178634-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-agendamentos->4033549032-voltar`, () => {
    cy.visit('http://system-A2/relatorios/conferenc-controles-incorp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~23650267D%7C%7C23650267&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4033549032-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-agendamentos->172111467-voltar`, () => {
    cy.visit('http://system-A2/relatorios/creditos-curto-longo-prazo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19490268D%7C%7C19490268&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="172111467-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-agendamentos->4272249838-voltar`, () => {
    cy.visit('http://system-A2/relatorios/creditos-parcelados-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~289818D%7C%7C289818&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4272249838-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-agendamentos->532328817-voltar`, () => {
    cy.visit('http://system-A2/relatorios/diferencas-arredondamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19272838D%7C%7C19272838&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="532328817-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-agendamentos->3190201375-voltar`, () => {
    cy.visit('http://system-A2/relatorios/operacoes-de-saida?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26924586D%7C%7C26924586&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3190201375-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-agendamentos->1912915204-voltar`, () => {
    cy.visit('http://system-A2/relatorios/val-creditos-icms-controle?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26851825D%7C%7C26851825&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1912915204-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-agendamentos->1868435819-voltar`, () => {
    cy.visit('http://system-A2/informes-fiscais/cred-icms-tomados-priod?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~15422515D%7C%7C15422515&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1868435819-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-agendamentos->1437611396-voltar`, () => {
    cy.visit('http://system-A2/informes-fiscais/contr-cred-modc?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210795D%7C%7C210795&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1437611396-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-agendamentos->2762236547-voltar`, () => {
    cy.visit('http://system-A2/informes-fiscais/contr-cred-modc-mg?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~1868632D%7C%7C1868632&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2762236547-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-agendamentos->2762236705-voltar`, () => {
    cy.visit('http://system-A2/informes-fiscais/contr-cred-modc-rj?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~4625911D%7C%7C4625911&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2762236705-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-agendamentos->1437611397-voltar`, () => {
    cy.visit('http://system-A2/informes-fiscais/contr-cred-modd?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210796D%7C%7C210796&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1437611397-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
