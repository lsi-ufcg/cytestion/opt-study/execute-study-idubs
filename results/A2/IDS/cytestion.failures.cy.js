describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element operacoes->operacoes/ativacao`, () => {
    cy.clickCauseExist(`[data-cy="operacoes"]`);
    cy.clickCauseExist(`[data-cy="operacoes/ativacao"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-visualizar/editar`, () => {
    cy.clickCauseExist(`[data-cy="regras"]`);
    cy.clickCauseExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
    cy.clickCauseExist(`[data-cy="3291988906-selectoutlined"]`);
    cy.clickCauseExist(`[data-cy="2051661756-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
  });
});
