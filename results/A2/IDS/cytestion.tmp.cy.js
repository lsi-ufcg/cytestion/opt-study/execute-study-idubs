describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
const actualId = [`root`,`tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
const actualId = [`root`,`tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabil`, () => {
const actualId = [`root`,`contabil`];
    cy.clickIfExist(`[data-cy="contabil"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes`, () => {
const actualId = [`root`,`integracoes`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element controles-ciap`, () => {
const actualId = [`root`,`controles-ciap`];
    cy.clickIfExist(`[data-cy="controles-ciap"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes`, () => {
const actualId = [`root`,`operacoes`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras`, () => {
const actualId = [`root`,`regras`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais`, () => {
const actualId = [`root`,`informes-fiscais`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/cfop`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/cfop`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/cfop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tabela-codigos-efd-icms-ipi`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tabela-codigos-efd-icms-ipi`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tabela-codigos-efd-icms-ipi"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/ncm`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/ncm`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/ncm"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/parametrizacao-geral`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametrizacao-geral"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/edof`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/edof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/edof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/nop`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/nop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/nop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/config-livro`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/config-livro`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/config-livro"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/hierarquia-pessoas`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/hierarquia-pessoas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/hierarquia-pessoas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/cadastro`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-prestacoes`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/cadastro-prestacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabil->contabil/plano-contas`, () => {
const actualId = [`root`,`contabil`,`contabil/plano-contas`];
    cy.clickIfExist(`[data-cy="contabil"]`);
      cy.clickIfExist(`[data-cy="contabil/plano-contas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabil->contabil/centro-custo`, () => {
const actualId = [`root`,`contabil`,`contabil/centro-custo`];
    cy.clickIfExist(`[data-cy="contabil"]`);
      cy.clickIfExist(`[data-cy="contabil/centro-custo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/edicao`, () => {
const actualId = [`root`,`integracoes`,`integracoes/edicao`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/edicao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/importacao`, () => {
const actualId = [`root`,`integracoes`,`integracoes/importacao`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/importacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/exportacao`, () => {
const actualId = [`root`,`integracoes`,`integracoes/exportacao`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/exportacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/executar-interfaces`, () => {
const actualId = [`root`,`integracoes`,`integracoes/executar-interfaces`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/executar-interfaces"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/dof`, () => {
const actualId = [`root`,`transacoes`,`transacoes/dof`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element controles-ciap->1421670172-novo`, () => {
const actualId = [`root`,`controles-ciap`,`1421670172-novo`];
    cy.clickIfExist(`[data-cy="controles-ciap"]`);
      cy.clickIfExist(`[data-cy="1421670172-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element controles-ciap->1421670172-power-search-button`, () => {
const actualId = [`root`,`controles-ciap`,`1421670172-power-search-button`];
    cy.clickIfExist(`[data-cy="controles-ciap"]`);
      cy.clickIfExist(`[data-cy="1421670172-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/ajuste-valores`, () => {
const actualId = [`root`,`operacoes`,`operacoes/ajuste-valores`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/ajuste-valores"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/ativacao`, () => {
const actualId = [`root`,`operacoes`,`operacoes/ativacao`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/ativacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/saidas`, () => {
const actualId = [`root`,`operacoes`,`operacoes/saidas`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/saidas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/atribuicao`, () => {
const actualId = [`root`,`operacoes`,`operacoes/atribuicao`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/atribuicao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/total-saidas`, () => {
const actualId = [`root`,`operacoes`,`operacoes/total-saidas`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/total-saidas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/geracao-controle`, () => {
const actualId = [`root`,`regras`,`regras/geracao-controle`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/geracao-controle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/apuracao-saidas`, () => {
const actualId = [`root`,`regras`,`regras/apuracao-saidas`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/apuracao-saidas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/calculo-creditos`, () => {
const actualId = [`root`,`regras`,`regras/calculo-creditos`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/calculo-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/parametros-uf`, () => {
const actualId = [`root`,`regras`,`regras/parametros-uf`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/parametros-uf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-creditos`, () => {
const actualId = [`root`,`processos`,`processos/geracao-creditos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/lançamentos-ajustes`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/lançamentos-ajustes`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/lançamentos-ajustes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->538981869-power-search-button`, () => {
const actualId = [`root`,`download`,`538981869-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="538981869-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->538981869-download`, () => {
const actualId = [`root`,`download`,`538981869-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="538981869-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->538981869-detalhes`, () => {
const actualId = [`root`,`download`,`538981869-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="538981869-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->538981869-excluir`, () => {
const actualId = [`root`,`download`,`538981869-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="538981869-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element controles-ciap->1421670172-novo->4032240461-salvar`, () => {
const actualId = [`root`,`controles-ciap`,`1421670172-novo`,`4032240461-salvar`];
    cy.clickIfExist(`[data-cy="controles-ciap"]`);
      cy.clickIfExist(`[data-cy="1421670172-novo"]`);
      cy.clickIfExist(`[data-cy="4032240461-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element controles-ciap->1421670172-novo->4032240461-voltar`, () => {
const actualId = [`root`,`controles-ciap`,`1421670172-novo`,`4032240461-voltar`];
    cy.clickIfExist(`[data-cy="controles-ciap"]`);
      cy.clickIfExist(`[data-cy="1421670172-novo"]`);
      cy.clickIfExist(`[data-cy="4032240461-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values controles-ciap->1421670172-novo->4032240461-input-bemCodigo-4032240461-input-numPlaqueta-4032240461-input-projeto-4032240461-input-seqIncorporacao-4032240461-input-monetary-qtd-4032240461-powerselect-dfltNbmCodigo-4032240461-textarea-descricao-4032240461-textarea-funcao-4032240461-powerselect-codCta-4032240461-powerselect-codCcus-4032240461-powerselect-codCtaDepr-4032240461-input-vidaUtil-4032240461-checkbox-checkBemAtivCirculante-4032240461-checkbox-checkIndCpc-4032240461-powerselect-cmercCodigo-4032240461-powerselect-edofCodigo-4032240461-input-serieSubserie-4032240461-input-numero-4032240461-powerselect-cfopCodigo-4032240461-powerselect-nopCodigo-4032240461-input-numItem-4032240461-powerselect-subclasseIdf-4032240461-powerselect-unid-4032240461-powerselect-emitentePfjCodigo-4032240461-powerselect-emitenteLocCodigo-4032240461-powerselect-remetentePfjCodigo-4032240461-powerselect-remetenteLocCodigo-4032240461-input-monetary-vlOriginalCorrigido-4032240461-input-monetary-vlIcmsApropriado-4032240461-input-monetary-vlIcmsDifApropriado-4032240461-input-monetary-vlIcmsSttApropriado-4032240461-input-monetary-vlIcmsStfApropriado-4032240461-input-monetary-vlIcmsFrtApropriado-4032240461-input-numLivroFiscal-4032240461-input-folhaLivroFiscal-4032240461-input-nfeLocalizador-4032240461-input-numDa and submit`, () => {
const actualId = [`root`,`controles-ciap`,`1421670172-novo`,`4032240461-input-bemCodigo-4032240461-input-numPlaqueta-4032240461-input-projeto-4032240461-input-seqIncorporacao-4032240461-input-monetary-qtd-4032240461-powerselect-dfltNbmCodigo-4032240461-textarea-descricao-4032240461-textarea-funcao-4032240461-powerselect-codCta-4032240461-powerselect-codCcus-4032240461-powerselect-codCtaDepr-4032240461-input-vidaUtil-4032240461-checkbox-checkBemAtivCirculante-4032240461-checkbox-checkIndCpc-4032240461-powerselect-cmercCodigo-4032240461-powerselect-edofCodigo-4032240461-input-serieSubserie-4032240461-input-numero-4032240461-powerselect-cfopCodigo-4032240461-powerselect-nopCodigo-4032240461-input-numItem-4032240461-powerselect-subclasseIdf-4032240461-powerselect-unid-4032240461-powerselect-emitentePfjCodigo-4032240461-powerselect-emitenteLocCodigo-4032240461-powerselect-remetentePfjCodigo-4032240461-powerselect-remetenteLocCodigo-4032240461-input-monetary-vlOriginalCorrigido-4032240461-input-monetary-vlIcmsApropriado-4032240461-input-monetary-vlIcmsDifApropriado-4032240461-input-monetary-vlIcmsSttApropriado-4032240461-input-monetary-vlIcmsStfApropriado-4032240461-input-monetary-vlIcmsFrtApropriado-4032240461-input-numLivroFiscal-4032240461-input-folhaLivroFiscal-4032240461-input-nfeLocalizador-4032240461-input-numDa`];
    cy.clickIfExist(`[data-cy="controles-ciap"]`);
      cy.clickIfExist(`[data-cy="1421670172-novo"]`);
      cy.fillInput(`[data-cy="4032240461-input-bemCodigo"] textarea`, `Designer`);
cy.fillInput(`[data-cy="4032240461-input-numPlaqueta"] textarea`, `Chilean Peso Unidades de fomento`);
cy.fillInput(`[data-cy="4032240461-input-projeto"] textarea`, `Netherlands Antillian Guilder`);
cy.fillInput(`[data-cy="4032240461-input-seqIncorporacao"] textarea`, `incentivize`);
cy.fillInput(`[data-cy="4032240461-input-monetary-qtd"] textarea`, `10`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-dfltNbmCodigo"] input`);
cy.fillInput(`[data-cy="4032240461-textarea-descricao"] input`, `quantifying`);
cy.fillInput(`[data-cy="4032240461-textarea-funcao"] input`, `SAS`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-codCta"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-codCcus"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-codCtaDepr"] input`);
cy.fillInput(`[data-cy="4032240461-input-vidaUtil"] textarea`, `Configurao`);
cy.fillInputCheckboxOrRadio(`[data-cy="4032240461-checkbox-checkBemAtivCirculante"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4032240461-checkbox-checkIndCpc"] textarea`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-cmercCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="4032240461-input-serieSubserie"] textarea`, `deposit`);
cy.fillInput(`[data-cy="4032240461-input-numero"] textarea`, `THX`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-nopCodigo"] input`);
cy.fillInput(`[data-cy="4032240461-input-numItem"] textarea`, `Chips`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-subclasseIdf"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-unid"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-emitentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-emitenteLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-remetentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="4032240461-powerselect-remetenteLocCodigo"] input`);
cy.fillInput(`[data-cy="4032240461-input-monetary-vlOriginalCorrigido"] textarea`, `4`);
cy.fillInput(`[data-cy="4032240461-input-monetary-vlIcmsApropriado"] textarea`, `9`);
cy.fillInput(`[data-cy="4032240461-input-monetary-vlIcmsDifApropriado"] textarea`, `10`);
cy.fillInput(`[data-cy="4032240461-input-monetary-vlIcmsSttApropriado"] textarea`, `1`);
cy.fillInput(`[data-cy="4032240461-input-monetary-vlIcmsStfApropriado"] textarea`, `1`);
cy.fillInput(`[data-cy="4032240461-input-monetary-vlIcmsFrtApropriado"] textarea`, `10`);
cy.fillInput(`[data-cy="4032240461-input-numLivroFiscal"] textarea`, `Tasty`);
cy.fillInput(`[data-cy="4032240461-input-folhaLivroFiscal"] textarea`, `envisioneer`);
cy.fillInput(`[data-cy="4032240461-input-nfeLocalizador"] textarea`, `Riel`);
cy.fillInput(`[data-cy="4032240461-input-numDa"] textarea`, `Cotton`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/ajuste-valores->3035848061-power-search-button`, () => {
const actualId = [`root`,`operacoes`,`operacoes/ajuste-valores`,`3035848061-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/ajuste-valores"]`);
      cy.clickIfExist(`[data-cy="3035848061-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/saidas->2604458697-novo`, () => {
const actualId = [`root`,`operacoes`,`operacoes/saidas`,`2604458697-novo`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/saidas"]`);
      cy.clickIfExist(`[data-cy="2604458697-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/saidas->2604458697-power-search-button`, () => {
const actualId = [`root`,`operacoes`,`operacoes/saidas`,`2604458697-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/saidas"]`);
      cy.clickIfExist(`[data-cy="2604458697-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/atribuicao->3844251654-pesquisar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/atribuicao`,`3844251654-pesquisar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/atribuicao"]`);
      cy.clickIfExist(`[data-cy="3844251654-pesquisar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/total-saidas->809189514-novo`, () => {
const actualId = [`root`,`operacoes`,`operacoes/total-saidas`,`809189514-novo`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/total-saidas"]`);
      cy.clickIfExist(`[data-cy="809189514-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/total-saidas->809189514-power-search-button`, () => {
const actualId = [`root`,`operacoes`,`operacoes/total-saidas`,`809189514-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/total-saidas"]`);
      cy.clickIfExist(`[data-cy="809189514-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/geracao-controle->269251538-plusoutlined`, () => {
const actualId = [`root`,`regras`,`regras/geracao-controle`,`269251538-plusoutlined`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/geracao-controle"]`);
      cy.clickIfExist(`[data-cy="269251538-plusoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/geracao-controle->269251538-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/geracao-controle`,`269251538-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/geracao-controle"]`);
      cy.clickIfExist(`[data-cy="269251538-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/apuracao-saidas->3172943428-plusoutlined`, () => {
const actualId = [`root`,`regras`,`regras/apuracao-saidas`,`3172943428-plusoutlined`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/apuracao-saidas"]`);
      cy.clickIfExist(`[data-cy="3172943428-plusoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/apuracao-saidas->3172943428-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/apuracao-saidas`,`3172943428-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/apuracao-saidas"]`);
      cy.clickIfExist(`[data-cy="3172943428-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/calculo-creditos->854429064-plusoutlined`, () => {
const actualId = [`root`,`regras`,`regras/calculo-creditos`,`854429064-plusoutlined`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/calculo-creditos"]`);
      cy.clickIfExist(`[data-cy="854429064-plusoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/calculo-creditos->854429064-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/calculo-creditos`,`854429064-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/calculo-creditos"]`);
      cy.clickIfExist(`[data-cy="854429064-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/parametros-uf->111041507-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/parametros-uf`,`111041507-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/parametros-uf"]`);
      cy.clickIfExist(`[data-cy="111041507-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/parametros-uf->111041507-eyeoutlined`, () => {
const actualId = [`root`,`regras`,`regras/parametros-uf`,`111041507-eyeoutlined`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/parametros-uf"]`);
      cy.clickIfExist(`[data-cy="111041507-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-novo`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-excluir`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-excluir`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-executar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-ajuda`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-ajuda`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-visualização`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-regerar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-executar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-ajuda`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-ajuda`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-visualização`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-executar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-visualização`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-executar`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-visualização`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-executar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-visualização`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-regerar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-excluir`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-executar`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-visualização`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-regerar`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-excluir`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-executar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-visualização`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-executar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-visualização`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-executar`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-visualização`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-regerar`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-excluir`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-executar`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-visualização`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-executar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-executar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-agendamentos`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-agendamentos`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-power-search-button`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-power-search-button`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-visualização`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-visualização`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-regerar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-regerar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-detalhes`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-detalhes`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-abrir visualização`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-abrir visualização`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-excluir`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-excluir`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-executar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-executar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-agendamentos`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-agendamentos`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-power-search-button`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-power-search-button`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-visualização`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-visualização`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-executar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-executar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-agendamentos`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-agendamentos`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-power-search-button`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-power-search-button`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-visualização`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-visualização`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-executar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-executar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-agendamentos`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-agendamentos`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-power-search-button`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-power-search-button`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-visualização`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-visualização`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-executar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-executar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-agendamentos`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-agendamentos`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-power-search-button`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-power-search-button`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-visualização`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-visualização`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2182589889-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`2182589889-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2182589889-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2182589889-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`2182589889-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2182589889-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2182589889-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`2182589889-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2182589889-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/saidas->2604458697-novo->189297600-salvar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/saidas`,`2604458697-novo`,`189297600-salvar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/saidas"]`);
      cy.clickIfExist(`[data-cy="2604458697-novo"]`);
      cy.clickIfExist(`[data-cy="189297600-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/saidas->2604458697-novo->189297600-voltar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/saidas`,`2604458697-novo`,`189297600-voltar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/saidas"]`);
      cy.clickIfExist(`[data-cy="2604458697-novo"]`);
      cy.clickIfExist(`[data-cy="189297600-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values operacoes->operacoes/saidas->2604458697-novo->189297600-powerselect-informanteEstCodigo-189297600-powerselect-ctrlNotaFiscal-189297600-powerselect-ctrlOperacao-189297600-input-motivoBaixa and submit`, () => {
const actualId = [`root`,`operacoes`,`operacoes/saidas`,`2604458697-novo`,`189297600-powerselect-informanteEstCodigo-189297600-powerselect-ctrlNotaFiscal-189297600-powerselect-ctrlOperacao-189297600-input-motivoBaixa`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/saidas"]`);
      cy.clickIfExist(`[data-cy="2604458697-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="189297600-powerselect-informanteEstCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="189297600-powerselect-ctrlNotaFiscal"] input`);
cy.fillInputPowerSelect(`[data-cy="189297600-powerselect-ctrlOperacao"] input`);
cy.fillInput(`[data-cy="189297600-input-motivoBaixa"] textarea`, `navigate`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/atribuicao->3844251654-pesquisar->3844251654-power-search-button`, () => {
const actualId = [`root`,`operacoes`,`operacoes/atribuicao`,`3844251654-pesquisar`,`3844251654-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/atribuicao"]`);
      cy.clickIfExist(`[data-cy="3844251654-pesquisar"]`);
      cy.clickIfExist(`[data-cy="3844251654-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/atribuicao->3844251654-pesquisar->3844251654-cancelar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/atribuicao`,`3844251654-pesquisar`,`3844251654-cancelar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/atribuicao"]`);
      cy.clickIfExist(`[data-cy="3844251654-pesquisar"]`);
      cy.clickIfExist(`[data-cy="3844251654-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/total-saidas->809189514-novo->809189514-salvar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/total-saidas`,`809189514-novo`,`809189514-salvar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/total-saidas"]`);
      cy.clickIfExist(`[data-cy="809189514-novo"]`);
      cy.clickIfExist(`[data-cy="809189514-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/total-saidas->809189514-novo->809189514-voltar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/total-saidas`,`809189514-novo`,`809189514-voltar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/total-saidas"]`);
      cy.clickIfExist(`[data-cy="809189514-novo"]`);
      cy.clickIfExist(`[data-cy="809189514-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values operacoes->operacoes/total-saidas->809189514-novo->809189514-powerselect-estCodigo-809189514-input-monetary-vlTotalBases-809189514-input-monetary-vlTotalIsentas-809189514-input-monetary-vlTotalSaidas-809189514-checkbox-indicaHierarquia and submit`, () => {
const actualId = [`root`,`operacoes`,`operacoes/total-saidas`,`809189514-novo`,`809189514-powerselect-estCodigo-809189514-input-monetary-vlTotalBases-809189514-input-monetary-vlTotalIsentas-809189514-input-monetary-vlTotalSaidas-809189514-checkbox-indicaHierarquia`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/total-saidas"]`);
      cy.clickIfExist(`[data-cy="809189514-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="809189514-powerselect-estCodigo"] input`);
cy.fillInput(`[data-cy="809189514-input-monetary-vlTotalBases"] textarea`, `6`);
cy.fillInput(`[data-cy="809189514-input-monetary-vlTotalIsentas"] textarea`, `2`);
cy.fillInput(`[data-cy="809189514-input-monetary-vlTotalSaidas"] textarea`, `4`);
cy.fillInputCheckboxOrRadio(`[data-cy="809189514-checkbox-indicaHierarquia"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/geracao-controle->269251538-plusoutlined->269251538-input-titulo and submit`, () => {
const actualId = [`root`,`regras`,`regras/geracao-controle`,`269251538-plusoutlined`,`269251538-input-titulo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/geracao-controle"]`);
      cy.clickIfExist(`[data-cy="269251538-plusoutlined"]`);
      cy.fillInput(`[data-cy="269251538-input-titulo"] textarea`, `Chips`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/apuracao-saidas->3172943428-plusoutlined->3172943428-input-titulo and submit`, () => {
const actualId = [`root`,`regras`,`regras/apuracao-saidas`,`3172943428-plusoutlined`,`3172943428-input-titulo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/apuracao-saidas"]`);
      cy.clickIfExist(`[data-cy="3172943428-plusoutlined"]`);
      cy.fillInput(`[data-cy="3172943428-input-titulo"] textarea`, `digital`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/calculo-creditos->854429064-plusoutlined->854429064-input-titulo and submit`, () => {
const actualId = [`root`,`regras`,`regras/calculo-creditos`,`854429064-plusoutlined`,`854429064-input-titulo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/calculo-creditos"]`);
      cy.clickIfExist(`[data-cy="854429064-plusoutlined"]`);
      cy.fillInput(`[data-cy="854429064-input-titulo"] textarea`, `lavender`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/parametros-uf->111041507-eyeoutlined->3222286876-salvar`, () => {
const actualId = [`root`,`regras`,`regras/parametros-uf`,`111041507-eyeoutlined`,`3222286876-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/parametros-uf"]`);
      cy.clickIfExist(`[data-cy="111041507-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="3222286876-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/parametros-uf->111041507-eyeoutlined->3222286876-voltar`, () => {
const actualId = [`root`,`regras`,`regras/parametros-uf`,`111041507-eyeoutlined`,`3222286876-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/parametros-uf"]`);
      cy.clickIfExist(`[data-cy="111041507-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="3222286876-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-novo->738981759-salvar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-novo`,`738981759-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-novo"]`);
      cy.clickIfExist(`[data-cy="738981759-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-novo->738981759-voltar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-novo`,`738981759-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-novo"]`);
      cy.clickIfExist(`[data-cy="738981759-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/lancamentos-ajustes-regras->3291988906-novo->738981759-input-rgajCodigo-738981759-textarea-obsRegra and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-novo`,`738981759-input-rgajCodigo-738981759-textarea-obsRegra`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-novo"]`);
      cy.fillInput(`[data-cy="738981759-input-rgajCodigo"] textarea`, `Secured`);
cy.fillInput(`[data-cy="738981759-textarea-obsRegra"] input`, `Corporativo`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-novo`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-visualizar/editar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-visualizar/editar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-excluir`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-excluir`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-selecionar critérios`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-selecionar critérios`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-selecionar critérios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-estabelecimentos da regra`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-estabelecimentos da regra`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-estabelecimentos da regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-itens de ajuste`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-itens de ajuste`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-itens de ajuste"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-remover item`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-remover item`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-salvar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-voltar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-textarea-obsRegra and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-textarea-obsRegra`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.fillInput(`[data-cy="2442857990-textarea-obsRegra"] input`, `redundant`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-executar->1614615400-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-executar`,`1614615400-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-executar"]`);
      cy.clickIfExist(`[data-cy="1614615400-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-executar->1614615400-agendar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-executar`,`1614615400-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-executar"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-voltar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-excluir`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/geracao-controles->1614615400-visualização->1614615400-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-visualização`,`1614615400-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1614615400-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-detalhes->1614615400-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-detalhes`,`1614615400-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-detalhes"]`);
      cy.clickIfExist(`[data-cy="1614615400-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-abrir visualização->1614615400-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-abrir visualização`,`1614615400-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614615400-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-abrir visualização->1614615400-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-abrir visualização`,`1614615400-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614615400-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-abrir visualização->1614615400-expandir`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-abrir visualização`,`1614615400-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614615400-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-abrir visualização->1614615400-download`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-abrir visualização`,`1614615400-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614615400-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-executar->287994784-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-executar`,`287994784-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-executar"]`);
      cy.clickIfExist(`[data-cy="287994784-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-executar->287994784-agendar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-executar`,`287994784-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-executar"]`);
      cy.clickIfExist(`[data-cy="287994784-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-agendamentos->287994784-voltar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-agendamentos`,`287994784-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-agendamentos"]`);
      cy.clickIfExist(`[data-cy="287994784-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/exclusao-controles->287994784-visualização->287994784-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-visualização`,`287994784-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="287994784-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-executar->1629306443-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-executar`,`1629306443-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-executar"]`);
      cy.clickIfExist(`[data-cy="1629306443-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-executar->1629306443-agendar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-executar`,`1629306443-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-executar"]`);
      cy.clickIfExist(`[data-cy="1629306443-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/exclusao-controle-dof-aquisicao->1629306443-executar->1629306443-input-P_NUMERO_DOF-1629306443-input-P_SERIE_SUBSERIE-1629306443-input-P_ITEM_DOF and submit`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-executar`,`1629306443-input-P_NUMERO_DOF-1629306443-input-P_SERIE_SUBSERIE-1629306443-input-P_ITEM_DOF`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-executar"]`);
      cy.fillInput(`[data-cy="1629306443-input-P_NUMERO_DOF"] textarea`, `Objectbased`);
cy.fillInput(`[data-cy="1629306443-input-P_SERIE_SUBSERIE"] textarea`, `interfaces`);
cy.fillInput(`[data-cy="1629306443-input-P_ITEM_DOF"] textarea`, `Paraba`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-agendamentos->1629306443-voltar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-agendamentos`,`1629306443-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1629306443-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/exclusao-controle-dof-aquisicao->1629306443-visualização->1629306443-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-visualização`,`1629306443-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1629306443-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-executar->633719453-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-executar`,`633719453-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-executar"]`);
      cy.clickIfExist(`[data-cy="633719453-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-executar->633719453-agendar`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-executar`,`633719453-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-executar"]`);
      cy.clickIfExist(`[data-cy="633719453-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/totalizacao-saidas->633719453-executar->633719453-input-P_PERIODO and submit`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-executar`,`633719453-input-P_PERIODO`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-executar"]`);
      cy.fillInput(`[data-cy="633719453-input-P_PERIODO"] textarea`, `wellmodulated`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-agendamentos->633719453-voltar`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-agendamentos`,`633719453-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-agendamentos"]`);
      cy.clickIfExist(`[data-cy="633719453-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/totalizacao-saidas->633719453-visualização->633719453-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-visualização`,`633719453-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="633719453-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-executar->2814741288-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-executar`,`2814741288-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-executar"]`);
      cy.clickIfExist(`[data-cy="2814741288-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-executar->2814741288-agendar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-executar`,`2814741288-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-executar"]`);
      cy.clickIfExist(`[data-cy="2814741288-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-agendamentos->2814741288-voltar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-agendamentos`,`2814741288-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2814741288-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/exclusao-creditos->2814741288-visualização->2814741288-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-visualização`,`2814741288-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2814741288-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-detalhes->2814741288-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-detalhes`,`2814741288-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-detalhes"]`);
      cy.clickIfExist(`[data-cy="2814741288-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-abrir visualização->2814741288-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-abrir visualização`,`2814741288-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2814741288-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-abrir visualização->2814741288-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-abrir visualização`,`2814741288-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2814741288-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-abrir visualização->2814741288-expandir`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-abrir visualização`,`2814741288-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2814741288-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-abrir visualização->2814741288-download`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-abrir visualização`,`2814741288-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2814741288-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-executar->298565727-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-executar`,`298565727-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-executar"]`);
      cy.clickIfExist(`[data-cy="298565727-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-executar->298565727-agendar`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-executar`,`298565727-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-executar"]`);
      cy.clickIfExist(`[data-cy="298565727-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/apropriacao-credito-extemporaneos->298565727-executar->298565727-input-P_DT_INICIO-298565727-input-P_DT_FIM-298565727-input-P_PERIODO and submit`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-executar`,`298565727-input-P_DT_INICIO-298565727-input-P_DT_FIM-298565727-input-P_PERIODO`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-executar"]`);
      cy.fillInput(`[data-cy="298565727-input-P_DT_INICIO"] textarea`, `Arquiteto`);
cy.fillInput(`[data-cy="298565727-input-P_DT_FIM"] textarea`, `virtual`);
cy.fillInput(`[data-cy="298565727-input-P_PERIODO"] textarea`, `Zmbia`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-agendamentos->298565727-voltar`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-agendamentos`,`298565727-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-agendamentos"]`);
      cy.clickIfExist(`[data-cy="298565727-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/apropriacao-credito-extemporaneos->298565727-visualização->298565727-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-visualização`,`298565727-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="298565727-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-detalhes->298565727-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-detalhes`,`298565727-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-detalhes"]`);
      cy.clickIfExist(`[data-cy="298565727-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-abrir visualização->298565727-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-abrir visualização`,`298565727-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="298565727-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-abrir visualização->298565727-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-abrir visualização`,`298565727-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="298565727-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-abrir visualização->298565727-expandir`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-abrir visualização`,`298565727-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="298565727-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-abrir visualização->298565727-download`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-abrir visualização`,`298565727-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="298565727-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-executar->2224043782-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-executar`,`2224043782-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-executar"]`);
      cy.clickIfExist(`[data-cy="2224043782-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-executar->2224043782-agendar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-executar`,`2224043782-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-executar"]`);
      cy.clickIfExist(`[data-cy="2224043782-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-agendamentos->2224043782-voltar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-agendamentos`,`2224043782-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2224043782-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/atualizacao-indicador->2224043782-visualização->2224043782-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-visualização`,`2224043782-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2224043782-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-executar->3183959863-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-executar`,`3183959863-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-executar"]`);
      cy.clickIfExist(`[data-cy="3183959863-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-executar->3183959863-agendar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-executar`,`3183959863-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-executar"]`);
      cy.clickIfExist(`[data-cy="3183959863-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/exclusao-op-saida->3183959863-executar->3183959863-input-P_NUMERO_DOF and submit`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-executar`,`3183959863-input-P_NUMERO_DOF`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-executar"]`);
      cy.fillInput(`[data-cy="3183959863-input-P_NUMERO_DOF"] textarea`, `quantify`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-agendamentos->3183959863-voltar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-agendamentos`,`3183959863-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3183959863-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/exclusao-op-saida->3183959863-visualização->3183959863-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-visualização`,`3183959863-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3183959863-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-executar->985668109-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-executar`,`985668109-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-executar"]`);
      cy.clickIfExist(`[data-cy="985668109-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-executar->985668109-agendar`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-executar`,`985668109-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-executar"]`);
      cy.clickIfExist(`[data-cy="985668109-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/suspencao-retomada-creditos->985668109-executar->985668109-input-P_MOTIVO and submit`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-executar`,`985668109-input-P_MOTIVO`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-executar"]`);
      cy.fillInput(`[data-cy="985668109-input-P_MOTIVO"] textarea`, `Checking Account`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-agendamentos->985668109-voltar`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-agendamentos`,`985668109-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-agendamentos"]`);
      cy.clickIfExist(`[data-cy="985668109-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/suspencao-retomada-creditos->985668109-visualização->985668109-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-visualização`,`985668109-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="985668109-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-detalhes->985668109-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-detalhes`,`985668109-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-detalhes"]`);
      cy.clickIfExist(`[data-cy="985668109-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-abrir visualização->985668109-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-abrir visualização`,`985668109-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="985668109-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-abrir visualização->985668109-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-abrir visualização`,`985668109-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="985668109-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-abrir visualização->985668109-expandir`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-abrir visualização`,`985668109-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="985668109-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-abrir visualização->985668109-download`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-abrir visualização`,`985668109-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="985668109-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-executar->1642007593-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-executar`,`1642007593-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-executar"]`);
      cy.clickIfExist(`[data-cy="1642007593-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-executar->1642007593-agendar`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-executar`,`1642007593-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-executar"]`);
      cy.clickIfExist(`[data-cy="1642007593-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/transferencia-inc-societaria->1642007593-executar->1642007593-input-P_MOTIVO_BAIXA and submit`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-executar`,`1642007593-input-P_MOTIVO_BAIXA`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-executar"]`);
      cy.fillInput(`[data-cy="1642007593-input-P_MOTIVO_BAIXA"] textarea`, `solutions`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-agendamentos->1642007593-voltar`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-agendamentos`,`1642007593-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1642007593-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/transferencia-inc-societaria->1642007593-visualização->1642007593-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-visualização`,`1642007593-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1642007593-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-executar->2680178634-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-executar`,`2680178634-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-executar"]`);
      cy.clickIfExist(`[data-cy="2680178634-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-executar->2680178634-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-executar`,`2680178634-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-executar"]`);
      cy.clickIfExist(`[data-cy="2680178634-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/controles-ciap->2680178634-executar->2680178634-input-P_BEM_DE-2680178634-input-P_BEM_ATE-2680178634-input-P_NUM_PLAQ_DE-2680178634-input-P_NUM_PLAQ_ATE and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-executar`,`2680178634-input-P_BEM_DE-2680178634-input-P_BEM_ATE-2680178634-input-P_NUM_PLAQ_DE-2680178634-input-P_NUM_PLAQ_ATE`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-executar"]`);
      cy.fillInput(`[data-cy="2680178634-input-P_BEM_DE"] textarea`, `Investment Account`);
cy.fillInput(`[data-cy="2680178634-input-P_BEM_ATE"] textarea`, `hierarchy`);
cy.fillInput(`[data-cy="2680178634-input-P_NUM_PLAQ_DE"] textarea`, `input`);
cy.fillInput(`[data-cy="2680178634-input-P_NUM_PLAQ_ATE"] textarea`, `ubiquitous`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-agendamentos->2680178634-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-agendamentos`,`2680178634-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2680178634-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/controles-ciap->2680178634-visualização->2680178634-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-visualização`,`2680178634-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2680178634-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-executar->4033549032-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-executar`,`4033549032-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-executar"]`);
      cy.clickIfExist(`[data-cy="4033549032-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-executar->4033549032-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-executar`,`4033549032-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-executar"]`);
      cy.clickIfExist(`[data-cy="4033549032-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/conferenc-controles-incorp->4033549032-executar->4033549032-input-P_PERIODO-4033549032-input-P_ID_CTRL_PAI-4033549032-input-P_COD_BEM_PAI-4033549032-input-P_NUM_PLAQUETA_PAI-4033549032-input-P_ID_CTRL_FILHO-4033549032-input-P_COD_BEM_FILHO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-executar`,`4033549032-input-P_PERIODO-4033549032-input-P_ID_CTRL_PAI-4033549032-input-P_COD_BEM_PAI-4033549032-input-P_NUM_PLAQUETA_PAI-4033549032-input-P_ID_CTRL_FILHO-4033549032-input-P_COD_BEM_FILHO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-executar"]`);
      cy.fillInput(`[data-cy="4033549032-input-P_PERIODO"] textarea`, `navigating`);
cy.fillInput(`[data-cy="4033549032-input-P_ID_CTRL_PAI"] textarea`, `Unbranded`);
cy.fillInput(`[data-cy="4033549032-input-P_COD_BEM_PAI"] textarea`, `Triplebuffered`);
cy.fillInput(`[data-cy="4033549032-input-P_NUM_PLAQUETA_PAI"] textarea`, `Executivo`);
cy.fillInput(`[data-cy="4033549032-input-P_ID_CTRL_FILHO"] textarea`, `Ponte`);
cy.fillInput(`[data-cy="4033549032-input-P_COD_BEM_FILHO"] textarea`, `Rondnia`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/conferenc-controles-incorp->4033549032-agendamentos->4033549032-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-agendamentos`,`4033549032-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4033549032-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/conferenc-controles-incorp->4033549032-visualização->4033549032-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/conferenc-controles-incorp`,`4033549032-visualização`,`4033549032-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/conferenc-controles-incorp"]`);
      cy.clickIfExist(`[data-cy="4033549032-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4033549032-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-executar->172111467-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-executar`,`172111467-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-executar"]`);
      cy.clickIfExist(`[data-cy="172111467-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-executar->172111467-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-executar`,`172111467-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-executar"]`);
      cy.clickIfExist(`[data-cy="172111467-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/creditos-curto-longo-prazo->172111467-executar->172111467-input-P_CURTO_PRAZO-172111467-input-P_LONGO_PRAZO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-executar`,`172111467-input-P_CURTO_PRAZO-172111467-input-P_LONGO_PRAZO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-executar"]`);
      cy.fillInput(`[data-cy="172111467-input-P_CURTO_PRAZO"] textarea`, `deposit`);
cy.fillInput(`[data-cy="172111467-input-P_LONGO_PRAZO"] textarea`, `deposit`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-agendamentos->172111467-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-agendamentos`,`172111467-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-agendamentos"]`);
      cy.clickIfExist(`[data-cy="172111467-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/creditos-curto-longo-prazo->172111467-visualização->172111467-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-visualização`,`172111467-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="172111467-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-executar->4272249838-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-executar`,`4272249838-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-executar"]`);
      cy.clickIfExist(`[data-cy="4272249838-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-executar->4272249838-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-executar`,`4272249838-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-executar"]`);
      cy.clickIfExist(`[data-cy="4272249838-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-agendamentos->4272249838-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-agendamentos`,`4272249838-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4272249838-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/creditos-parcelados-periodo->4272249838-visualização->4272249838-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-visualização`,`4272249838-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4272249838-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-executar->532328817-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-executar`,`532328817-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-executar"]`);
      cy.clickIfExist(`[data-cy="532328817-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-executar->532328817-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-executar`,`532328817-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-executar"]`);
      cy.clickIfExist(`[data-cy="532328817-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/diferencas-arredondamento->532328817-executar->532328817-input-P_PERIODO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-executar`,`532328817-input-P_PERIODO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-executar"]`);
      cy.fillInput(`[data-cy="532328817-input-P_PERIODO"] textarea`, `enhance`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-agendamentos->532328817-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-agendamentos`,`532328817-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-agendamentos"]`);
      cy.clickIfExist(`[data-cy="532328817-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/diferencas-arredondamento->532328817-visualização->532328817-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-visualização`,`532328817-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="532328817-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-executar->3190201375-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-executar`,`3190201375-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-executar"]`);
      cy.clickIfExist(`[data-cy="3190201375-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-executar->3190201375-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-executar`,`3190201375-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-executar"]`);
      cy.clickIfExist(`[data-cy="3190201375-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/operacoes-de-saida->3190201375-executar->3190201375-input-P_NUMERO-3190201375-input-P_SERIE-3190201375-input-P_ID_CONTROLE-3190201375-input-P_BEM_CODIGO-3190201375-input-P_PLAQUETA-3190201375-input-P_COD_CTA and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-executar`,`3190201375-input-P_NUMERO-3190201375-input-P_SERIE-3190201375-input-P_ID_CONTROLE-3190201375-input-P_BEM_CODIGO-3190201375-input-P_PLAQUETA-3190201375-input-P_COD_CTA`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-executar"]`);
      cy.fillInput(`[data-cy="3190201375-input-P_NUMERO"] textarea`, `emulation`);
cy.fillInput(`[data-cy="3190201375-input-P_SERIE"] textarea`, `Branding`);
cy.fillInput(`[data-cy="3190201375-input-P_ID_CONTROLE"] textarea`, `Planejador`);
cy.fillInput(`[data-cy="3190201375-input-P_BEM_CODIGO"] textarea`, `haptic`);
cy.fillInput(`[data-cy="3190201375-input-P_PLAQUETA"] textarea`, `Fantastic`);
cy.fillInput(`[data-cy="3190201375-input-P_COD_CTA"] textarea`, `contingency`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/operacoes-de-saida->3190201375-visualização->3190201375-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-visualização`,`3190201375-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3190201375-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-executar->1912915204-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-executar`,`1912915204-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-executar"]`);
      cy.clickIfExist(`[data-cy="1912915204-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-executar->1912915204-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-executar`,`1912915204-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-executar"]`);
      cy.clickIfExist(`[data-cy="1912915204-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-agendamentos->1912915204-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-agendamentos`,`1912915204-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1912915204-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/val-creditos-icms-controle->1912915204-visualização->1912915204-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-visualização`,`1912915204-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1912915204-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-detalhes->1912915204-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-detalhes`,`1912915204-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-detalhes"]`);
      cy.clickIfExist(`[data-cy="1912915204-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-abrir visualização->1912915204-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-abrir visualização`,`1912915204-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1912915204-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-abrir visualização->1912915204-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-abrir visualização`,`1912915204-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1912915204-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-abrir visualização->1912915204-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-abrir visualização`,`1912915204-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1912915204-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-abrir visualização->1912915204-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-abrir visualização`,`1912915204-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1912915204-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-executar->1868435819-múltipla seleção`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-executar`,`1868435819-múltipla seleção`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-executar"]`);
      cy.clickIfExist(`[data-cy="1868435819-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-executar->1868435819-agendar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-executar`,`1868435819-agendar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-executar"]`);
      cy.clickIfExist(`[data-cy="1868435819-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-executar->1868435819-input-P_ANO-1868435819-input-P_MES and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-executar`,`1868435819-input-P_ANO-1868435819-input-P_MES`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-executar"]`);
      cy.fillInput(`[data-cy="1868435819-input-P_ANO"] textarea`, `pixel`);
cy.fillInput(`[data-cy="1868435819-input-P_MES"] textarea`, `Handmade Rubber Car`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-agendamentos->1868435819-voltar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-agendamentos`,`1868435819-voltar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1868435819-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-visualização->1868435819-item- and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-visualização`,`1868435819-item-`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1868435819-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-detalhes->1868435819-dados disponíveis para impressão`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-detalhes`,`1868435819-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-detalhes"]`);
      cy.clickIfExist(`[data-cy="1868435819-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-abrir visualização->1868435819-aumentar o zoom`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-abrir visualização`,`1868435819-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1868435819-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-abrir visualização->1868435819-diminuir o zoom`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-abrir visualização`,`1868435819-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1868435819-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-abrir visualização->1868435819-expandir`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-abrir visualização`,`1868435819-expandir`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1868435819-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-abrir visualização->1868435819-download`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-abrir visualização`,`1868435819-download`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1868435819-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-executar->1437611396-múltipla seleção`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-executar`,`1437611396-múltipla seleção`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-executar"]`);
      cy.clickIfExist(`[data-cy="1437611396-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-executar->1437611396-agendar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-executar`,`1437611396-agendar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-executar"]`);
      cy.clickIfExist(`[data-cy="1437611396-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-executar->1437611396-input-P_NUMERO_LIVRO-1437611396-input-P_FOLHA_INICIAL and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-executar`,`1437611396-input-P_NUMERO_LIVRO-1437611396-input-P_FOLHA_INICIAL`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-executar"]`);
      cy.fillInput(`[data-cy="1437611396-input-P_NUMERO_LIVRO"] textarea`, `driver`);
cy.fillInput(`[data-cy="1437611396-input-P_FOLHA_INICIAL"] textarea`, `Granite`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-agendamentos->1437611396-voltar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-agendamentos`,`1437611396-voltar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1437611396-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-visualização->1437611396-item- and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-visualização`,`1437611396-item-`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1437611396-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-executar->2762236547-múltipla seleção`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-executar`,`2762236547-múltipla seleção`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-executar"]`);
      cy.clickIfExist(`[data-cy="2762236547-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-executar->2762236547-agendar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-executar`,`2762236547-agendar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-executar"]`);
      cy.clickIfExist(`[data-cy="2762236547-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-executar->2762236547-input-P_NUMERO_LIVRO-2762236547-input-P_FOLHA_INICIAL and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-executar`,`2762236547-input-P_NUMERO_LIVRO-2762236547-input-P_FOLHA_INICIAL`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-executar"]`);
      cy.fillInput(`[data-cy="2762236547-input-P_NUMERO_LIVRO"] textarea`, `Marca`);
cy.fillInput(`[data-cy="2762236547-input-P_FOLHA_INICIAL"] textarea`, `bypassing`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-agendamentos->2762236547-voltar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-agendamentos`,`2762236547-voltar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2762236547-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-visualização->2762236547-item- and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-visualização`,`2762236547-item-`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2762236547-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-executar->2762236705-múltipla seleção`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-executar`,`2762236705-múltipla seleção`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-executar"]`);
      cy.clickIfExist(`[data-cy="2762236705-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-executar->2762236705-agendar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-executar`,`2762236705-agendar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-executar"]`);
      cy.clickIfExist(`[data-cy="2762236705-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-executar->2762236705-input-P_NUMERO_LIVRO-2762236705-input-P_FOLHA_INICIAL and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-executar`,`2762236705-input-P_NUMERO_LIVRO-2762236705-input-P_FOLHA_INICIAL`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-executar"]`);
      cy.fillInput(`[data-cy="2762236705-input-P_NUMERO_LIVRO"] textarea`, `Cheese`);
cy.fillInput(`[data-cy="2762236705-input-P_FOLHA_INICIAL"] textarea`, `Credit Card Account`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-agendamentos->2762236705-voltar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-agendamentos`,`2762236705-voltar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2762236705-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-visualização->2762236705-item- and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-visualização`,`2762236705-item-`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2762236705-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-executar->1437611397-múltipla seleção`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-executar`,`1437611397-múltipla seleção`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-executar"]`);
      cy.clickIfExist(`[data-cy="1437611397-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-executar->1437611397-agendar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-executar`,`1437611397-agendar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-executar"]`);
      cy.clickIfExist(`[data-cy="1437611397-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-executar->1437611397-input-P_NUMERO_LIVRO-1437611397-input-P_FOLHA_INICIAL and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-executar`,`1437611397-input-P_NUMERO_LIVRO-1437611397-input-P_FOLHA_INICIAL`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-executar"]`);
      cy.fillInput(`[data-cy="1437611397-input-P_NUMERO_LIVRO"] textarea`, `copy`);
cy.fillInput(`[data-cy="1437611397-input-P_FOLHA_INICIAL"] textarea`, `Opensource`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-agendamentos->1437611397-voltar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-agendamentos`,`1437611397-voltar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1437611397-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-visualização->1437611397-item- and submit`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-visualização`,`1437611397-item-`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1437611397-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-gerenciar labels->3982231145-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-gerenciar labels`,`3982231145-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="3982231145-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-visualizar/editar->2618916230-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-visualizar/editar`,`2618916230-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2618916230-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3982231145-visualizar/editar->2618916230-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`3982231145-visualizar/editar`,`2618916230-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="3982231145-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2618916230-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-novo->4090291865-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-novo`,`4090291865-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-novo"]`);
      cy.clickIfExist(`[data-cy="4090291865-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-novo->4090291865-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-novo`,`4090291865-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-novo"]`);
      cy.clickIfExist(`[data-cy="4090291865-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->4090291865-novo->4090291865-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-novo`,`4090291865-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-novo"]`);
      cy.fillInput(`[data-cy="4090291865-input-number-ano"] textarea`, `4`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-editar->4090291865-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-editar`,`4090291865-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-editar"]`);
      cy.clickIfExist(`[data-cy="4090291865-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4090291865-editar->4090291865-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4090291865-editar`,`4090291865-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4090291865-editar"]`);
      cy.clickIfExist(`[data-cy="4090291865-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2182589889-novo->2182589889-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`2182589889-novo`,`2182589889-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2182589889-novo"]`);
      cy.clickIfExist(`[data-cy="2182589889-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualização->1017963809-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualização`,`1017963809-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1017963809-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar->1017963809-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`,`1017963809-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.clickIfExist(`[data-cy="1017963809-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-novo->1470273197-salvar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-novo`,`1470273197-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-novo"]`);
      cy.clickIfExist(`[data-cy="1470273197-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-novo->1470273197-voltar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-novo`,`1470273197-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-novo"]`);
      cy.clickIfExist(`[data-cy="1470273197-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/lancamentos-ajustes-regras->3291988906-selectoutlined->2051661756-novo->1470273197-powerselect-ufCodigo-1470273197-checkbox-detalhaAjuste-1470273197-checkbox-operacaoIncentivada-1470273197-powerselect-vlAjuste-1470273197-powerselect-descrAjuste and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-selectoutlined`,`2051661756-novo`,`1470273197-powerselect-ufCodigo-1470273197-checkbox-detalhaAjuste-1470273197-checkbox-operacaoIncentivada-1470273197-powerselect-vlAjuste-1470273197-powerselect-descrAjuste`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2051661756-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1470273197-powerselect-ufCodigo"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="1470273197-checkbox-detalhaAjuste"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1470273197-checkbox-operacaoIncentivada"] textarea`);
cy.fillInputPowerSelect(`[data-cy="1470273197-powerselect-vlAjuste"] input`);
cy.fillInputPowerSelect(`[data-cy="1470273197-powerselect-descrAjuste"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-selecionar critérios->2442857990-item-cfop`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-selecionar critérios`,`2442857990-item-cfop`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-selecionar critérios"]`);
      cy.clickIfExist(`[data-cy="2442857990-item-cfop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-selecionar critérios->2442857990-item-nop`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-selecionar critérios`,`2442857990-item-nop`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-selecionar critérios"]`);
      cy.clickIfExist(`[data-cy="2442857990-item-nop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-estabelecimentos da regra->2442857990-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-estabelecimentos da regra`,`2442857990-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-estabelecimentos da regra"]`);
      cy.clickIfExist(`[data-cy="2442857990-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-estabelecimentos da regra->2442857990-carregar mais`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-estabelecimentos da regra`,`2442857990-carregar mais`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-estabelecimentos da regra"]`);
      cy.clickIfExist(`[data-cy="2442857990-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-estabelecimentos da regra->2442857990-fechar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-estabelecimentos da regra`,`2442857990-fechar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-estabelecimentos da regra"]`);
      cy.clickIfExist(`[data-cy="2442857990-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/lancamentos-ajustes-regras->3291988906-visualizar/editar->2442857990-estabelecimentos da regra->2442857990-power-search-input and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-ajustes-regras`,`3291988906-visualizar/editar`,`2442857990-estabelecimentos da regra`,`2442857990-power-search-input`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-ajustes-regras"]`);
      cy.clickIfExist(`[data-cy="3291988906-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2442857990-estabelecimentos da regra"]`);
      cy.fillInputPowerSearch(`[data-cy="2442857990-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-executar->1614615400-múltipla seleção->1614615400-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-executar`,`1614615400-múltipla seleção`,`1614615400-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-executar"]`);
      cy.clickIfExist(`[data-cy="1614615400-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1614615400-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar->1614615400-button`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`,`1614615400-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.clickIfExist(`[data-cy="1614615400-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-abrir visualização->1614615400-expandir->1614615400-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-abrir visualização`,`1614615400-expandir`,`1614615400-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1614615400-expandir"]`);
      cy.clickIfExist(`[data-cy="1614615400-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controles->287994784-executar->287994784-múltipla seleção->287994784-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controles`,`287994784-executar`,`287994784-múltipla seleção`,`287994784-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="287994784-executar"]`);
      cy.clickIfExist(`[data-cy="287994784-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="287994784-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-controle-dof-aquisicao->1629306443-executar->1629306443-múltipla seleção->1629306443-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-controle-dof-aquisicao`,`1629306443-executar`,`1629306443-múltipla seleção`,`1629306443-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-controle-dof-aquisicao"]`);
      cy.clickIfExist(`[data-cy="1629306443-executar"]`);
      cy.clickIfExist(`[data-cy="1629306443-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1629306443-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/totalizacao-saidas->633719453-executar->633719453-múltipla seleção->633719453-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/totalizacao-saidas`,`633719453-executar`,`633719453-múltipla seleção`,`633719453-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/totalizacao-saidas"]`);
      cy.clickIfExist(`[data-cy="633719453-executar"]`);
      cy.clickIfExist(`[data-cy="633719453-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="633719453-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-executar->2814741288-múltipla seleção->2814741288-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-executar`,`2814741288-múltipla seleção`,`2814741288-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-executar"]`);
      cy.clickIfExist(`[data-cy="2814741288-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2814741288-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-creditos->2814741288-abrir visualização->2814741288-expandir->2814741288-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-creditos`,`2814741288-abrir visualização`,`2814741288-expandir`,`2814741288-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="2814741288-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2814741288-expandir"]`);
      cy.clickIfExist(`[data-cy="2814741288-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-executar->298565727-múltipla seleção->298565727-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-executar`,`298565727-múltipla seleção`,`298565727-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-executar"]`);
      cy.clickIfExist(`[data-cy="298565727-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="298565727-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apropriacao-credito-extemporaneos->298565727-abrir visualização->298565727-expandir->298565727-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/apropriacao-credito-extemporaneos`,`298565727-abrir visualização`,`298565727-expandir`,`298565727-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apropriacao-credito-extemporaneos"]`);
      cy.clickIfExist(`[data-cy="298565727-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="298565727-expandir"]`);
      cy.clickIfExist(`[data-cy="298565727-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/atualizacao-indicador->2224043782-executar->2224043782-múltipla seleção->2224043782-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/atualizacao-indicador`,`2224043782-executar`,`2224043782-múltipla seleção`,`2224043782-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2224043782-executar"]`);
      cy.clickIfExist(`[data-cy="2224043782-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2224043782-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-op-saida->3183959863-executar->3183959863-múltipla seleção->3183959863-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/exclusao-op-saida`,`3183959863-executar`,`3183959863-múltipla seleção`,`3183959863-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/exclusao-op-saida"]`);
      cy.clickIfExist(`[data-cy="3183959863-executar"]`);
      cy.clickIfExist(`[data-cy="3183959863-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3183959863-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-executar->985668109-múltipla seleção->985668109-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-executar`,`985668109-múltipla seleção`,`985668109-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-executar"]`);
      cy.clickIfExist(`[data-cy="985668109-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="985668109-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/suspencao-retomada-creditos->985668109-abrir visualização->985668109-expandir->985668109-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/suspencao-retomada-creditos`,`985668109-abrir visualização`,`985668109-expandir`,`985668109-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/suspencao-retomada-creditos"]`);
      cy.clickIfExist(`[data-cy="985668109-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="985668109-expandir"]`);
      cy.clickIfExist(`[data-cy="985668109-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/transferencia-inc-societaria->1642007593-executar->1642007593-múltipla seleção->1642007593-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/transferencia-inc-societaria`,`1642007593-executar`,`1642007593-múltipla seleção`,`1642007593-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/transferencia-inc-societaria"]`);
      cy.clickIfExist(`[data-cy="1642007593-executar"]`);
      cy.clickIfExist(`[data-cy="1642007593-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1642007593-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controles-ciap->2680178634-executar->2680178634-múltipla seleção->2680178634-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controles-ciap`,`2680178634-executar`,`2680178634-múltipla seleção`,`2680178634-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controles-ciap"]`);
      cy.clickIfExist(`[data-cy="2680178634-executar"]`);
      cy.clickIfExist(`[data-cy="2680178634-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2680178634-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-curto-longo-prazo->172111467-executar->172111467-múltipla seleção->172111467-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-curto-longo-prazo`,`172111467-executar`,`172111467-múltipla seleção`,`172111467-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="172111467-executar"]`);
      cy.clickIfExist(`[data-cy="172111467-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="172111467-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/creditos-parcelados-periodo->4272249838-executar->4272249838-múltipla seleção->4272249838-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/creditos-parcelados-periodo`,`4272249838-executar`,`4272249838-múltipla seleção`,`4272249838-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/creditos-parcelados-periodo"]`);
      cy.clickIfExist(`[data-cy="4272249838-executar"]`);
      cy.clickIfExist(`[data-cy="4272249838-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4272249838-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/diferencas-arredondamento->532328817-executar->532328817-múltipla seleção->532328817-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/diferencas-arredondamento`,`532328817-executar`,`532328817-múltipla seleção`,`532328817-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/diferencas-arredondamento"]`);
      cy.clickIfExist(`[data-cy="532328817-executar"]`);
      cy.clickIfExist(`[data-cy="532328817-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="532328817-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/operacoes-de-saida->3190201375-executar->3190201375-múltipla seleção->3190201375-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/operacoes-de-saida`,`3190201375-executar`,`3190201375-múltipla seleção`,`3190201375-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/operacoes-de-saida"]`);
      cy.clickIfExist(`[data-cy="3190201375-executar"]`);
      cy.clickIfExist(`[data-cy="3190201375-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3190201375-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-executar->1912915204-múltipla seleção->1912915204-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-executar`,`1912915204-múltipla seleção`,`1912915204-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-executar"]`);
      cy.clickIfExist(`[data-cy="1912915204-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1912915204-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/val-creditos-icms-controle->1912915204-abrir visualização->1912915204-expandir->1912915204-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/val-creditos-icms-controle`,`1912915204-abrir visualização`,`1912915204-expandir`,`1912915204-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/val-creditos-icms-controle"]`);
      cy.clickIfExist(`[data-cy="1912915204-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1912915204-expandir"]`);
      cy.clickIfExist(`[data-cy="1912915204-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-executar->1868435819-múltipla seleção->1868435819-cancelar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-executar`,`1868435819-múltipla seleção`,`1868435819-cancelar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-executar"]`);
      cy.clickIfExist(`[data-cy="1868435819-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1868435819-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/cred-icms-tomados-priod->1868435819-abrir visualização->1868435819-expandir->1868435819-diminuir`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/cred-icms-tomados-priod`,`1868435819-abrir visualização`,`1868435819-expandir`,`1868435819-diminuir`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/cred-icms-tomados-priod"]`);
      cy.clickIfExist(`[data-cy="1868435819-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1868435819-expandir"]`);
      cy.clickIfExist(`[data-cy="1868435819-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc->1437611396-executar->1437611396-múltipla seleção->1437611396-cancelar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc`,`1437611396-executar`,`1437611396-múltipla seleção`,`1437611396-cancelar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc"]`);
      cy.clickIfExist(`[data-cy="1437611396-executar"]`);
      cy.clickIfExist(`[data-cy="1437611396-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1437611396-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-mg->2762236547-executar->2762236547-múltipla seleção->2762236547-cancelar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-mg`,`2762236547-executar`,`2762236547-múltipla seleção`,`2762236547-cancelar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-mg"]`);
      cy.clickIfExist(`[data-cy="2762236547-executar"]`);
      cy.clickIfExist(`[data-cy="2762236547-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2762236547-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modc-rj->2762236705-executar->2762236705-múltipla seleção->2762236705-cancelar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modc-rj`,`2762236705-executar`,`2762236705-múltipla seleção`,`2762236705-cancelar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modc-rj"]`);
      cy.clickIfExist(`[data-cy="2762236705-executar"]`);
      cy.clickIfExist(`[data-cy="2762236705-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2762236705-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element informes-fiscais->informes-fiscais/contr-cred-modd->1437611397-executar->1437611397-múltipla seleção->1437611397-cancelar`, () => {
const actualId = [`root`,`informes-fiscais`,`informes-fiscais/contr-cred-modd`,`1437611397-executar`,`1437611397-múltipla seleção`,`1437611397-cancelar`];
    cy.clickIfExist(`[data-cy="informes-fiscais"]`);
      cy.clickIfExist(`[data-cy="informes-fiscais/contr-cred-modd"]`);
      cy.clickIfExist(`[data-cy="1437611397-executar"]`);
      cy.clickIfExist(`[data-cy="1437611397-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1437611397-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-voltar às obrigações do módulo->829857080-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-voltar às obrigações do módulo`,`829857080-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-voltar às obrigações do módulo"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-nova solicitação->829857080-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-nova solicitação`,`829857080-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="829857080-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-nova solicitação->829857080-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-nova solicitação`,`829857080-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="829857080-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-agendamentos->1017963809-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-agendamentos`,`1017963809-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1017963809-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-agendamentos->1017963809-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-agendamentos`,`1017963809-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar->1017963809-dados disponíveis para impressão->1017963809-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`,`1017963809-dados disponíveis para impressão`,`1017963809-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.clickIfExist(`[data-cy="1017963809-dados disponíveis para impressão"]`);
      cy.clickIfExist(`[data-cy="1017963809-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar->1017963809-dados disponíveis para impressão->1017963809-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`,`1017963809-dados disponíveis para impressão`,`1017963809-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.clickIfExist(`[data-cy="1017963809-dados disponíveis para impressão"]`);
      cy.clickIfExist(`[data-cy="1017963809-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar->1017963809-dados disponíveis para impressão->1017963809-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`,`1017963809-dados disponíveis para impressão`,`1017963809-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.clickIfExist(`[data-cy="1017963809-dados disponíveis para impressão"]`);
      cy.clickIfExist(`[data-cy="1017963809-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar->1017963809-dados disponíveis para impressão->1017963809-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`,`1017963809-dados disponíveis para impressão`,`1017963809-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.clickIfExist(`[data-cy="1017963809-dados disponíveis para impressão"]`);
      cy.clickIfExist(`[data-cy="1017963809-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar->1614615400-button->1614615400-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`,`1614615400-button`,`1614615400-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.clickIfExist(`[data-cy="1614615400-button"]`);
      cy.clickIfExist(`[data-cy="1614615400-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar->1614615400-button->1614615400-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`,`1614615400-button`,`1614615400-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.clickIfExist(`[data-cy="1614615400-button"]`);
      cy.clickIfExist(`[data-cy="1614615400-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar->1614615400-button->1614615400-expandir`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`,`1614615400-button`,`1614615400-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.clickIfExist(`[data-cy="1614615400-button"]`);
      cy.clickIfExist(`[data-cy="1614615400-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar->1614615400-button->1614615400-download`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`,`1614615400-button`,`1614615400-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.clickIfExist(`[data-cy="1614615400-button"]`);
      cy.clickIfExist(`[data-cy="1614615400-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/solicitacoes->829857080-ir para todas as obrigações->829857080-agendamentos->1017963809-visualização->1017963809-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`829857080-ir para todas as obrigações`,`829857080-agendamentos`,`1017963809-visualização`,`1017963809-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="829857080-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="829857080-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1017963809-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1017963809-visualizar->1017963809-dados disponíveis para impressão->1017963809-expandir->1017963809-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1017963809-visualizar`,`1017963809-dados disponíveis para impressão`,`1017963809-expandir`,`1017963809-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1017963809-visualizar"]`);
      cy.clickIfExist(`[data-cy="1017963809-dados disponíveis para impressão"]`);
      cy.clickIfExist(`[data-cy="1017963809-expandir"]`);
      cy.clickIfExist(`[data-cy="1017963809-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/geracao-controles->1614615400-agendamentos->1614615400-visualizar->1614615400-button->1614615400-expandir->1614615400-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/geracao-controles`,`1614615400-agendamentos`,`1614615400-visualizar`,`1614615400-button`,`1614615400-expandir`,`1614615400-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1614615400-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1614615400-visualizar"]`);
      cy.clickIfExist(`[data-cy="1614615400-button"]`);
      cy.clickIfExist(`[data-cy="1614615400-expandir"]`);
      cy.clickIfExist(`[data-cy="1614615400-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
