describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/mercadoria-correlacionada`, () => {
    cy.clickCauseExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickCauseExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickCauseExist(`[data-cy="tabelas-corporativas/mercadorias/mercadoria-correlacionada"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/servicos-prestados->2209141131-agendamentos->2209141131-voltar`, () => {
    cy.visit('http://system-A5/relatorios/informacoes-usuario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53618349D%7C%7C53618349&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2209141131-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-parametros-dof->2279056711-agendamentos->2279056711-voltar`, () => {
    cy.visit('http://system-A5/relatorios/criticas-parametros-dof?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210434D%7C%7C210434&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2279056711-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/relatorio-coleta->350188562-agendamentos->350188562-voltar`, () => {
    cy.visit('http://system-A5/relatorios/relatorio-coleta?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~55845943D%7C%7C55845943&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="350188562-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/atua-ind-escrituracao->3167149836-agendamentos->3167149836-voltar`, () => {
    cy.visit('http://system-A5/processos/atua-ind-escrituracao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~4637163D%7C%7C4637163&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3167149836-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/natureza-rendimento->4012914919-visualizar/editar->1636556969-salvar`, () => {
    cy.visit('http://system-A5/natureza-rendimento/editar/10001?');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1636556969-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/natureza-rendimento->4012914919-visualizar/editar->1636556969-input-codigo-1636556969-input-descricao-1636556969-checkbox-indIrrf-1636556969-checkbox-indPcc-1636556969-checkbox-indPis-1636556969-checkbox-indCofins-1636556969-checkbox-indCsll-1636556969-checkbox-indIrrfCompAgreg-1636556969-checkbox-indPisCompAgreg-1636556969-checkbox-indCofinsCompAgreg-1636556969-checkbox-indCsllCompAgreg-1636556969-checkbox-indFci-1636556969-checkbox-indDecimoTerceiro-1636556969-checkbox-indRra-1636556969-checkbox-indPfBr-1636556969-checkbox-indPjBr-1636556969-checkbox-indPfExt-1636556969-checkbox-indPjExt and submit`, () => {
    cy.visit('http://system-A5/natureza-rendimento');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4012914919-visualizar/editar"]`);
    cy.fillInput(`[data-cy="1636556969-input-codigo"] textarea`, `Alagoas`);
    cy.fillInput(`[data-cy="1636556969-input-descricao"] textarea`, `Plastic`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indIrrf"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPcc"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPis"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indCofins"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indCsll"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indIrrfCompAgreg"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPisCompAgreg"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indCofinsCompAgreg"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indCsllCompAgreg"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indFci"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indDecimoTerceiro"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indRra"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPfBr"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPjBr"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPfExt"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1636556969-checkbox-indPjExt"] textarea`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/servicos->tabelas-oficiais/servicos/clesocial->471987860-eyeoutlined->3189687516-salvar`, () => {
    cy.visit('http://system-A5/servicos/classificacao-servicos/editar/100000001');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3189687516-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento->333178532-novo->2991104197-button`, () => {
    cy.visit('http://system-A5/info-mercadoria-estabelecimento/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2991104197-button"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-visualizar/editar->1371913325-salvar`, () => {
    cy.visit('http://system-A5/administracao-sistema/usuario/editar/AAQUINO1');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1371913325-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/conectores->425611521-novo->1315645576-salvar`, () => {
    cy.visit('http://system-A5/administracao-sistema/conectores/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1315645576-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/limpeza-ger-prc->1299372877-agendamentos->1299372877-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/limpeza-ger-prc?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210275D%7C%7C210275&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1299372877-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/limpeza-tabelas-temp->3095808654-agendamentos->3095808654-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/limpeza-tabelas-temp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210274D%7C%7C210274&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3095808654-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/leiaute-rep->741711499-agendamentos->741711499-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/leiaute-rep?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210294D%7C%7C210294&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="741711499-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/leiaute-tabelas->641775388-agendamentos->641775388-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/leiaute-tabelas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210295D%7C%7C210295&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="641775388-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/carga-tabelas-views->1794544182-agendamentos->1794544182-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/carga-tabelas-views?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210297D%7C%7C210297&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1794544182-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/carga-sinonimos->4098462950-agendamentos->4098462950-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/carga-sinonimos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210298D%7C%7C210298&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4098462950-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/carga-relacionamentos->3002811261-agendamentos->3002811261-voltar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/carga-relacionamentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210299D%7C%7C210299&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3002811261-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/carga-relacionamentos->3002811261-agendamentos->3002811261-visualizar`, () => {
    cy.visit('http://system-A5/adm-sis/processos/carga-relacionamentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210299D%7C%7C210299&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3002811261-visualizar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/carga-relacionamentos->3002811261-agendamentos->3002811261-excluir`, () => {
    cy.visit('http://system-A5/adm-sis/processos/carga-relacionamentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210299D%7C%7C210299&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3002811261-excluir"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/recage->2589678041-calendaroutlined->3830953399-novo->1380074898-salvar`, () => {
    cy.visit('http://system-A5/parametros/codigo-receita/teste/agenda/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1380074898-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/natureza-rendimento->4012914919-visualizar/editar->1636556969-novo->1636556969-powerselect-indIsencaoDeducao and submit`, () => {
    cy.visit('http://system-A5/natureza-rendimento/editar/10001?');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1636556969-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1636556969-powerselect-indIsencaoDeducao"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/natureza-rendimento->4012914919-visualizar/editar->1636556969-visualizar/editar->1636556969-powerselect-codigo and submit`, () => {
    cy.visit('http://system-A5/natureza-rendimento/editar/10001?');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1636556969-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1636556969-powerselect-codigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-visualizar/editar->1371913325-desativar usuário->1371913325-desativar`, () => {
    cy.visit('http://system-A5/administracao-sistema/usuario/editar/AAQUINO1');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1371913325-desativar usuário"]`);
    cy.clickCauseExist(`[data-cy="1371913325-desativar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-moreoutlined->3056648786-item-integração oracle->3056648786-salvar`, () => {
    cy.visit('http://system-A5/administracao-sistema/usuario');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3056648786-moreoutlined"]`);
    cy.clickCauseExist(`[data-cy="3056648786-item-integração oracle"]`);
    cy.clickCauseExist(`[data-cy="3056648786-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-moreoutlined->3056648786-item-integração oracle->3056648786-input-dbUsuario and submit`, () => {
    cy.visit('http://system-A5/administracao-sistema/usuario');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3056648786-moreoutlined"]`);
    cy.clickCauseExist(`[data-cy="3056648786-item-integração oracle"]`);
    cy.fillInput(`[data-cy="3056648786-input-dbUsuario"] textarea`, `SMTP`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/iss->tabelas-oficiais/tributacao/iss/aliquota-iss->1265236310-novo->3933498707-mais operações->3933498707-item-`, () => {
    cy.visit('http://system-A5/tributacao/aliquota-iss/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3933498707-mais operações"]`);
    cy.clickCauseExist(`[data-cy="3933498707-item-"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/manutencao-oracle-cloud-fusion->adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-emissao-dof->adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-emissao-dof/dados-conciliacao-emissao->3409591181-agendamentos->3409591181-voltar`, () => {
    cy.visit(
      'http://system-A5/adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-emissao-dof/dados-conciliacao-emissao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54527665D%7C%7C54527665&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3409591181-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/manutencao-oracle-cloud-fusion->adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-escrituracao-dof->adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-escrituracao-dof/definir-tipo-rotina-escrituracao->730724852-agendamentos->730724852-voltar`, () => {
    cy.visit(
      'http://system-A5/adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-escrituracao-dof/definir-tipo-rotina-escrituracao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~51156957D%7C%7C51156957&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="730724852-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/processos->adm-sis/processos/manutencao-oracle-cloud-fusion->adm-sis/processos/manutencao-oracle-cloud-fusion/administracao-geral->adm-sis/processos/manutencao-oracle-cloud-fusion/manutencao-escrituracao-dof/quantidade-jobs-oracle->1295544714-agendamentos->1295544714-voltar`, () => {
    cy.visit(
      'http://system-A5/adm-sis/processos/manutencao-oracle-cloud-fusion/administracao-geral/quantidade-jobs-oracle?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54527673D%7C%7C54527673&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1295544714-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
