describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/mercadoria-correlacionada`, () => {
    cy.clickCauseExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickCauseExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickCauseExist(`[data-cy="tabelas-corporativas/mercadorias/mercadoria-correlacionada"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/servicos->tabelas-oficiais/servicos/clesocial->471987860-eyeoutlined->3189687516-salvar`, () => {
    cy.clickCauseExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/servicos"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/servicos/clesocial"]`);
    cy.clickCauseExist(`[data-cy="471987860-eyeoutlined"]`);
    cy.clickCauseExist(`[data-cy="3189687516-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-visualizar/editar->1371913325-salvar`, () => {
    cy.clickCauseExist(`[data-cy="adm-sis"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos/usuarios"]`);
    cy.clickCauseExist(`[data-cy="3056648786-visualizar/editar"]`);
    cy.clickCauseExist(`[data-cy="1371913325-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/conectores->425611521-novo->1315645576-salvar`, () => {
    cy.clickCauseExist(`[data-cy="adm-sis"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos/conectores"]`);
    cy.clickCauseExist(`[data-cy="425611521-novo"]`);
    cy.clickCauseExist(`[data-cy="1315645576-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/recage->2589678041-calendaroutlined->3830953399-novo->1380074898-salvar`, () => {
    cy.clickCauseExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/fiscais"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/fiscais/recage"]`);
    cy.clickCauseExist(`[data-cy="2589678041-calendaroutlined"]`);
    cy.clickCauseExist(`[data-cy="3830953399-novo"]`);
    cy.clickCauseExist(`[data-cy="1380074898-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-visualizar/editar->1371913325-desativar usuário->1371913325-desativar`, () => {
    cy.clickCauseExist(`[data-cy="adm-sis"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos/usuarios"]`);
    cy.clickCauseExist(`[data-cy="3056648786-visualizar/editar"]`);
    cy.clickCauseExist(`[data-cy="1371913325-desativar usuário"]`);
    cy.clickCauseExist(`[data-cy="1371913325-desativar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-moreoutlined->3056648786-item-integração oracle->3056648786-salvar`, () => {
    cy.clickCauseExist(`[data-cy="adm-sis"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos/usuarios"]`);
    cy.clickCauseExist(`[data-cy="3056648786-moreoutlined"]`);
    cy.clickCauseExist(`[data-cy="3056648786-item-integração oracle"]`);
    cy.clickCauseExist(`[data-cy="3056648786-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values adm-sis->adm-sis/acessos->adm-sis/acessos/usuarios->3056648786-moreoutlined->3056648786-item-integração oracle->3056648786-input-dbUsuario and submit`, () => {
    cy.clickCauseExist(`[data-cy="adm-sis"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos"]`);
    cy.clickCauseExist(`[data-cy="adm-sis/acessos/usuarios"]`);
    cy.clickCauseExist(`[data-cy="3056648786-moreoutlined"]`);
    cy.clickCauseExist(`[data-cy="3056648786-item-integração oracle"]`);
    cy.fillInput(`[data-cy="3056648786-input-dbUsuario"] textarea`, `Distrito Federal`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/iss->tabelas-oficiais/tributacao/iss/aliquota-iss->1265236310-novo->3933498707-mais operações->3933498707-item-`, () => {
    cy.clickCauseExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/tributacao"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/tributacao/iss"]`);
    cy.clickCauseExist(`[data-cy="tabelas-oficiais/tributacao/iss/aliquota-iss"]`);
    cy.clickCauseExist(`[data-cy="1265236310-novo"]`);
    cy.clickCauseExist(`[data-cy="3933498707-mais operações"]`);
    cy.clickCauseExist(`[data-cy="3933498707-item-"]`);
    cy.checkErrorsWereDetected();
  });
});
