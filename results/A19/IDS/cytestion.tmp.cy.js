describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-gerais`, () => {
const actualId = [`root`,`parametros-gerais`];
    cy.clickIfExist(`[data-cy="parametros-gerais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos`, () => {
const actualId = [`root`,`lancamentos`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes`, () => {
const actualId = [`root`,`operacoes`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
const actualId = [`root`,`downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-gerais->parametros-gerais/codigos-receita-agenda-recolhimento`, () => {
const actualId = [`root`,`parametros-gerais`,`parametros-gerais/codigos-receita-agenda-recolhimento`];
    cy.clickIfExist(`[data-cy="parametros-gerais"]`);
      cy.clickIfExist(`[data-cy="parametros-gerais/codigos-receita-agenda-recolhimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-analiticos`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-analiticos`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-analiticos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-consolidados`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-consolidados`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-consolidados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-creditos`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-creditos`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/conciliacao-pagamento-darf`, () => {
const actualId = [`root`,`operacoes`,`operacoes/conciliacao-pagamento-darf`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/conciliacao-pagamento-darf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/importacao-debitos-creditos`, () => {
const actualId = [`root`,`operacoes`,`operacoes/importacao-debitos-creditos`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/importacao-debitos-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->421757132-power-search-button`, () => {
const actualId = [`root`,`downloads`,`421757132-power-search-button`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="421757132-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->421757132-download`, () => {
const actualId = [`root`,`downloads`,`421757132-download`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="421757132-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->421757132-detalhes`, () => {
const actualId = [`root`,`downloads`,`421757132-detalhes`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="421757132-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->421757132-excluir`, () => {
const actualId = [`root`,`downloads`,`421757132-excluir`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="421757132-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-analiticos->741543100-novo`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-analiticos`,`741543100-novo`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-analiticos"]`);
      cy.clickIfExist(`[data-cy="741543100-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-analiticos->741543100-power-search-button`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-analiticos`,`741543100-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-analiticos"]`);
      cy.clickIfExist(`[data-cy="741543100-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-consolidados->144024936-agenda`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-consolidados`,`144024936-agenda`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="144024936-agenda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-consolidados->144024936-power-search-button`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-consolidados`,`144024936-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="144024936-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-creditos->3993712770-novo`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-creditos`,`3993712770-novo`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-creditos"]`);
      cy.clickIfExist(`[data-cy="3993712770-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-creditos->3993712770-power-search-button`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-creditos`,`3993712770-power-search-button`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-creditos"]`);
      cy.clickIfExist(`[data-cy="3993712770-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/conciliacao-pagamento-darf->92414607-power-search-button`, () => {
const actualId = [`root`,`operacoes`,`operacoes/conciliacao-pagamento-darf`,`92414607-power-search-button`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/conciliacao-pagamento-darf"]`);
      cy.clickIfExist(`[data-cy="92414607-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element operacoes->operacoes/importacao-debitos-creditos->1315386227-pesquisar`, () => {
const actualId = [`root`,`operacoes`,`operacoes/importacao-debitos-creditos`,`1315386227-pesquisar`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/importacao-debitos-creditos"]`);
      cy.clickIfExist(`[data-cy="1315386227-pesquisar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values operacoes->operacoes/importacao-debitos-creditos->1315386227-powerselect-estCodigo-1315386227-powerselect-origDebCred-1315386227-powerselect-codReceita-1315386227-powerselect-grisEstab-1315386227-powerselect-indManAuto and submit`, () => {
const actualId = [`root`,`operacoes`,`operacoes/importacao-debitos-creditos`,`1315386227-powerselect-estCodigo-1315386227-powerselect-origDebCred-1315386227-powerselect-codReceita-1315386227-powerselect-grisEstab-1315386227-powerselect-indManAuto`];
    cy.clickIfExist(`[data-cy="operacoes"]`);
      cy.clickIfExist(`[data-cy="operacoes/importacao-debitos-creditos"]`);
      cy.fillInputPowerSelect(`[data-cy="1315386227-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1315386227-powerselect-origDebCred"] input`);
cy.fillInputPowerSelect(`[data-cy="1315386227-powerselect-codReceita"] input`);
cy.fillInputPowerSelect(`[data-cy="1315386227-powerselect-grisEstab"] input`);
cy.fillInputPowerSelect(`[data-cy="1315386227-powerselect-indManAuto"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-gerar obrigação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-gerar obrigação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-gerar obrigação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-visualizar resultado da geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-visualizar resultado da geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-visualizar resultado da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-protocolo transmissão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-protocolo transmissão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-protocolo transmissão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-excluir geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-excluir geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-excluir geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->829132674-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`829132674-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="829132674-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->829132674-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`829132674-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="829132674-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->829132674-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`829132674-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="829132674-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-executar`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-visualização`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-analiticos->741543100-novo->963479981-salvar`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-analiticos`,`741543100-novo`,`963479981-salvar`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-analiticos"]`);
      cy.clickIfExist(`[data-cy="741543100-novo"]`);
      cy.clickIfExist(`[data-cy="963479981-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-analiticos->741543100-novo->963479981-voltar`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-analiticos`,`741543100-novo`,`963479981-voltar`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-analiticos"]`);
      cy.clickIfExist(`[data-cy="741543100-novo"]`);
      cy.clickIfExist(`[data-cy="963479981-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamentos->lancamentos/lancamento-debitos-analiticos->741543100-novo->963479981-powerselect-estCodigo-963479981-powerselect-ccriCodigo-963479981-input-tributo-963479981-input-descricao-963479981-input-monetary-vlDebito and submit`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-analiticos`,`741543100-novo`,`963479981-powerselect-estCodigo-963479981-powerselect-ccriCodigo-963479981-input-tributo-963479981-input-descricao-963479981-input-monetary-vlDebito`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-analiticos"]`);
      cy.clickIfExist(`[data-cy="741543100-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="963479981-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="963479981-powerselect-ccriCodigo"] input`);
cy.fillInput(`[data-cy="963479981-input-tributo"] textarea`, `panel`);
cy.fillInput(`[data-cy="963479981-input-descricao"] textarea`, `circuit`);
cy.fillInput(`[data-cy="963479981-input-monetary-vlDebito"] textarea`, `9,92`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-debitos-consolidados->144024936-agenda->144024936-pesquisar`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-consolidados`,`144024936-agenda`,`144024936-pesquisar`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="144024936-agenda"]`);
      cy.clickIfExist(`[data-cy="144024936-pesquisar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values lancamentos->lancamentos/lancamento-debitos-consolidados->144024936-agenda->144024936-powerselect-estCodigo and submit`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-debitos-consolidados`,`144024936-agenda`,`144024936-powerselect-estCodigo`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="144024936-agenda"]`);
      cy.fillInputPowerSelect(`[data-cy="144024936-powerselect-estCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element lancamentos->lancamentos/lancamento-creditos->3993712770-novo->3993712770-item-`, () => {
const actualId = [`root`,`lancamentos`,`lancamentos/lancamento-creditos`,`3993712770-novo`,`3993712770-item-`];
    cy.clickIfExist(`[data-cy="lancamentos"]`);
      cy.clickIfExist(`[data-cy="lancamentos/lancamento-creditos"]`);
      cy.clickIfExist(`[data-cy="3993712770-novo"]`);
      cy.clickIfExist(`[data-cy="3993712770-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-gerenciar labels->678895530-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-gerenciar labels`,`678895530-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="678895530-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-visualizar/editar->1585736775-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-visualizar/editar`,`1585736775-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1585736775-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->678895530-visualizar/editar->1585736775-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao`,`678895530-visualizar/editar`,`1585736775-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
      cy.clickIfExist(`[data-cy="678895530-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1585736775-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ir para todas as obrigações->3223976761-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ir para todas as obrigações`,`3223976761-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3223976761-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-nova solicitação->3223976761-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-nova solicitação`,`3223976761-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="3223976761-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-nova solicitação->3223976761-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-nova solicitação`,`3223976761-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="3223976761-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração->3230782574-rightoutlined`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`,`3230782574-rightoutlined`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="3230782574-rightoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração->3230782574-habilitar edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`,`3230782574-habilitar edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="3230782574-habilitar edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração->3230782574-abrir janela de edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`,`3230782574-abrir janela de edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="3230782574-abrir janela de edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-visualizar resultado da geração->3223976761-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-visualizar resultado da geração`,`3223976761-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="3223976761-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-visualizar resultado da geração->3223976761-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-visualizar resultado da geração`,`3223976761-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="3223976761-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-visualizar resultado da geração->3223976761-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-visualizar resultado da geração`,`3223976761-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="3223976761-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-visualizar resultado da geração->3223976761-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-visualizar resultado da geração`,`3223976761-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="3223976761-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-protocolo transmissão->569283666-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-protocolo transmissão`,`569283666-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="569283666-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-visualização->3371290466-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-visualização`,`3371290466-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3371290466-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-abrir visualização->3371290466-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-abrir visualização`,`3371290466-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-abrir visualização->3371290466-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-abrir visualização`,`3371290466-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-abrir visualização->3371290466-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-abrir visualização`,`3371290466-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-abrir visualização->3371290466-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-abrir visualização`,`3371290466-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-visualizar->3371290466-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-visualizar`,`3371290466-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualizar"]`);
      cy.clickIfExist(`[data-cy="3371290466-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-novo->2148651226-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-novo`,`2148651226-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-novo"]`);
      cy.clickIfExist(`[data-cy="2148651226-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-novo->2148651226-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-novo`,`2148651226-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-novo"]`);
      cy.clickIfExist(`[data-cy="2148651226-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->2148651226-novo->2148651226-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-novo`,`2148651226-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-novo"]`);
      cy.fillInput(`[data-cy="2148651226-input-number-ano"] textarea`, `2`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-editar->2148651226-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-editar`,`2148651226-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-editar"]`);
      cy.clickIfExist(`[data-cy="2148651226-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2148651226-editar->2148651226-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2148651226-editar`,`2148651226-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2148651226-editar"]`);
      cy.clickIfExist(`[data-cy="2148651226-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->829132674-novo->829132674-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`829132674-novo`,`829132674-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="829132674-novo"]`);
      cy.clickIfExist(`[data-cy="829132674-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-executar->4010788594-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-executar`,`4010788594-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-executar"]`);
      cy.clickIfExist(`[data-cy="4010788594-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-executar->4010788594-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-executar`,`4010788594-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-executar"]`);
      cy.clickIfExist(`[data-cy="4010788594-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-agendamentos->4010788594-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-agendamentos`,`4010788594-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4010788594-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/agenda-recolhimento->4010788594-visualização->4010788594-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-visualização`,`4010788594-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4010788594-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-executar->1436609750-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-executar`,`1436609750-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-executar"]`);
      cy.clickIfExist(`[data-cy="1436609750-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-executar->1436609750-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-executar`,`1436609750-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-executar"]`);
      cy.clickIfExist(`[data-cy="1436609750-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-agendamentos->1436609750-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-agendamentos`,`1436609750-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1436609750-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/pagamento-multa-juros->1436609750-visualização->1436609750-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-visualização`,`1436609750-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1436609750-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-executar->2549579271-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-executar`,`2549579271-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-executar"]`);
      cy.clickIfExist(`[data-cy="2549579271-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-executar->2549579271-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-executar`,`2549579271-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-executar"]`);
      cy.clickIfExist(`[data-cy="2549579271-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-agendamentos->2549579271-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-agendamentos`,`2549579271-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2549579271-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-visualização->2549579271-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-visualização`,`2549579271-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2549579271-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-executar->772074419-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-executar`,`772074419-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-executar"]`);
      cy.clickIfExist(`[data-cy="772074419-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-executar->772074419-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-executar`,`772074419-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-executar"]`);
      cy.clickIfExist(`[data-cy="772074419-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-agendamentos->772074419-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-agendamentos`,`772074419-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-agendamentos"]`);
      cy.clickIfExist(`[data-cy="772074419-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-visualização->772074419-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-visualização`,`772074419-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="772074419-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-detalhes->772074419-não há dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-detalhes`,`772074419-não há dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-detalhes"]`);
      cy.clickIfExist(`[data-cy="772074419-não há dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-abrir visualização->772074419-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-abrir visualização`,`772074419-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="772074419-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-abrir visualização->772074419-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-abrir visualização`,`772074419-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="772074419-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-abrir visualização->772074419-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-abrir visualização`,`772074419-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="772074419-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-abrir visualização->772074419-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-abrir visualização`,`772074419-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="772074419-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-executar->2580041027-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-executar`,`2580041027-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-executar"]`);
      cy.clickIfExist(`[data-cy="2580041027-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-executar->2580041027-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-executar`,`2580041027-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-executar"]`);
      cy.clickIfExist(`[data-cy="2580041027-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-agendamentos->2580041027-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-agendamentos`,`2580041027-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2580041027-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lancamentos-de-creditos->2580041027-visualização->2580041027-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-visualização`,`2580041027-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2580041027-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-executar->3415243707-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-executar`,`3415243707-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-executar"]`);
      cy.clickIfExist(`[data-cy="3415243707-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-executar->3415243707-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-executar`,`3415243707-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-executar"]`);
      cy.clickIfExist(`[data-cy="3415243707-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-agendamentos->3415243707-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-agendamentos`,`3415243707-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3415243707-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-visualização->3415243707-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-visualização`,`3415243707-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3415243707-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-executar->4238912785-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-executar`,`4238912785-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-executar"]`);
      cy.clickIfExist(`[data-cy="4238912785-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-executar->4238912785-agendar`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-executar`,`4238912785-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-executar"]`);
      cy.clickIfExist(`[data-cy="4238912785-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-agendamentos->4238912785-voltar`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-agendamentos`,`4238912785-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4238912785-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/consolidacao-lancamentos-de-debitos->4238912785-visualização->4238912785-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-visualização`,`4238912785-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4238912785-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-visualização->3371290466-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-visualização`,`3371290466-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3371290466-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-abrir visualização->3371290466-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-abrir visualização`,`3371290466-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-abrir visualização->3371290466-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-abrir visualização`,`3371290466-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-abrir visualização->3371290466-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-abrir visualização`,`3371290466-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-abrir visualização->3371290466-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-abrir visualização`,`3371290466-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-visualizar->3371290466-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-visualizar`,`3371290466-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-visualizar"]`);
      cy.clickIfExist(`[data-cy="3371290466-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração->3230782574-rightoutlined->3230782574-downoutlined`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`,`3230782574-rightoutlined`,`3230782574-downoutlined`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="3230782574-rightoutlined"]`);
      cy.clickIfExist(`[data-cy="3230782574-downoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração->3230782574-habilitar edição->3230782574-confirmar  edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`,`3230782574-habilitar edição`,`3230782574-confirmar  edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="3230782574-habilitar edição"]`);
      cy.clickIfExist(`[data-cy="3230782574-confirmar  edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-ajustar parâmetros da geração->3230782574-abrir janela de edição->3230782574-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-ajustar parâmetros da geração`,`3230782574-abrir janela de edição`,`3230782574-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="3230782574-abrir janela de edição"]`);
      cy.clickIfExist(`[data-cy="3230782574-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-visualizar resultado da geração->3223976761-expandir->3223976761-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-visualizar resultado da geração`,`3223976761-expandir`,`3223976761-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="3223976761-expandir"]`);
      cy.clickIfExist(`[data-cy="3223976761-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-protocolo transmissão->569283666-editar->569283666-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-protocolo transmissão`,`569283666-editar`,`569283666-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="569283666-editar"]`);
      cy.clickIfExist(`[data-cy="569283666-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-protocolo transmissão->569283666-editar->569283666-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-protocolo transmissão`,`569283666-editar`,`569283666-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="569283666-editar"]`);
      cy.clickIfExist(`[data-cy="569283666-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3371290466-abrir visualização->3371290466-expandir->3371290466-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3371290466-abrir visualização`,`3371290466-expandir`,`3371290466-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-expandir"]`);
      cy.clickIfExist(`[data-cy="3371290466-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-executar->4010788594-múltipla seleção->4010788594-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/agenda-recolhimento`,`4010788594-executar`,`4010788594-múltipla seleção`,`4010788594-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/agenda-recolhimento"]`);
      cy.clickIfExist(`[data-cy="4010788594-executar"]`);
      cy.clickIfExist(`[data-cy="4010788594-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4010788594-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-executar->1436609750-múltipla seleção->1436609750-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/pagamento-multa-juros`,`1436609750-executar`,`1436609750-múltipla seleção`,`1436609750-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/pagamento-multa-juros"]`);
      cy.clickIfExist(`[data-cy="1436609750-executar"]`);
      cy.clickIfExist(`[data-cy="1436609750-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1436609750-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-executar->2549579271-múltipla seleção->2549579271-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem`,`2549579271-executar`,`2549579271-múltipla seleção`,`2549579271-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem"]`);
      cy.clickIfExist(`[data-cy="2549579271-executar"]`);
      cy.clickIfExist(`[data-cy="2549579271-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2549579271-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-executar->772074419-múltipla seleção->772074419-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-executar`,`772074419-múltipla seleção`,`772074419-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-executar"]`);
      cy.clickIfExist(`[data-cy="772074419-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="772074419-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-abrir visualização->772074419-expandir->772074419-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-debitos-consolidados`,`772074419-abrir visualização`,`772074419-expandir`,`772074419-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-debitos-consolidados"]`);
      cy.clickIfExist(`[data-cy="772074419-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="772074419-expandir"]`);
      cy.clickIfExist(`[data-cy="772074419-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-executar->2580041027-múltipla seleção->2580041027-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos`,`2580041027-executar`,`2580041027-múltipla seleção`,`2580041027-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos"]`);
      cy.clickIfExist(`[data-cy="2580041027-executar"]`);
      cy.clickIfExist(`[data-cy="2580041027-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2580041027-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-executar->3415243707-múltipla seleção->3415243707-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso`,`3415243707-executar`,`3415243707-múltipla seleção`,`3415243707-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso"]`);
      cy.clickIfExist(`[data-cy="3415243707-executar"]`);
      cy.clickIfExist(`[data-cy="3415243707-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3415243707-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-executar->4238912785-múltipla seleção->4238912785-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/consolidacao-lancamentos-de-debitos`,`4238912785-executar`,`4238912785-múltipla seleção`,`4238912785-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/consolidacao-lancamentos-de-debitos"]`);
      cy.clickIfExist(`[data-cy="4238912785-executar"]`);
      cy.clickIfExist(`[data-cy="4238912785-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4238912785-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element obrigacoes->obrigacoes/solicitacoes->3223976761-agendamentos->3371290466-abrir visualização->3371290466-expandir->3371290466-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3223976761-agendamentos`,`3371290466-abrir visualização`,`3371290466-expandir`,`3371290466-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3223976761-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3371290466-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3371290466-expandir"]`);
      cy.clickIfExist(`[data-cy="3371290466-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
