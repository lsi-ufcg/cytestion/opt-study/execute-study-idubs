describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Visits index page`, () => {
    cy.checkErrorsWereDetected();
  });
});
