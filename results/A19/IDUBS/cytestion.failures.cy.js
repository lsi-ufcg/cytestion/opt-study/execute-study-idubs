describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element relatorios->relatorios/agenda-recolhimento->4010788594-agendamentos->4010788594-voltar`, () => {
    cy.visit('http://system-A19/relatorios/agenda-recolhimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~246112D%7C%7C246112&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4010788594-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/pagamento-multa-juros->1436609750-agendamentos->1436609750-voltar`, () => {
    cy.visit('http://system-A19/relatorios/pagamento-multa-juros?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~248138D%7C%7C248138&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1436609750-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem->2549579271-agendamentos->2549579271-voltar`, () => {
    cy.visit(
      'http://system-A19/relatorios/lancamentos-de-debitos-analiticos-por-sistema-de-origem?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~246382D%7C%7C246382&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2549579271-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/lancamentos-de-debitos-consolidados->772074419-agendamentos->772074419-voltar`, () => {
    cy.visit(
      'http://system-A19/relatorios/lancamentos-de-debitos-consolidados?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~246187D%7C%7C246187&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="772074419-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/lancamentos-de-creditos->2580041027-agendamentos->2580041027-voltar`, () => {
    cy.visit('http://system-A19/relatorios/lancamentos-de-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~246276D%7C%7C246276&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2580041027-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso->3415243707-agendamentos->3415243707-voltar`, () => {
    cy.visit(
      'http://system-A19/relatorios/lancamentos-de-creditos-do-tipo-pagamento-em-atraso?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~246446D%7C%7C246446&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3415243707-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/consolidacao-lancamentos-de-debitos->4238912785-agendamentos->4238912785-voltar`, () => {
    cy.visit(
      'http://system-A19/processos/consolidacao-lancamentos-de-debitos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245266D%7C%7C245266&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4238912785-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
