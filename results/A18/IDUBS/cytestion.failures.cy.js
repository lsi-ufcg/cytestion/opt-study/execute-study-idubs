describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-remover item`, () => {
    cy.visit('http://system-A18/A18/cfiscal-merc/editar/VVV_CAT_PAUTA_2');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1380780575-remover item"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-salvar`, () => {
    cy.visit('http://system-A18/A18/cfiscal-merc/editar/VVV_CAT_PAUTA_2');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1380780575-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-voltar`, () => {
    cy.visit('http://system-A18/A18/cfiscal-merc/editar/VVV_CAT_PAUTA_2');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1380780575-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-agendamentos->3200271866-voltar`, () => {
    cy.visit('http://system-A18/relatorios/cadastro-mercadoria?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31877509D%7C%7C31877509&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3200271866-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/posicao-estoque->568761414-agendamentos->568761414-voltar`, () => {
    cy.visit('http://system-A18/relatorios/posicao-estoque?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31877506D%7C%7C31877506&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="568761414-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-agendamentos->1811188643-voltar`, () => {
    cy.visit('http://system-A18/relatorios/controle-ressarcimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27727305D%7C%7C27727305&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1811188643-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/controle-saldo->656091685-agendamentos->656091685-voltar`, () => {
    cy.visit('http://system-A18/relatorios/controle-saldo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~45706867D%7C%7C45706867&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="656091685-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas->1799809287-agendamentos->1799809287-voltar`, () => {
    cy.visit('http://system-A18/relatorios/criticas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46510627D%7C%7C46510627&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1799809287-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-agendamentos->1625609257-voltar`, () => {
    cy.visit('http://system-A18/processos/carga-inicial-mercadorias?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27735411D%7C%7C27735411&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1625609257-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/carga-inicial-dof->2154116344-agendamentos->2154116344-voltar`, () => {
    cy.visit('http://system-A18/processos/carga-inicial-dof?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31877523D%7C%7C31877523&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2154116344-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/carga-mensal-dof->974703873-agendamentos->974703873-voltar`, () => {
    cy.visit('http://system-A18/processos/carga-mensal-dof?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27734395D%7C%7C27734395&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="974703873-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/icms-st->3570614338-agendamentos->3570614338-voltar`, () => {
    cy.visit('http://system-A18/processos/icms-st?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27734398D%7C%7C27734398&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3570614338-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/dados-sintegra-mg->3373103500-agendamentos->3373103500-voltar`, () => {
    cy.visit('http://system-A18/processos/dados-sintegra-mg?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46506529D%7C%7C46506529&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3373103500-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/apagar-dados-mercadorias->589853967-agendamentos->589853967-voltar`, () => {
    cy.visit('http://system-A18/processos/apagar-dados-mercadorias?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53843474D%7C%7C53843474&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="589853967-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/apagar-dados-recup-st->1307678592-agendamentos->1307678592-voltar`, () => {
    cy.visit('http://system-A18/processos/apagar-dados-recup-st?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27727318D%7C%7C27727318&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1307678592-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
