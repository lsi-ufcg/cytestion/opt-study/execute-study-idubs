describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização->568761414-expandir`, () => {
    cy.clickCauseExist(`[data-cy="relatorios"]`);
    cy.clickCauseExist(`[data-cy="relatorios/posicao-estoque"]`);
    cy.clickCauseExist(`[data-cy="568761414-abrir visualização"]`);
    cy.clickCauseExist(`[data-cy="568761414-expandir"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-múltipla seleção`, () => {
    cy.clickCauseExist(`[data-cy="processos"]`);
    cy.clickCauseExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
    cy.clickCauseExist(`[data-cy="589853967-executar"]`);
    cy.clickCauseExist(`[data-cy="589853967-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element recup-st->recup-st/dof-st->1171267882-visualizar/editar->85249851-novo item->85249851-salvar item`, () => {
    cy.clickCauseExist(`[data-cy="recup-st"]`);
    cy.clickCauseExist(`[data-cy="recup-st/dof-st"]`);
    cy.clickCauseExist(`[data-cy="1171267882-visualizar/editar"]`);
    cy.clickCauseExist(`[data-cy="85249851-novo item"]`);
    cy.clickCauseExist(`[data-cy="85249851-salvar item"]`);
    cy.checkErrorsWereDetected();
  });
});
