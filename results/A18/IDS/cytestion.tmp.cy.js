describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
const actualId = [`root`,`tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
const actualId = [`root`,`tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st`, () => {
const actualId = [`root`,`recup-st`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/fiscais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/mercadorias`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tributacao`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscais`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/fiscais`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/manutencao-dof`, () => {
const actualId = [`root`,`transacoes`,`transacoes/manutencao-dof`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/manutencao-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/manutencao-cupom-fiscal`, () => {
const actualId = [`root`,`transacoes`,`transacoes/manutencao-cupom-fiscal`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/manutencao-cupom-fiscal"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/lancamento-saldo-inicial`, () => {
const actualId = [`root`,`transacoes`,`transacoes/lancamento-saldo-inicial`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/lancamento-saldo-inicial"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/dof-st`, () => {
const actualId = [`root`,`recup-st`,`recup-st/dof-st`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/dof-st"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/ajuste-saldo`, () => {
const actualId = [`root`,`recup-st`,`recup-st/ajuste-saldo`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/ajuste-saldo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/dof`, () => {
const actualId = [`root`,`relatorios`,`relatorios/dof`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-baixa`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-baixa`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-baixa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->638705563-power-search-button`, () => {
const actualId = [`root`,`download`,`638705563-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="638705563-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->638705563-download`, () => {
const actualId = [`root`,`download`,`638705563-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="638705563-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->638705563-detalhes`, () => {
const actualId = [`root`,`download`,`638705563-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="638705563-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->638705563-excluir`, () => {
const actualId = [`root`,`download`,`638705563-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="638705563-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/mercadorias->tabelas-oficiais/mercadorias/ncm`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/mercadorias`,`tabelas-oficiais/mercadorias/ncm`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias/ncm"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/mercadorias->tabelas-oficiais/mercadorias/cest`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/mercadorias`,`tabelas-oficiais/mercadorias/cest`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias/cest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/mercadorias->tabelas-oficiais/mercadorias/unimed`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/mercadorias`,`tabelas-oficiais/mercadorias/unimed`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias/unimed"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/icms-padrao`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tributacao`,`tabelas-oficiais/tributacao/icms-padrao`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao/icms-padrao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/icms-especifica`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tributacao`,`tabelas-oficiais/tributacao/icms-especifica`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao/icms-especifica"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/icms-base-calculo`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tributacao`,`tabelas-oficiais/tributacao/icms-base-calculo`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao/icms-base-calculo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/mva-cest`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tributacao`,`tabelas-oficiais/tributacao/mva-cest`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao/mva-cest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tributacao->tabelas-oficiais/tributacao/mva-mer-cnae`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tributacao`,`tabelas-oficiais/tributacao/mva-mer-cnae`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tributacao/mva-mer-cnae"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscais->tabelas-corporativas/fiscais/nop`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/fiscais`,`tabelas-corporativas/fiscais/nop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais/nop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/cadastro`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`,`tabelas-corporativas/mercadorias/cadastro`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/cadastro"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/fornecedor`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`,`tabelas-corporativas/mercadorias/fornecedor`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/fornecedor"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/dof-st->1171267882-novo documento`, () => {
const actualId = [`root`,`recup-st`,`recup-st/dof-st`,`1171267882-novo documento`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/dof-st"]`);
      cy.clickIfExist(`[data-cy="1171267882-novo documento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/dof-st->1171267882-power-search-button`, () => {
const actualId = [`root`,`recup-st`,`recup-st/dof-st`,`1171267882-power-search-button`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/dof-st"]`);
      cy.clickIfExist(`[data-cy="1171267882-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-power-search-button`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-power-search-button`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-visualizar/editar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-excluir`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-excluir`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/ajuste-saldo->2374057292-power-search-button`, () => {
const actualId = [`root`,`recup-st`,`recup-st/ajuste-saldo`,`2374057292-power-search-button`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/ajuste-saldo"]`);
      cy.clickIfExist(`[data-cy="2374057292-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-novo`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-novo`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-power-search-button`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-power-search-button`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-visualizar/editar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-visualizar/editar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-excluir`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-excluir`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-gerar obrigação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-gerar obrigação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-gerar obrigação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-visualizar resultado da geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-visualizar resultado da geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-visualizar resultado da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-protocolo transmissão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-protocolo transmissão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-protocolo transmissão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-excluir geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-excluir geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-excluir geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->4074389884-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`4074389884-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="4074389884-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->4074389884-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`4074389884-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="4074389884-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->4074389884-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`4074389884-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="4074389884-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio->4125557054-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio`,`4125557054-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio"]`);
      cy.clickIfExist(`[data-cy="4125557054-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio->4125557054-gerar arquivo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio`,`4125557054-gerar arquivo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio"]`);
      cy.clickIfExist(`[data-cy="4125557054-gerar arquivo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio->4125557054-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio`,`4125557054-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio"]`);
      cy.clickIfExist(`[data-cy="4125557054-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/diretorio->4125557054-power-search-input and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio`,`4125557054-power-search-input`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio"]`);
      cy.fillInputPowerSearch(`[data-cy="4125557054-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/dof->1121647418-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/dof`,`1121647418-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/dof"]`);
      cy.clickIfExist(`[data-cy="1121647418-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/dof->1121647418-ajuda contextualizada`, () => {
const actualId = [`root`,`relatorios`,`relatorios/dof`,`1121647418-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/dof"]`);
      cy.clickIfExist(`[data-cy="1121647418-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/dof->1121647418-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/dof`,`1121647418-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/dof"]`);
      cy.clickIfExist(`[data-cy="1121647418-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/dof->1121647418-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/dof`,`1121647418-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/dof"]`);
      cy.fillInputPowerSearch(`[data-cy="1121647418-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-ajuda contextualizada`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-ajuda contextualizada`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-ajuda contextualizada`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-ajuda contextualizada`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-baixa->640389551-exibir dados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-baixa`,`640389551-exibir dados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-baixa"]`);
      cy.clickIfExist(`[data-cy="640389551-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-executar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-ajuda contextualizada`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-visualização`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-regerar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-excluir`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-executar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-ajuda contextualizada`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-visualização`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-executar`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-visualização`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-regerar`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-excluir`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-executar`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-ajuda contextualizada`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-visualização`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-regerar`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-excluir`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-executar`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-ajuda contextualizada`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-visualização`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-ajuda contextualizada`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-visualização`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-executar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-ajuda contextualizada`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-ajuda contextualizada`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-ajuda contextualizada"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-visualização`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-regerar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-excluir`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/dof-st->1171267882-novo documento->1171267882-item-`, () => {
const actualId = [`root`,`recup-st`,`recup-st/dof-st`,`1171267882-novo documento`,`1171267882-item-`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/dof-st"]`);
      cy.clickIfExist(`[data-cy="1171267882-novo documento"]`);
      cy.clickIfExist(`[data-cy="1171267882-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-remover item`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-visualizar/editar`,`1380780575-remover item`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1380780575-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-salvar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-visualizar/editar`,`1380780575-salvar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1380780575-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-voltar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-visualizar/editar`,`1380780575-voltar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1380780575-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values recup-st->recup-st/cfiscal-merc->1321884977-visualizar/editar->1380780575-powerselect-ncmCodigo-1380780575-powerselect-cestCodigo-1380780575-powerselect-indAlteracaoCargaTributaria-1380780575-powerselect-dfltUniCodigoEntrada-1380780575-powerselect-dfltUniCodigoSaida and submit`, () => {
const actualId = [`root`,`recup-st`,`recup-st/cfiscal-merc`,`1321884977-visualizar/editar`,`1380780575-powerselect-ncmCodigo-1380780575-powerselect-cestCodigo-1380780575-powerselect-indAlteracaoCargaTributaria-1380780575-powerselect-dfltUniCodigoEntrada-1380780575-powerselect-dfltUniCodigoSaida`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/cfiscal-merc"]`);
      cy.clickIfExist(`[data-cy="1321884977-visualizar/editar"]`);
      cy.fillInputPowerSelect(`[data-cy="1380780575-powerselect-ncmCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1380780575-powerselect-cestCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1380780575-powerselect-indAlteracaoCargaTributaria"] input`);
cy.fillInputPowerSelect(`[data-cy="1380780575-powerselect-dfltUniCodigoEntrada"] input`);
cy.fillInputPowerSelect(`[data-cy="1380780575-powerselect-dfltUniCodigoSaida"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-novo->701054877-salvar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-novo`,`701054877-salvar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-novo"]`);
      cy.clickIfExist(`[data-cy="701054877-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-novo->701054877-voltar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-novo`,`701054877-voltar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-novo"]`);
      cy.clickIfExist(`[data-cy="701054877-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values recup-st->recup-st/parametrizacao-controle-st->1509515468-novo->701054877-powerselect-ufCodigo-701054877-powerselect-cfopCodigo-701054877-powerselect-nopCodigo-701054877-powerselect-mercCodigo-701054877-powerselect-ncmCodigo-701054877-powerselect-cestCodigo-701054877-checkbox-indResIcmsSt-701054877-checkbox-indResIcmsProprio and submit`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-novo`,`701054877-powerselect-ufCodigo-701054877-powerselect-cfopCodigo-701054877-powerselect-nopCodigo-701054877-powerselect-mercCodigo-701054877-powerselect-ncmCodigo-701054877-powerselect-cestCodigo-701054877-checkbox-indResIcmsSt-701054877-checkbox-indResIcmsProprio`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="701054877-powerselect-ufCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="701054877-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="701054877-powerselect-nopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="701054877-powerselect-mercCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="701054877-powerselect-ncmCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="701054877-powerselect-cestCodigo"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="701054877-checkbox-indResIcmsSt"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="701054877-checkbox-indResIcmsProprio"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-visualizar/editar->2168276900-remover item`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-visualizar/editar`,`2168276900-remover item`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2168276900-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-visualizar/editar->2168276900-salvar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-visualizar/editar`,`2168276900-salvar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2168276900-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element recup-st->recup-st/parametrizacao-controle-st->1509515468-visualizar/editar->2168276900-voltar`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-visualizar/editar`,`2168276900-voltar`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2168276900-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values recup-st->recup-st/parametrizacao-controle-st->1509515468-visualizar/editar->2168276900-checkbox-indResIcmsSt-2168276900-checkbox-indResIcmsProprio and submit`, () => {
const actualId = [`root`,`recup-st`,`recup-st/parametrizacao-controle-st`,`1509515468-visualizar/editar`,`2168276900-checkbox-indResIcmsSt-2168276900-checkbox-indResIcmsProprio`];
    cy.clickIfExist(`[data-cy="recup-st"]`);
      cy.clickIfExist(`[data-cy="recup-st/parametrizacao-controle-st"]`);
      cy.clickIfExist(`[data-cy="1509515468-visualizar/editar"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2168276900-checkbox-indResIcmsSt"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="2168276900-checkbox-indResIcmsProprio"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-gerenciar labels->2844032748-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-gerenciar labels`,`2844032748-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="2844032748-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-visualizar/editar->3650852489-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-visualizar/editar`,`3650852489-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3650852489-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->2844032748-visualizar/editar->3650852489-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`2844032748-visualizar/editar`,`3650852489-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="2844032748-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3650852489-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ir para todas as obrigações->4051203067-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ir para todas as obrigações`,`4051203067-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="4051203067-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-nova solicitação->4051203067-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-nova solicitação`,`4051203067-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="4051203067-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-nova solicitação->4051203067-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-nova solicitação`,`4051203067-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="4051203067-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração->1644760300-rightoutlined`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`,`1644760300-rightoutlined`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1644760300-rightoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração->1644760300-habilitar edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`,`1644760300-habilitar edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1644760300-habilitar edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração->1644760300-abrir janela de edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`,`1644760300-abrir janela de edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1644760300-abrir janela de edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-visualizar resultado da geração->4051203067-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-visualizar resultado da geração`,`4051203067-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="4051203067-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-visualizar resultado da geração->4051203067-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-visualizar resultado da geração`,`4051203067-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="4051203067-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-visualizar resultado da geração->4051203067-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-visualizar resultado da geração`,`4051203067-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="4051203067-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-visualizar resultado da geração->4051203067-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-visualizar resultado da geração`,`4051203067-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="4051203067-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-protocolo transmissão->1706279188-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-protocolo transmissão`,`1706279188-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="1706279188-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-visualização->1101762724-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-visualização`,`1101762724-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1101762724-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-abrir visualização->1101762724-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-abrir visualização`,`1101762724-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-abrir visualização->1101762724-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-abrir visualização`,`1101762724-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-abrir visualização->1101762724-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-abrir visualização`,`1101762724-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-abrir visualização->1101762724-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-abrir visualização`,`1101762724-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1101762724-visualizar->1101762724-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1101762724-visualizar`,`1101762724-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualizar"]`);
      cy.clickIfExist(`[data-cy="1101762724-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-novo->4174090780-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-novo`,`4174090780-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-novo"]`);
      cy.clickIfExist(`[data-cy="4174090780-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-novo->4174090780-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-novo`,`4174090780-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-novo"]`);
      cy.clickIfExist(`[data-cy="4174090780-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->4174090780-novo->4174090780-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-novo`,`4174090780-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-novo"]`);
      cy.fillInput(`[data-cy="4174090780-input-number-ano"] textarea`, `10`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-editar->4174090780-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-editar`,`4174090780-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-editar"]`);
      cy.clickIfExist(`[data-cy="4174090780-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4174090780-editar->4174090780-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`4174090780-editar`,`4174090780-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="4174090780-editar"]`);
      cy.clickIfExist(`[data-cy="4174090780-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->4074389884-novo->4074389884-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`4074389884-novo`,`4074389884-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="4074389884-novo"]`);
      cy.clickIfExist(`[data-cy="4074389884-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/dof->1121647418-visualização->1121647418-salvar configuração`, () => {
const actualId = [`root`,`relatorios`,`relatorios/dof`,`1121647418-visualização`,`1121647418-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/dof"]`);
      cy.clickIfExist(`[data-cy="1121647418-visualização"]`);
      cy.clickIfExist(`[data-cy="1121647418-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-executar->3200271866-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-executar`,`3200271866-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-executar"]`);
      cy.clickIfExist(`[data-cy="3200271866-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-executar->3200271866-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-executar`,`3200271866-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-executar"]`);
      cy.clickIfExist(`[data-cy="3200271866-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-agendamentos->3200271866-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-agendamentos`,`3200271866-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3200271866-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/cadastro-mercadoria->3200271866-visualização->3200271866-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-visualização`,`3200271866-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3200271866-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-executar->568761414-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-executar`,`568761414-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-executar"]`);
      cy.clickIfExist(`[data-cy="568761414-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-executar->568761414-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-executar`,`568761414-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-executar"]`);
      cy.clickIfExist(`[data-cy="568761414-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-agendamentos->568761414-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-agendamentos`,`568761414-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-agendamentos"]`);
      cy.clickIfExist(`[data-cy="568761414-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/posicao-estoque->568761414-visualização->568761414-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-visualização`,`568761414-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="568761414-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-detalhes->568761414-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-detalhes`,`568761414-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-detalhes"]`);
      cy.clickIfExist(`[data-cy="568761414-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização->568761414-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-abrir visualização`,`568761414-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="568761414-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização->568761414-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-abrir visualização`,`568761414-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="568761414-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização->568761414-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-abrir visualização`,`568761414-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="568761414-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização->568761414-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-abrir visualização`,`568761414-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="568761414-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-executar->1811188643-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-executar`,`1811188643-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-executar"]`);
      cy.clickIfExist(`[data-cy="1811188643-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-executar->1811188643-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-executar`,`1811188643-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-executar"]`);
      cy.clickIfExist(`[data-cy="1811188643-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-agendamentos->1811188643-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-agendamentos`,`1811188643-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1811188643-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/controle-ressarcimento->1811188643-visualização->1811188643-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-visualização`,`1811188643-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1811188643-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-detalhes->1811188643-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-detalhes`,`1811188643-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-detalhes"]`);
      cy.clickIfExist(`[data-cy="1811188643-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-abrir visualização->1811188643-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-abrir visualização`,`1811188643-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1811188643-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-abrir visualização->1811188643-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-abrir visualização`,`1811188643-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1811188643-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-abrir visualização->1811188643-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-abrir visualização`,`1811188643-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1811188643-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-abrir visualização->1811188643-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-abrir visualização`,`1811188643-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1811188643-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-executar->656091685-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-executar`,`656091685-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-executar"]`);
      cy.clickIfExist(`[data-cy="656091685-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-executar->656091685-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-executar`,`656091685-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-executar"]`);
      cy.clickIfExist(`[data-cy="656091685-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-agendamentos->656091685-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-agendamentos`,`656091685-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-agendamentos"]`);
      cy.clickIfExist(`[data-cy="656091685-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/controle-saldo->656091685-visualização->656091685-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-visualização`,`656091685-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="656091685-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-detalhes->656091685-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-detalhes`,`656091685-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-detalhes"]`);
      cy.clickIfExist(`[data-cy="656091685-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-abrir visualização->656091685-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-abrir visualização`,`656091685-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="656091685-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-abrir visualização->656091685-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-abrir visualização`,`656091685-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="656091685-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-abrir visualização->656091685-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-abrir visualização`,`656091685-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="656091685-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-abrir visualização->656091685-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-abrir visualização`,`656091685-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="656091685-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-executar->1799809287-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-executar`,`1799809287-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-executar"]`);
      cy.clickIfExist(`[data-cy="1799809287-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-executar->1799809287-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-executar`,`1799809287-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-executar"]`);
      cy.clickIfExist(`[data-cy="1799809287-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-agendamentos->1799809287-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-agendamentos`,`1799809287-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1799809287-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas->1799809287-visualização->1799809287-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-visualização`,`1799809287-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1799809287-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-detalhes->1799809287-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-detalhes`,`1799809287-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-detalhes"]`);
      cy.clickIfExist(`[data-cy="1799809287-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-abrir visualização->1799809287-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-abrir visualização`,`1799809287-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1799809287-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-abrir visualização->1799809287-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-abrir visualização`,`1799809287-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1799809287-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-abrir visualização->1799809287-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-abrir visualização`,`1799809287-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1799809287-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-abrir visualização->1799809287-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-abrir visualização`,`1799809287-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1799809287-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-executar->1625609257-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`,`1625609257-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.clickIfExist(`[data-cy="1625609257-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-executar->1625609257-agendar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`,`1625609257-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.clickIfExist(`[data-cy="1625609257-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/carga-inicial-mercadorias->1625609257-executar->1625609257-input-CODIGO_NCM-1625609257-input-CODIGO_MERCADORIA and submit`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`,`1625609257-input-CODIGO_NCM-1625609257-input-CODIGO_MERCADORIA`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.fillInput(`[data-cy="1625609257-input-CODIGO_NCM"] textarea`, `Small`);
cy.fillInput(`[data-cy="1625609257-input-CODIGO_MERCADORIA"] textarea`, `AI`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-agendamentos->1625609257-voltar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-agendamentos`,`1625609257-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1625609257-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/carga-inicial-mercadorias->1625609257-visualização->1625609257-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-visualização`,`1625609257-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1625609257-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-detalhes->1625609257-não há dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-detalhes`,`1625609257-não há dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-detalhes"]`);
      cy.clickIfExist(`[data-cy="1625609257-não há dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-executar->2154116344-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-executar`,`2154116344-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-executar"]`);
      cy.clickIfExist(`[data-cy="2154116344-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-executar->2154116344-requisitos de preenchimento`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-executar`,`2154116344-requisitos de preenchimento`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-executar"]`);
      cy.clickIfExist(`[data-cy="2154116344-requisitos de preenchimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-executar->2154116344-agendar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-executar`,`2154116344-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-executar"]`);
      cy.clickIfExist(`[data-cy="2154116344-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/carga-inicial-dof->2154116344-executar->2154116344-input-MERCADORIA and submit`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-executar`,`2154116344-input-MERCADORIA`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-executar"]`);
      cy.fillInput(`[data-cy="2154116344-input-MERCADORIA"] textarea`, `Rustic Wooden Bacon`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-agendamentos->2154116344-voltar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-agendamentos`,`2154116344-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2154116344-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/carga-inicial-dof->2154116344-visualização->2154116344-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-visualização`,`2154116344-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2154116344-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-executar->974703873-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-executar`,`974703873-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-executar"]`);
      cy.clickIfExist(`[data-cy="974703873-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-executar->974703873-agendar`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-executar`,`974703873-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-executar"]`);
      cy.clickIfExist(`[data-cy="974703873-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-agendamentos->974703873-voltar`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-agendamentos`,`974703873-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-agendamentos"]`);
      cy.clickIfExist(`[data-cy="974703873-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/carga-mensal-dof->974703873-visualização->974703873-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-visualização`,`974703873-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="974703873-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-detalhes->974703873-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-detalhes`,`974703873-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-detalhes"]`);
      cy.clickIfExist(`[data-cy="974703873-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-abrir visualização->974703873-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-abrir visualização`,`974703873-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="974703873-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-abrir visualização->974703873-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-abrir visualização`,`974703873-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="974703873-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-abrir visualização->974703873-expandir`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-abrir visualização`,`974703873-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="974703873-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-abrir visualização->974703873-download`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-abrir visualização`,`974703873-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="974703873-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-executar->3570614338-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-executar`,`3570614338-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-executar"]`);
      cy.clickIfExist(`[data-cy="3570614338-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-executar->3570614338-agendar`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-executar`,`3570614338-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-executar"]`);
      cy.clickIfExist(`[data-cy="3570614338-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-agendamentos->3570614338-voltar`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-agendamentos`,`3570614338-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3570614338-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/icms-st->3570614338-visualização->3570614338-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-visualização`,`3570614338-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3570614338-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-detalhes->3570614338-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-detalhes`,`3570614338-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-detalhes"]`);
      cy.clickIfExist(`[data-cy="3570614338-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-abrir visualização->3570614338-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-abrir visualização`,`3570614338-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3570614338-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-abrir visualização->3570614338-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-abrir visualização`,`3570614338-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3570614338-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-abrir visualização->3570614338-expandir`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-abrir visualização`,`3570614338-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3570614338-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-abrir visualização->3570614338-download`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-abrir visualização`,`3570614338-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3570614338-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-executar->3373103500-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-executar`,`3373103500-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-executar"]`);
      cy.clickIfExist(`[data-cy="3373103500-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-executar->3373103500-agendar`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-executar`,`3373103500-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-executar"]`);
      cy.clickIfExist(`[data-cy="3373103500-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-agendamentos->3373103500-voltar`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-agendamentos`,`3373103500-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3373103500-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/dados-sintegra-mg->3373103500-visualização->3373103500-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-visualização`,`3373103500-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3373103500-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`,`589853967-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.clickIfExist(`[data-cy="589853967-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-agendar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`,`589853967-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.clickIfExist(`[data-cy="589853967-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-input-CODIGO_NCM-589853967-input-CODIGO_MERCADORIA and submit`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`,`589853967-input-CODIGO_NCM-589853967-input-CODIGO_MERCADORIA`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.fillInput(`[data-cy="589853967-input-CODIGO_NCM"] textarea`, `Alameda`);
cy.fillInput(`[data-cy="589853967-input-CODIGO_MERCADORIA"] textarea`, `Comoros`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-agendamentos->589853967-voltar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-agendamentos`,`589853967-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-agendamentos"]`);
      cy.clickIfExist(`[data-cy="589853967-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/apagar-dados-mercadorias->589853967-visualização->589853967-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-visualização`,`589853967-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="589853967-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-agendamentos->1307678592-voltar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-agendamentos`,`1307678592-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1307678592-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/apagar-dados-recup-st->1307678592-visualização->1307678592-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-visualização`,`1307678592-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1307678592-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-detalhes->1307678592-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-detalhes`,`1307678592-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-detalhes"]`);
      cy.clickIfExist(`[data-cy="1307678592-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-abrir visualização->1307678592-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-abrir visualização`,`1307678592-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1307678592-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-abrir visualização->1307678592-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-abrir visualização`,`1307678592-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1307678592-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-abrir visualização->1307678592-expandir`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-abrir visualização`,`1307678592-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1307678592-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-abrir visualização->1307678592-download`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-abrir visualização`,`1307678592-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1307678592-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-visualização->1101762724-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-visualização`,`1101762724-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1101762724-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-abrir visualização->1101762724-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-abrir visualização`,`1101762724-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-abrir visualização->1101762724-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-abrir visualização`,`1101762724-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-abrir visualização->1101762724-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-abrir visualização`,`1101762724-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-abrir visualização->1101762724-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-abrir visualização`,`1101762724-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-visualizar->1101762724-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-visualizar`,`1101762724-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-visualizar"]`);
      cy.clickIfExist(`[data-cy="1101762724-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração->1644760300-rightoutlined->1644760300-downoutlined`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`,`1644760300-rightoutlined`,`1644760300-downoutlined`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1644760300-rightoutlined"]`);
      cy.clickIfExist(`[data-cy="1644760300-downoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração->1644760300-habilitar edição->1644760300-confirmar  edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`,`1644760300-habilitar edição`,`1644760300-confirmar  edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1644760300-habilitar edição"]`);
      cy.clickIfExist(`[data-cy="1644760300-confirmar  edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-ajustar parâmetros da geração->1644760300-abrir janela de edição->1644760300-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-ajustar parâmetros da geração`,`1644760300-abrir janela de edição`,`1644760300-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1644760300-abrir janela de edição"]`);
      cy.clickIfExist(`[data-cy="1644760300-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-visualizar resultado da geração->4051203067-expandir->4051203067-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-visualizar resultado da geração`,`4051203067-expandir`,`4051203067-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="4051203067-expandir"]`);
      cy.clickIfExist(`[data-cy="4051203067-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-protocolo transmissão->1706279188-editar->1706279188-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-protocolo transmissão`,`1706279188-editar`,`1706279188-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="1706279188-editar"]`);
      cy.clickIfExist(`[data-cy="1706279188-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-protocolo transmissão->1706279188-editar->1706279188-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-protocolo transmissão`,`1706279188-editar`,`1706279188-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="1706279188-editar"]`);
      cy.clickIfExist(`[data-cy="1706279188-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/cadastro-mercadoria->3200271866-executar->3200271866-múltipla seleção->3200271866-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/cadastro-mercadoria`,`3200271866-executar`,`3200271866-múltipla seleção`,`3200271866-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/cadastro-mercadoria"]`);
      cy.clickIfExist(`[data-cy="3200271866-executar"]`);
      cy.clickIfExist(`[data-cy="3200271866-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3200271866-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-executar->568761414-múltipla seleção->568761414-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-executar`,`568761414-múltipla seleção`,`568761414-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-executar"]`);
      cy.clickIfExist(`[data-cy="568761414-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="568761414-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/posicao-estoque->568761414-abrir visualização->568761414-expandir->568761414-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/posicao-estoque`,`568761414-abrir visualização`,`568761414-expandir`,`568761414-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/posicao-estoque"]`);
      cy.clickIfExist(`[data-cy="568761414-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="568761414-expandir"]`);
      cy.clickIfExist(`[data-cy="568761414-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-executar->1811188643-múltipla seleção->1811188643-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-executar`,`1811188643-múltipla seleção`,`1811188643-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-executar"]`);
      cy.clickIfExist(`[data-cy="1811188643-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1811188643-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-ressarcimento->1811188643-abrir visualização->1811188643-expandir->1811188643-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-ressarcimento`,`1811188643-abrir visualização`,`1811188643-expandir`,`1811188643-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-ressarcimento"]`);
      cy.clickIfExist(`[data-cy="1811188643-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1811188643-expandir"]`);
      cy.clickIfExist(`[data-cy="1811188643-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-executar->656091685-múltipla seleção->656091685-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-executar`,`656091685-múltipla seleção`,`656091685-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-executar"]`);
      cy.clickIfExist(`[data-cy="656091685-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="656091685-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/controle-saldo->656091685-abrir visualização->656091685-expandir->656091685-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/controle-saldo`,`656091685-abrir visualização`,`656091685-expandir`,`656091685-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/controle-saldo"]`);
      cy.clickIfExist(`[data-cy="656091685-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="656091685-expandir"]`);
      cy.clickIfExist(`[data-cy="656091685-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-executar->1799809287-múltipla seleção->1799809287-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-executar`,`1799809287-múltipla seleção`,`1799809287-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-executar"]`);
      cy.clickIfExist(`[data-cy="1799809287-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1799809287-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas->1799809287-abrir visualização->1799809287-expandir->1799809287-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas`,`1799809287-abrir visualização`,`1799809287-expandir`,`1799809287-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas"]`);
      cy.clickIfExist(`[data-cy="1799809287-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1799809287-expandir"]`);
      cy.clickIfExist(`[data-cy="1799809287-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-executar->1625609257-múltipla seleção->1625609257-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`,`1625609257-múltipla seleção`,`1625609257-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.clickIfExist(`[data-cy="1625609257-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1625609257-este parâmetro possui mais de 100 valores. clique aqui para carregar todos."]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-executar->1625609257-múltipla seleção->1625609257-próximo`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`,`1625609257-múltipla seleção`,`1625609257-próximo`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.clickIfExist(`[data-cy="1625609257-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1625609257-próximo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-mercadorias->1625609257-executar->1625609257-múltipla seleção->1625609257-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-mercadorias`,`1625609257-executar`,`1625609257-múltipla seleção`,`1625609257-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-mercadorias"]`);
      cy.clickIfExist(`[data-cy="1625609257-executar"]`);
      cy.clickIfExist(`[data-cy="1625609257-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1625609257-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-inicial-dof->2154116344-executar->2154116344-múltipla seleção->2154116344-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/carga-inicial-dof`,`2154116344-executar`,`2154116344-múltipla seleção`,`2154116344-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-inicial-dof"]`);
      cy.clickIfExist(`[data-cy="2154116344-executar"]`);
      cy.clickIfExist(`[data-cy="2154116344-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2154116344-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-executar->974703873-múltipla seleção->974703873-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-executar`,`974703873-múltipla seleção`,`974703873-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-executar"]`);
      cy.clickIfExist(`[data-cy="974703873-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="974703873-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/carga-mensal-dof->974703873-abrir visualização->974703873-expandir->974703873-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/carga-mensal-dof`,`974703873-abrir visualização`,`974703873-expandir`,`974703873-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/carga-mensal-dof"]`);
      cy.clickIfExist(`[data-cy="974703873-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="974703873-expandir"]`);
      cy.clickIfExist(`[data-cy="974703873-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-executar->3570614338-múltipla seleção->3570614338-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-executar`,`3570614338-múltipla seleção`,`3570614338-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-executar"]`);
      cy.clickIfExist(`[data-cy="3570614338-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3570614338-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/icms-st->3570614338-abrir visualização->3570614338-expandir->3570614338-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/icms-st`,`3570614338-abrir visualização`,`3570614338-expandir`,`3570614338-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/icms-st"]`);
      cy.clickIfExist(`[data-cy="3570614338-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3570614338-expandir"]`);
      cy.clickIfExist(`[data-cy="3570614338-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/dados-sintegra-mg->3373103500-executar->3373103500-múltipla seleção->3373103500-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/dados-sintegra-mg`,`3373103500-executar`,`3373103500-múltipla seleção`,`3373103500-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/dados-sintegra-mg"]`);
      cy.clickIfExist(`[data-cy="3373103500-executar"]`);
      cy.clickIfExist(`[data-cy="3373103500-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3373103500-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-múltipla seleção->589853967-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`,`589853967-múltipla seleção`,`589853967-este parâmetro possui mais de 100 valores. clique aqui para carregar todos.`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.clickIfExist(`[data-cy="589853967-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="589853967-este parâmetro possui mais de 100 valores. clique aqui para carregar todos."]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-múltipla seleção->589853967-próximo`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`,`589853967-múltipla seleção`,`589853967-próximo`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.clickIfExist(`[data-cy="589853967-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="589853967-próximo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-mercadorias->589853967-executar->589853967-múltipla seleção->589853967-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-mercadorias`,`589853967-executar`,`589853967-múltipla seleção`,`589853967-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-mercadorias"]`);
      cy.clickIfExist(`[data-cy="589853967-executar"]`);
      cy.clickIfExist(`[data-cy="589853967-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="589853967-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/apagar-dados-recup-st->1307678592-abrir visualização->1307678592-expandir->1307678592-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/apagar-dados-recup-st`,`1307678592-abrir visualização`,`1307678592-expandir`,`1307678592-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/apagar-dados-recup-st"]`);
      cy.clickIfExist(`[data-cy="1307678592-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1307678592-expandir"]`);
      cy.clickIfExist(`[data-cy="1307678592-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element obrigacoes->obrigacoes/solicitacao-resultado->4051203067-agendamentos->1101762724-abrir visualização->1101762724-expandir->1101762724-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacao-resultado`,`4051203067-agendamentos`,`1101762724-abrir visualização`,`1101762724-expandir`,`1101762724-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacao-resultado"]`);
      cy.clickIfExist(`[data-cy="4051203067-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1101762724-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1101762724-expandir"]`);
      cy.clickIfExist(`[data-cy="1101762724-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
