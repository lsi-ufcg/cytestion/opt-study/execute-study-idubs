describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
    const actualId = [`root`, `tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
    const actualId = [`root`, `tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-contribuicoes`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`];
    cy.clickIfExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element credito-presumido`, () => {
    const actualId = [`root`, `credito-presumido`];
    cy.clickIfExist(`[data-cy="credito-presumido"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
    const actualId = [`root`, `obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
    const actualId = [`root`, `relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
    const actualId = [`root`, `processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
    const actualId = [`root`, `processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
    const actualId = [`root`, `downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 3802876227-exibir dados`, () => {
    const actualId = [`root`, `3802876227-exibir dados`];
    cy.clickIfExist(`[data-cy="3802876227-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values 3802876227-powerselect-codEstabelecimento and submit`, () => {
    const actualId = [`root`, `3802876227-powerselect-codEstabelecimento`];
    cy.fillInputPowerSelect(`[data-cy="3802876227-powerselect-codEstabelecimento"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/cfop`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/cfop`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/cfop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/modelo-dof`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/modelo-dof`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/modelo-dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-prestacoes`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-prestacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-servicos`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-servicos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-servicos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/processos`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/processos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametros`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamentos-apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamentos-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamentos-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/controles"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`];
    cy.clickIfExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.clickIfExist(`[data-cy="apuracao-contribuicoes/receita-bruta-mensal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element credito-presumido->credito-presumido/operacoes`, () => {
    const actualId = [`root`, `credito-presumido`, `credito-presumido/operacoes`];
    cy.clickIfExist(`[data-cy="credito-presumido"]`);
    cy.clickIfExist(`[data-cy="credito-presumido/operacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element credito-presumido->credito-presumido/parametros-gerais`, () => {
    const actualId = [`root`, `credito-presumido`, `credito-presumido/parametros-gerais`];
    cy.clickIfExist(`[data-cy="credito-presumido"]`);
    cy.clickIfExist(`[data-cy="credito-presumido/parametros-gerais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lfis`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lfis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/deleta-lfis-periodo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/credito-presumido`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/exclusao-icms-base-calculo-pis-cofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->528015135-power-search-button`, () => {
    const actualId = [`root`, `downloads`, `528015135-power-search-button`];
    cy.visit('http://system-A16/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="528015135-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->528015135-download`, () => {
    const actualId = [`root`, `downloads`, `528015135-download`];
    cy.visit('http://system-A16/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="528015135-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->528015135-detalhes`, () => {
    const actualId = [`root`, `downloads`, `528015135-detalhes`];
    cy.visit('http://system-A16/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="528015135-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->528015135-excluir`, () => {
    const actualId = [`root`, `downloads`, `528015135-excluir`];
    cy.visit('http://system-A16/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="528015135-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/naturezaOperacao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/naturezaOperacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/naturezaOperacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/edof`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/edof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/edof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/equipamento`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/equipamento`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/equipamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/codigo-receita`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/codigo-receita`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/codigo-receita"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/deducao-nop`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/deducao-nop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/deducao-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/deducao-cfop`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/deducao-cfop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/deducao-cfop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/pessoas-fisicas-juridicas`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/pessoas-fisicas-juridicas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/pessoas-fisicas-juridicas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/hierarquia-pessoas`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/hierarquia-pessoas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/hierarquia-pessoas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/tipo-contribuinte`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/tipo-contribuinte`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/tipo-contribuinte"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/classe-pfj`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/classe-pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/classe-pfj"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/A4ista`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/A4ista`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/A4ista"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/cadastro-mercadoria`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/cadastro-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/cadastro-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/origem-mercadoria`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/origem-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/origem-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-oficiais/mercadorias/stribpis`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-oficiais/mercadorias/stribpis`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias/stribpis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-oficiais/mercadorias/stribcofins`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-oficiais/mercadorias/stribcofins`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias/stribcofins"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-prestacoes->tabelas-corporativas/cadastro-prestacoes/prestacoes`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-prestacoes`, `tabelas-corporativas/cadastro-prestacoes/prestacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes/prestacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-servicos->tabelas-corporativas/cadastro-servicos/servicos`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-servicos`, `tabelas-corporativas/cadastro-servicos/servicos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-servicos"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-servicos/servicos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/digitacao-manutencao-dof`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/digitacao-manutencao-dof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes/digitacao-manutencao-dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->transacoes/leitura-situacao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `transacoes/leitura-situacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.clickIfExist(`[data-cy="transacoes/leitura-situacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/processos->tabelas-corporativas/processo-administrativo-judicial`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/processos`, `tabelas-corporativas/processo-administrativo-judicial`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/processos"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/processo-administrativo-judicial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametros->escrituracao-apuracao/parametros/parametro-apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`, `escrituracao-apuracao/parametros/parametro-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametros"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametros/parametro-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamentos-apuracao->escrituracao-apuracao/lancamentos-apuracao/consulta-geracao-lancamento-fiscal`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamentos-apuracao`, `escrituracao-apuracao/lancamentos-apuracao/consulta-geracao-lancamento-fiscal`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamentos-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamentos-apuracao/consulta-geracao-lancamento-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-valor-retido-fonte`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-valor-retido-fonte`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/controles"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/controles/controle-valor-retido-fonte"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/controles"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/controles/controle-credito-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-power-search-button`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-power-search-button`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-visualizar/editar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-visualizar/editar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-excluir`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-excluir`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-carregar mais`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-carregar mais`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-power-search-button`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1989850871-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-gerenciar labels`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-gerenciar labels`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1989850871-gerenciar labels"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-visualizar parâmetros`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-visualizar parâmetros`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1989850871-visualizar parâmetros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-visualizar/editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-visualizar/editar`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1989850871-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-ir para todas as obrigações`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-ir para todas as obrigações`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-ajuda`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-ajuda`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-nova solicitação`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-nova solicitação`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-nova solicitação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-agendamentos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-agendamentos`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-atualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-atualizar`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-power-search-button`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-visualização`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-abrir visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-abrir visualização`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-visualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-visualizar`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-novo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-novo`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-power-search-button`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-editar`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-excluir`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->2993739087-novo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/estabelecimento`, `2993739087-novo`];
    cy.visit('http://system-A16/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2993739087-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->2993739087-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/estabelecimento`, `2993739087-power-search-button`];
    cy.visit('http://system-A16/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2993739087-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->2993739087-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/estabelecimento`, `2993739087-excluir`];
    cy.visit('http://system-A16/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2993739087-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido/item-grupo-regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido/documentos-lancamentos-cupons"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="relatorios/credito-presumido/demonstrativo-de-report-gerado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis-snapshot`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis-snapshot`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-lfis-snapshot"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-executar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-executar`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-agendamentos`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-power-search-button`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-visualização`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-regerar`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-detalhes`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-abrir visualização`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-excluir`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-executar`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-executar`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-agendamentos`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-power-search-button`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-visualização`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido/geracao-de-dados-para-report"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido/gera-dados-report-hierarquia"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido/deleta-geracao-dados-report"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido"]`);
    cy.clickIfExist(`[data-cy="processos/credito-presumido/gera-dados-gerenciais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-executar`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-executar`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-agendamentos`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-power-search-button`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-visualização`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-regerar`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-detalhes`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-abrir visualização`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-excluir`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametros->escrituracao-apuracao/parametros/parametro-apuracao->2695317342-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`, `escrituracao-apuracao/parametros/parametro-apuracao`, `2695317342-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametros/parametro-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2695317342-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametros->escrituracao-apuracao/parametros/parametro-apuracao->2695317342-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`, `escrituracao-apuracao/parametros/parametro-apuracao`, `2695317342-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametros/parametro-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2695317342-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-unorderedlistoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-excluir`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-carregar mais`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-carregar mais`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamentos-apuracao->escrituracao-apuracao/lancamentos-apuracao/consulta-geracao-lancamento-fiscal->3863203708-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamentos-apuracao`, `escrituracao-apuracao/lancamentos-apuracao/consulta-geracao-lancamento-fiscal`, `3863203708-power-search-button`];
    cy.visit('http://system-A16/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3863203708-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-valor-retido-fonte->2084800543-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-valor-retido-fonte`, `2084800543-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-valor-retido-fonte');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2084800543-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-valor-retido-fonte->2084800543-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-valor-retido-fonte`, `2084800543-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-valor-retido-fonte');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2084800543-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-visualizar/editar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-visualizar/editar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-excluir`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-carregar mais`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-carregar mais`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100->701237180-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`, `701237180-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="701237180-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100->701237180-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`, `701237180-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="701237180-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600->1661905004-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`, `1661905004-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1661905004-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600->1661905004-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`, `1661905004-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1661905004-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-visualizar/editar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-visualizar/editar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-excluir`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-carregar mais`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-carregar mais`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-salvar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2686775629-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-voltar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-voltar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2686775629-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-input-monetary-vlTribMercInterno-2686775629-input-monetary-vlNaoTribMercInterno-2686775629-input-monetary-vlExportacao-2686775629-input-monetary-vlCumulativa and submit`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-input-monetary-vlTribMercInterno-2686775629-input-monetary-vlNaoTribMercInterno-2686775629-input-monetary-vlExportacao-2686775629-input-monetary-vlCumulativa`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-novo"]`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlTribMercInterno"] textarea`, `2`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlNaoTribMercInterno"] textarea`, `1`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlExportacao"] textarea`, `5`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlCumulativa"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-visualizar/editar->345568596-remover item`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-visualizar/editar`, `345568596-remover item`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/editar/241');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="345568596-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-visualizar/editar->345568596-salvar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-visualizar/editar`, `345568596-salvar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/editar/241');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="345568596-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-visualizar/editar->345568596-voltar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-visualizar/editar`, `345568596-voltar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/editar/241');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="345568596-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-visualizar/editar->345568596-input-monetary-vlTribMercInterno-345568596-input-monetary-vlNaoTribMercInterno-345568596-input-monetary-vlExportacao-345568596-input-monetary-vlCumulativa and submit`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-visualizar/editar`, `345568596-input-monetary-vlTribMercInterno-345568596-input-monetary-vlNaoTribMercInterno-345568596-input-monetary-vlExportacao-345568596-input-monetary-vlCumulativa`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="622944540-visualizar/editar"]`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlTribMercInterno"] textarea`, `5`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlNaoTribMercInterno"] textarea`, `6`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlExportacao"] textarea`, `10`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlCumulativa"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-gerenciar labels->1989850871-fechar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-gerenciar labels`, `1989850871-fechar`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1989850871-gerenciar labels"]`);
    cy.clickIfExist(`[data-cy="1989850871-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-visualizar/editar->1073240852-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-visualizar/editar`, `1073240852-salvar`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1073240852-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1989850871-visualizar/editar->1073240852-voltar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/propriedades`, `1989850871-visualizar/editar`, `1073240852-voltar`];
    cy.visit('http://system-A16/obrigacoes/configuracao-obrigacao-fiscal/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1073240852-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-ir para todas as obrigações->2037403718-voltar às obrigações do módulo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-ir para todas as obrigações`, `2037403718-voltar às obrigações do módulo`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="2037403718-voltar às obrigações do módulo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-nova solicitação->2037403718-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-nova solicitação`, `2037403718-salvar`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="2037403718-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-nova solicitação->2037403718-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-nova solicitação`, `2037403718-cancelar`];
    cy.visit('http://system-A16/obrigacoes/solicitacoes-resultados?estab=AAA_DF&obrSigla=EFD-PIS-COFINS');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2037403718-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="2037403718-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-agendamentos->3119776687-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-agendamentos`, `3119776687-power-search-button`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?sigla=~eq~EFD-PIS-COFINS%7C%7CEFD-PIS-COFINS&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->2037403718-agendamentos->3119776687-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-agendamentos`, `3119776687-visualização`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?sigla=~eq~EFD-PIS-COFINS%7C%7CEFD-PIS-COFINS&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-visualização->3119776687-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-visualização`, `3119776687-item-`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3119776687-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-abrir visualização->3119776687-aumentar o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-abrir visualização`, `3119776687-aumentar o zoom`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3119776687-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-abrir visualização->3119776687-diminuir o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-abrir visualização`, `3119776687-diminuir o zoom`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3119776687-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-abrir visualização->3119776687-expandir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-abrir visualização`, `3119776687-expandir`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3119776687-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-abrir visualização->3119776687-download`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-abrir visualização`, `3119776687-download`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3119776687-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-visualizar->3119776687-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-visualizar`, `3119776687-dados disponíveis para impressão`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-visualizar"]`);
    cy.clickIfExist(`[data-cy="3119776687-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-novo->1897137447-criar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-novo`, `1897137447-criar`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-novo"]`);
    cy.clickIfExist(`[data-cy="1897137447-criar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-novo->1897137447-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-novo`, `1897137447-cancelar`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-novo"]`);
    cy.clickIfExist(`[data-cy="1897137447-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->1897137447-novo->1897137447-input-number-ano and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-novo`, `1897137447-input-number-ano`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-novo"]`);
    cy.fillInput(`[data-cy="1897137447-input-number-ano"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-editar->1897137447-remover item`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-editar`, `1897137447-remover item`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-editar"]`);
    cy.clickIfExist(`[data-cy="1897137447-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->1897137447-editar->1897137447-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `1897137447-editar`, `1897137447-salvar`];
    cy.visit('http://system-A16/obrigacoes/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1897137447-editar"]`);
    cy.clickIfExist(`[data-cy="1897137447-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->2993739087-novo->2993739087-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/estabelecimento`, `2993739087-novo`, `2993739087-cancelar`];
    cy.visit('http://system-A16/obrigacoes/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2993739087-novo"]`);
    cy.clickIfExist(`[data-cy="2993739087-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-executar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-agendamentos`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-power-search-button`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-visualização`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-executar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-agendamentos`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-power-search-button`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-visualização`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-executar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-agendamentos`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-power-search-button`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-visualização`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4165638550-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4165638550-power-search-button`];
    cy.visit('http://system-A16/relatorios/inconsistencias/dof-sem-item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4165638550-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4165638550-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4165638550-visualização`];
    cy.visit('http://system-A16/relatorios/inconsistencias/dof-sem-item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4165638550-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4165638550-power-search-input and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4165638550-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-item"]`);
    cy.fillInputPowerSearch(`[data-cy="4165638550-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis-snapshot->1354342954-exibir dados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis-snapshot`, `1354342954-exibir dados`];
    cy.visit('http://system-A16/relatorios/inconsistencias/dof-sem-lfis-snapshot');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1354342954-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-executar->65189148-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-executar`, `65189148-múltipla seleção`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-executar"]`);
    cy.clickIfExist(`[data-cy="65189148-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-executar->65189148-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-executar`, `65189148-agendar`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-executar"]`);
    cy.clickIfExist(`[data-cy="65189148-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-agendamentos->65189148-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-agendamentos`, `65189148-voltar`];
    cy.visit('http://system-A16/processos/gera-lfis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~57673102D%7C%7C57673102&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/gera-lfis->65189148-visualização->65189148-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-visualização`, `65189148-item-`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="65189148-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-detalhes->65189148-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-detalhes`, `65189148-dados disponíveis para impressão`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-detalhes"]`);
    cy.clickIfExist(`[data-cy="65189148-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-abrir visualização->65189148-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-abrir visualização`, `65189148-aumentar o zoom`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="65189148-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-abrir visualização->65189148-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-abrir visualização`, `65189148-diminuir o zoom`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="65189148-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-abrir visualização->65189148-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-abrir visualização`, `65189148-expandir`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="65189148-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-abrir visualização->65189148-download`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-abrir visualização`, `65189148-download`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="65189148-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-executar->3501244739-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-executar`, `3501244739-múltipla seleção`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-executar"]`);
    cy.clickIfExist(`[data-cy="3501244739-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-executar->3501244739-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-executar`, `3501244739-agendar`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-executar"]`);
    cy.clickIfExist(`[data-cy="3501244739-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-agendamentos->3501244739-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-agendamentos`, `3501244739-voltar`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~57305999D%7C%7C57305999&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/deleta-lfis-periodo->3501244739-visualização->3501244739-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-visualização`, `3501244739-item-`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3501244739-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-executar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-executar`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-agendamentos`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-power-search-button`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-visualização`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-executar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-executar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-agendamentos`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-power-search-button`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-visualização`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-executar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-executar`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-agendamentos`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-power-search-button`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-visualização`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-executar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-executar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-agendamentos`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-power-search-button`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-visualização`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-executar->3904502845-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-executar`, `3904502845-múltipla seleção`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-executar"]`);
    cy.clickIfExist(`[data-cy="3904502845-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-executar->3904502845-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-executar`, `3904502845-agendar`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-executar"]`);
    cy.clickIfExist(`[data-cy="3904502845-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-agendamentos->3904502845-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-agendamentos`, `3904502845-voltar`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~48767671D%7C%7C48767671&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-visualização->3904502845-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-visualização`, `3904502845-item-`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3904502845-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-detalhes->3904502845-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-detalhes`, `3904502845-dados disponíveis para impressão`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-detalhes"]`);
    cy.clickIfExist(`[data-cy="3904502845-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-abrir visualização->3904502845-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-abrir visualização`, `3904502845-aumentar o zoom`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3904502845-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-abrir visualização->3904502845-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-abrir visualização`, `3904502845-diminuir o zoom`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3904502845-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-abrir visualização->3904502845-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-abrir visualização`, `3904502845-expandir`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3904502845-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-abrir visualização->3904502845-download`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-abrir visualização`, `3904502845-download`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3904502845-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametros->escrituracao-apuracao/parametros/parametro-apuracao->2695317342-novo->2063508555-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`, `escrituracao-apuracao/parametros/parametro-apuracao`, `2695317342-novo`, `2063508555-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametros/parametro-apuracao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2063508555-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametros->escrituracao-apuracao/parametros/parametro-apuracao->2695317342-novo->2063508555-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`, `escrituracao-apuracao/parametros/parametro-apuracao`, `2695317342-novo`, `2063508555-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametros/parametro-apuracao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2063508555-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametros->escrituracao-apuracao/parametros/parametro-apuracao->2695317342-novo->2063508555-powerselect-indFormaTribLucro-2063508555-powerselect-indCodIncidTrib-2063508555-powerselect-indReconReceita-2063508555-powerselect-indMetodApropriacao-2063508555-powerselect-indTipoContrib-2063508555-powerselect-indNatPessoaJur-2063508555-powerselect-indGeracaoRegF600-2063508555-powerselect-indSomarDespRecBruta-2063508555-powerselect-indUsoRetencao-2063508555-powerselect-indUsoCredito and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametros`, `escrituracao-apuracao/parametros/parametro-apuracao`, `2695317342-novo`, `2063508555-powerselect-indFormaTribLucro-2063508555-powerselect-indCodIncidTrib-2063508555-powerselect-indReconReceita-2063508555-powerselect-indMetodApropriacao-2063508555-powerselect-indTipoContrib-2063508555-powerselect-indNatPessoaJur-2063508555-powerselect-indGeracaoRegF600-2063508555-powerselect-indSomarDespRecBruta-2063508555-powerselect-indUsoRetencao-2063508555-powerselect-indUsoCredito`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametros/parametro-apuracao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2695317342-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indFormaTribLucro"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indCodIncidTrib"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indReconReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indMetodApropriacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indTipoContrib"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indNatPessoaJur"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indGeracaoRegF600"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indSomarDespRecBruta"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indUsoRetencao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2063508555-powerselect-indUsoCredito"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo->3709285096-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-novo`, `3709285096-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3709285096-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo->3709285096-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-novo`, `3709285096-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3709285096-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo->3709285096-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-novo`, `3709285096-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3709285096-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo->3709285096-input-rglfCodigo-3709285096-input-descricao-3709285096-powerselect-imposto-3709285096-powerselect-modoGeracao-3709285096-powerselect-indEntradaSaida-3709285096-checkbox-indComplPreco-3709285096-checkbox-indComplImposto-3709285096-checkbox-indComplPrecoImposto-3709285096-powerselect-indSituacaoDof-3709285096-powerselect-indRespFrete-3709285096-powerselect-indSubclasseIdf and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-novo`, `3709285096-input-rglfCodigo-3709285096-input-descricao-3709285096-powerselect-imposto-3709285096-powerselect-modoGeracao-3709285096-powerselect-indEntradaSaida-3709285096-checkbox-indComplPreco-3709285096-checkbox-indComplImposto-3709285096-checkbox-indComplPrecoImposto-3709285096-powerselect-indSituacaoDof-3709285096-powerselect-indRespFrete-3709285096-powerselect-indSubclasseIdf`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-novo"]`);
    cy.fillInput(`[data-cy="3709285096-input-rglfCodigo"] textarea`, `Granite`);
    cy.fillInput(`[data-cy="3709285096-input-descricao"] textarea`, `Auto Loan Account`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-modoGeracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indEntradaSaida"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indSituacaoDof"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indRespFrete"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indSubclasseIdf"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-dof `, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-dof `];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-dof "]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf `, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-item de regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-item de regra`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-item de regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-copiar regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-copiar regra`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-copiar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-verificar regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-verificar regras`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-verificar regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-remover item`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-input-descricao-2672490364-powerselect-imposto-2672490364-powerselect-modoGeracao-2672490364-powerselect-indEntradaSaida-2672490364-checkbox-indComplPreco-2672490364-checkbox-indComplImposto-2672490364-checkbox-indComplPrecoImposto-2672490364-powerselect-indSituacaoDof-2672490364-powerselect-indRespFrete-2672490364-powerselect-indSubclasseIdf and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-input-descricao-2672490364-powerselect-imposto-2672490364-powerselect-modoGeracao-2672490364-powerselect-indEntradaSaida-2672490364-checkbox-indComplPreco-2672490364-checkbox-indComplImposto-2672490364-checkbox-indComplPrecoImposto-2672490364-powerselect-indSituacaoDof-2672490364-powerselect-indRespFrete-2672490364-powerselect-indSubclasseIdf`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1932348065-eyeoutlined"]`);
    cy.fillInput(`[data-cy="2672490364-input-descricao"] textarea`, `online`);
    cy.fillInputPowerSelect(`[data-cy="2672490364-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="2672490364-powerselect-modoGeracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2672490364-powerselect-indEntradaSaida"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2672490364-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2672490364-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2672490364-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="2672490364-powerselect-indSituacaoDof"] input`);
    cy.fillInputPowerSelect(`[data-cy="2672490364-powerselect-indRespFrete"] input`);
    cy.fillInputPowerSelect(`[data-cy="2672490364-powerselect-indSubclasseIdf"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1439317568-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1439317568-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1439317568-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-excluir`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1439317568-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-valor-retido-fonte->2084800543-novo->6403114-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-valor-retido-fonte`, `2084800543-novo`, `6403114-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-valor-retido-fonte/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="6403114-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-valor-retido-fonte->2084800543-novo->6403114-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-valor-retido-fonte`, `2084800543-novo`, `6403114-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-valor-retido-fonte/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="6403114-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-valor-retido-fonte->2084800543-novo->6403114-powerselect-tributo-6403114-input-monetary-vlTotal-6403114-input-monetary-vlDeduzido-6403114-input-monetary-vlRestituicao-6403114-input-monetary-vlCompensacao-6403114-powerselect-indNatRet and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-valor-retido-fonte`, `2084800543-novo`, `6403114-powerselect-tributo-6403114-input-monetary-vlTotal-6403114-input-monetary-vlDeduzido-6403114-input-monetary-vlRestituicao-6403114-input-monetary-vlCompensacao-6403114-powerselect-indNatRet`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-valor-retido-fonte');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2084800543-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="6403114-powerselect-tributo"] input`);
    cy.fillInput(`[data-cy="6403114-input-monetary-vlTotal"] textarea`, `4`);
    cy.fillInput(`[data-cy="6403114-input-monetary-vlDeduzido"] textarea`, `2`);
    cy.fillInput(`[data-cy="6403114-input-monetary-vlRestituicao"] textarea`, `3`);
    cy.fillInput(`[data-cy="6403114-input-monetary-vlCompensacao"] textarea`, `10`);
    cy.fillInputPowerSelect(`[data-cy="6403114-powerselect-indNatRet"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-novo->1680039642-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-novo`, `1680039642-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680039642-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-novo->1680039642-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-novo`, `1680039642-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1680039642-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-novo->1680039642-powerselect-indTipoImposto-1680039642-powerselect-indOrigemCredito-1680039642-powerselect-idTipoCredito-1680039642-input-monetary-vlApurado-1680039642-input-monetary-vlExtApurado-1680039642-input-monetary-vlDescPeriodoAnt-1680039642-input-monetary-vlPedidoRessarcAnt-1680039642-input-monetary-vlDeclCompenInter-1680039642-input-monetary-vlCredito-1680039642-input-monetary-vlRessarc-1680039642-input-monetary-vlCompensacao-1680039642-input-monetary-vlTransferido-1680039642-input-monetary-vlOutrasFormas and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-novo`, `1680039642-powerselect-indTipoImposto-1680039642-powerselect-indOrigemCredito-1680039642-powerselect-idTipoCredito-1680039642-input-monetary-vlApurado-1680039642-input-monetary-vlExtApurado-1680039642-input-monetary-vlDescPeriodoAnt-1680039642-input-monetary-vlPedidoRessarcAnt-1680039642-input-monetary-vlDeclCompenInter-1680039642-input-monetary-vlCredito-1680039642-input-monetary-vlRessarc-1680039642-input-monetary-vlCompensacao-1680039642-input-monetary-vlTransferido-1680039642-input-monetary-vlOutrasFormas`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1680039642-powerselect-indTipoImposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="1680039642-powerselect-indOrigemCredito"] input`);
    cy.fillInputPowerSelect(`[data-cy="1680039642-powerselect-idTipoCredito"] input`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlApurado"] textarea`, `8`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlExtApurado"] textarea`, `5`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlDescPeriodoAnt"] textarea`, `1`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlPedidoRessarcAnt"] textarea`, `10`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlDeclCompenInter"] textarea`, `2`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlCredito"] textarea`, `2`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlRessarc"] textarea`, `4`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlCompensacao"] textarea`, `7`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlTransferido"] textarea`, `10`);
    cy.fillInput(`[data-cy="1680039642-input-monetary-vlOutrasFormas"] textarea`, `9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-visualizar/editar->2350925089-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-visualizar/editar`, `2350925089-remover item`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal/editar/161');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2350925089-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-visualizar/editar->2350925089-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-visualizar/editar`, `2350925089-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal/editar/161');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2350925089-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-visualizar/editar->2350925089-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-visualizar/editar`, `2350925089-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal/editar/161');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2350925089-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/controles->escrituracao-apuracao/controles/controle-credito-fiscal->778956655-visualizar/editar->2350925089-powerselect-indTipoImposto-2350925089-powerselect-indOrigemCredito-2350925089-powerselect-idTipoCredito-2350925089-input-monetary-vlApurado-2350925089-input-monetary-vlExtApurado-2350925089-input-monetary-vlDescPeriodoAnt-2350925089-input-monetary-vlPedidoRessarcAnt-2350925089-input-monetary-vlDeclCompenInter-2350925089-input-monetary-vlCredito-2350925089-input-monetary-vlRessarc-2350925089-input-monetary-vlCompensacao-2350925089-input-monetary-vlTransferido-2350925089-input-monetary-vlOutrasFormas and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/controles`, `escrituracao-apuracao/controles/controle-credito-fiscal`, `778956655-visualizar/editar`, `2350925089-powerselect-indTipoImposto-2350925089-powerselect-indOrigemCredito-2350925089-powerselect-idTipoCredito-2350925089-input-monetary-vlApurado-2350925089-input-monetary-vlExtApurado-2350925089-input-monetary-vlDescPeriodoAnt-2350925089-input-monetary-vlPedidoRessarcAnt-2350925089-input-monetary-vlDeclCompenInter-2350925089-input-monetary-vlCredito-2350925089-input-monetary-vlRessarc-2350925089-input-monetary-vlCompensacao-2350925089-input-monetary-vlTransferido-2350925089-input-monetary-vlOutrasFormas`];
    cy.visit('http://system-A16/escrituracao-apuracao/controles/controle-credito-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="778956655-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="2350925089-powerselect-indTipoImposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="2350925089-powerselect-indOrigemCredito"] input`);
    cy.fillInputPowerSelect(`[data-cy="2350925089-powerselect-idTipoCredito"] input`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlApurado"] textarea`, `6`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlExtApurado"] textarea`, `6`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlDescPeriodoAnt"] textarea`, `2`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlPedidoRessarcAnt"] textarea`, `1`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlDeclCompenInter"] textarea`, `6`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlCredito"] textarea`, `4`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlRessarc"] textarea`, `8`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlCompensacao"] textarea`, `8`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlTransferido"] textarea`, `3`);
    cy.fillInput(`[data-cy="2350925089-input-monetary-vlOutrasFormas"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100->701237180-novo->1262055085-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`, `701237180-novo`, `1262055085-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1262055085-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100->701237180-novo->1262055085-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`, `701237180-novo`, `1262055085-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1262055085-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100->701237180-novo->1262055085-powerselect-codParticipante-1262055085-powerselect-indOperacao-1262055085-powerselect-tipoItem-1262055085-input-monetary-vlOper-1262055085-powerselect-codCstPis-1262055085-input-monetary-vlBcPis-1262055085-input-monetary-aliqPis-1262055085-powerselect-codCstCofins-1262055085-input-monetary-vlBcCofins-1262055085-input-monetary-aliqCofins-1262055085-powerselect-codBcCred-1262055085-powerselect-indOrigemCred-1262055085-input-codContaContabil-1262055085-input-codCcus-1262055085-textarea-descDoc and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100`, `701237180-novo`, `1262055085-powerselect-codParticipante-1262055085-powerselect-indOperacao-1262055085-powerselect-tipoItem-1262055085-input-monetary-vlOper-1262055085-powerselect-codCstPis-1262055085-input-monetary-vlBcPis-1262055085-input-monetary-aliqPis-1262055085-powerselect-codCstCofins-1262055085-input-monetary-vlBcCofins-1262055085-input-monetary-aliqCofins-1262055085-powerselect-codBcCred-1262055085-powerselect-indOrigemCred-1262055085-input-codContaContabil-1262055085-input-codCcus-1262055085-textarea-descDoc`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/doc-geracao-contribuicao-creditos-f100');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="701237180-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-codParticipante"] input`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-indOperacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-tipoItem"] input`);
    cy.fillInput(`[data-cy="1262055085-input-monetary-vlOper"] textarea`, `8`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-codCstPis"] input`);
    cy.fillInput(`[data-cy="1262055085-input-monetary-vlBcPis"] textarea`, `8`);
    cy.fillInput(`[data-cy="1262055085-input-monetary-aliqPis"] textarea`, `6`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-codCstCofins"] input`);
    cy.fillInput(`[data-cy="1262055085-input-monetary-vlBcCofins"] textarea`, `2`);
    cy.fillInput(`[data-cy="1262055085-input-monetary-aliqCofins"] textarea`, `4`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-codBcCred"] input`);
    cy.fillInputPowerSelect(`[data-cy="1262055085-powerselect-indOrigemCred"] input`);
    cy.fillInput(`[data-cy="1262055085-input-codContaContabil"] textarea`, `RAM`);
    cy.fillInput(`[data-cy="1262055085-input-codCcus"] textarea`, `deposit`);
    cy.fillInput(`[data-cy="1262055085-textarea-descDoc"] input`, `Gibraltar Pound`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600->1661905004-novo->3203382269-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`, `1661905004-novo`, `3203382269-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3203382269-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600->1661905004-novo->3203382269-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`, `1661905004-novo`, `3203382269-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3203382269-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600->1661905004-novo->3203382269-powerselect-pfjFontePagadora-3203382269-powerselect-indNatRetenFonte-3203382269-powerselect-indCondDeclarante-3203382269-powerselect-indNatReceita-3203382269-powerselect-codReceita-3203382269-input-monetary-vlBcRetencao-3203382269-input-monetary-vlRetido-3203382269-input-monetary-vlPis-3203382269-input-monetary-vlCofins and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600`, `1661905004-novo`, `3203382269-powerselect-pfjFontePagadora-3203382269-powerselect-indNatRetenFonte-3203382269-powerselect-indCondDeclarante-3203382269-powerselect-indNatReceita-3203382269-powerselect-codReceita-3203382269-input-monetary-vlBcRetencao-3203382269-input-monetary-vlRetido-3203382269-input-monetary-vlPis-3203382269-input-monetary-vlCofins`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/contribuicao-social-retida-fonte-f600');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1661905004-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3203382269-powerselect-pfjFontePagadora"] input`);
    cy.fillInputPowerSelect(`[data-cy="3203382269-powerselect-indNatRetenFonte"] input`);
    cy.fillInputPowerSelect(`[data-cy="3203382269-powerselect-indCondDeclarante"] input`);
    cy.fillInputPowerSelect(`[data-cy="3203382269-powerselect-indNatReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="3203382269-powerselect-codReceita"] input`);
    cy.fillInput(`[data-cy="3203382269-input-monetary-vlBcRetencao"] textarea`, `3`);
    cy.fillInput(`[data-cy="3203382269-input-monetary-vlRetido"] textarea`, `10`);
    cy.fillInput(`[data-cy="3203382269-input-monetary-vlPis"] textarea`, `6`);
    cy.fillInput(`[data-cy="3203382269-input-monetary-vlCofins"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-novo->2181606137-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-novo`, `2181606137-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181606137-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-novo->2181606137-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-novo`, `2181606137-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2181606137-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-novo->2181606137-powerselect-codBaseCalculo-2181606137-powerselect-indTipoBem-2181606137-powerselect-indOrigemCred-2181606137-powerselect-indBemIncorporado-2181606137-input-monetary-vlDepAmortAquis-2181606137-input-monetary-vlParcDepAmortAquis-2181606137-input-monetary-vlBaseCalculo-2181606137-powerselect-qtdParcelas-2181606137-powerselect-codCstPis-2181606137-input-monetary-aliqPis-2181606137-powerselect-codCstCofins-2181606137-input-monetary-aliqCofins-2181606137-input-codContaContabil-2181606137-input-codCcus-2181606137-textarea-descComplementar and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-novo`, `2181606137-powerselect-codBaseCalculo-2181606137-powerselect-indTipoBem-2181606137-powerselect-indOrigemCred-2181606137-powerselect-indBemIncorporado-2181606137-input-monetary-vlDepAmortAquis-2181606137-input-monetary-vlParcDepAmortAquis-2181606137-input-monetary-vlBaseCalculo-2181606137-powerselect-qtdParcelas-2181606137-powerselect-codCstPis-2181606137-input-monetary-aliqPis-2181606137-powerselect-codCstCofins-2181606137-input-monetary-aliqCofins-2181606137-input-codContaContabil-2181606137-input-codCcus-2181606137-textarea-descComplementar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-codBaseCalculo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-indTipoBem"] input`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-indOrigemCred"] input`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-indBemIncorporado"] input`);
    cy.fillInput(`[data-cy="2181606137-input-monetary-vlDepAmortAquis"] textarea`, `1`);
    cy.fillInput(`[data-cy="2181606137-input-monetary-vlParcDepAmortAquis"] textarea`, `5`);
    cy.fillInput(`[data-cy="2181606137-input-monetary-vlBaseCalculo"] textarea`, `8`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-qtdParcelas"] input`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-codCstPis"] input`);
    cy.fillInput(`[data-cy="2181606137-input-monetary-aliqPis"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="2181606137-powerselect-codCstCofins"] input`);
    cy.fillInput(`[data-cy="2181606137-input-monetary-aliqCofins"] textarea`, `1`);
    cy.fillInput(`[data-cy="2181606137-input-codContaContabil"] textarea`, `experiences`);
    cy.fillInput(`[data-cy="2181606137-input-codCcus"] textarea`, `Sleek`);
    cy.fillInput(`[data-cy="2181606137-textarea-descComplementar"] input`, `scale`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-visualizar/editar->2881338880-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-visualizar/editar`, `2881338880-remover item`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130/editar/702');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2881338880-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-visualizar/editar->2881338880-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-visualizar/editar`, `2881338880-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130/editar/702');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2881338880-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-visualizar/editar->2881338880-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-visualizar/editar`, `2881338880-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130/editar/702');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2881338880-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/bloco-f->escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130->3819129840-visualizar/editar->2881338880-powerselect-codBaseCalculo-2881338880-powerselect-indTipoBem-2881338880-powerselect-indOrigemCred-2881338880-powerselect-indBemIncorporado-2881338880-input-monetary-vlDepAmortAquis-2881338880-input-monetary-vlParcDepAmortAquis-2881338880-input-monetary-vlBaseCalculo-2881338880-powerselect-qtdParcelas-2881338880-powerselect-codCstPis-2881338880-input-monetary-aliqPis-2881338880-powerselect-codCstCofins-2881338880-input-monetary-aliqCofins-2881338880-input-codContaContabil-2881338880-input-codCcus-2881338880-textarea-descComplementar and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/bloco-f`, `escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130`, `3819129840-visualizar/editar`, `2881338880-powerselect-codBaseCalculo-2881338880-powerselect-indTipoBem-2881338880-powerselect-indOrigemCred-2881338880-powerselect-indBemIncorporado-2881338880-input-monetary-vlDepAmortAquis-2881338880-input-monetary-vlParcDepAmortAquis-2881338880-input-monetary-vlBaseCalculo-2881338880-powerselect-qtdParcelas-2881338880-powerselect-codCstPis-2881338880-input-monetary-aliqPis-2881338880-powerselect-codCstCofins-2881338880-input-monetary-aliqCofins-2881338880-input-codContaContabil-2881338880-input-codCcus-2881338880-textarea-descComplementar`];
    cy.visit('http://system-A16/escrituracao-apuracao/bloco-f/cred-ativ-imob-f120-f130');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3819129840-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-codBaseCalculo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-indTipoBem"] input`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-indOrigemCred"] input`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-indBemIncorporado"] input`);
    cy.fillInput(`[data-cy="2881338880-input-monetary-vlDepAmortAquis"] textarea`, `6`);
    cy.fillInput(`[data-cy="2881338880-input-monetary-vlParcDepAmortAquis"] textarea`, `6`);
    cy.fillInput(`[data-cy="2881338880-input-monetary-vlBaseCalculo"] textarea`, `8`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-qtdParcelas"] input`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-codCstPis"] input`);
    cy.fillInput(`[data-cy="2881338880-input-monetary-aliqPis"] textarea`, `10`);
    cy.fillInputPowerSelect(`[data-cy="2881338880-powerselect-codCstCofins"] input`);
    cy.fillInput(`[data-cy="2881338880-input-monetary-aliqCofins"] textarea`, `5`);
    cy.fillInput(`[data-cy="2881338880-input-codContaContabil"] textarea`, `Visionoriented`);
    cy.fillInput(`[data-cy="2881338880-input-codCcus"] textarea`, `utilize`);
    cy.fillInput(`[data-cy="2881338880-textarea-descComplementar"] input`, `Businessfocused`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-remover item`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-salvar`, `345568596-remover item`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/editar/361');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="345568596-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-salvar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-salvar`, `345568596-salvar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/editar/361');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="345568596-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-voltar`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-salvar`, `345568596-voltar`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/editar/361');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="345568596-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-input-monetary-vlTribMercInterno-345568596-input-monetary-vlNaoTribMercInterno-345568596-input-monetary-vlExportacao-345568596-input-monetary-vlCumulativa and submit`, () => {
    const actualId = [`root`, `apuracao-contribuicoes`, `apuracao-contribuicoes/receita-bruta-mensal`, `622944540-novo`, `2686775629-salvar`, `345568596-input-monetary-vlTribMercInterno-345568596-input-monetary-vlNaoTribMercInterno-345568596-input-monetary-vlExportacao-345568596-input-monetary-vlCumulativa`];
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2686775629-salvar"]`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlTribMercInterno"] textarea`, `8`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlNaoTribMercInterno"] textarea`, `4`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlExportacao"] textarea`, `9`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlCumulativa"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/solicitacoes->2037403718-agendamentos->3119776687-visualização->3119776687-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes`, `2037403718-agendamentos`, `3119776687-visualização`, `3119776687-item-`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?sigla=~eq~EFD-PIS-COFINS%7C%7CEFD-PIS-COFINS&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3119776687-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3119776687-abrir visualização->3119776687-expandir->3119776687-diminuir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `3119776687-abrir visualização`, `3119776687-expandir`, `3119776687-diminuir`];
    cy.visit('http://system-A16/obrigacoes/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3119776687-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3119776687-expandir"]`);
    cy.clickIfExist(`[data-cy="3119776687-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-executar->2257824164-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-executar`, `2257824164-múltipla seleção`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-executar"]`);
    cy.clickIfExist(`[data-cy="2257824164-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-executar->2257824164-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-executar`, `2257824164-agendar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-executar"]`);
    cy.clickIfExist(`[data-cy="2257824164-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-agendamentos->2257824164-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-agendamentos`, `2257824164-voltar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26669932D%7C%7C26669932&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-visualização->2257824164-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-visualização`, `2257824164-item-`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2257824164-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-executar->1745771065-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-executar`, `1745771065-múltipla seleção`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-executar"]`);
    cy.clickIfExist(`[data-cy="1745771065-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-executar->1745771065-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-executar`, `1745771065-agendar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-executar"]`);
    cy.clickIfExist(`[data-cy="1745771065-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-agendamentos->1745771065-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-agendamentos`, `1745771065-voltar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26732508D%7C%7C26732508&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-visualização->1745771065-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-visualização`, `1745771065-item-`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1745771065-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-executar->893456424-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-executar`, `893456424-múltipla seleção`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-executar"]`);
    cy.clickIfExist(`[data-cy="893456424-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-executar->893456424-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-executar`, `893456424-agendar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-executar"]`);
    cy.clickIfExist(`[data-cy="893456424-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-executar->893456424-input-P_PERIODO and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-executar`, `893456424-input-P_PERIODO`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-executar"]`);
    cy.fillInput(`[data-cy="893456424-input-P_PERIODO"] textarea`, `Ergonomic`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-agendamentos->893456424-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-agendamentos`, `893456424-voltar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46704477D%7C%7C46704477&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-visualização->893456424-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-visualização`, `893456424-item-`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="893456424-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4165638550-visualização->4165638550-salvar configuração`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4165638550-visualização`, `4165638550-salvar configuração`];
    cy.visit('http://system-A16/relatorios/inconsistencias/dof-sem-item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4165638550-visualização"]`);
    cy.clickIfExist(`[data-cy="4165638550-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-executar->65189148-múltipla seleção->65189148-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-executar`, `65189148-múltipla seleção`, `65189148-cancelar`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-executar"]`);
    cy.clickIfExist(`[data-cy="65189148-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="65189148-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/gera-lfis->65189148-abrir visualização->65189148-expandir->65189148-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lfis`, `65189148-abrir visualização`, `65189148-expandir`, `65189148-diminuir`];
    cy.visit('http://system-A16/processos/gera-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="65189148-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="65189148-expandir"]`);
    cy.clickIfExist(`[data-cy="65189148-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/deleta-lfis-periodo->3501244739-executar->3501244739-múltipla seleção->3501244739-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/deleta-lfis-periodo`, `3501244739-executar`, `3501244739-múltipla seleção`, `3501244739-cancelar`];
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3501244739-executar"]`);
    cy.clickIfExist(`[data-cy="3501244739-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3501244739-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-executar->2080364286-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-executar`, `2080364286-múltipla seleção`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-executar"]`);
    cy.clickIfExist(`[data-cy="2080364286-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-executar->2080364286-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-executar`, `2080364286-agendar`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-executar"]`);
    cy.clickIfExist(`[data-cy="2080364286-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-agendamentos->2080364286-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-agendamentos`, `2080364286-voltar`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~24768407D%7C%7C24768407&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-visualização->2080364286-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-visualização`, `2080364286-item-`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2080364286-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-executar->1572238036-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-executar`, `1572238036-múltipla seleção`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-executar"]`);
    cy.clickIfExist(`[data-cy="1572238036-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-executar->1572238036-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-executar`, `1572238036-agendar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-executar"]`);
    cy.clickIfExist(`[data-cy="1572238036-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-agendamentos->1572238036-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-agendamentos`, `1572238036-voltar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26807343D%7C%7C26807343&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-visualização->1572238036-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-visualização`, `1572238036-item-`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1572238036-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-executar->2189535267-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-executar`, `2189535267-múltipla seleção`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-executar"]`);
    cy.clickIfExist(`[data-cy="2189535267-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-executar->2189535267-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-executar`, `2189535267-agendar`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-executar"]`);
    cy.clickIfExist(`[data-cy="2189535267-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-agendamentos->2189535267-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-agendamentos`, `2189535267-voltar`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~24768413D%7C%7C24768413&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-visualização->2189535267-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-visualização`, `2189535267-item-`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2189535267-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-executar->4085456520-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-executar`, `4085456520-múltipla seleção`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-executar"]`);
    cy.clickIfExist(`[data-cy="4085456520-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-executar->4085456520-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-executar`, `4085456520-agendar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-executar"]`);
    cy.clickIfExist(`[data-cy="4085456520-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-agendamentos->4085456520-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-agendamentos`, `4085456520-voltar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26595028D%7C%7C26595028&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-visualização->4085456520-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-visualização`, `4085456520-item-`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4085456520-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-executar->3904502845-múltipla seleção->3904502845-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-executar`, `3904502845-múltipla seleção`, `3904502845-cancelar`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-executar"]`);
    cy.clickIfExist(`[data-cy="3904502845-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3904502845-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-abrir visualização->3904502845-expandir->3904502845-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/exclusao-icms-base-calculo-pis-cofins`, `3904502845-abrir visualização`, `3904502845-expandir`, `3904502845-diminuir`];
    cy.visit('http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3904502845-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3904502845-expandir"]`);
    cy.clickIfExist(`[data-cy="3904502845-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-dof ->2672490364-item-estabelecimento`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-dof `, `2672490364-item-estabelecimento`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-dof "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-dof ->2672490364-item-espécie`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-dof `, `2672490364-item-espécie`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-dof "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-espécie"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-dof ->2672490364-item-pfj`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-dof `, `2672490364-item-pfj`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-dof "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-pfj"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-nop`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-nop`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-cfop`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-cfop`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-cfop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-ncm`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-ncm`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-ncm"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-sta`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-sta`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-sta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-stn`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-stn`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-stn"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-prestação`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-prestação`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-prestação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-mercadoria`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-mercadoria`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-copiar regra->2672490364-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-copiar regra`, `2672490364-cancelar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-copiar regra"]`);
    cy.clickIfExist(`[data-cy="2672490364-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-copiar regra->2672490364-input-regFonte and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-copiar regra`, `2672490364-input-regFonte`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-copiar regra"]`);
    cy.fillInput(`[data-cy="2672490364-input-regFonte"] textarea`, `Auto Loan Account`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-verificar regras->2672490364-ok`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-verificar regras`, `2672490364-ok`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-verificar regras"]`);
    cy.clickIfExist(`[data-cy="2672490364-ok"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-verificar regras->2672490364-radio-type and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-verificar regras`, `2672490364-radio-type`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-verificar regras"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2672490364-radio-type"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo->4109150889-copiar item de uma regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`, `4109150889-copiar item de uma regra`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4109150889-copiar item de uma regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo->4109150889-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`, `4109150889-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4109150889-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo->4109150889-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`, `4109150889-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4109150889-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo->4109150889-input-irglfCodigo-4109150889-powerselect-defMercCodigo-4109150889-powerselect-defPresCodigo-4109150889-powerselect-defFiscalUniCodigo-4109150889-powerselect-defVlMercad-4109150889-powerselect-defPrecoTotal-4109150889-powerselect-defQuantidade-4109150889-powerselect-defOmCodigo-4109150889-powerselect-defStcCodigo-4109150889-powerselect-defStpCodigo-4109150889-powerselect-defStaCodigo-4109150889-powerselect-defStnCodigo-4109150889-powerselect-defCodBcCredito-4109150889-powerselect-defNfeLocalizador-4109150889-powerselect-defCfopCodigo-4109150889-powerselect-defNopCodigo-4109150889-powerselect-defNbmCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`, `4109150889-input-irglfCodigo-4109150889-powerselect-defMercCodigo-4109150889-powerselect-defPresCodigo-4109150889-powerselect-defFiscalUniCodigo-4109150889-powerselect-defVlMercad-4109150889-powerselect-defPrecoTotal-4109150889-powerselect-defQuantidade-4109150889-powerselect-defOmCodigo-4109150889-powerselect-defStcCodigo-4109150889-powerselect-defStpCodigo-4109150889-powerselect-defStaCodigo-4109150889-powerselect-defStnCodigo-4109150889-powerselect-defCodBcCredito-4109150889-powerselect-defNfeLocalizador-4109150889-powerselect-defCfopCodigo-4109150889-powerselect-defNopCodigo-4109150889-powerselect-defNbmCodigo`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1439317568-novo"]`);
    cy.fillInput(`[data-cy="4109150889-input-irglfCodigo"] textarea`, `primary`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defMercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defPresCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defFiscalUniCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defVlMercad"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defPrecoTotal"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defQuantidade"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defOmCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defStcCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defStpCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defStaCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defStnCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defCodBcCredito"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defNfeLocalizador"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defCfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defNopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-defNbmCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-copiar item de uma regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-copiar item de uma regra`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2294677020-copiar item de uma regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-remover item`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2294677020-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-salvar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2294677020-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-voltar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2294677020-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-input-irglfCodigo-2294677020-powerselect-defMercCodigo-2294677020-powerselect-defPresCodigo-2294677020-powerselect-defFiscalUniCodigo-2294677020-powerselect-defVlMercad-2294677020-powerselect-defPrecoTotal-2294677020-powerselect-defQuantidade-2294677020-powerselect-defOmCodigo-2294677020-powerselect-defStcCodigo-2294677020-powerselect-defStpCodigo-2294677020-powerselect-defStaCodigo-2294677020-powerselect-defStnCodigo-2294677020-powerselect-defCodBcCredito-2294677020-powerselect-defNfeLocalizador-2294677020-powerselect-defCfopCodigo-2294677020-powerselect-defNopCodigo-2294677020-powerselect-defNbmCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-input-irglfCodigo-2294677020-powerselect-defMercCodigo-2294677020-powerselect-defPresCodigo-2294677020-powerselect-defFiscalUniCodigo-2294677020-powerselect-defVlMercad-2294677020-powerselect-defPrecoTotal-2294677020-powerselect-defQuantidade-2294677020-powerselect-defOmCodigo-2294677020-powerselect-defStcCodigo-2294677020-powerselect-defStpCodigo-2294677020-powerselect-defStaCodigo-2294677020-powerselect-defStnCodigo-2294677020-powerselect-defCodBcCredito-2294677020-powerselect-defNfeLocalizador-2294677020-powerselect-defCfopCodigo-2294677020-powerselect-defNopCodigo-2294677020-powerselect-defNbmCodigo`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1439317568-visualizar/editar"]`);
    cy.fillInput(`[data-cy="2294677020-input-irglfCodigo"] textarea`, `bypass`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defMercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defPresCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defFiscalUniCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defVlMercad"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defPrecoTotal"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defQuantidade"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defOmCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defStcCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defStpCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defStaCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defStnCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defCodBcCredito"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defNfeLocalizador"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defCfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defNopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-defNbmCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-executar->2257824164-múltipla seleção->2257824164-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/item-grupo-regra`, `2257824164-executar`, `2257824164-múltipla seleção`, `2257824164-cancelar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2257824164-executar"]`);
    cy.clickIfExist(`[data-cy="2257824164-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2257824164-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-executar->1745771065-múltipla seleção->1745771065-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/documentos-lancamentos-cupons`, `1745771065-executar`, `1745771065-múltipla seleção`, `1745771065-cancelar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1745771065-executar"]`);
    cy.clickIfExist(`[data-cy="1745771065-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1745771065-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-executar->893456424-múltipla seleção->893456424-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/credito-presumido`, `relatorios/credito-presumido/demonstrativo-de-report-gerado`, `893456424-executar`, `893456424-múltipla seleção`, `893456424-cancelar`];
    cy.visit('http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="893456424-executar"]`);
    cy.clickIfExist(`[data-cy="893456424-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="893456424-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-executar->2080364286-múltipla seleção->2080364286-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/geracao-de-dados-para-report`, `2080364286-executar`, `2080364286-múltipla seleção`, `2080364286-cancelar`];
    cy.visit('http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2080364286-executar"]`);
    cy.clickIfExist(`[data-cy="2080364286-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2080364286-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-executar->1572238036-múltipla seleção->1572238036-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-report-hierarquia`, `1572238036-executar`, `1572238036-múltipla seleção`, `1572238036-cancelar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1572238036-executar"]`);
    cy.clickIfExist(`[data-cy="1572238036-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1572238036-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-executar->2189535267-múltipla seleção->2189535267-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/deleta-geracao-dados-report`, `2189535267-executar`, `2189535267-múltipla seleção`, `2189535267-cancelar`];
    cy.visit('http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2189535267-executar"]`);
    cy.clickIfExist(`[data-cy="2189535267-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2189535267-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-executar->4085456520-múltipla seleção->4085456520-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/credito-presumido`, `processos/credito-presumido/gera-dados-gerenciais`, `4085456520-executar`, `4085456520-múltipla seleção`, `4085456520-cancelar`];
    cy.visit('http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4085456520-executar"]`);
    cy.clickIfExist(`[data-cy="4085456520-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4085456520-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-dof ->2672490364-item-espécie->2672490364-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-dof `, `2672490364-item-espécie`, `2672490364-power-search-button`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-dof "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-espécie"]`);
    cy.clickIfExist(`[data-cy="2672490364-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-dof ->2672490364-item-espécie->2672490364-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-dof `, `2672490364-item-espécie`, `2672490364-power-search-input`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-dof "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-espécie"]`);
    cy.fillInputPowerSearch(`[data-cy="2672490364-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-nop->2672490364-dashoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-nop`, `2672490364-dashoutlined`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-nop"]`);
    cy.clickIfExist(`[data-cy="2672490364-dashoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-verificar regras->2672490364-ok->2672490364-download`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-verificar regras`, `2672490364-ok`, `2672490364-download`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-verificar regras"]`);
    cy.clickIfExist(`[data-cy="2672490364-ok"]`);
    cy.clickIfExist(`[data-cy="2672490364-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo->4109150889-copiar item de uma regra->4109150889-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`, `4109150889-copiar item de uma regra`, `4109150889-cancelar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4109150889-copiar item de uma regra"]`);
    cy.clickIfExist(`[data-cy="4109150889-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-novo->4109150889-copiar item de uma regra->4109150889-powerselect-rglfFonte and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-novo`, `4109150889-copiar item de uma regra`, `4109150889-powerselect-rglfFonte`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4109150889-copiar item de uma regra"]`);
    cy.fillInputPowerSelect(`[data-cy="4109150889-powerselect-rglfFonte"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-copiar item de uma regra->2294677020-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-copiar item de uma regra`, `2294677020-cancelar`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2294677020-copiar item de uma regra"]`);
    cy.clickIfExist(`[data-cy="2294677020-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-unorderedlistoutlined->1439317568-visualizar/editar->2294677020-copiar item de uma regra->2294677020-powerselect-rglfFonte and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-unorderedlistoutlined`, `1439317568-visualizar/editar`, `2294677020-copiar item de uma regra`, `2294677020-powerselect-rglfFonte`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/DEFP/item-regra-lancamento-fiscal/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2294677020-copiar item de uma regra"]`);
    cy.fillInputPowerSelect(`[data-cy="2294677020-powerselect-rglfFonte"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-eyeoutlined->2672490364-idf ->2672490364-item-nop->2672490364-dashoutlined->2672490364-item-`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal`, `1932348065-eyeoutlined`, `2672490364-idf `, `2672490364-item-nop`, `2672490364-dashoutlined`, `2672490364-item-`];
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal/editar/DEFP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2672490364-idf "]`);
    cy.clickIfExist(`[data-cy="2672490364-item-nop"]`);
    cy.clickIfExist(`[data-cy="2672490364-dashoutlined"]`);
    cy.clickIfExist(`[data-cy="2672490364-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
