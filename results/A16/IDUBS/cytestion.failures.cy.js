describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-input-monetary-vlTribMercInterno-2686775629-input-monetary-vlNaoTribMercInterno-2686775629-input-monetary-vlExportacao-2686775629-input-monetary-vlCumulativa and submit`, () => {
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="622944540-novo"]`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlTribMercInterno"] textarea`, `2`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlNaoTribMercInterno"] textarea`, `1`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlExportacao"] textarea`, `5`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlCumulativa"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/gera-lfis->65189148-agendamentos->65189148-voltar`, () => {
    cy.visit('http://system-A16/processos/gera-lfis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~57673102D%7C%7C57673102&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="65189148-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/deleta-lfis-periodo->3501244739-agendamentos->3501244739-voltar`, () => {
    cy.visit('http://system-A16/processos/deleta-lfis-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~57305999D%7C%7C57305999&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3501244739-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/exclusao-icms-base-calculo-pis-cofins->3904502845-agendamentos->3904502845-voltar`, () => {
    cy.visit(
      'http://system-A16/processos/exclusao-icms-base-calculo-pis-cofins?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~48767671D%7C%7C48767671&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3904502845-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo->3709285096-input-rglfCodigo-3709285096-input-descricao-3709285096-powerselect-imposto-3709285096-powerselect-modoGeracao-3709285096-powerselect-indEntradaSaida-3709285096-checkbox-indComplPreco-3709285096-checkbox-indComplImposto-3709285096-checkbox-indComplPrecoImposto-3709285096-powerselect-indSituacaoDof-3709285096-powerselect-indRespFrete-3709285096-powerselect-indSubclasseIdf and submit`, () => {
    cy.visit('http://system-A16/escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1932348065-novo"]`);
    cy.fillInput(`[data-cy="3709285096-input-rglfCodigo"] textarea`, `Granite`);
    cy.fillInput(`[data-cy="3709285096-input-descricao"] textarea`, `Auto Loan Account`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-modoGeracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indEntradaSaida"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indSituacaoDof"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indRespFrete"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indSubclasseIdf"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-input-monetary-vlTribMercInterno-345568596-input-monetary-vlNaoTribMercInterno-345568596-input-monetary-vlExportacao-345568596-input-monetary-vlCumulativa and submit`, () => {
    cy.visit('http://system-A16/apuracao-contribuicoes/receita-bruta-mensal/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2686775629-salvar"]`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlTribMercInterno"] textarea`, `8`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlNaoTribMercInterno"] textarea`, `4`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlExportacao"] textarea`, `9`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlCumulativa"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/item-grupo-regra->2257824164-agendamentos->2257824164-voltar`, () => {
    cy.visit(
      'http://system-A16/relatorios/credito-presumido/item-grupo-regra?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26669932D%7C%7C26669932&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2257824164-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/documentos-lancamentos-cupons->1745771065-agendamentos->1745771065-voltar`, () => {
    cy.visit(
      'http://system-A16/relatorios/credito-presumido/documentos-lancamentos-cupons?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26732508D%7C%7C26732508&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1745771065-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/credito-presumido->relatorios/credito-presumido/demonstrativo-de-report-gerado->893456424-agendamentos->893456424-voltar`, () => {
    cy.visit(
      'http://system-A16/relatorios/credito-presumido/demonstrativo-de-report-gerado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46704477D%7C%7C46704477&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="893456424-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/credito-presumido->processos/credito-presumido/geracao-de-dados-para-report->2080364286-agendamentos->2080364286-voltar`, () => {
    cy.visit(
      'http://system-A16/processos/credito-presumido/geracao-de-dados-para-report?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~24768407D%7C%7C24768407&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2080364286-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-report-hierarquia->1572238036-agendamentos->1572238036-voltar`, () => {
    cy.visit(
      'http://system-A16/processos/credito-presumido/gera-dados-report-hierarquia?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26807343D%7C%7C26807343&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1572238036-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/credito-presumido->processos/credito-presumido/deleta-geracao-dados-report->2189535267-agendamentos->2189535267-voltar`, () => {
    cy.visit(
      'http://system-A16/processos/credito-presumido/deleta-geracao-dados-report?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~24768413D%7C%7C24768413&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2189535267-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/credito-presumido->processos/credito-presumido/gera-dados-gerenciais->4085456520-agendamentos->4085456520-voltar`, () => {
    cy.visit(
      'http://system-A16/processos/credito-presumido/gera-dados-gerenciais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~26595028D%7C%7C26595028&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4085456520-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
