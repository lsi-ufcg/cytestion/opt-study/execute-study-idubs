describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-input-monetary-vlTribMercInterno-2686775629-input-monetary-vlNaoTribMercInterno-2686775629-input-monetary-vlExportacao-2686775629-input-monetary-vlCumulativa and submit`, () => {
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes/receita-bruta-mensal"]`);
    cy.clickCauseExist(`[data-cy="622944540-novo"]`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlTribMercInterno"] textarea`, `8`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlNaoTribMercInterno"] textarea`, `3`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlExportacao"] textarea`, `5`);
    cy.fillInput(`[data-cy="2686775629-input-monetary-vlCumulativa"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal->1932348065-novo->3709285096-input-rglfCodigo-3709285096-input-descricao-3709285096-powerselect-imposto-3709285096-powerselect-modoGeracao-3709285096-powerselect-indEntradaSaida-3709285096-checkbox-indComplPreco-3709285096-checkbox-indComplImposto-3709285096-checkbox-indComplPrecoImposto-3709285096-powerselect-indSituacaoDof-3709285096-powerselect-indRespFrete-3709285096-powerselect-indSubclasseIdf and submit`, () => {
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/lancamento-fiscal"]`);
    cy.clickCauseExist(`[data-cy="1932348065-novo"]`);
    cy.fillInput(`[data-cy="3709285096-input-rglfCodigo"] textarea`, `RAM`);
    cy.fillInput(`[data-cy="3709285096-input-descricao"] textarea`, `hack`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-modoGeracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indEntradaSaida"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3709285096-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indSituacaoDof"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indRespFrete"] input`);
    cy.fillInputPowerSelect(`[data-cy="3709285096-powerselect-indSubclasseIdf"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-remover item`, () => {
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes/receita-bruta-mensal"]`);
    cy.clickCauseExist(`[data-cy="622944540-novo"]`);
    cy.clickCauseExist(`[data-cy="2686775629-salvar"]`);
    cy.clickCauseExist(`[data-cy="345568596-remover item"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-salvar`, () => {
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes/receita-bruta-mensal"]`);
    cy.clickCauseExist(`[data-cy="622944540-novo"]`);
    cy.clickCauseExist(`[data-cy="2686775629-salvar"]`);
    cy.clickCauseExist(`[data-cy="345568596-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-voltar`, () => {
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes/receita-bruta-mensal"]`);
    cy.clickCauseExist(`[data-cy="622944540-novo"]`);
    cy.clickCauseExist(`[data-cy="2686775629-salvar"]`);
    cy.clickCauseExist(`[data-cy="345568596-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values apuracao-contribuicoes->apuracao-contribuicoes/receita-bruta-mensal->622944540-novo->2686775629-salvar->345568596-input-monetary-vlTribMercInterno-345568596-input-monetary-vlNaoTribMercInterno-345568596-input-monetary-vlExportacao-345568596-input-monetary-vlCumulativa and submit`, () => {
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes"]`);
    cy.clickCauseExist(`[data-cy="apuracao-contribuicoes/receita-bruta-mensal"]`);
    cy.clickCauseExist(`[data-cy="622944540-novo"]`);
    cy.clickCauseExist(`[data-cy="2686775629-salvar"]`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlTribMercInterno"] textarea`, `9`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlNaoTribMercInterno"] textarea`, `10`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlExportacao"] textarea`, `8`);
    cy.fillInput(`[data-cy="345568596-input-monetary-vlCumulativa"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
