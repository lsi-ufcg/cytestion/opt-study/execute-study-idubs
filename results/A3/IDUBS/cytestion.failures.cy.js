describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values regras->regras/configuracao-estabelecimento->2012385885-novo->1951752364-powerselect-codigoUsual-1951752364-powerselect-plcntCodigo-1951752364-powerselect-itfCodigo-1951752364-powerselect-cntcfgId and submit`, () => {
    cy.visit('http://system-A3/regras/est-extensao');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2012385885-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-codigoUsual"] input`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-plcntCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-itfCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-cntcfgId"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/executa-contabilizacao->923473359-agendamentos->923473359-voltar`, () => {
    cy.visit('http://system-A3/processos/executa-contabilizacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210566D%7C%7C210566&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="923473359-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-agendamentos->1622304045-voltar`, () => {
    cy.visit('http://system-A3/relatorios/criticas-contabilizacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210557D%7C%7C210557&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1622304045-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-agendamentos->4205939014-voltar`, () => {
    cy.visit('http://system-A3/relatorios/situacao-processos-contabilizacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210558D%7C%7C210558&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4205939014-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-agendamentos->1463183472-voltar`, () => {
    cy.visit('http://system-A3/relatorios/analise-periodo-contabil?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210559D%7C%7C210559&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1463183472-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-agendamentos->2520482827-voltar`, () => {
    cy.visit('http://system-A3/relatorios/enquadramento-contabil-processo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210560D%7C%7C210560&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2520482827-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-agendamentos->3614868856-voltar`, () => {
    cy.visit('http://system-A3/relatorios/enquadramento-contabil-regra?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210561D%7C%7C210561&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3614868856-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-agendamentos->3469256492-voltar`, () => {
    cy.visit('http://system-A3/relatorios/lancamento-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210562D%7C%7C210562&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3469256492-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-agendamentos->1751406876-voltar`, () => {
    cy.visit('http://system-A3/relatorios/resumo-lancamento-cantabeis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210563D%7C%7C210563&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1751406876-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/regras-contabeis->597180432-agendamentos->597180432-voltar`, () => {
    cy.visit('http://system-A3/relatorios/regras-contabeis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210564D%7C%7C210564&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="597180432-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-agendamentos->3522401022-voltar`, () => {
    cy.visit('http://system-A3/relatorios/consulta-contabilizacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~214428D%7C%7C214428&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3522401022-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra->3700427807-novo`, () => {
    cy.visit('http://system-A3/regras/lancamentos-contabeis/7349/626/D/transacao-contabil-lancamento');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3700427807-novo"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra->3700427807-visualizar/editar`, () => {
    cy.visit('http://system-A3/regras/lancamentos-contabeis/7349/626/D/transacao-contabil-lancamento');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3700427807-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
  });
});
