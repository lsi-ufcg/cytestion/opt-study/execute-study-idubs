describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis`, () => {
const actualId = [`root`,`tabelas-contabeis`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras`, () => {
const actualId = [`root`,`regras`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao`, () => {
const actualId = [`root`,`contabilizacao`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/dof`, () => {
const actualId = [`root`,`transacoes`,`transacoes/dof`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/consulta-documento`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/consulta-documento`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/consulta-documento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/solicitacao`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/solicitacao`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/solicitacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->3599192363-power-search-button`, () => {
const actualId = [`root`,`download`,`3599192363-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="3599192363-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->3599192363-download`, () => {
const actualId = [`root`,`download`,`3599192363-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="3599192363-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->3599192363-detalhes`, () => {
const actualId = [`root`,`download`,`3599192363-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="3599192363-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->3599192363-excluir`, () => {
const actualId = [`root`,`download`,`3599192363-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="3599192363-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-novo`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-novo`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-power-search-button`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-visualizar/editar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-visualizar/editar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-excluir`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-excluir`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-novo`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-novo`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-power-search-button`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-visualizar/editar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-visualizar/editar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-excluir`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-excluir`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-novo`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-novo`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-power-search-button`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-excluir`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-excluir`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-novo`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-visualizar/editar`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-visualizar/editar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-excluir`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-excluir`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-novo`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-visualizar/editar`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-visualizar/editar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-excluir`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-excluir`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-novo`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-excluir`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-excluir`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-novo`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-excluir`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-excluir`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/consulta-documento->105201589-power-search-button`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/consulta-documento`,`105201589-power-search-button`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/consulta-documento"]`);
      cy.clickIfExist(`[data-cy="105201589-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/solicitacao->4102354395-novo`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/solicitacao`,`4102354395-novo`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/solicitacao"]`);
      cy.clickIfExist(`[data-cy="4102354395-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/solicitacao->4102354395-power-search-button`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/solicitacao`,`4102354395-power-search-button`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/solicitacao"]`);
      cy.clickIfExist(`[data-cy="4102354395-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-executar`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-visualização`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-visualizar/editar->3183655557-remover item`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-visualizar/editar`,`3183655557-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3183655557-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-visualizar/editar->3183655557-salvar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-visualizar/editar`,`3183655557-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3183655557-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-visualizar/editar->3183655557-voltar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-visualizar/editar`,`3183655557-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3183655557-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-contabeis->tabelas-contabeis/centro-custo->1228861579-visualizar/editar->3183655557-input-codigo-3183655557-textarea-nome and submit`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/centro-custo`,`1228861579-visualizar/editar`,`3183655557-input-codigo-3183655557-textarea-nome`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/centro-custo"]`);
      cy.clickIfExist(`[data-cy="1228861579-visualizar/editar"]`);
      cy.fillInput(`[data-cy="3183655557-input-codigo"] textarea`, `Hat`);
cy.fillInput(`[data-cy="3183655557-textarea-nome"] input`, `Samoa`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-novo->2536889396-salvar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-novo`,`2536889396-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-novo"]`);
      cy.clickIfExist(`[data-cy="2536889396-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-novo->2536889396-voltar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-novo`,`2536889396-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-novo"]`);
      cy.clickIfExist(`[data-cy="2536889396-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-contabeis->tabelas-contabeis/historicos->654639573-novo->2536889396-input-codigo-2536889396-input-descricao and submit`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-novo`,`2536889396-input-codigo-2536889396-input-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-novo"]`);
      cy.fillInput(`[data-cy="2536889396-input-codigo"] textarea`, `Hat`);
cy.fillInput(`[data-cy="2536889396-input-descricao"] textarea`, `Handmade`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-visualizar/editar->2912016123-remover item`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-visualizar/editar`,`2912016123-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2912016123-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-visualizar/editar->2912016123-salvar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-visualizar/editar`,`2912016123-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2912016123-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/historicos->654639573-visualizar/editar->2912016123-voltar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-visualizar/editar`,`2912016123-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2912016123-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-contabeis->tabelas-contabeis/historicos->654639573-visualizar/editar->2912016123-input-codigo-2912016123-input-descricao and submit`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/historicos`,`654639573-visualizar/editar`,`2912016123-input-codigo-2912016123-input-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/historicos"]`);
      cy.clickIfExist(`[data-cy="654639573-visualizar/editar"]`);
      cy.fillInput(`[data-cy="2912016123-input-codigo"] textarea`, `interface`);
cy.fillInput(`[data-cy="2912016123-input-descricao"] textarea`, `Santa Catarina`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-novo->1062901772-salvar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-novo`,`1062901772-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-novo"]`);
      cy.clickIfExist(`[data-cy="1062901772-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-novo->1062901772-voltar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-novo`,`1062901772-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-novo"]`);
      cy.clickIfExist(`[data-cy="1062901772-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-novo->1062901772-input-codigo-1062901772-textarea-descricao and submit`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-novo`,`1062901772-input-codigo-1062901772-textarea-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-novo"]`);
      cy.fillInput(`[data-cy="1062901772-input-codigo"] textarea`, `approach`);
cy.fillInput(`[data-cy="1062901772-textarea-descricao"] input`, `grey`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-remover item`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-remover item`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-salvar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-salvar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-voltar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-voltar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-novo`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-novo`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-power-search-button`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-power-search-button`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-visualizar/editar`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-visualizar/editar`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-excluir`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-excluir`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="726973635-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values tabelas-contabeis->tabelas-contabeis/plano-contas->2208317181-visualizar/editar->726973635-input-codigo-726973635-textarea-descricao and submit`, () => {
const actualId = [`root`,`tabelas-contabeis`,`tabelas-contabeis/plano-contas`,`2208317181-visualizar/editar`,`726973635-input-codigo-726973635-textarea-descricao`];
    cy.clickIfExist(`[data-cy="tabelas-contabeis"]`);
      cy.clickIfExist(`[data-cy="tabelas-contabeis/plano-contas"]`);
      cy.clickIfExist(`[data-cy="2208317181-visualizar/editar"]`);
      cy.fillInput(`[data-cy="726973635-input-codigo"] textarea`, `Jordanian Dinar`);
cy.fillInput(`[data-cy="726973635-textarea-descricao"] input`, `synthesizing`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-novo->2857022235-salvar`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-novo`,`2857022235-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-novo"]`);
      cy.clickIfExist(`[data-cy="2857022235-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-novo->2857022235-voltar`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-novo`,`2857022235-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-novo"]`);
      cy.clickIfExist(`[data-cy="2857022235-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/configuracoes-lotes->2099328654-novo->2857022235-textarea-titulo-2857022235-powerselect-agrupamento1-2857022235-powerselect-agrupamento2-2857022235-powerselect-loteUn-2857022235-powerselect-cninfIdPrefixoLote and submit`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-novo`,`2857022235-textarea-titulo-2857022235-powerselect-agrupamento1-2857022235-powerselect-agrupamento2-2857022235-powerselect-loteUn-2857022235-powerselect-cninfIdPrefixoLote`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-novo"]`);
      cy.fillInput(`[data-cy="2857022235-textarea-titulo"] input`, `methodologies`);
cy.fillInputPowerSelect(`[data-cy="2857022235-powerselect-agrupamento1"] input`);
cy.fillInputPowerSelect(`[data-cy="2857022235-powerselect-agrupamento2"] input`);
cy.fillInputPowerSelect(`[data-cy="2857022235-powerselect-loteUn"] input`);
cy.fillInputPowerSelect(`[data-cy="2857022235-powerselect-cninfIdPrefixoLote"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-visualizar/editar->1391305890-remover item`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-visualizar/editar`,`1391305890-remover item`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1391305890-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-visualizar/editar->1391305890-salvar`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-visualizar/editar`,`1391305890-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1391305890-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracoes-lotes->2099328654-visualizar/editar->1391305890-voltar`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-visualizar/editar`,`1391305890-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1391305890-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/configuracoes-lotes->2099328654-visualizar/editar->1391305890-textarea-titulo-1391305890-powerselect-agrupamento1-1391305890-powerselect-agrupamento2-1391305890-powerselect-loteUn-1391305890-powerselect-cninfIdPrefixoLote and submit`, () => {
const actualId = [`root`,`regras`,`regras/configuracoes-lotes`,`2099328654-visualizar/editar`,`1391305890-textarea-titulo-1391305890-powerselect-agrupamento1-1391305890-powerselect-agrupamento2-1391305890-powerselect-loteUn-1391305890-powerselect-cninfIdPrefixoLote`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracoes-lotes"]`);
      cy.clickIfExist(`[data-cy="2099328654-visualizar/editar"]`);
      cy.fillInput(`[data-cy="1391305890-textarea-titulo"] input`, `Tticas`);
cy.fillInputPowerSelect(`[data-cy="1391305890-powerselect-agrupamento1"] input`);
cy.fillInputPowerSelect(`[data-cy="1391305890-powerselect-agrupamento2"] input`);
cy.fillInputPowerSelect(`[data-cy="1391305890-powerselect-loteUn"] input`);
cy.fillInputPowerSelect(`[data-cy="1391305890-powerselect-cninfIdPrefixoLote"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-novo->1951752364-salvar`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-novo`,`1951752364-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-novo"]`);
      cy.clickIfExist(`[data-cy="1951752364-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-novo->1951752364-voltar`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-novo`,`1951752364-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-novo"]`);
      cy.clickIfExist(`[data-cy="1951752364-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/configuracao-estabelecimento->2012385885-novo->1951752364-powerselect-codigoUsual-1951752364-powerselect-plcntCodigo-1951752364-powerselect-itfCodigo-1951752364-powerselect-cntcfgId and submit`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-novo`,`1951752364-powerselect-codigoUsual-1951752364-powerselect-plcntCodigo-1951752364-powerselect-itfCodigo-1951752364-powerselect-cntcfgId`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-codigoUsual"] input`);
cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-plcntCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-itfCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-cntcfgId"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-visualizar/editar->2045387123-remover item`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-visualizar/editar`,`2045387123-remover item`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2045387123-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-visualizar/editar->2045387123-salvar`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-visualizar/editar`,`2045387123-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2045387123-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/configuracao-estabelecimento->2012385885-visualizar/editar->2045387123-voltar`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-visualizar/editar`,`2045387123-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2045387123-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/configuracao-estabelecimento->2012385885-visualizar/editar->2045387123-powerselect-plcntCodigo-2045387123-powerselect-itfCodigo-2045387123-powerselect-cntcfgId and submit`, () => {
const actualId = [`root`,`regras`,`regras/configuracao-estabelecimento`,`2012385885-visualizar/editar`,`2045387123-powerselect-plcntCodigo-2045387123-powerselect-itfCodigo-2045387123-powerselect-cntcfgId`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="2012385885-visualizar/editar"]`);
      cy.fillInputPowerSelect(`[data-cy="2045387123-powerselect-plcntCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2045387123-powerselect-itfCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2045387123-powerselect-cntcfgId"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-selecionar a informação`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-selecionar a informação`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.clickIfExist(`[data-cy="1727053496-selecionar a informação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-salvar`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.clickIfExist(`[data-cy="1727053496-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-voltar`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.clickIfExist(`[data-cy="1727053496-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-input-codigo-1727053496-powerselect-baseContabilizacao-1727053496-powerselect-usoInformacao and submit`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-input-codigo-1727053496-powerselect-baseContabilizacao-1727053496-powerselect-usoInformacao`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.fillInput(`[data-cy="1727053496-input-codigo"] textarea`, `generate`);
cy.fillInputPowerSelect(`[data-cy="1727053496-powerselect-baseContabilizacao"] input`);
cy.fillInputPowerSelect(`[data-cy="1727053496-powerselect-usoInformacao"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar->3503408767-ver regras/config.lote`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`,`3503408767-ver regras/config.lote`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3503408767-ver regras/config.lote"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar->3503408767-remover item`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`,`3503408767-remover item`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3503408767-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar->3503408767-salvar`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`,`3503408767-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3503408767-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar->3503408767-voltar`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`,`3503408767-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3503408767-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar->3503408767-input-codigo and submit`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`,`3503408767-input-codigo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.fillInput(`[data-cy="3503408767-input-codigo"] textarea`, `Incredible`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-novo->1323530880-salvar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-novo`,`1323530880-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-novo"]`);
      cy.clickIfExist(`[data-cy="1323530880-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-novo->1323530880-voltar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-novo`,`1323530880-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-novo"]`);
      cy.clickIfExist(`[data-cy="1323530880-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/lancamentos-contabeis->1124639753-novo->1323530880-input-codigo-1323530880-powerselect-plcntId-1323530880-powerselect-baseContabilizacao-1323530880-powerselect-indEntradaSaida-1323530880-textarea-descricao-1323530880-checkbox-indEstadual-1323530880-checkbox-indInterestadual-1323530880-checkbox-indExterior-1323530880-checkbox-indMercadoria-1323530880-checkbox-indPrestacao-1323530880-checkbox-indServicoIss-1323530880-checkbox-indOutros-1323530880-checkbox-indComplPreco-1323530880-checkbox-indComplImposto-1323530880-checkbox-indComplPrecoImposto and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-novo`,`1323530880-input-codigo-1323530880-powerselect-plcntId-1323530880-powerselect-baseContabilizacao-1323530880-powerselect-indEntradaSaida-1323530880-textarea-descricao-1323530880-checkbox-indEstadual-1323530880-checkbox-indInterestadual-1323530880-checkbox-indExterior-1323530880-checkbox-indMercadoria-1323530880-checkbox-indPrestacao-1323530880-checkbox-indServicoIss-1323530880-checkbox-indOutros-1323530880-checkbox-indComplPreco-1323530880-checkbox-indComplImposto-1323530880-checkbox-indComplPrecoImposto`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-novo"]`);
      cy.fillInput(`[data-cy="1323530880-input-codigo"] textarea`, `Cambridgeshire`);
cy.fillInputPowerSelect(`[data-cy="1323530880-powerselect-plcntId"] input`);
cy.fillInputPowerSelect(`[data-cy="1323530880-powerselect-baseContabilizacao"] input`);
cy.fillInputPowerSelect(`[data-cy="1323530880-powerselect-indEntradaSaida"] input`);
cy.fillInput(`[data-cy="1323530880-textarea-descricao"] input`, `Intelligent`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indEstadual"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indInterestadual"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indExterior"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indMercadoria"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indPrestacao"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indServicoIss"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indOutros"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indComplPreco"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indComplImposto"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="1323530880-checkbox-indComplPrecoImposto"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-selecionar critérios`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-selecionar critérios`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-selecionar critérios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-estabelecimentos`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-estabelecimentos`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-estabelecimentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-itens da regra`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-itens da regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-consolidar regras`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-consolidar regras`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-consolidar regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-copiar regra`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-copiar regra`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-copiar regra"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-remover item`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-remover item`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-salvar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-salvar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-voltar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-input-codigo-4234841159-powerselect-plcntId-4234841159-powerselect-baseContabilizacao-4234841159-powerselect-indEntradaSaida-4234841159-textarea-descricao-4234841159-checkbox-indEstadual-4234841159-checkbox-indInterestadual-4234841159-checkbox-indExterior-4234841159-checkbox-indMercadoria-4234841159-checkbox-indPrestacao-4234841159-checkbox-indServicoIss-4234841159-checkbox-indOutros-4234841159-checkbox-indComplPreco-4234841159-checkbox-indComplImposto-4234841159-checkbox-indComplPrecoImposto and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-input-codigo-4234841159-powerselect-plcntId-4234841159-powerselect-baseContabilizacao-4234841159-powerselect-indEntradaSaida-4234841159-textarea-descricao-4234841159-checkbox-indEstadual-4234841159-checkbox-indInterestadual-4234841159-checkbox-indExterior-4234841159-checkbox-indMercadoria-4234841159-checkbox-indPrestacao-4234841159-checkbox-indServicoIss-4234841159-checkbox-indOutros-4234841159-checkbox-indComplPreco-4234841159-checkbox-indComplImposto-4234841159-checkbox-indComplPrecoImposto`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.fillInput(`[data-cy="4234841159-input-codigo"] textarea`, `webenabled`);
cy.fillInputPowerSelect(`[data-cy="4234841159-powerselect-plcntId"] input`);
cy.fillInputPowerSelect(`[data-cy="4234841159-powerselect-baseContabilizacao"] input`);
cy.fillInputPowerSelect(`[data-cy="4234841159-powerselect-indEntradaSaida"] input`);
cy.fillInput(`[data-cy="4234841159-textarea-descricao"] input`, `Fantastic`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indEstadual"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indInterestadual"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indExterior"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indMercadoria"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indPrestacao"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indServicoIss"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indOutros"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indComplPreco"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indComplImposto"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="4234841159-checkbox-indComplPrecoImposto"] textarea`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/solicitacao->4102354395-novo->3385555950-salvar`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/solicitacao`,`4102354395-novo`,`3385555950-salvar`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/solicitacao"]`);
      cy.clickIfExist(`[data-cy="4102354395-novo"]`);
      cy.clickIfExist(`[data-cy="3385555950-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabilizacao->contabilizacao/solicitacao->4102354395-novo->3385555950-voltar`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/solicitacao`,`4102354395-novo`,`3385555950-voltar`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/solicitacao"]`);
      cy.clickIfExist(`[data-cy="4102354395-novo"]`);
      cy.clickIfExist(`[data-cy="3385555950-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values contabilizacao->contabilizacao/solicitacao->4102354395-novo->3385555950-powerselect-estCodigo-3385555950-input-titulo-3385555950-input-periodoContabil-3385555950-powerselect-baseContabilizacao-3385555950-powerselect-criterioGeracao-3385555950-powerselect-indEncerrar and submit`, () => {
const actualId = [`root`,`contabilizacao`,`contabilizacao/solicitacao`,`4102354395-novo`,`3385555950-powerselect-estCodigo-3385555950-input-titulo-3385555950-input-periodoContabil-3385555950-powerselect-baseContabilizacao-3385555950-powerselect-criterioGeracao-3385555950-powerselect-indEncerrar`];
    cy.clickIfExist(`[data-cy="contabilizacao"]`);
      cy.clickIfExist(`[data-cy="contabilizacao/solicitacao"]`);
      cy.clickIfExist(`[data-cy="4102354395-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="3385555950-powerselect-estCodigo"] input`);
cy.fillInput(`[data-cy="3385555950-input-titulo"] textarea`, `Turkish Lira`);
cy.fillInput(`[data-cy="3385555950-input-periodoContabil"] textarea`, `Nacional`);
cy.fillInputPowerSelect(`[data-cy="3385555950-powerselect-baseContabilizacao"] input`);
cy.fillInputPowerSelect(`[data-cy="3385555950-powerselect-criterioGeracao"] input`);
cy.fillInputPowerSelect(`[data-cy="3385555950-powerselect-indEncerrar"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-executar->923473359-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-executar`,`923473359-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-executar"]`);
      cy.clickIfExist(`[data-cy="923473359-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-executar->923473359-agendar`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-executar`,`923473359-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-executar"]`);
      cy.clickIfExist(`[data-cy="923473359-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/executa-contabilizacao->923473359-agendamentos->923473359-voltar`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-agendamentos`,`923473359-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-agendamentos"]`);
      cy.clickIfExist(`[data-cy="923473359-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/executa-contabilizacao->923473359-visualização->923473359-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-visualização`,`923473359-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="923473359-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-executar->1622304045-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-executar`,`1622304045-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-executar"]`);
      cy.clickIfExist(`[data-cy="1622304045-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-executar->1622304045-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-executar`,`1622304045-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-executar"]`);
      cy.clickIfExist(`[data-cy="1622304045-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-agendamentos->1622304045-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-agendamentos`,`1622304045-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1622304045-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-contabilizacao->1622304045-visualização->1622304045-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-visualização`,`1622304045-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1622304045-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-executar->4205939014-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-executar`,`4205939014-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-executar"]`);
      cy.clickIfExist(`[data-cy="4205939014-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-executar->4205939014-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-executar`,`4205939014-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-executar"]`);
      cy.clickIfExist(`[data-cy="4205939014-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-agendamentos->4205939014-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-agendamentos`,`4205939014-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4205939014-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/situacao-processos-contabilizacao->4205939014-visualização->4205939014-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-visualização`,`4205939014-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4205939014-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-executar->1463183472-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-executar`,`1463183472-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-executar"]`);
      cy.clickIfExist(`[data-cy="1463183472-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-executar->1463183472-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-executar`,`1463183472-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-executar"]`);
      cy.clickIfExist(`[data-cy="1463183472-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/analise-periodo-contabil->1463183472-executar->1463183472-input-P_PERIODO_CONTABIL and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-executar`,`1463183472-input-P_PERIODO_CONTABIL`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-executar"]`);
      cy.fillInput(`[data-cy="1463183472-input-P_PERIODO_CONTABIL"] textarea`, `bifurcated`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-agendamentos->1463183472-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-agendamentos`,`1463183472-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1463183472-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/analise-periodo-contabil->1463183472-visualização->1463183472-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-visualização`,`1463183472-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1463183472-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-executar->2520482827-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-executar`,`2520482827-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-executar"]`);
      cy.clickIfExist(`[data-cy="2520482827-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-executar->2520482827-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-executar`,`2520482827-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-executar"]`);
      cy.clickIfExist(`[data-cy="2520482827-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-agendamentos->2520482827-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-agendamentos`,`2520482827-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2520482827-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/enquadramento-contabil-processo->2520482827-visualização->2520482827-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-visualização`,`2520482827-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2520482827-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-executar->3614868856-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-executar`,`3614868856-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-executar"]`);
      cy.clickIfExist(`[data-cy="3614868856-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-executar->3614868856-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-executar`,`3614868856-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-executar"]`);
      cy.clickIfExist(`[data-cy="3614868856-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-agendamentos->3614868856-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-agendamentos`,`3614868856-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3614868856-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/enquadramento-contabil-regra->3614868856-visualização->3614868856-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-visualização`,`3614868856-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3614868856-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-executar->3469256492-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-executar`,`3469256492-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-executar"]`);
      cy.clickIfExist(`[data-cy="3469256492-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-executar->3469256492-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-executar`,`3469256492-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-executar"]`);
      cy.clickIfExist(`[data-cy="3469256492-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-agendamentos->3469256492-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-agendamentos`,`3469256492-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3469256492-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/lancamento-periodo->3469256492-visualização->3469256492-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-visualização`,`3469256492-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3469256492-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-executar->1751406876-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-executar`,`1751406876-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-executar"]`);
      cy.clickIfExist(`[data-cy="1751406876-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-executar->1751406876-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-executar`,`1751406876-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-executar"]`);
      cy.clickIfExist(`[data-cy="1751406876-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-agendamentos->1751406876-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-agendamentos`,`1751406876-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1751406876-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-visualização->1751406876-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-visualização`,`1751406876-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1751406876-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-executar->597180432-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-executar`,`597180432-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-executar"]`);
      cy.clickIfExist(`[data-cy="597180432-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-executar->597180432-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-executar`,`597180432-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-executar"]`);
      cy.clickIfExist(`[data-cy="597180432-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/regras-contabeis->597180432-agendamentos->597180432-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-agendamentos`,`597180432-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-agendamentos"]`);
      cy.clickIfExist(`[data-cy="597180432-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/regras-contabeis->597180432-visualização->597180432-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-visualização`,`597180432-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="597180432-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-executar->3522401022-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-executar`,`3522401022-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-executar"]`);
      cy.clickIfExist(`[data-cy="3522401022-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-executar->3522401022-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-executar`,`3522401022-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-executar"]`);
      cy.clickIfExist(`[data-cy="3522401022-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-agendamentos->3522401022-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-agendamentos`,`3522401022-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3522401022-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/consulta-contabilizacao->3522401022-visualização->3522401022-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-visualização`,`3522401022-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3522401022-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-selecionar a informação->1727053496-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-selecionar a informação`,`1727053496-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.clickIfExist(`[data-cy="1727053496-selecionar a informação"]`);
      cy.clickIfExist(`[data-cy="1727053496-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-selecionar a informação->1727053496-cancelar`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-selecionar a informação`,`1727053496-cancelar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.clickIfExist(`[data-cy="1727053496-selecionar a informação"]`);
      cy.clickIfExist(`[data-cy="1727053496-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values regras->regras/informacoes-contabilizacao->1205837009-novo->1727053496-input-codigo-1727053496-powerselect-baseContabilizacao-1727053496-powerselect-usoInformacao->1727053496-powerselect-colunaLcnt and submit`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-novo`,`1727053496-input-codigo-1727053496-powerselect-baseContabilizacao-1727053496-powerselect-usoInformacao`,`1727053496-powerselect-colunaLcnt`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-novo"]`);
      cy.fillInput(`[data-cy="1727053496-input-codigo"] textarea`, `generate`);
cy.fillInputPowerSelect(`[data-cy="1727053496-powerselect-baseContabilizacao"] input`);
cy.fillInputPowerSelect(`[data-cy="1727053496-powerselect-usoInformacao"] input`);
cy.submitIfExist(`.ant-form`);

      cy.fillInputPowerSelect(`[data-cy="1727053496-powerselect-colunaLcnt"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/informacoes-contabilizacao->1205837009-visualizar/editar->3503408767-ver regras/config.lote->3503408767-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/informacoes-contabilizacao`,`1205837009-visualizar/editar`,`3503408767-ver regras/config.lote`,`3503408767-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/informacoes-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1205837009-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3503408767-ver regras/config.lote"]`);
      cy.clickIfExist(`[data-cy="3503408767-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-estabelecimentos->4234841159-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-estabelecimentos`,`4234841159-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="4234841159-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-estabelecimentos->4234841159-prioridade de execução`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-estabelecimentos`,`4234841159-prioridade de execução`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="4234841159-prioridade de execução"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-estabelecimentos->4234841159-fechar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-estabelecimentos`,`4234841159-fechar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-estabelecimentos"]`);
      cy.clickIfExist(`[data-cy="4234841159-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-estabelecimentos->4234841159-power-search-input and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-estabelecimentos`,`4234841159-power-search-input`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-estabelecimentos"]`);
      cy.fillInputPowerSearch(`[data-cy="4234841159-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra->3700427807-novo`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-itens da regra`,`3700427807-novo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-itens da regra"]`);
      cy.clickIfExist(`[data-cy="3700427807-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra->3700427807-power-search-button`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-itens da regra`,`3700427807-power-search-button`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-itens da regra"]`);
      cy.clickIfExist(`[data-cy="3700427807-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra->3700427807-voltar`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-itens da regra`,`3700427807-voltar`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-itens da regra"]`);
      cy.clickIfExist(`[data-cy="3700427807-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-copiar regra->4234841159-input-copiaFrom-4234841159-input-copiaTo and submit`, () => {
const actualId = [`root`,`regras`,`regras/lancamentos-contabeis`,`1124639753-visualizar/editar`,`4234841159-copiar regra`,`4234841159-input-copiaFrom-4234841159-input-copiaTo`];
    cy.clickIfExist(`[data-cy="regras"]`);
      cy.clickIfExist(`[data-cy="regras/lancamentos-contabeis"]`);
      cy.clickIfExist(`[data-cy="1124639753-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="4234841159-copiar regra"]`);
      cy.fillInput(`[data-cy="4234841159-input-copiaFrom"] textarea`, `Tcnico`);
cy.fillInput(`[data-cy="4234841159-input-copiaTo"] textarea`, `Hat`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/executa-contabilizacao->923473359-executar->923473359-múltipla seleção->923473359-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/executa-contabilizacao`,`923473359-executar`,`923473359-múltipla seleção`,`923473359-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/executa-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="923473359-executar"]`);
      cy.clickIfExist(`[data-cy="923473359-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="923473359-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-contabilizacao->1622304045-executar->1622304045-múltipla seleção->1622304045-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-contabilizacao`,`1622304045-executar`,`1622304045-múltipla seleção`,`1622304045-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="1622304045-executar"]`);
      cy.clickIfExist(`[data-cy="1622304045-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1622304045-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/situacao-processos-contabilizacao->4205939014-executar->4205939014-múltipla seleção->4205939014-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/situacao-processos-contabilizacao`,`4205939014-executar`,`4205939014-múltipla seleção`,`4205939014-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/situacao-processos-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="4205939014-executar"]`);
      cy.clickIfExist(`[data-cy="4205939014-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4205939014-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/analise-periodo-contabil->1463183472-executar->1463183472-múltipla seleção->1463183472-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/analise-periodo-contabil`,`1463183472-executar`,`1463183472-múltipla seleção`,`1463183472-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/analise-periodo-contabil"]`);
      cy.clickIfExist(`[data-cy="1463183472-executar"]`);
      cy.clickIfExist(`[data-cy="1463183472-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1463183472-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/enquadramento-contabil-processo->2520482827-executar->2520482827-múltipla seleção->2520482827-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-processo`,`2520482827-executar`,`2520482827-múltipla seleção`,`2520482827-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-processo"]`);
      cy.clickIfExist(`[data-cy="2520482827-executar"]`);
      cy.clickIfExist(`[data-cy="2520482827-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2520482827-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/enquadramento-contabil-regra->3614868856-executar->3614868856-múltipla seleção->3614868856-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/enquadramento-contabil-regra`,`3614868856-executar`,`3614868856-múltipla seleção`,`3614868856-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/enquadramento-contabil-regra"]`);
      cy.clickIfExist(`[data-cy="3614868856-executar"]`);
      cy.clickIfExist(`[data-cy="3614868856-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3614868856-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/lancamento-periodo->3469256492-executar->3469256492-múltipla seleção->3469256492-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/lancamento-periodo`,`3469256492-executar`,`3469256492-múltipla seleção`,`3469256492-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/lancamento-periodo"]`);
      cy.clickIfExist(`[data-cy="3469256492-executar"]`);
      cy.clickIfExist(`[data-cy="3469256492-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3469256492-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/resumo-lancamento-cantabeis->1751406876-executar->1751406876-múltipla seleção->1751406876-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/resumo-lancamento-cantabeis`,`1751406876-executar`,`1751406876-múltipla seleção`,`1751406876-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/resumo-lancamento-cantabeis"]`);
      cy.clickIfExist(`[data-cy="1751406876-executar"]`);
      cy.clickIfExist(`[data-cy="1751406876-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1751406876-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/regras-contabeis->597180432-executar->597180432-múltipla seleção->597180432-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/regras-contabeis`,`597180432-executar`,`597180432-múltipla seleção`,`597180432-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/regras-contabeis"]`);
      cy.clickIfExist(`[data-cy="597180432-executar"]`);
      cy.clickIfExist(`[data-cy="597180432-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="597180432-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/consulta-contabilizacao->3522401022-executar->3522401022-múltipla seleção->3522401022-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/consulta-contabilizacao`,`3522401022-executar`,`3522401022-múltipla seleção`,`3522401022-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/consulta-contabilizacao"]`);
      cy.clickIfExist(`[data-cy="3522401022-executar"]`);
      cy.clickIfExist(`[data-cy="3522401022-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3522401022-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
