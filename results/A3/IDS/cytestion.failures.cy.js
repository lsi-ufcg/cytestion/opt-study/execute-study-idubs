describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values regras->regras/configuracao-estabelecimento->2012385885-novo->1951752364-powerselect-codigoUsual-1951752364-powerselect-plcntCodigo-1951752364-powerselect-itfCodigo-1951752364-powerselect-cntcfgId and submit`, () => {
    cy.clickCauseExist(`[data-cy="regras"]`);
    cy.clickCauseExist(`[data-cy="regras/configuracao-estabelecimento"]`);
    cy.clickCauseExist(`[data-cy="2012385885-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-codigoUsual"] input`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-plcntCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-itfCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1951752364-powerselect-cntcfgId"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element regras->regras/lancamentos-contabeis->1124639753-visualizar/editar->4234841159-itens da regra->3700427807-novo`, () => {
    cy.clickCauseExist(`[data-cy="regras"]`);
    cy.clickCauseExist(`[data-cy="regras/lancamentos-contabeis"]`);
    cy.clickCauseExist(`[data-cy="1124639753-visualizar/editar"]`);
    cy.clickCauseExist(`[data-cy="4234841159-itens da regra"]`);
    cy.clickCauseExist(`[data-cy="3700427807-novo"]`);
    cy.checkErrorsWereDetected();
  });
});
