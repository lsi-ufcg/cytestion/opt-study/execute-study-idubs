describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-power-search-input and submit`, () => {
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto"]`);
    cy.fillInputPowerSearch(`[data-cy="739716742-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario->2893041617-novo->3487034296-salvar`, () => {
    cy.clickCauseExist(`[data-cy="sped-fiscal"]`);
    cy.clickCauseExist(`[data-cy="sped-fiscal/bloco-h"]`);
    cy.clickCauseExist(`[data-cy="sped-fiscal/bloco-h/lancamento-inventario"]`);
    cy.clickCauseExist(`[data-cy="2893041617-novo"]`);
    cy.clickCauseExist(`[data-cy="3487034296-salvar"]`);
    cy.checkErrorsWereDetected();
  });
});
