describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
    const actualId = [`root`, `tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema`, () => {
    const actualId = [`root`, `parametros-sistema`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
    const actualId = [`root`, `tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal`, () => {
    const actualId = [`root`, `sped-fiscal`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
    const actualId = [`root`, `obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
    const actualId = [`root`, `relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
    const actualId = [`root`, `processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
    const actualId = [`root`, `processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
    const actualId = [`root`, `download`];
    cy.clickIfExist(`[data-cy="download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 4019658129-exibir dados`, () => {
    const actualId = [`root`, `4019658129-exibir dados`];
    cy.clickIfExist(`[data-cy="4019658129-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/cfop`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/cfop`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/cfop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/modelo-dof`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/modelo-dof`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/modelo-dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/tabela-codigos-gnre"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/aliquotaInternaIcms/ncm"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
    cy.clickIfExist(`[data-cy="tabelas-oficiais/aliquotaInternaIcms/mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema->parametros-sistema/parametros-gerais`, () => {
    const actualId = [`root`, `parametros-sistema`, `parametros-sistema/parametros-gerais`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
    cy.clickIfExist(`[data-cy="parametros-sistema/parametros-gerais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema->parametros-sistema/parametros-financeiros`, () => {
    const actualId = [`root`, `parametros-sistema`, `parametros-sistema/parametros-financeiros`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
    cy.clickIfExist(`[data-cy="parametros-sistema/parametros-financeiros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-prestacoes`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-prestacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-servicos`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-servicos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-servicos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-h"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/convenios`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/convenios`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/convenios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/diretorio-arquivos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/job`, () => {
    const actualId = [`root`, `processos`, `processos/job`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/job"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->73017837-power-search-button`, () => {
    const actualId = [`root`, `download`, `73017837-power-search-button`];
    cy.visit('http://system-A10/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="73017837-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element download->73017837-download`, () => {
    const actualId = [`root`, `download`, `73017837-download`];
    cy.visit('http://system-A10/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="73017837-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element download->73017837-detalhes`, () => {
    const actualId = [`root`, `download`, `73017837-detalhes`];
    cy.visit('http://system-A10/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="73017837-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element download->73017837-excluir`, () => {
    const actualId = [`root`, `download`, `73017837-excluir`];
    cy.visit('http://system-A10/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="73017837-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-novo`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-novo`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="91465929-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-power-search-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-power-search-button`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="91465929-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-visualizar/editar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-visualizar/editar`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="91465929-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-excluir`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-excluir`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="91465929-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-button`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-novo`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-novo`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-power-search-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-power-search-button`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-eyeoutlined`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-eyeoutlined`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-deleteoutlined`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-deleteoutlined`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-novo`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-novo`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-mais operações`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-mais operações`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-power-search-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-power-search-button`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-visualizar/editar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-visualizar/editar`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-excluir`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-excluir`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-novo`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-novo`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-mais operações`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-mais operações`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-power-search-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-power-search-button`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-visualizar/editar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-visualizar/editar`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-excluir`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-excluir`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros-sistema->parametros-sistema/parametros-gerais->parametros-sistema/parametros-gerais/parametrizacao-geral`, () => {
    const actualId = [`root`, `parametros-sistema`, `parametros-sistema/parametros-gerais`, `parametros-sistema/parametros-gerais/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
    cy.clickIfExist(`[data-cy="parametros-sistema/parametros-gerais"]`);
    cy.clickIfExist(`[data-cy="parametros-sistema/parametros-gerais/parametrizacao-geral"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema->parametros-sistema/parametros-financeiros->parametros-sistema/parametros-financeiros-banco-agencia-conta`, () => {
    const actualId = [`root`, `parametros-sistema`, `parametros-sistema/parametros-financeiros`, `parametros-sistema/parametros-financeiros-banco-agencia-conta`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
    cy.clickIfExist(`[data-cy="parametros-sistema/parametros-financeiros"]`);
    cy.clickIfExist(`[data-cy="parametros-sistema/parametros-financeiros-banco-agencia-conta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/naturezaOperacao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/naturezaOperacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/naturezaOperacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/edof`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/edof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/edof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/equipamento`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/equipamento`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/equipamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/cadastro-auxiliares`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/cadastro-auxiliares`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/cadastro-auxiliares"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/equivalencia-codigo`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/equivalencia-codigo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/equivalencia-codigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/codigo-receita`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/codigo-receita`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/codigo-receita"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/deducao-nop`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/deducao-nop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/deducao-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/deducao-cfop`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/deducao-cfop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/deducao-cfop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscal->tabelas-corporativas/fiscal/condicao-pagamento`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/fiscal`, `tabelas-corporativas/fiscal/condicao-pagamento`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscal/condicao-pagamento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/pessoas-fisicas-juridicas`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/pessoas-fisicas-juridicas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/pessoas-fisicas-juridicas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/hierarquia-pessoas`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/hierarquia-pessoas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/hierarquia-pessoas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/tipo-contribuinte`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/tipo-contribuinte`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/tipo-contribuinte"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/classe-pfj`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/classe-pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/classe-pfj"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/A4ista`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/pfj`, `tabelas-corporativas/pfj/A4ista`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/A4ista"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/cadastro-mercadoria`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/cadastro-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/cadastro-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/informacoes-mercadoria-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/aplicacao-mercadoria`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/aplicacao-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/aplicacao-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/mercadoria-correlacionada`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/mercadoria-correlacionada`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/mercadoria-correlacionada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/finalidade-mercadoria`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/finalidade-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/finalidade-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/origem-mercadoria`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/origem-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/origem-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/stc`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/stc`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/stc"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/stp`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/stp`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/stp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/classe-merc-pres-ncm`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/mercadorias`, `tabelas-corporativas/mercadorias/classe-merc-pres-ncm`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/classe-merc-pres-ncm"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-prestacoes->tabelas-corporativas/cadastro-prestacoes/prestacoes`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-prestacoes`, `tabelas-corporativas/cadastro-prestacoes/prestacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes/prestacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-servicos->tabelas-corporativas/cadastro-servicos/servicos`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/cadastro-servicos`, `tabelas-corporativas/cadastro-servicos/servicos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-servicos"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-servicos/servicos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/documento-fiscal`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/documento-fiscal`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes/documento-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->transacoes/leitura-situacao`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `transacoes/leitura-situacao`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.clickIfExist(`[data-cy="transacoes/leitura-situacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/inclusa-entrada-comand`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/inclusa-entrada-comand`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes/inclusa-entrada-comand"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
    cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes/desfaziamento-negocios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/lfis`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/lfis`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/lfis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/regra-info-adicional"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/reglanicms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/regra-recolhimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->sped-fiscal/bloco-1/informacao-1400`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `sped-fiscal/bloco-1/informacao-1400`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/informacao-1400"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-dim`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-dim`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/consulta-dim"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacao-dipam`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacao-dipam`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/informacao-dipam"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/recup-st"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/geraLancFiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/apagaLancFiscalPeriod"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/apuracao-icms-ipi"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/apagaApuracaoIcmsIpi"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/importacao-gru"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/calcDiferencialAliq"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/geraValoresDOFs"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/emissao-registro-apuracao`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/emissao-registro-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/emissao-registro-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/apagaApurProcAntigo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/limpaJobLfis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/processos/encerraReabrePeriodoFiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1200`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1200`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/controle-1200"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/registro-1310`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/registro-1310`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/registro-1310"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/movimentacao-1300`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/movimentacao-1300`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/movimentacao-1300"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1390`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1390`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/controle-1390"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/manutecao-cadastro-1400"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-valores-1400`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-valores-1400`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/manutecao-valores-1400"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/total-1600"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/documentos-1700`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/documentos-1700`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/documentos-1700"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/dcta-1800`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/dcta-1800`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/dcta-1800"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/informacao-adicional-apuracao`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/informacao-adicional-apuracao`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/informacao-adicional-apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-h"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-h/classificacao-mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/lancamento-inventario`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-h"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-h/lancamento-inventario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/consumo-0210"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/estoque-k200`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/estoque-k200`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/estoque-k200"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/desmontagem-k210-k215"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/outras-k220`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/outras-k220`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/outras-k220"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/itens-k230-k235`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/itens-k230-k235`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/itens-k230-k235"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/industrialização-k250-k255"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/reprocessamento-k260-k265`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/reprocessamento-k260-k265`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/reprocessamento-k260-k265"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k270-k275`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k270-k275`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/correcao-k270-k275"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k280`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k280`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/correcao-k280"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k290`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k290`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/producao-k290"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k300`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k300`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/producao-k300"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/operacao-produtos-anp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consulta-itens-componente`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consulta-itens-componente`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-k/consulta-itens-componente"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos/gerar-informacao-icms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos/apaga-informacao-icms"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos/limpar-informacao-blocok"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/processos/limpeza-consumo-especifico-padrao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-power-search-button`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3552734270-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-gerenciar labels`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-gerenciar labels`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3552734270-gerenciar labels"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-visualizar parâmetros`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-visualizar parâmetros`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3552734270-visualizar parâmetros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-visualizar/editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-visualizar/editar`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3552734270-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ir para todas as obrigações`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ir para todas as obrigações`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajuda`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajuda`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-nova solicitação`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-nova solicitação`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-nova solicitação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-atualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-atualizar`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-gerar obrigação`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-gerar obrigação`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-gerar obrigação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-ajustar parâmetros da geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-visualizar resultado da geração`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-visualizar resultado da geração`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-visualizar resultado da geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-protocolo transmissão`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-protocolo transmissão`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-protocolo transmissão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-excluir geração`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-excluir geração`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-excluir geração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-power-search-button`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-visualização`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-abrir visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-abrir visualização`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-visualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-visualizar`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-novo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-novo`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-power-search-button`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-editar`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-excluir`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->757558734-novo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `757558734-novo`];
    cy.visit('http://system-A10/obrigacoes/configuracao-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="757558734-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->757558734-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `757558734-power-search-button`];
    cy.visit('http://system-A10/obrigacoes/configuracao-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="757558734-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->757558734-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `757558734-excluir`];
    cy.visit('http://system-A10/obrigacoes/configuracao-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="757558734-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/processos/totalizacoes-mensais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/processos/totalizacoes-convenios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos->584594220-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/diretorio-arquivos`, `584594220-power-search-button`];
    cy.visit('http://system-A10/diretorio-arquivos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="584594220-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos->584594220-gerar arquivo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/diretorio-arquivos`, `584594220-gerar arquivo`];
    cy.visit('http://system-A10/diretorio-arquivos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="584594220-gerar arquivo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos->584594220-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/diretorio-arquivos`, `584594220-excluir`];
    cy.visit('http://system-A10/diretorio-arquivos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="584594220-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/diretorio-arquivos->584594220-power-search-input and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/diretorio-arquivos`, `584594220-power-search-input`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
    cy.fillInputPowerSearch(`[data-cy="584594220-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/apuracao-icms-ipi-detalhado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/debito-credito-detalhado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/diferencial-aliquotas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/diferencial-aliquotas-fcp"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lancamentos-ajustes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lancamentos-ajustes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/lancamentos-ajustes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/lista-cfop-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/operacao-com-cartao`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/operacao-com-cartao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/operacao-com-cartao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/registros-entradas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-saidas`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-saidas`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/registros-saidas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/valores-agregados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/valores-agregados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/valores-agregados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis-snapshot`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis-snapshot`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-lfis-snapshot"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/chave-eletronica-documentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-lfis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/quebra-sequencia-documentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/quebra-sequencia-documentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/quebra-sequencia-documentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/controle-producao-estoque"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/criticas-saldos-inventarios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/extracao-lancamentos-inventarios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/informacoes-bloco-k"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque"]`);
    cy.clickIfExist(`[data-cy="relatorios/producao-estoque/livro-inventario"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfe-59`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfe-59`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo/resumo-cfe-59"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-fiscal`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-fiscal`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo/resumo-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-fiscal-dof`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-fiscal-dof`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo/resumo-fiscal-dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo/resumo-cfop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo/resumo-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo"]`);
    cy.clickIfExist(`[data-cy="relatorios/resumo/resumo-uf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/job->3973244479-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/job`, `3973244479-power-search-button`];
    cy.visit('http://system-A10/job?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3973244479-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element processos->processos/job->3973244479-eyeoutlined`, () => {
    const actualId = [`root`, `processos`, `processos/job`, `3973244479-eyeoutlined`];
    cy.visit('http://system-A10/job?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3973244479-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-novo->1918725568-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-novo`, `1918725568-salvar`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1918725568-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-novo->1918725568-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-novo`, `1918725568-voltar`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1918725568-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-novo->1918725568-powerselect-codTabela-1918725568-input-codAjuste-1918725568-textarea-descricao and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-novo`, `1918725568-powerselect-codTabela-1918725568-input-codAjuste-1918725568-textarea-descricao`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="91465929-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1918725568-powerselect-codTabela"] input`);
    cy.fillInput(`[data-cy="1918725568-input-codAjuste"] textarea`, `Graphic Interface`);
    cy.fillInput(`[data-cy="1918725568-textarea-descricao"] input`, `Berkshire`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-visualizar/editar->1962865603-remover item`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-visualizar/editar`, `1962865603-remover item`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi/editar/MG56000999/5.3/MG/2022-01-01');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1962865603-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-visualizar/editar->1962865603-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-visualizar/editar`, `1962865603-salvar`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi/editar/MG56000999/5.3/MG/2022-01-01');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1962865603-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-visualizar/editar->1962865603-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-visualizar/editar`, `1962865603-voltar`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi/editar/MG56000999/5.3/MG/2022-01-01');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1962865603-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi->91465929-visualizar/editar->1962865603-textarea-descricao and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabelaCodigosEfdIcmsIpi`, `91465929-visualizar/editar`, `1962865603-textarea-descricao`];
    cy.visit('http://system-A10/tabela-codigo-efd-icms-ipi');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="91465929-visualizar/editar"]`);
    cy.fillInput(`[data-cy="1962865603-textarea-descricao"] input`, `eyeballs`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-novo->2589074865-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-novo`, `2589074865-button`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2589074865-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-novo->2589074865-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-novo`, `2589074865-salvar`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2589074865-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-novo->2589074865-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-novo`, `2589074865-voltar`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2589074865-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-novo->2589074865-powerselect-tabela-2589074865-input-codigo-2589074865-powerselect-codReceita-2589074865-input-codDetalheReceita-2589074865-powerselect-imposto-2589074865-powerselect-ufRecolhimento-2589074865-textarea-descricao and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-novo`, `2589074865-powerselect-tabela-2589074865-input-codigo-2589074865-powerselect-codReceita-2589074865-input-codDetalheReceita-2589074865-powerselect-imposto-2589074865-powerselect-ufRecolhimento-2589074865-textarea-descricao`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2589074865-powerselect-tabela"] input`);
    cy.fillInput(`[data-cy="2589074865-input-codigo"] textarea`, `Lder`);
    cy.fillInputPowerSelect(`[data-cy="2589074865-powerselect-codReceita"] input`);
    cy.fillInput(`[data-cy="2589074865-input-codDetalheReceita"] textarea`, `dynamic`);
    cy.fillInputPowerSelect(`[data-cy="2589074865-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="2589074865-powerselect-ufRecolhimento"] input`);
    cy.fillInput(`[data-cy="2589074865-textarea-descricao"] input`, `backing up`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-eyeoutlined->1727942328-button`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-eyeoutlined`, `1727942328-button`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/editar/414');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1727942328-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-eyeoutlined->1727942328-remover item`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-eyeoutlined`, `1727942328-remover item`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/editar/414');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1727942328-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-eyeoutlined->1727942328-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-eyeoutlined`, `1727942328-salvar`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/editar/414');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1727942328-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-eyeoutlined->1727942328-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-eyeoutlined`, `1727942328-voltar`];
    cy.visit('http://system-A10/cadastro-codigo-gnre/editar/414');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1727942328-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/fiscais/tabela-codigos-gnre->2081816120-eyeoutlined->1727942328-powerselect-codReceita-1727942328-input-codDetalheReceita-1727942328-powerselect-imposto-1727942328-powerselect-ufRecolhimento-1727942328-textarea-descricao and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/fiscais/tabela-codigos-gnre`, `2081816120-eyeoutlined`, `1727942328-powerselect-codReceita-1727942328-input-codDetalheReceita-1727942328-powerselect-imposto-1727942328-powerselect-ufRecolhimento-1727942328-textarea-descricao`];
    cy.visit('http://system-A10/cadastro-codigo-gnre');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2081816120-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="1727942328-powerselect-codReceita"] input`);
    cy.fillInput(`[data-cy="1727942328-input-codDetalheReceita"] textarea`, `compress`);
    cy.fillInputPowerSelect(`[data-cy="1727942328-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="1727942328-powerselect-ufRecolhimento"] input`);
    cy.fillInput(`[data-cy="1727942328-textarea-descricao"] input`, `B2B`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-novo->858823693-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-novo`, `858823693-salvar`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="858823693-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-novo->858823693-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-novo`, `858823693-voltar`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="858823693-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-novo->858823693-powerselect-ufCodigo-858823693-input-number-aliqInterna and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-novo`, `858823693-powerselect-ufCodigo-858823693-input-number-aliqInterna`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="858823693-powerselect-ufCodigo"] input`);
    cy.fillInput(`[data-cy="858823693-input-number-aliqInterna"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-visualizar/editar->2204733460-remover item`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-visualizar/editar`, `2204733460-remover item`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm/editar/68');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2204733460-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-visualizar/editar->2204733460-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-visualizar/editar`, `2204733460-salvar`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm/editar/68');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2204733460-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-visualizar/editar->2204733460-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-visualizar/editar`, `2204733460-voltar`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm/editar/68');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2204733460-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/ncm->3131440732-visualizar/editar->2204733460-input-number-aliqInterna and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/ncm`, `3131440732-visualizar/editar`, `2204733460-input-number-aliqInterna`];
    cy.visit('http://system-A10/aliquota-interna-icms/ncm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3131440732-visualizar/editar"]`);
    cy.fillInput(`[data-cy="2204733460-input-number-aliqInterna"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-novo->1826225254-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-novo`, `1826225254-salvar`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1826225254-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-novo->1826225254-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-novo`, `1826225254-voltar`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1826225254-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-novo->1826225254-powerselect-ufCodigo-1826225254-input-number-aliqInterna and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-novo`, `1826225254-powerselect-ufCodigo-1826225254-input-number-aliqInterna`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1826225254-powerselect-ufCodigo"] input`);
    cy.fillInput(`[data-cy="1826225254-input-number-aliqInterna"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-visualizar/editar->3566569901-remover item`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-visualizar/editar`, `3566569901-remover item`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria/editar/74');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3566569901-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-visualizar/editar->3566569901-salvar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-visualizar/editar`, `3566569901-salvar`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria/editar/74');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3566569901-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-visualizar/editar->3566569901-voltar`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-visualizar/editar`, `3566569901-voltar`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria/editar/74');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3566569901-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-oficiais->tabelas-oficiais/aliquotaInternaIcms/mercadoria->219548259-visualizar/editar->3566569901-input-number-aliqInterna and submit`, () => {
    const actualId = [`root`, `tabelas-oficiais`, `tabelas-oficiais/aliquotaInternaIcms/mercadoria`, `219548259-visualizar/editar`, `3566569901-input-number-aliqInterna`];
    cy.visit('http://system-A10/aliquota-interna-icms/mercadoria');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="219548259-visualizar/editar"]`);
    cy.fillInput(`[data-cy="3566569901-input-number-aliqInterna"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/inclusa-entrada-comand->2347901503-power-search-button`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/inclusa-entrada-comand`, `2347901503-power-search-button`];
    cy.visit('http://system-A10/documentos-fiscais/inclusao-alteracao-entrada-comandada');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2347901503-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`];
    cy.visit('http://system-A10/desfazimento-negocios?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2568354531-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-power-search-button`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-power-search-button`];
    cy.visit('http://system-A10/desfazimento-negocios?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2568354531-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-info-adicional->2264139126-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`, `2264139126-novo`];
    cy.visit('http://system-A10/regra-info-adicional/E115');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2264139126-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-info-adicional->2264139126-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`, `2264139126-power-search-button`];
    cy.visit('http://system-A10/regra-info-adicional/E115');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2264139126-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-novo`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-power-search-button`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-guias sem código de ajuste`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-guias sem código de ajuste`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-guias sem código de ajuste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-excluir`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-itens de ajuste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-novo`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-power-search-button`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-selectoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-selectoutlined`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-selectoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-deleteoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-deleteoutlined`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`];
    cy.visit('http://system-A10/inteligencia-fiscal/rlcpe');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="739716742-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-power-search-button`];
    cy.visit('http://system-A10/inteligencia-fiscal/rlcpe');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="739716742-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`];
    cy.visit('http://system-A10/inteligencia-fiscal/rlcpe');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="739716742-eyeoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-deleteoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-deleteoutlined`];
    cy.visit('http://system-A10/inteligencia-fiscal/rlcpe');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="739716742-deleteoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-power-search-input`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto"]`);
    cy.fillInputPowerSearch(`[data-cy="739716742-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->sped-fiscal/bloco-1/informacao-1400->3681969890-plusoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `sped-fiscal/bloco-1/informacao-1400`, `3681969890-plusoutlined`];
    cy.visit('http://system-A10/spedf-regra');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3681969890-plusoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->sped-fiscal/bloco-1/informacao-1400->3681969890-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `sped-fiscal/bloco-1/informacao-1400`, `3681969890-power-search-button`];
    cy.visit('http://system-A10/spedf-regra');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3681969890-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-power-search-button`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3748309102-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-identificação dos itens do documento fiscal (idf)`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-identificação dos itens do documento fiscal (idf)`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3748309102-identificação dos itens do documento fiscal (idf)"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-gerar lfis`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-gerar lfis`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3748309102-gerar lfis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-apagar lfis`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-apagar lfis`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3748309102-apagar lfis"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`];
    cy.visit('http://system-A10/lancamento-ajuste/C197');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3361402062-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-power-search-button`];
    cy.visit('http://system-A10/lancamento-ajuste/C197');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3361402062-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-power-search-input`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste"]`);
    cy.fillInputPowerSearch(`[data-cy="3361402062-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-novo`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-mais operações`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-power-search-button`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`];
    cy.visit('http://system-A10/guia-recolhimento-imp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtFatoGeradorImposto=~mth~1712493261029D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2583632075-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-power-search-button`];
    cy.visit('http://system-A10/guia-recolhimento-imp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtFatoGeradorImposto=~mth~1712493261029D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2583632075-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-dim->3890208878-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-dim`, `3890208878-power-search-button`];
    cy.visit('http://system-A10/consulta-dim?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3890208878-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg->2949867902-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`, `2949867902-novo`];
    cy.visit('http://system-A10/item-sintegra-mg?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2949867902-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg->2949867902-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`, `2949867902-power-search-button`];
    cy.visit('http://system-A10/item-sintegra-mg?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2949867902-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacao-dipam->133973439-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacao-dipam`, `133973439-novo`];
    cy.visit('http://system-A10/obr-inf-dipam?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133973439-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacao-dipam->133973439-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacao-dipam`, `133973439-power-search-button`];
    cy.visit('http://system-A10/obr-inf-dipam?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133973439-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am->1212595578-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am`, `1212595578-excluir`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/imp-nota-sefaz-dia-am-capa?estab=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1212595578-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am->1212595578-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am`, `1212595578-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/imp-nota-sefaz-dia-am-capa?estab=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1212595578-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg->3642031708-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`, `3642031708-novo`];
    cy.visit('http://system-A10/inventario-sintegra-mg?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3642031708-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg->3642031708-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`, `3642031708-power-search-button`];
    cy.visit('http://system-A10/inventario-sintegra-mg?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3642031708-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`];
    cy.visit('http://system-A10/lancamento-producao-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="766548531-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-mais operações`];
    cy.visit('http://system-A10/lancamento-producao-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="766548531-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-power-search-button`];
    cy.visit('http://system-A10/lancamento-producao-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="766548531-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4275029436-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4275029436-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc->escrituracao-apuracao/lancamento-apuracao/dime-sc`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, `escrituracao-apuracao/lancamento-apuracao/dime-sc`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc"]`);
    cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/dime-sc"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-regerar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-regerar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-detalhes`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-detalhes`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-abrir visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-abrir visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-excluir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-carregar mais`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-carregar mais`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-regerar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-regerar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-detalhes`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-detalhes`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-abrir visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-abrir visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-excluir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-gerar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-gerar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="77045265-gerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="77045265-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="77045265-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-regerar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-regerar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-detalhes`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-detalhes`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-abrir visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-abrir visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-excluir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-importar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-importar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-importar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-mais operações`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-excluir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/emissao-registro-apuracao->1902199267-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/emissao-registro-apuracao`, `1902199267-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/emissao-registro-apuracao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1902199267-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-requisitos de preenchimento`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-requisitos de preenchimento`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-requisitos de preenchimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1200->607853382-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1200`, `607853382-novo`];
    cy.visit('http://system-A10/credito-acumulado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="607853382-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1200->607853382-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1200`, `607853382-power-search-button`];
    cy.visit('http://system-A10/credito-acumulado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="607853382-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/registro-1310->1138421757-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/registro-1310`, `1138421757-novo`];
    cy.visit('http://system-A10/movimentacao-combustivel?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1138421757-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/registro-1310->1138421757-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/registro-1310`, `1138421757-power-search-button`];
    cy.visit('http://system-A10/movimentacao-combustivel?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1138421757-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/movimentacao-1300->962006845-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/movimentacao-1300`, `962006845-novo`];
    cy.visit('http://system-A10/bomba-combustivel?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="962006845-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/movimentacao-1300->962006845-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/movimentacao-1300`, `962006845-power-search-button`];
    cy.visit('http://system-A10/bomba-combustivel?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="962006845-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1390->1377825846-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1390`, `1377825846-novo`];
    cy.visit('http://system-A10/sped-fiscal/produsi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1377825846-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1390->1377825846-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1390`, `1377825846-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/produsi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1377825846-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-novo`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-power-search-button`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-percentual por município`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-percentual por município`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-percentual por município"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-excluir`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-excluir`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-valores-1400->2463159941-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-valores-1400`, `2463159941-novo`];
    cy.visit('http://system-A10/sped-fiscal/valor-agregado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463159941-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-valores-1400->2463159941-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-valores-1400`, `2463159941-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/valor-agregado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463159941-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600->2529762439-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`, `2529762439-novo`];
    cy.visit('http://system-A10/operacao-cartao/R1600');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2529762439-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600->2529762439-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`, `2529762439-power-search-button`];
    cy.visit('http://system-A10/operacao-cartao/R1600');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2529762439-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600->2529762439-power-search-input and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`, `2529762439-power-search-input`];
    cy.clickIfExist(`[data-cy="sped-fiscal"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1"]`);
    cy.clickIfExist(`[data-cy="sped-fiscal/bloco-1/total-1600"]`);
    cy.fillInputPowerSearch(`[data-cy="2529762439-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/documentos-1700->3806069425-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/documentos-1700`, `3806069425-novo`];
    cy.visit('http://system-A10/controle-aidf?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806069425-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/documentos-1700->3806069425-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/documentos-1700`, `3806069425-power-search-button`];
    cy.visit('http://system-A10/controle-aidf?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806069425-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/dcta-1800->1451465994-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/dcta-1800`, `1451465994-novo`];
    cy.visit('http://system-A10/sped-fiscal/dcta?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1451465994-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/dcta-1800->1451465994-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/dcta-1800`, `1451465994-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/dcta?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1451465994-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/informacao-adicional-apuracao->1404137609-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/informacao-adicional-apuracao`, `1404137609-novo`];
    cy.visit('http://system-A10/informacao-adicional-apuracao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1404137609-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/informacao-adicional-apuracao->1404137609-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/informacao-adicional-apuracao`, `1404137609-power-search-button`];
    cy.visit('http://system-A10/informacao-adicional-apuracao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1404137609-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-novo`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2738151926-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2738151926-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-power-search-button`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2738151926-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario->2893041617-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/lancamento-inventario`, `2893041617-novo`];
    cy.visit('http://system-A10/fiscal/lancamento-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2893041617-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario->2893041617-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/lancamento-inventario`, `2893041617-power-search-button`];
    cy.visit('http://system-A10/fiscal/lancamento-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2893041617-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-power-search-button`];
    cy.visit('http://system-A10/consumo-especifico-padrao?temInsumos=~eq~S%7C%7CSim');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3433597533-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`];
    cy.visit('http://system-A10/consumo-especifico-padrao?temInsumos=~eq~S%7C%7CSim');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3433597533-insumos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-detalhes da mercadoria`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-detalhes da mercadoria`];
    cy.visit('http://system-A10/consumo-especifico-padrao?temInsumos=~eq~S%7C%7CSim');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3433597533-detalhes da mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/estoque-k200->1995582512-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/estoque-k200`, `1995582512-novo`];
    cy.visit('http://system-A10/lpe-inventario/lancamento-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1995582512-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/estoque-k200->1995582512-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/estoque-k200`, `1995582512-power-search-button`];
    cy.visit('http://system-A10/lpe-inventario/lancamento-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1995582512-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-novo`];
    cy.visit('http://system-A10/desmontagem-mercadoria?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2610433226-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-power-search-button`];
    cy.visit('http://system-A10/desmontagem-mercadoria?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2610433226-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/outras-k220->1174805854-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/outras-k220`, `1174805854-novo`];
    cy.visit('http://system-A10/lancto-redesig-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1174805854-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/outras-k220->1174805854-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/outras-k220`, `1174805854-power-search-button`];
    cy.visit('http://system-A10/lancto-redesig-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1174805854-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/itens-k230-k235->2438584476-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/itens-k230-k235`, `2438584476-novo`];
    cy.visit('http://system-A10/lpe-inventario/fisordprod?mesAnoProducao=~eq~042024%7C%7C042024&estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2438584476-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/itens-k230-k235->2438584476-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/itens-k230-k235`, `2438584476-power-search-button`];
    cy.visit('http://system-A10/lpe-inventario/fisordprod?mesAnoProducao=~eq~042024%7C%7C042024&estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2438584476-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-novo`];
    cy.visit('http://system-A10/producao-terceiro?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3261365695-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-power-search-button`];
    cy.visit('http://system-A10/producao-terceiro?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3261365695-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/reprocessamento-k260-k265->2924958288-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/reprocessamento-k260-k265`, `2924958288-novo`];
    cy.visit('http://system-A10/reprocessamento-reparo-prod-insu?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2924958288-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/reprocessamento-k260-k265->2924958288-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/reprocessamento-k260-k265`, `2924958288-power-search-button`];
    cy.visit('http://system-A10/reprocessamento-reparo-prod-insu?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2924958288-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k280->3526005790-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k280`, `3526005790-novo`];
    cy.visit('http://system-A10/lpe-inventario/estoque-escriturado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3526005790-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k280->3526005790-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k280`, `3526005790-power-search-button`];
    cy.visit('http://system-A10/lpe-inventario/estoque-escriturado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3526005790-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k290->3875892842-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k290`, `3875892842-novo`];
    cy.visit('http://system-A10/producao-conjunta?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3875892842-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k290->3875892842-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k290`, `3875892842-power-search-button`];
    cy.visit('http://system-A10/producao-conjunta?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3875892842-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k300->549546469-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k300`, `549546469-novo`];
    cy.visit('http://system-A10/producao-conjunta-terc?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="549546469-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k300->549546469-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k300`, `549546469-power-search-button`];
    cy.visit('http://system-A10/producao-conjunta-terc?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="549546469-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-button`];
    cy.visit('http://system-A10/operacoes-produtos-anp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3657122254-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-novo`];
    cy.visit('http://system-A10/operacoes-produtos-anp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3657122254-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-power-search-button`];
    cy.visit('http://system-A10/operacoes-produtos-anp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3657122254-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consulta-itens-componente->1126086030-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consulta-itens-componente`, `1126086030-power-search-button`];
    cy.visit('http://system-A10/lpe-inventario/item-componente?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1126086030-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-executar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-executar`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-agendamentos`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-visualização`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-visualização`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-executar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-executar`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-agendamentos`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-agendamentos`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-visualização`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-visualização`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-executar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-executar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-agendamentos`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-agendamentos`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-visualização`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-visualização`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-regerar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-regerar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-detalhes`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-detalhes`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-executar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-executar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-agendamentos`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-agendamentos`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-power-search-button`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-visualização`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-visualização`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-gerenciar labels->3552734270-fechar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-gerenciar labels`, `3552734270-fechar`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3552734270-gerenciar labels"]`);
    cy.clickIfExist(`[data-cy="3552734270-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-visualizar/editar->664774363-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-visualizar/editar`, `664774363-salvar`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="664774363-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao->3552734270-visualizar/editar->664774363-voltar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao`, `3552734270-visualizar/editar`, `664774363-voltar`];
    cy.visit('http://system-A10/configuracao-obrigacao-fiscal/editar/55032');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="664774363-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ir para todas as obrigações->1723811533-voltar às obrigações do módulo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ir para todas as obrigações`, `1723811533-voltar às obrigações do módulo`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="1723811533-voltar às obrigações do módulo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-nova solicitação->1723811533-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-nova solicitação`, `1723811533-salvar`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="1723811533-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-nova solicitação->1723811533-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-nova solicitação`, `1723811533-cancelar`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="1723811533-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-power-search-button`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-visualização`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-abrir visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-abrir visualização`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-visualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-visualizar`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração->2059531610-rightoutlined`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`, `2059531610-rightoutlined`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059531610-rightoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração->2059531610-habilitar edição`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`, `2059531610-habilitar edição`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059531610-habilitar edição"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração->2059531610-abrir janela de edição`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`, `2059531610-abrir janela de edição`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059531610-abrir janela de edição"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-visualizar resultado da geração->1723811533-aumentar o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-visualizar resultado da geração`, `1723811533-aumentar o zoom`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="1723811533-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-visualizar resultado da geração->1723811533-diminuir o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-visualizar resultado da geração`, `1723811533-diminuir o zoom`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="1723811533-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-visualizar resultado da geração->1723811533-expandir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-visualizar resultado da geração`, `1723811533-expandir`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="1723811533-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-visualizar resultado da geração->1723811533-download`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-visualizar resultado da geração`, `1723811533-download`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="1723811533-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-protocolo transmissão->1982785510-editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-protocolo transmissão`, `1982785510-editar`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914/protocolos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1982785510-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->969732086-visualização->969732086-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-visualização`, `969732086-item-`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="969732086-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-abrir visualização->969732086-aumentar o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-abrir visualização`, `969732086-aumentar o zoom`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-abrir visualização->969732086-diminuir o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-abrir visualização`, `969732086-diminuir o zoom`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-abrir visualização->969732086-expandir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-abrir visualização`, `969732086-expandir`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-abrir visualização->969732086-download`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-abrir visualização`, `969732086-download`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-visualizar->969732086-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-visualizar`, `969732086-dados disponíveis para impressão`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualizar"]`);
    cy.clickIfExist(`[data-cy="969732086-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-novo->4042060142-criar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-novo`, `4042060142-criar`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-novo"]`);
    cy.clickIfExist(`[data-cy="4042060142-criar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-novo->4042060142-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-novo`, `4042060142-cancelar`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-novo"]`);
    cy.clickIfExist(`[data-cy="4042060142-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->4042060142-novo->4042060142-input-number-ano and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-novo`, `4042060142-input-number-ano`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-novo"]`);
    cy.fillInput(`[data-cy="4042060142-input-number-ano"] textarea`, `8`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-editar->4042060142-remover item`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-editar`, `4042060142-remover item`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-editar"]`);
    cy.clickIfExist(`[data-cy="4042060142-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->4042060142-editar->4042060142-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `4042060142-editar`, `4042060142-salvar`];
    cy.visit('http://system-A10/dominio-periodicidade');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4042060142-editar"]`);
    cy.clickIfExist(`[data-cy="4042060142-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->757558734-novo->757558734-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `757558734-novo`, `757558734-cancelar`];
    cy.visit('http://system-A10/obrigacoes/configuracao-estabelecimento/obrigacao-estabelecimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="757558734-novo"]`);
    cy.clickIfExist(`[data-cy="757558734-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-executar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-executar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-agendamentos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-agendamentos`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-power-search-button`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-visualização`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-executar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-executar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-agendamentos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-agendamentos`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-power-search-button`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-visualização`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-regerar`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-detalhes`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-abrir visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-excluir`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-ajuda contextualizada`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-ajuda contextualizada`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-ajuda contextualizada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-regerar`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-detalhes`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-abrir visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-excluir`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-ajuda contextualizada`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-ajuda contextualizada`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-ajuda contextualizada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-ajuda contextualizada`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-ajuda contextualizada`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-ajuda contextualizada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-regerar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-detalhes`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lancamentos-ajustes->1093322407-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lancamentos-ajustes`, `1093322407-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/lancamentos-ajustes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1093322407-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lancamentos-ajustes->1093322407-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lancamentos-ajustes`, `1093322407-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/lancamentos-ajustes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1093322407-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/lancamentos-ajustes->1093322407-power-search-input and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lancamentos-ajustes`, `1093322407-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/lancamentos-ajustes"]`);
    cy.fillInputPowerSearch(`[data-cy="1093322407-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-ajuda`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-ajuda`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-regerar`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-detalhes`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-abrir visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-excluir`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/operacao-com-cartao->667902761-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/operacao-com-cartao`, `667902761-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/operacao-com-cartao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="667902761-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/operacao-com-cartao->667902761-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/operacao-com-cartao`, `667902761-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/operacao-com-cartao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="667902761-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/operacao-com-cartao->667902761-power-search-input and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/operacao-com-cartao`, `667902761-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/operacao-com-cartao"]`);
    cy.fillInputPowerSearch(`[data-cy="667902761-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-executar`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-agendamentos`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-regerar`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-detalhes`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-abrir visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-carregar mais`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-carregar mais`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/valores-agregados->897726810-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/valores-agregados`, `897726810-power-search-button`];
    cy.visit('http://system-A10/relatorios/apuracao/valores-agregados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="897726810-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/valores-agregados->897726810-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/valores-agregados`, `897726810-visualização`];
    cy.visit('http://system-A10/relatorios/apuracao/valores-agregados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="897726810-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/valores-agregados->897726810-power-search-input and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/valores-agregados`, `897726810-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao"]`);
    cy.clickIfExist(`[data-cy="relatorios/apuracao/valores-agregados"]`);
    cy.fillInputPowerSearch(`[data-cy="897726810-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis-snapshot->1651567516-exibir dados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis-snapshot`, `1651567516-exibir dados`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis-snapshot');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1651567516-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-executar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-agendamentos`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-power-search-button`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-visualização`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-regerar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-detalhes`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-excluir`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4204759012-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4204759012-power-search-button`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204759012-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4204759012-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4204759012-visualização`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204759012-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4204759012-power-search-input and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4204759012-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/dof-sem-item"]`);
    cy.fillInputPowerSearch(`[data-cy="4204759012-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-executar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-agendamentos`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-power-search-button`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-visualização`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-regerar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-detalhes`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-abrir visualização`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-excluir`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/quebra-sequencia-documentos->4125632896-exibir dados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/quebra-sequencia-documentos`, `4125632896-exibir dados`];
    cy.visit('http://system-A10/relatorios/inconsistencias/quebra-sequencia-documentos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4125632896-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/inconsistencias->relatorios/inconsistencias/quebra-sequencia-documentos->4125632896-input-SERIE-4125632896-input-PROCESSODIAS-4125632896-input-PROCESSODIASAVANCAR and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/quebra-sequencia-documentos`, `4125632896-input-SERIE-4125632896-input-PROCESSODIAS-4125632896-input-PROCESSODIASAVANCAR`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias"]`);
    cy.clickIfExist(`[data-cy="relatorios/inconsistencias/quebra-sequencia-documentos"]`);
    cy.fillInput(`[data-cy="4125632896-input-SERIE"] textarea`, `invoice`);
    cy.fillInput(`[data-cy="4125632896-input-PROCESSODIAS"] textarea`, `Cordoba Oro`);
    cy.fillInput(`[data-cy="4125632896-input-PROCESSODIASAVANCAR"] textarea`, `Jewelery`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-regerar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-detalhes`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-abrir visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-excluir`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-executar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-agendamentos`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-power-search-button`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-regerar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-detalhes`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-abrir visualização`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-excluir`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfe-59->2475223840-exibir dados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfe-59`, `2475223840-exibir dados`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfe-59');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2475223840-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-fiscal->2564352239-exibir dados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-fiscal`, `2564352239-exibir dados`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-fiscal');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2564352239-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-fiscal-dof->1063441245-exibir dados`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-fiscal-dof`, `1063441245-exibir dados`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-fiscal-dof');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1063441245-exibir dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-executar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-agendamentos`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-power-search-button`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-visualização`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-regerar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-regerar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-detalhes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-detalhes`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-abrir visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-abrir visualização`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-excluir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-excluir`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-executar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-agendamentos`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-power-search-button`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-ajuda contextualizada`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-ajuda contextualizada`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-ajuda contextualizada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-visualização`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-executar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-agendamentos`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-power-search-button`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-ajuda contextualizada`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-ajuda contextualizada`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-ajuda contextualizada"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-visualização`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo->3988544486-pesquisar dof`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`, `3988544486-pesquisar dof`];
    cy.visit('http://system-A10/desfazimento-negocios/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3988544486-pesquisar dof"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo->3988544486-salvar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`, `3988544486-salvar`];
    cy.visit('http://system-A10/desfazimento-negocios/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3988544486-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo->3988544486-voltar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`, `3988544486-voltar`];
    cy.visit('http://system-A10/desfazimento-negocios/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3988544486-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo->3988544486-powerselect-estCodigo-3988544486-textarea-motivo and submit`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`, `3988544486-powerselect-estCodigo-3988544486-textarea-motivo`];
    cy.visit('http://system-A10/desfazimento-negocios?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2568354531-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3988544486-powerselect-estCodigo"] input`);
    cy.fillInput(`[data-cy="3988544486-textarea-motivo"] input`, `Direto`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-info-adicional->2264139126-novo->2139012521-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`, `2264139126-novo`, `2139012521-salvar`];
    cy.visit('http://system-A10/regra-info-adicional/E115/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2139012521-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-info-adicional->2264139126-novo->2139012521-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`, `2264139126-novo`, `2139012521-voltar`];
    cy.visit('http://system-A10/regra-info-adicional/E115/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2139012521-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-info-adicional->2264139126-novo->2139012521-powerselect-estCodigo-2139012521-powerselect-codAjuste-2139012521-powerselect-cfopCodigo-2139012521-powerselect-tipoTot and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-info-adicional`, `2264139126-novo`, `2139012521-powerselect-estCodigo-2139012521-powerselect-codAjuste-2139012521-powerselect-cfopCodigo-2139012521-powerselect-tipoTot`];
    cy.visit('http://system-A10/regra-info-adicional/E115');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2264139126-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2139012521-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2139012521-powerselect-codAjuste"] input`);
    cy.fillInputPowerSelect(`[data-cy="2139012521-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2139012521-powerselect-tipoTot"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-novo->1457766198-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-novo`, `1457766198-salvar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1457766198-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-novo->1457766198-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-novo`, `1457766198-voltar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1457766198-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-novo->1457766198-input-rgajCodigo-1457766198-powerselect-indBaseAju-1457766198-checkbox-indComplPreco-1457766198-checkbox-indComplImposto-1457766198-checkbox-indComplPrecoImposto-1457766198-radio-imposto-1457766198-textarea-obsRegra and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-novo`, `1457766198-input-rgajCodigo-1457766198-powerselect-indBaseAju-1457766198-checkbox-indComplPreco-1457766198-checkbox-indComplImposto-1457766198-checkbox-indComplPrecoImposto-1457766198-radio-imposto-1457766198-textarea-obsRegra`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-novo"]`);
    cy.fillInput(`[data-cy="1457766198-input-rgajCodigo"] textarea`, `SMTP`);
    cy.fillInputPowerSelect(`[data-cy="1457766198-powerselect-indBaseAju"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-radio-imposto"] input`);
    cy.fillInput(`[data-cy="1457766198-textarea-obsRegra"] input`, `extensible`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-exportar regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-exportar regras`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-exportar regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-importar regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-importar regras`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-importar regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-guias sem código de ajuste->3390840739-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-guias sem código de ajuste`, `3390840739-novo`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_DIFA/guia/E116');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390840739-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-guias sem código de ajuste->3390840739-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-guias sem código de ajuste`, `3390840739-power-search-button`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_DIFA/guia/E116');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390840739-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-guia sem código de ajuste`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-guia sem código de ajuste`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-guia sem código de ajuste"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-testar regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-testar regra`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-testar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-copiar regra`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-copiar regra`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-copiar regra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-selecionar critérios`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-selecionar critérios`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-selecionar critérios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-remover item`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-salvar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-voltar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-powerselect-indBaseAju-3352686717-radio-indEntradaSaida-3352686717-checkbox-indComplPreco-3352686717-checkbox-indComplImposto-3352686717-checkbox-indComplPrecoImposto-3352686717-radio-imposto-3352686717-textarea-obsRegra and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-powerselect-indBaseAju-3352686717-radio-indEntradaSaida-3352686717-checkbox-indComplPreco-3352686717-checkbox-indComplImposto-3352686717-checkbox-indComplPrecoImposto-3352686717-radio-imposto-3352686717-textarea-obsRegra`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="3352686717-powerselect-indBaseAju"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3352686717-radio-indEntradaSaida"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3352686717-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3352686717-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3352686717-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3352686717-radio-imposto"] input`);
    cy.fillInput(`[data-cy="3352686717-textarea-obsRegra"] input`, `Auto Loan Account`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-novo`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4266322252-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-power-search-button`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4266322252-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-visualizar/editar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-visualizar/editar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4266322252-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-excluir`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4266322252-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-novo->526076071-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-novo`, `526076071-salvar`];
    cy.visit('http://system-A10/regras-recolhimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="526076071-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-novo->526076071-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-novo`, `526076071-voltar`];
    cy.visit('http://system-A10/regras-recolhimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="526076071-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-novo->526076071-input-codigo-526076071-powerselect-imposto-526076071-textarea-observacao-526076071-powerselect-codObrigacao and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-novo`, `526076071-input-codigo-526076071-powerselect-imposto-526076071-textarea-observacao-526076071-powerselect-codObrigacao`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-novo"]`);
    cy.fillInput(`[data-cy="526076071-input-codigo"] textarea`, `invoice`);
    cy.fillInputPowerSelect(`[data-cy="526076071-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="526076071-textarea-observacao"] input`, `Handmade Concrete Chicken`);
    cy.fillInputPowerSelect(`[data-cy="526076071-powerselect-codObrigacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-exportar regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-exportar regras`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-exportar regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-importar regras`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-importar regras`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-importar regras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-selectoutlined->270269936-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-selectoutlined`, `270269936-power-search-button`];
    cy.visit('http://system-A10/regras-recolhimento/RVF_IPI/regras-recolhimento-est');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="270269936-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-selectoutlined->270269936-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-selectoutlined`, `270269936-power-search-input`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-selectoutlined"]`);
    cy.fillInputPowerSearch(`[data-cy="270269936-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined->2858606894-mais operações`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`, `2858606894-mais operações`];
    cy.visit('http://system-A10/regras-recolhimento/editar/RVF_IPI');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2858606894-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined->2858606894-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`, `2858606894-remover item`];
    cy.visit('http://system-A10/regras-recolhimento/editar/RVF_IPI');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2858606894-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined->2858606894-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`, `2858606894-salvar`];
    cy.visit('http://system-A10/regras-recolhimento/editar/RVF_IPI');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2858606894-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined->2858606894-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`, `2858606894-voltar`];
    cy.visit('http://system-A10/regras-recolhimento/editar/RVF_IPI');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2858606894-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined->2858606894-powerselect-imposto-2858606894-powerselect-codReceita-2858606894-textarea-observacao and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`, `2858606894-powerselect-imposto-2858606894-powerselect-codReceita-2858606894-textarea-observacao`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="2858606894-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="2858606894-powerselect-codReceita"] input`);
    cy.fillInput(`[data-cy="2858606894-textarea-observacao"] input`, `parse`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1701794451-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-voltar`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1701794451-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-powerselect-cfopCodigo-1701794451-powerselect-nopCodigo-1701794451-powerselect-edofCodigo-1701794451-powerselect-finCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-powerselect-cfopCodigo-1701794451-powerselect-nopCodigo-1701794451-powerselect-edofCodigo-1701794451-powerselect-finCodigo`];
    cy.visit('http://system-A10/inteligencia-fiscal/rlcpe');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="739716742-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1701794451-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1701794451-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1701794451-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1701794451-powerselect-finCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-estabelecimentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-estabelecimentos`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/682');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-estabelecimentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-remover item`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/682');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-salvar`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/682');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-voltar`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/682');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-powerselect-cfopCodigo-3989490202-powerselect-nopCodigo-3989490202-powerselect-edofCodigo-3989490202-powerselect-finCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-powerselect-cfopCodigo-3989490202-powerselect-nopCodigo-3989490202-powerselect-edofCodigo-3989490202-powerselect-finCodigo`];
    cy.visit('http://system-A10/inteligencia-fiscal/rlcpe');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="739716742-eyeoutlined"]`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-finCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->sped-fiscal/bloco-1/informacao-1400->3681969890-plusoutlined->3681969890-input-titulo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `sped-fiscal/bloco-1/informacao-1400`, `3681969890-plusoutlined`, `3681969890-input-titulo`];
    cy.visit('http://system-A10/spedf-regra');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3681969890-plusoutlined"]`);
    cy.fillInput(`[data-cy="3681969890-input-titulo"] textarea`, `hack`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-identificação dos itens do documento fiscal (idf)->4066098089-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-identificação dos itens do documento fiscal (idf)`, `4066098089-power-search-button`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal/1/5005506/idf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4066098089-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-identificação dos itens do documento fiscal (idf)->4066098089-fileoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-identificação dos itens do documento fiscal (idf)`, `4066098089-fileoutlined`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal/1/5005506/idf');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4066098089-fileoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-gerar lfis->3748309102-imprimir relatório`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-gerar lfis`, `3748309102-imprimir relatório`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3748309102-gerar lfis"]`);
    cy.clickIfExist(`[data-cy="3748309102-imprimir relatório"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof->3748309102-gerar lfis->3748309102-apagar relatório`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/consulta-lfis-dof`, `3748309102-gerar lfis`, `3748309102-apagar relatório`];
    cy.visit('http://system-A10/consulta-geracao-lancamento-fiscal?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3748309102-gerar lfis"]`);
    cy.clickIfExist(`[data-cy="3748309102-apagar relatório"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo->2045306075-pesquisar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`, `2045306075-pesquisar`];
    cy.visit('http://system-A10/lancamento-ajuste/C197/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2045306075-pesquisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo->2045306075-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`, `2045306075-salvar`];
    cy.visit('http://system-A10/lancamento-ajuste/C197/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2045306075-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo->2045306075-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`, `2045306075-voltar`];
    cy.visit('http://system-A10/lancamento-ajuste/C197/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2045306075-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo->2045306075-powerselect-estCodigo-2045306075-powerselect-codAjuste-2045306075-input-descrAjuste-2045306075-input-monetary-vlBcIcms-2045306075-input-monetary-vlIcmsTotal-2045306075-input-monetary-aliqIcms-2045306075-input-monetary-vlOutros-2045306075-input-observacao-2045306075-input-txtCompl-2045306075-powerselect-codReceita-2045306075-powerselect-codObr-2045306075-powerselect-cpgCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`, `2045306075-powerselect-estCodigo-2045306075-powerselect-codAjuste-2045306075-input-descrAjuste-2045306075-input-monetary-vlBcIcms-2045306075-input-monetary-vlIcmsTotal-2045306075-input-monetary-aliqIcms-2045306075-input-monetary-vlOutros-2045306075-input-observacao-2045306075-input-txtCompl-2045306075-powerselect-codReceita-2045306075-powerselect-codObr-2045306075-powerselect-cpgCodigo`];
    cy.visit('http://system-A10/lancamento-ajuste/C197');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3361402062-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2045306075-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2045306075-powerselect-codAjuste"] input`);
    cy.fillInput(`[data-cy="2045306075-input-descrAjuste"] textarea`, `maximize`);
    cy.fillInput(`[data-cy="2045306075-input-monetary-vlBcIcms"] textarea`, `1,13`);
    cy.fillInput(`[data-cy="2045306075-input-monetary-vlIcmsTotal"] textarea`, `7,73`);
    cy.fillInput(`[data-cy="2045306075-input-monetary-aliqIcms"] textarea`, `7,24`);
    cy.fillInput(`[data-cy="2045306075-input-monetary-vlOutros"] textarea`, `1,8`);
    cy.fillInput(`[data-cy="2045306075-input-observacao"] textarea`, `Generic`);
    cy.fillInput(`[data-cy="2045306075-input-txtCompl"] textarea`, `Ponte`);
    cy.fillInputPowerSelect(`[data-cy="2045306075-powerselect-codReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="2045306075-powerselect-codObr"] input`);
    cy.fillInputPowerSelect(`[data-cy="2045306075-powerselect-cpgCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-novo->1281797189-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-novo`, `1281797189-salvar`];
    cy.visit('http://system-A10/informacoes-adicionais-e115/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1281797189-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-novo->1281797189-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-novo`, `1281797189-voltar`];
    cy.visit('http://system-A10/informacoes-adicionais-e115/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1281797189-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-novo->1281797189-powerselect-imposto-1281797189-input-codInfAdic-1281797189-input-monetary-vlInfAdic-1281797189-textarea-descrInfAdic and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-novo`, `1281797189-powerselect-imposto-1281797189-input-codInfAdic-1281797189-input-monetary-vlInfAdic-1281797189-textarea-descrInfAdic`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1281797189-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="1281797189-input-codInfAdic"] textarea`, `pricing structure`);
    cy.fillInput(`[data-cy="1281797189-input-monetary-vlInfAdic"] textarea`, `1,31`);
    cy.fillInput(`[data-cy="1281797189-textarea-descrInfAdic"] input`, `invoice`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-mais operações->50637604-item-gerar automatização e115`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-mais operações`, `50637604-item-gerar automatização e115`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-mais operações"]`);
    cy.clickIfExist(`[data-cy="50637604-item-gerar automatização e115"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-mais operações->50637604-item-excluir registros e115`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-mais operações`, `50637604-item-excluir registros e115`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-mais operações"]`);
    cy.clickIfExist(`[data-cy="50637604-item-excluir registros e115"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações adicionais`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações adicionais`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações adicionais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações para a gnre`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações para a gnre`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações para a gnre"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-salvar`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-voltar`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-powerselect-estCodigo-518106878-powerselect-imposto-518106878-powerselect-ufCodigo-518106878-powerselect-periodicidade-518106878-input-codDetalheReceita-518106878-input-monetary-vlImposto-518106878-input-monetary-vlAcresFinanc-518106878-input-monetary-vlJuros-518106878-input-monetary-vlMulta-518106878-input-monetary-vlHorAdv-518106878-input-monetary-vlCompensacao-518106878-input-codRecCred-518106878-input-monetary-vlTotRecol-518106878-textarea-descProcesso-518106878-input-numProcesso-518106878-powerselect-indProcesso-518106878-textarea-observacao and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-powerselect-estCodigo-518106878-powerselect-imposto-518106878-powerselect-ufCodigo-518106878-powerselect-periodicidade-518106878-input-codDetalheReceita-518106878-input-monetary-vlImposto-518106878-input-monetary-vlAcresFinanc-518106878-input-monetary-vlJuros-518106878-input-monetary-vlMulta-518106878-input-monetary-vlHorAdv-518106878-input-monetary-vlCompensacao-518106878-input-codRecCred-518106878-input-monetary-vlTotRecol-518106878-textarea-descProcesso-518106878-input-numProcesso-518106878-powerselect-indProcesso-518106878-textarea-observacao`];
    cy.visit('http://system-A10/guia-recolhimento-imp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtFatoGeradorImposto=~mth~1712493261029D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2583632075-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-imposto"] input`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-ufCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-periodicidade"] input`);
    cy.fillInput(`[data-cy="518106878-input-codDetalheReceita"] textarea`, `website`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlImposto"] textarea`, `5,67`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlAcresFinanc"] textarea`, `6,04`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlJuros"] textarea`, `6,45`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlMulta"] textarea`, `2,69`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlHorAdv"] textarea`, `2,44`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlCompensacao"] textarea`, `6,85`);
    cy.fillInput(`[data-cy="518106878-input-codRecCred"] textarea`, `Contas`);
    cy.fillInput(`[data-cy="518106878-input-monetary-vlTotRecol"] textarea`, `6,93`);
    cy.fillInput(`[data-cy="518106878-textarea-descProcesso"] input`, `Snior`);
    cy.fillInput(`[data-cy="518106878-input-numProcesso"] textarea`, `Concrete`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-indProcesso"] input`);
    cy.fillInput(`[data-cy="518106878-textarea-observacao"] input`, `Rio Grande do Sul`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg->2949867902-novo->1119016491-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`, `2949867902-novo`, `1119016491-salvar`];
    cy.visit('http://system-A10/item-sintegra-mg/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1119016491-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg->2949867902-novo->1119016491-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`, `2949867902-novo`, `1119016491-voltar`];
    cy.visit('http://system-A10/item-sintegra-mg/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1119016491-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg->2949867902-novo->1119016491-powerselect-mdofCodigo-1119016491-input-serieSubserie-1119016491-input-numero-1119016491-powerselect-remetentePfjCodigo-1119016491-input-number-idfNum-1119016491-powerselect-mercCodigo-1119016491-powerselect-cfopCodigo-1119016491-input-cstCodigo-1119016491-input-number-quantidade-1119016491-input-monetary-vlIpi-1119016491-input-monetary-vlBrutoProd-1119016491-input-monetary-vlDesconto-1119016491-input-monetary-vlBaseIcmsOp-1119016491-input-monetary-vlBaseIcmsSt-1119016491-input-monetary-aliqIcmsOp-1119016491-input-monetary-aliqIcmsSt and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/itens-sintegra-mg`, `2949867902-novo`, `1119016491-powerselect-mdofCodigo-1119016491-input-serieSubserie-1119016491-input-numero-1119016491-powerselect-remetentePfjCodigo-1119016491-input-number-idfNum-1119016491-powerselect-mercCodigo-1119016491-powerselect-cfopCodigo-1119016491-input-cstCodigo-1119016491-input-number-quantidade-1119016491-input-monetary-vlIpi-1119016491-input-monetary-vlBrutoProd-1119016491-input-monetary-vlDesconto-1119016491-input-monetary-vlBaseIcmsOp-1119016491-input-monetary-vlBaseIcmsSt-1119016491-input-monetary-aliqIcmsOp-1119016491-input-monetary-aliqIcmsSt`];
    cy.visit('http://system-A10/item-sintegra-mg?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2949867902-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1119016491-powerselect-mdofCodigo"] input`);
    cy.fillInput(`[data-cy="1119016491-input-serieSubserie"] textarea`, `haptic`);
    cy.fillInput(`[data-cy="1119016491-input-numero"] textarea`, `mesh`);
    cy.fillInputPowerSelect(`[data-cy="1119016491-powerselect-remetentePfjCodigo"] input`);
    cy.fillInput(`[data-cy="1119016491-input-number-idfNum"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="1119016491-powerselect-mercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1119016491-powerselect-cfopCodigo"] input`);
    cy.fillInput(`[data-cy="1119016491-input-cstCodigo"] textarea`, `Funcionalidade`);
    cy.fillInput(`[data-cy="1119016491-input-number-quantidade"] textarea`, `1`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-vlIpi"] textarea`, `7`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-vlBrutoProd"] textarea`, `4`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-vlDesconto"] textarea`, `6`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-vlBaseIcmsOp"] textarea`, `10`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-vlBaseIcmsSt"] textarea`, `4`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-aliqIcmsOp"] textarea`, `2`);
    cy.fillInput(`[data-cy="1119016491-input-monetary-aliqIcmsSt"] textarea`, `9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacao-dipam->133973439-novo->2627631754-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacao-dipam`, `133973439-novo`, `2627631754-salvar`];
    cy.visit('http://system-A10/obr-inf-dipam/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2627631754-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacao-dipam->133973439-novo->2627631754-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacao-dipam`, `133973439-novo`, `2627631754-voltar`];
    cy.visit('http://system-A10/obr-inf-dipam/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2627631754-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacao-dipam->133973439-novo->2627631754-powerselect-dipCodigo-2627631754-powerselect-munCodigo-2627631754-input-monetary-valor and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacao-dipam`, `133973439-novo`, `2627631754-powerselect-dipCodigo-2627631754-powerselect-munCodigo-2627631754-input-monetary-valor`];
    cy.visit('http://system-A10/obr-inf-dipam?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="133973439-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2627631754-powerselect-dipCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2627631754-powerselect-munCodigo"] input`);
    cy.fillInput(`[data-cy="2627631754-input-monetary-valor"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am->1212595578-excluir->1212595578-moreoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am`, `1212595578-excluir`, `1212595578-moreoutlined`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/imp-nota-sefaz-dia-am-capa?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1212595578-moreoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am->1212595578-excluir->1212595578-editoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dia-am`, `1212595578-excluir`, `1212595578-editoutlined`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/imp-nota-sefaz-dia-am-capa?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1212595578-editoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg->3642031708-novo->3256982541-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`, `3642031708-novo`, `3256982541-salvar`];
    cy.visit('http://system-A10/inventario-sintegra-mg/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3256982541-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg->3642031708-novo->3256982541-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`, `3642031708-novo`, `3256982541-voltar`];
    cy.visit('http://system-A10/inventario-sintegra-mg/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3256982541-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg->3642031708-novo->3256982541-powerselect-mercCodigo-3256982541-input-number-quantidade-3256982541-input-monetary-vlIcmsOp-3256982541-input-monetary-vlIcmsSt and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/inventario-sintegra-mg`, `3642031708-novo`, `3256982541-powerselect-mercCodigo-3256982541-input-number-quantidade-3256982541-input-monetary-vlIcmsOp-3256982541-input-monetary-vlIcmsSt`];
    cy.visit('http://system-A10/inventario-sintegra-mg?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3642031708-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3256982541-powerselect-mercCodigo"] input`);
    cy.fillInput(`[data-cy="3256982541-input-number-quantidade"] textarea`, `4`);
    cy.fillInput(`[data-cy="3256982541-input-monetary-vlIcmsOp"] textarea`, `4`);
    cy.fillInput(`[data-cy="3256982541-input-monetary-vlIcmsSt"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-selecionar mercadoria`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-selecionar mercadoria`];
    cy.visit('http://system-A10/lancamento-producao-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74501782-selecionar mercadoria"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-salvar`];
    cy.visit('http://system-A10/lancamento-producao-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74501782-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-voltar`];
    cy.visit('http://system-A10/lancamento-producao-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74501782-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-powerselect-estCodigo-74501782-input-mercCodigo-74501782-input-monetary-vlTributavelIcms-74501782-input-monetary-quantidade-74501782-input-monetary-vlMercadoria-74501782-input-monetary-vlIpi-74501782-input-monetary-baseIcmsOpCat17-74501782-input-numero-74501782-input-serieSubserie-74501782-powerselect-indEntradaSaida-74501782-input-tipoTransacao-74501782-powerselect-cfopCodigo-74501782-powerselect-edofCodigo-74501782-powerselect-nopCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-powerselect-estCodigo-74501782-input-mercCodigo-74501782-input-monetary-vlTributavelIcms-74501782-input-monetary-quantidade-74501782-input-monetary-vlMercadoria-74501782-input-monetary-vlIpi-74501782-input-monetary-baseIcmsOpCat17-74501782-input-numero-74501782-input-serieSubserie-74501782-powerselect-indEntradaSaida-74501782-input-tipoTransacao-74501782-powerselect-cfopCodigo-74501782-powerselect-edofCodigo-74501782-powerselect-nopCodigo`];
    cy.visit('http://system-A10/lancamento-producao-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="766548531-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="74501782-powerselect-estCodigo"] input`);
    cy.fillInput(`[data-cy="74501782-input-mercCodigo"] textarea`, `auxiliary`);
    cy.fillInput(`[data-cy="74501782-input-monetary-vlTributavelIcms"] textarea`, `9,74`);
    cy.fillInput(`[data-cy="74501782-input-monetary-quantidade"] textarea`, `9,57`);
    cy.fillInput(`[data-cy="74501782-input-monetary-vlMercadoria"] textarea`, `7,85`);
    cy.fillInput(`[data-cy="74501782-input-monetary-vlIpi"] textarea`, `8,56`);
    cy.fillInput(`[data-cy="74501782-input-monetary-baseIcmsOpCat17"] textarea`, `1,07`);
    cy.fillInput(`[data-cy="74501782-input-numero"] textarea`, `Kuwait`);
    cy.fillInput(`[data-cy="74501782-input-serieSubserie"] textarea`, `Money Market Account`);
    cy.fillInputPowerSelect(`[data-cy="74501782-powerselect-indEntradaSaida"] input`);
    cy.fillInput(`[data-cy="74501782-input-tipoTransacao"] textarea`, `Frozen`);
    cy.fillInputPowerSelect(`[data-cy="74501782-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="74501782-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="74501782-powerselect-nopCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-mais operações->766548531-item-gerar lcpe`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-mais operações`, `766548531-item-gerar lcpe`];
    cy.visit('http://system-A10/lancamento-producao-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="766548531-mais operações"]`);
    cy.clickIfExist(`[data-cy="766548531-item-gerar lcpe"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-pesquisar item nf`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-pesquisar item nf`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1490841773-pesquisar item nf"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-salvar`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1490841773-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1490841773-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-input-monetary-vlUnitResFcpSt-1490841773-input-monetary-aliqIcmsUltE-1490841773-input-monetary-vlUnitLimiteBcIcmsUltE-1490841773-input-monetary-vlUnitIcmsUltE and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-input-monetary-vlUnitResFcpSt-1490841773-input-monetary-aliqIcmsUltE-1490841773-input-monetary-vlUnitLimiteBcIcmsUltE-1490841773-input-monetary-vlUnitIcmsUltE`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4275029436-novo"]`);
    cy.fillInput(`[data-cy="1490841773-input-monetary-vlUnitResFcpSt"] textarea`, `6`);
    cy.fillInput(`[data-cy="1490841773-input-monetary-aliqIcmsUltE"] textarea`, `5`);
    cy.fillInput(`[data-cy="1490841773-input-monetary-vlUnitLimiteBcIcmsUltE"] textarea`, `5`);
    cy.fillInput(`[data-cy="1490841773-input-monetary-vlUnitIcmsUltE"] textarea`, `9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc->escrituracao-apuracao/lancamento-apuracao/dime-sc->1322398884-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, `escrituracao-apuracao/lancamento-apuracao/dime-sc`, `1322398884-novo`];
    cy.visit('http://system-A10/escrituracao-apuracao/demonstrativo-apuracao-sc?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1322398884-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc->escrituracao-apuracao/lancamento-apuracao/dime-sc->1322398884-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, `escrituracao-apuracao/lancamento-apuracao/dime-sc`, `1322398884-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/demonstrativo-apuracao-sc?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1322398884-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-executar->602971242-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-executar`, `602971242-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-executar"]`);
    cy.clickIfExist(`[data-cy="602971242-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-executar->602971242-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-executar`, `602971242-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-executar"]`);
    cy.clickIfExist(`[data-cy="602971242-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-agendamentos->602971242-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-agendamentos`, `602971242-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210452D%7C%7C210452&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-agendamentos->602971242-visualizar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-agendamentos`, `602971242-visualizar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210452D%7C%7C210452&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-visualização->602971242-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-visualização`, `602971242-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="602971242-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-detalhes->602971242-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-detalhes`, `602971242-dados disponíveis para impressão`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-detalhes"]`);
    cy.clickIfExist(`[data-cy="602971242-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-abrir visualização->602971242-aumentar o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-abrir visualização`, `602971242-aumentar o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="602971242-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-abrir visualização->602971242-diminuir o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-abrir visualização`, `602971242-diminuir o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="602971242-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-abrir visualização->602971242-expandir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-abrir visualização`, `602971242-expandir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="602971242-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-abrir visualização->602971242-download`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-abrir visualização`, `602971242-download`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="602971242-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-executar->2717158496-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-executar`, `2717158496-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-executar"]`);
    cy.clickIfExist(`[data-cy="2717158496-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-executar->2717158496-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-executar`, `2717158496-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-executar"]`);
    cy.clickIfExist(`[data-cy="2717158496-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-agendamentos->2717158496-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-agendamentos`, `2717158496-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210449D%7C%7C210449&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-visualização->2717158496-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-visualização`, `2717158496-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2717158496-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-detalhes->2717158496-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-detalhes`, `2717158496-dados disponíveis para impressão`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-detalhes"]`);
    cy.clickIfExist(`[data-cy="2717158496-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-abrir visualização->2717158496-aumentar o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-abrir visualização`, `2717158496-aumentar o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2717158496-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-abrir visualização->2717158496-diminuir o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-abrir visualização`, `2717158496-diminuir o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2717158496-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-abrir visualização->2717158496-expandir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-abrir visualização`, `2717158496-expandir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2717158496-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-abrir visualização->2717158496-download`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-abrir visualização`, `2717158496-download`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2717158496-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-gerar->4282877304-gerar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-gerar`, `4282877304-gerar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4282877304-gerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-gerar->4282877304-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-gerar`, `4282877304-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4282877304-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-gerar->4282877304-powerselect-estCodigo-4282877304-powerselect-formato and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-gerar`, `4282877304-powerselect-estCodigo-4282877304-powerselect-formato`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="77045265-gerar"]`);
    cy.fillInputPowerSelect(`[data-cy="4282877304-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4282877304-powerselect-formato"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="77045265-mais operações"]`);
    cy.clickIfExist(`[data-cy="77045265-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-executar->283960367-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-executar`, `283960367-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-executar"]`);
    cy.clickIfExist(`[data-cy="283960367-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-executar->283960367-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-executar`, `283960367-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-executar"]`);
    cy.clickIfExist(`[data-cy="283960367-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-agendamentos->283960367-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-agendamentos`, `283960367-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~51138154D%7C%7C51138154&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-visualização->283960367-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-visualização`, `283960367-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="283960367-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-detalhes->283960367-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-detalhes`, `283960367-dados disponíveis para impressão`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-detalhes"]`);
    cy.clickIfExist(`[data-cy="283960367-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-abrir visualização->283960367-aumentar o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-abrir visualização`, `283960367-aumentar o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="283960367-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-abrir visualização->283960367-diminuir o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-abrir visualização`, `283960367-diminuir o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="283960367-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-abrir visualização->283960367-expandir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-abrir visualização`, `283960367-expandir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="283960367-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-abrir visualização->283960367-download`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-abrir visualização`, `283960367-download`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="283960367-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-importar->2115853574- selecionar arquivo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-importar`, `2115853574- selecionar arquivo`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-importar"]`);
    cy.clickIfExist(`[data-cy="2115853574- selecionar arquivo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-importar->2115853574-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-importar`, `2115853574-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-importar"]`);
    cy.clickIfExist(`[data-cy="2115853574-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-importar->2115853574-powerselect-estabelecimento-2115853574-radio-tipoArquivo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-importar`, `2115853574-powerselect-estabelecimento-2115853574-radio-tipoArquivo`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-importar"]`);
    cy.fillInputPowerSelect(`[data-cy="2115853574-powerselect-estabelecimento"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2115853574-radio-tipoArquivo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/importacao-gru->2115853574-mais operações->2115853574-item-exportar leiaute padrão`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/importacao-gru`, `2115853574-mais operações`, `2115853574-item-exportar leiaute padrão`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/importacao-guias-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2115853574-mais operações"]`);
    cy.clickIfExist(`[data-cy="2115853574-item-exportar leiaute padrão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-executar->2815987121-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-executar`, `2815987121-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-executar"]`);
    cy.clickIfExist(`[data-cy="2815987121-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-executar->2815987121-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-executar`, `2815987121-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-executar"]`);
    cy.clickIfExist(`[data-cy="2815987121-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-agendamentos->2815987121-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-agendamentos`, `2815987121-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210451D%7C%7C210451&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-visualização->2815987121-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-visualização`, `2815987121-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2815987121-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-executar->4056389192-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-executar`, `4056389192-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-executar"]`);
    cy.clickIfExist(`[data-cy="4056389192-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-executar->4056389192-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-executar`, `4056389192-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-executar"]`);
    cy.clickIfExist(`[data-cy="4056389192-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-agendamentos->4056389192-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-agendamentos`, `4056389192-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245636D%7C%7C245636&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-visualização->4056389192-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-visualização`, `4056389192-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4056389192-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-executar->21201123-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-executar`, `21201123-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-executar"]`);
    cy.clickIfExist(`[data-cy="21201123-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-executar->21201123-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-executar`, `21201123-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-executar"]`);
    cy.clickIfExist(`[data-cy="21201123-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-agendamentos->21201123-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-agendamentos`, `21201123-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47905313D%7C%7C47905313&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-visualização->21201123-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-visualização`, `21201123-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="21201123-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-executar->4195790773-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-executar`, `4195790773-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-executar"]`);
    cy.clickIfExist(`[data-cy="4195790773-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-agendamentos->4195790773-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-agendamentos`, `4195790773-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~48066345D%7C%7C48066345&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-visualização->4195790773-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/limpaJobLfis`, `4195790773-visualização`, `4195790773-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4195790773-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4195790773-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-executar->832827420-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-executar`, `832827420-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-executar"]`);
    cy.clickIfExist(`[data-cy="832827420-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-executar->832827420-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-executar`, `832827420-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-executar"]`);
    cy.clickIfExist(`[data-cy="832827420-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-agendamentos->832827420-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-agendamentos`, `832827420-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~49057672D%7C%7C49057672&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-visualização->832827420-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-visualização`, `832827420-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="832827420-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-executar->3840793780-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-executar`, `3840793780-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-executar"]`);
    cy.clickIfExist(`[data-cy="3840793780-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-executar->3840793780-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-executar`, `3840793780-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-executar"]`);
    cy.clickIfExist(`[data-cy="3840793780-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-agendamentos->3840793780-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-agendamentos`, `3840793780-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~52427359D%7C%7C52427359&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-visualização->3840793780-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-visualização`, `3840793780-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3840793780-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-executar->2515312772-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-executar`, `2515312772-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-executar"]`);
    cy.clickIfExist(`[data-cy="2515312772-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-executar->2515312772-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-executar`, `2515312772-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-executar"]`);
    cy.clickIfExist(`[data-cy="2515312772-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-executar->2515312772-input-Motivo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-executar`, `2515312772-input-Motivo`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-executar"]`);
    cy.fillInput(`[data-cy="2515312772-input-Motivo"] textarea`, `webreadiness`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-agendamentos->2515312772-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-agendamentos`, `2515312772-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53141006D%7C%7C53141006&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-visualização->2515312772-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-visualização`, `2515312772-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2515312772-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1200->607853382-novo->3585802595-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1200`, `607853382-novo`, `3585802595-salvar`];
    cy.visit('http://system-A10/credito-acumulado/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585802595-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1200->607853382-novo->3585802595-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1200`, `607853382-novo`, `3585802595-voltar`];
    cy.visit('http://system-A10/credito-acumulado/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3585802595-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1200->607853382-novo->3585802595-powerselect-codigoAjuste-3585802595-input-number-sldCred-3585802595-input-number-credReceb-3585802595-input-number-credUtil-3585802595-input-number-credApr-3585802595-input-number-sldCredFim and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1200`, `607853382-novo`, `3585802595-powerselect-codigoAjuste-3585802595-input-number-sldCred-3585802595-input-number-credReceb-3585802595-input-number-credUtil-3585802595-input-number-credApr-3585802595-input-number-sldCredFim`];
    cy.visit('http://system-A10/credito-acumulado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="607853382-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3585802595-powerselect-codigoAjuste"] input`);
    cy.fillInput(`[data-cy="3585802595-input-number-sldCred"] textarea`, `10`);
    cy.fillInput(`[data-cy="3585802595-input-number-credReceb"] textarea`, `9`);
    cy.fillInput(`[data-cy="3585802595-input-number-credUtil"] textarea`, `9`);
    cy.fillInput(`[data-cy="3585802595-input-number-credApr"] textarea`, `8`);
    cy.fillInput(`[data-cy="3585802595-input-number-sldCredFim"] textarea`, `6`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/registro-1310->1138421757-novo->2636547852-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/registro-1310`, `1138421757-novo`, `2636547852-salvar`];
    cy.visit('http://system-A10/movimentacao-combustivel/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2636547852-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/registro-1310->1138421757-novo->2636547852-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/registro-1310`, `1138421757-novo`, `2636547852-voltar`];
    cy.visit('http://system-A10/movimentacao-combustivel/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2636547852-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/registro-1310->1138421757-novo->2636547852-powerselect-mercCodigo-2636547852-input-monetary-estqAbert-2636547852-input-monetary-volEntr-2636547852-input-monetary-volDisp-2636547852-input-monetary-volSaidas-2636547852-input-monetary-valAjGanho-2636547852-input-monetary-valAjPerda-2636547852-input-monetary-estqEscr-2636547852-input-monetary-fechFisico and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/registro-1310`, `1138421757-novo`, `2636547852-powerselect-mercCodigo-2636547852-input-monetary-estqAbert-2636547852-input-monetary-volEntr-2636547852-input-monetary-volDisp-2636547852-input-monetary-volSaidas-2636547852-input-monetary-valAjGanho-2636547852-input-monetary-valAjPerda-2636547852-input-monetary-estqEscr-2636547852-input-monetary-fechFisico`];
    cy.visit('http://system-A10/movimentacao-combustivel?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1138421757-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2636547852-powerselect-mercCodigo"] input`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-estqAbert"] textarea`, `1,34`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-volEntr"] textarea`, `5,34`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-volDisp"] textarea`, `4,86`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-volSaidas"] textarea`, `9,57`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-valAjGanho"] textarea`, `7,87`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-valAjPerda"] textarea`, `3,78`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-estqEscr"] textarea`, `1,18`);
    cy.fillInput(`[data-cy="2636547852-input-monetary-fechFisico"] textarea`, `9,73`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/movimentacao-1300->962006845-novo->144437196-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/movimentacao-1300`, `962006845-novo`, `144437196-salvar`];
    cy.visit('http://system-A10/bomba-combustivel/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="144437196-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/movimentacao-1300->962006845-novo->144437196-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/movimentacao-1300`, `962006845-novo`, `144437196-voltar`];
    cy.visit('http://system-A10/bomba-combustivel/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="144437196-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/movimentacao-1300->962006845-novo->144437196-input-serie-144437196-input-fabricante-144437196-input-modelo-144437196-powerselect-tipoMedicao and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/movimentacao-1300`, `962006845-novo`, `144437196-input-serie-144437196-input-fabricante-144437196-input-modelo-144437196-powerselect-tipoMedicao`];
    cy.visit('http://system-A10/bomba-combustivel?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="962006845-novo"]`);
    cy.fillInput(`[data-cy="144437196-input-serie"] textarea`, `sensor`);
    cy.fillInput(`[data-cy="144437196-input-fabricante"] textarea`, `Rustic`);
    cy.fillInput(`[data-cy="144437196-input-modelo"] textarea`, `software`);
    cy.fillInputPowerSelect(`[data-cy="144437196-powerselect-tipoMedicao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1390->1377825846-novo->3869785715-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1390`, `1377825846-novo`, `3869785715-salvar`];
    cy.visit('http://system-A10/sped-fiscal/produsi/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3869785715-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1390->1377825846-novo->3869785715-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1390`, `1377825846-novo`, `3869785715-voltar`];
    cy.visit('http://system-A10/sped-fiscal/produsi/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3869785715-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/controle-1390->1377825846-novo->3869785715-powerselect-produto-3869785715-input-number-qtdEsmagada-3869785715-input-number-qtdProduzida-3869785715-input-number-estoqueInicial-3869785715-input-number-entradaAnidHid-3869785715-input-number-outrasEntradas-3869785715-input-number-consumo-3869785715-input-number-perda-3869785715-input-number-saidaAnidHid-3869785715-input-number-saidas-3869785715-input-number-estqFin-3869785715-input-number-estIniMel-3869785715-input-number-prodDiaMel-3869785715-input-number-utilMel-3869785715-input-number-prodAlcMel-3869785715-textarea-observacao-3869785715-powerselect-codItem-3869785715-powerselect-tpResiduo-3869785715-input-number-qtdResiduo-3869785715-input-number-qtdResiduoDdg-3869785715-input-number-qtdResiduoWdg-3869785715-input-number-qtdResiduoCana and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/controle-1390`, `1377825846-novo`, `3869785715-powerselect-produto-3869785715-input-number-qtdEsmagada-3869785715-input-number-qtdProduzida-3869785715-input-number-estoqueInicial-3869785715-input-number-entradaAnidHid-3869785715-input-number-outrasEntradas-3869785715-input-number-consumo-3869785715-input-number-perda-3869785715-input-number-saidaAnidHid-3869785715-input-number-saidas-3869785715-input-number-estqFin-3869785715-input-number-estIniMel-3869785715-input-number-prodDiaMel-3869785715-input-number-utilMel-3869785715-input-number-prodAlcMel-3869785715-textarea-observacao-3869785715-powerselect-codItem-3869785715-powerselect-tpResiduo-3869785715-input-number-qtdResiduo-3869785715-input-number-qtdResiduoDdg-3869785715-input-number-qtdResiduoWdg-3869785715-input-number-qtdResiduoCana`];
    cy.visit('http://system-A10/sped-fiscal/produsi?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1377825846-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3869785715-powerselect-produto"] input`);
    cy.fillInput(`[data-cy="3869785715-input-number-qtdEsmagada"] textarea`, `9`);
    cy.fillInput(`[data-cy="3869785715-input-number-qtdProduzida"] textarea`, `7`);
    cy.fillInput(`[data-cy="3869785715-input-number-estoqueInicial"] textarea`, `4`);
    cy.fillInput(`[data-cy="3869785715-input-number-entradaAnidHid"] textarea`, `8`);
    cy.fillInput(`[data-cy="3869785715-input-number-outrasEntradas"] textarea`, `9`);
    cy.fillInput(`[data-cy="3869785715-input-number-consumo"] textarea`, `4`);
    cy.fillInput(`[data-cy="3869785715-input-number-perda"] textarea`, `6`);
    cy.fillInput(`[data-cy="3869785715-input-number-saidaAnidHid"] textarea`, `4`);
    cy.fillInput(`[data-cy="3869785715-input-number-saidas"] textarea`, `4`);
    cy.fillInput(`[data-cy="3869785715-input-number-estqFin"] textarea`, `7`);
    cy.fillInput(`[data-cy="3869785715-input-number-estIniMel"] textarea`, `8`);
    cy.fillInput(`[data-cy="3869785715-input-number-prodDiaMel"] textarea`, `8`);
    cy.fillInput(`[data-cy="3869785715-input-number-utilMel"] textarea`, `6`);
    cy.fillInput(`[data-cy="3869785715-input-number-prodAlcMel"] textarea`, `2`);
    cy.fillInput(`[data-cy="3869785715-textarea-observacao"] input`, `Bedfordshire`);
    cy.fillInputPowerSelect(`[data-cy="3869785715-powerselect-codItem"] input`);
    cy.fillInputPowerSelect(`[data-cy="3869785715-powerselect-tpResiduo"] input`);
    cy.fillInput(`[data-cy="3869785715-input-number-qtdResiduo"] textarea`, `5`);
    cy.fillInput(`[data-cy="3869785715-input-number-qtdResiduoDdg"] textarea`, `4`);
    cy.fillInput(`[data-cy="3869785715-input-number-qtdResiduoWdg"] textarea`, `10`);
    cy.fillInput(`[data-cy="3869785715-input-number-qtdResiduoCana"] textarea`, `6`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-novo->2044339865-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-novo`, `2044339865-salvar`];
    cy.visit('http://system-A10/cadastro-ipm/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2044339865-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-novo->2044339865-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-novo`, `2044339865-voltar`];
    cy.visit('http://system-A10/cadastro-ipm/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2044339865-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-novo->2044339865-powerselect-ufCodigo-2044339865-input-codIpm-2044339865-textarea-descricao and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-novo`, `2044339865-powerselect-ufCodigo-2044339865-input-codIpm-2044339865-textarea-descricao`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2044339865-powerselect-ufCodigo"] input`);
    cy.fillInput(`[data-cy="2044339865-input-codIpm"] textarea`, `Malaysian Ringgit`);
    cy.fillInput(`[data-cy="2044339865-textarea-descricao"] input`, `Soft`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-percentual por município->2807946815-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-percentual por município`, `2807946815-novo`];
    cy.visit('http://system-A10/cadastro-ipm/581/242342/AG/percentual-participacao-municipio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2807946815-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-percentual por município->2807946815-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-percentual por município`, `2807946815-power-search-button`];
    cy.visit('http://system-A10/cadastro-ipm/581/242342/AG/percentual-participacao-municipio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2807946815-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar->68489544-mais operações`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`, `68489544-mais operações`];
    cy.visit('http://system-A10/cadastro-ipm/editar/242342/AG');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="68489544-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar->68489544-remover item`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`, `68489544-remover item`];
    cy.visit('http://system-A10/cadastro-ipm/editar/242342/AG');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="68489544-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar->68489544-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`, `68489544-salvar`];
    cy.visit('http://system-A10/cadastro-ipm/editar/242342/AG');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="68489544-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar->68489544-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`, `68489544-voltar`];
    cy.visit('http://system-A10/cadastro-ipm/editar/242342/AG');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="68489544-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar->68489544-textarea-descricao and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`, `68489544-textarea-descricao`];
    cy.visit('http://system-A10/cadastro-ipm');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2692418640-visualizar/editar"]`);
    cy.fillInput(`[data-cy="68489544-textarea-descricao"] input`, `indexing`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-valores-1400->2463159941-novo->3255006596-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-valores-1400`, `2463159941-novo`, `3255006596-salvar`];
    cy.visit('http://system-A10/sped-fiscal/valor-agregado/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3255006596-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-valores-1400->2463159941-novo->3255006596-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-valores-1400`, `2463159941-novo`, `3255006596-voltar`];
    cy.visit('http://system-A10/sped-fiscal/valor-agregado/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3255006596-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-valores-1400->2463159941-novo->3255006596-powerselect-tipoApuracao-3255006596-powerselect-mes-3255006596-input-number-ano-3255006596-checkbox-indRateio-3255006596-input-number-vlMunRat-3255006596-powerselect-codIpm-3255006596-powerselect-munCodigo and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-valores-1400`, `2463159941-novo`, `3255006596-powerselect-tipoApuracao-3255006596-powerselect-mes-3255006596-input-number-ano-3255006596-checkbox-indRateio-3255006596-input-number-vlMunRat-3255006596-powerselect-codIpm-3255006596-powerselect-munCodigo`];
    cy.visit('http://system-A10/sped-fiscal/valor-agregado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2463159941-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3255006596-powerselect-tipoApuracao"] input`);
    cy.fillInputPowerSelect(`[data-cy="3255006596-powerselect-mes"] input`);
    cy.fillInput(`[data-cy="3255006596-input-number-ano"] textarea`, `1`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3255006596-checkbox-indRateio"] textarea`);
    cy.fillInput(`[data-cy="3255006596-input-number-vlMunRat"] textarea`, `1`);
    cy.fillInputPowerSelect(`[data-cy="3255006596-powerselect-codIpm"] input`);
    cy.fillInputPowerSelect(`[data-cy="3255006596-powerselect-munCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600->2529762439-novo->4021330114-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`, `2529762439-novo`, `4021330114-salvar`];
    cy.visit('http://system-A10/operacao-cartao/R1600/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4021330114-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600->2529762439-novo->4021330114-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`, `2529762439-novo`, `4021330114-voltar`];
    cy.visit('http://system-A10/operacao-cartao/R1600/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4021330114-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/total-1600->2529762439-novo->4021330114-powerselect-admPfjCodigo-4021330114-input-monetary-totCredito-4021330114-input-monetary-totDebito and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/total-1600`, `2529762439-novo`, `4021330114-powerselect-admPfjCodigo-4021330114-input-monetary-totCredito-4021330114-input-monetary-totDebito`];
    cy.visit('http://system-A10/operacao-cartao/R1600');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2529762439-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="4021330114-powerselect-admPfjCodigo"] input`);
    cy.fillInput(`[data-cy="4021330114-input-monetary-totCredito"] textarea`, `3,31`);
    cy.fillInput(`[data-cy="4021330114-input-monetary-totDebito"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/documentos-1700->3806069425-novo->556336344-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/documentos-1700`, `3806069425-novo`, `556336344-salvar`];
    cy.visit('http://system-A10/controle-aidf/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="556336344-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/documentos-1700->3806069425-novo->556336344-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/documentos-1700`, `3806069425-novo`, `556336344-voltar`];
    cy.visit('http://system-A10/controle-aidf/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="556336344-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/documentos-1700->3806069425-novo->556336344-input-modelo-556336344-input-serie-556336344-input-big-decimal-numInicial-556336344-input-big-decimal-numFinal-556336344-input-numAidf-556336344-powerselect-indDispAutorizado-556336344-textarea-mensagem and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/documentos-1700`, `3806069425-novo`, `556336344-input-modelo-556336344-input-serie-556336344-input-big-decimal-numInicial-556336344-input-big-decimal-numFinal-556336344-input-numAidf-556336344-powerselect-indDispAutorizado-556336344-textarea-mensagem`];
    cy.visit('http://system-A10/controle-aidf?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806069425-novo"]`);
    cy.fillInput(`[data-cy="556336344-input-modelo"] textarea`, `HDD`);
    cy.fillInput(`[data-cy="556336344-input-serie"] textarea`, `infomediaries`);
    cy.fillInput(`[data-cy="556336344-input-big-decimal-numInicial"] textarea`, `8,05`);
    cy.fillInput(`[data-cy="556336344-input-big-decimal-numFinal"] textarea`, `4,73`);
    cy.fillInput(`[data-cy="556336344-input-numAidf"] textarea`, `transparent`);
    cy.fillInputPowerSelect(`[data-cy="556336344-powerselect-indDispAutorizado"] input`);
    cy.fillInput(`[data-cy="556336344-textarea-mensagem"] input`, `bypassing`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/dcta-1800->1451465994-novo->1074854431-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/dcta-1800`, `1451465994-novo`, `1074854431-salvar`];
    cy.visit('http://system-A10/sped-fiscal/dcta/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1074854431-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/dcta-1800->1451465994-novo->1074854431-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/dcta-1800`, `1451465994-novo`, `1074854431-voltar`];
    cy.visit('http://system-A10/sped-fiscal/dcta/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1074854431-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/dcta-1800->1451465994-novo->1074854431-input-number-vlCarga-1074854431-input-number-vlPass-1074854431-input-number-vlFat-1074854431-input-number-indRat-1074854431-input-number-vlBcIcms-1074854431-input-number-vlIcmsAnt-1074854431-input-number-vlBcIcmsApur-1074854431-input-number-vlIcmsApur-1074854431-input-number-vlDif and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/dcta-1800`, `1451465994-novo`, `1074854431-input-number-vlCarga-1074854431-input-number-vlPass-1074854431-input-number-vlFat-1074854431-input-number-indRat-1074854431-input-number-vlBcIcms-1074854431-input-number-vlIcmsAnt-1074854431-input-number-vlBcIcmsApur-1074854431-input-number-vlIcmsApur-1074854431-input-number-vlDif`];
    cy.visit('http://system-A10/sped-fiscal/dcta?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1451465994-novo"]`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlCarga"] textarea`, `8`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlPass"] textarea`, `2`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlFat"] textarea`, `7`);
    cy.fillInput(`[data-cy="1074854431-input-number-indRat"] textarea`, `10`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlBcIcms"] textarea`, `4`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlIcmsAnt"] textarea`, `3`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlBcIcmsApur"] textarea`, `10`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlIcmsApur"] textarea`, `4`);
    cy.fillInput(`[data-cy="1074854431-input-number-vlDif"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/informacao-adicional-apuracao->1404137609-novo->1581678080-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/informacao-adicional-apuracao`, `1404137609-novo`, `1581678080-salvar`];
    cy.visit('http://system-A10/informacao-adicional-apuracao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1581678080-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/informacao-adicional-apuracao->1404137609-novo->1581678080-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/informacao-adicional-apuracao`, `1404137609-novo`, `1581678080-voltar`];
    cy.visit('http://system-A10/informacao-adicional-apuracao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1581678080-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/informacao-adicional-apuracao->1404137609-novo->1581678080-powerselect-imposto-1581678080-input-codInfAdic-1581678080-input-monetary-vlInfAdic-1581678080-textarea-descrInfAdic-1581678080-powerselect-icmsSubApuracao and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/informacao-adicional-apuracao`, `1404137609-novo`, `1581678080-powerselect-imposto-1581678080-input-codInfAdic-1581678080-input-monetary-vlInfAdic-1581678080-textarea-descrInfAdic-1581678080-powerselect-icmsSubApuracao`];
    cy.visit('http://system-A10/informacao-adicional-apuracao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1404137609-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1581678080-powerselect-imposto"] input`);
    cy.fillInput(`[data-cy="1581678080-input-codInfAdic"] textarea`, `teal`);
    cy.fillInput(`[data-cy="1581678080-input-monetary-vlInfAdic"] textarea`, `5,45`);
    cy.fillInput(`[data-cy="1581678080-textarea-descrInfAdic"] input`, `grey`);
    cy.fillInputPowerSelect(`[data-cy="1581678080-powerselect-icmsSubApuracao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-novo->3594643635-mais operações`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-novo`, `3594643635-mais operações`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3594643635-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-novo->3594643635-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-novo`, `3594643635-salvar`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3594643635-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-novo->3594643635-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-novo`, `3594643635-voltar`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3594643635-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-novo->3594643635-powerselect-estCodigo-3594643635-powerselect-mercCodigo-3594643635-powerselect-cinvCodigo and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-novo`, `3594643635-powerselect-estCodigo-3594643635-powerselect-mercCodigo-3594643635-powerselect-cinvCodigo`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2738151926-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3594643635-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3594643635-powerselect-mercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3594643635-powerselect-cinvCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2738151926-mais operações"]`);
    cy.clickIfExist(`[data-cy="2738151926-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario->2893041617-novo->3487034296-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/lancamento-inventario`, `2893041617-novo`, `3487034296-salvar`];
    cy.visit('http://system-A10/fiscal/lancamento-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3487034296-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario->2893041617-novo->3487034296-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/lancamento-inventario`, `2893041617-novo`, `3487034296-voltar`];
    cy.visit('http://system-A10/fiscal/lancamento-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3487034296-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`];
    cy.visit('http://system-A10/consumo-especifico-padrao/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="795118794-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-power-search-button`];
    cy.visit('http://system-A10/consumo-especifico-padrao/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="795118794-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-visualizar/editar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-visualizar/editar`];
    cy.visit('http://system-A10/consumo-especifico-padrao/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="795118794-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-excluir`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-excluir`];
    cy.visit('http://system-A10/consumo-especifico-padrao/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="795118794-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/estoque-k200->1995582512-novo->2050749113-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/estoque-k200`, `1995582512-novo`, `2050749113-salvar`];
    cy.visit('http://system-A10/lpe-inventario/lancamento-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2050749113-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/estoque-k200->1995582512-novo->2050749113-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/estoque-k200`, `1995582512-novo`, `2050749113-voltar`];
    cy.visit('http://system-A10/lpe-inventario/lancamento-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2050749113-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/estoque-k200->1995582512-novo->2050749113-input-number-livro-2050749113-input-number-folha-2050749113-powerselect-mercCodigo-2050749113-powerselect-uniCodigo-2050749113-powerselect-indOrigemMovimentacao-2050749113-powerselect-pfjvigIdOrigem-2050749113-input-monetary-quantidade-2050749113-input-monetary-valorTotal and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/estoque-k200`, `1995582512-novo`, `2050749113-input-number-livro-2050749113-input-number-folha-2050749113-powerselect-mercCodigo-2050749113-powerselect-uniCodigo-2050749113-powerselect-indOrigemMovimentacao-2050749113-powerselect-pfjvigIdOrigem-2050749113-input-monetary-quantidade-2050749113-input-monetary-valorTotal`];
    cy.visit('http://system-A10/lpe-inventario/lancamento-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1995582512-novo"]`);
    cy.fillInput(`[data-cy="2050749113-input-number-livro"] textarea`, `5`);
    cy.fillInput(`[data-cy="2050749113-input-number-folha"] textarea`, `2`);
    cy.fillInputPowerSelect(`[data-cy="2050749113-powerselect-mercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2050749113-powerselect-uniCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2050749113-powerselect-indOrigemMovimentacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2050749113-powerselect-pfjvigIdOrigem"] input`);
    cy.fillInput(`[data-cy="2050749113-input-monetary-quantidade"] textarea`, `1,38`);
    cy.fillInput(`[data-cy="2050749113-input-monetary-valorTotal"] textarea`, `2,95`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-novo->3658278836-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-novo`, `3658278836-salvar`];
    cy.visit('http://system-A10/desmontagem-mercadoria/AAA_DF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3658278836-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-novo->3658278836-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-novo`, `3658278836-voltar`];
    cy.visit('http://system-A10/desmontagem-mercadoria/AAA_DF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3658278836-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-novo->3658278836-input-codDocOs-3658278836-powerselect-codItemOri-3658278836-input-number-qtdOri and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-novo`, `3658278836-input-codDocOs-3658278836-powerselect-codItemOri-3658278836-input-number-qtdOri`];
    cy.visit('http://system-A10/desmontagem-mercadoria?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2610433226-novo"]`);
    cy.fillInput(`[data-cy="3658278836-input-codDocOs"] textarea`, `uniform`);
    cy.fillInputPowerSelect(`[data-cy="3658278836-powerselect-codItemOri"] input`);
    cy.fillInput(`[data-cy="3658278836-input-number-qtdOri"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/outras-k220->1174805854-novo->2910162507-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/outras-k220`, `1174805854-novo`, `2910162507-salvar`];
    cy.visit('http://system-A10/lancto-redesig-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2910162507-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/outras-k220->1174805854-novo->2910162507-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/outras-k220`, `1174805854-novo`, `2910162507-voltar`];
    cy.visit('http://system-A10/lancto-redesig-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2910162507-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/outras-k220->1174805854-novo->2910162507-powerselect-mercCodigoOri-2910162507-powerselect-mercCodigoDest-2910162507-input-number-qtd-2910162507-input-number-qtdDest-2910162507-powerselect-uniCodigo-2910162507-powerselect-edofCodigo-2910162507-input-numero and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/outras-k220`, `1174805854-novo`, `2910162507-powerselect-mercCodigoOri-2910162507-powerselect-mercCodigoDest-2910162507-input-number-qtd-2910162507-input-number-qtdDest-2910162507-powerselect-uniCodigo-2910162507-powerselect-edofCodigo-2910162507-input-numero`];
    cy.visit('http://system-A10/lancto-redesig-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1174805854-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2910162507-powerselect-mercCodigoOri"] input`);
    cy.fillInputPowerSelect(`[data-cy="2910162507-powerselect-mercCodigoDest"] input`);
    cy.fillInput(`[data-cy="2910162507-input-number-qtd"] textarea`, `2`);
    cy.fillInput(`[data-cy="2910162507-input-number-qtdDest"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="2910162507-powerselect-uniCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2910162507-powerselect-edofCodigo"] input`);
    cy.fillInput(`[data-cy="2910162507-input-numero"] textarea`, `Guernsey`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/itens-k230-k235->2438584476-novo->34286029-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/itens-k230-k235`, `2438584476-novo`, `34286029-salvar`];
    cy.visit('http://system-A10/lpe-inventario/fisordprod/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="34286029-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/itens-k230-k235->2438584476-novo->34286029-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/itens-k230-k235`, `2438584476-novo`, `34286029-voltar`];
    cy.visit('http://system-A10/lpe-inventario/fisordprod/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="34286029-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/itens-k230-k235->2438584476-novo->34286029-input-opCodigo-34286029-powerselect-mercCodigo-34286029-powerselect-uniCodigo-34286029-input-number-qtdProd-34286029-input-number-custInc and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/itens-k230-k235`, `2438584476-novo`, `34286029-input-opCodigo-34286029-powerselect-mercCodigo-34286029-powerselect-uniCodigo-34286029-input-number-qtdProd-34286029-input-number-custInc`];
    cy.visit('http://system-A10/lpe-inventario/fisordprod?mesAnoProducao=~eq~042024%7C%7C042024&estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2438584476-novo"]`);
    cy.fillInput(`[data-cy="34286029-input-opCodigo"] textarea`, `withdrawal`);
    cy.fillInputPowerSelect(`[data-cy="34286029-powerselect-mercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="34286029-powerselect-uniCodigo"] input`);
    cy.fillInput(`[data-cy="34286029-input-number-qtdProd"] textarea`, `3`);
    cy.fillInput(`[data-cy="34286029-input-number-custInc"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-novo->2538331999-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-novo`, `2538331999-salvar`];
    cy.visit('http://system-A10/producao-terceiro/AAA_DF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2538331999-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-novo->2538331999-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-novo`, `2538331999-voltar`];
    cy.visit('http://system-A10/producao-terceiro/AAA_DF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2538331999-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-novo->2538331999-powerselect-mercCodigo-2538331999-powerselect-uniCodigo-2538331999-input-number-qtdProd-2538331999-powerselect-codFase-2538331999-powerselect-pfjCodigoTerc and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-novo`, `2538331999-powerselect-mercCodigo-2538331999-powerselect-uniCodigo-2538331999-input-number-qtdProd-2538331999-powerselect-codFase-2538331999-powerselect-pfjCodigoTerc`];
    cy.visit('http://system-A10/producao-terceiro?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3261365695-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2538331999-powerselect-mercCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2538331999-powerselect-uniCodigo"] input`);
    cy.fillInput(`[data-cy="2538331999-input-number-qtdProd"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="2538331999-powerselect-codFase"] input`);
    cy.fillInputPowerSelect(`[data-cy="2538331999-powerselect-pfjCodigoTerc"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/reprocessamento-k260-k265->2924958288-novo->683253913-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/reprocessamento-k260-k265`, `2924958288-novo`, `683253913-salvar`];
    cy.visit('http://system-A10/reprocessamento-reparo-prod-insu/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="683253913-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/reprocessamento-k260-k265->2924958288-novo->683253913-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/reprocessamento-k260-k265`, `2924958288-novo`, `683253913-voltar`];
    cy.visit('http://system-A10/reprocessamento-reparo-prod-insu/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="683253913-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/reprocessamento-k260-k265->2924958288-novo->683253913-input-codOpOs-683253913-powerselect-codProd-683253913-input-number-qtdSaida-683253913-input-number-qtdRet and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/reprocessamento-k260-k265`, `2924958288-novo`, `683253913-input-codOpOs-683253913-powerselect-codProd-683253913-input-number-qtdSaida-683253913-input-number-qtdRet`];
    cy.visit('http://system-A10/reprocessamento-reparo-prod-insu?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2924958288-novo"]`);
    cy.fillInput(`[data-cy="683253913-input-codOpOs"] textarea`, `Books`);
    cy.fillInputPowerSelect(`[data-cy="683253913-powerselect-codProd"] input`);
    cy.fillInput(`[data-cy="683253913-input-number-qtdSaida"] textarea`, `2`);
    cy.fillInput(`[data-cy="683253913-input-number-qtdRet"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k280->3526005790-novo->3142406027-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k280`, `3526005790-novo`, `3142406027-salvar`];
    cy.visit('http://system-A10/lpe-inventario/estoque-escriturado/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3142406027-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k280->3526005790-novo->3142406027-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k280`, `3526005790-novo`, `3142406027-voltar`];
    cy.visit('http://system-A10/lpe-inventario/estoque-escriturado/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3142406027-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/correcao-k280->3526005790-novo->3142406027-powerselect-indEst-3142406027-powerselect-codItem-3142406027-powerselect-codPart-3142406027-input-number-qtdCorPos-3142406027-input-number-qtdCorNeg and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/correcao-k280`, `3526005790-novo`, `3142406027-powerselect-indEst-3142406027-powerselect-codItem-3142406027-powerselect-codPart-3142406027-input-number-qtdCorPos-3142406027-input-number-qtdCorNeg`];
    cy.visit('http://system-A10/lpe-inventario/estoque-escriturado?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3526005790-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3142406027-powerselect-indEst"] input`);
    cy.fillInputPowerSelect(`[data-cy="3142406027-powerselect-codItem"] input`);
    cy.fillInputPowerSelect(`[data-cy="3142406027-powerselect-codPart"] input`);
    cy.fillInput(`[data-cy="3142406027-input-number-qtdCorPos"] textarea`, `9`);
    cy.fillInput(`[data-cy="3142406027-input-number-qtdCorNeg"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k290->3875892842-novo->551224511-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k290`, `3875892842-novo`, `551224511-salvar`];
    cy.visit('http://system-A10/producao-conjunta/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="551224511-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k290->3875892842-novo->551224511-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k290`, `3875892842-novo`, `551224511-voltar`];
    cy.visit('http://system-A10/producao-conjunta/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="551224511-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k290->3875892842-novo->551224511-input-codDocOp-551224511-powerselect-codItem-551224511-powerselect-indProdInsumo-551224511-input-number-qtd and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k290`, `3875892842-novo`, `551224511-input-codDocOp-551224511-powerselect-codItem-551224511-powerselect-indProdInsumo-551224511-input-number-qtd`];
    cy.visit('http://system-A10/producao-conjunta?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3875892842-novo"]`);
    cy.fillInput(`[data-cy="551224511-input-codDocOp"] textarea`, `onetoone`);
    cy.fillInputPowerSelect(`[data-cy="551224511-powerselect-codItem"] input`);
    cy.fillInputPowerSelect(`[data-cy="551224511-powerselect-indProdInsumo"] input`);
    cy.fillInput(`[data-cy="551224511-input-number-qtd"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k300->549546469-novo->3863477796-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k300`, `549546469-novo`, `3863477796-salvar`];
    cy.visit('http://system-A10/producao-conjunta-terc/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3863477796-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k300->549546469-novo->3863477796-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k300`, `549546469-novo`, `3863477796-voltar`];
    cy.visit('http://system-A10/producao-conjunta-terc/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3863477796-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/producao-k300->549546469-novo->3863477796-powerselect-indProdInsumo-3863477796-powerselect-codItem-3863477796-input-big-decimal-qtd and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/producao-k300`, `549546469-novo`, `3863477796-powerselect-indProdInsumo-3863477796-powerselect-codItem-3863477796-input-big-decimal-qtd`];
    cy.visit('http://system-A10/producao-conjunta-terc?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="549546469-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3863477796-powerselect-indProdInsumo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3863477796-powerselect-codItem"] input`);
    cy.fillInput(`[data-cy="3863477796-input-big-decimal-qtd"] textarea`, `9,38`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-novo->2016784347-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-novo`, `2016784347-button`];
    cy.visit('http://system-A10/operacoes-produtos-anp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2016784347-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-novo->2016784347-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-novo`, `2016784347-salvar`];
    cy.visit('http://system-A10/operacoes-produtos-anp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2016784347-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-novo->2016784347-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-novo`, `2016784347-voltar`];
    cy.visit('http://system-A10/operacoes-produtos-anp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2016784347-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/operacao-produtos-anp->3657122254-novo->2016784347-powerselect-codOperacao-2016784347-input-codProdutoAnp-2016784347-input-monetary-quantidade-2016784347-powerselect-tipoOperacao-2016784347-powerselect-codPfjTerceiro-2016784347-input-monetary-valor-2016784347-input-codProdutoAnpPara and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/operacao-produtos-anp`, `3657122254-novo`, `2016784347-powerselect-codOperacao-2016784347-input-codProdutoAnp-2016784347-input-monetary-quantidade-2016784347-powerselect-tipoOperacao-2016784347-powerselect-codPfjTerceiro-2016784347-input-monetary-valor-2016784347-input-codProdutoAnpPara`];
    cy.visit('http://system-A10/operacoes-produtos-anp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3657122254-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2016784347-powerselect-codOperacao"] input`);
    cy.fillInput(`[data-cy="2016784347-input-codProdutoAnp"] textarea`, `utilize`);
    cy.fillInput(`[data-cy="2016784347-input-monetary-quantidade"] textarea`, `7,75`);
    cy.fillInputPowerSelect(`[data-cy="2016784347-powerselect-tipoOperacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2016784347-powerselect-codPfjTerceiro"] input`);
    cy.fillInput(`[data-cy="2016784347-input-monetary-valor"] textarea`, `6,83`);
    cy.fillInput(`[data-cy="2016784347-input-codProdutoAnpPara"] textarea`, `capacitor`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-executar->1426197106-múltipla seleção`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-executar`, `1426197106-múltipla seleção`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-executar"]`);
    cy.clickIfExist(`[data-cy="1426197106-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-executar->1426197106-agendar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-executar`, `1426197106-agendar`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-executar"]`);
    cy.clickIfExist(`[data-cy="1426197106-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos->1426197106-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-agendamentos`, `1426197106-voltar`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505229D%7C%7C46505229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos->1426197106-visualizar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-agendamentos`, `1426197106-visualizar`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505229D%7C%7C46505229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos->1426197106-excluir`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-agendamentos`, `1426197106-excluir`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505229D%7C%7C46505229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-visualização->1426197106-item- and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-visualização`, `1426197106-item-`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1426197106-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-executar->2503184473-múltipla seleção`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-executar`, `2503184473-múltipla seleção`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-executar"]`);
    cy.clickIfExist(`[data-cy="2503184473-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-executar->2503184473-agendar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-executar`, `2503184473-agendar`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-executar"]`);
    cy.clickIfExist(`[data-cy="2503184473-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-agendamentos->2503184473-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-agendamentos`, `2503184473-voltar`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505232D%7C%7C46505232&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-visualização->2503184473-item- and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-visualização`, `2503184473-item-`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2503184473-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-executar->1473367084-múltipla seleção`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-executar`, `1473367084-múltipla seleção`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-executar"]`);
    cy.clickIfExist(`[data-cy="1473367084-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-executar->1473367084-agendar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-executar`, `1473367084-agendar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-executar"]`);
    cy.clickIfExist(`[data-cy="1473367084-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-executar->1473367084-input-ORDEM and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-executar`, `1473367084-input-ORDEM`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-executar"]`);
    cy.fillInput(`[data-cy="1473367084-input-ORDEM"] textarea`, `Fantastic Frozen Towels`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-agendamentos->1473367084-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-agendamentos`, `1473367084-voltar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27733764D%7C%7C27733764&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-visualização->1473367084-item- and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-visualização`, `1473367084-item-`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1473367084-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-detalhes->1473367084-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-detalhes`, `1473367084-não há dados disponíveis para impressão`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-detalhes"]`);
    cy.clickIfExist(`[data-cy="1473367084-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-executar->3124260640-múltipla seleção`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-executar`, `3124260640-múltipla seleção`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-executar"]`);
    cy.clickIfExist(`[data-cy="3124260640-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-executar->3124260640-agendar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-executar`, `3124260640-agendar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-executar"]`);
    cy.clickIfExist(`[data-cy="3124260640-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-agendamentos->3124260640-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-agendamentos`, `3124260640-voltar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~51024135D%7C%7C51024135&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-visualização->3124260640-item- and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-visualização`, `3124260640-item-`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3124260640-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-visualização->969732086-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-visualização`, `969732086-item-`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="969732086-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-abrir visualização->969732086-aumentar o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-abrir visualização`, `969732086-aumentar o zoom`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-abrir visualização->969732086-diminuir o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-abrir visualização`, `969732086-diminuir o zoom`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-abrir visualização->969732086-expandir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-abrir visualização`, `969732086-expandir`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-abrir visualização->969732086-download`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-abrir visualização`, `969732086-download`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-visualizar->969732086-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-visualizar`, `969732086-dados disponíveis para impressão`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-visualizar"]`);
    cy.clickIfExist(`[data-cy="969732086-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração->2059531610-rightoutlined->2059531610-downoutlined`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`, `2059531610-rightoutlined`, `2059531610-downoutlined`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059531610-rightoutlined"]`);
    cy.clickIfExist(`[data-cy="2059531610-downoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração->2059531610-habilitar edição->2059531610-confirmar  edição`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`, `2059531610-habilitar edição`, `2059531610-confirmar  edição`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059531610-habilitar edição"]`);
    cy.clickIfExist(`[data-cy="2059531610-confirmar  edição"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-ajustar parâmetros da geração->2059531610-abrir janela de edição->2059531610-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-ajustar parâmetros da geração`, `2059531610-abrir janela de edição`, `2059531610-power-search-button`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2059531610-abrir janela de edição"]`);
    cy.clickIfExist(`[data-cy="2059531610-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-visualizar resultado da geração->1723811533-expandir->1723811533-diminuir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-visualizar resultado da geração`, `1723811533-expandir`, `1723811533-diminuir`];
    cy.visit('http://system-A10/solicitacoes-resultados?estab=AAA_DF&obrSigla=PER-DCOMP');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1723811533-visualizar resultado da geração"]`);
    cy.clickIfExist(`[data-cy="1723811533-expandir"]`);
    cy.clickIfExist(`[data-cy="1723811533-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-protocolo transmissão->1982785510-editar->1982785510-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-protocolo transmissão`, `1982785510-editar`, `1982785510-salvar`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914/protocolos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1982785510-editar"]`);
    cy.clickIfExist(`[data-cy="1982785510-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-protocolo transmissão->1982785510-editar->1982785510-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-protocolo transmissão`, `1982785510-editar`, `1982785510-cancelar`];
    cy.visit('http://system-A10/solicitacoes-resultados/361369/obrigacao-gerada/424914/protocolos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1982785510-editar"]`);
    cy.clickIfExist(`[data-cy="1982785510-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->969732086-abrir visualização->969732086-expandir->969732086-diminuir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `969732086-abrir visualização`, `969732086-expandir`, `969732086-diminuir`];
    cy.visit('http://system-A10/obrigacoes-executadas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-expandir"]`);
    cy.clickIfExist(`[data-cy="969732086-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-executar->354122518-múltipla seleção`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-executar`, `354122518-múltipla seleção`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-executar"]`);
    cy.clickIfExist(`[data-cy="354122518-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-executar->354122518-agendar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-executar`, `354122518-agendar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-executar"]`);
    cy.clickIfExist(`[data-cy="354122518-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-agendamentos->354122518-voltar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-agendamentos`, `354122518-voltar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210587D%7C%7C210587&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-visualização->354122518-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-visualização`, `354122518-item-`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="354122518-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-executar->2241957464-múltipla seleção`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-executar`, `2241957464-múltipla seleção`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-executar"]`);
    cy.clickIfExist(`[data-cy="2241957464-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-executar->2241957464-agendar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-executar`, `2241957464-agendar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-executar"]`);
    cy.clickIfExist(`[data-cy="2241957464-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-agendamentos->2241957464-voltar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-agendamentos`, `2241957464-voltar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47661888D%7C%7C47661888&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-visualização->2241957464-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-visualização`, `2241957464-item-`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2241957464-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-executar->2697675163-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-executar`, `2697675163-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-executar"]`);
    cy.clickIfExist(`[data-cy="2697675163-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-executar->2697675163-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-executar`, `2697675163-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-executar"]`);
    cy.clickIfExist(`[data-cy="2697675163-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-agendamentos->2697675163-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-agendamentos`, `2697675163-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54895821D%7C%7C54895821&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-visualização->2697675163-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-visualização`, `2697675163-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2697675163-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-detalhes->2697675163-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-detalhes`, `2697675163-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-detalhes"]`);
    cy.clickIfExist(`[data-cy="2697675163-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-abrir visualização->2697675163-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-abrir visualização`, `2697675163-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2697675163-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-abrir visualização->2697675163-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-abrir visualização`, `2697675163-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2697675163-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-abrir visualização->2697675163-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-abrir visualização`, `2697675163-expandir`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2697675163-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-abrir visualização->2697675163-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-abrir visualização`, `2697675163-download`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2697675163-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-executar->3866849537-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-executar`, `3866849537-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-executar"]`);
    cy.clickIfExist(`[data-cy="3866849537-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-executar->3866849537-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-executar`, `3866849537-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-executar"]`);
    cy.clickIfExist(`[data-cy="3866849537-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-agendamentos->3866849537-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-agendamentos`, `3866849537-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~55689114D%7C%7C55689114&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-visualização->3866849537-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-visualização`, `3866849537-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3866849537-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-detalhes->3866849537-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-detalhes`, `3866849537-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-detalhes"]`);
    cy.clickIfExist(`[data-cy="3866849537-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-abrir visualização->3866849537-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-abrir visualização`, `3866849537-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3866849537-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-abrir visualização->3866849537-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-abrir visualização`, `3866849537-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3866849537-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-abrir visualização->3866849537-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-abrir visualização`, `3866849537-expandir`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3866849537-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-abrir visualização->3866849537-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-abrir visualização`, `3866849537-download`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3866849537-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-executar->3969562164-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-executar`, `3969562164-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-executar"]`);
    cy.clickIfExist(`[data-cy="3969562164-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-executar->3969562164-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-executar`, `3969562164-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-executar"]`);
    cy.clickIfExist(`[data-cy="3969562164-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-agendamentos->3969562164-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-agendamentos`, `3969562164-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210506D%7C%7C210506&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-visualização->3969562164-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-visualização`, `3969562164-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3969562164-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-executar->390233146-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-executar`, `390233146-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-executar"]`);
    cy.clickIfExist(`[data-cy="390233146-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-executar->390233146-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-executar`, `390233146-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-executar"]`);
    cy.clickIfExist(`[data-cy="390233146-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-agendamentos->390233146-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-agendamentos`, `390233146-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27644553D%7C%7C27644553&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-visualização->390233146-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-visualização`, `390233146-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="390233146-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-detalhes->390233146-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-detalhes`, `390233146-não há dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-detalhes"]`);
    cy.clickIfExist(`[data-cy="390233146-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-executar->2613819546-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-executar`, `2613819546-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-executar"]`);
    cy.clickIfExist(`[data-cy="2613819546-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-executar->2613819546-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-executar`, `2613819546-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-executar"]`);
    cy.clickIfExist(`[data-cy="2613819546-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-agendamentos->2613819546-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-agendamentos`, `2613819546-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47921082D%7C%7C47921082&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-visualização->2613819546-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-visualização`, `2613819546-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2613819546-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lancamentos-ajustes->1093322407-visualização->1093322407-salvar configuração`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lancamentos-ajustes`, `1093322407-visualização`, `1093322407-salvar configuração`];
    cy.visit('http://system-A10/relatorios/apuracao/lancamentos-ajustes');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1093322407-visualização"]`);
    cy.clickIfExist(`[data-cy="1093322407-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-executar->3648812560-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-executar`, `3648812560-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-executar"]`);
    cy.clickIfExist(`[data-cy="3648812560-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-executar->3648812560-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-executar`, `3648812560-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-executar"]`);
    cy.clickIfExist(`[data-cy="3648812560-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-agendamentos->3648812560-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-agendamentos`, `3648812560-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210505D%7C%7C210505&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-visualização->3648812560-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-visualização`, `3648812560-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3648812560-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-detalhes->3648812560-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-detalhes`, `3648812560-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-detalhes"]`);
    cy.clickIfExist(`[data-cy="3648812560-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-abrir visualização->3648812560-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-abrir visualização`, `3648812560-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3648812560-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-abrir visualização->3648812560-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-abrir visualização`, `3648812560-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3648812560-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-abrir visualização->3648812560-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-abrir visualização`, `3648812560-expandir`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3648812560-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-abrir visualização->3648812560-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-abrir visualização`, `3648812560-download`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3648812560-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/operacao-com-cartao->667902761-visualização->667902761-salvar configuração`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/operacao-com-cartao`, `667902761-visualização`, `667902761-salvar configuração`];
    cy.visit('http://system-A10/relatorios/apuracao/operacao-com-cartao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="667902761-visualização"]`);
    cy.clickIfExist(`[data-cy="667902761-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-executar->2707770761-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-executar`, `2707770761-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-executar"]`);
    cy.clickIfExist(`[data-cy="2707770761-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-executar->2707770761-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-executar`, `2707770761-agendar`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-executar"]`);
    cy.clickIfExist(`[data-cy="2707770761-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-agendamentos->2707770761-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-agendamentos`, `2707770761-voltar`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210485D%7C%7C210485&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-visualização->2707770761-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-visualização`, `2707770761-item-`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2707770761-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-detalhes->2707770761-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-detalhes`, `2707770761-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-detalhes"]`);
    cy.clickIfExist(`[data-cy="2707770761-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-abrir visualização->2707770761-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-abrir visualização`, `2707770761-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2707770761-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-abrir visualização->2707770761-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-abrir visualização`, `2707770761-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2707770761-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-abrir visualização->2707770761-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-abrir visualização`, `2707770761-expandir`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2707770761-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-abrir visualização->2707770761-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-abrir visualização`, `2707770761-download`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2707770761-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/valores-agregados->897726810-visualização->897726810-salvar configuração`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/valores-agregados`, `897726810-visualização`, `897726810-salvar configuração`];
    cy.visit('http://system-A10/relatorios/apuracao/valores-agregados');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="897726810-visualização"]`);
    cy.clickIfExist(`[data-cy="897726810-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-executar->2394123409-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-executar`, `2394123409-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-executar"]`);
    cy.clickIfExist(`[data-cy="2394123409-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-executar->2394123409-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-executar`, `2394123409-agendar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-executar"]`);
    cy.clickIfExist(`[data-cy="2394123409-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-executar->2394123409-input-P_CFOP-2394123409-input-P_NOP and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-executar`, `2394123409-input-P_CFOP-2394123409-input-P_NOP`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-executar"]`);
    cy.fillInput(`[data-cy="2394123409-input-P_CFOP"] textarea`, `Barbados Dollar`);
    cy.fillInput(`[data-cy="2394123409-input-P_NOP"] textarea`, `Vaticano`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-agendamentos->2394123409-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-agendamentos`, `2394123409-voltar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31802311D%7C%7C31802311&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-visualização->2394123409-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-visualização`, `2394123409-item-`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2394123409-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-detalhes->2394123409-não há dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-detalhes`, `2394123409-não há dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-detalhes"]`);
    cy.clickIfExist(`[data-cy="2394123409-não há dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-item->4204759012-visualização->4204759012-salvar configuração`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-item`, `4204759012-visualização`, `4204759012-salvar configuração`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-item');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204759012-visualização"]`);
    cy.clickIfExist(`[data-cy="4204759012-salvar configuração"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-executar->4204835061-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-executar`, `4204835061-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-executar"]`);
    cy.clickIfExist(`[data-cy="4204835061-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-executar->4204835061-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-executar`, `4204835061-agendar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-executar"]`);
    cy.clickIfExist(`[data-cy="4204835061-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-agendamentos->4204835061-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-agendamentos`, `4204835061-voltar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210436D%7C%7C210436&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-visualização->4204835061-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-visualização`, `4204835061-item-`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4204835061-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-detalhes->4204835061-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-detalhes`, `4204835061-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-detalhes"]`);
    cy.clickIfExist(`[data-cy="4204835061-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-abrir visualização->4204835061-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-abrir visualização`, `4204835061-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4204835061-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-abrir visualização->4204835061-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-abrir visualização`, `4204835061-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4204835061-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-abrir visualização->4204835061-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-abrir visualização`, `4204835061-expandir`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4204835061-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-abrir visualização->4204835061-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-abrir visualização`, `4204835061-download`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4204835061-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-executar->2549852581-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-executar`, `2549852581-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-executar"]`);
    cy.clickIfExist(`[data-cy="2549852581-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-executar->2549852581-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-executar`, `2549852581-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-executar"]`);
    cy.clickIfExist(`[data-cy="2549852581-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-executar->2549852581-input-PAGINA_INICIAL and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-executar`, `2549852581-input-PAGINA_INICIAL`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-executar"]`);
    cy.fillInput(`[data-cy="2549852581-input-PAGINA_INICIAL"] textarea`, `systemworthy`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-agendamentos->2549852581-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-agendamentos`, `2549852581-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210491D%7C%7C210491&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-visualização->2549852581-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-visualização`, `2549852581-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2549852581-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-executar->3806525293-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-executar`, `3806525293-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-executar"]`);
    cy.clickIfExist(`[data-cy="3806525293-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-executar->3806525293-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-executar`, `3806525293-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-executar"]`);
    cy.clickIfExist(`[data-cy="3806525293-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-agendamentos->3806525293-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-agendamentos`, `3806525293-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27728737D%7C%7C27728737&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-visualização->3806525293-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-visualização`, `3806525293-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3806525293-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-executar->117133032-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-executar`, `117133032-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-executar"]`);
    cy.clickIfExist(`[data-cy="117133032-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-executar->117133032-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-executar`, `117133032-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-executar"]`);
    cy.clickIfExist(`[data-cy="117133032-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-agendamentos->117133032-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-agendamentos`, `117133032-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31853105D%7C%7C31853105&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-visualização->117133032-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-visualização`, `117133032-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="117133032-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-executar->877676973-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-executar`, `877676973-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-executar"]`);
    cy.clickIfExist(`[data-cy="877676973-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-executar->877676973-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-executar`, `877676973-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-executar"]`);
    cy.clickIfExist(`[data-cy="877676973-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-agendamentos->877676973-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-agendamentos`, `877676973-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53040144D%7C%7C53040144&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-visualização->877676973-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-visualização`, `877676973-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="877676973-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-detalhes->877676973-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-detalhes`, `877676973-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-detalhes"]`);
    cy.clickIfExist(`[data-cy="877676973-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-abrir visualização->877676973-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-abrir visualização`, `877676973-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="877676973-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-abrir visualização->877676973-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-abrir visualização`, `877676973-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="877676973-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-abrir visualização->877676973-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-abrir visualização`, `877676973-expandir`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="877676973-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-abrir visualização->877676973-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-abrir visualização`, `877676973-download`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="877676973-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-executar->1452122080-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-executar`, `1452122080-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-executar"]`);
    cy.clickIfExist(`[data-cy="1452122080-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-executar->1452122080-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-executar`, `1452122080-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-executar"]`);
    cy.clickIfExist(`[data-cy="1452122080-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-executar->1452122080-input-CODIGO_ITEM and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-executar`, `1452122080-input-CODIGO_ITEM`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-executar"]`);
    cy.fillInput(`[data-cy="1452122080-input-CODIGO_ITEM"] textarea`, `invoice`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-agendamentos->1452122080-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-agendamentos`, `1452122080-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54895824D%7C%7C54895824&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-visualização->1452122080-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-lancamentos-inventarios`, `1452122080-visualização`, `1452122080-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1452122080-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1452122080-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-executar->3374403702-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-executar`, `3374403702-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-executar"]`);
    cy.clickIfExist(`[data-cy="3374403702-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-executar->3374403702-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-executar`, `3374403702-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-executar"]`);
    cy.clickIfExist(`[data-cy="3374403702-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-agendamentos->3374403702-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-agendamentos`, `3374403702-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54895798D%7C%7C54895798&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-visualização->3374403702-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/informacoes-bloco-k`, `3374403702-visualização`, `3374403702-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3374403702-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3374403702-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-executar->1298845030-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-executar`, `1298845030-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-executar"]`);
    cy.clickIfExist(`[data-cy="1298845030-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-executar->1298845030-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-executar`, `1298845030-agendar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-executar"]`);
    cy.clickIfExist(`[data-cy="1298845030-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-executar->1298845030-input-PAGINA_INICIAL and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-executar`, `1298845030-input-PAGINA_INICIAL`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-executar"]`);
    cy.fillInput(`[data-cy="1298845030-input-PAGINA_INICIAL"] textarea`, `Incredible`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-agendamentos->1298845030-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-agendamentos`, `1298845030-voltar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210490D%7C%7C210490&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-visualização->1298845030-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-visualização`, `1298845030-item-`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1298845030-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-detalhes->1298845030-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-detalhes`, `1298845030-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-detalhes"]`);
    cy.clickIfExist(`[data-cy="1298845030-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-abrir visualização->1298845030-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-abrir visualização`, `1298845030-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1298845030-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-abrir visualização->1298845030-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-abrir visualização`, `1298845030-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1298845030-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-abrir visualização->1298845030-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-abrir visualização`, `1298845030-expandir`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1298845030-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-abrir visualização->1298845030-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-abrir visualização`, `1298845030-download`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1298845030-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-executar->3761231093-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-executar`, `3761231093-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-executar"]`);
    cy.clickIfExist(`[data-cy="3761231093-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-executar->3761231093-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-executar`, `3761231093-agendar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-executar"]`);
    cy.clickIfExist(`[data-cy="3761231093-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-agendamentos->3761231093-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-agendamentos`, `3761231093-voltar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210510D%7C%7C210510&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-visualização->3761231093-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-visualização`, `3761231093-item-`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3761231093-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-detalhes->3761231093-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-detalhes`, `3761231093-dados disponíveis para impressão`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-detalhes"]`);
    cy.clickIfExist(`[data-cy="3761231093-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-abrir visualização->3761231093-aumentar o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-abrir visualização`, `3761231093-aumentar o zoom`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3761231093-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-abrir visualização->3761231093-diminuir o zoom`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-abrir visualização`, `3761231093-diminuir o zoom`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3761231093-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-abrir visualização->3761231093-expandir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-abrir visualização`, `3761231093-expandir`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3761231093-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-abrir visualização->3761231093-download`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-abrir visualização`, `3761231093-download`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3761231093-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-executar->3723571518-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-executar`, `3723571518-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-executar"]`);
    cy.clickIfExist(`[data-cy="3723571518-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-executar->3723571518-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-executar`, `3723571518-agendar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-executar"]`);
    cy.clickIfExist(`[data-cy="3723571518-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-agendamentos->3723571518-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-agendamentos`, `3723571518-voltar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210512D%7C%7C210512&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-visualização->3723571518-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-visualização`, `3723571518-item-`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3723571518-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-executar->3722346050-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-executar`, `3722346050-múltipla seleção`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-executar"]`);
    cy.clickIfExist(`[data-cy="3722346050-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-executar->3722346050-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-executar`, `3722346050-agendar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-executar"]`);
    cy.clickIfExist(`[data-cy="3722346050-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-agendamentos->3722346050-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-agendamentos`, `3722346050-voltar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~57898507D%7C%7C57898507&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-visualização->3722346050-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-visualização`, `3722346050-item-`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3722346050-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo->3988544486-pesquisar dof->3988544486-power-search-button`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`, `3988544486-pesquisar dof`, `3988544486-power-search-button`];
    cy.visit('http://system-A10/desfazimento-negocios/novo?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3988544486-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/desfaziamento-negocios->2568354531-novo->3988544486-pesquisar dof->3988544486-cancelar`, () => {
    const actualId = [`root`, `tabelas-corporativas`, `tabelas-corporativas/transacoes`, `tabelas-corporativas/transacoes/desfaziamento-negocios`, `2568354531-novo`, `3988544486-pesquisar dof`, `3988544486-cancelar`];
    cy.visit('http://system-A10/desfazimento-negocios/novo?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3988544486-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-novo->1457766198-input-rgajCodigo-1457766198-powerselect-indBaseAju-1457766198-checkbox-indComplPreco-1457766198-checkbox-indComplImposto-1457766198-checkbox-indComplPrecoImposto-1457766198-radio-imposto-1457766198-textarea-obsRegra->1457766198-radio-indEntradaSaida and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-novo`, `1457766198-input-rgajCodigo-1457766198-powerselect-indBaseAju-1457766198-checkbox-indComplPreco-1457766198-checkbox-indComplImposto-1457766198-checkbox-indComplPrecoImposto-1457766198-radio-imposto-1457766198-textarea-obsRegra`, `1457766198-radio-indEntradaSaida`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-novo"]`);
    cy.fillInput(`[data-cy="1457766198-input-rgajCodigo"] textarea`, `SMTP`);
    cy.fillInputPowerSelect(`[data-cy="1457766198-powerselect-indBaseAju"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-checkbox-indComplPreco"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-checkbox-indComplImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-checkbox-indComplPrecoImposto"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-radio-imposto"] input`);
    cy.fillInput(`[data-cy="1457766198-textarea-obsRegra"] input`, `extensible`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputCheckboxOrRadio(`[data-cy="1457766198-radio-indEntradaSaida"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-exportar regras->3021063571-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-exportar regras`, `3021063571-cancelar`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-exportar regras"]`);
    cy.clickIfExist(`[data-cy="3021063571-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-exportar regras->3021063571-checkbox-valoresTabelaBd and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-exportar regras`, `3021063571-checkbox-valoresTabelaBd`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-exportar regras"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3021063571-checkbox-valoresTabelaBd"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-importar regras->3021063571-upload arquivo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-importar regras`, `3021063571-upload arquivo`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-importar regras"]`);
    cy.clickIfExist(`[data-cy="3021063571-upload arquivo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-importar regras->3021063571-importar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-importar regras`, `3021063571-importar`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-importar regras"]`);
    cy.clickIfExist(`[data-cy="3021063571-importar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-mais operações->3021063571-item-importar regras->3021063571-checkbox-sobreporRegistrosExistentes and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-mais operações`, `3021063571-item-importar regras`, `3021063571-checkbox-sobreporRegistrosExistentes`];
    cy.visit('http://system-A10/regra-lancamento-impostos');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3021063571-mais operações"]`);
    cy.clickIfExist(`[data-cy="3021063571-item-importar regras"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3021063571-checkbox-sobreporRegistrosExistentes"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-guias sem código de ajuste->3390840739-novo->841832821-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-guias sem código de ajuste`, `3390840739-novo`, `841832821-salvar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_DIFA/guia/E116/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="841832821-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-guias sem código de ajuste->3390840739-novo->841832821-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-guias sem código de ajuste`, `3390840739-novo`, `841832821-voltar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_DIFA/guia/E116/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="841832821-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-guias sem código de ajuste->3390840739-novo->841832821-powerselect-ufCodigo-841832821-powerselect-vlAjuste-841832821-powerselect-observacao-841832821-powerselect-codReceita-841832821-powerselect-codObrigacao-841832821-powerselect-cpgCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-guias sem código de ajuste`, `3390840739-novo`, `841832821-powerselect-ufCodigo-841832821-powerselect-vlAjuste-841832821-powerselect-observacao-841832821-powerselect-codReceita-841832821-powerselect-codObrigacao-841832821-powerselect-cpgCodigo`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_DIFA/guia/E116');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3390840739-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="841832821-powerselect-ufCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="841832821-powerselect-vlAjuste"] input`);
    cy.fillInputPowerSelect(`[data-cy="841832821-powerselect-observacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="841832821-powerselect-codReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="841832821-powerselect-codObrigacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="841832821-powerselect-cpgCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra->3352686717-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`, `3352686717-power-search-button`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra->3352686717-orderedlistoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`, `3352686717-orderedlistoutlined`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-orderedlistoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra->3352686717-fechar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`, `3352686717-fechar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra->3352686717-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`, `3352686717-power-search-input`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.fillInputPowerSearch(`[data-cy="3352686717-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-testar regra->3352686717-ok`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-testar regra`, `3352686717-ok`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-testar regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-ok"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-testar regra->3352686717-radio-testarRegraCheck and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-testar regra`, `3352686717-radio-testarRegraCheck`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-testar regra"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3352686717-radio-testarRegraCheck"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-copiar regra->3352686717-input-copiaFrom-3352686717-input-copiaTo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-copiar regra`, `3352686717-input-copiaFrom-3352686717-input-copiaTo`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-copiar regra"]`);
    cy.fillInput(`[data-cy="3352686717-input-copiaFrom"] textarea`, `Bike`);
    cy.fillInput(`[data-cy="3352686717-input-copiaTo"] textarea`, `Chicken`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-novo->2107180829-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-novo`, `2107180829-button`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2107180829-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-novo->2107180829-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-novo`, `2107180829-salvar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2107180829-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-novo->2107180829-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-novo`, `2107180829-voltar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2107180829-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-novo->2107180829-powerselect-ufCodigo-2107180829-powerselect-descrAjuste-2107180829-powerselect-codItem-2107180829-powerselect-vlBcIcms-2107180829-powerselect-aliqIcms-2107180829-powerselect-vlIcms-2107180829-powerselect-vlOutros-2107180829-powerselect-observacao-2107180829-powerselect-textoCompl-2107180829-powerselect-codReceita-2107180829-powerselect-cpgCodigo-2107180829-powerselect-codObrigacao and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-novo`, `2107180829-powerselect-ufCodigo-2107180829-powerselect-descrAjuste-2107180829-powerselect-codItem-2107180829-powerselect-vlBcIcms-2107180829-powerselect-aliqIcms-2107180829-powerselect-vlIcms-2107180829-powerselect-vlOutros-2107180829-powerselect-observacao-2107180829-powerselect-textoCompl-2107180829-powerselect-codReceita-2107180829-powerselect-cpgCodigo-2107180829-powerselect-codObrigacao`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4266322252-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-ufCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-descrAjuste"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-codItem"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-vlBcIcms"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-aliqIcms"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-vlIcms"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-vlOutros"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-observacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-textoCompl"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-codReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-cpgCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2107180829-powerselect-codObrigacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-visualizar/editar->1394695403-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-visualizar/editar`, `1394695403-button`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1394695403-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-visualizar/editar->1394695403-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-visualizar/editar`, `1394695403-remover item`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1394695403-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-visualizar/editar->1394695403-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-visualizar/editar`, `1394695403-salvar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1394695403-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-visualizar/editar->1394695403-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-visualizar/editar`, `1394695403-voltar`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF/editar/1');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1394695403-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-itens de ajuste->4266322252-visualizar/editar->1394695403-powerselect-ufCodigo-1394695403-powerselect-codAjuste-1394695403-powerselect-descrAjuste-1394695403-powerselect-codItem-1394695403-powerselect-vlBcIcms-1394695403-powerselect-aliqIcms-1394695403-powerselect-vlIcms-1394695403-powerselect-vlOutros-1394695403-powerselect-observacao-1394695403-powerselect-textoCompl-1394695403-powerselect-codReceita-1394695403-powerselect-cpgCodigo-1394695403-powerselect-codObrigacao and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-itens de ajuste`, `4266322252-visualizar/editar`, `1394695403-powerselect-ufCodigo-1394695403-powerselect-codAjuste-1394695403-powerselect-descrAjuste-1394695403-powerselect-codItem-1394695403-powerselect-vlBcIcms-1394695403-powerselect-aliqIcms-1394695403-powerselect-vlIcms-1394695403-powerselect-vlOutros-1394695403-powerselect-observacao-1394695403-powerselect-textoCompl-1394695403-powerselect-codReceita-1394695403-powerselect-cpgCodigo-1394695403-powerselect-codObrigacao`];
    cy.visit('http://system-A10/regra-lancamento-impostos/RVF_OC/ajustes/DOF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4266322252-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-ufCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-codAjuste"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-descrAjuste"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-codItem"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-vlBcIcms"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-aliqIcms"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-vlIcms"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-vlOutros"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-observacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-textoCompl"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-codReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-cpgCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1394695403-powerselect-codObrigacao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-exportar regras->2903792002-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-exportar regras`, `2903792002-cancelar`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-exportar regras"]`);
    cy.clickIfExist(`[data-cy="2903792002-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-exportar regras->2903792002-checkbox-valoresTabelaBd and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-exportar regras`, `2903792002-checkbox-valoresTabelaBd`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-exportar regras"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2903792002-checkbox-valoresTabelaBd"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-importar regras->2903792002-upload arquivo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-importar regras`, `2903792002-upload arquivo`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-importar regras"]`);
    cy.clickIfExist(`[data-cy="2903792002-upload arquivo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-importar regras->2903792002-importar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-importar regras`, `2903792002-importar`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-importar regras"]`);
    cy.clickIfExist(`[data-cy="2903792002-importar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-mais operações->2903792002-item-importar regras->2903792002-checkbox-sobreporRegistrosExistentes and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-mais operações`, `2903792002-item-importar regras`, `2903792002-checkbox-sobreporRegistrosExistentes`];
    cy.visit('http://system-A10/regras-recolhimento');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2903792002-mais operações"]`);
    cy.clickIfExist(`[data-cy="2903792002-item-importar regras"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2903792002-checkbox-sobreporRegistrosExistentes"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regra-recolhimento->2903792002-eyeoutlined->2858606894-mais operações->2858606894-item-`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regra-recolhimento`, `2903792002-eyeoutlined`, `2858606894-mais operações`, `2858606894-item-`];
    cy.visit('http://system-A10/regras-recolhimento/editar/RVF_IPI');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2858606894-mais operações"]`);
    cy.clickIfExist(`[data-cy="2858606894-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-estabelecimentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-estabelecimentos`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/1743');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-estabelecimentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-remover item`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-remover item`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/1743');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-salvar`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/1743');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-voltar`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/1743');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-powerselect-cfopCodigo-3989490202-powerselect-nopCodigo-3989490202-powerselect-edofCodigo-3989490202-powerselect-finCodigo and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-powerselect-cfopCodigo-3989490202-powerselect-nopCodigo-3989490202-powerselect-edofCodigo-3989490202-powerselect-finCodigo`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1701794451-salvar"]`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-cfopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-edofCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3989490202-powerselect-finCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-estabelecimentos->3989490202-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-estabelecimentos`, `3989490202-power-search-button`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/682');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-estabelecimentos"]`);
    cy.clickIfExist(`[data-cy="3989490202-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-eyeoutlined->3989490202-estabelecimentos->3989490202-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-eyeoutlined`, `3989490202-estabelecimentos`, `3989490202-power-search-input`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/682');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-estabelecimentos"]`);
    cy.fillInputPowerSearch(`[data-cy="3989490202-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo->2045306075-pesquisar->2045306075-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`, `2045306075-pesquisar`, `2045306075-power-search-button`];
    cy.visit('http://system-A10/lancamento-ajuste/C197/novo?dtFatoGeradorImposto=~mth~1712497808936D%7C%7C04%2F2024&informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2045306075-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste->3361402062-novo->2045306075-pesquisar->2045306075-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/parametrizacao-regras/lancamento-ajuste`, `3361402062-novo`, `2045306075-pesquisar`, `2045306075-cancelar`];
    cy.visit('http://system-A10/lancamento-ajuste/C197/novo?dtFatoGeradorImposto=~mth~1712497808936D%7C%7C04%2F2024&informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2045306075-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-mais operações->50637604-item-gerar automatização e115->50637604-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-mais operações`, `50637604-item-gerar automatização e115`, `50637604-executar`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-mais operações"]`);
    cy.clickIfExist(`[data-cy="50637604-item-gerar automatização e115"]`);
    cy.clickIfExist(`[data-cy="50637604-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-mais operações->50637604-item-gerar automatização e115->50637604-powerselect-estabelecimento and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-mais operações`, `50637604-item-gerar automatização e115`, `50637604-powerselect-estabelecimento`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-mais operações"]`);
    cy.clickIfExist(`[data-cy="50637604-item-gerar automatização e115"]`);
    cy.fillInputPowerSelect(`[data-cy="50637604-powerselect-estabelecimento"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115->50637604-mais operações->50637604-item-excluir registros e115->50637604-input-codInfAdic and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-adicionais-e115`, `50637604-mais operações`, `50637604-item-excluir registros e115`, `50637604-input-codInfAdic`];
    cy.visit('http://system-A10/informacoes-adicionais-e115?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="50637604-mais operações"]`);
    cy.clickIfExist(`[data-cy="50637604-item-excluir registros e115"]`);
    cy.fillInput(`[data-cy="50637604-input-codInfAdic"] textarea`, `payment`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações adicionais->518106878-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações adicionais`, `518106878-cancelar`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações adicionais"]`);
    cy.clickIfExist(`[data-cy="518106878-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações adicionais->518106878-limpar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações adicionais`, `518106878-limpar`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações adicionais"]`);
    cy.clickIfExist(`[data-cy="518106878-limpar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações adicionais->518106878-ok`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações adicionais`, `518106878-ok`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações adicionais"]`);
    cy.clickIfExist(`[data-cy="518106878-ok"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações adicionais->518106878-input-nrReferencia-518106878-input-numDa-518106878-input-docOrigem-518106878-input-parcela-518106878-input-orgaoArrecadador-518106878-input-number-classeVencimento-518106878-input-codBarras-518106878-powerselect-bancoNum-518106878-input-numConvenio-518106878-input-codAutBanco-518106878-input-numeroGnr and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações adicionais`, `518106878-input-nrReferencia-518106878-input-numDa-518106878-input-docOrigem-518106878-input-parcela-518106878-input-orgaoArrecadador-518106878-input-number-classeVencimento-518106878-input-codBarras-518106878-powerselect-bancoNum-518106878-input-numConvenio-518106878-input-codAutBanco-518106878-input-numeroGnr`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações adicionais"]`);
    cy.fillInput(`[data-cy="518106878-input-nrReferencia"] textarea`, `Sports`);
    cy.fillInput(`[data-cy="518106878-input-numDa"] textarea`, `Comunicaes`);
    cy.fillInput(`[data-cy="518106878-input-docOrigem"] textarea`, `Tasty`);
    cy.fillInput(`[data-cy="518106878-input-parcela"] textarea`, `wireless`);
    cy.fillInput(`[data-cy="518106878-input-orgaoArrecadador"] textarea`, `hybrid`);
    cy.fillInput(`[data-cy="518106878-input-number-classeVencimento"] textarea`, `5`);
    cy.fillInput(`[data-cy="518106878-input-codBarras"] textarea`, `grey`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-bancoNum"] input`);
    cy.fillInput(`[data-cy="518106878-input-numConvenio"] textarea`, `navigating`);
    cy.fillInput(`[data-cy="518106878-input-codAutBanco"] textarea`, `Metal`);
    cy.fillInput(`[data-cy="518106878-input-numeroGnr"] textarea`, `Programa`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-informações para a gnre->518106878-powerselect-periodoRefGnre-518106878-powerselect-codProdutoGnre-518106878-powerselect-codTipoDocGnre-518106878-input-numeroDof-518106878-powerselect-codCampoExtra1-518106878-input-textoCampoExtra1-518106878-powerselect-codCampoExtra2-518106878-input-textoCampoExtra2-518106878-powerselect-codCampoExtra3-518106878-input-textoCampoExtra3-518106878-powerselect-statusGnre-518106878-checkbox-enviaGnre-518106878-textarea-histProcessamento-518106878-textarea-urlGnre and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-informações para a gnre`, `518106878-powerselect-periodoRefGnre-518106878-powerselect-codProdutoGnre-518106878-powerselect-codTipoDocGnre-518106878-input-numeroDof-518106878-powerselect-codCampoExtra1-518106878-input-textoCampoExtra1-518106878-powerselect-codCampoExtra2-518106878-input-textoCampoExtra2-518106878-powerselect-codCampoExtra3-518106878-input-textoCampoExtra3-518106878-powerselect-statusGnre-518106878-checkbox-enviaGnre-518106878-textarea-histProcessamento-518106878-textarea-urlGnre`];
    cy.visit('http://system-A10/guia-recolhimento-imp/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="518106878-informações para a gnre"]`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-periodoRefGnre"] input`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-codProdutoGnre"] input`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-codTipoDocGnre"] input`);
    cy.fillInput(`[data-cy="518106878-input-numeroDof"] textarea`, `success`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-codCampoExtra1"] input`);
    cy.fillInput(`[data-cy="518106878-input-textoCampoExtra1"] textarea`, `program`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-codCampoExtra2"] input`);
    cy.fillInput(`[data-cy="518106878-input-textoCampoExtra2"] textarea`, `alarm`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-codCampoExtra3"] input`);
    cy.fillInput(`[data-cy="518106878-input-textoCampoExtra3"] textarea`, `exploit`);
    cy.fillInputPowerSelect(`[data-cy="518106878-powerselect-statusGnre"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="518106878-checkbox-enviaGnre"] textarea`);
    cy.fillInput(`[data-cy="518106878-textarea-histProcessamento"] input`, `TCP`);
    cy.fillInput(`[data-cy="518106878-textarea-urlGnre"] input`, `Movies`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-voltar->2583632075-novo`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-voltar`, `2583632075-novo`];
    cy.visit('http://system-A10/guia-recolhimento-imp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtFatoGeradorImposto=~mth~1712497926486D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2583632075-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid->2583632075-novo->518106878-voltar->2583632075-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/geracao-manutencao-grid`, `2583632075-novo`, `518106878-voltar`, `2583632075-power-search-button`];
    cy.visit('http://system-A10/guia-recolhimento-imp?estCodigo=~eq~AAA_DF%7C%7CAAA_DF&dtFatoGeradorImposto=~mth~1712497926486D%7C%7C04%2F2024');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2583632075-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-selecionar mercadoria->74501782-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-selecionar mercadoria`, `74501782-power-search-button`];
    cy.visit('http://system-A10/lancamento-producao-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74501782-selecionar mercadoria"]`);
    cy.clickIfExist(`[data-cy="74501782-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-selecionar mercadoria->74501782-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-selecionar mercadoria`, `74501782-cancelar`];
    cy.visit('http://system-A10/lancamento-producao-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74501782-selecionar mercadoria"]`);
    cy.clickIfExist(`[data-cy="74501782-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-novo->74501782-selecionar mercadoria->74501782-checkbox-finalidadeEntradaIndIpi-74501782-checkbox-finalidadeSaidaIndIpi and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-novo`, `74501782-selecionar mercadoria`, `74501782-checkbox-finalidadeEntradaIndIpi-74501782-checkbox-finalidadeSaidaIndIpi`];
    cy.visit('http://system-A10/lancamento-producao-estoque/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="74501782-selecionar mercadoria"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="74501782-checkbox-finalidadeEntradaIndIpi"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="74501782-checkbox-finalidadeSaidaIndIpi"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque->766548531-mais operações->766548531-item-gerar lcpe->766548531-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/lancamento-producao-estoque`, `766548531-mais operações`, `766548531-item-gerar lcpe`, `766548531-cancelar`];
    cy.visit('http://system-A10/lancamento-producao-estoque?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="766548531-mais operações"]`);
    cy.clickIfExist(`[data-cy="766548531-item-gerar lcpe"]`);
    cy.clickIfExist(`[data-cy="766548531-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-pesquisar item nf->1490841773-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-pesquisar item nf`, `1490841773-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st/novo?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1490841773-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-pesquisar item nf->1490841773-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-pesquisar item nf`, `1490841773-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st/novo?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1490841773-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/recup-st->4275029436-novo->1490841773-pesquisar item nf->1490841773-limpar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/recup-st`, `4275029436-novo`, `1490841773-pesquisar item nf`, `1490841773-limpar`];
    cy.visit('http://system-A10/escrituracao-apuracao/lancamento-apuracao/recup-st/novo?');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1490841773-limpar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc->escrituracao-apuracao/lancamento-apuracao/dime-sc->1322398884-novo->4212331205-salvar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, `escrituracao-apuracao/lancamento-apuracao/dime-sc`, `1322398884-novo`, `4212331205-salvar`];
    cy.visit('http://system-A10/escrituracao-apuracao/demonstrativo-apuracao-sc/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4212331205-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc->escrituracao-apuracao/lancamento-apuracao/dime-sc->1322398884-novo->4212331205-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, `escrituracao-apuracao/lancamento-apuracao/dime-sc`, `1322398884-novo`, `4212331205-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/demonstrativo-apuracao-sc/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4212331205-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc->escrituracao-apuracao/lancamento-apuracao/dime-sc->1322398884-novo->4212331205-powerselect-informanteEstCodigo-4212331205-input-codBeneficioTtd-4212331205-input-numeroConcessaoTtd-4212331205-input-monetary-vlBaseConcessao-4212331205-input-monetary-vlIcmsExonerado-4212331205-input-subtipoDcip-4212331205-powerselect-codCalculoFumdes-4212331205-input-monetary-vlFumdes-4212331205-powerselect-codCalculoFundoSocial-4212331205-input-monetary-vlFundoSocial-4212331205-input-monetary-vlBaseIcmsDevol-4212331205-input-monetary-vlIcmsDevol-4212331205-input-monetary-vlFumdesDevolucao-4212331205-input-monetary-vlFundoSocialDevolucao-4212331205-input-monetary-vlCredorFumdesAnt-4212331205-input-monetary-vlCredorFumdoSocialAnt and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/lancamento-apuracao`, `escrituracao-apuracao/lancamento-apuracao/informacoes-dime-sc`, `escrituracao-apuracao/lancamento-apuracao/dime-sc`, `1322398884-novo`, `4212331205-powerselect-informanteEstCodigo-4212331205-input-codBeneficioTtd-4212331205-input-numeroConcessaoTtd-4212331205-input-monetary-vlBaseConcessao-4212331205-input-monetary-vlIcmsExonerado-4212331205-input-subtipoDcip-4212331205-powerselect-codCalculoFumdes-4212331205-input-monetary-vlFumdes-4212331205-powerselect-codCalculoFundoSocial-4212331205-input-monetary-vlFundoSocial-4212331205-input-monetary-vlBaseIcmsDevol-4212331205-input-monetary-vlIcmsDevol-4212331205-input-monetary-vlFumdesDevolucao-4212331205-input-monetary-vlFundoSocialDevolucao-4212331205-input-monetary-vlCredorFumdesAnt-4212331205-input-monetary-vlCredorFumdoSocialAnt`];
    cy.visit('http://system-A10/escrituracao-apuracao/demonstrativo-apuracao-sc?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1322398884-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="4212331205-powerselect-informanteEstCodigo"] input`);
    cy.fillInput(`[data-cy="4212331205-input-codBeneficioTtd"] textarea`, `Buckinghamshire`);
    cy.fillInput(`[data-cy="4212331205-input-numeroConcessaoTtd"] textarea`, `Ergonomic`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlBaseConcessao"] textarea`, `3,97`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlIcmsExonerado"] textarea`, `7,98`);
    cy.fillInput(`[data-cy="4212331205-input-subtipoDcip"] textarea`, `Comoro Franc`);
    cy.fillInputPowerSelect(`[data-cy="4212331205-powerselect-codCalculoFumdes"] input`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlFumdes"] textarea`, `3,94`);
    cy.fillInputPowerSelect(`[data-cy="4212331205-powerselect-codCalculoFundoSocial"] input`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlFundoSocial"] textarea`, `1,57`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlBaseIcmsDevol"] textarea`, `7,92`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlIcmsDevol"] textarea`, `6,15`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlFumdesDevolucao"] textarea`, `3,54`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlFundoSocialDevolucao"] textarea`, `4,93`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlCredorFumdesAnt"] textarea`, `3,81`);
    cy.fillInput(`[data-cy="4212331205-input-monetary-vlCredorFumdoSocialAnt"] textarea`, `1,21`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-executar->602971242-múltipla seleção->602971242-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-executar`, `602971242-múltipla seleção`, `602971242-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-executar"]`);
    cy.clickIfExist(`[data-cy="602971242-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="602971242-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-abrir visualização->602971242-expandir->602971242-diminuir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraLancFiscal`, `602971242-abrir visualização`, `602971242-expandir`, `602971242-diminuir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="602971242-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="602971242-expandir"]`);
    cy.clickIfExist(`[data-cy="602971242-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-executar->2717158496-múltipla seleção->2717158496-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-executar`, `2717158496-múltipla seleção`, `2717158496-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-executar"]`);
    cy.clickIfExist(`[data-cy="2717158496-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2717158496-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-abrir visualização->2717158496-expandir->2717158496-diminuir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaLancFiscalPeriod`, `2717158496-abrir visualização`, `2717158496-expandir`, `2717158496-diminuir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2717158496-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2717158496-expandir"]`);
    cy.clickIfExist(`[data-cy="2717158496-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-executar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-executar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-agendamentos`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-agendamentos`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-power-search-button`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-regerar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-regerar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-detalhes`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-detalhes`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-abrir visualização`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-abrir visualização`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-excluir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-excluir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-executar->283960367-múltipla seleção->283960367-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-executar`, `283960367-múltipla seleção`, `283960367-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-executar"]`);
    cy.clickIfExist(`[data-cy="283960367-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="283960367-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-abrir visualização->283960367-expandir->283960367-diminuir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApuracaoIcmsIpi`, `283960367-abrir visualização`, `283960367-expandir`, `283960367-diminuir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="283960367-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="283960367-expandir"]`);
    cy.clickIfExist(`[data-cy="283960367-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-executar->2815987121-múltipla seleção->2815987121-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/calcDiferencialAliq`, `2815987121-executar`, `2815987121-múltipla seleção`, `2815987121-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2815987121-executar"]`);
    cy.clickIfExist(`[data-cy="2815987121-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2815987121-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-executar->4056389192-múltipla seleção->4056389192-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/geraValoresDOFs`, `4056389192-executar`, `4056389192-múltipla seleção`, `4056389192-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4056389192-executar"]`);
    cy.clickIfExist(`[data-cy="4056389192-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4056389192-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-executar->21201123-múltipla seleção->21201123-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apagaApurProcAntigo`, `21201123-executar`, `21201123-múltipla seleção`, `21201123-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="21201123-executar"]`);
    cy.clickIfExist(`[data-cy="21201123-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="21201123-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-executar->832827420-múltipla seleção->832827420-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal`, `832827420-executar`, `832827420-múltipla seleção`, `832827420-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="832827420-executar"]`);
    cy.clickIfExist(`[data-cy="832827420-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="832827420-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-executar->3840793780-múltipla seleção->3840793780-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento`, `3840793780-executar`, `3840793780-múltipla seleção`, `3840793780-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3840793780-executar"]`);
    cy.clickIfExist(`[data-cy="3840793780-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3840793780-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-executar->2515312772-múltipla seleção->2515312772-cancelar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/encerraReabrePeriodoFiscal`, `2515312772-executar`, `2515312772-múltipla seleção`, `2515312772-cancelar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2515312772-executar"]`);
    cy.clickIfExist(`[data-cy="2515312772-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2515312772-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-percentual por município->2807946815-novo->2900898314-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-percentual por município`, `2807946815-novo`, `2900898314-salvar`];
    cy.visit('http://system-A10/cadastro-ipm/581/242342/AG/percentual-participacao-municipio/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2900898314-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-percentual por município->2807946815-novo->2900898314-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-percentual por município`, `2807946815-novo`, `2900898314-voltar`];
    cy.visit('http://system-A10/cadastro-ipm/581/242342/AG/percentual-participacao-municipio/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2900898314-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-percentual por município->2807946815-novo->2900898314-powerselect-munCodigo-2900898314-input-number-percPartMun and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-percentual por município`, `2807946815-novo`, `2900898314-powerselect-munCodigo-2900898314-input-number-percPartMun`];
    cy.visit('http://system-A10/cadastro-ipm/581/242342/AG/percentual-participacao-municipio');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2807946815-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2900898314-powerselect-munCodigo"] input`);
    cy.fillInput(`[data-cy="2900898314-input-number-percPartMun"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-1->sped-fiscal/bloco-1/manutecao-cadastro-1400->2692418640-visualizar/editar->68489544-mais operações->68489544-item-`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-1`, `sped-fiscal/bloco-1/manutecao-cadastro-1400`, `2692418640-visualizar/editar`, `68489544-mais operações`, `68489544-item-`];
    cy.visit('http://system-A10/cadastro-ipm/editar/242342/AG');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="68489544-mais operações"]`);
    cy.clickIfExist(`[data-cy="68489544-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-novo->3594643635-mais operações->3594643635-item-`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-novo`, `3594643635-mais operações`, `3594643635-item-`];
    cy.visit('http://system-A10/fiscal/classificacao-mercadoria-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3594643635-mais operações"]`);
    cy.clickIfExist(`[data-cy="3594643635-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-novo`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-mais operações`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-mais operações`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-power-search-button`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-excluir`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-excluir`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo->2187377653-selecionar insumo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`, `2187377653-selecionar insumo`];
    cy.visit('http://system-A10/consumo-especifico-padrao/novo/AAA_DF/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2187377653-selecionar insumo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo->2187377653-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`, `2187377653-salvar`];
    cy.visit('http://system-A10/consumo-especifico-padrao/novo/AAA_DF/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2187377653-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo->2187377653-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`, `2187377653-voltar`];
    cy.visit('http://system-A10/consumo-especifico-padrao/novo/AAA_DF/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2187377653-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo->2187377653-powerselect-uniCodigo-2187377653-input-compCodigo-2187377653-powerselect-uniCompCodigo-2187377653-powerselect-codFase-2187377653-input-number-qtdCompMax-2187377653-input-number-perdaQuebra and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`, `2187377653-powerselect-uniCodigo-2187377653-input-compCodigo-2187377653-powerselect-uniCompCodigo-2187377653-powerselect-codFase-2187377653-input-number-qtdCompMax-2187377653-input-number-perdaQuebra`];
    cy.visit('http://system-A10/consumo-especifico-padrao/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="795118794-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2187377653-powerselect-uniCodigo"] input`);
    cy.fillInput(`[data-cy="2187377653-input-compCodigo"] textarea`, `vortals`);
    cy.fillInputPowerSelect(`[data-cy="2187377653-powerselect-uniCompCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2187377653-powerselect-codFase"] input`);
    cy.fillInput(`[data-cy="2187377653-input-number-qtdCompMax"] textarea`, `7`);
    cy.fillInput(`[data-cy="2187377653-input-number-perdaQuebra"] textarea`, `3`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-visualizar/editar->1546194375-remover item`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-visualizar/editar`, `1546194375-remover item`];
    cy.visit('http://system-A10/consumo-especifico-padrao/editar/SYNCHRO/12345/1101035311/2019-06-03');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1546194375-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-visualizar/editar->1546194375-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-visualizar/editar`, `1546194375-salvar`];
    cy.visit('http://system-A10/consumo-especifico-padrao/editar/SYNCHRO/12345/1101035311/2019-06-03');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1546194375-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-visualizar/editar->1546194375-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-visualizar/editar`, `1546194375-voltar`];
    cy.visit('http://system-A10/consumo-especifico-padrao/editar/SYNCHRO/12345/1101035311/2019-06-03');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1546194375-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-visualizar/editar->1546194375-powerselect-uniCodigo-1546194375-input-compCodigo-1546194375-powerselect-uniCompCodigo-1546194375-powerselect-codFase-1546194375-input-number-qtdCompMax-1546194375-input-number-perdaQuebra and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-visualizar/editar`, `1546194375-powerselect-uniCodigo-1546194375-input-compCodigo-1546194375-powerselect-uniCompCodigo-1546194375-powerselect-codFase-1546194375-input-number-qtdCompMax-1546194375-input-number-perdaQuebra`];
    cy.visit('http://system-A10/consumo-especifico-padrao/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="795118794-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1546194375-powerselect-uniCodigo"] input`);
    cy.fillInput(`[data-cy="1546194375-input-compCodigo"] textarea`, `seize`);
    cy.fillInputPowerSelect(`[data-cy="1546194375-powerselect-uniCompCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1546194375-powerselect-codFase"] input`);
    cy.fillInput(`[data-cy="1546194375-input-number-qtdCompMax"] textarea`, `8`);
    cy.fillInput(`[data-cy="1546194375-input-number-perdaQuebra"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-novo->3658278836-voltar->500483669-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-novo`, `3658278836-voltar`, `500483669-novo`];
    cy.visit('http://system-A10/desmontagem-mercadoria/AAA_DF?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="500483669-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/desmontagem-k210-k215->2610433226-novo->3658278836-voltar->500483669-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/desmontagem-k210-k215`, `2610433226-novo`, `3658278836-voltar`, `500483669-power-search-button`];
    cy.visit('http://system-A10/desmontagem-mercadoria/AAA_DF?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="500483669-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-novo->2538331999-voltar->148795338-novo`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-novo`, `2538331999-voltar`, `148795338-novo`];
    cy.visit('http://system-A10/producao-terceiro/AAA_DF?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="148795338-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/industrialização-k250-k255->3261365695-novo->2538331999-voltar->148795338-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/industrialização-k250-k255`, `3261365695-novo`, `2538331999-voltar`, `148795338-power-search-button`];
    cy.visit('http://system-A10/producao-terceiro/AAA_DF?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="148795338-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-executar->1426197106-múltipla seleção->1426197106-cancelar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/gerar-informacao-icms`, `1426197106-executar`, `1426197106-múltipla seleção`, `1426197106-cancelar`];
    cy.visit('http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1426197106-executar"]`);
    cy.clickIfExist(`[data-cy="1426197106-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1426197106-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-executar->2503184473-múltipla seleção->2503184473-cancelar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/apaga-informacao-icms`, `2503184473-executar`, `2503184473-múltipla seleção`, `2503184473-cancelar`];
    cy.visit('http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2503184473-executar"]`);
    cy.clickIfExist(`[data-cy="2503184473-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2503184473-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-executar->1473367084-múltipla seleção->1473367084-cancelar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpar-informacao-blocok`, `1473367084-executar`, `1473367084-múltipla seleção`, `1473367084-cancelar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1473367084-executar"]`);
    cy.clickIfExist(`[data-cy="1473367084-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1473367084-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-executar->3124260640-múltipla seleção->3124260640-cancelar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/processos`, `sped-fiscal/processos/limpeza-consumo-especifico-padrao`, `3124260640-executar`, `3124260640-múltipla seleção`, `3124260640-cancelar`];
    cy.visit('http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3124260640-executar"]`);
    cy.clickIfExist(`[data-cy="3124260640-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3124260640-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->1723811533-agendamentos->969732086-abrir visualização->969732086-expandir->969732086-diminuir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `1723811533-agendamentos`, `969732086-abrir visualização`, `969732086-expandir`, `969732086-diminuir`];
    cy.visit('http://system-A10/obrigacoes-executadas?sigla=~eq~PER-DCOMP%7C%7CPER-DCOMP&vlEstabelecimento=~eq~AAA_DF%7C%7CAAA_DF&usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="969732086-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="969732086-expandir"]`);
    cy.clickIfExist(`[data-cy="969732086-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-executar->354122518-múltipla seleção->354122518-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-mensais`, `354122518-executar`, `354122518-múltipla seleção`, `354122518-cancelar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="354122518-executar"]`);
    cy.clickIfExist(`[data-cy="354122518-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="354122518-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-executar->2241957464-múltipla seleção->2241957464-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/processos`, `obrigacoes/processos/totalizacoes-convenios`, `2241957464-executar`, `2241957464-múltipla seleção`, `2241957464-cancelar`];
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2241957464-executar"]`);
    cy.clickIfExist(`[data-cy="2241957464-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2241957464-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-executar->2697675163-múltipla seleção->2697675163-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-executar`, `2697675163-múltipla seleção`, `2697675163-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-executar"]`);
    cy.clickIfExist(`[data-cy="2697675163-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2697675163-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-abrir visualização->2697675163-expandir->2697675163-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/apuracao-icms-ipi-detalhado`, `2697675163-abrir visualização`, `2697675163-expandir`, `2697675163-diminuir`];
    cy.visit('http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2697675163-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2697675163-expandir"]`);
    cy.clickIfExist(`[data-cy="2697675163-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-executar->3866849537-múltipla seleção->3866849537-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-executar`, `3866849537-múltipla seleção`, `3866849537-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-executar"]`);
    cy.clickIfExist(`[data-cy="3866849537-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3866849537-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-abrir visualização->3866849537-expandir->3866849537-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/debito-credito-detalhado`, `3866849537-abrir visualização`, `3866849537-expandir`, `3866849537-diminuir`];
    cy.visit('http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3866849537-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3866849537-expandir"]`);
    cy.clickIfExist(`[data-cy="3866849537-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-executar->3969562164-múltipla seleção->3969562164-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas`, `3969562164-executar`, `3969562164-múltipla seleção`, `3969562164-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3969562164-executar"]`);
    cy.clickIfExist(`[data-cy="3969562164-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3969562164-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-executar->390233146-múltipla seleção->390233146-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/diferencial-aliquotas-fcp`, `390233146-executar`, `390233146-múltipla seleção`, `390233146-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="390233146-executar"]`);
    cy.clickIfExist(`[data-cy="390233146-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="390233146-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-executar->2613819546-múltipla seleção->2613819546-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado`, `2613819546-executar`, `2613819546-múltipla seleção`, `2613819546-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2613819546-executar"]`);
    cy.clickIfExist(`[data-cy="2613819546-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2613819546-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-executar->3648812560-múltipla seleção->3648812560-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-executar`, `3648812560-múltipla seleção`, `3648812560-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-executar"]`);
    cy.clickIfExist(`[data-cy="3648812560-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3648812560-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-abrir visualização->3648812560-expandir->3648812560-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/lista-cfop-nop`, `3648812560-abrir visualização`, `3648812560-expandir`, `3648812560-diminuir`];
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3648812560-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3648812560-expandir"]`);
    cy.clickIfExist(`[data-cy="3648812560-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-executar->2707770761-múltipla seleção->2707770761-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-executar`, `2707770761-múltipla seleção`, `2707770761-cancelar`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-executar"]`);
    cy.clickIfExist(`[data-cy="2707770761-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2707770761-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-abrir visualização->2707770761-expandir->2707770761-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/apuracao`, `relatorios/apuracao/registros-entradas`, `2707770761-abrir visualização`, `2707770761-expandir`, `2707770761-diminuir`];
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2707770761-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="2707770761-expandir"]`);
    cy.clickIfExist(`[data-cy="2707770761-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-executar->2394123409-múltipla seleção->2394123409-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/chave-eletronica-documentos`, `2394123409-executar`, `2394123409-múltipla seleção`, `2394123409-cancelar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2394123409-executar"]`);
    cy.clickIfExist(`[data-cy="2394123409-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2394123409-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-executar->4204835061-múltipla seleção->4204835061-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-executar`, `4204835061-múltipla seleção`, `4204835061-cancelar`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-executar"]`);
    cy.clickIfExist(`[data-cy="4204835061-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4204835061-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-abrir visualização->4204835061-expandir->4204835061-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/inconsistencias`, `relatorios/inconsistencias/dof-sem-lfis`, `4204835061-abrir visualização`, `4204835061-expandir`, `4204835061-diminuir`];
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4204835061-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="4204835061-expandir"]`);
    cy.clickIfExist(`[data-cy="4204835061-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-executar->2549852581-múltipla seleção->2549852581-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/controle-producao-estoque`, `2549852581-executar`, `2549852581-múltipla seleção`, `2549852581-cancelar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2549852581-executar"]`);
    cy.clickIfExist(`[data-cy="2549852581-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="2549852581-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-executar->3806525293-múltipla seleção->3806525293-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k`, `3806525293-executar`, `3806525293-múltipla seleção`, `3806525293-cancelar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3806525293-executar"]`);
    cy.clickIfExist(`[data-cy="3806525293-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3806525293-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-executar->117133032-múltipla seleção->117133032-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/criticas-saldos-inventarios`, `117133032-executar`, `117133032-múltipla seleção`, `117133032-cancelar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="117133032-executar"]`);
    cy.clickIfExist(`[data-cy="117133032-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="117133032-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-executar->877676973-múltipla seleção->877676973-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-executar`, `877676973-múltipla seleção`, `877676973-cancelar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-executar"]`);
    cy.clickIfExist(`[data-cy="877676973-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="877676973-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-abrir visualização->877676973-expandir->877676973-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido`, `877676973-abrir visualização`, `877676973-expandir`, `877676973-diminuir`];
    cy.visit('http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="877676973-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="877676973-expandir"]`);
    cy.clickIfExist(`[data-cy="877676973-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-executar->1298845030-múltipla seleção->1298845030-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-executar`, `1298845030-múltipla seleção`, `1298845030-cancelar`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-executar"]`);
    cy.clickIfExist(`[data-cy="1298845030-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1298845030-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-abrir visualização->1298845030-expandir->1298845030-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/producao-estoque`, `relatorios/producao-estoque/livro-inventario`, `1298845030-abrir visualização`, `1298845030-expandir`, `1298845030-diminuir`];
    cy.visit('http://system-A10/relatorios/producao-estoque/livro-inventario?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298845030-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1298845030-expandir"]`);
    cy.clickIfExist(`[data-cy="1298845030-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-executar->3761231093-múltipla seleção->3761231093-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-executar`, `3761231093-múltipla seleção`, `3761231093-cancelar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-executar"]`);
    cy.clickIfExist(`[data-cy="3761231093-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3761231093-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-abrir visualização->3761231093-expandir->3761231093-diminuir`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-cfop`, `3761231093-abrir visualização`, `3761231093-expandir`, `3761231093-diminuir`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3761231093-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3761231093-expandir"]`);
    cy.clickIfExist(`[data-cy="3761231093-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-executar->3723571518-múltipla seleção->3723571518-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-nop`, `3723571518-executar`, `3723571518-múltipla seleção`, `3723571518-cancelar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3723571518-executar"]`);
    cy.clickIfExist(`[data-cy="3723571518-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3723571518-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-executar->3722346050-múltipla seleção->3722346050-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/resumo`, `relatorios/resumo/resumo-uf`, `3722346050-executar`, `3722346050-múltipla seleção`, `3722346050-cancelar`];
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3722346050-executar"]`);
    cy.clickIfExist(`[data-cy="3722346050-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3722346050-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra->3352686717-orderedlistoutlined->3352686717-downoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`, `3352686717-orderedlistoutlined`, `3352686717-downoutlined`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-orderedlistoutlined"]`);
    cy.clickIfExist(`[data-cy="3352686717-downoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-estabelecimentos da regra->3352686717-orderedlistoutlined->3352686717-upoutlined`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-estabelecimentos da regra`, `3352686717-orderedlistoutlined`, `3352686717-upoutlined`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-estabelecimentos da regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-orderedlistoutlined"]`);
    cy.clickIfExist(`[data-cy="3352686717-upoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/reglanicms->3021063571-visualizar/editar->3352686717-testar regra->3352686717-ok->3352686717-download`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/reglanicms`, `3021063571-visualizar/editar`, `3352686717-testar regra`, `3352686717-ok`, `3352686717-download`];
    cy.visit('http://system-A10/regra-lancamento-impostos/editar/RVF_DIFA');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3352686717-testar regra"]`);
    cy.clickIfExist(`[data-cy="3352686717-ok"]`);
    cy.clickIfExist(`[data-cy="3352686717-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-estabelecimentos->3989490202-power-search-button`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-estabelecimentos`, `3989490202-power-search-button`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/1743');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-estabelecimentos"]`);
    cy.clickIfExist(`[data-cy="3989490202-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-novo->1701794451-salvar->3989490202-estabelecimentos->3989490202-power-search-input and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/parametrizacao-regras`, `escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto`, `739716742-novo`, `1701794451-salvar`, `3989490202-estabelecimentos`, `3989490202-power-search-input`];
    cy.visit('http://system-A10/inteligencia-fiscal/regra-lcpe/editar/1743');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3989490202-estabelecimentos"]`);
    cy.fillInputPowerSearch(`[data-cy="3989490202-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-executar->604343111-múltipla seleção`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-executar`, `604343111-múltipla seleção`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-executar"]`);
    cy.clickIfExist(`[data-cy="604343111-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-executar->604343111-agendar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-executar`, `604343111-agendar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-executar"]`);
    cy.clickIfExist(`[data-cy="604343111-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-agendamentos->604343111-voltar`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-agendamentos`, `604343111-voltar`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47367594D%7C%7C47367594&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-visualização->604343111-item- and submit`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-visualização`, `604343111-item-`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="604343111-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-detalhes->604343111-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-detalhes`, `604343111-dados disponíveis para impressão`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-detalhes"]`);
    cy.clickIfExist(`[data-cy="604343111-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-abrir visualização->604343111-aumentar o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-abrir visualização`, `604343111-aumentar o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="604343111-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-abrir visualização->604343111-diminuir o zoom`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-abrir visualização`, `604343111-diminuir o zoom`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="604343111-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-abrir visualização->604343111-expandir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-abrir visualização`, `604343111-expandir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="604343111-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-abrir visualização->604343111-download`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-abrir visualização`, `604343111-download`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="604343111-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-novo->2434758571-mais operações`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-novo`, `2434758571-mais operações`];
    cy.visit('http://system-A10/fiscal/classe-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2434758571-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-novo->2434758571-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-novo`, `2434758571-salvar`];
    cy.visit('http://system-A10/fiscal/classe-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2434758571-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-novo->2434758571-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-novo`, `2434758571-voltar`];
    cy.visit('http://system-A10/fiscal/classe-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2434758571-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-novo->2434758571-input-cinvCodigo-2434758571-input-classeInventario-2434758571-textarea-descricao-2434758571-checkbox-indLivroInvent and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-novo`, `2434758571-input-cinvCodigo-2434758571-input-classeInventario-2434758571-textarea-descricao-2434758571-checkbox-indLivroInvent`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-novo"]`);
    cy.fillInput(`[data-cy="2434758571-input-cinvCodigo"] textarea`, `Cliente`);
    cy.fillInput(`[data-cy="2434758571-input-classeInventario"] textarea`, `Beauty`);
    cy.fillInput(`[data-cy="2434758571-textarea-descricao"] input`, `Automotive`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2434758571-checkbox-indLivroInvent"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-mais operações->1298651134-item-`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-mais operações`, `1298651134-item-`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-mais operações"]`);
    cy.clickIfExist(`[data-cy="1298651134-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar->3516972338-mais operações`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`, `3516972338-mais operações`];
    cy.visit('http://system-A10/fiscal/classe-inventario/editar/A');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516972338-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar->3516972338-remover item`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`, `3516972338-remover item`];
    cy.visit('http://system-A10/fiscal/classe-inventario/editar/A');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516972338-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar->3516972338-salvar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`, `3516972338-salvar`];
    cy.visit('http://system-A10/fiscal/classe-inventario/editar/A');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516972338-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar->3516972338-voltar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`, `3516972338-voltar`];
    cy.visit('http://system-A10/fiscal/classe-inventario/editar/A');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516972338-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar->3516972338-input-cinvCodigo-3516972338-input-classeInventario-3516972338-textarea-descricao-3516972338-checkbox-indLivroInvent and submit`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`, `3516972338-input-cinvCodigo-3516972338-input-classeInventario-3516972338-textarea-descricao-3516972338-checkbox-indLivroInvent`];
    cy.visit('http://system-A10/fiscal/classe-inventario');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1298651134-visualizar/editar"]`);
    cy.fillInput(`[data-cy="3516972338-input-cinvCodigo"] textarea`, `Ponte`);
    cy.fillInput(`[data-cy="3516972338-input-classeInventario"] textarea`, `Shoes`);
    cy.fillInput(`[data-cy="3516972338-textarea-descricao"] input`, `functionalities`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3516972338-checkbox-indLivroInvent"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo->2187377653-selecionar insumo->2187377653-power-search-button`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`, `2187377653-selecionar insumo`, `2187377653-power-search-button`];
    cy.visit('http://system-A10/consumo-especifico-padrao/novo/AAA_DF/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2187377653-selecionar insumo"]`);
    cy.clickIfExist(`[data-cy="2187377653-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element sped-fiscal->sped-fiscal/bloco-k->sped-fiscal/bloco-k/consumo-0210->3433597533-insumos->795118794-novo->2187377653-selecionar insumo->2187377653-cancelar`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-k`, `sped-fiscal/bloco-k/consumo-0210`, `3433597533-insumos`, `795118794-novo`, `2187377653-selecionar insumo`, `2187377653-cancelar`];
    cy.visit('http://system-A10/consumo-especifico-padrao/novo/AAA_DF/12345');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2187377653-selecionar insumo"]`);
    cy.clickIfExist(`[data-cy="2187377653-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-abrir visualização->604343111-expandir->604343111-diminuir`, () => {
    const actualId = [`root`, `escrituracao-apuracao`, `escrituracao-apuracao/processos`, `escrituracao-apuracao/processos/apuracao-icms-ipi`, `77045265-mais operações`, `77045265-item-`, `604343111-abrir visualização`, `604343111-expandir`, `604343111-diminuir`];
    cy.visit('http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="604343111-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="604343111-expandir"]`);
    cy.clickIfExist(`[data-cy="604343111-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-novo->2434758571-mais operações->2434758571-item-`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-novo`, `2434758571-mais operações`, `2434758571-item-`];
    cy.visit('http://system-A10/fiscal/classe-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2434758571-mais operações"]`);
    cy.clickIfExist(`[data-cy="2434758571-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/classificacao-mercadoria->2738151926-mais operações->2738151926-item-->1298651134-visualizar/editar->3516972338-mais operações->3516972338-item-`, () => {
    const actualId = [`root`, `sped-fiscal`, `sped-fiscal/bloco-h`, `sped-fiscal/bloco-h/classificacao-mercadoria`, `2738151926-mais operações`, `2738151926-item-`, `1298651134-visualizar/editar`, `3516972338-mais operações`, `3516972338-item-`];
    cy.visit('http://system-A10/fiscal/classe-inventario/editar/A');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3516972338-mais operações"]`);
    cy.clickIfExist(`[data-cy="3516972338-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
