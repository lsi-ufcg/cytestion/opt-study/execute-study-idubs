describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values escrituracao-apuracao->escrituracao-apuracao/parametrizacao-regras->escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto->739716742-power-search-input and submit`, () => {
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao"]`);
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras"]`);
    cy.clickCauseExist(`[data-cy="escrituracao-apuracao/parametrizacao-regras/regras-lancamento-imposto"]`);
    cy.fillInputPowerSearch(`[data-cy="739716742-power-search-input"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-agendamentos->602971242-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210452D%7C%7C210452&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="602971242-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraLancFiscal->602971242-agendamentos->602971242-visualizar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/geraLancFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210452D%7C%7C210452&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="602971242-visualizar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaLancFiscalPeriod->2717158496-agendamentos->2717158496-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/apagaLancFiscalPeriod?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210449D%7C%7C210449&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2717158496-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApuracaoIcmsIpi->283960367-agendamentos->283960367-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/apagaApuracaoIcmsIpi?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~51138154D%7C%7C51138154&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="283960367-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/calcDiferencialAliq->2815987121-agendamentos->2815987121-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/calcDiferencialAliq?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210451D%7C%7C210451&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2815987121-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/geraValoresDOFs->4056389192-agendamentos->4056389192-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/geraValoresDOFs?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~245636D%7C%7C245636&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4056389192-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apagaApurProcAntigo->21201123-agendamentos->21201123-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/apagaApurProcAntigo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47905313D%7C%7C47905313&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="21201123-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/limpaJobLfis->4195790773-agendamentos->4195790773-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/limpaJobLfis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~48066345D%7C%7C48066345&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4195790773-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal->832827420-agendamentos->832827420-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/fechamentoEscrituracaoFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~49057672D%7C%7C49057672&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="832827420-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento->3840793780-agendamentos->3840793780-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/bloqueioLancamentoRecolhimento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~52427359D%7C%7C52427359&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3840793780-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/encerraReabrePeriodoFiscal->2515312772-agendamentos->2515312772-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/encerraReabrePeriodoFiscal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53141006D%7C%7C53141006&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2515312772-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/bloco-h->sped-fiscal/bloco-h/lancamento-inventario->2893041617-novo->3487034296-salvar`, () => {
    cy.visit('http://system-A10/fiscal/lancamento-inventario/novo');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3487034296-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos->1426197106-voltar`, () => {
    cy.visit(
      'http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505229D%7C%7C46505229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1426197106-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos->1426197106-visualizar`, () => {
    cy.visit(
      'http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505229D%7C%7C46505229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1426197106-visualizar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/gerar-informacao-icms->1426197106-agendamentos->1426197106-excluir`, () => {
    cy.visit(
      'http://system-A10/sped-fiscal/processos/gerar-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505229D%7C%7C46505229&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1426197106-excluir"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/apaga-informacao-icms->2503184473-agendamentos->2503184473-voltar`, () => {
    cy.visit(
      'http://system-A10/sped-fiscal/processos/apaga-informacao-icms?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~46505232D%7C%7C46505232&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2503184473-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpar-informacao-blocok->1473367084-agendamentos->1473367084-voltar`, () => {
    cy.visit(
      'http://system-A10/sped-fiscal/processos/limpar-informacao-blocok?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27733764D%7C%7C27733764&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1473367084-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element sped-fiscal->sped-fiscal/processos->sped-fiscal/processos/limpeza-consumo-especifico-padrao->3124260640-agendamentos->3124260640-voltar`, () => {
    cy.visit(
      'http://system-A10/sped-fiscal/processos/limpeza-consumo-especifico-padrao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~51024135D%7C%7C51024135&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3124260640-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-mensais->354122518-agendamentos->354122518-voltar`, () => {
    cy.visit('http://system-A10/obrigacoes/processos/totalizacoes-mensais?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210587D%7C%7C210587&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="354122518-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/totalizacoes-convenios->2241957464-agendamentos->2241957464-voltar`, () => {
    cy.visit(
      'http://system-A10/obrigacoes/processos/totalizacoes-convenios?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47661888D%7C%7C47661888&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2241957464-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/apuracao-icms-ipi-detalhado->2697675163-agendamentos->2697675163-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/apuracao/apuracao-icms-ipi-detalhado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54895821D%7C%7C54895821&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2697675163-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/debito-credito-detalhado->3866849537-agendamentos->3866849537-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/apuracao/debito-credito-detalhado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~55689114D%7C%7C55689114&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3866849537-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas->3969562164-agendamentos->3969562164-voltar`, () => {
    cy.visit('http://system-A10/relatorios/apuracao/diferencial-aliquotas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210506D%7C%7C210506&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3969562164-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/diferencial-aliquotas-fcp->390233146-agendamentos->390233146-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/apuracao/diferencial-aliquotas-fcp?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27644553D%7C%7C27644553&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="390233146-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado->2613819546-agendamentos->2613819546-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/apuracao/historico-encerramento-reabertura-periodo-apurado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47921082D%7C%7C47921082&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2613819546-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/lista-cfop-nop->3648812560-agendamentos->3648812560-voltar`, () => {
    cy.visit('http://system-A10/relatorios/apuracao/lista-cfop-nop?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210505D%7C%7C210505&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3648812560-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/apuracao->relatorios/apuracao/registros-entradas->2707770761-agendamentos->2707770761-voltar`, () => {
    cy.visit('http://system-A10/relatorios/apuracao/registros-entradas?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210485D%7C%7C210485&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2707770761-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/chave-eletronica-documentos->2394123409-agendamentos->2394123409-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/inconsistencias/chave-eletronica-documentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31802311D%7C%7C31802311&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2394123409-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/inconsistencias->relatorios/inconsistencias/dof-sem-lfis->4204835061-agendamentos->4204835061-voltar`, () => {
    cy.visit('http://system-A10/relatorios/inconsistencias/dof-sem-lfis?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210436D%7C%7C210436&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4204835061-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/controle-producao-estoque->2549852581-agendamentos->2549852581-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/controle-producao-estoque?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210491D%7C%7C210491&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2549852581-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k->3806525293-agendamentos->3806525293-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/criticas-informacoes-sped-fiscal-bloco-k?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27728737D%7C%7C27728737&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3806525293-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/criticas-saldos-inventarios->117133032-agendamentos->117133032-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/criticas-saldos-inventarios?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~31853105D%7C%7C31853105&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="117133032-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido->877676973-agendamentos->877676973-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/extracao-informacoes-controle-estoque-resumido?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~53040144D%7C%7C53040144&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="877676973-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/extracao-lancamentos-inventarios->1452122080-agendamentos->1452122080-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/extracao-lancamentos-inventarios?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54895824D%7C%7C54895824&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1452122080-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/informacoes-bloco-k->3374403702-agendamentos->3374403702-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/informacoes-bloco-k?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54895798D%7C%7C54895798&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3374403702-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/producao-estoque->relatorios/producao-estoque/livro-inventario->1298845030-agendamentos->1298845030-voltar`, () => {
    cy.visit(
      'http://system-A10/relatorios/producao-estoque/livro-inventario?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210490D%7C%7C210490&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1298845030-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-cfop->3761231093-agendamentos->3761231093-voltar`, () => {
    cy.visit('http://system-A10/relatorios/resumo/resumo-cfop?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210510D%7C%7C210510&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3761231093-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-nop->3723571518-agendamentos->3723571518-voltar`, () => {
    cy.visit('http://system-A10/relatorios/resumo/resumo-nop?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210512D%7C%7C210512&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3723571518-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/resumo->relatorios/resumo/resumo-uf->3722346050-agendamentos->3722346050-voltar`, () => {
    cy.visit('http://system-A10/relatorios/resumo/resumo-uf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~57898507D%7C%7C57898507&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3722346050-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element escrituracao-apuracao->escrituracao-apuracao/processos->escrituracao-apuracao/processos/apuracao-icms-ipi->77045265-mais operações->77045265-item-->604343111-agendamentos->604343111-voltar`, () => {
    cy.visit(
      'http://system-A10/escrituracao-apuracao/processos/apuracao-icms-ipi/agendamento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47367594D%7C%7C47367594&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="604343111-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
