describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-agendamentos->1869266243-voltar`, () => {
    cy.visit('http://system-A9/conteudo-importacao/processos/gerar-ci?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149071D%7C%7C33149071&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1869266243-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-agendamentos->2161799073-voltar`, () => {
    cy.visit(
      'http://system-A9/conteudo-importacao/processos/gerar-ci-estimativa?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149077D%7C%7C33149077&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2161799073-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-agendamentos->3930886052-voltar`, () => {
    cy.visit('http://system-A9/conteudo-importacao/processos/replicar-ci?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149088D%7C%7C33149088&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3930886052-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-agendamentos->18966040-voltar`, () => {
    cy.visit('http://system-A9/conteudo-importacao/processos/apagar-ci?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149084D%7C%7C33149084&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="18966040-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-agendamentos->4013629049-voltar`, () => {
    cy.visit('http://system-A9/conteudo-importacao/processos/exportar-fci?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149092D%7C%7C33149092&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4013629049-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-agendamentos->2164517070-voltar`, () => {
    cy.visit(
      'http://system-A9/conteudo-importacao/relatorios/memoria-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149050D%7C%7C33149050&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2164517070-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-agendamentos->2832538547-voltar`, () => {
    cy.visit('http://system-A9/conteudo-importacao/relatorios/composicao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149054D%7C%7C33149054&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2832538547-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-agendamentos->3114380081-voltar`, () => {
    cy.visit(
      'http://system-A9/conteudo-importacao/relatorios/composicao-estimativa?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149059D%7C%7C33149059&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3114380081-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-agendamentos->573119890-voltar`, () => {
    cy.visit(
      'http://system-A9/conteudo-importacao/relatorios/enquadramento-documentos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~33149065D%7C%7C33149065&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="573119890-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
