describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  it(`Click on element root->conteudo-importacao->conteudo-importacao-informacoes-cadastrais->conteudo-importacao-informacoes-cadastrais-estrutura-mercadorias-fci`, () => {
    cy.clickCauseExist(`[data-cy="conteudo-importacao"]`);
    cy.clickCauseExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
    cy.clickCauseExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
    cy.checkErrorsWereDetected();
  });
});
