describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
const actualId = [`root`,`tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema`, () => {
const actualId = [`root`,`parametros-sistema`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
const actualId = [`root`,`tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao`, () => {
const actualId = [`root`,`conteudo-importacao`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
const actualId = [`root`,`downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 3791239504-exibir dados`, () => {
const actualId = [`root`,`3791239504-exibir dados`];
    cy.clickIfExist(`[data-cy="3791239504-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/origem-mercadoria`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/origem-mercadoria`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/origem-mercadoria"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema->parametros-sistema/parametros-gerais`, () => {
const actualId = [`root`,`parametros-sistema`,`parametros-sistema/parametros-gerais`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
      cy.clickIfExist(`[data-cy="parametros-sistema/parametros-gerais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/transacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/transacoes`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1930978028-power-search-button`, () => {
const actualId = [`root`,`downloads`,`1930978028-power-search-button`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1930978028-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1930978028-download`, () => {
const actualId = [`root`,`downloads`,`1930978028-download`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1930978028-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1930978028-detalhes`, () => {
const actualId = [`root`,`downloads`,`1930978028-detalhes`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1930978028-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->1930978028-excluir`, () => {
const actualId = [`root`,`downloads`,`1930978028-excluir`];
    cy.clickIfExist(`[data-cy="downloads"]`);
      cy.clickIfExist(`[data-cy="1930978028-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros-sistema->parametros-sistema/parametros-gerais->parametros-sistema/parametros-gerais/parametrizacao-geral`, () => {
const actualId = [`root`,`parametros-sistema`,`parametros-sistema/parametros-gerais`,`parametros-sistema/parametros-gerais/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="parametros-sistema"]`);
      cy.clickIfExist(`[data-cy="parametros-sistema/parametros-gerais"]`);
      cy.clickIfExist(`[data-cy="parametros-sistema/parametros-gerais/parametrizacao-geral"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/pessoas-fisicas-juridicas`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pfj`,`tabelas-corporativas/pfj/pessoas-fisicas-juridicas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/pessoas-fisicas-juridicas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/cadastro-mercadorias`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`,`tabelas-corporativas/mercadorias/cadastro-mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/cadastro-mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/unidade-medida`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`,`tabelas-corporativas/mercadorias/unidade-medida`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/unidade-medida"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/transacoes->tabelas-corporativas/transacoes/digitacao-manutencao-dof`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/transacoes`,`tabelas-corporativas/transacoes/digitacao-manutencao-dof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/transacoes/digitacao-manutencao-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/importacao-controles-fci`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/importacao-controles-fci`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/importacao-controles-fci"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->3784989026-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`3784989026-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3784989026-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->3784989026-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`3784989026-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3784989026-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->3784989026-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`3784989026-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3784989026-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-novo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-novo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-selectoutlined`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-selectoutlined`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-selectoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-deleteoutlined`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-deleteoutlined`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-novo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-novo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-regerar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-regerar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-detalhes`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-detalhes`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-abrir visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-abrir visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-excluir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-excluir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-regerar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-regerar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-detalhes`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-detalhes`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-abrir visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-abrir visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-excluir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-excluir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-regerar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-regerar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-detalhes`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-detalhes`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-abrir visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-abrir visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-excluir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-excluir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/importacao-controles-fci->3614748927-importar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/importacao-controles-fci`,`3614748927-importar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/importacao-controles-fci"]`);
      cy.clickIfExist(`[data-cy="3614748927-importar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/importacao-controles-fci->3614748927-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/importacao-controles-fci`,`3614748927-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/importacao-controles-fci"]`);
      cy.clickIfExist(`[data-cy="3614748927-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-regerar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-regerar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-detalhes`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-detalhes`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-abrir visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-abrir visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-excluir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-excluir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-executar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-executar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-agendamentos`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-agendamentos`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-visualização`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-visualização`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-gerenciar labels->778959242-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-gerenciar labels`,`778959242-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="778959242-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-visualizar/editar->2070648359-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-visualizar/editar`,`2070648359-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2070648359-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->778959242-visualizar/editar->2070648359-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`778959242-visualizar/editar`,`2070648359-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="778959242-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2070648359-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-visualização->1557170498-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-visualização`,`1557170498-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1557170498-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-abrir visualização->1557170498-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-abrir visualização`,`1557170498-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1557170498-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-abrir visualização->1557170498-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-abrir visualização`,`1557170498-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1557170498-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-abrir visualização->1557170498-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-abrir visualização`,`1557170498-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1557170498-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-abrir visualização->1557170498-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-abrir visualização`,`1557170498-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1557170498-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-visualizar->1557170498-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-visualizar`,`1557170498-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-visualizar"]`);
      cy.clickIfExist(`[data-cy="1557170498-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-novo->334531258-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-novo`,`334531258-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-novo"]`);
      cy.clickIfExist(`[data-cy="334531258-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-novo->334531258-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-novo`,`334531258-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-novo"]`);
      cy.clickIfExist(`[data-cy="334531258-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->334531258-novo->334531258-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-novo`,`334531258-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-novo"]`);
      cy.fillInput(`[data-cy="334531258-input-number-ano"] textarea`, `10`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-editar->334531258-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-editar`,`334531258-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-editar"]`);
      cy.clickIfExist(`[data-cy="334531258-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->334531258-editar->334531258-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`334531258-editar`,`334531258-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="334531258-editar"]`);
      cy.clickIfExist(`[data-cy="334531258-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/estabelecimento->3784989026-novo->3784989026-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/estabelecimento`,`3784989026-novo`,`3784989026-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3784989026-novo"]`);
      cy.clickIfExist(`[data-cy="3784989026-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-selectoutlined->2635423676-novo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-selectoutlined`,`2635423676-novo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2635423676-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-selectoutlined->2635423676-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-selectoutlined`,`2635423676-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-selectoutlined"]`);
      cy.clickIfExist(`[data-cy="2635423676-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-mais operações`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-mais operações`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-mais operações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-remover item`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-remover item`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-salvar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-salvar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-novo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-novo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-eyeoutlined`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-eyeoutlined`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-deleteoutlined`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-deleteoutlined`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-powerselect-estCodigo and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-powerselect-estCodigo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.fillInputPowerSelect(`[data-cy="456659030-powerselect-estCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-novo->1607739854-salvar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-novo`,`1607739854-salvar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.clickIfExist(`[data-cy="1607739854-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-novo->1607739854-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-novo`,`1607739854-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.clickIfExist(`[data-cy="1607739854-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-novo->1607739854-powerselect-estCodigo-1607739854-powerselect-mercCodigo-1607739854-input-fciNumero-1607739854-checkbox-fciNova-1607739854-powerselect-omCodigo-1607739854-input-monetary-valorImportado-1607739854-input-monetary-qtdImportado-1607739854-input-monetary-valorNacional-1607739854-input-monetary-qtdNacional-1607739854-input-monetary-valorInterestadual-1607739854-input-monetary-qtdInterestadual-1607739854-input-monetary-conteudoImportacao-1607739854-input-monetary-valorImportacao-1607739854-input-protocolo and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-novo`,`1607739854-powerselect-estCodigo-1607739854-powerselect-mercCodigo-1607739854-input-fciNumero-1607739854-checkbox-fciNova-1607739854-powerselect-omCodigo-1607739854-input-monetary-valorImportado-1607739854-input-monetary-qtdImportado-1607739854-input-monetary-valorNacional-1607739854-input-monetary-qtdNacional-1607739854-input-monetary-valorInterestadual-1607739854-input-monetary-qtdInterestadual-1607739854-input-monetary-conteudoImportacao-1607739854-input-monetary-valorImportacao-1607739854-input-protocolo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1607739854-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1607739854-powerselect-mercCodigo"] input`);
cy.fillInput(`[data-cy="1607739854-input-fciNumero"] textarea`, `Concrete`);
cy.fillInputCheckboxOrRadio(`[data-cy="1607739854-checkbox-fciNova"] textarea`);
cy.fillInputPowerSelect(`[data-cy="1607739854-powerselect-omCodigo"] input`);
cy.fillInput(`[data-cy="1607739854-input-monetary-valorImportado"] textarea`, `3,07`);
cy.fillInput(`[data-cy="1607739854-input-monetary-qtdImportado"] textarea`, `2,61`);
cy.fillInput(`[data-cy="1607739854-input-monetary-valorNacional"] textarea`, `8,92`);
cy.fillInput(`[data-cy="1607739854-input-monetary-qtdNacional"] textarea`, `1,71`);
cy.fillInput(`[data-cy="1607739854-input-monetary-valorInterestadual"] textarea`, `1,15`);
cy.fillInput(`[data-cy="1607739854-input-monetary-qtdInterestadual"] textarea`, `1,35`);
cy.fillInput(`[data-cy="1607739854-input-monetary-conteudoImportacao"] textarea`, `1,31`);
cy.fillInput(`[data-cy="1607739854-input-monetary-valorImportacao"] textarea`, `2,4`);
cy.fillInput(`[data-cy="1607739854-input-protocolo"] textarea`, `multibyte`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-executar->1869266243-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-executar`,`1869266243-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-executar"]`);
      cy.clickIfExist(`[data-cy="1869266243-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-executar->1869266243-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-executar`,`1869266243-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-executar"]`);
      cy.clickIfExist(`[data-cy="1869266243-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-executar->1869266243-input-PERIODO-1869266243-input-PERIODO2 and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-executar`,`1869266243-input-PERIODO-1869266243-input-PERIODO2`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-executar"]`);
      cy.fillInput(`[data-cy="1869266243-input-PERIODO"] textarea`, `Shoes`);
cy.fillInput(`[data-cy="1869266243-input-PERIODO2"] textarea`, `Customizable`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-agendamentos->1869266243-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-agendamentos`,`1869266243-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1869266243-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-visualização->1869266243-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-visualização`,`1869266243-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1869266243-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-detalhes->1869266243-dados disponíveis para impressão`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-detalhes`,`1869266243-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-detalhes"]`);
      cy.clickIfExist(`[data-cy="1869266243-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-abrir visualização->1869266243-aumentar o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-abrir visualização`,`1869266243-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1869266243-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-abrir visualização->1869266243-diminuir o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-abrir visualização`,`1869266243-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1869266243-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-abrir visualização->1869266243-expandir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-abrir visualização`,`1869266243-expandir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1869266243-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-abrir visualização->1869266243-download`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-abrir visualização`,`1869266243-download`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1869266243-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-executar->2161799073-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-executar`,`2161799073-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-executar"]`);
      cy.clickIfExist(`[data-cy="2161799073-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-executar->2161799073-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-executar`,`2161799073-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-executar"]`);
      cy.clickIfExist(`[data-cy="2161799073-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-executar->2161799073-input-PERIODO-2161799073-input-PERIODO2 and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-executar`,`2161799073-input-PERIODO-2161799073-input-PERIODO2`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-executar"]`);
      cy.fillInput(`[data-cy="2161799073-input-PERIODO"] textarea`, `transition`);
cy.fillInput(`[data-cy="2161799073-input-PERIODO2"] textarea`, `Gorgeous Concrete Ball`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-agendamentos->2161799073-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-agendamentos`,`2161799073-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2161799073-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-visualização->2161799073-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-visualização`,`2161799073-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2161799073-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-detalhes->2161799073-dados disponíveis para impressão`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-detalhes`,`2161799073-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-detalhes"]`);
      cy.clickIfExist(`[data-cy="2161799073-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-abrir visualização->2161799073-aumentar o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-abrir visualização`,`2161799073-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2161799073-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-abrir visualização->2161799073-diminuir o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-abrir visualização`,`2161799073-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2161799073-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-abrir visualização->2161799073-expandir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-abrir visualização`,`2161799073-expandir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2161799073-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-abrir visualização->2161799073-download`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-abrir visualização`,`2161799073-download`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2161799073-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-executar->3930886052-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-executar`,`3930886052-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-executar"]`);
      cy.clickIfExist(`[data-cy="3930886052-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-executar->3930886052-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-executar`,`3930886052-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-executar"]`);
      cy.clickIfExist(`[data-cy="3930886052-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-executar->3930886052-input-PERIODO and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-executar`,`3930886052-input-PERIODO`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-executar"]`);
      cy.fillInput(`[data-cy="3930886052-input-PERIODO"] textarea`, `Small`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-agendamentos->3930886052-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-agendamentos`,`3930886052-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3930886052-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-visualização->3930886052-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-visualização`,`3930886052-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3930886052-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-executar->18966040-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-executar`,`18966040-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-executar"]`);
      cy.clickIfExist(`[data-cy="18966040-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-executar->18966040-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-executar`,`18966040-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-executar"]`);
      cy.clickIfExist(`[data-cy="18966040-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-agendamentos->18966040-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-agendamentos`,`18966040-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-agendamentos"]`);
      cy.clickIfExist(`[data-cy="18966040-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-visualização->18966040-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-visualização`,`18966040-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="18966040-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-detalhes->18966040-dados disponíveis para impressão`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-detalhes`,`18966040-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-detalhes"]`);
      cy.clickIfExist(`[data-cy="18966040-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-abrir visualização->18966040-aumentar o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-abrir visualização`,`18966040-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="18966040-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-abrir visualização->18966040-diminuir o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-abrir visualização`,`18966040-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="18966040-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-abrir visualização->18966040-expandir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-abrir visualização`,`18966040-expandir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="18966040-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-abrir visualização->18966040-download`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-abrir visualização`,`18966040-download`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="18966040-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-executar->4013629049-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-executar`,`4013629049-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-executar"]`);
      cy.clickIfExist(`[data-cy="4013629049-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-executar->4013629049-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-executar`,`4013629049-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-executar"]`);
      cy.clickIfExist(`[data-cy="4013629049-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-executar->4013629049-input-PERIODO and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-executar`,`4013629049-input-PERIODO`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-executar"]`);
      cy.fillInput(`[data-cy="4013629049-input-PERIODO"] textarea`, `Ergonomic`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-agendamentos->4013629049-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-agendamentos`,`4013629049-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4013629049-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-visualização->4013629049-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-visualização`,`4013629049-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4013629049-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/importacao-controles-fci->3614748927-importar->3614748927- selecionar arquivo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/importacao-controles-fci`,`3614748927-importar`,`3614748927- selecionar arquivo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/importacao-controles-fci"]`);
      cy.clickIfExist(`[data-cy="3614748927-importar"]`);
      cy.clickIfExist(`[data-cy="3614748927- selecionar arquivo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/importacao-controles-fci->3614748927-importar->3614748927-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/importacao-controles-fci`,`3614748927-importar`,`3614748927-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/importacao-controles-fci"]`);
      cy.clickIfExist(`[data-cy="3614748927-importar"]`);
      cy.clickIfExist(`[data-cy="3614748927-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/importacao-controles-fci->3614748927-importar->3614748927-powerselect-estabelecimento-3614748927-radio-tipoArquivo and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/importacao-controles-fci`,`3614748927-importar`,`3614748927-powerselect-estabelecimento-3614748927-radio-tipoArquivo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/importacao-controles-fci"]`);
      cy.clickIfExist(`[data-cy="3614748927-importar"]`);
      cy.fillInputPowerSelect(`[data-cy="3614748927-powerselect-estabelecimento"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="3614748927-radio-tipoArquivo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-executar->2164517070-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-executar`,`2164517070-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-executar"]`);
      cy.clickIfExist(`[data-cy="2164517070-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-executar->2164517070-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-executar`,`2164517070-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-executar"]`);
      cy.clickIfExist(`[data-cy="2164517070-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-executar->2164517070-input-PERIODO and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-executar`,`2164517070-input-PERIODO`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-executar"]`);
      cy.fillInput(`[data-cy="2164517070-input-PERIODO"] textarea`, `focus group`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-agendamentos->2164517070-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-agendamentos`,`2164517070-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2164517070-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-visualização->2164517070-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-visualização`,`2164517070-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2164517070-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-detalhes->2164517070-dados disponíveis para impressão`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-detalhes`,`2164517070-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-detalhes"]`);
      cy.clickIfExist(`[data-cy="2164517070-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-abrir visualização->2164517070-aumentar o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-abrir visualização`,`2164517070-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2164517070-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-abrir visualização->2164517070-diminuir o zoom`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-abrir visualização`,`2164517070-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2164517070-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-abrir visualização->2164517070-expandir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-abrir visualização`,`2164517070-expandir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2164517070-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-abrir visualização->2164517070-download`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-abrir visualização`,`2164517070-download`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2164517070-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-executar->2832538547-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-executar`,`2832538547-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-executar"]`);
      cy.clickIfExist(`[data-cy="2832538547-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-executar->2832538547-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-executar`,`2832538547-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-executar"]`);
      cy.clickIfExist(`[data-cy="2832538547-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-executar->2832538547-input-PERIODO-2832538547-input-PERIODO2 and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-executar`,`2832538547-input-PERIODO-2832538547-input-PERIODO2`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-executar"]`);
      cy.fillInput(`[data-cy="2832538547-input-PERIODO"] textarea`, `Keyboard`);
cy.fillInput(`[data-cy="2832538547-input-PERIODO2"] textarea`, `Grupo`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-agendamentos->2832538547-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-agendamentos`,`2832538547-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2832538547-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-visualização->2832538547-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-visualização`,`2832538547-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2832538547-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-executar->3114380081-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-executar`,`3114380081-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-executar"]`);
      cy.clickIfExist(`[data-cy="3114380081-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-executar->3114380081-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-executar`,`3114380081-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-executar"]`);
      cy.clickIfExist(`[data-cy="3114380081-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-executar->3114380081-input-PERIODO-3114380081-input-PERIODO2 and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-executar`,`3114380081-input-PERIODO-3114380081-input-PERIODO2`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-executar"]`);
      cy.fillInput(`[data-cy="3114380081-input-PERIODO"] textarea`, `efficient`);
cy.fillInput(`[data-cy="3114380081-input-PERIODO2"] textarea`, `Rodovia`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-agendamentos->3114380081-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-agendamentos`,`3114380081-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3114380081-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-visualização->3114380081-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-visualização`,`3114380081-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3114380081-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-executar->573119890-múltipla seleção`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-executar`,`573119890-múltipla seleção`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-executar"]`);
      cy.clickIfExist(`[data-cy="573119890-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-executar->573119890-agendar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-executar`,`573119890-agendar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-executar"]`);
      cy.clickIfExist(`[data-cy="573119890-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-executar->573119890-input-PERIODO-573119890-input-PERIODO2 and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-executar`,`573119890-input-PERIODO-573119890-input-PERIODO2`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-executar"]`);
      cy.fillInput(`[data-cy="573119890-input-PERIODO"] textarea`, `Automotive`);
cy.fillInput(`[data-cy="573119890-input-PERIODO2"] textarea`, `Implementao`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-agendamentos->573119890-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-agendamentos`,`573119890-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-agendamentos"]`);
      cy.clickIfExist(`[data-cy="573119890-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-visualização->573119890-item- and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-visualização`,`573119890-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="573119890-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-voltar às obrigações do módulo->3611409689-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-voltar às obrigações do módulo`,`3611409689-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-voltar às obrigações do módulo"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-nova solicitação->3611409689-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-nova solicitação`,`3611409689-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="3611409689-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-nova solicitação->3611409689-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-nova solicitação`,`3611409689-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="3611409689-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-agendamentos->1557170498-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-agendamentos`,`1557170498-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1557170498-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-agendamentos->1557170498-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-agendamentos`,`1557170498-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1557170498-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1557170498-abrir visualização->1557170498-expandir->1557170498-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`1557170498-abrir visualização`,`1557170498-expandir`,`1557170498-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="1557170498-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1557170498-expandir"]`);
      cy.clickIfExist(`[data-cy="1557170498-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-mais operações->456659030-item-`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-mais operações`,`456659030-item-`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-mais operações"]`);
      cy.clickIfExist(`[data-cy="456659030-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-novo->240213176-salvar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-novo`,`240213176-salvar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-novo"]`);
      cy.clickIfExist(`[data-cy="240213176-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-novo->240213176-voltar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-novo`,`240213176-voltar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-novo"]`);
      cy.clickIfExist(`[data-cy="240213176-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values conteudo-importacao->conteudo-importacao/informacoes-cadastrais->conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci->1084560218-eyeoutlined->456659030-novo->240213176-powerselect-mercCodigo-240213176-input-monetary-qtd-240213176-input-monetary-qtdReal-240213176-powerselect-umReal-240213176-powerselect-indOrigem and submit`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/informacoes-cadastrais`,`conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci`,`1084560218-eyeoutlined`,`456659030-novo`,`240213176-powerselect-mercCodigo-240213176-input-monetary-qtd-240213176-input-monetary-qtdReal-240213176-powerselect-umReal-240213176-powerselect-indOrigem`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/informacoes-cadastrais/estrutura-mercadorias-fci"]`);
      cy.clickIfExist(`[data-cy="1084560218-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="456659030-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="240213176-powerselect-mercCodigo"] input`);
cy.fillInput(`[data-cy="240213176-input-monetary-qtd"] textarea`, `4,23`);
cy.fillInput(`[data-cy="240213176-input-monetary-qtdReal"] textarea`, `9,45`);
cy.fillInputPowerSelect(`[data-cy="240213176-powerselect-umReal"] input`);
cy.fillInputPowerSelect(`[data-cy="240213176-powerselect-indOrigem"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-novo->1607739854-voltar->2038373371-novo`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-novo`,`1607739854-voltar`,`2038373371-novo`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.clickIfExist(`[data-cy="1607739854-voltar"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/transacoes->conteudo-importacao/transacoes/conteudo-importacao->2038373371-novo->1607739854-voltar->2038373371-power-search-button`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/transacoes`,`conteudo-importacao/transacoes/conteudo-importacao`,`2038373371-novo`,`1607739854-voltar`,`2038373371-power-search-button`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/transacoes/conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="2038373371-novo"]`);
      cy.clickIfExist(`[data-cy="1607739854-voltar"]`);
      cy.clickIfExist(`[data-cy="2038373371-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-executar->1869266243-múltipla seleção->1869266243-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-executar`,`1869266243-múltipla seleção`,`1869266243-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-executar"]`);
      cy.clickIfExist(`[data-cy="1869266243-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1869266243-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci->1869266243-abrir visualização->1869266243-expandir->1869266243-diminuir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci`,`1869266243-abrir visualização`,`1869266243-expandir`,`1869266243-diminuir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci"]`);
      cy.clickIfExist(`[data-cy="1869266243-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="1869266243-expandir"]`);
      cy.clickIfExist(`[data-cy="1869266243-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-executar->2161799073-múltipla seleção->2161799073-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-executar`,`2161799073-múltipla seleção`,`2161799073-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-executar"]`);
      cy.clickIfExist(`[data-cy="2161799073-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2161799073-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/gerar-ci-estimativa->2161799073-abrir visualização->2161799073-expandir->2161799073-diminuir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/gerar-ci-estimativa`,`2161799073-abrir visualização`,`2161799073-expandir`,`2161799073-diminuir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/gerar-ci-estimativa"]`);
      cy.clickIfExist(`[data-cy="2161799073-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2161799073-expandir"]`);
      cy.clickIfExist(`[data-cy="2161799073-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/replicar-ci->3930886052-executar->3930886052-múltipla seleção->3930886052-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/replicar-ci`,`3930886052-executar`,`3930886052-múltipla seleção`,`3930886052-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/replicar-ci"]`);
      cy.clickIfExist(`[data-cy="3930886052-executar"]`);
      cy.clickIfExist(`[data-cy="3930886052-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3930886052-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-executar->18966040-múltipla seleção->18966040-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-executar`,`18966040-múltipla seleção`,`18966040-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-executar"]`);
      cy.clickIfExist(`[data-cy="18966040-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="18966040-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/apagar-ci->18966040-abrir visualização->18966040-expandir->18966040-diminuir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/apagar-ci`,`18966040-abrir visualização`,`18966040-expandir`,`18966040-diminuir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/apagar-ci"]`);
      cy.clickIfExist(`[data-cy="18966040-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="18966040-expandir"]`);
      cy.clickIfExist(`[data-cy="18966040-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/processos->conteudo-importacao/processos/exportar-fci->4013629049-executar->4013629049-múltipla seleção->4013629049-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/processos`,`conteudo-importacao/processos/exportar-fci`,`4013629049-executar`,`4013629049-múltipla seleção`,`4013629049-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/processos/exportar-fci"]`);
      cy.clickIfExist(`[data-cy="4013629049-executar"]`);
      cy.clickIfExist(`[data-cy="4013629049-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="4013629049-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-executar->2164517070-múltipla seleção->2164517070-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-executar`,`2164517070-múltipla seleção`,`2164517070-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-executar"]`);
      cy.clickIfExist(`[data-cy="2164517070-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2164517070-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/memoria-calculo->2164517070-abrir visualização->2164517070-expandir->2164517070-diminuir`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/memoria-calculo`,`2164517070-abrir visualização`,`2164517070-expandir`,`2164517070-diminuir`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/memoria-calculo"]`);
      cy.clickIfExist(`[data-cy="2164517070-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2164517070-expandir"]`);
      cy.clickIfExist(`[data-cy="2164517070-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao->2832538547-executar->2832538547-múltipla seleção->2832538547-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao`,`2832538547-executar`,`2832538547-múltipla seleção`,`2832538547-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao"]`);
      cy.clickIfExist(`[data-cy="2832538547-executar"]`);
      cy.clickIfExist(`[data-cy="2832538547-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2832538547-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/composicao-estimativa->3114380081-executar->3114380081-múltipla seleção->3114380081-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/composicao-estimativa`,`3114380081-executar`,`3114380081-múltipla seleção`,`3114380081-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/composicao-estimativa"]`);
      cy.clickIfExist(`[data-cy="3114380081-executar"]`);
      cy.clickIfExist(`[data-cy="3114380081-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3114380081-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element conteudo-importacao->conteudo-importacao/relatorios->conteudo-importacao/relatorios/enquadramento-documentos->573119890-executar->573119890-múltipla seleção->573119890-cancelar`, () => {
const actualId = [`root`,`conteudo-importacao`,`conteudo-importacao/relatorios`,`conteudo-importacao/relatorios/enquadramento-documentos`,`573119890-executar`,`573119890-múltipla seleção`,`573119890-cancelar`];
    cy.clickIfExist(`[data-cy="conteudo-importacao"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios"]`);
      cy.clickIfExist(`[data-cy="conteudo-importacao/relatorios/enquadramento-documentos"]`);
      cy.clickIfExist(`[data-cy="573119890-executar"]`);
      cy.clickIfExist(`[data-cy="573119890-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="573119890-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values obrigacoes->obrigacoes/solicitacoes->3611409689-ir para todas as obrigações->3611409689-agendamentos->1557170498-visualização->1557170498-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3611409689-ir para todas as obrigações`,`3611409689-agendamentos`,`1557170498-visualização`,`1557170498-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3611409689-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3611409689-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1557170498-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1557170498-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
