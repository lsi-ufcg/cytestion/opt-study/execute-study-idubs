describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values parametros->parametros/parametros-recebimento->1593746842-novo->3908357007-powerselect-parametroCodigo->3908357007-powerselect-valor and submit`, () => {
    cy.clickCauseExist(`[data-cy="parametros"]`);
    cy.clickCauseExist(`[data-cy="parametros/parametros-recebimento"]`);
    cy.clickCauseExist(`[data-cy="1593746842-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-parametroCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-valor"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
