describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
    const actualId = [`root`, `transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element pedido-compras`, () => {
    const actualId = [`root`, `pedido-compras`];
    cy.clickIfExist(`[data-cy="pedido-compras"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros`, () => {
    const actualId = [`root`, `parametros`];
    cy.clickIfExist(`[data-cy="parametros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
    const actualId = [`root`, `relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
    const actualId = [`root`, `processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
    const actualId = [`root`, `downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/documentos-fiscais`, () => {
    const actualId = [`root`, `transacoes`, `transacoes/documentos-fiscais`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
    cy.clickIfExist(`[data-cy="transacoes/documentos-fiscais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`];
    cy.clickIfExist(`[data-cy="pedido-compras"]`);
    cy.clickIfExist(`[data-cy="pedido-compras/manutencao-pedido-compra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/recebimento-pedido-compra`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/recebimento-pedido-compra`];
    cy.clickIfExist(`[data-cy="pedido-compras"]`);
    cy.clickIfExist(`[data-cy="pedido-compras/recebimento-pedido-compra"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`];
    cy.clickIfExist(`[data-cy="parametros"]`);
    cy.clickIfExist(`[data-cy="parametros/tolerancia-recebimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`];
    cy.clickIfExist(`[data-cy="parametros"]`);
    cy.clickIfExist(`[data-cy="parametros/parametros-recebimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`];
    cy.clickIfExist(`[data-cy="parametros"]`);
    cy.clickIfExist(`[data-cy="parametros/tipo-operacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`];
    cy.clickIfExist(`[data-cy="parametros"]`);
    cy.clickIfExist(`[data-cy="parametros/itens-fornecedor-nop"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/recebimentos-com-autorizacao`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/recebimentos-com-autorizacao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
    cy.clickIfExist(`[data-cy="relatorios/pedidosComprasPendentes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->713806766-power-search-button`, () => {
    const actualId = [`root`, `downloads`, `713806766-power-search-button`];
    cy.visit('http://system-A17/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="713806766-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->713806766-download`, () => {
    const actualId = [`root`, `downloads`, `713806766-download`];
    cy.visit('http://system-A17/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="713806766-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->713806766-detalhes`, () => {
    const actualId = [`root`, `downloads`, `713806766-detalhes`];
    cy.visit('http://system-A17/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="713806766-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element downloads->713806766-excluir`, () => {
    const actualId = [`root`, `downloads`, `713806766-excluir`];
    cy.visit('http://system-A17/downloads?user%7CUsu%C3%A1rio=~eq~SYNCHRO%7C%7CSYNCHRO&removed%7CRemovido=~eq~0%7C%7CN%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="713806766-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`];
    cy.visit('http://system-A17/pedido-compras/manutencao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1987488916-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-power-search-button`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-power-search-button`];
    cy.visit('http://system-A17/pedido-compras/manutencao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1987488916-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/recebimento-pedido-compra->2207174704-novo`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/recebimento-pedido-compra`, `2207174704-novo`];
    cy.visit('http://system-A17/pedido-compras/recebimento?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2207174704-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/recebimento-pedido-compra->2207174704-mais operações`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/recebimento-pedido-compra`, `2207174704-mais operações`];
    cy.visit('http://system-A17/pedido-compras/recebimento?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2207174704-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/recebimento-pedido-compra->2207174704-power-search-button`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/recebimento-pedido-compra`, `2207174704-power-search-button`];
    cy.visit('http://system-A17/pedido-compras/recebimento?informanteEstCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2207174704-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-mais operações`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-mais operações`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-power-search-button`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-excluir`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-excluir`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-novo`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-novo`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-mais operações`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-mais operações`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-power-search-button`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-visualizar/editar`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-visualizar/editar`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-excluir`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-excluir`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-novo`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-novo`];
    cy.visit('http://system-A17/parametros/tipo-operacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2172680575-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-power-search-button`];
    cy.visit('http://system-A17/parametros/tipo-operacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2172680575-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-visualizar/editar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-visualizar/editar`];
    cy.visit('http://system-A17/parametros/tipo-operacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2172680575-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-excluir`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-excluir`];
    cy.visit('http://system-A17/parametros/tipo-operacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2172680575-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-power-search-button`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3863115053-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3863115053-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/recebimentos-com-autorizacao->2473578525-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`, `2473578525-executar`];
    cy.visit('http://system-A17/relatorios/recebimentos-com-autorizacao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2473578525-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/recebimentos-com-autorizacao->2473578525-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`, `2473578525-agendamentos`];
    cy.visit('http://system-A17/relatorios/recebimentos-com-autorizacao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2473578525-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/recebimentos-com-autorizacao->2473578525-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`, `2473578525-power-search-button`];
    cy.visit('http://system-A17/relatorios/recebimentos-com-autorizacao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2473578525-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/recebimentos-com-autorizacao->2473578525-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`, `2473578525-visualização`];
    cy.visit('http://system-A17/relatorios/recebimentos-com-autorizacao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2473578525-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-executar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-executar`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-agendamentos`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-agendamentos`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-power-search-button`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-power-search-button`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-visualização`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-visualização`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-pesquisar`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-pesquisar`];
    cy.visit('http://system-A17/pedido-compras/manutencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2754351317-pesquisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-salvar`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-salvar`];
    cy.visit('http://system-A17/pedido-compras/manutencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2754351317-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-voltar`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-voltar`];
    cy.visit('http://system-A17/pedido-compras/manutencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2754351317-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-input-numeroInterno-2754351317-powerselect-indDtRefCpag-2754351317-input-numeroFornecedor-2754351317-input-situacao-2754351317-powerselect-entregaPfjCodigo-2754351317-checkbox-indCandidato-2754351317-checkbox-indPedidoCompleto-2754351317-powerselect-indmonCodigo-2754351317-powerselect-indRespFrete-2754351317-input-monetary-txConversaoMoedaNac-2754351317-input-monetary-vlFrete-2754351317-input-monetary-vlSeguro-2754351317-input-monetary-vlOda-2754351317-checkbox-indFreteFixo-2754351317-checkbox-indSeguroFixo-2754351317-checkbox-indOdaFixo and submit`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-input-numeroInterno-2754351317-powerselect-indDtRefCpag-2754351317-input-numeroFornecedor-2754351317-input-situacao-2754351317-powerselect-entregaPfjCodigo-2754351317-checkbox-indCandidato-2754351317-checkbox-indPedidoCompleto-2754351317-powerselect-indmonCodigo-2754351317-powerselect-indRespFrete-2754351317-input-monetary-txConversaoMoedaNac-2754351317-input-monetary-vlFrete-2754351317-input-monetary-vlSeguro-2754351317-input-monetary-vlOda-2754351317-checkbox-indFreteFixo-2754351317-checkbox-indSeguroFixo-2754351317-checkbox-indOdaFixo`];
    cy.visit('http://system-A17/pedido-compras/manutencao?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1987488916-novo"]`);
    cy.fillInput(`[data-cy="2754351317-input-numeroInterno"] textarea`, `methodology`);
    cy.fillInputPowerSelect(`[data-cy="2754351317-powerselect-indDtRefCpag"] input`);
    cy.fillInput(`[data-cy="2754351317-input-numeroFornecedor"] textarea`, `Lder`);
    cy.fillInput(`[data-cy="2754351317-input-situacao"] textarea`, `Amap`);
    cy.fillInputPowerSelect(`[data-cy="2754351317-powerselect-entregaPfjCodigo"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2754351317-checkbox-indCandidato"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2754351317-checkbox-indPedidoCompleto"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="2754351317-powerselect-indmonCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2754351317-powerselect-indRespFrete"] input`);
    cy.fillInput(`[data-cy="2754351317-input-monetary-txConversaoMoedaNac"] textarea`, `5`);
    cy.fillInput(`[data-cy="2754351317-input-monetary-vlFrete"] textarea`, `9`);
    cy.fillInput(`[data-cy="2754351317-input-monetary-vlSeguro"] textarea`, `5`);
    cy.fillInput(`[data-cy="2754351317-input-monetary-vlOda"] textarea`, `2`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2754351317-checkbox-indFreteFixo"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2754351317-checkbox-indSeguroFixo"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2754351317-checkbox-indOdaFixo"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/recebimento-pedido-compra->2207174704-novo->3387281081-fechar`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/recebimento-pedido-compra`, `2207174704-novo`, `3387281081-fechar`];
    cy.visit('http://system-A17/pedido-compras/recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3387281081-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-pesquisar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-pesquisar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2509216313-pesquisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-salvar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2509216313-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-voltar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2509216313-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-powerselect-estCodigo-2509216313-powerselect-mpsTipo-2509216313-input-monetary-percTolValorAMais-2509216313-input-monetary-vlTolValorAMais-2509216313-input-monetary-percTolValorAMenos-2509216313-input-monetary-vlTolValorAMenos-2509216313-input-monetary-percTolQtdAMais-2509216313-input-monetary-vlTolQtdAMais-2509216313-input-monetary-percTolQtdAMenos-2509216313-input-monetary-vlTolQtdAMenos-2509216313-input-monetary-percTolFSOdaAMais-2509216313-input-monetary-vlTolFSOdaAMais-2509216313-input-monetary-percTolFSOdaAMenos-2509216313-input-monetary-vlTolFSOdaAMenos-2509216313-input-number-vlTolDiasAMais-2509216313-input-number-vlTolDiasAMenos-2509216313-checkbox-indVerificaCpag and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-powerselect-estCodigo-2509216313-powerselect-mpsTipo-2509216313-input-monetary-percTolValorAMais-2509216313-input-monetary-vlTolValorAMais-2509216313-input-monetary-percTolValorAMenos-2509216313-input-monetary-vlTolValorAMenos-2509216313-input-monetary-percTolQtdAMais-2509216313-input-monetary-vlTolQtdAMais-2509216313-input-monetary-percTolQtdAMenos-2509216313-input-monetary-vlTolQtdAMenos-2509216313-input-monetary-percTolFSOdaAMais-2509216313-input-monetary-vlTolFSOdaAMais-2509216313-input-monetary-percTolFSOdaAMenos-2509216313-input-monetary-vlTolFSOdaAMenos-2509216313-input-number-vlTolDiasAMais-2509216313-input-number-vlTolDiasAMenos-2509216313-checkbox-indVerificaCpag`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2509216313-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2509216313-powerselect-mpsTipo"] input`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-percTolValorAMais"] textarea`, `9`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-vlTolValorAMais"] textarea`, `10`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-percTolValorAMenos"] textarea`, `5`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-vlTolValorAMenos"] textarea`, `7`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-percTolQtdAMais"] textarea`, `3`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-vlTolQtdAMais"] textarea`, `10`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-percTolQtdAMenos"] textarea`, `9`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-vlTolQtdAMenos"] textarea`, `5`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-percTolFSOdaAMais"] textarea`, `1`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-vlTolFSOdaAMais"] textarea`, `2`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-percTolFSOdaAMenos"] textarea`, `1`);
    cy.fillInput(`[data-cy="2509216313-input-monetary-vlTolFSOdaAMenos"] textarea`, `2`);
    cy.fillInput(`[data-cy="2509216313-input-number-vlTolDiasAMais"] textarea`, `9`);
    cy.fillInput(`[data-cy="2509216313-input-number-vlTolDiasAMenos"] textarea`, `8`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2509216313-checkbox-indVerificaCpag"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-mais operações->3906196656-item-copiar tolerâncias`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-mais operações`, `3906196656-item-copiar tolerâncias`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-mais operações"]`);
    cy.clickIfExist(`[data-cy="3906196656-item-copiar tolerâncias"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-pesquisar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-pesquisar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-pesquisar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-remover item`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-remover item`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-salvar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-voltar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-powerselect-mpsTipo-2913221952-input-monetary-percTolValorAMais-2913221952-input-monetary-vlTolValorAMais-2913221952-input-monetary-percTolValorAMenos-2913221952-input-monetary-vlTolValorAMenos-2913221952-input-monetary-percTolQtdAMais-2913221952-input-monetary-vlTolQtdAMais-2913221952-input-monetary-percTolQtdAMenos-2913221952-input-monetary-vlTolQtdAMenos-2913221952-input-monetary-percTolFSOdaAMais-2913221952-input-monetary-vlTolFSOdaAMais-2913221952-input-monetary-percTolFSOdaAMenos-2913221952-input-monetary-vlTolFSOdaAMenos-2913221952-input-number-vlTolDiasAMais-2913221952-input-number-vlTolDiasAMenos-2913221952-checkbox-indVerificaCpag and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-powerselect-mpsTipo-2913221952-input-monetary-percTolValorAMais-2913221952-input-monetary-vlTolValorAMais-2913221952-input-monetary-percTolValorAMenos-2913221952-input-monetary-vlTolValorAMenos-2913221952-input-monetary-percTolQtdAMais-2913221952-input-monetary-vlTolQtdAMais-2913221952-input-monetary-percTolQtdAMenos-2913221952-input-monetary-vlTolQtdAMenos-2913221952-input-monetary-percTolFSOdaAMais-2913221952-input-monetary-vlTolFSOdaAMais-2913221952-input-monetary-percTolFSOdaAMenos-2913221952-input-monetary-vlTolFSOdaAMenos-2913221952-input-number-vlTolDiasAMais-2913221952-input-number-vlTolDiasAMenos-2913221952-checkbox-indVerificaCpag`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3906196656-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="2913221952-powerselect-mpsTipo"] input`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-percTolValorAMais"] textarea`, `7`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-vlTolValorAMais"] textarea`, `10`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-percTolValorAMenos"] textarea`, `9`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-vlTolValorAMenos"] textarea`, `9`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-percTolQtdAMais"] textarea`, `4`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-vlTolQtdAMais"] textarea`, `9`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-percTolQtdAMenos"] textarea`, `10`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-vlTolQtdAMenos"] textarea`, `2`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-percTolFSOdaAMais"] textarea`, `7`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-vlTolFSOdaAMais"] textarea`, `7`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-percTolFSOdaAMenos"] textarea`, `5`);
    cy.fillInput(`[data-cy="2913221952-input-monetary-vlTolFSOdaAMenos"] textarea`, `10`);
    cy.fillInput(`[data-cy="2913221952-input-number-vlTolDiasAMais"] textarea`, `10`);
    cy.fillInput(`[data-cy="2913221952-input-number-vlTolDiasAMenos"] textarea`, `2`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2913221952-checkbox-indVerificaCpag"] textarea`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-novo->3908357007-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-novo`, `3908357007-salvar`];
    cy.visit('http://system-A17/parametros/parametros-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3908357007-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-novo->3908357007-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-novo`, `3908357007-voltar`];
    cy.visit('http://system-A17/parametros/parametros-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3908357007-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/parametros-recebimento->1593746842-novo->3908357007-powerselect-parametroCodigo and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-novo`, `3908357007-powerselect-parametroCodigo`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-parametroCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-mais operações->1593746842-item-copiar parâmetros`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-mais operações`, `1593746842-item-copiar parâmetros`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-mais operações"]`);
    cy.clickIfExist(`[data-cy="1593746842-item-copiar parâmetros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-visualizar/editar->522315669-remover item`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-visualizar/editar`, `522315669-remover item`];
    cy.visit('http://system-A17/parametros/parametros-recebimento/editar/RPD_DATA_EMISSAO/AAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="522315669-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-visualizar/editar->522315669-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-visualizar/editar`, `522315669-salvar`];
    cy.visit('http://system-A17/parametros/parametros-recebimento/editar/RPD_DATA_EMISSAO/AAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="522315669-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-visualizar/editar->522315669-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-visualizar/editar`, `522315669-voltar`];
    cy.visit('http://system-A17/parametros/parametros-recebimento/editar/RPD_DATA_EMISSAO/AAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="522315669-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/parametros-recebimento->1593746842-visualizar/editar->522315669-powerselect-valor and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-visualizar/editar`, `522315669-powerselect-valor`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="522315669-powerselect-valor"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-novo->3294928586-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-novo`, `3294928586-salvar`];
    cy.visit('http://system-A17/parametros/tipo-operacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3294928586-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-novo->3294928586-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-novo`, `3294928586-voltar`];
    cy.visit('http://system-A17/parametros/tipo-operacao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="3294928586-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/tipo-operacao->2172680575-novo->3294928586-input-number-codigo-3294928586-input-titulo-3294928586-input-descricao and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-novo`, `3294928586-input-number-codigo-3294928586-input-titulo-3294928586-input-descricao`];
    cy.visit('http://system-A17/parametros/tipo-operacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2172680575-novo"]`);
    cy.fillInput(`[data-cy="3294928586-input-number-codigo"] textarea`, `9`);
    cy.fillInput(`[data-cy="3294928586-input-titulo"] textarea`, `Books`);
    cy.fillInput(`[data-cy="3294928586-input-descricao"] textarea`, `Movies`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-visualizar/editar->940429585-remover item`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-visualizar/editar`, `940429585-remover item`];
    cy.visit('http://system-A17/parametros/tipo-operacao/editar/9');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="940429585-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-visualizar/editar->940429585-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-visualizar/editar`, `940429585-salvar`];
    cy.visit('http://system-A17/parametros/tipo-operacao/editar/9');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="940429585-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tipo-operacao->2172680575-visualizar/editar->940429585-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-visualizar/editar`, `940429585-voltar`];
    cy.visit('http://system-A17/parametros/tipo-operacao/editar/9');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="940429585-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/tipo-operacao->2172680575-visualizar/editar->940429585-input-titulo-940429585-input-descricao and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/tipo-operacao`, `2172680575-visualizar/editar`, `940429585-input-titulo-940429585-input-descricao`];
    cy.visit('http://system-A17/parametros/tipo-operacao');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2172680575-visualizar/editar"]`);
    cy.fillInput(`[data-cy="940429585-input-titulo"] textarea`, `product`);
    cy.fillInput(`[data-cy="940429585-input-descricao"] textarea`, `Facilitador`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4194398979-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-power-search-button`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4194398979-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-visualizar/editar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-visualizar/editar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4194398979-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-excluir`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-excluir`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4194398979-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/recebimentos-com-autorizacao->2473578525-agendamentos->2473578525-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`, `2473578525-agendamentos`, `2473578525-voltar`];
    cy.visit('http://system-A17/relatorios/recebimentos-com-autorizacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~244076D%7C%7C244076&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2473578525-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/recebimentos-com-autorizacao->2473578525-visualização->2473578525-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/recebimentos-com-autorizacao`, `2473578525-visualização`, `2473578525-item-`];
    cy.visit('http://system-A17/relatorios/recebimentos-com-autorizacao?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2473578525-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2473578525-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-executar->4127273427-múltipla seleção`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-executar`, `4127273427-múltipla seleção`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-executar"]`);
    cy.clickIfExist(`[data-cy="4127273427-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-executar->4127273427-agendar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-executar`, `4127273427-agendar`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-executar"]`);
    cy.clickIfExist(`[data-cy="4127273427-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-agendamentos->4127273427-voltar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-agendamentos`, `4127273427-voltar`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~4856981D%7C%7C4856981&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values relatorios->relatorios/pedidosComprasPendentes->4127273427-visualização->4127273427-item- and submit`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-visualização`, `4127273427-item-`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4127273427-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-pesquisar->2754351317-power-search-button`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-pesquisar`, `2754351317-power-search-button`];
    cy.visit('http://system-A17/pedido-compras/manutencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2754351317-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2754351317-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-pesquisar->2754351317-cancelar`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-pesquisar`, `2754351317-cancelar`];
    cy.visit('http://system-A17/pedido-compras/manutencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2754351317-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2754351317-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element pedido-compras->pedido-compras/manutencao-pedido-compra->1987488916-novo->2754351317-pesquisar->2754351317-limpar`, () => {
    const actualId = [`root`, `pedido-compras`, `pedido-compras/manutencao-pedido-compra`, `1987488916-novo`, `2754351317-pesquisar`, `2754351317-limpar`];
    cy.visit('http://system-A17/pedido-compras/manutencao/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2754351317-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2754351317-limpar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-pesquisar->2509216313-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-pesquisar`, `2509216313-power-search-button`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2509216313-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2509216313-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-pesquisar->2509216313-cancelar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-pesquisar`, `2509216313-cancelar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2509216313-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2509216313-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-novo->2509216313-pesquisar->2509216313-limpar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-novo`, `2509216313-pesquisar`, `2509216313-limpar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2509216313-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2509216313-limpar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-pesquisar->2913221952-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-pesquisar`, `2913221952-power-search-button`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2913221952-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-pesquisar->2913221952-cancelar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-pesquisar`, `2913221952-cancelar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2913221952-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/tolerancia-recebimento->3906196656-visualizar/editar->2913221952-pesquisar->2913221952-limpar`, () => {
    const actualId = [`root`, `parametros`, `parametros/tolerancia-recebimento`, `3906196656-visualizar/editar`, `2913221952-pesquisar`, `2913221952-limpar`];
    cy.visit('http://system-A17/parametros/tolerancia-recebimento/editar/9315514');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="2913221952-pesquisar"]`);
    cy.clickIfExist(`[data-cy="2913221952-limpar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/parametros-recebimento->1593746842-novo->3908357007-powerselect-parametroCodigo->3908357007-powerselect-valor and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-novo`, `3908357007-powerselect-parametroCodigo`, `3908357007-powerselect-valor`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-parametroCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-valor"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-mais operações->1593746842-item-copiar parâmetros->1593746842- cancelar `, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-mais operações`, `1593746842-item-copiar parâmetros`, `1593746842- cancelar `];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-mais operações"]`);
    cy.clickIfExist(`[data-cy="1593746842-item-copiar parâmetros"]`);
    cy.clickIfExist(`[data-cy="1593746842- cancelar "]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/parametros-recebimento->1593746842-mais operações->1593746842-item-copiar parâmetros->1593746842-copiar`, () => {
    const actualId = [`root`, `parametros`, `parametros/parametros-recebimento`, `1593746842-mais operações`, `1593746842-item-copiar parâmetros`, `1593746842-copiar`];
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1593746842-mais operações"]`);
    cy.clickIfExist(`[data-cy="1593746842-item-copiar parâmetros"]`);
    cy.clickIfExist(`[data-cy="1593746842-copiar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-salvar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/nop-x-mercadoria/ESTAB-2/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1185951701-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-voltar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/nop-x-mercadoria/ESTAB-2/novo');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1185951701-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-powerselect-nopCodigo-1185951701-powerselect-mercCodigo and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-powerselect-nopCodigo-1185951701-powerselect-mercCodigo`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4194398979-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1185951701-powerselect-nopCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="1185951701-powerselect-mercCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-visualizar/editar->1133325788-remover item`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-visualizar/editar`, `1133325788-remover item`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/nop-x-mercadoria/ESTAB-2/editar/58352495');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1133325788-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-visualizar/editar->1133325788-salvar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-visualizar/editar`, `1133325788-salvar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/nop-x-mercadoria/ESTAB-2/editar/58352495');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1133325788-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-visualizar/editar->1133325788-voltar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-visualizar/editar`, `1133325788-voltar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/nop-x-mercadoria/ESTAB-2/editar/58352495');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1133325788-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Filling values parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-visualizar/editar->1133325788-powerselect-mercCodigo and submit`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-visualizar/editar`, `1133325788-powerselect-mercCodigo`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4194398979-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1133325788-powerselect-mercCodigo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it.skip(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-executar->4127273427-múltipla seleção->4127273427-cancelar`, () => {
    const actualId = [`root`, `relatorios`, `relatorios/pedidosComprasPendentes`, `4127273427-executar`, `4127273427-múltipla seleção`, `4127273427-cancelar`];
    cy.visit('http://system-A17/relatorios/pedidosComprasPendentes?usuario=~eq~SYNCHRO%7C%7CSYNCHRO');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="4127273427-executar"]`);
    cy.clickIfExist(`[data-cy="4127273427-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="4127273427-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-voltar->1673481282-novo`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-voltar`, `1673481282-novo`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2/m');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673481282-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-voltar->1673481282-power-search-button`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-voltar`, `1673481282-power-search-button`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2/m');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673481282-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-voltar->1673481282-visualizar/editar`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-voltar`, `1673481282-visualizar/editar`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2/m');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673481282-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
  it(`Click on element parametros->parametros/itens-fornecedor-nop->3863115053-visualizar/editar->4194398979-novo->1185951701-voltar->1673481282-excluir`, () => {
    const actualId = [`root`, `parametros`, `parametros/itens-fornecedor-nop`, `3863115053-visualizar/editar`, `4194398979-novo`, `1185951701-voltar`, `1673481282-excluir`];
    cy.visit('http://system-A17/parametros/itens-fornecedor-nop/ESTAB-2/m');
    cy.waitNetworkFinished();
    cy.clickIfExist(`[data-cy="1673481282-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });//--CODE--
});
