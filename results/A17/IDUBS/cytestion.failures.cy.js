describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element relatorios->relatorios/recebimentos-com-autorizacao->2473578525-agendamentos->2473578525-voltar`, () => {
    cy.visit(
      'http://system-A17/relatorios/recebimentos-com-autorizacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~244076D%7C%7C244076&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2473578525-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/pedidosComprasPendentes->4127273427-agendamentos->4127273427-voltar`, () => {
    cy.visit(
      'http://system-A17/relatorios/pedidosComprasPendentes?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~4856981D%7C%7C4856981&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="4127273427-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values parametros->parametros/parametros-recebimento->1593746842-novo->3908357007-powerselect-parametroCodigo->3908357007-powerselect-valor and submit`, () => {
    cy.visit('http://system-A17/parametros/parametros-recebimento?estCodigo=~eq~AAA_DF%7C%7CAAA_DF');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1593746842-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-parametroCodigo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.fillInputPowerSelect(`[data-cy="3908357007-powerselect-valor"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
