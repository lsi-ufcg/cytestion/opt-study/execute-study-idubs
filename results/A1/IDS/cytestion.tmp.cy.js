describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
const actualId = [`root`,`tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
const actualId = [`root`,`tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabil`, () => {
const actualId = [`root`,`contabil`];
    cy.clickIfExist(`[data-cy="contabil"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo`, () => {
const actualId = [`root`,`ativo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao`, () => {
const actualId = [`root`,`escrituracao-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes`, () => {
const actualId = [`root`,`integracoes`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/fiscais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/mercadorias`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/parametros-gerais`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/parametros-gerais`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametros-gerais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscais`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/fiscais`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/cadastro-prestacoes`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/cadastro-prestacoes`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/cadastro-prestacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabil->contabil/plano-contas`, () => {
const actualId = [`root`,`contabil`,`contabil/plano-contas`];
    cy.clickIfExist(`[data-cy="contabil"]`);
      cy.clickIfExist(`[data-cy="contabil/plano-contas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element contabil->contabil/centro-custo`, () => {
const actualId = [`root`,`contabil`,`contabil/centro-custo`];
    cy.clickIfExist(`[data-cy="contabil"]`);
      cy.clickIfExist(`[data-cy="contabil/centro-custo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/dof`, () => {
const actualId = [`root`,`transacoes`,`transacoes/dof`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/parametrizacao-de-regras`, () => {
const actualId = [`root`,`escrituracao-apuracao`,`escrituracao-apuracao/parametrizacao-de-regras`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
      cy.clickIfExist(`[data-cy="escrituracao-apuracao/parametrizacao-de-regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao`, () => {
const actualId = [`root`,`escrituracao-apuracao`,`escrituracao-apuracao/lancamento-apuracao`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
      cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/visualizacao`, () => {
const actualId = [`root`,`integracoes`,`integracoes/visualizacao`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/visualizacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element integracoes->integracoes/executar-interfaces`, () => {
const actualId = [`root`,`integracoes`,`integracoes/executar-interfaces`];
    cy.clickIfExist(`[data-cy="integracoes"]`);
      cy.clickIfExist(`[data-cy="integracoes/executar-interfaces"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares`, () => {
const actualId = [`root`,`processos`,`processos/regulares`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado`, () => {
const actualId = [`root`,`processos`,`processos/avancado`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1851087535-power-search-button`, () => {
const actualId = [`root`,`download`,`1851087535-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1851087535-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1851087535-download`, () => {
const actualId = [`root`,`download`,`1851087535-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1851087535-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1851087535-detalhes`, () => {
const actualId = [`root`,`download`,`1851087535-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1851087535-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->1851087535-excluir`, () => {
const actualId = [`root`,`download`,`1851087535-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="1851087535-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/fiscais->tabelas-oficiais/fiscais/cfop`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/fiscais`,`tabelas-oficiais/fiscais/cfop`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/fiscais/cfop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/mercadorias->tabelas-oficiais/mercadorias/ncm`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/mercadorias`,`tabelas-oficiais/mercadorias/ncm`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/mercadorias/ncm"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/parametros-gerais->tabelas-corporativas/parametros-gerais/parametrizacao-geral`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/parametros-gerais`,`tabelas-corporativas/parametros-gerais/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametros-gerais"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametros-gerais/parametrizacao-geral"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscais->tabelas-corporativas/fiscais/nop`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/fiscais`,`tabelas-corporativas/fiscais/nop`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais/nop"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/fiscais->tabelas-corporativas/fiscais/edof`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/fiscais`,`tabelas-corporativas/fiscais/edof`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/fiscais/edof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/pfj`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pfj`,`tabelas-corporativas/pfj/pfj`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/pfj"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/pfj->tabelas-corporativas/pfj/hierarquia-pessoas`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/pfj`,`tabelas-corporativas/pfj/hierarquia-pessoas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/pfj/hierarquia-pessoas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/mercadorias->tabelas-corporativas/mercadorias/cadastro`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/mercadorias`,`tabelas-corporativas/mercadorias/cadastro`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/mercadorias/cadastro"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/ativacao`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/ativacao`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/ativacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/geracao-controle`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/geracao-controle`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/geracao-controle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/parametrizacao-calculo-credito`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element escrituracao-apuracao->escrituracao-apuracao/lancamento-apuracao->escrituracao-apuracao/lancamento-apuracao/visualizacao-lancamento`, () => {
const actualId = [`root`,`escrituracao-apuracao`,`escrituracao-apuracao/lancamento-apuracao`,`escrituracao-apuracao/lancamento-apuracao/visualizacao-lancamento`];
    cy.clickIfExist(`[data-cy="escrituracao-apuracao"]`);
      cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao"]`);
      cy.clickIfExist(`[data-cy="escrituracao-apuracao/lancamento-apuracao/visualizacao-lancamento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->3727925183-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`3727925183-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3727925183-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->3727925183-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`3727925183-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3727925183-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->3727925183-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`3727925183-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3727925183-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-novo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-novo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-visualizar/editar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-visualizar/editar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-excluir`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-excluir`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-moreoutlined`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-moreoutlined`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-moreoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-novo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-novo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-itens`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-itens`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-itens"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-excluir`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-excluir`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/ativacao->1448035828-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/ativacao`,`1448035828-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/ativacao"]`);
      cy.clickIfExist(`[data-cy="1448035828-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-excluir`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-excluir`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/geracao-controle->1761007042-plusoutlined`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/geracao-controle`,`1761007042-plusoutlined`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/geracao-controle"]`);
      cy.clickIfExist(`[data-cy="1761007042-plusoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/geracao-controle->1761007042-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/geracao-controle`,`1761007042-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/geracao-controle"]`);
      cy.clickIfExist(`[data-cy="1761007042-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-novo`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/parametrizacao-calculo-credito`,`4133865625-novo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
      cy.clickIfExist(`[data-cy="4133865625-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/parametrizacao-calculo-credito`,`4133865625-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
      cy.clickIfExist(`[data-cy="4133865625-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-gerenciar labels->370329959-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-gerenciar labels`,`370329959-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="370329959-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-visualizar/editar->3080639876-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-visualizar/editar`,`3080639876-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3080639876-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->370329959-visualizar/editar->3080639876-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`370329959-visualizar/editar`,`3080639876-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="370329959-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3080639876-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-novo->2872936343-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-novo`,`2872936343-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-novo"]`);
      cy.clickIfExist(`[data-cy="2872936343-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-novo->2872936343-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-novo`,`2872936343-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-novo"]`);
      cy.clickIfExist(`[data-cy="2872936343-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->2872936343-novo->2872936343-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-novo`,`2872936343-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-novo"]`);
      cy.fillInput(`[data-cy="2872936343-input-number-ano"] textarea`, `8`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-editar->2872936343-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-editar`,`2872936343-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-editar"]`);
      cy.clickIfExist(`[data-cy="2872936343-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2872936343-editar->2872936343-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2872936343-editar`,`2872936343-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2872936343-editar"]`);
      cy.clickIfExist(`[data-cy="2872936343-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->3727925183-novo->3727925183-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`3727925183-novo`,`3727925183-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="3727925183-novo"]`);
      cy.clickIfExist(`[data-cy="3727925183-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-visualização->4095575583-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-visualização`,`4095575583-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4095575583-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-abrir visualização->4095575583-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-abrir visualização`,`4095575583-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4095575583-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-abrir visualização->4095575583-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-abrir visualização`,`4095575583-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4095575583-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-abrir visualização->4095575583-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-abrir visualização`,`4095575583-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4095575583-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-abrir visualização->4095575583-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-abrir visualização`,`4095575583-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4095575583-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-visualizar->4095575583-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-visualizar`,`4095575583-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-visualizar"]`);
      cy.clickIfExist(`[data-cy="4095575583-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-executar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-visualização`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-executar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-visualização`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-executar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-visualização`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-regerar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-detalhes`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-abrir visualização`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-excluir`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-executar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-visualização`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-executar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-visualização`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-executar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-visualização`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-executar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-visualização`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-novo->2562154495-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-novo`,`2562154495-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-novo"]`);
      cy.clickIfExist(`[data-cy="2562154495-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-novo->2562154495-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-novo`,`2562154495-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-novo"]`);
      cy.clickIfExist(`[data-cy="2562154495-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/controles->ativo/controles/manutencao->1798114090-novo->2562154495-input-bemCodigo-2562154495-input-numPlaqueta-2562154495-input-projeto-2562154495-input-seqIncorporacao-2562154495-textarea-descricao-2562154495-textarea-funcao-2562154495-powerselect-dfltNbmCodigo-2562154495-powerselect-identBemImob-2562154495-input-number-vidaUtil-2562154495-input-monetary-qtd-2562154495-checkbox-bemAtivCirculante-2562154495-powerselect-codCtaStore-2562154495-powerselect-codCtaDeprStore-2562154495-powerselect-codCcus and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-novo`,`2562154495-input-bemCodigo-2562154495-input-numPlaqueta-2562154495-input-projeto-2562154495-input-seqIncorporacao-2562154495-textarea-descricao-2562154495-textarea-funcao-2562154495-powerselect-dfltNbmCodigo-2562154495-powerselect-identBemImob-2562154495-input-number-vidaUtil-2562154495-input-monetary-qtd-2562154495-checkbox-bemAtivCirculante-2562154495-powerselect-codCtaStore-2562154495-powerselect-codCtaDeprStore-2562154495-powerselect-codCcus`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-novo"]`);
      cy.fillInput(`[data-cy="2562154495-input-bemCodigo"] textarea`, `247`);
cy.fillInput(`[data-cy="2562154495-input-numPlaqueta"] textarea`, `Lari`);
cy.fillInput(`[data-cy="2562154495-input-projeto"] textarea`, `So Paulo`);
cy.fillInput(`[data-cy="2562154495-input-seqIncorporacao"] textarea`, `black`);
cy.fillInput(`[data-cy="2562154495-textarea-descricao"] input`, `Sleek Metal Pants`);
cy.fillInput(`[data-cy="2562154495-textarea-funcao"] input`, `withdrawal`);
cy.fillInputPowerSelect(`[data-cy="2562154495-powerselect-dfltNbmCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2562154495-powerselect-identBemImob"] input`);
cy.fillInput(`[data-cy="2562154495-input-number-vidaUtil"] textarea`, `2`);
cy.fillInput(`[data-cy="2562154495-input-monetary-qtd"] textarea`, `9`);
cy.fillInputCheckboxOrRadio(`[data-cy="2562154495-checkbox-bemAtivCirculante"] textarea`);
cy.fillInputPowerSelect(`[data-cy="2562154495-powerselect-codCtaStore"] input`);
cy.fillInputPowerSelect(`[data-cy="2562154495-powerselect-codCtaDeprStore"] input`);
cy.fillInputPowerSelect(`[data-cy="2562154495-powerselect-codCcus"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-visualizar/editar->2369132166-remover item`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-visualizar/editar`,`2369132166-remover item`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2369132166-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-visualizar/editar->2369132166-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-visualizar/editar`,`2369132166-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2369132166-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-visualizar/editar->2369132166-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-visualizar/editar`,`2369132166-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="2369132166-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/controles->ativo/controles/manutencao->1798114090-visualizar/editar->2369132166-input-bemCodigo-2369132166-input-numPlaqueta-2369132166-input-projeto-2369132166-input-seqIncorporacao-2369132166-textarea-descricao-2369132166-textarea-funcao-2369132166-powerselect-dfltNbmCodigo-2369132166-powerselect-identBemImob-2369132166-input-number-vidaUtil-2369132166-input-monetary-qtd-2369132166-checkbox-bemAtivCirculante-2369132166-powerselect-codCtaStore-2369132166-powerselect-codCtaDeprStore-2369132166-powerselect-codCcus and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-visualizar/editar`,`2369132166-input-bemCodigo-2369132166-input-numPlaqueta-2369132166-input-projeto-2369132166-input-seqIncorporacao-2369132166-textarea-descricao-2369132166-textarea-funcao-2369132166-powerselect-dfltNbmCodigo-2369132166-powerselect-identBemImob-2369132166-input-number-vidaUtil-2369132166-input-monetary-qtd-2369132166-checkbox-bemAtivCirculante-2369132166-powerselect-codCtaStore-2369132166-powerselect-codCtaDeprStore-2369132166-powerselect-codCcus`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-visualizar/editar"]`);
      cy.fillInput(`[data-cy="2369132166-input-bemCodigo"] textarea`, `Industrial`);
cy.fillInput(`[data-cy="2369132166-input-numPlaqueta"] textarea`, `Hong Kong Dollar`);
cy.fillInput(`[data-cy="2369132166-input-projeto"] textarea`, `Fresh`);
cy.fillInput(`[data-cy="2369132166-input-seqIncorporacao"] textarea`, `Kuwaiti Dinar`);
cy.fillInput(`[data-cy="2369132166-textarea-descricao"] input`, `vortals`);
cy.fillInput(`[data-cy="2369132166-textarea-funcao"] input`, `Lder`);
cy.fillInputPowerSelect(`[data-cy="2369132166-powerselect-dfltNbmCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2369132166-powerselect-identBemImob"] input`);
cy.fillInput(`[data-cy="2369132166-input-number-vidaUtil"] textarea`, `5`);
cy.fillInput(`[data-cy="2369132166-input-monetary-qtd"] textarea`, `8`);
cy.fillInputCheckboxOrRadio(`[data-cy="2369132166-checkbox-bemAtivCirculante"] textarea`);
cy.fillInputPowerSelect(`[data-cy="2369132166-powerselect-codCtaStore"] input`);
cy.fillInputPowerSelect(`[data-cy="2369132166-powerselect-codCtaDeprStore"] input`);
cy.fillInputPowerSelect(`[data-cy="2369132166-powerselect-codCcus"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-moreoutlined->1798114090-item-`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-moreoutlined`,`1798114090-item-`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-moreoutlined"]`);
      cy.clickIfExist(`[data-cy="1798114090-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-novo->1892375536-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-novo`,`1892375536-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-novo"]`);
      cy.clickIfExist(`[data-cy="1892375536-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-novo->1892375536-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-novo`,`1892375536-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-novo"]`);
      cy.clickIfExist(`[data-cy="1892375536-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-novo->1892375536-powerselect-edofCodigo-1892375536-input-serieSubserie-1892375536-input-numero-1892375536-input-nfeLocalizador-1892375536-powerselect-emitentePfjCodigo-1892375536-powerselect-emitenteLocCodigo-1892375536-powerselect-remetentePfjCodigo-1892375536-powerselect-remetenteLocCodigo-1892375536-powerselect-destinatarioPfjCodigo-1892375536-powerselect-destinatarioLocCodigo-1892375536-powerselect-cfopCodigo-1892375536-powerselect-nopCodigo-1892375536-powerselect-ctrlTipoGeracao and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-novo`,`1892375536-powerselect-edofCodigo-1892375536-input-serieSubserie-1892375536-input-numero-1892375536-input-nfeLocalizador-1892375536-powerselect-emitentePfjCodigo-1892375536-powerselect-emitenteLocCodigo-1892375536-powerselect-remetentePfjCodigo-1892375536-powerselect-remetenteLocCodigo-1892375536-powerselect-destinatarioPfjCodigo-1892375536-powerselect-destinatarioLocCodigo-1892375536-powerselect-cfopCodigo-1892375536-powerselect-nopCodigo-1892375536-powerselect-ctrlTipoGeracao`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="1892375536-input-serieSubserie"] textarea`, `Music`);
cy.fillInput(`[data-cy="1892375536-input-numero"] textarea`, `Roraima`);
cy.fillInput(`[data-cy="1892375536-input-nfeLocalizador"] textarea`, `turnkey`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-emitentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-emitenteLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-remetentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-remetenteLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-destinatarioPfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-destinatarioLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-nopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1892375536-powerselect-ctrlTipoGeracao"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-itens->4219030487-novo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-itens`,`4219030487-novo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-itens"]`);
      cy.clickIfExist(`[data-cy="4219030487-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-itens->4219030487-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-itens`,`4219030487-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-itens"]`);
      cy.clickIfExist(`[data-cy="4219030487-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar->1407162807-mais operações`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`,`1407162807-mais operações`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1407162807-mais operações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar->1407162807-remover item`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`,`1407162807-remover item`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1407162807-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar->1407162807-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`,`1407162807-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1407162807-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar->1407162807-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`,`1407162807-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1407162807-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar->1407162807-powerselect-edofCodigo-1407162807-input-serieSubserie-1407162807-input-numero-1407162807-input-nfeLocalizador-1407162807-powerselect-emitentePfjCodigo-1407162807-powerselect-emitenteLocCodigo-1407162807-powerselect-remetentePfjCodigo-1407162807-powerselect-remetenteLocCodigo-1407162807-powerselect-destinatarioPfjCodigo-1407162807-powerselect-destinatarioLocCodigo-1407162807-powerselect-cfopCodigo-1407162807-powerselect-nopCodigo and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`,`1407162807-powerselect-edofCodigo-1407162807-input-serieSubserie-1407162807-input-numero-1407162807-input-nfeLocalizador-1407162807-powerselect-emitentePfjCodigo-1407162807-powerselect-emitenteLocCodigo-1407162807-powerselect-remetentePfjCodigo-1407162807-powerselect-remetenteLocCodigo-1407162807-powerselect-destinatarioPfjCodigo-1407162807-powerselect-destinatarioLocCodigo-1407162807-powerselect-cfopCodigo-1407162807-powerselect-nopCodigo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="1407162807-input-serieSubserie"] textarea`, `Acre`);
cy.fillInput(`[data-cy="1407162807-input-numero"] textarea`, `Visionary`);
cy.fillInput(`[data-cy="1407162807-input-nfeLocalizador"] textarea`, `THX`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-emitentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-emitenteLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-remetentePfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-remetenteLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-destinatarioPfjCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-destinatarioLocCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="1407162807-powerselect-nopCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-mais operações`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-mais operações`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-mais operações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-consultar dof`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-consultar dof`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-consultar dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-powerselect-ctrlOperacao-655170588-powerselect-ctrlNotaFiscal-655170588-input-motivoBaixa-655170588-powerselect-edofCodigo-655170588-input-serieSubserie-655170588-input-numero and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-powerselect-ctrlOperacao-655170588-powerselect-ctrlNotaFiscal-655170588-input-motivoBaixa-655170588-powerselect-edofCodigo-655170588-input-serieSubserie-655170588-input-numero`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="655170588-powerselect-ctrlOperacao"] input`);
cy.fillInputPowerSelect(`[data-cy="655170588-powerselect-ctrlNotaFiscal"] input`);
cy.fillInput(`[data-cy="655170588-input-motivoBaixa"] textarea`, `transmitter`);
cy.fillInputPowerSelect(`[data-cy="655170588-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="655170588-input-serieSubserie"] textarea`, `circuit`);
cy.fillInput(`[data-cy="655170588-input-numero"] textarea`, `Integrao`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar->1499695331-mais operações`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`,`1499695331-mais operações`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1499695331-mais operações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar->1499695331-remover item`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`,`1499695331-remover item`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1499695331-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar->1499695331-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`,`1499695331-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1499695331-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar->1499695331-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`,`1499695331-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1499695331-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar->1499695331-powerselect-ctrlOperacao-1499695331-powerselect-ctrlNotaFiscal-1499695331-input-motivoBaixa and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`,`1499695331-powerselect-ctrlOperacao-1499695331-powerselect-ctrlNotaFiscal-1499695331-input-motivoBaixa`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.fillInputPowerSelect(`[data-cy="1499695331-powerselect-ctrlOperacao"] input`);
cy.fillInputPowerSelect(`[data-cy="1499695331-powerselect-ctrlNotaFiscal"] input`);
cy.fillInput(`[data-cy="1499695331-input-motivoBaixa"] textarea`, `Mercados`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/regras->ativo/regras/geracao-controle->1761007042-plusoutlined->1761007042-input-titulo and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/geracao-controle`,`1761007042-plusoutlined`,`1761007042-input-titulo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/geracao-controle"]`);
      cy.clickIfExist(`[data-cy="1761007042-plusoutlined"]`);
      cy.fillInput(`[data-cy="1761007042-input-titulo"] textarea`, `parsing`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-novo->2446766064-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/parametrizacao-calculo-credito`,`4133865625-novo`,`2446766064-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
      cy.clickIfExist(`[data-cy="4133865625-novo"]`);
      cy.clickIfExist(`[data-cy="2446766064-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-novo->2446766064-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/parametrizacao-calculo-credito`,`4133865625-novo`,`2446766064-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
      cy.clickIfExist(`[data-cy="4133865625-novo"]`);
      cy.clickIfExist(`[data-cy="2446766064-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-novo->2446766064-powerselect-estCodigo-2446766064-powerselect-tipoCalculo-2446766064-input-number-parcela-2446766064-powerselect-coefAceleraDepr and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/regras`,`ativo/regras/parametrizacao-calculo-credito`,`4133865625-novo`,`2446766064-powerselect-estCodigo-2446766064-powerselect-tipoCalculo-2446766064-input-number-parcela-2446766064-powerselect-coefAceleraDepr`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/regras"]`);
      cy.clickIfExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
      cy.clickIfExist(`[data-cy="4133865625-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2446766064-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2446766064-powerselect-tipoCalculo"] input`);
cy.fillInput(`[data-cy="2446766064-input-number-parcela"] textarea`, `5`);
cy.fillInputPowerSelect(`[data-cy="2446766064-powerselect-coefAceleraDepr"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-voltar às obrigações do módulo->3477272246-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-voltar às obrigações do módulo`,`3477272246-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-voltar às obrigações do módulo"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-nova solicitação->3477272246-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-nova solicitação`,`3477272246-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="3477272246-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-nova solicitação->3477272246-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-nova solicitação`,`3477272246-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="3477272246-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-agendamentos->4095575583-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-agendamentos`,`4095575583-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4095575583-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-agendamentos->4095575583-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-agendamentos`,`4095575583-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4095575583-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->4095575583-abrir visualização->4095575583-expandir->4095575583-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`4095575583-abrir visualização`,`4095575583-expandir`,`4095575583-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="4095575583-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="4095575583-expandir"]`);
      cy.clickIfExist(`[data-cy="4095575583-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-executar->1866943525-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-executar`,`1866943525-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-executar"]`);
      cy.clickIfExist(`[data-cy="1866943525-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-executar->1866943525-agendar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-executar`,`1866943525-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-executar"]`);
      cy.clickIfExist(`[data-cy="1866943525-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-agendamentos->1866943525-voltar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-agendamentos`,`1866943525-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1866943525-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/regulares->processos/regulares/geracao-controles->1866943525-visualização->1866943525-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-visualização`,`1866943525-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1866943525-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-executar->3815199363-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-executar`,`3815199363-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-executar"]`);
      cy.clickIfExist(`[data-cy="3815199363-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-executar->3815199363-agendar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-executar`,`3815199363-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-executar"]`);
      cy.clickIfExist(`[data-cy="3815199363-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-agendamentos->3815199363-voltar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-agendamentos`,`3815199363-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3815199363-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-visualização->3815199363-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-visualização`,`3815199363-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3815199363-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-executar->3558411779-agendar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-executar`,`3558411779-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-executar"]`);
      cy.clickIfExist(`[data-cy="3558411779-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-agendamentos->3558411779-voltar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-agendamentos`,`3558411779-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3558411779-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-visualização->3558411779-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-visualização`,`3558411779-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3558411779-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-detalhes->3558411779-dados disponíveis para impressão`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-detalhes`,`3558411779-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-detalhes"]`);
      cy.clickIfExist(`[data-cy="3558411779-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-abrir visualização->3558411779-aumentar o zoom`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-abrir visualização`,`3558411779-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3558411779-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-abrir visualização->3558411779-diminuir o zoom`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-abrir visualização`,`3558411779-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3558411779-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-abrir visualização->3558411779-expandir`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-abrir visualização`,`3558411779-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3558411779-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-abrir visualização->3558411779-download`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-abrir visualização`,`3558411779-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3558411779-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-executar->3067069413-agendar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-executar`,`3067069413-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-executar"]`);
      cy.clickIfExist(`[data-cy="3067069413-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-agendamentos->3067069413-voltar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-agendamentos`,`3067069413-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3067069413-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-visualização->3067069413-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-creditos`,`3067069413-visualização`,`3067069413-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-creditos"]`);
      cy.clickIfExist(`[data-cy="3067069413-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3067069413-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-executar->2339427124-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-executar`,`2339427124-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-executar"]`);
      cy.clickIfExist(`[data-cy="2339427124-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-executar->2339427124-agendar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-executar`,`2339427124-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-executar"]`);
      cy.clickIfExist(`[data-cy="2339427124-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-agendamentos->2339427124-voltar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-agendamentos`,`2339427124-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2339427124-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-visualização->2339427124-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-visualização`,`2339427124-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2339427124-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-executar->812801546-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-executar`,`812801546-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-executar"]`);
      cy.clickIfExist(`[data-cy="812801546-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-executar->812801546-agendar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-executar`,`812801546-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-executar"]`);
      cy.clickIfExist(`[data-cy="812801546-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-agendamentos->812801546-voltar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-agendamentos`,`812801546-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-agendamentos"]`);
      cy.clickIfExist(`[data-cy="812801546-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-visualização->812801546-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-visualização`,`812801546-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="812801546-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-executar->2388984350-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-executar`,`2388984350-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-executar"]`);
      cy.clickIfExist(`[data-cy="2388984350-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-executar->2388984350-agendar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-executar`,`2388984350-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-executar"]`);
      cy.clickIfExist(`[data-cy="2388984350-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-agendamentos->2388984350-voltar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-agendamentos`,`2388984350-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2388984350-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-visualização->2388984350-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-visualização`,`2388984350-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2388984350-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-executar->2234285697-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-executar`,`2234285697-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-executar"]`);
      cy.clickIfExist(`[data-cy="2234285697-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-executar->2234285697-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-executar`,`2234285697-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-executar"]`);
      cy.clickIfExist(`[data-cy="2234285697-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-agendamentos->2234285697-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-agendamentos`,`2234285697-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2234285697-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-visualização->2234285697-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-visualização`,`2234285697-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2234285697-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-executar->1894021289-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-executar`,`1894021289-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-executar"]`);
      cy.clickIfExist(`[data-cy="1894021289-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-executar->1894021289-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-executar`,`1894021289-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-executar"]`);
      cy.clickIfExist(`[data-cy="1894021289-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-agendamentos->1894021289-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-agendamentos`,`1894021289-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1894021289-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-visualização->1894021289-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-visualização`,`1894021289-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1894021289-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-executar->2113613011-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-executar`,`2113613011-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-executar"]`);
      cy.clickIfExist(`[data-cy="2113613011-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-executar->2113613011-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-executar`,`2113613011-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-executar"]`);
      cy.clickIfExist(`[data-cy="2113613011-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-executar->2113613011-input-P_PERIODO-2113613011-input-P_LONGO_PRAZO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-executar`,`2113613011-input-P_PERIODO-2113613011-input-P_LONGO_PRAZO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-executar"]`);
      cy.fillInput(`[data-cy="2113613011-input-P_PERIODO"] textarea`, `Representante`);
cy.fillInput(`[data-cy="2113613011-input-P_LONGO_PRAZO"] textarea`, `green`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-agendamentos->2113613011-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-agendamentos`,`2113613011-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2113613011-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-visualização->2113613011-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-visualização`,`2113613011-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2113613011-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-executar->159161114-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-executar`,`159161114-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-executar"]`);
      cy.clickIfExist(`[data-cy="159161114-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-executar->159161114-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-executar`,`159161114-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-executar"]`);
      cy.clickIfExist(`[data-cy="159161114-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-executar->159161114-input-P_PERIODO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-executar`,`159161114-input-P_PERIODO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-executar"]`);
      cy.fillInput(`[data-cy="159161114-input-P_PERIODO"] textarea`, `matrix`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-agendamentos->159161114-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-agendamentos`,`159161114-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-agendamentos"]`);
      cy.clickIfExist(`[data-cy="159161114-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-visualização->159161114-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-visualização`,`159161114-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="159161114-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-executar->1151795328-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-executar`,`1151795328-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-executar"]`);
      cy.clickIfExist(`[data-cy="1151795328-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-executar->1151795328-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-executar`,`1151795328-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-executar"]`);
      cy.clickIfExist(`[data-cy="1151795328-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-executar->1151795328-input-P_PERIODO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-executar`,`1151795328-input-P_PERIODO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-executar"]`);
      cy.fillInput(`[data-cy="1151795328-input-P_PERIODO"] textarea`, `overriding`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-agendamentos->1151795328-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-agendamentos`,`1151795328-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1151795328-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-visualização->1151795328-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-visualização`,`1151795328-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1151795328-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-executar->739562625-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-executar`,`739562625-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-executar"]`);
      cy.clickIfExist(`[data-cy="739562625-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-executar->739562625-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-executar`,`739562625-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-executar"]`);
      cy.clickIfExist(`[data-cy="739562625-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-executar->739562625-input-P_PERIODO_INI-739562625-input-P_PERIODO_FIN and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-executar`,`739562625-input-P_PERIODO_INI-739562625-input-P_PERIODO_FIN`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-executar"]`);
      cy.fillInput(`[data-cy="739562625-input-P_PERIODO_INI"] textarea`, `Especialista`);
cy.fillInput(`[data-cy="739562625-input-P_PERIODO_FIN"] textarea`, `maroon`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-agendamentos->739562625-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-agendamentos`,`739562625-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-agendamentos"]`);
      cy.clickIfExist(`[data-cy="739562625-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-visualização->739562625-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-visualização`,`739562625-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="739562625-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-executar->2488447758-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-executar`,`2488447758-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-executar"]`);
      cy.clickIfExist(`[data-cy="2488447758-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-executar->2488447758-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-executar`,`2488447758-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-executar"]`);
      cy.clickIfExist(`[data-cy="2488447758-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-executar->2488447758-input-P_COD_BEM_INI-2488447758-input-P_COD_BEM_FIN-2488447758-input-P_PERIODO_INI-2488447758-input-P_PERIODO_FIN and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-executar`,`2488447758-input-P_COD_BEM_INI-2488447758-input-P_COD_BEM_FIN-2488447758-input-P_PERIODO_INI-2488447758-input-P_PERIODO_FIN`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-executar"]`);
      cy.fillInput(`[data-cy="2488447758-input-P_COD_BEM_INI"] textarea`, `primary`);
cy.fillInput(`[data-cy="2488447758-input-P_COD_BEM_FIN"] textarea`, `Niue`);
cy.fillInput(`[data-cy="2488447758-input-P_PERIODO_INI"] textarea`, `Rodovia`);
cy.fillInput(`[data-cy="2488447758-input-P_PERIODO_FIN"] textarea`, `Agente`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-agendamentos->2488447758-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-agendamentos`,`2488447758-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2488447758-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-visualização->2488447758-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-visualização`,`2488447758-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2488447758-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-executar->2842860626-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-executar`,`2842860626-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-executar"]`);
      cy.clickIfExist(`[data-cy="2842860626-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-executar->2842860626-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-executar`,`2842860626-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-executar"]`);
      cy.clickIfExist(`[data-cy="2842860626-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-agendamentos->2842860626-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-agendamentos`,`2842860626-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2842860626-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-visualização->2842860626-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-visualização`,`2842860626-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2842860626-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-executar->3050234195-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-executar`,`3050234195-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-executar"]`);
      cy.clickIfExist(`[data-cy="3050234195-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-executar->3050234195-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-executar`,`3050234195-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-executar"]`);
      cy.clickIfExist(`[data-cy="3050234195-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-executar->3050234195-input-P_PERIODO and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-executar`,`3050234195-input-P_PERIODO`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-executar"]`);
      cy.fillInput(`[data-cy="3050234195-input-P_PERIODO"] textarea`, `Gois`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-agendamentos->3050234195-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-agendamentos`,`3050234195-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3050234195-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-visualização->3050234195-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-visualização`,`3050234195-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3050234195-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-executar->2047622754-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-executar`,`2047622754-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-executar"]`);
      cy.clickIfExist(`[data-cy="2047622754-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-executar->2047622754-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-executar`,`2047622754-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-executar"]`);
      cy.clickIfExist(`[data-cy="2047622754-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-agendamentos->2047622754-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-agendamentos`,`2047622754-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2047622754-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-visualização->2047622754-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-visualização`,`2047622754-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2047622754-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-moreoutlined->1798114090-item-->1798114090-cancelar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-moreoutlined`,`1798114090-item-`,`1798114090-cancelar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-moreoutlined"]`);
      cy.clickIfExist(`[data-cy="1798114090-item-"]`);
      cy.clickIfExist(`[data-cy="1798114090-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/manutencao->1798114090-moreoutlined->1798114090-item-->1798114090-próximo`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-moreoutlined`,`1798114090-item-`,`1798114090-próximo`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-moreoutlined"]`);
      cy.clickIfExist(`[data-cy="1798114090-item-"]`);
      cy.clickIfExist(`[data-cy="1798114090-próximo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values ativo->ativo/controles->ativo/controles/manutencao->1798114090-moreoutlined->1798114090-item-->1798114090-input-number-qtdExplosoes-1798114090-input-number-incremento-1798114090-input-number-qtdIndControle-1798114090-input-numPlaqueta-1798114090-input-bemCodigo-1798114090-input-descricaoBem and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/manutencao`,`1798114090-moreoutlined`,`1798114090-item-`,`1798114090-input-number-qtdExplosoes-1798114090-input-number-incremento-1798114090-input-number-qtdIndControle-1798114090-input-numPlaqueta-1798114090-input-bemCodigo-1798114090-input-descricaoBem`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/manutencao"]`);
      cy.clickIfExist(`[data-cy="1798114090-moreoutlined"]`);
      cy.clickIfExist(`[data-cy="1798114090-item-"]`);
      cy.fillInput(`[data-cy="1798114090-input-number-qtdExplosoes"] textarea`, `6`);
cy.fillInput(`[data-cy="1798114090-input-number-incremento"] textarea`, `7`);
cy.fillInput(`[data-cy="1798114090-input-number-qtdIndControle"] textarea`, `6`);
cy.fillInput(`[data-cy="1798114090-input-numPlaqueta"] textarea`, `Berkshire`);
cy.fillInput(`[data-cy="1798114090-input-bemCodigo"] textarea`, `navigate`);
cy.fillInput(`[data-cy="1798114090-input-descricaoBem"] textarea`, `Frozen`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-itens->4219030487-novo->3041493874-salvar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-itens`,`4219030487-novo`,`3041493874-salvar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-itens"]`);
      cy.clickIfExist(`[data-cy="4219030487-novo"]`);
      cy.clickIfExist(`[data-cy="3041493874-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-itens->4219030487-novo->3041493874-voltar`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-itens`,`4219030487-novo`,`3041493874-voltar`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-itens"]`);
      cy.clickIfExist(`[data-cy="4219030487-novo"]`);
      cy.clickIfExist(`[data-cy="3041493874-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-itens->4219030487-novo->3041493874-powerselect-subclasseIdf-3041493874-powerselect-mercCodigo-3041493874-textarea-descricao-3041493874-powerselect-omCodigo-3041493874-powerselect-cfopCodigo-3041493874-powerselect-nopCodigo-3041493874-powerselect-dfltNbmCodigo-3041493874-powerselect-uniCodigo-3041493874-input-number-qtd-3041493874-powerselect-staCodigo-3041493874-powerselect-stnCodigo-3041493874-powerselect-amCodigo-3041493874-powerselect-finCodigo-3041493874-powerselect-cmercCodigo-3041493874-powerselect-codCta-3041493874-powerselect-codCcus-3041493874-input-monetary-vlBaseCpc-3041493874-input-monetary-vlPis-3041493874-input-monetary-vlCofins-3041493874-input-monetary-vlOriginalCorrigido-3041493874-input-monetary-vlIcmsApropriado-3041493874-input-monetary-vlIcmsDifApropriado-3041493874-input-monetary-vlIcmsSttApropriado-3041493874-input-monetary-vlIcmsStfApropriado-3041493874-input-monetary-vlIcmsFrtApropriado and submit`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-itens`,`4219030487-novo`,`3041493874-powerselect-subclasseIdf-3041493874-powerselect-mercCodigo-3041493874-textarea-descricao-3041493874-powerselect-omCodigo-3041493874-powerselect-cfopCodigo-3041493874-powerselect-nopCodigo-3041493874-powerselect-dfltNbmCodigo-3041493874-powerselect-uniCodigo-3041493874-input-number-qtd-3041493874-powerselect-staCodigo-3041493874-powerselect-stnCodigo-3041493874-powerselect-amCodigo-3041493874-powerselect-finCodigo-3041493874-powerselect-cmercCodigo-3041493874-powerselect-codCta-3041493874-powerselect-codCcus-3041493874-input-monetary-vlBaseCpc-3041493874-input-monetary-vlPis-3041493874-input-monetary-vlCofins-3041493874-input-monetary-vlOriginalCorrigido-3041493874-input-monetary-vlIcmsApropriado-3041493874-input-monetary-vlIcmsDifApropriado-3041493874-input-monetary-vlIcmsSttApropriado-3041493874-input-monetary-vlIcmsStfApropriado-3041493874-input-monetary-vlIcmsFrtApropriado`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-itens"]`);
      cy.clickIfExist(`[data-cy="4219030487-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-subclasseIdf"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-mercCodigo"] input`);
cy.fillInput(`[data-cy="3041493874-textarea-descricao"] input`, `strategic`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-omCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-cfopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-nopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-dfltNbmCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-uniCodigo"] input`);
cy.fillInput(`[data-cy="3041493874-input-number-qtd"] textarea`, `8`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-staCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-stnCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-amCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-finCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-cmercCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-codCta"] input`);
cy.fillInputPowerSelect(`[data-cy="3041493874-powerselect-codCcus"] input`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlBaseCpc"] textarea`, `10`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlPis"] textarea`, `4`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlCofins"] textarea`, `8`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlOriginalCorrigido"] textarea`, `8`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlIcmsApropriado"] textarea`, `5`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlIcmsDifApropriado"] textarea`, `8`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlIcmsSttApropriado"] textarea`, `7`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlIcmsStfApropriado"] textarea`, `4`);
cy.fillInput(`[data-cy="3041493874-input-monetary-vlIcmsFrtApropriado"] textarea`, `8`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/documento-entrada-controle-ativo->607493273-visualizar/editar->1407162807-mais operações->1407162807-item-`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/documento-entrada-controle-ativo`,`607493273-visualizar/editar`,`1407162807-mais operações`,`1407162807-item-`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/documento-entrada-controle-ativo"]`);
      cy.clickIfExist(`[data-cy="607493273-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1407162807-mais operações"]`);
      cy.clickIfExist(`[data-cy="1407162807-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-mais operações->655170588-item-`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-mais operações`,`655170588-item-`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-mais operações"]`);
      cy.clickIfExist(`[data-cy="655170588-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-consultar dof->655170588-power-search-button`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-consultar dof`,`655170588-power-search-button`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-consultar dof"]`);
      cy.clickIfExist(`[data-cy="655170588-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-novo->655170588-consultar dof->655170588-selectoutlined`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-novo`,`655170588-consultar dof`,`655170588-selectoutlined`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-novo"]`);
      cy.clickIfExist(`[data-cy="655170588-consultar dof"]`);
      cy.clickIfExist(`[data-cy="655170588-selectoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element ativo->ativo/controles->ativo/controles/saida-controle-dof-ativo->1750036717-visualizar/editar->1499695331-mais operações->1499695331-item-`, () => {
const actualId = [`root`,`ativo`,`ativo/controles`,`ativo/controles/saida-controle-dof-ativo`,`1750036717-visualizar/editar`,`1499695331-mais operações`,`1499695331-item-`];
    cy.clickIfExist(`[data-cy="ativo"]`);
      cy.clickIfExist(`[data-cy="ativo/controles"]`);
      cy.clickIfExist(`[data-cy="ativo/controles/saida-controle-dof-ativo"]`);
      cy.clickIfExist(`[data-cy="1750036717-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="1499695331-mais operações"]`);
      cy.clickIfExist(`[data-cy="1499695331-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values obrigacoes->obrigacoes/solicitacoes->3477272246-ir para todas as obrigações->3477272246-agendamentos->4095575583-visualização->4095575583-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes`,`3477272246-ir para todas as obrigações`,`3477272246-agendamentos`,`4095575583-visualização`,`4095575583-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes"]`);
      cy.clickIfExist(`[data-cy="3477272246-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="3477272246-agendamentos"]`);
      cy.clickIfExist(`[data-cy="4095575583-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="4095575583-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-executar->1866943525-múltipla seleção->1866943525-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-controles`,`1866943525-executar`,`1866943525-múltipla seleção`,`1866943525-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-controles"]`);
      cy.clickIfExist(`[data-cy="1866943525-executar"]`);
      cy.clickIfExist(`[data-cy="1866943525-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1866943525-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-executar->3815199363-múltipla seleção->3815199363-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/exclusao-controles`,`3815199363-executar`,`3815199363-múltipla seleção`,`3815199363-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/exclusao-controles"]`);
      cy.clickIfExist(`[data-cy="3815199363-executar"]`);
      cy.clickIfExist(`[data-cy="3815199363-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3815199363-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-abrir visualização->3558411779-expandir->3558411779-diminuir`, () => {
const actualId = [`root`,`processos`,`processos/regulares`,`processos/regulares/geracao-creditos`,`3558411779-abrir visualização`,`3558411779-expandir`,`3558411779-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/regulares"]`);
      cy.clickIfExist(`[data-cy="processos/regulares/geracao-creditos"]`);
      cy.clickIfExist(`[data-cy="3558411779-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3558411779-expandir"]`);
      cy.clickIfExist(`[data-cy="3558411779-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-executar->2339427124-múltipla seleção->2339427124-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/atualizacao-indicador`,`2339427124-executar`,`2339427124-múltipla seleção`,`2339427124-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/atualizacao-indicador"]`);
      cy.clickIfExist(`[data-cy="2339427124-executar"]`);
      cy.clickIfExist(`[data-cy="2339427124-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2339427124-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-executar->812801546-múltipla seleção->812801546-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/copia-parametrizacao-calculo`,`812801546-executar`,`812801546-múltipla seleção`,`812801546-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/copia-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="812801546-executar"]`);
      cy.clickIfExist(`[data-cy="812801546-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="812801546-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-executar->2388984350-múltipla seleção->2388984350-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/avancado`,`processos/avancado/exclui-parametrizacao-calculo`,`2388984350-executar`,`2388984350-múltipla seleção`,`2388984350-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/avancado"]`);
      cy.clickIfExist(`[data-cy="processos/avancado/exclui-parametrizacao-calculo"]`);
      cy.clickIfExist(`[data-cy="2388984350-executar"]`);
      cy.clickIfExist(`[data-cy="2388984350-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2388984350-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-executar->2234285697-múltipla seleção->2234285697-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/consolidado-mensal-rs`,`2234285697-executar`,`2234285697-múltipla seleção`,`2234285697-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/consolidado-mensal-rs"]`);
      cy.clickIfExist(`[data-cy="2234285697-executar"]`);
      cy.clickIfExist(`[data-cy="2234285697-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2234285697-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-executar->1894021289-múltipla seleção->1894021289-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/listagem-dof-base`,`1894021289-executar`,`1894021289-múltipla seleção`,`1894021289-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/listagem-dof-base"]`);
      cy.clickIfExist(`[data-cy="1894021289-executar"]`);
      cy.clickIfExist(`[data-cy="1894021289-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1894021289-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-executar->2113613011-múltipla seleção->2113613011-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-curto-longo-prazo`,`2113613011-executar`,`2113613011-múltipla seleção`,`2113613011-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-curto-longo-prazo"]`);
      cy.clickIfExist(`[data-cy="2113613011-executar"]`);
      cy.clickIfExist(`[data-cy="2113613011-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2113613011-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-executar->159161114-múltipla seleção->159161114-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/itens-sem-apropriacao-creditos`,`159161114-executar`,`159161114-múltipla seleção`,`159161114-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/itens-sem-apropriacao-creditos"]`);
      cy.clickIfExist(`[data-cy="159161114-executar"]`);
      cy.clickIfExist(`[data-cy="159161114-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="159161114-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-executar->1151795328-múltipla seleção->1151795328-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/saidas-controles-sem-apropriacao`,`1151795328-executar`,`1151795328-múltipla seleção`,`1151795328-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/saidas-controles-sem-apropriacao"]`);
      cy.clickIfExist(`[data-cy="1151795328-executar"]`);
      cy.clickIfExist(`[data-cy="1151795328-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1151795328-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-executar->739562625-múltipla seleção->739562625-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/apropriacao-creditos-por-periodo`,`739562625-executar`,`739562625-múltipla seleção`,`739562625-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/apropriacao-creditos-por-periodo"]`);
      cy.clickIfExist(`[data-cy="739562625-executar"]`);
      cy.clickIfExist(`[data-cy="739562625-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="739562625-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-executar->2488447758-múltipla seleção->2488447758-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/ocorrencias-por-controle`,`2488447758-executar`,`2488447758-múltipla seleção`,`2488447758-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/ocorrencias-por-controle"]`);
      cy.clickIfExist(`[data-cy="2488447758-executar"]`);
      cy.clickIfExist(`[data-cy="2488447758-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2488447758-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-executar->2842860626-múltipla seleção->2842860626-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/controles-ativo`,`2842860626-executar`,`2842860626-múltipla seleção`,`2842860626-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/controles-ativo"]`);
      cy.clickIfExist(`[data-cy="2842860626-executar"]`);
      cy.clickIfExist(`[data-cy="2842860626-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2842860626-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-executar->3050234195-múltipla seleção->3050234195-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/criticas-apoio`,`relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado`,`3050234195-executar`,`3050234195-múltipla seleção`,`3050234195-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio"]`);
      cy.clickIfExist(`[data-cy="relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado"]`);
      cy.clickIfExist(`[data-cy="3050234195-executar"]`);
      cy.clickIfExist(`[data-cy="3050234195-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3050234195-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-executar->2047622754-múltipla seleção->2047622754-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/registros-fiscais-convenios`,`relatorios/registros-fiscais-convenios/consolidado-mensal-creditos`,`2047622754-executar`,`2047622754-múltipla seleção`,`2047622754-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios"]`);
      cy.clickIfExist(`[data-cy="relatorios/registros-fiscais-convenios/consolidado-mensal-creditos"]`);
      cy.clickIfExist(`[data-cy="2047622754-executar"]`);
      cy.clickIfExist(`[data-cy="2047622754-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2047622754-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
