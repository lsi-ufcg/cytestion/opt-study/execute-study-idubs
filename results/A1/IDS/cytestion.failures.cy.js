describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-visualizar/editar->3424382391-salvar`, () => {
    cy.clickCauseExist(`[data-cy="ativo"]`);
    cy.clickCauseExist(`[data-cy="ativo/regras"]`);
    cy.clickCauseExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
    cy.clickCauseExist(`[data-cy="4133865625-visualizar/editar"]`);
    cy.clickCauseExist(`[data-cy="3424382391-salvar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values ativo->ativo/regras->ativo/regras/parametrizacao-calculo-credito->4133865625-visualizar/editar->3424382391-powerselect-estCodigo-3424382391-powerselect-tipoCalculo-3424382391-powerselect-coefAceleraDepr and submit`, () => {
    cy.clickCauseExist(`[data-cy="ativo"]`);
    cy.clickCauseExist(`[data-cy="ativo/regras"]`);
    cy.clickCauseExist(`[data-cy="ativo/regras/parametrizacao-calculo-credito"]`);
    cy.clickCauseExist(`[data-cy="4133865625-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="3424382391-powerselect-estCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3424382391-powerselect-tipoCalculo"] input`);
    cy.fillInputPowerSelect(`[data-cy="3424382391-powerselect-coefAceleraDepr"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
