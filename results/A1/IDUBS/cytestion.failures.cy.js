describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element processos->processos/regulares->processos/regulares/geracao-controles->1866943525-agendamentos->1866943525-voltar`, () => {
    cy.visit('http://system-A1/processos/regulares/geracao-controles?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143093D%7C%7C19143093&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1866943525-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/regulares->processos/regulares/exclusao-controles->3815199363-agendamentos->3815199363-voltar`, () => {
    cy.visit('http://system-A1/processos/regulares/exclusao-controles?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143086D%7C%7C19143086&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3815199363-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/regulares->processos/regulares/geracao-creditos->3558411779-agendamentos->3558411779-voltar`, () => {
    cy.visit('http://system-A1/processos/regulares/geracao-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143097D%7C%7C19143097&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3558411779-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/regulares->processos/regulares/exclusao-creditos->3067069413-agendamentos->3067069413-voltar`, () => {
    cy.visit('http://system-A1/processos/regulares/exclusao-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143090D%7C%7C19143090&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3067069413-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/avancado->processos/avancado/atualizacao-indicador->2339427124-agendamentos->2339427124-voltar`, () => {
    cy.visit('http://system-A1/processos/avancado/atualizacao-indicador?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19217350D%7C%7C19217350&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2339427124-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/avancado->processos/avancado/copia-parametrizacao-calculo->812801546-agendamentos->812801546-voltar`, () => {
    cy.visit(
      'http://system-A1/processos/avancado/copia-parametrizacao-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143105D%7C%7C19143105&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="812801546-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/avancado->processos/avancado/exclui-parametrizacao-calculo->2388984350-agendamentos->2388984350-voltar`, () => {
    cy.visit(
      'http://system-A1/processos/avancado/exclui-parametrizacao-calculo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143108D%7C%7C19143108&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2388984350-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/consolidado-mensal-rs->2234285697-agendamentos->2234285697-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/consolidado-mensal-rs?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143110D%7C%7C19143110&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2234285697-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/listagem-dof-base->1894021289-agendamentos->1894021289-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/listagem-dof-base?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143078D%7C%7C19143078&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1894021289-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-curto-longo-prazo->2113613011-agendamentos->2113613011-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/creditos-curto-longo-prazo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19797227D%7C%7C19797227&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2113613011-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/itens-sem-apropriacao-creditos->159161114-agendamentos->159161114-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/itens-sem-apropriacao-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19791561D%7C%7C19791561&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="159161114-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/saidas-controles-sem-apropriacao->1151795328-agendamentos->1151795328-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/saidas-controles-sem-apropriacao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19837997D%7C%7C19837997&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1151795328-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/apropriacao-creditos-por-periodo->739562625-agendamentos->739562625-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/apropriacao-creditos-por-periodo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19840411D%7C%7C19840411&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="739562625-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/ocorrencias-por-controle->2488447758-agendamentos->2488447758-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/ocorrencias-por-controle?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19868575D%7C%7C19868575&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2488447758-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/controles-ativo->2842860626-agendamentos->2842860626-voltar`, () => {
    cy.visit('http://system-A1/relatorios/criticas-apoio/controles-ativo?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~23708068D%7C%7C23708068&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2842860626-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/criticas-apoio->relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado->3050234195-agendamentos->3050234195-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/criticas-apoio/creditos-apropriados-sem-limp-gerado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~23174448D%7C%7C23174448&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3050234195-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/registros-fiscais-convenios->relatorios/registros-fiscais-convenios/consolidado-mensal-creditos->2047622754-agendamentos->2047622754-voltar`, () => {
    cy.visit(
      'http://system-A1/relatorios/registros-fiscais-convenios/consolidado-mensal-creditos?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~19143073D%7C%7C19143073&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2047622754-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
