describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element processos->processos/limpeza-snf->1756863936-agendamentos->1756863936-voltar`, () => {
    cy.visit('http://system-A7/processos/limpeza-snf?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~240208D%7C%7C240208&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1756863936-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-agendamentos->1172322540-voltar`, () => {
    cy.visit('http://system-A7/processos/limpeza-log-integra-nfe?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~12215984D%7C%7C12215984&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1172322540-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-agendamentos->2009102830-voltar`, () => {
    cy.visit('http://system-A7/processos/limpeza-eventos-adaptador?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~14685568D%7C%7C14685568&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2009102830-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
