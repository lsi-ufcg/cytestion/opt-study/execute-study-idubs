describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo`, () => {
const actualId = [`root`,`tabelas-corporativo`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros`, () => {
const actualId = [`root`,`parametros`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs`, () => {
const actualId = [`root`,`gerenciamento-dofs`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
const actualId = [`root`,`processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorio`, () => {
const actualId = [`root`,`relatorio`];
    cy.clickIfExist(`[data-cy="relatorio"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 1564287553-operações em lote`, () => {
const actualId = [`root`,`1564287553-operações em lote`];
    cy.clickIfExist(`[data-cy="1564287553-operações em lote"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 1564287553-power-search-button`, () => {
const actualId = [`root`,`1564287553-power-search-button`];
    cy.clickIfExist(`[data-cy="1564287553-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 1564287553-playcircleoutlined`, () => {
const actualId = [`root`,`1564287553-playcircleoutlined`];
    cy.clickIfExist(`[data-cy="1564287553-playcircleoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values 1564287553-input-number-timer and submit`, () => {
const actualId = [`root`,`1564287553-input-number-timer`];
    cy.fillInput(`[data-cy="1564287553-input-number-timer"] textarea`, `6`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->tabelas-corporativas/parametros-gerais/parest`, () => {
const actualId = [`root`,`tabelas-corporativo`,`tabelas-corporativas/parametros-gerais/parest`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametros-gerais/parest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativo->series-estabelecimento`, () => {
const actualId = [`root`,`tabelas-corporativo`,`series-estabelecimento`];
    cy.clickIfExist(`[data-cy="tabelas-corporativo"]`);
      cy.clickIfExist(`[data-cy="series-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/notas-tecnicas-por-uf`, () => {
const actualId = [`root`,`parametros`,`parametros/notas-tecnicas-por-uf`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/notas-tecnicas-por-uf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/digitacao-manutencao-dof`, () => {
const actualId = [`root`,`transacoes`,`transacoes/digitacao-manutencao-dof`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/digitacao-manutencao-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/inutilizacao-nfe`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/inutilizacao-nfe`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/inutilizacao-nfe"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/gerenciador-destinos`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/gerenciador-destinos`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/gerenciador-destinos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/nota-fiscal-m21`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/nota-fiscal-m21`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/nota-fiscal-m21"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorio->relatorio/relatorio-quebra-de-sequencia-de-documentos`, () => {
const actualId = [`root`,`relatorio`,`relatorio/relatorio-quebra-de-sequencia-de-documentos`];
    cy.clickIfExist(`[data-cy="relatorio"]`);
      cy.clickIfExist(`[data-cy="relatorio/relatorio-quebra-de-sequencia-de-documentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->176558749-power-search-button`, () => {
const actualId = [`root`,`download`,`176558749-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="176558749-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->176558749-download`, () => {
const actualId = [`root`,`download`,`176558749-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="176558749-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->176558749-detalhes`, () => {
const actualId = [`root`,`download`,`176558749-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="176558749-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->176558749-excluir`, () => {
const actualId = [`root`,`download`,`176558749-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="176558749-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-novo`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-novo`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-power-search-button`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-power-search-button`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-eyeoutlined`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-eyeoutlined`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-deleteoutlined`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-deleteoutlined`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-deleteoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-carregar mais`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-carregar mais`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->parametros/notas-tecnicas-por-uf->2894110433-powerselect-notaTecnicaKey and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/notas-tecnicas-por-uf`,`2894110433-powerselect-notaTecnicaKey`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/notas-tecnicas-por-uf"]`);
      cy.fillInputPowerSelect(`[data-cy="2894110433-powerselect-notaTecnicaKey"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/formas-envio`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/formas-envio`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/formas-envio"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/inutilizacao-nfe->2361222596-atualize as informações da busca`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/inutilizacao-nfe`,`2361222596-atualize as informações da busca`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/inutilizacao-nfe"]`);
      cy.clickIfExist(`[data-cy="2361222596-atualize as informações da busca"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/inutilizacao-nfe->2361222596-enquadrar`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/inutilizacao-nfe`,`2361222596-enquadrar`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/inutilizacao-nfe"]`);
      cy.clickIfExist(`[data-cy="2361222596-enquadrar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/inutilizacao-nfe->2361222596-power-search-button`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/inutilizacao-nfe`,`2361222596-power-search-button`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/inutilizacao-nfe"]`);
      cy.clickIfExist(`[data-cy="2361222596-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/inutilizacao-nfe->2361222596-carregar mais`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/inutilizacao-nfe`,`2361222596-carregar mais`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/inutilizacao-nfe"]`);
      cy.clickIfExist(`[data-cy="2361222596-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values gerenciamento-dofs->gerenciamento-dofs/inutilizacao-nfe->2361222596-powerselect-estCodigo-2361222596-powerselect-mdofCodigo-2361222596-powerselect-indEntradaSaida-2361222596-powerselect-serie-2361222596-powerselect-indInutilizacao and submit`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/inutilizacao-nfe`,`2361222596-powerselect-estCodigo-2361222596-powerselect-mdofCodigo-2361222596-powerselect-indEntradaSaida-2361222596-powerselect-serie-2361222596-powerselect-indInutilizacao`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/inutilizacao-nfe"]`);
      cy.fillInputPowerSelect(`[data-cy="2361222596-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2361222596-powerselect-mdofCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2361222596-powerselect-indEntradaSaida"] input`);
cy.fillInputPowerSelect(`[data-cy="2361222596-powerselect-serie"] input`);
cy.fillInputPowerSelect(`[data-cy="2361222596-powerselect-indInutilizacao"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/gerenciador-destinos->2781661985-criar destino de emissão`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/gerenciador-destinos`,`2781661985-criar destino de emissão`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/gerenciador-destinos"]`);
      cy.clickIfExist(`[data-cy="2781661985-criar destino de emissão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/gerenciador-destinos->2781661985-power-search-button`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/gerenciador-destinos`,`2781661985-power-search-button`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/gerenciador-destinos"]`);
      cy.clickIfExist(`[data-cy="2781661985-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/nota-fiscal-m21->485494959-operações em lote`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/nota-fiscal-m21`,`485494959-operações em lote`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/nota-fiscal-m21"]`);
      cy.clickIfExist(`[data-cy="485494959-operações em lote"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element gerenciamento-dofs->gerenciamento-dofs/nota-fiscal-m21->485494959-power-search-button`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/nota-fiscal-m21`,`485494959-power-search-button`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/nota-fiscal-m21"]`);
      cy.clickIfExist(`[data-cy="485494959-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-executar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-visualização`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-executar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-visualização`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-executar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-agendamentos`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-power-search-button`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-visualização`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-novo->2032344527-salvar`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-novo`,`2032344527-salvar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-novo"]`);
      cy.clickIfExist(`[data-cy="2032344527-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-novo->2032344527-voltar`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-novo`,`2032344527-voltar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-novo"]`);
      cy.clickIfExist(`[data-cy="2032344527-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->digitacao/emissao-dof/filas-adaptador->1597369178-novo->2032344527-input-titulo-2032344527-powerselect-estCodigo-2032344527-powerselect-nopCodigo-2032344527-powerselect-edofCodigo-2032344527-input-sistemaOrigem and submit`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-novo`,`2032344527-input-titulo-2032344527-powerselect-estCodigo-2032344527-powerselect-nopCodigo-2032344527-powerselect-edofCodigo-2032344527-input-sistemaOrigem`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-novo"]`);
      cy.fillInput(`[data-cy="2032344527-input-titulo"] textarea`, `Automotive`);
cy.fillInputPowerSelect(`[data-cy="2032344527-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2032344527-powerselect-nopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="2032344527-powerselect-edofCodigo"] input`);
cy.fillInput(`[data-cy="2032344527-input-sistemaOrigem"] textarea`, `Viela`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-eyeoutlined->640694358-remover item`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-eyeoutlined`,`640694358-remover item`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="640694358-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-eyeoutlined->640694358-salvar`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-eyeoutlined`,`640694358-salvar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="640694358-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->digitacao/emissao-dof/filas-adaptador->1597369178-eyeoutlined->640694358-voltar`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-eyeoutlined`,`640694358-voltar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="640694358-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->digitacao/emissao-dof/filas-adaptador->1597369178-eyeoutlined->640694358-input-titulo-640694358-powerselect-estCodigo-640694358-powerselect-nopCodigo-640694358-powerselect-edofCodigo-640694358-powerselect-serieId-640694358-input-sistemaOrigem and submit`, () => {
const actualId = [`root`,`parametros`,`digitacao/emissao-dof/filas-adaptador`,`1597369178-eyeoutlined`,`640694358-input-titulo-640694358-powerselect-estCodigo-640694358-powerselect-nopCodigo-640694358-powerselect-edofCodigo-640694358-powerselect-serieId-640694358-input-sistemaOrigem`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="digitacao/emissao-dof/filas-adaptador"]`);
      cy.clickIfExist(`[data-cy="1597369178-eyeoutlined"]`);
      cy.fillInput(`[data-cy="640694358-input-titulo"] textarea`, `Equador`);
cy.fillInputPowerSelect(`[data-cy="640694358-powerselect-estCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="640694358-powerselect-nopCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="640694358-powerselect-edofCodigo"] input`);
cy.fillInputPowerSelect(`[data-cy="640694358-powerselect-serieId"] input`);
cy.fillInput(`[data-cy="640694358-input-sistemaOrigem"] textarea`, `Wooden`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/notas-tecnicas-por-uf->2894110433-powerselect-notaTecnicaKey->2894110433-power-search-button`, () => {
const actualId = [`root`,`parametros`,`parametros/notas-tecnicas-por-uf`,`2894110433-powerselect-notaTecnicaKey`,`2894110433-power-search-button`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/notas-tecnicas-por-uf"]`);
      cy.fillInputPowerSelect(`[data-cy="2894110433-powerselect-notaTecnicaKey"] input`);
cy.submitIfExist(`.ant-form`);

      cy.clickIfExist(`[data-cy="2894110433-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/notas-tecnicas-por-uf->2894110433-powerselect-notaTecnicaKey->2894110433-carregar mais`, () => {
const actualId = [`root`,`parametros`,`parametros/notas-tecnicas-por-uf`,`2894110433-powerselect-notaTecnicaKey`,`2894110433-carregar mais`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/notas-tecnicas-por-uf"]`);
      cy.fillInputPowerSelect(`[data-cy="2894110433-powerselect-notaTecnicaKey"] input`);
cy.submitIfExist(`.ant-form`);

      cy.clickIfExist(`[data-cy="2894110433-carregar mais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/notas-tecnicas-por-uf->2894110433-powerselect-notaTecnicaKey->2894110433-fechar`, () => {
const actualId = [`root`,`parametros`,`parametros/notas-tecnicas-por-uf`,`2894110433-powerselect-notaTecnicaKey`,`2894110433-fechar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/notas-tecnicas-por-uf"]`);
      cy.fillInputPowerSelect(`[data-cy="2894110433-powerselect-notaTecnicaKey"] input`);
cy.submitIfExist(`.ant-form`);

      cy.clickIfExist(`[data-cy="2894110433-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->parametros/notas-tecnicas-por-uf->2894110433-powerselect-notaTecnicaKey->2894110433-power-search-input and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/notas-tecnicas-por-uf`,`2894110433-powerselect-notaTecnicaKey`,`2894110433-power-search-input`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/notas-tecnicas-por-uf"]`);
      cy.fillInputPowerSelect(`[data-cy="2894110433-powerselect-notaTecnicaKey"] input`);
cy.submitIfExist(`.ant-form`);

      cy.fillInputPowerSearch(`[data-cy="2894110433-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new->2396904729-novo`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`,`2396904729-novo`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.clickIfExist(`[data-cy="2396904729-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new->2396904729-power-search-button`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`,`2396904729-power-search-button`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.clickIfExist(`[data-cy="2396904729-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/formas-envio->2374286314-remover item`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/formas-envio`,`2374286314-remover item`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/formas-envio"]`);
      cy.clickIfExist(`[data-cy="2374286314-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/formas-envio->2374286314-salvar`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/formas-envio`,`2374286314-salvar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/formas-envio"]`);
      cy.clickIfExist(`[data-cy="2374286314-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/formas-envio->2374286314-checkbox-email-2374286314-checkbox-bucket-2374286314-input-remetenteMa-2374286314-input-emailRemetenteMa-2374286314-input-password-senhaRemetenteMa-2374286314-input-hostMa-2374286314-input-number-portaServidorMa-2374286314-powerselect-sslMa-2374286314-powerselect-tlsMa-2374286314-powerselect-envioAutomaticoMa-2374286314-input-accessKeyIdBu-2374286314-input-password-secretAccessKeyBu-2374286314-input-nomeBucketBu-2374286314-input-endpointBu-2374286314-input-number-portaServidorBu-2374286314-input-regionNameBu-2374286314-powerselect-secureBu-2374286314-powerselect-envioAutomaticoBu and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/formas-envio`,`2374286314-checkbox-email-2374286314-checkbox-bucket-2374286314-input-remetenteMa-2374286314-input-emailRemetenteMa-2374286314-input-password-senhaRemetenteMa-2374286314-input-hostMa-2374286314-input-number-portaServidorMa-2374286314-powerselect-sslMa-2374286314-powerselect-tlsMa-2374286314-powerselect-envioAutomaticoMa-2374286314-input-accessKeyIdBu-2374286314-input-password-secretAccessKeyBu-2374286314-input-nomeBucketBu-2374286314-input-endpointBu-2374286314-input-number-portaServidorBu-2374286314-input-regionNameBu-2374286314-powerselect-secureBu-2374286314-powerselect-envioAutomaticoBu`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/formas-envio"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2374286314-checkbox-email"] textarea`);
cy.fillInputCheckboxOrRadio(`[data-cy="2374286314-checkbox-bucket"] textarea`);
cy.fillInput(`[data-cy="2374286314-input-remetenteMa"] textarea`, `Unbranded Rubber Towels`);
cy.fillInput(`[data-cy="2374286314-input-emailRemetenteMa"] textarea`, `Sleek`);
cy.fillInput(`[data-cy="2374286314-input-password-senhaRemetenteMa"] textarea`, `synthesizing`);
cy.fillInput(`[data-cy="2374286314-input-hostMa"] textarea`, `Arquiplago de Chagos`);
cy.fillInput(`[data-cy="2374286314-input-number-portaServidorMa"] textarea`, `8`);
cy.fillInputPowerSelect(`[data-cy="2374286314-powerselect-sslMa"] input`);
cy.fillInputPowerSelect(`[data-cy="2374286314-powerselect-tlsMa"] input`);
cy.fillInputPowerSelect(`[data-cy="2374286314-powerselect-envioAutomaticoMa"] input`);
cy.fillInput(`[data-cy="2374286314-input-accessKeyIdBu"] textarea`, `Programa`);
cy.fillInput(`[data-cy="2374286314-input-password-secretAccessKeyBu"] textarea`, `definition`);
cy.fillInput(`[data-cy="2374286314-input-nomeBucketBu"] textarea`, `Litunia`);
cy.fillInput(`[data-cy="2374286314-input-endpointBu"] textarea`, `Indian Rupee Ngultrum`);
cy.fillInput(`[data-cy="2374286314-input-number-portaServidorBu"] textarea`, `9`);
cy.fillInput(`[data-cy="2374286314-input-regionNameBu"] textarea`, `Berkshire`);
cy.fillInputPowerSelect(`[data-cy="2374286314-powerselect-secureBu"] input`);
cy.fillInputPowerSelect(`[data-cy="2374286314-powerselect-envioAutomaticoBu"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values gerenciamento-dofs->gerenciamento-dofs/gerenciador-destinos->2781661985-criar destino de emissão->2781661985-powerselect-docDefId-2781661985-input-titulo and submit`, () => {
const actualId = [`root`,`gerenciamento-dofs`,`gerenciamento-dofs/gerenciador-destinos`,`2781661985-criar destino de emissão`,`2781661985-powerselect-docDefId-2781661985-input-titulo`];
    cy.clickIfExist(`[data-cy="gerenciamento-dofs"]`);
      cy.clickIfExist(`[data-cy="gerenciamento-dofs/gerenciador-destinos"]`);
      cy.clickIfExist(`[data-cy="2781661985-criar destino de emissão"]`);
      cy.fillInputPowerSelect(`[data-cy="2781661985-powerselect-docDefId"] input`);
cy.fillInput(`[data-cy="2781661985-input-titulo"] textarea`, `24365`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-executar->1756863936-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-executar`,`1756863936-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-executar"]`);
      cy.clickIfExist(`[data-cy="1756863936-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-executar->1756863936-agendar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-executar`,`1756863936-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-executar"]`);
      cy.clickIfExist(`[data-cy="1756863936-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-snf->1756863936-executar->1756863936-input-P_NUMDIAS-1756863936-input-PCOD_REFERENCIA and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-executar`,`1756863936-input-P_NUMDIAS-1756863936-input-PCOD_REFERENCIA`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-executar"]`);
      cy.fillInput(`[data-cy="1756863936-input-P_NUMDIAS"] textarea`, `Indian Rupee Ngultrum`);
cy.fillInput(`[data-cy="1756863936-input-PCOD_REFERENCIA"] textarea`, `Cambridgeshire`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-snf->1756863936-agendamentos->1756863936-voltar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-agendamentos`,`1756863936-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1756863936-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-snf->1756863936-visualização->1756863936-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-visualização`,`1756863936-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1756863936-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-executar->1172322540-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-executar`,`1172322540-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-executar"]`);
      cy.clickIfExist(`[data-cy="1172322540-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-executar->1172322540-agendar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-executar`,`1172322540-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-executar"]`);
      cy.clickIfExist(`[data-cy="1172322540-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-log-integra-nfe->1172322540-executar->1172322540-input-PDIAS and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-executar`,`1172322540-input-PDIAS`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-executar"]`);
      cy.fillInput(`[data-cy="1172322540-input-PDIAS"] textarea`, `EXE`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-agendamentos->1172322540-voltar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-agendamentos`,`1172322540-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1172322540-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-log-integra-nfe->1172322540-visualização->1172322540-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-visualização`,`1172322540-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1172322540-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-executar->2009102830-múltipla seleção`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-executar`,`2009102830-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-executar"]`);
      cy.clickIfExist(`[data-cy="2009102830-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-executar->2009102830-agendar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-executar`,`2009102830-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-executar"]`);
      cy.clickIfExist(`[data-cy="2009102830-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-eventos-adaptador->2009102830-executar->2009102830-input-PDIAS and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-executar`,`2009102830-input-PDIAS`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-executar"]`);
      cy.fillInput(`[data-cy="2009102830-input-PDIAS"] textarea`, `Rand`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-agendamentos->2009102830-voltar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-agendamentos`,`2009102830-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2009102830-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpeza-eventos-adaptador->2009102830-visualização->2009102830-item- and submit`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-visualização`,`2009102830-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2009102830-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new->2396904729-novo->2142727024-upload logotipo`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`,`2396904729-novo`,`2142727024-upload logotipo`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.clickIfExist(`[data-cy="2396904729-novo"]`);
      cy.clickIfExist(`[data-cy="2142727024-upload logotipo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new->2396904729-novo->2142727024-salvar`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`,`2396904729-novo`,`2142727024-salvar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.clickIfExist(`[data-cy="2396904729-novo"]`);
      cy.clickIfExist(`[data-cy="2142727024-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new->2396904729-novo->2142727024-voltar`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`,`2396904729-novo`,`2142727024-voltar`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.clickIfExist(`[data-cy="2396904729-novo"]`);
      cy.clickIfExist(`[data-cy="2142727024-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values parametros->parametros/nota-fiscal-m21-parametros->parametros/nota-fiscal-m21-parametros/logotipo-new->2396904729-novo->2142727024-powerselect-indGenerico-2142727024-powerselect-formulario-2142727024-powerselect-estCodigo and submit`, () => {
const actualId = [`root`,`parametros`,`parametros/nota-fiscal-m21-parametros`,`parametros/nota-fiscal-m21-parametros/logotipo-new`,`2396904729-novo`,`2142727024-powerselect-indGenerico-2142727024-powerselect-formulario-2142727024-powerselect-estCodigo`];
    cy.clickIfExist(`[data-cy="parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros"]`);
      cy.clickIfExist(`[data-cy="parametros/nota-fiscal-m21-parametros/logotipo-new"]`);
      cy.clickIfExist(`[data-cy="2396904729-novo"]`);
      cy.fillInputPowerSelect(`[data-cy="2142727024-powerselect-indGenerico"] input`);
cy.fillInputPowerSelect(`[data-cy="2142727024-powerselect-formulario"] input`);
cy.fillInputPowerSelect(`[data-cy="2142727024-powerselect-estCodigo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/limpeza-snf->1756863936-executar->1756863936-múltipla seleção->1756863936-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-snf`,`1756863936-executar`,`1756863936-múltipla seleção`,`1756863936-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-snf"]`);
      cy.clickIfExist(`[data-cy="1756863936-executar"]`);
      cy.clickIfExist(`[data-cy="1756863936-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1756863936-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/limpeza-log-integra-nfe->1172322540-executar->1172322540-múltipla seleção->1172322540-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-log-integra-nfe`,`1172322540-executar`,`1172322540-múltipla seleção`,`1172322540-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-log-integra-nfe"]`);
      cy.clickIfExist(`[data-cy="1172322540-executar"]`);
      cy.clickIfExist(`[data-cy="1172322540-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1172322540-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element processos->processos/limpeza-eventos-adaptador->2009102830-executar->2009102830-múltipla seleção->2009102830-cancelar`, () => {
const actualId = [`root`,`processos`,`processos/limpeza-eventos-adaptador`,`2009102830-executar`,`2009102830-múltipla seleção`,`2009102830-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
      cy.clickIfExist(`[data-cy="processos/limpeza-eventos-adaptador"]`);
      cy.clickIfExist(`[data-cy="2009102830-executar"]`);
      cy.clickIfExist(`[data-cy="2009102830-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2009102830-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
