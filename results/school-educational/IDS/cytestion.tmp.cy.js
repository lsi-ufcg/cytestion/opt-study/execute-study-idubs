describe('Cytestion', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-navbar-toggle`, () => {
const actualId = [`root`,`index-navbar-toggle`];
    cy.clickIfExist(`[data-cy="index-navbar-toggle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-navbar-brand`, () => {
const actualId = [`root`,`index-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-index`, () => {
const actualId = [`root`,`index-index`];
    cy.clickIfExist(`[data-cy="index-index"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about`, () => {
const actualId = [`root`,`index-about`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services`, () => {
const actualId = [`root`,`index-services`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses`, () => {
const actualId = [`root`,`index-courses`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing`, () => {
const actualId = [`root`,`index-pricing`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact`, () => {
const actualId = [`root`,`index-contact`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-read-more-1`, () => {
const actualId = [`root`,`index-read-more-1`];
    cy.clickIfExist(`[data-cy="index-read-more-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-read-more-2`, () => {
const actualId = [`root`,`index-read-more-2`];
    cy.clickIfExist(`[data-cy="index-read-more-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-img-1`, () => {
const actualId = [`root`,`index-img-1`];
    cy.clickIfExist(`[data-cy="index-img-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-img-2`, () => {
const actualId = [`root`,`index-img-2`];
    cy.clickIfExist(`[data-cy="index-img-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-img-3`, () => {
const actualId = [`root`,`index-img-3`];
    cy.clickIfExist(`[data-cy="index-img-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-events`, () => {
const actualId = [`root`,`index-events`];
    cy.clickIfExist(`[data-cy="index-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-terms`, () => {
const actualId = [`root`,`index-terms`];
    cy.clickIfExist(`[data-cy="index-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-privacy`, () => {
const actualId = [`root`,`index-privacy`];
    cy.clickIfExist(`[data-cy="index-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-career`, () => {
const actualId = [`root`,`index-career`];
    cy.clickIfExist(`[data-cy="index-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact-us`, () => {
const actualId = [`root`,`index-contact-us`];
    cy.clickIfExist(`[data-cy="index-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-1`, () => {
const actualId = [`root`,`index-text-1`];
    cy.clickIfExist(`[data-cy="index-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-2`, () => {
const actualId = [`root`,`index-text-2`];
    cy.clickIfExist(`[data-cy="index-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-3`, () => {
const actualId = [`root`,`index-text-3`];
    cy.clickIfExist(`[data-cy="index-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-4`, () => {
const actualId = [`root`,`index-text-4`];
    cy.clickIfExist(`[data-cy="index-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-5`, () => {
const actualId = [`root`,`index-text-5`];
    cy.clickIfExist(`[data-cy="index-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-text-6`, () => {
const actualId = [`root`,`index-text-6`];
    cy.clickIfExist(`[data-cy="index-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-WebThemez`, () => {
const actualId = [`root`,`index-WebThemez`];
    cy.clickIfExist(`[data-cy="index-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-facebook`, () => {
const actualId = [`root`,`index-facebook`];
    cy.clickIfExist(`[data-cy="index-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-twitter`, () => {
const actualId = [`root`,`index-twitter`];
    cy.clickIfExist(`[data-cy="index-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-linkedin`, () => {
const actualId = [`root`,`index-linkedin`];
    cy.clickIfExist(`[data-cy="index-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pinterest`, () => {
const actualId = [`root`,`index-pinterest`];
    cy.clickIfExist(`[data-cy="index-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-google`, () => {
const actualId = [`root`,`index-google`];
    cy.clickIfExist(`[data-cy="index-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-angle-up`, () => {
const actualId = [`root`,`index-angle-up`];
    cy.clickIfExist(`[data-cy="index-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-navbar-collapse`, () => {
const actualId = [`root`,`index-about`,`about-navbar-collapse`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-collapse"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-navbar-brand`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-home`, () => {
const actualId = [`root`,`index-about`,`about-home`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-about`, () => {
const actualId = [`root`,`index-about`,`about-about`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-service`, () => {
const actualId = [`root`,`index-about`,`about-service`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-service"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-courses`, () => {
const actualId = [`root`,`index-about`,`about-courses`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-pricing`, () => {
const actualId = [`root`,`index-about`,`about-pricing`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-contact`, () => {
const actualId = [`root`,`index-about`,`about-contact`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-read-more`, () => {
const actualId = [`root`,`index-about`,`about-read-more`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-read-more"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-collapse-1`, () => {
const actualId = [`root`,`index-about`,`about-collapse-1`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-collapse-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-contact-2`, () => {
const actualId = [`root`,`index-about`,`about-contact-2`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-contact-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-contact-3`, () => {
const actualId = [`root`,`index-about`,`about-contact-3`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-contact-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-contact-4`, () => {
const actualId = [`root`,`index-about`,`about-contact-4`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-contact-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-text-1`, () => {
const actualId = [`root`,`index-about`,`about-text-1`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-text-2`, () => {
const actualId = [`root`,`index-about`,`about-text-2`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-text-3`, () => {
const actualId = [`root`,`index-about`,`about-text-3`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-text-4`, () => {
const actualId = [`root`,`index-about`,`about-text-4`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-text-5`, () => {
const actualId = [`root`,`index-about`,`about-text-5`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-text-6`, () => {
const actualId = [`root`,`index-about`,`about-text-6`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-events`, () => {
const actualId = [`root`,`index-about`,`about-events`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-terms`, () => {
const actualId = [`root`,`index-about`,`about-terms`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-privacy`, () => {
const actualId = [`root`,`index-about`,`about-privacy`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-career`, () => {
const actualId = [`root`,`index-about`,`about-career`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-contact-us`, () => {
const actualId = [`root`,`index-about`,`about-contact-us`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-WebThemez`, () => {
const actualId = [`root`,`index-about`,`about-WebThemez`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-facebook`, () => {
const actualId = [`root`,`index-about`,`about-facebook`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-twitter`, () => {
const actualId = [`root`,`index-about`,`about-twitter`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-linkedin`, () => {
const actualId = [`root`,`index-about`,`about-linkedin`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-pinterest`, () => {
const actualId = [`root`,`index-about`,`about-pinterest`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-google`, () => {
const actualId = [`root`,`index-about`,`about-google`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-about->about-angle-up`, () => {
const actualId = [`root`,`index-about`,`about-angle-up`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-navbar-toggle`, () => {
const actualId = [`root`,`index-services`,`services-navbar-toggle`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-navbar-toggle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-navbar-brand`, () => {
const actualId = [`root`,`index-services`,`services-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-index`, () => {
const actualId = [`root`,`index-services`,`services-index`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-index"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-about`, () => {
const actualId = [`root`,`index-services`,`services-about`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-services`, () => {
const actualId = [`root`,`index-services`,`services-services`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-services"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-courses`, () => {
const actualId = [`root`,`index-services`,`services-courses`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-pricing`, () => {
const actualId = [`root`,`index-services`,`services-pricing`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-contact`, () => {
const actualId = [`root`,`index-services`,`services-contact`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-events`, () => {
const actualId = [`root`,`index-services`,`services-events`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-terms`, () => {
const actualId = [`root`,`index-services`,`services-terms`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-privacy`, () => {
const actualId = [`root`,`index-services`,`services-privacy`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-career`, () => {
const actualId = [`root`,`index-services`,`services-career`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-contact-us`, () => {
const actualId = [`root`,`index-services`,`services-contact-us`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-text-1`, () => {
const actualId = [`root`,`index-services`,`services-text-1`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-text-2`, () => {
const actualId = [`root`,`index-services`,`services-text-2`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-text-3`, () => {
const actualId = [`root`,`index-services`,`services-text-3`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-text-4`, () => {
const actualId = [`root`,`index-services`,`services-text-4`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-text-5`, () => {
const actualId = [`root`,`index-services`,`services-text-5`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-text-6`, () => {
const actualId = [`root`,`index-services`,`services-text-6`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-WebThemez`, () => {
const actualId = [`root`,`index-services`,`services-WebThemez`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-facebook`, () => {
const actualId = [`root`,`index-services`,`services-facebook`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-twitter`, () => {
const actualId = [`root`,`index-services`,`services-twitter`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-linkedin`, () => {
const actualId = [`root`,`index-services`,`services-linkedin`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-pinterest`, () => {
const actualId = [`root`,`index-services`,`services-pinterest`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-google`, () => {
const actualId = [`root`,`index-services`,`services-google`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-services->services-angle-up`, () => {
const actualId = [`root`,`index-services`,`services-angle-up`];
    cy.clickIfExist(`[data-cy="index-services"]`);
      cy.clickIfExist(`[data-cy="services-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-navbar-toggle`, () => {
const actualId = [`root`,`index-courses`,`courses-navbar-toggle`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-navbar-toggle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-navbar-brand`, () => {
const actualId = [`root`,`index-courses`,`courses-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-home`, () => {
const actualId = [`root`,`index-courses`,`courses-home`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-about`, () => {
const actualId = [`root`,`index-courses`,`courses-about`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->about-service`, () => {
const actualId = [`root`,`index-courses`,`about-service`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="about-service"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-courses`, () => {
const actualId = [`root`,`index-courses`,`courses-courses`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-pricing`, () => {
const actualId = [`root`,`index-courses`,`courses-pricing`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-contact`, () => {
const actualId = [`root`,`index-courses`,`courses-contact`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-web-design`, () => {
const actualId = [`root`,`index-courses`,`courses-web-design`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-web-design"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-mobile-app`, () => {
const actualId = [`root`,`index-courses`,`courses-mobile-app`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-mobile-app"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-ui-design`, () => {
const actualId = [`root`,`index-courses`,`courses-ui-design`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-ui-design"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-1`, () => {
const actualId = [`root`,`index-courses`,`courses-work-1`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-1`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-1`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-1`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-1`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-2`, () => {
const actualId = [`root`,`index-courses`,`courses-work-2`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-2`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-2`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-2`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-2`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-3`, () => {
const actualId = [`root`,`index-courses`,`courses-work-3`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-3`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-3`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-3`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-3`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-4`, () => {
const actualId = [`root`,`index-courses`,`courses-work-4`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-4`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-4`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-4`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-4`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-5`, () => {
const actualId = [`root`,`index-courses`,`courses-work-5`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-5`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-5`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-5`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-5`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-6`, () => {
const actualId = [`root`,`index-courses`,`courses-work-6`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-6`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-6`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-6`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-6`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-7`, () => {
const actualId = [`root`,`index-courses`,`courses-work-7`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-7"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-7`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-7`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-7"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-7`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-7`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-7"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-work-8`, () => {
const actualId = [`root`,`index-courses`,`courses-work-8`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-work-8"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-course-title-8`, () => {
const actualId = [`root`,`index-courses`,`courses-course-title-8`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-course-title-8"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-learn-more-8`, () => {
const actualId = [`root`,`index-courses`,`courses-learn-more-8`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-learn-more-8"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-events`, () => {
const actualId = [`root`,`index-courses`,`courses-events`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-terms`, () => {
const actualId = [`root`,`index-courses`,`courses-terms`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-privacy`, () => {
const actualId = [`root`,`index-courses`,`courses-privacy`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-career`, () => {
const actualId = [`root`,`index-courses`,`courses-career`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-contact-us`, () => {
const actualId = [`root`,`index-courses`,`courses-contact-us`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-text-1`, () => {
const actualId = [`root`,`index-courses`,`courses-text-1`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-text-2`, () => {
const actualId = [`root`,`index-courses`,`courses-text-2`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-text-3`, () => {
const actualId = [`root`,`index-courses`,`courses-text-3`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-text-4`, () => {
const actualId = [`root`,`index-courses`,`courses-text-4`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-text-5`, () => {
const actualId = [`root`,`index-courses`,`courses-text-5`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-text-6`, () => {
const actualId = [`root`,`index-courses`,`courses-text-6`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-WebThemez`, () => {
const actualId = [`root`,`index-courses`,`courses-WebThemez`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-facebook`, () => {
const actualId = [`root`,`index-courses`,`courses-facebook`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-twitter`, () => {
const actualId = [`root`,`index-courses`,`courses-twitter`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-linkedin`, () => {
const actualId = [`root`,`index-courses`,`courses-linkedin`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-pinterest`, () => {
const actualId = [`root`,`index-courses`,`courses-pinterest`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-google`, () => {
const actualId = [`root`,`index-courses`,`courses-google`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-courses->courses-angle-up`, () => {
const actualId = [`root`,`index-courses`,`courses-angle-up`];
    cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.clickIfExist(`[data-cy="courses-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-navbar-collapse`, () => {
const actualId = [`root`,`index-pricing`,`pricing-navbar-collapse`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-navbar-collapse"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-navbar-brand`, () => {
const actualId = [`root`,`index-pricing`,`pricing-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-home`, () => {
const actualId = [`root`,`index-pricing`,`pricing-home`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-about`, () => {
const actualId = [`root`,`index-pricing`,`pricing-about`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-service`, () => {
const actualId = [`root`,`index-pricing`,`pricing-service`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-service"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-courses`, () => {
const actualId = [`root`,`index-pricing`,`pricing-courses`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-pricing`, () => {
const actualId = [`root`,`index-pricing`,`pricing-pricing`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-contact`, () => {
const actualId = [`root`,`index-pricing`,`pricing-contact`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-1`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-1`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-get-now-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-2`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-2`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-get-now-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-3`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-3`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-get-now-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-get-now-4`, () => {
const actualId = [`root`,`index-pricing`,`pricing-get-now-4`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-get-now-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-events`, () => {
const actualId = [`root`,`index-pricing`,`pricing-events`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-terms`, () => {
const actualId = [`root`,`index-pricing`,`pricing-terms`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-privacy`, () => {
const actualId = [`root`,`index-pricing`,`pricing-privacy`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-career`, () => {
const actualId = [`root`,`index-pricing`,`pricing-career`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-contact-us`, () => {
const actualId = [`root`,`index-pricing`,`pricing-contact-us`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-text-1`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-1`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-text-2`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-2`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-text-3`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-3`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-text-4`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-4`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-text-5`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-5`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-text-6`, () => {
const actualId = [`root`,`index-pricing`,`pricing-text-6`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-WebThemez`, () => {
const actualId = [`root`,`index-pricing`,`pricing-WebThemez`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-facebook`, () => {
const actualId = [`root`,`index-pricing`,`pricing-facebook`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-twitter`, () => {
const actualId = [`root`,`index-pricing`,`pricing-twitter`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-linkedin`, () => {
const actualId = [`root`,`index-pricing`,`pricing-linkedin`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-pinterest`, () => {
const actualId = [`root`,`index-pricing`,`pricing-pinterest`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-google`, () => {
const actualId = [`root`,`index-pricing`,`pricing-google`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-pricing->pricing-angle-up`, () => {
const actualId = [`root`,`index-pricing`,`pricing-angle-up`];
    cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.clickIfExist(`[data-cy="pricing-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-navbar-collapse`, () => {
const actualId = [`root`,`index-contact`,`contact-navbar-collapse`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-navbar-collapse"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-navbar-brand`, () => {
const actualId = [`root`,`index-contact`,`contact-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-home`, () => {
const actualId = [`root`,`index-contact`,`contact-home`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-about`, () => {
const actualId = [`root`,`index-contact`,`contact-about`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-services`, () => {
const actualId = [`root`,`index-contact`,`contact-services`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-services"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-courses`, () => {
const actualId = [`root`,`index-contact`,`contact-courses`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-pricing`, () => {
const actualId = [`root`,`index-contact`,`contact-pricing`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-contact`, () => {
const actualId = [`root`,`index-contact`,`contact-contact`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->close-button`, () => {
const actualId = [`root`,`index-contact`,`close-button`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="close-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-events`, () => {
const actualId = [`root`,`index-contact`,`contact-events`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-terms`, () => {
const actualId = [`root`,`index-contact`,`contact-terms`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-privacy`, () => {
const actualId = [`root`,`index-contact`,`contact-privacy`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-career`, () => {
const actualId = [`root`,`index-contact`,`contact-career`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-contact-us`, () => {
const actualId = [`root`,`index-contact`,`contact-contact-us`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-text-1`, () => {
const actualId = [`root`,`index-contact`,`contact-text-1`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-text-2`, () => {
const actualId = [`root`,`index-contact`,`contact-text-2`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-text-3`, () => {
const actualId = [`root`,`index-contact`,`contact-text-3`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-text-4`, () => {
const actualId = [`root`,`index-contact`,`contact-text-4`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-text-5`, () => {
const actualId = [`root`,`index-contact`,`contact-text-5`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-text-6`, () => {
const actualId = [`root`,`index-contact`,`contact-text-6`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-WebThemez`, () => {
const actualId = [`root`,`index-contact`,`contact-WebThemez`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-facebook`, () => {
const actualId = [`root`,`index-contact`,`contact-facebook`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-twitter`, () => {
const actualId = [`root`,`index-contact`,`contact-twitter`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-linkedin`, () => {
const actualId = [`root`,`index-contact`,`contact-linkedin`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-pinterest`, () => {
const actualId = [`root`,`index-contact`,`contact-pinterest`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-google`, () => {
const actualId = [`root`,`index-contact`,`contact-google`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element index-contact->contact-angle-up`, () => {
const actualId = [`root`,`index-contact`,`contact-angle-up`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.clickIfExist(`[data-cy="contact-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values index-contact->name-input-email-input-message-textarea-submit-input and submit`, () => {
const actualId = [`root`,`index-contact`,`name-input-email-input-message-textarea-submit-input`];
    cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.fillInput(`[data-cy="name-input"]`, `Credit Card Account`);
cy.fillInput(`[data-cy="email-input"]`, `neural`);
cy.fillInput(`[data-cy="message-textarea"]`, `Publickey`);
cy.fillInput(`[data-cy="submit-input"]`, `withdrawal`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-navbar-toggle`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-navbar-toggle`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-navbar-toggle"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-navbar-brand`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-navbar-brand`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-navbar-brand"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-index`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-index`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-index"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-about`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-about`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-about"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-services`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-services`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-services"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-courses`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-courses`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-courses"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-pricing`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-pricing`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-pricing"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-contact`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-contact`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-contact"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-read-more-1`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-read-more-1`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-read-more-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-read-more-2`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-read-more-2`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-read-more-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-img-1`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-img-1`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-img-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-img-2`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-img-2`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-img-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-img-3`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-img-3`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-img-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-events`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-events`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-events"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-terms`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-terms`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-terms"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-privacy`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-privacy`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-privacy"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-career`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-career`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-career"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-contact-us`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-contact-us`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-contact-us"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-1`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-1`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-text-1"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-2`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-2`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-text-2"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-3`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-3`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-text-3"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-4`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-4`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-text-4"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-5`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-5`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-text-5"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-text-6`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-text-6`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-text-6"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-WebThemez`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-WebThemez`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-WebThemez"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-facebook`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-facebook`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-facebook"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-twitter`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-twitter`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-twitter"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-linkedin`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-linkedin`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-linkedin"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-pinterest`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-pinterest`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-pinterest"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-google`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-google`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-google"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element index-about->about-navbar-brand->index-angle-up`, () => {
const actualId = [`root`,`index-about`,`about-navbar-brand`,`index-angle-up`];
    cy.clickIfExist(`[data-cy="index-about"]`);
      cy.clickIfExist(`[data-cy="about-navbar-brand"]`);
      cy.clickIfExist(`[data-cy="index-angle-up"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
