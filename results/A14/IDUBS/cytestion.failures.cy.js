describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-agendamentos->560530743-voltar`, () => {
    cy.visit('http://system-A14/relatorios/livro-iss-padrao?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~346173D%7C%7C346173&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="560530743-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/livro-iss-53->2093155880-agendamentos->2093155880-voltar`, () => {
    cy.visit('http://system-A14/relatorios/livro-iss-53?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~210493D%7C%7C210493&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2093155880-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element relatorios->relatorios/livro-iss-56->2093155883-agendamentos->2093155883-voltar`, () => {
    cy.visit('http://system-A14/relatorios/livro-iss-56?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~242342D%7C%7C242342&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2093155883-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-agendamentos->1788893911-voltar`, () => {
    cy.visit(
      'http://system-A14/nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~18032543D%7C%7C18032543&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1788893911-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-agendamentos->3637687372-voltar`, () => {
    cy.visit(
      'http://system-A14/nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~27719627D%7C%7C27719627&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o'
    );
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3637687372-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-agendamentos->1227584939-voltar`, () => {
    cy.visit('http://system-A14/obrigacoes/processos/geracao-nfts?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47339918D%7C%7C47339918&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1227584939-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-agendamentos->774170564-voltar`, () => {
    cy.visit('http://system-A14/obrigacoes/processos/importacao-nfts?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~47339922D%7C%7C47339922&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="774170564-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
