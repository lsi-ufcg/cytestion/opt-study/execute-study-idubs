describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
const actualId = [`root`,`home`];
    cy.clickIfExist(`[data-cy="home"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais`, () => {
const actualId = [`root`,`tabelas-oficiais`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas`, () => {
const actualId = [`root`,`tabelas-corporativas`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes`, () => {
const actualId = [`root`,`transacoes`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse`, () => {
const actualId = [`root`,`nfse`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss`, () => {
const actualId = [`root`,`apuracao-iss`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
const actualId = [`root`,`obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element paineis`, () => {
const actualId = [`root`,`paineis`];
    cy.clickIfExist(`[data-cy="paineis"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios`, () => {
const actualId = [`root`,`relatorios`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
const actualId = [`root`,`processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download`, () => {
const actualId = [`root`,`download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
const actualId = [`root`,`collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
const actualId = [`root`,`modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 3791242893-exibir dados`, () => {
const actualId = [`root`,`3791242893-exibir dados`];
    cy.clickIfExist(`[data-cy="3791242893-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/codigo-generico`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/codigo-generico`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/codigo-generico"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquota-iss`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/aliquota-iss`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/aliquota-iss"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/aliquota-generica`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/aliquota-generica`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/aliquota-generica"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/tabela-progressiva-irrf`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/tabela-progressiva-irrf`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/tabela-progressiva-irrf"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/financeiros`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/financeiros`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/financeiros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/parametrizacao-geral`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/parametrizacao-geral`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/parametrizacao-geral"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-corporativas->tabelas-corporativas/servicos`, () => {
const actualId = [`root`,`tabelas-corporativas`,`tabelas-corporativas/servicos`];
    cy.clickIfExist(`[data-cy="tabelas-corporativas"]`);
      cy.clickIfExist(`[data-cy="tabelas-corporativas/servicos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element transacoes->transacoes/digitacao-manutencao-dof`, () => {
const actualId = [`root`,`transacoes`,`transacoes/digitacao-manutencao-dof`];
    cy.clickIfExist(`[data-cy="transacoes"]`);
      cy.clickIfExist(`[data-cy="transacoes/digitacao-manutencao-dof"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/configuracao-obrigacao-nfe`, () => {
const actualId = [`root`,`nfse`,`nfse/configuracao-obrigacao-nfe`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/configuracao-obrigacao-nfe"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/exportar`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/exportar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/exportar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/importar`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/importar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/importar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss->apuracao-iss/regras`, () => {
const actualId = [`root`,`apuracao-iss`,`apuracao-iss/regras`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/regras"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss->apuracao-iss/apuracao`, () => {
const actualId = [`root`,`apuracao-iss`,`apuracao-iss/apuracao`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/apuracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio-arquivos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element paineis->paineis/servicos-prestados`, () => {
const actualId = [`root`,`paineis`,`paineis/servicos-prestados`];
    cy.clickIfExist(`[data-cy="paineis"]`);
      cy.clickIfExist(`[data-cy="paineis/servicos-prestados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element paineis->paineis/servicos-tomados`, () => {
const actualId = [`root`,`paineis`,`paineis/servicos-tomados`];
    cy.clickIfExist(`[data-cy="paineis"]`);
      cy.clickIfExist(`[data-cy="paineis/servicos-tomados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados-snapshot`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados-snapshot`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados-snapshot"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados-snapshot`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados-snapshot`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados-snapshot"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/guia-complementar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/guia-complementar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/guia-complementar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/driss`, () => {
const actualId = [`root`,`relatorios`,`relatorios/driss`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/driss"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->2326448105-power-search-button`, () => {
const actualId = [`root`,`download`,`2326448105-power-search-button`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="2326448105-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->2326448105-download`, () => {
const actualId = [`root`,`download`,`2326448105-download`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="2326448105-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->2326448105-detalhes`, () => {
const actualId = [`root`,`download`,`2326448105-detalhes`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="2326448105-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element download->2326448105-excluir`, () => {
const actualId = [`root`,`download`,`2326448105-excluir`];
    cy.clickIfExist(`[data-cy="download"]`);
      cy.clickIfExist(`[data-cy="2326448105-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element tabelas-oficiais->tabelas-oficiais/financeiros->tabelas-oficiais/financeiros/juros-iss`, () => {
const actualId = [`root`,`tabelas-oficiais`,`tabelas-oficiais/financeiros`,`tabelas-oficiais/financeiros/juros-iss`];
    cy.clickIfExist(`[data-cy="tabelas-oficiais"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/financeiros"]`);
      cy.clickIfExist(`[data-cy="tabelas-oficiais/financeiros/juros-iss"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/configuracao-obrigacao-nfe->822772261-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/configuracao-obrigacao-nfe`,`822772261-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/configuracao-obrigacao-nfe"]`);
      cy.clickIfExist(`[data-cy="822772261-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/configuracao-obrigacao-nfe->822772261-eyeoutlined`, () => {
const actualId = [`root`,`nfse`,`nfse/configuracao-obrigacao-nfe`,`822772261-eyeoutlined`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/configuracao-obrigacao-nfe"]`);
      cy.clickIfExist(`[data-cy="822772261-eyeoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/exportar->182389851-operações em lote`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/exportar`,`182389851-operações em lote`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/exportar"]`);
      cy.clickIfExist(`[data-cy="182389851-operações em lote"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/exportar->182389851-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/exportar`,`182389851-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/exportar"]`);
      cy.clickIfExist(`[data-cy="182389851-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/exportar->182389851-executar`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/exportar`,`182389851-executar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/exportar"]`);
      cy.clickIfExist(`[data-cy="182389851-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/importacao-exportacao/exportar->182389851-powerselect-municipio-182389851-powerselect-estabelecimento and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/exportar`,`182389851-powerselect-municipio-182389851-powerselect-estabelecimento`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/exportar"]`);
      cy.fillInputPowerSelect(`[data-cy="182389851-powerselect-municipio"] input`);
cy.fillInputPowerSelect(`[data-cy="182389851-powerselect-estabelecimento"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/importar->1686057996-limpar`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/importar`,`1686057996-limpar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/importar"]`);
      cy.clickIfExist(`[data-cy="1686057996-limpar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/importacao-exportacao/importar->1686057996-powerselect-municipio-1686057996-powerselect-estabelecimento-1686057996-radio-tipoArquivo and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/importar`,`1686057996-powerselect-municipio-1686057996-powerselect-estabelecimento-1686057996-radio-tipoArquivo`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/importar"]`);
      cy.fillInputPowerSelect(`[data-cy="1686057996-powerselect-municipio"] input`);
cy.fillInputPowerSelect(`[data-cy="1686057996-powerselect-estabelecimento"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="1686057996-radio-tipoArquivo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/configuracao-automacao`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/nfse-lote`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/nfse-lote`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/nfse-lote"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/integracao`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/integracao`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/integracao"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss->apuracao-iss/regras->apuracao-iss/regras/limp`, () => {
const actualId = [`root`,`apuracao-iss`,`apuracao-iss/regras`,`apuracao-iss/regras/limp`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/regras"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/regras/limp"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss->apuracao-iss/regras->apuracao-iss/regras/grid`, () => {
const actualId = [`root`,`apuracao-iss`,`apuracao-iss/regras`,`apuracao-iss/regras/grid`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/regras"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/regras/grid"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss->apuracao-iss/apuracao->apuracao-iss/apuracao/geracao-impostos-lancamento-impostos`, () => {
const actualId = [`root`,`apuracao-iss`,`apuracao-iss/apuracao`,`apuracao-iss/apuracao/geracao-impostos-lancamento-impostos`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/apuracao"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/apuracao/geracao-impostos-lancamento-impostos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao-iss->apuracao-iss/apuracao->apuracao-iss/apuracao/apuracao-grid`, () => {
const actualId = [`root`,`apuracao-iss`,`apuracao-iss/apuracao`,`apuracao-iss/apuracao/apuracao-grid`];
    cy.clickIfExist(`[data-cy="apuracao-iss"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/apuracao"]`);
      cy.clickIfExist(`[data-cy="apuracao-iss/apuracao/apuracao-grid"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-gerenciar labels`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-gerenciar labels"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-visualizar parâmetros`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-visualizar parâmetros"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-visualizar/editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-visualizar/editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ir para todas as obrigações`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ir para todas as obrigações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajuda`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajuda"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-nova solicitação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-nova solicitação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-atualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-atualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-gerar obrigação`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-gerar obrigação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-gerar obrigação"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-visualizar resultado da geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-visualizar resultado da geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-visualizar resultado da geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-protocolo transmissão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-protocolo transmissão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-protocolo transmissão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-excluir geração`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-excluir geração`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-excluir geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1253435338-novo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1253435338-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1253435338-novo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1253435338-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1253435338-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1253435338-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1253435338-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1253435338-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1253435338-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos->959680688-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio-arquivos`,`959680688-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
      cy.clickIfExist(`[data-cy="959680688-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos->959680688-gerar arquivo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio-arquivos`,`959680688-gerar arquivo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
      cy.clickIfExist(`[data-cy="959680688-gerar arquivo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/diretorio-arquivos->959680688-excluir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio-arquivos`,`959680688-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
      cy.clickIfExist(`[data-cy="959680688-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/diretorio-arquivos->959680688-power-search-input and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/diretorio-arquivos`,`959680688-power-search-input`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/diretorio-arquivos"]`);
      cy.fillInputPowerSearch(`[data-cy="959680688-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element paineis->paineis/servicos-prestados->2193274730-exibir dados`, () => {
const actualId = [`root`,`paineis`,`paineis/servicos-prestados`,`2193274730-exibir dados`];
    cy.clickIfExist(`[data-cy="paineis"]`);
      cy.clickIfExist(`[data-cy="paineis/servicos-prestados"]`);
      cy.clickIfExist(`[data-cy="2193274730-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element paineis->paineis/servicos-tomados->3576039736-exibir dados`, () => {
const actualId = [`root`,`paineis`,`paineis/servicos-tomados`,`3576039736-exibir dados`];
    cy.clickIfExist(`[data-cy="paineis"]`);
      cy.clickIfExist(`[data-cy="paineis/servicos-tomados"]`);
      cy.clickIfExist(`[data-cy="3576039736-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados->1530342401-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados`,`1530342401-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados"]`);
      cy.clickIfExist(`[data-cy="1530342401-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados->1530342401-requisitos de geração`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados`,`1530342401-requisitos de geração`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados"]`);
      cy.clickIfExist(`[data-cy="1530342401-requisitos de geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados->1530342401-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados`,`1530342401-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados"]`);
      cy.clickIfExist(`[data-cy="1530342401-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/servicos-prestados->1530342401-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados`,`1530342401-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados"]`);
      cy.fillInputPowerSearch(`[data-cy="1530342401-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados-snapshot->124132880-exibir dados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados-snapshot`,`124132880-exibir dados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados-snapshot"]`);
      cy.clickIfExist(`[data-cy="124132880-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados->1318369167-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados`,`1318369167-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados"]`);
      cy.clickIfExist(`[data-cy="1318369167-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados->1318369167-requisitos de geração`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados`,`1318369167-requisitos de geração`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados"]`);
      cy.clickIfExist(`[data-cy="1318369167-requisitos de geração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados->1318369167-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados`,`1318369167-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados"]`);
      cy.clickIfExist(`[data-cy="1318369167-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/servicos-tomados->1318369167-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados`,`1318369167-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados"]`);
      cy.fillInputPowerSearch(`[data-cy="1318369167-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados-snapshot->3989942850-exibir dados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados-snapshot`,`3989942850-exibir dados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados-snapshot"]`);
      cy.clickIfExist(`[data-cy="3989942850-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-regerar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-regerar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-regerar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-detalhes`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-detalhes`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-detalhes"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-excluir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-excluir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-excluir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-abrir visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-abrir visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-executar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-executar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-agendamentos`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-agendamentos`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-visualização`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-visualização`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/guia-complementar->49384815-exibir dados`, () => {
const actualId = [`root`,`relatorios`,`relatorios/guia-complementar`,`49384815-exibir dados`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/guia-complementar"]`);
      cy.clickIfExist(`[data-cy="49384815-exibir dados"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/driss->1874349932-operações em lote`, () => {
const actualId = [`root`,`relatorios`,`relatorios/driss`,`1874349932-operações em lote`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/driss"]`);
      cy.clickIfExist(`[data-cy="1874349932-operações em lote"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/driss->1874349932-power-search-button`, () => {
const actualId = [`root`,`relatorios`,`relatorios/driss`,`1874349932-power-search-button`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/driss"]`);
      cy.clickIfExist(`[data-cy="1874349932-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/driss->1874349932-power-search-input and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/driss`,`1874349932-power-search-input`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/driss"]`);
      cy.fillInputPowerSearch(`[data-cy="1874349932-power-search-input"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/configuracao-obrigacao-nfe->822772261-eyeoutlined->2220715887-dashoutlined`, () => {
const actualId = [`root`,`nfse`,`nfse/configuracao-obrigacao-nfe`,`822772261-eyeoutlined`,`2220715887-dashoutlined`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/configuracao-obrigacao-nfe"]`);
      cy.clickIfExist(`[data-cy="822772261-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2220715887-dashoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/configuracao-obrigacao-nfe->822772261-eyeoutlined->2220715887-voltar`, () => {
const actualId = [`root`,`nfse`,`nfse/configuracao-obrigacao-nfe`,`822772261-eyeoutlined`,`2220715887-voltar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/configuracao-obrigacao-nfe"]`);
      cy.clickIfExist(`[data-cy="822772261-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2220715887-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/importacao-exportacao/exportar->182389851-operações em lote->182389851-cancelar`, () => {
const actualId = [`root`,`nfse`,`nfse/importacao-exportacao/exportar`,`182389851-operações em lote`,`182389851-cancelar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/importacao-exportacao/exportar"]`);
      cy.clickIfExist(`[data-cy="182389851-operações em lote"]`);
      cy.clickIfExist(`[data-cy="182389851-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao->4098540531-dashoutlined`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/configuracao-automacao`,`4098540531-dashoutlined`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
      cy.clickIfExist(`[data-cy="4098540531-dashoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao->4098540531-salvar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/configuracao-automacao`,`4098540531-salvar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
      cy.clickIfExist(`[data-cy="4098540531-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao->4098540531-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/configuracao-automacao`,`4098540531-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
      cy.clickIfExist(`[data-cy="4098540531-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao->4098540531-powerselect-ibgeCodigo-4098540531-radio-indAtivo and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/configuracao-automacao`,`4098540531-powerselect-ibgeCodigo-4098540531-radio-indAtivo`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
      cy.fillInputPowerSelect(`[data-cy="4098540531-powerselect-ibgeCodigo"] input`);
cy.fillInputCheckboxOrRadio(`[data-cy="4098540531-radio-indAtivo"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/nfse-lote->833800008-mais operações`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/nfse-lote`,`833800008-mais operações`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/nfse-lote"]`);
      cy.clickIfExist(`[data-cy="833800008-mais operações"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/nfse-lote->833800008-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/nfse-lote`,`833800008-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/nfse-lote"]`);
      cy.clickIfExist(`[data-cy="833800008-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/integracao->1833717869-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/integracao`,`1833717869-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/integracao"]`);
      cy.clickIfExist(`[data-cy="1833717869-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/integracao->1833717869-powerselect-municipio and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/integracao`,`1833717869-powerselect-municipio`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/integracao"]`);
      cy.fillInputPowerSelect(`[data-cy="1833717869-powerselect-municipio"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-executar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-executar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-agendamentos`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-agendamentos`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-visualização`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-visualização`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-executar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-executar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-agendamentos`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-agendamentos`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-power-search-button`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-power-search-button`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-visualização`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-visualização`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-gerenciar labels->1194603578-fechar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-gerenciar labels`,`1194603578-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-gerenciar labels"]`);
      cy.clickIfExist(`[data-cy="1194603578-fechar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-visualizar/editar->3321469143-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-visualizar/editar`,`3321469143-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3321469143-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/propriedades->1194603578-visualizar/editar->3321469143-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/propriedades`,`1194603578-visualizar/editar`,`3321469143-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/propriedades"]`);
      cy.clickIfExist(`[data-cy="1194603578-visualizar/editar"]`);
      cy.clickIfExist(`[data-cy="3321469143-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ir para todas as obrigações->2771762121-voltar às obrigações do módulo`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ir para todas as obrigações`,`2771762121-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ir para todas as obrigações"]`);
      cy.clickIfExist(`[data-cy="2771762121-voltar às obrigações do módulo"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-nova solicitação->2771762121-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-nova solicitação`,`2771762121-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="2771762121-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-nova solicitação->2771762121-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-nova solicitação`,`2771762121-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-nova solicitação"]`);
      cy.clickIfExist(`[data-cy="2771762121-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-abrir visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-visualizar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualizar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração->1784032734-rightoutlined`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`,`1784032734-rightoutlined`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1784032734-rightoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração->1784032734-habilitar edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`,`1784032734-habilitar edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1784032734-habilitar edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração->1784032734-abrir janela de edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`,`1784032734-abrir janela de edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1784032734-abrir janela de edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-visualizar resultado da geração->2771762121-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-visualizar resultado da geração`,`2771762121-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="2771762121-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-visualizar resultado da geração->2771762121-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-visualizar resultado da geração`,`2771762121-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="2771762121-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-visualizar resultado da geração->2771762121-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-visualizar resultado da geração`,`2771762121-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="2771762121-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-visualizar resultado da geração->2771762121-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-visualizar resultado da geração`,`2771762121-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="2771762121-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-protocolo transmissão->2514547938-editar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-protocolo transmissão`,`2514547938-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="2514547938-editar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-visualização->3960763378-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-visualização`,`3960763378-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3960763378-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-abrir visualização->3960763378-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-abrir visualização`,`3960763378-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-abrir visualização->3960763378-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-abrir visualização`,`3960763378-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-abrir visualização->3960763378-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-abrir visualização`,`3960763378-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-abrir visualização->3960763378-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-abrir visualização`,`3960763378-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-visualizar->3960763378-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-visualizar`,`3960763378-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualizar"]`);
      cy.clickIfExist(`[data-cy="3960763378-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-novo->2738124138-criar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-novo`,`2738124138-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-novo"]`);
      cy.clickIfExist(`[data-cy="2738124138-criar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-novo->2738124138-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-novo`,`2738124138-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-novo"]`);
      cy.clickIfExist(`[data-cy="2738124138-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->2738124138-novo->2738124138-input-number-ano and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-novo`,`2738124138-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-novo"]`);
      cy.fillInput(`[data-cy="2738124138-input-number-ano"] textarea`, `5`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-editar->2738124138-remover item`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-editar`,`2738124138-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-editar"]`);
      cy.clickIfExist(`[data-cy="2738124138-remover item"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->2738124138-editar->2738124138-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/periodicidade`,`2738124138-editar`,`2738124138-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
      cy.clickIfExist(`[data-cy="2738124138-editar"]`);
      cy.clickIfExist(`[data-cy="2738124138-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->1253435338-novo->1253435338-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/configuracao-estabelecimento`,`1253435338-novo`,`1253435338-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
      cy.clickIfExist(`[data-cy="1253435338-novo"]`);
      cy.clickIfExist(`[data-cy="1253435338-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-executar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-executar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-executar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-executar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-executar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-agendamentos`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-agendamentos"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-visualização`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-visualização"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-prestados->1530342401-visualização->1530342401-salvar configuração`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-prestados`,`1530342401-visualização`,`1530342401-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-prestados"]`);
      cy.clickIfExist(`[data-cy="1530342401-visualização"]`);
      cy.clickIfExist(`[data-cy="1530342401-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/servicos-tomados->1318369167-visualização->1318369167-salvar configuração`, () => {
const actualId = [`root`,`relatorios`,`relatorios/servicos-tomados`,`1318369167-visualização`,`1318369167-salvar configuração`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/servicos-tomados"]`);
      cy.clickIfExist(`[data-cy="1318369167-visualização"]`);
      cy.clickIfExist(`[data-cy="1318369167-salvar configuração"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-executar->560530743-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-executar`,`560530743-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-executar"]`);
      cy.clickIfExist(`[data-cy="560530743-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-executar->560530743-requisitos de preenchimento`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-executar`,`560530743-requisitos de preenchimento`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-executar"]`);
      cy.clickIfExist(`[data-cy="560530743-requisitos de preenchimento"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-executar->560530743-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-executar`,`560530743-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-executar"]`);
      cy.clickIfExist(`[data-cy="560530743-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/livro-iss-padrao->560530743-executar->560530743-input-PAGINA and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-executar`,`560530743-input-PAGINA`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-executar"]`);
      cy.fillInput(`[data-cy="560530743-input-PAGINA"] textarea`, `Mexican Peso Mexican Unidad de Inversion UDI`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-agendamentos->560530743-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-agendamentos`,`560530743-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-agendamentos"]`);
      cy.clickIfExist(`[data-cy="560530743-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/livro-iss-padrao->560530743-visualização->560530743-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-visualização`,`560530743-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="560530743-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-detalhes->560530743-dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-detalhes`,`560530743-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-detalhes"]`);
      cy.clickIfExist(`[data-cy="560530743-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-abrir visualização->560530743-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-abrir visualização`,`560530743-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="560530743-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-abrir visualização->560530743-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-abrir visualização`,`560530743-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="560530743-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-abrir visualização->560530743-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-abrir visualização`,`560530743-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="560530743-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-abrir visualização->560530743-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-abrir visualização`,`560530743-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="560530743-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-executar->2093155880-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-executar`,`2093155880-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-executar"]`);
      cy.clickIfExist(`[data-cy="2093155880-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-executar->2093155880-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-executar`,`2093155880-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-executar"]`);
      cy.clickIfExist(`[data-cy="2093155880-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/livro-iss-53->2093155880-executar->2093155880-input-PAGINA and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-executar`,`2093155880-input-PAGINA`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-executar"]`);
      cy.fillInput(`[data-cy="2093155880-input-PAGINA"] textarea`, `Tocantins`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-agendamentos->2093155880-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-agendamentos`,`2093155880-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2093155880-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/livro-iss-53->2093155880-visualização->2093155880-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-visualização`,`2093155880-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2093155880-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-detalhes->2093155880-não há dados disponíveis para impressão`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-detalhes`,`2093155880-não há dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-detalhes"]`);
      cy.clickIfExist(`[data-cy="2093155880-não há dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-abrir visualização->2093155880-aumentar o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-abrir visualização`,`2093155880-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2093155880-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-abrir visualização->2093155880-diminuir o zoom`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-abrir visualização`,`2093155880-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2093155880-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-abrir visualização->2093155880-expandir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-abrir visualização`,`2093155880-expandir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2093155880-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-abrir visualização->2093155880-download`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-abrir visualização`,`2093155880-download`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2093155880-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-executar->2093155883-múltipla seleção`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-executar`,`2093155883-múltipla seleção`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-executar"]`);
      cy.clickIfExist(`[data-cy="2093155883-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-executar->2093155883-agendar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-executar`,`2093155883-agendar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-executar"]`);
      cy.clickIfExist(`[data-cy="2093155883-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/livro-iss-56->2093155883-executar->2093155883-input-PAGINA and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-executar`,`2093155883-input-PAGINA`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-executar"]`);
      cy.fillInput(`[data-cy="2093155883-input-PAGINA"] textarea`, `Central`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-agendamentos->2093155883-voltar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-agendamentos`,`2093155883-voltar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-agendamentos"]`);
      cy.clickIfExist(`[data-cy="2093155883-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values relatorios->relatorios/livro-iss-56->2093155883-visualização->2093155883-item- and submit`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-visualização`,`2093155883-item-`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="2093155883-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/configuracao-obrigacao-nfe->822772261-eyeoutlined->2220715887-dashoutlined->2220715887-item-`, () => {
const actualId = [`root`,`nfse`,`nfse/configuracao-obrigacao-nfe`,`822772261-eyeoutlined`,`2220715887-dashoutlined`,`2220715887-item-`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/configuracao-obrigacao-nfe"]`);
      cy.clickIfExist(`[data-cy="822772261-eyeoutlined"]`);
      cy.clickIfExist(`[data-cy="2220715887-dashoutlined"]`);
      cy.clickIfExist(`[data-cy="2220715887-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao->4098540531-dashoutlined->4098540531-item-`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-nfse-prefeitura`,`nfse/integracao-nfse-prefeitura/configuracao-automacao`,`4098540531-dashoutlined`,`4098540531-item-`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
      cy.clickIfExist(`[data-cy="4098540531-dashoutlined"]`);
      cy.clickIfExist(`[data-cy="4098540531-item-"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-executar->1788893911-múltipla seleção`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-executar`,`1788893911-múltipla seleção`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-executar"]`);
      cy.clickIfExist(`[data-cy="1788893911-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-executar->1788893911-agendar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-executar`,`1788893911-agendar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-executar"]`);
      cy.clickIfExist(`[data-cy="1788893911-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-executar->1788893911-input-pIntervaloHora and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-executar`,`1788893911-input-pIntervaloHora`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-executar"]`);
      cy.fillInput(`[data-cy="1788893911-input-pIntervaloHora"] textarea`, `Intelligent Rubber Mouse`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-agendamentos->1788893911-voltar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-agendamentos`,`1788893911-voltar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-agendamentos"]`);
      cy.clickIfExist(`[data-cy="1788893911-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-visualização->1788893911-item- and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-visualização`,`1788893911-item-`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1788893911-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-executar->3637687372-múltipla seleção`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-executar`,`3637687372-múltipla seleção`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-executar"]`);
      cy.clickIfExist(`[data-cy="3637687372-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-executar->3637687372-agendar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-executar`,`3637687372-agendar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-executar"]`);
      cy.clickIfExist(`[data-cy="3637687372-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-agendamentos->3637687372-voltar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-agendamentos`,`3637687372-voltar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3637687372-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-visualização->3637687372-item- and submit`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-visualização`,`3637687372-item-`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3637687372-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-visualização->3960763378-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-visualização`,`3960763378-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="3960763378-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-abrir visualização->3960763378-aumentar o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-abrir visualização`,`3960763378-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-aumentar o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-abrir visualização->3960763378-diminuir o zoom`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-abrir visualização`,`3960763378-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-diminuir o zoom"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-abrir visualização->3960763378-expandir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-abrir visualização`,`3960763378-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-expandir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-abrir visualização->3960763378-download`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-abrir visualização`,`3960763378-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-download"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-visualizar->3960763378-dados disponíveis para impressão`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-visualizar`,`3960763378-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-visualizar"]`);
      cy.clickIfExist(`[data-cy="3960763378-dados disponíveis para impressão"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração->1784032734-rightoutlined->1784032734-downoutlined`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`,`1784032734-rightoutlined`,`1784032734-downoutlined`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1784032734-rightoutlined"]`);
      cy.clickIfExist(`[data-cy="1784032734-downoutlined"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração->1784032734-habilitar edição->1784032734-confirmar  edição`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`,`1784032734-habilitar edição`,`1784032734-confirmar  edição`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1784032734-habilitar edição"]`);
      cy.clickIfExist(`[data-cy="1784032734-confirmar  edição"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-ajustar parâmetros da geração->1784032734-abrir janela de edição->1784032734-power-search-button`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-ajustar parâmetros da geração`,`1784032734-abrir janela de edição`,`1784032734-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-ajustar parâmetros da geração"]`);
      cy.clickIfExist(`[data-cy="1784032734-abrir janela de edição"]`);
      cy.clickIfExist(`[data-cy="1784032734-power-search-button"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-visualizar resultado da geração->2771762121-expandir->2771762121-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-visualizar resultado da geração`,`2771762121-expandir`,`2771762121-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-visualizar resultado da geração"]`);
      cy.clickIfExist(`[data-cy="2771762121-expandir"]`);
      cy.clickIfExist(`[data-cy="2771762121-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-protocolo transmissão->2514547938-editar->2514547938-salvar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-protocolo transmissão`,`2514547938-editar`,`2514547938-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="2514547938-editar"]`);
      cy.clickIfExist(`[data-cy="2514547938-salvar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-protocolo transmissão->2514547938-editar->2514547938-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-protocolo transmissão`,`2514547938-editar`,`2514547938-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-protocolo transmissão"]`);
      cy.clickIfExist(`[data-cy="2514547938-editar"]`);
      cy.clickIfExist(`[data-cy="2514547938-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->3960763378-abrir visualização->3960763378-expandir->3960763378-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/obrigacoes-executadas`,`3960763378-abrir visualização`,`3960763378-expandir`,`3960763378-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-expandir"]`);
      cy.clickIfExist(`[data-cy="3960763378-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-executar->1227584939-múltipla seleção`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-executar`,`1227584939-múltipla seleção`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-executar"]`);
      cy.clickIfExist(`[data-cy="1227584939-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-executar->1227584939-agendar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-executar`,`1227584939-agendar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-executar"]`);
      cy.clickIfExist(`[data-cy="1227584939-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-visualização->1227584939-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-visualização`,`1227584939-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="1227584939-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-executar->774170564-múltipla seleção`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-executar`,`774170564-múltipla seleção`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-executar"]`);
      cy.clickIfExist(`[data-cy="774170564-múltipla seleção"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-executar->774170564-agendar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-executar`,`774170564-agendar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-executar"]`);
      cy.clickIfExist(`[data-cy="774170564-agendar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-agendamentos->774170564-voltar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-agendamentos`,`774170564-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-agendamentos"]`);
      cy.clickIfExist(`[data-cy="774170564-voltar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-visualização->774170564-item- and submit`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-visualização`,`774170564-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-visualização"]`);
      cy.fillInputCheckboxOrRadio(`[data-cy="774170564-item-"] input`);
cy.submitIfExist(`.ant-form`);

      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-executar->560530743-múltipla seleção->560530743-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-executar`,`560530743-múltipla seleção`,`560530743-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-executar"]`);
      cy.clickIfExist(`[data-cy="560530743-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="560530743-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-padrao->560530743-abrir visualização->560530743-expandir->560530743-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-padrao`,`560530743-abrir visualização`,`560530743-expandir`,`560530743-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-padrao"]`);
      cy.clickIfExist(`[data-cy="560530743-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="560530743-expandir"]`);
      cy.clickIfExist(`[data-cy="560530743-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-executar->2093155880-múltipla seleção->2093155880-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-executar`,`2093155880-múltipla seleção`,`2093155880-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-executar"]`);
      cy.clickIfExist(`[data-cy="2093155880-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2093155880-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-53->2093155880-abrir visualização->2093155880-expandir->2093155880-diminuir`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-53`,`2093155880-abrir visualização`,`2093155880-expandir`,`2093155880-diminuir`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-53"]`);
      cy.clickIfExist(`[data-cy="2093155880-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="2093155880-expandir"]`);
      cy.clickIfExist(`[data-cy="2093155880-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element relatorios->relatorios/livro-iss-56->2093155883-executar->2093155883-múltipla seleção->2093155883-cancelar`, () => {
const actualId = [`root`,`relatorios`,`relatorios/livro-iss-56`,`2093155883-executar`,`2093155883-múltipla seleção`,`2093155883-cancelar`];
    cy.clickIfExist(`[data-cy="relatorios"]`);
      cy.clickIfExist(`[data-cy="relatorios/livro-iss-56"]`);
      cy.clickIfExist(`[data-cy="2093155883-executar"]`);
      cy.clickIfExist(`[data-cy="2093155883-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="2093155883-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe->1788893911-executar->1788893911-múltipla seleção->1788893911-cancelar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe`,`1788893911-executar`,`1788893911-múltipla seleção`,`1788893911-cancelar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criacao-integracao-nfse-dfe"]`);
      cy.clickIfExist(`[data-cy="1788893911-executar"]`);
      cy.clickIfExist(`[data-cy="1788893911-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1788893911-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element nfse->nfse/integracao-dfe-nfse->nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse->3637687372-executar->3637687372-múltipla seleção->3637687372-cancelar`, () => {
const actualId = [`root`,`nfse`,`nfse/integracao-dfe-nfse`,`nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse`,`3637687372-executar`,`3637687372-múltipla seleção`,`3637687372-cancelar`];
    cy.clickIfExist(`[data-cy="nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse"]`);
      cy.clickIfExist(`[data-cy="nfse/integracao-dfe-nfse/criticas-atualizacoes-ebs-nfse"]`);
      cy.clickIfExist(`[data-cy="3637687372-executar"]`);
      cy.clickIfExist(`[data-cy="3637687372-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="3637687372-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->2771762121-agendamentos->3960763378-abrir visualização->3960763378-expandir->3960763378-diminuir`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/solicitacoes-resultados`,`2771762121-agendamentos`,`3960763378-abrir visualização`,`3960763378-expandir`,`3960763378-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
      cy.clickIfExist(`[data-cy="2771762121-agendamentos"]`);
      cy.clickIfExist(`[data-cy="3960763378-abrir visualização"]`);
      cy.clickIfExist(`[data-cy="3960763378-expandir"]`);
      cy.clickIfExist(`[data-cy="3960763378-diminuir"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/geracao-de-nfts->1227584939-executar->1227584939-múltipla seleção->1227584939-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/geracao-de-nfts`,`1227584939-executar`,`1227584939-múltipla seleção`,`1227584939-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/geracao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="1227584939-executar"]`);
      cy.clickIfExist(`[data-cy="1227584939-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="1227584939-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element obrigacoes->obrigacoes/processos->obrigacoes/processos/importacao-de-nfts->774170564-executar->774170564-múltipla seleção->774170564-cancelar`, () => {
const actualId = [`root`,`obrigacoes`,`obrigacoes/processos`,`obrigacoes/processos/importacao-de-nfts`,`774170564-executar`,`774170564-múltipla seleção`,`774170564-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos"]`);
      cy.clickIfExist(`[data-cy="obrigacoes/processos/importacao-de-nfts"]`);
      cy.clickIfExist(`[data-cy="774170564-executar"]`);
      cy.clickIfExist(`[data-cy="774170564-múltipla seleção"]`);
      cy.clickIfExist(`[data-cy="774170564-cancelar"]`);
      cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
