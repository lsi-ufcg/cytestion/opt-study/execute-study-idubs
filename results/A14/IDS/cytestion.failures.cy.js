describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Filling values nfse->nfse/integracao-nfse-prefeitura->nfse/integracao-nfse-prefeitura/configuracao-automacao->4098540531-powerselect-ibgeCodigo-4098540531-radio-indAtivo and submit`, () => {
    cy.clickCauseExist(`[data-cy="nfse"]`);
    cy.clickCauseExist(`[data-cy="nfse/integracao-nfse-prefeitura"]`);
    cy.clickCauseExist(`[data-cy="nfse/integracao-nfse-prefeitura/configuracao-automacao"]`);
    cy.fillInputPowerSelect(`[data-cy="4098540531-powerselect-ibgeCodigo"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="4098540531-radio-indAtivo"] input`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
