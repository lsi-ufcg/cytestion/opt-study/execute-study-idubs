describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    const currentTestFunction = Cypress.mocha.getRunner().test.fn.toString();
    if (currentTestFunction.includes('.visit(')) {
      return;
    }
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element 1564411349-verificar dados`, () => {
    cy.clickCauseExist(`[data-cy="1564411349-verificar dados"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element movimentacao-contabil->movimentacao-contabil/conta-contabil-referencial-estab->2872373034-novo`, () => {
    cy.visit('http://system-A13/cadastros/conta-contabil-referencial-estab');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="2872373034-novo"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-referencial-contabil->247807478-novo`, () => {
    cy.visit('http://system-A13/movimentacao-contabil/saldo-referencial-contabil');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="247807478-novo"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values bloco-x->bloco-x/operacao-exterior-x291->1691052351-novo->2100922122-powerselect-idOperacaoExterior-2100922122-input-monetary-vlOperacaoExterior and submit`, () => {
    cy.visit('http://system-A13/bloco-x/operacao-exterior-x291');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="1691052351-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2100922122-powerselect-idOperacaoExterior"] input`);
    cy.fillInput(`[data-cy="2100922122-input-monetary-vlOperacaoExterior"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/saldo-referencial->3414944032-agendamentos->3414944032-voltar`, () => {
    cy.visit('http://system-A13/processos/saldo-referencial?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~52188714D%7C%7C52188714&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3414944032-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/saldo-acumulado->3193724833-agendamentos->3193724833-voltar`, () => {
    cy.visit('http://system-A13/processos/saldo-acumulado?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~50974346D%7C%7C50974346&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3193724833-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/gera-saldo-mensal->409460278-agendamentos->409460278-voltar`, () => {
    cy.visit('http://system-A13/processos/gera-saldo-mensal?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~54967120D%7C%7C54967120&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="409460278-voltar"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-agendamentos->3143174073-voltar`, () => {
    cy.visit('http://system-A13/processos/gera-lancamentos-encerramento?usuCodigo=~eq~SYNCHRO%7C%7CSYNCHRO&prcdefId=~eq~55470851D%7C%7C55470851&indSituacao=~eq~1%7C%7CAguardando%20execu%C3%A7%C3%A3o');
    cy.waitNetworkFinished();
    cy.clickCauseExist(`[data-cy="3143174073-voltar"]`);
    cy.checkErrorsWereDetected();
  });
});
