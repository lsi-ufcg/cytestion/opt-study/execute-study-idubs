describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  //--CODE--
  it.skip(`Visits index page`, () => {
    const actualId = [`root`];
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element home`, () => {
    const actualId = [`root`, `home`];
    cy.clickIfExist(`[data-cy="home"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros`, () => {
    const actualId = [`root`, `cadastros`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao`, () => {
    const actualId = [`root`, `apuracao`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil`, () => {
    const actualId = [`root`, `movimentacao-contabil`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB`, () => {
    const actualId = [`root`, `parteB`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l`, () => {
    const actualId = [`root`, `bloco-l`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w`, () => {
    const actualId = [`root`, `bloco-v-w`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x`, () => {
    const actualId = [`root`, `bloco-x`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y`, () => {
    const actualId = [`root`, `bloco-y`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes`, () => {
    const actualId = [`root`, `obrigacoes`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos`, () => {
    const actualId = [`root`, `processos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos-customizados`, () => {
    const actualId = [`root`, `processos-customizados`];
    cy.clickIfExist(`[data-cy="processos-customizados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads`, () => {
    const actualId = [`root`, `downloads`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element collapse-menu`, () => {
    const actualId = [`root`, `collapse-menu`];
    cy.clickIfExist(`[data-cy="collapse-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element modules-menu`, () => {
    const actualId = [`root`, `modules-menu`];
    cy.clickIfExist(`[data-cy="modules-menu"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 1564411349-verificar dados`, () => {
    const actualId = [`root`, `1564411349-verificar dados`];
    cy.clickIfExist(`[data-cy="1564411349-verificar dados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element 1564411349-apurar`, () => {
    const actualId = [`root`, `1564411349-apurar`];
    cy.clickIfExist(`[data-cy="1564411349-apurar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values 1564411349-powerselect-codEstabelecimento-1564411349-radio-indApuracao and submit`, () => {
    const actualId = [`root`, `1564411349-powerselect-codEstabelecimento-1564411349-radio-indApuracao`];
    cy.fillInputPowerSelect(`[data-cy="1564411349-powerselect-codEstabelecimento"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1564411349-radio-indApuracao"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/pfj`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/pfj`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/pfj"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/plano-conta`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/plano-conta`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/plano-conta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/plano-conta-uso`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/plano-conta-uso`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/plano-conta-uso"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/conta-contabil`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/conta-contabil`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/conta-contabil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/plano-conta-referencial`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/plano-conta-referencial`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/plano-conta-referencial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/centro-custo`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/centro-custo`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/centro-custo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoPorBalancete`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoPorBalancete`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoPorBalancete"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoReceitaBruta`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoReceitaBruta`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoReceitaBruta"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/lancamento-contabil`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/conta-contabil-referencial-estab`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/conta-contabil-referencial-estab`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/conta-contabil-referencial-estab"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-antes-encerramento`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-antes-encerramento`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-antes-encerramento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-referencial-contabil`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-referencial-contabil`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-referencial-contabil"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/subMenuPlanoReferencial`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/subMenuPlanoReferencial`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/subMenuPlanoReferencial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-contabil-associacao-referencial`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-contabil-associacao-referencial`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-contabil-associacao-referencial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/contas-controle`, () => {
    const actualId = [`root`, `parteB`, `parteB/contas-controle`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/contas-controle"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/lancamentos`, () => {
    const actualId = [`root`, `parteB`, `parteB/lancamentos`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/lancamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/saldo-parte-b`, () => {
    const actualId = [`root`, `parteB`, `parteB/saldo-parte-b`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/saldo-parte-b"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/exportacoes-x300`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/exportacoes-x300`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/exportacoes-x300"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/importacoes-x320`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/importacoes-x320`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/importacoes-x320"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/pagam-receb-ext-Y520`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/pagam-receb-ext-Y520`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/pagam-receb-ext-Y520"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/retidos-na-fonte`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/retidos-na-fonte`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/retidos-na-fonte"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/rendimento-socio-titular`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/rendimento-socio-titular`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/rendimento-socio-titular"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/equivalencia-patrimonial-y620`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/equivalencia-patrimonial-y620`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/equivalencia-patrimonial-y620"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/fundos-clubes-investimento-y630`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/fundos-clubes-investimento-y630`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/fundos-clubes-investimento-y630"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participacoes-consorcios-empresas-Y640`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participacoes-consorcios-empresas-Y640`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participacoes-consorcios-empresas-Y640"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participantes-consorcio`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participantes-consorcio`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participantes-consorcio"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/periodos-anteriores`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/periodos-anteriores`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/periodos-anteriores"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->2617216305-power-search-button`, () => {
    const actualId = [`root`, `downloads`, `2617216305-power-search-button`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.clickIfExist(`[data-cy="2617216305-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->2617216305-download`, () => {
    const actualId = [`root`, `downloads`, `2617216305-download`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.clickIfExist(`[data-cy="2617216305-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->2617216305-detalhes`, () => {
    const actualId = [`root`, `downloads`, `2617216305-detalhes`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.clickIfExist(`[data-cy="2617216305-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element downloads->2617216305-excluir`, () => {
    const actualId = [`root`, `downloads`, `2617216305-excluir`];
    cy.clickIfExist(`[data-cy="downloads"]`);
    cy.clickIfExist(`[data-cy="2617216305-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-novo`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-novo`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-power-search-button`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-power-search-button`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-visualizar/editar`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-visualizar/editar`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-excluir`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-excluir`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/plano-conta-referencial->2552767814-power-search-button`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/plano-conta-referencial`, `2552767814-power-search-button`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/plano-conta-referencial"]`);
    cy.clickIfExist(`[data-cy="2552767814-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/plano-conta-referencial->2552767814-detalhes da conta referencial`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/plano-conta-referencial`, `2552767814-detalhes da conta referencial`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/plano-conta-referencial"]`);
    cy.clickIfExist(`[data-cy="2552767814-detalhes da conta referencial"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/plano-conta-referencial->2552767814-carregar mais`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/plano-conta-referencial`, `2552767814-carregar mais`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/plano-conta-referencial"]`);
    cy.clickIfExist(`[data-cy="2552767814-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoPorBalancete->1370036182-aberto`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoPorBalancete`, `1370036182-aberto`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoPorBalancete"]`);
    cy.clickIfExist(`[data-cy="1370036182-aberto"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoPorBalancete->1370036182-saldos parte b`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoPorBalancete`, `1370036182-saldos parte b`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoPorBalancete"]`);
    cy.clickIfExist(`[data-cy="1370036182-saldos parte b"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoReceitaBruta->2993748847-atualizar`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoReceitaBruta`, `2993748847-atualizar`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoReceitaBruta"]`);
    cy.clickIfExist(`[data-cy="2993748847-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoReceitaBruta->2993748847-encerrado`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoReceitaBruta`, `2993748847-encerrado`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoReceitaBruta"]`);
    cy.clickIfExist(`[data-cy="2993748847-encerrado"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-novo`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-mais operações`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-mais operações`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-mais operações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->2472760992-novo`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/lancamento-contabil`, `2472760992-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
    cy.clickIfExist(`[data-cy="2472760992-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->2472760992-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/lancamento-contabil`, `2472760992-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
    cy.clickIfExist(`[data-cy="2472760992-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/conta-contabil-referencial-estab->2872373034-novo`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/conta-contabil-referencial-estab`, `2872373034-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/conta-contabil-referencial-estab"]`);
    cy.clickIfExist(`[data-cy="2872373034-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/conta-contabil-referencial-estab->2872373034-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/conta-contabil-referencial-estab`, `2872373034-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/conta-contabil-referencial-estab"]`);
    cy.clickIfExist(`[data-cy="2872373034-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-antes-encerramento->4280385632-novo`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-antes-encerramento`, `4280385632-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-antes-encerramento"]`);
    cy.clickIfExist(`[data-cy="4280385632-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-antes-encerramento->4280385632-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-antes-encerramento`, `4280385632-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-antes-encerramento"]`);
    cy.clickIfExist(`[data-cy="4280385632-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-referencial-contabil->247807478-novo`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-referencial-contabil`, `247807478-novo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-referencial-contabil"]`);
    cy.clickIfExist(`[data-cy="247807478-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-referencial-contabil->247807478-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-referencial-contabil`, `247807478-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-referencial-contabil"]`);
    cy.clickIfExist(`[data-cy="247807478-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/subMenuPlanoReferencial->661569372-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/subMenuPlanoReferencial`, `661569372-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/subMenuPlanoReferencial"]`);
    cy.clickIfExist(`[data-cy="661569372-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-contabil-associacao-referencial->3192672379-power-search-button`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-contabil-associacao-referencial`, `3192672379-power-search-button`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-contabil-associacao-referencial"]`);
    cy.clickIfExist(`[data-cy="3192672379-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-contabil-associacao-referencial->3192672379-carregar mais`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-contabil-associacao-referencial`, `3192672379-carregar mais`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-contabil-associacao-referencial"]`);
    cy.clickIfExist(`[data-cy="3192672379-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/contas-controle->4171402380-novo`, () => {
    const actualId = [`root`, `parteB`, `parteB/contas-controle`, `4171402380-novo`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/contas-controle"]`);
    cy.clickIfExist(`[data-cy="4171402380-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/contas-controle->4171402380-power-search-button`, () => {
    const actualId = [`root`, `parteB`, `parteB/contas-controle`, `4171402380-power-search-button`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/contas-controle"]`);
    cy.clickIfExist(`[data-cy="4171402380-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/lancamentos->3032598740-power-search-button`, () => {
    const actualId = [`root`, `parteB`, `parteB/lancamentos`, `3032598740-power-search-button`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/lancamentos"]`);
    cy.clickIfExist(`[data-cy="3032598740-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/saldo-parte-b->521668412-power-search-button`, () => {
    const actualId = [`root`, `parteB`, `parteB/saldo-parte-b`, `521668412-power-search-button`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/saldo-parte-b"]`);
    cy.clickIfExist(`[data-cy="521668412-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-novo`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-novo`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-power-search-button`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-excluir`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-excluir`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-carregar mais`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-novo`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-power-search-button`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-excluir`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-excluir`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-carregar mais`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-novo`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-novo`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-power-search-button`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-excluir`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-excluir`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-carregar mais`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-novo`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-novo`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-power-search-button`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-excluir`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-excluir`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-carregar mais`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/exportacoes-x300->469132206-novo`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/exportacoes-x300`, `469132206-novo`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/exportacoes-x300"]`);
    cy.clickIfExist(`[data-cy="469132206-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/exportacoes-x300->469132206-power-search-button`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/exportacoes-x300`, `469132206-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/exportacoes-x300"]`);
    cy.clickIfExist(`[data-cy="469132206-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/importacoes-x320->2176193540-novo`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/importacoes-x320`, `2176193540-novo`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/importacoes-x320"]`);
    cy.clickIfExist(`[data-cy="2176193540-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/importacoes-x320->2176193540-power-search-button`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/importacoes-x320`, `2176193540-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/importacoes-x320"]`);
    cy.clickIfExist(`[data-cy="2176193540-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-power-search-button`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-moreoutlined`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-moreoutlined`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-moreoutlined"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-excluir`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-excluir`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-carregar mais`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-novo`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-novo`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-power-search-button`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-excluir`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-excluir`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-carregar mais`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-novo`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-novo`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-power-search-button`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-excluir`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-excluir`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-carregar mais`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/pagam-receb-ext-Y520->1335457661-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/pagam-receb-ext-Y520`, `1335457661-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/pagam-receb-ext-Y520"]`);
    cy.clickIfExist(`[data-cy="1335457661-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/pagam-receb-ext-Y520->1335457661-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/pagam-receb-ext-Y520`, `1335457661-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/pagam-receb-ext-Y520"]`);
    cy.clickIfExist(`[data-cy="1335457661-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/retidos-na-fonte->355170916-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/retidos-na-fonte`, `355170916-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/retidos-na-fonte"]`);
    cy.clickIfExist(`[data-cy="355170916-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/retidos-na-fonte->355170916-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/retidos-na-fonte`, `355170916-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/retidos-na-fonte"]`);
    cy.clickIfExist(`[data-cy="355170916-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-excluir`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-excluir`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-carregar mais`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/rendimento-socio-titular->1231882239-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/rendimento-socio-titular`, `1231882239-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/rendimento-socio-titular"]`);
    cy.clickIfExist(`[data-cy="1231882239-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/rendimento-socio-titular->1231882239-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/rendimento-socio-titular`, `1231882239-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/rendimento-socio-titular"]`);
    cy.clickIfExist(`[data-cy="1231882239-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/equivalencia-patrimonial-y620->425823894-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/equivalencia-patrimonial-y620`, `425823894-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/equivalencia-patrimonial-y620"]`);
    cy.clickIfExist(`[data-cy="425823894-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/equivalencia-patrimonial-y620->425823894-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/equivalencia-patrimonial-y620`, `425823894-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/equivalencia-patrimonial-y620"]`);
    cy.clickIfExist(`[data-cy="425823894-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/fundos-clubes-investimento-y630->1064065977-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/fundos-clubes-investimento-y630`, `1064065977-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/fundos-clubes-investimento-y630"]`);
    cy.clickIfExist(`[data-cy="1064065977-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/fundos-clubes-investimento-y630->1064065977-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/fundos-clubes-investimento-y630`, `1064065977-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/fundos-clubes-investimento-y630"]`);
    cy.clickIfExist(`[data-cy="1064065977-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participacoes-consorcios-empresas-Y640->3014860639-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participacoes-consorcios-empresas-Y640`, `3014860639-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participacoes-consorcios-empresas-Y640"]`);
    cy.clickIfExist(`[data-cy="3014860639-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participacoes-consorcios-empresas-Y640->3014860639-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participacoes-consorcios-empresas-Y640`, `3014860639-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participacoes-consorcios-empresas-Y640"]`);
    cy.clickIfExist(`[data-cy="3014860639-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participantes-consorcio->3806821821-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participantes-consorcio`, `3806821821-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participantes-consorcio"]`);
    cy.clickIfExist(`[data-cy="3806821821-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participantes-consorcio->3806821821-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participantes-consorcio`, `3806821821-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participantes-consorcio"]`);
    cy.clickIfExist(`[data-cy="3806821821-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participantes-consorcio->3806821821-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participantes-consorcio`, `3806821821-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participantes-consorcio"]`);
    cy.clickIfExist(`[data-cy="3806821821-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participantes-consorcio->3806821821-excluir`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participantes-consorcio`, `3806821821-excluir`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participantes-consorcio"]`);
    cy.clickIfExist(`[data-cy="3806821821-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participantes-consorcio->3806821821-carregar mais`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participantes-consorcio`, `3806821821-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participantes-consorcio"]`);
    cy.clickIfExist(`[data-cy="3806821821-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/periodos-anteriores->4126259043-novo`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/periodos-anteriores`, `4126259043-novo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/periodos-anteriores"]`);
    cy.clickIfExist(`[data-cy="4126259043-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/periodos-anteriores->4126259043-power-search-button`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/periodos-anteriores`, `4126259043-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/periodos-anteriores"]`);
    cy.clickIfExist(`[data-cy="4126259043-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-gerenciar labels`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-gerenciar labels`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-gerenciar labels"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-visualizar parâmetros`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-visualizar parâmetros`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-visualizar parâmetros"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-visualizar/editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-visualizar/editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ajuda`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ajuda`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ajuda"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-abrir visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-abrir visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-visualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-visualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-visualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-novo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-editar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-editar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2480925693-novo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `2480925693-novo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
    cy.clickIfExist(`[data-cy="2480925693-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2480925693-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `2480925693-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
    cy.clickIfExist(`[data-cy="2480925693-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2480925693-excluir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `2480925693-excluir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
    cy.clickIfExist(`[data-cy="2480925693-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-executar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-executar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-executar`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-regerar`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-regerar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-regerar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-detalhes`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-detalhes`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-detalhes"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-abrir visualização`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-abrir visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-abrir visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-excluir`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-excluir`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-executar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-executar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-executar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-executar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-agendamentos`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-agendamentos`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-power-search-button`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-power-search-button`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-visualização`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-visualização`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-novo->2056303010-salvar`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-novo`, `2056303010-salvar`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-novo"]`);
    cy.clickIfExist(`[data-cy="2056303010-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-novo->2056303010-próximo`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-novo`, `2056303010-próximo`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-novo"]`);
    cy.clickIfExist(`[data-cy="2056303010-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-novo->2056303010-cancelar`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-novo`, `2056303010-cancelar`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-novo"]`);
    cy.clickIfExist(`[data-cy="2056303010-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values cadastros->cadastros/parametros-ecf-vigencia->1083834279-novo->2056303010-powerselect-estCodigo-2056303010-input-number-ano-2056303010-powerselect-situacaoInicioPeriodo-2056303010-powerselect-situacaoEspecial-2056303010-powerselect-codigoNaturezaJuridica-2056303010-input-number-indQuantidadeScp-2056303010-powerselect-tipoEscrituracaoGeral-2056303010-input-numReciboEcfAnterior-2056303010-powerselect-tipoEcfScp-2056303010-checkbox-indOptanteRefis-2056303010-checkbox-indOptantePaes-2056303010-powerselect-formaTributacaoLucro-2056303010-powerselect-qualificacaoPfj-2056303010-radio-indFormaApuracaoEcf-2056303010-powerselect-indReconhecimentoReceita-2056303010-powerselect-formaTributacaoPeriodo-2056303010-powerselect-formaTributacaoPeriodo2tri-2056303010-powerselect-formaTributacaoPeriodo3tri-2056303010-powerselect-formaTributacaoPeriodo4tri-2056303010-input-monetary-aliquotaIrpj-2056303010-input-monetary-aliquotaAdicionalIrpj-2056303010-powerselect-indAliquotaCsll-2056303010-powerselect-indEncerramentoSocietario-2056303010-checkbox-indAdmFunClubInvest-2056303010-checkbox-indPjEnquadadrada-2056303010-checkbox-indAtividadeRural-2056303010-checkbox-indLucroExploracao-2056303010-checkbox-indIsencaoReducaoPresumido-2056303010-checkbox-indFinorFinamFunres-2056303010-checkbox-indPaisPais-2056303010-checkbox-indDerex-2056303010-checkbox-indPessoaVinculada-2056303010-checkbox-indOperacoesExterior-2056303010-checkbox-indParticipacoesExterior-2056303010-checkbox-indComerciorEletronicoTi-2056303010-checkbox-indRoyaltiesPagos-2056303010-checkbox-indRoyaltiesRecebidos-2056303010-checkbox-indRendimentosRelServ-2056303010-checkbox-indPagamentosRemessa-2056303010-checkbox-indInovacaoTecnologica-2056303010-checkbox-indCapacitacaoInformatica-2056303010-checkbox-indPjHabilitada-2056303010-checkbox-indPoloAmazonia-2056303010-checkbox-indZonaExportacao-2056303010-checkbox-indAreaLivreComercio-2056303010-checkbox-indPgtoExterior-2056303010-checkbox-indRendimentosExterior-2056303010-checkbox-indVendaExportParaExp-2056303010-checkbox-indComercialExportadora-2056303010-checkbox-indDoacaoCampEleitoral-2056303010-checkbox-indAtivosExterior-2056303010-checkbox-indParticipacaoColCon-2056303010-checkbox-indParticipacaoConsorcios-2056303010-checkbox-indOutrasInformacoes-2056303010-checkbox-indRepes-2056303010-checkbox-indRecap-2056303010-checkbox-indPadis-2056303010-checkbox-indReidi-2056303010-checkbox-indRecine-2056303010-checkbox-indRetid-2056303010-checkbox-indOutros-2056303010-powerselect-contabilistaCodigo-2056303010-powerselect-codigoQualificaContabilista-2056303010-powerselect-pfjSignatarioCodigo-2056303010-powerselect-codigoQualificaResponsavel and submit`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-novo`, `2056303010-powerselect-estCodigo-2056303010-input-number-ano-2056303010-powerselect-situacaoInicioPeriodo-2056303010-powerselect-situacaoEspecial-2056303010-powerselect-codigoNaturezaJuridica-2056303010-input-number-indQuantidadeScp-2056303010-powerselect-tipoEscrituracaoGeral-2056303010-input-numReciboEcfAnterior-2056303010-powerselect-tipoEcfScp-2056303010-checkbox-indOptanteRefis-2056303010-checkbox-indOptantePaes-2056303010-powerselect-formaTributacaoLucro-2056303010-powerselect-qualificacaoPfj-2056303010-radio-indFormaApuracaoEcf-2056303010-powerselect-indReconhecimentoReceita-2056303010-powerselect-formaTributacaoPeriodo-2056303010-powerselect-formaTributacaoPeriodo2tri-2056303010-powerselect-formaTributacaoPeriodo3tri-2056303010-powerselect-formaTributacaoPeriodo4tri-2056303010-input-monetary-aliquotaIrpj-2056303010-input-monetary-aliquotaAdicionalIrpj-2056303010-powerselect-indAliquotaCsll-2056303010-powerselect-indEncerramentoSocietario-2056303010-checkbox-indAdmFunClubInvest-2056303010-checkbox-indPjEnquadadrada-2056303010-checkbox-indAtividadeRural-2056303010-checkbox-indLucroExploracao-2056303010-checkbox-indIsencaoReducaoPresumido-2056303010-checkbox-indFinorFinamFunres-2056303010-checkbox-indPaisPais-2056303010-checkbox-indDerex-2056303010-checkbox-indPessoaVinculada-2056303010-checkbox-indOperacoesExterior-2056303010-checkbox-indParticipacoesExterior-2056303010-checkbox-indComerciorEletronicoTi-2056303010-checkbox-indRoyaltiesPagos-2056303010-checkbox-indRoyaltiesRecebidos-2056303010-checkbox-indRendimentosRelServ-2056303010-checkbox-indPagamentosRemessa-2056303010-checkbox-indInovacaoTecnologica-2056303010-checkbox-indCapacitacaoInformatica-2056303010-checkbox-indPjHabilitada-2056303010-checkbox-indPoloAmazonia-2056303010-checkbox-indZonaExportacao-2056303010-checkbox-indAreaLivreComercio-2056303010-checkbox-indPgtoExterior-2056303010-checkbox-indRendimentosExterior-2056303010-checkbox-indVendaExportParaExp-2056303010-checkbox-indComercialExportadora-2056303010-checkbox-indDoacaoCampEleitoral-2056303010-checkbox-indAtivosExterior-2056303010-checkbox-indParticipacaoColCon-2056303010-checkbox-indParticipacaoConsorcios-2056303010-checkbox-indOutrasInformacoes-2056303010-checkbox-indRepes-2056303010-checkbox-indRecap-2056303010-checkbox-indPadis-2056303010-checkbox-indReidi-2056303010-checkbox-indRecine-2056303010-checkbox-indRetid-2056303010-checkbox-indOutros-2056303010-powerselect-contabilistaCodigo-2056303010-powerselect-codigoQualificaContabilista-2056303010-powerselect-pfjSignatarioCodigo-2056303010-powerselect-codigoQualificaResponsavel`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-estCodigo"] input`);
    cy.fillInput(`[data-cy="2056303010-input-number-ano"] textarea`, `6`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-situacaoInicioPeriodo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-situacaoEspecial"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-codigoNaturezaJuridica"] input`);
    cy.fillInput(`[data-cy="2056303010-input-number-indQuantidadeScp"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-tipoEscrituracaoGeral"] input`);
    cy.fillInput(`[data-cy="2056303010-input-numReciboEcfAnterior"] textarea`, `ebusiness`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-tipoEcfScp"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indOptanteRefis"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indOptantePaes"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-formaTributacaoLucro"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-qualificacaoPfj"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-radio-indFormaApuracaoEcf"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-indReconhecimentoReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-formaTributacaoPeriodo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-formaTributacaoPeriodo2tri"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-formaTributacaoPeriodo3tri"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-formaTributacaoPeriodo4tri"] input`);
    cy.fillInput(`[data-cy="2056303010-input-monetary-aliquotaIrpj"] textarea`, `9`);
    cy.fillInput(`[data-cy="2056303010-input-monetary-aliquotaAdicionalIrpj"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-indAliquotaCsll"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-indEncerramentoSocietario"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indAdmFunClubInvest"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPjEnquadadrada"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indAtividadeRural"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indLucroExploracao"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indIsencaoReducaoPresumido"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indFinorFinamFunres"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPaisPais"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indDerex"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPessoaVinculada"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indOperacoesExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indParticipacoesExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indComerciorEletronicoTi"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRoyaltiesPagos"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRoyaltiesRecebidos"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRendimentosRelServ"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPagamentosRemessa"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indInovacaoTecnologica"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indCapacitacaoInformatica"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPjHabilitada"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPoloAmazonia"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indZonaExportacao"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indAreaLivreComercio"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPgtoExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRendimentosExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indVendaExportParaExp"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indComercialExportadora"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indDoacaoCampEleitoral"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indAtivosExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indParticipacaoColCon"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indParticipacaoConsorcios"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indOutrasInformacoes"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRepes"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRecap"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indPadis"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indReidi"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRecine"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indRetid"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2056303010-checkbox-indOutros"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-contabilistaCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-codigoQualificaContabilista"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-pfjSignatarioCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2056303010-powerselect-codigoQualificaResponsavel"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-visualizar/editar->2845074479-remover item`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-visualizar/editar`, `2845074479-remover item`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2845074479-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-visualizar/editar->2845074479-salvar`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-visualizar/editar`, `2845074479-salvar`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2845074479-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-visualizar/editar->2845074479-próximo`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-visualizar/editar`, `2845074479-próximo`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2845074479-próximo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element cadastros->cadastros/parametros-ecf-vigencia->1083834279-visualizar/editar->2845074479-cancelar`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-visualizar/editar`, `2845074479-cancelar`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2845074479-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values cadastros->cadastros/parametros-ecf-vigencia->1083834279-visualizar/editar->2845074479-powerselect-estCodigo-2845074479-input-number-ano-2845074479-powerselect-situacaoInicioPeriodo-2845074479-powerselect-situacaoEspecial-2845074479-powerselect-codigoNaturezaJuridica-2845074479-input-number-indQuantidadeScp-2845074479-powerselect-tipoEscrituracaoGeral-2845074479-input-numReciboEcfAnterior-2845074479-powerselect-tipoEcfScp-2845074479-checkbox-indOptanteRefis-2845074479-checkbox-indOptantePaes-2845074479-powerselect-formaTributacaoLucro-2845074479-powerselect-qualificacaoPfj-2845074479-powerselect-indReconhecimentoReceita-2845074479-powerselect-formaTributacaoPeriodo-2845074479-powerselect-formaTributacaoPeriodo2tri-2845074479-powerselect-formaTributacaoPeriodo3tri-2845074479-powerselect-formaTributacaoPeriodo4tri-2845074479-input-monetary-aliquotaIrpj-2845074479-input-monetary-aliquotaAdicionalIrpj-2845074479-powerselect-indAliquotaCsll-2845074479-powerselect-indEncerramentoSocietario-2845074479-powerselect-mesEncerramentoSocietario-2845074479-checkbox-indAdmFunClubInvest-2845074479-checkbox-indPjEnquadadrada-2845074479-checkbox-indAtividadeRural-2845074479-checkbox-indLucroExploracao-2845074479-checkbox-indIsencaoReducaoPresumido-2845074479-checkbox-indFinorFinamFunres-2845074479-checkbox-indDerex-2845074479-checkbox-indPessoaVinculada-2845074479-checkbox-indOperacoesExterior-2845074479-checkbox-indParticipacoesExterior-2845074479-checkbox-indComerciorEletronicoTi-2845074479-checkbox-indRoyaltiesPagos-2845074479-checkbox-indRoyaltiesRecebidos-2845074479-checkbox-indRendimentosRelServ-2845074479-checkbox-indPagamentosRemessa-2845074479-checkbox-indInovacaoTecnologica-2845074479-checkbox-indCapacitacaoInformatica-2845074479-checkbox-indPjHabilitada-2845074479-checkbox-indPoloAmazonia-2845074479-checkbox-indZonaExportacao-2845074479-checkbox-indAreaLivreComercio-2845074479-checkbox-indPgtoExterior-2845074479-checkbox-indRendimentosExterior-2845074479-checkbox-indVendaExportParaExp-2845074479-checkbox-indComercialExportadora-2845074479-checkbox-indDoacaoCampEleitoral-2845074479-checkbox-indAtivosExterior-2845074479-checkbox-indParticipacaoColCon-2845074479-checkbox-indParticipacaoConsorcios-2845074479-checkbox-indOutrasInformacoes-2845074479-checkbox-indRepes-2845074479-checkbox-indRecap-2845074479-checkbox-indPadis-2845074479-checkbox-indReidi-2845074479-checkbox-indRecine-2845074479-checkbox-indRetid-2845074479-checkbox-indOutros-2845074479-powerselect-contabilistaCodigo-2845074479-powerselect-codigoQualificaContabilista-2845074479-powerselect-pfjSignatarioCodigo-2845074479-powerselect-codigoQualificaResponsavel and submit`, () => {
    const actualId = [`root`, `cadastros`, `cadastros/parametros-ecf-vigencia`, `1083834279-visualizar/editar`, `2845074479-powerselect-estCodigo-2845074479-input-number-ano-2845074479-powerselect-situacaoInicioPeriodo-2845074479-powerselect-situacaoEspecial-2845074479-powerselect-codigoNaturezaJuridica-2845074479-input-number-indQuantidadeScp-2845074479-powerselect-tipoEscrituracaoGeral-2845074479-input-numReciboEcfAnterior-2845074479-powerselect-tipoEcfScp-2845074479-checkbox-indOptanteRefis-2845074479-checkbox-indOptantePaes-2845074479-powerselect-formaTributacaoLucro-2845074479-powerselect-qualificacaoPfj-2845074479-powerselect-indReconhecimentoReceita-2845074479-powerselect-formaTributacaoPeriodo-2845074479-powerselect-formaTributacaoPeriodo2tri-2845074479-powerselect-formaTributacaoPeriodo3tri-2845074479-powerselect-formaTributacaoPeriodo4tri-2845074479-input-monetary-aliquotaIrpj-2845074479-input-monetary-aliquotaAdicionalIrpj-2845074479-powerselect-indAliquotaCsll-2845074479-powerselect-indEncerramentoSocietario-2845074479-powerselect-mesEncerramentoSocietario-2845074479-checkbox-indAdmFunClubInvest-2845074479-checkbox-indPjEnquadadrada-2845074479-checkbox-indAtividadeRural-2845074479-checkbox-indLucroExploracao-2845074479-checkbox-indIsencaoReducaoPresumido-2845074479-checkbox-indFinorFinamFunres-2845074479-checkbox-indDerex-2845074479-checkbox-indPessoaVinculada-2845074479-checkbox-indOperacoesExterior-2845074479-checkbox-indParticipacoesExterior-2845074479-checkbox-indComerciorEletronicoTi-2845074479-checkbox-indRoyaltiesPagos-2845074479-checkbox-indRoyaltiesRecebidos-2845074479-checkbox-indRendimentosRelServ-2845074479-checkbox-indPagamentosRemessa-2845074479-checkbox-indInovacaoTecnologica-2845074479-checkbox-indCapacitacaoInformatica-2845074479-checkbox-indPjHabilitada-2845074479-checkbox-indPoloAmazonia-2845074479-checkbox-indZonaExportacao-2845074479-checkbox-indAreaLivreComercio-2845074479-checkbox-indPgtoExterior-2845074479-checkbox-indRendimentosExterior-2845074479-checkbox-indVendaExportParaExp-2845074479-checkbox-indComercialExportadora-2845074479-checkbox-indDoacaoCampEleitoral-2845074479-checkbox-indAtivosExterior-2845074479-checkbox-indParticipacaoColCon-2845074479-checkbox-indParticipacaoConsorcios-2845074479-checkbox-indOutrasInformacoes-2845074479-checkbox-indRepes-2845074479-checkbox-indRecap-2845074479-checkbox-indPadis-2845074479-checkbox-indReidi-2845074479-checkbox-indRecine-2845074479-checkbox-indRetid-2845074479-checkbox-indOutros-2845074479-powerselect-contabilistaCodigo-2845074479-powerselect-codigoQualificaContabilista-2845074479-powerselect-pfjSignatarioCodigo-2845074479-powerselect-codigoQualificaResponsavel`];
    cy.clickIfExist(`[data-cy="cadastros"]`);
    cy.clickIfExist(`[data-cy="cadastros/parametros-ecf-vigencia"]`);
    cy.clickIfExist(`[data-cy="1083834279-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-estCodigo"] input`);
    cy.fillInput(`[data-cy="2845074479-input-number-ano"] textarea`, `3`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-situacaoInicioPeriodo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-situacaoEspecial"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-codigoNaturezaJuridica"] input`);
    cy.fillInput(`[data-cy="2845074479-input-number-indQuantidadeScp"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-tipoEscrituracaoGeral"] input`);
    cy.fillInput(`[data-cy="2845074479-input-numReciboEcfAnterior"] textarea`, `Concrete`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-tipoEcfScp"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indOptanteRefis"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indOptantePaes"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-formaTributacaoLucro"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-qualificacaoPfj"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-indReconhecimentoReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-formaTributacaoPeriodo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-formaTributacaoPeriodo2tri"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-formaTributacaoPeriodo3tri"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-formaTributacaoPeriodo4tri"] input`);
    cy.fillInput(`[data-cy="2845074479-input-monetary-aliquotaIrpj"] textarea`, `9`);
    cy.fillInput(`[data-cy="2845074479-input-monetary-aliquotaAdicionalIrpj"] textarea`, `1`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-indAliquotaCsll"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-indEncerramentoSocietario"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-mesEncerramentoSocietario"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indAdmFunClubInvest"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPjEnquadadrada"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indAtividadeRural"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indLucroExploracao"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indIsencaoReducaoPresumido"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indFinorFinamFunres"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indDerex"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPessoaVinculada"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indOperacoesExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indParticipacoesExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indComerciorEletronicoTi"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRoyaltiesPagos"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRoyaltiesRecebidos"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRendimentosRelServ"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPagamentosRemessa"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indInovacaoTecnologica"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indCapacitacaoInformatica"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPjHabilitada"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPoloAmazonia"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indZonaExportacao"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indAreaLivreComercio"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPgtoExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRendimentosExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indVendaExportParaExp"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indComercialExportadora"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indDoacaoCampEleitoral"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indAtivosExterior"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indParticipacaoColCon"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indParticipacaoConsorcios"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indOutrasInformacoes"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRepes"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRecap"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indPadis"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indReidi"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRecine"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indRetid"] textarea`);
    cy.fillInputCheckboxOrRadio(`[data-cy="2845074479-checkbox-indOutros"] textarea`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-contabilistaCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-codigoQualificaContabilista"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-pfjSignatarioCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2845074479-powerselect-codigoQualificaResponsavel"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element apuracao->apuracaoPorBalancete->1370036182-saldos parte b->521668412-power-search-button`, () => {
    const actualId = [`root`, `apuracao`, `apuracaoPorBalancete`, `1370036182-saldos parte b`, `521668412-power-search-button`];
    cy.clickIfExist(`[data-cy="apuracao"]`);
    cy.clickIfExist(`[data-cy="apuracaoPorBalancete"]`);
    cy.clickIfExist(`[data-cy="1370036182-saldos parte b"]`);
    cy.clickIfExist(`[data-cy="521668412-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-novo->3159554804-salvar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-novo`, `3159554804-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-novo"]`);
    cy.clickIfExist(`[data-cy="3159554804-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-novo->3159554804-voltar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-novo`, `3159554804-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-novo"]`);
    cy.clickIfExist(`[data-cy="3159554804-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-novo->3159554804-powerselect-icconIdAnalitica-3159554804-input-monetary-vlInicial-3159554804-powerselect-indDbCr-3159554804-input-monetary-vlDebito-3159554804-input-monetary-vlCredito-3159554804-input-monetary-vlSaldoFinalAcumulado-3159554804-powerselect-indDbCrSldFinal-3159554804-powerselect-icodIdCcusto and submit`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-novo`, `3159554804-powerselect-icconIdAnalitica-3159554804-input-monetary-vlInicial-3159554804-powerselect-indDbCr-3159554804-input-monetary-vlDebito-3159554804-input-monetary-vlCredito-3159554804-input-monetary-vlSaldoFinalAcumulado-3159554804-powerselect-indDbCrSldFinal-3159554804-powerselect-icodIdCcusto`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3159554804-powerselect-icconIdAnalitica"] input`);
    cy.fillInput(`[data-cy="3159554804-input-monetary-vlInicial"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="3159554804-powerselect-indDbCr"] input`);
    cy.fillInput(`[data-cy="3159554804-input-monetary-vlDebito"] textarea`, `10`);
    cy.fillInput(`[data-cy="3159554804-input-monetary-vlCredito"] textarea`, `10`);
    cy.fillInput(`[data-cy="3159554804-input-monetary-vlSaldoFinalAcumulado"] textarea`, `10`);
    cy.fillInputPowerSelect(`[data-cy="3159554804-powerselect-indDbCrSldFinal"] input`);
    cy.fillInputPowerSelect(`[data-cy="3159554804-powerselect-icodIdCcusto"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-mais operações->3663702805-item-exportar balancete`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-mais operações`, `3663702805-item-exportar balancete`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-mais operações"]`);
    cy.clickIfExist(`[data-cy="3663702805-item-exportar balancete"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-mais operações->3663702805-item-importar balancete`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-mais operações`, `3663702805-item-importar balancete`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-mais operações"]`);
    cy.clickIfExist(`[data-cy="3663702805-item-importar balancete"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-mais operações->3663702805-item-erros de importação`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-mais operações`, `3663702805-item-erros de importação`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-mais operações"]`);
    cy.clickIfExist(`[data-cy="3663702805-item-erros de importação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->2472760992-novo->3876834889-salvar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/lancamento-contabil`, `2472760992-novo`, `3876834889-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
    cy.clickIfExist(`[data-cy="2472760992-novo"]`);
    cy.clickIfExist(`[data-cy="3876834889-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/lancamento-contabil->2472760992-novo->3876834889-voltar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/lancamento-contabil`, `2472760992-novo`, `3876834889-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
    cy.clickIfExist(`[data-cy="2472760992-novo"]`);
    cy.clickIfExist(`[data-cy="3876834889-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/lancamento-contabil->2472760992-novo->3876834889-input-numLancamento-3876834889-input-numArquivamento-3876834889-powerselect-icconIdAnalitica-3876834889-powerselect-icodIdCusto-3876834889-input-monetary-vlLancamento-3876834889-powerselect-indDbCr-3876834889-input-histComplementar-3876834889-powerselect-icodIdHistorico-3876834889-powerselect-tipoLcto and submit`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/lancamento-contabil`, `2472760992-novo`, `3876834889-input-numLancamento-3876834889-input-numArquivamento-3876834889-powerselect-icconIdAnalitica-3876834889-powerselect-icodIdCusto-3876834889-input-monetary-vlLancamento-3876834889-powerselect-indDbCr-3876834889-input-histComplementar-3876834889-powerselect-icodIdHistorico-3876834889-powerselect-tipoLcto`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/lancamento-contabil"]`);
    cy.clickIfExist(`[data-cy="2472760992-novo"]`);
    cy.fillInput(`[data-cy="3876834889-input-numLancamento"] textarea`, `missioncritical`);
    cy.fillInput(`[data-cy="3876834889-input-numArquivamento"] textarea`, `Rio Grande do Sul`);
    cy.fillInputPowerSelect(`[data-cy="3876834889-powerselect-icconIdAnalitica"] input`);
    cy.fillInputPowerSelect(`[data-cy="3876834889-powerselect-icodIdCusto"] input`);
    cy.fillInput(`[data-cy="3876834889-input-monetary-vlLancamento"] textarea`, `1`);
    cy.fillInputPowerSelect(`[data-cy="3876834889-powerselect-indDbCr"] input`);
    cy.fillInput(`[data-cy="3876834889-input-histComplementar"] textarea`, `Plastic`);
    cy.fillInputPowerSelect(`[data-cy="3876834889-powerselect-icodIdHistorico"] input`);
    cy.fillInputPowerSelect(`[data-cy="3876834889-powerselect-tipoLcto"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-antes-encerramento->4280385632-novo->1617542281-salvar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-antes-encerramento`, `4280385632-novo`, `1617542281-salvar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-antes-encerramento"]`);
    cy.clickIfExist(`[data-cy="4280385632-novo"]`);
    cy.clickIfExist(`[data-cy="1617542281-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-antes-encerramento->4280385632-novo->1617542281-voltar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-antes-encerramento`, `4280385632-novo`, `1617542281-voltar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-antes-encerramento"]`);
    cy.clickIfExist(`[data-cy="4280385632-novo"]`);
    cy.clickIfExist(`[data-cy="1617542281-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values movimentacao-contabil->movimentacao-contabil/saldo-antes-encerramento->4280385632-novo->1617542281-powerselect-icconIdAnalitica-1617542281-powerselect-icodIdCcusto-1617542281-input-monetary-valorSaldo-1617542281-powerselect-indDcValorSaldo and submit`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-antes-encerramento`, `4280385632-novo`, `1617542281-powerselect-icconIdAnalitica-1617542281-powerselect-icodIdCcusto-1617542281-input-monetary-valorSaldo-1617542281-powerselect-indDcValorSaldo`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-antes-encerramento"]`);
    cy.clickIfExist(`[data-cy="4280385632-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1617542281-powerselect-icconIdAnalitica"] input`);
    cy.fillInputPowerSelect(`[data-cy="1617542281-powerselect-icodIdCcusto"] input`);
    cy.fillInput(`[data-cy="1617542281-input-monetary-valorSaldo"] textarea`, `2`);
    cy.fillInputPowerSelect(`[data-cy="1617542281-powerselect-indDcValorSaldo"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/contas-controle->4171402380-novo->4106578909-salvar`, () => {
    const actualId = [`root`, `parteB`, `parteB/contas-controle`, `4171402380-novo`, `4106578909-salvar`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/contas-controle"]`);
    cy.clickIfExist(`[data-cy="4171402380-novo"]`);
    cy.clickIfExist(`[data-cy="4106578909-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element parteB->parteB/contas-controle->4171402380-novo->4106578909-voltar`, () => {
    const actualId = [`root`, `parteB`, `parteB/contas-controle`, `4171402380-novo`, `4106578909-voltar`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/contas-controle"]`);
    cy.clickIfExist(`[data-cy="4171402380-novo"]`);
    cy.clickIfExist(`[data-cy="4106578909-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values parteB->parteB/contas-controle->4171402380-novo->4106578909-input-codigo-4106578909-input-descricao-4106578909-powerselect-codOficial-4106578909-input-monetary-lalurSaldoInicial-4106578909-powerselect-indDcLalurSaldoInicial-4106578909-input-monetary-lacsSaldoInicial-4106578909-powerselect-indDcLacsSaldoInicial and submit`, () => {
    const actualId = [`root`, `parteB`, `parteB/contas-controle`, `4171402380-novo`, `4106578909-input-codigo-4106578909-input-descricao-4106578909-powerselect-codOficial-4106578909-input-monetary-lalurSaldoInicial-4106578909-powerselect-indDcLalurSaldoInicial-4106578909-input-monetary-lacsSaldoInicial-4106578909-powerselect-indDcLacsSaldoInicial`];
    cy.clickIfExist(`[data-cy="parteB"]`);
    cy.clickIfExist(`[data-cy="parteB/contas-controle"]`);
    cy.clickIfExist(`[data-cy="4171402380-novo"]`);
    cy.fillInput(`[data-cy="4106578909-input-codigo"] textarea`, `SDD`);
    cy.fillInput(`[data-cy="4106578909-input-descricao"] textarea`, `PCI`);
    cy.fillInputPowerSelect(`[data-cy="4106578909-powerselect-codOficial"] input`);
    cy.fillInput(`[data-cy="4106578909-input-monetary-lalurSaldoInicial"] textarea`, `4`);
    cy.fillInputPowerSelect(`[data-cy="4106578909-powerselect-indDcLalurSaldoInicial"] input`);
    cy.fillInput(`[data-cy="4106578909-input-monetary-lacsSaldoInicial"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="4106578909-powerselect-indDcLacsSaldoInicial"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-novo->246763134-salvar`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-novo`, `246763134-salvar`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-novo"]`);
    cy.clickIfExist(`[data-cy="246763134-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-novo->246763134-voltar`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-novo`, `246763134-voltar`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-novo"]`);
    cy.clickIfExist(`[data-cy="246763134-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-novo->246763134-powerselect-periodo-246763134-powerselect-indAvalEstoq and submit`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-novo`, `246763134-powerselect-periodo-246763134-powerselect-indAvalEstoq`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="246763134-powerselect-periodo"] input`);
    cy.fillInputPowerSelect(`[data-cy="246763134-powerselect-indAvalEstoq"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-visualizar/editar->4062929467-remover item`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-visualizar/editar`, `4062929467-remover item`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="4062929467-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-visualizar/editar->4062929467-salvar`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-visualizar/editar`, `4062929467-salvar`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="4062929467-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-visualizar/editar->4062929467-voltar`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-visualizar/editar`, `4062929467-voltar`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="4062929467-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-l->bloco-l/metodo-aval-estoque-final-l200->4264831049-visualizar/editar->4062929467-powerselect-periodo-4062929467-powerselect-indAvalEstoq and submit`, () => {
    const actualId = [`root`, `bloco-l`, `bloco-l/metodo-aval-estoque-final-l200`, `4264831049-visualizar/editar`, `4062929467-powerselect-periodo-4062929467-powerselect-indAvalEstoq`];
    cy.clickIfExist(`[data-cy="bloco-l"]`);
    cy.clickIfExist(`[data-cy="bloco-l/metodo-aval-estoque-final-l200"]`);
    cy.clickIfExist(`[data-cy="4264831049-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="4062929467-powerselect-periodo"] input`);
    cy.fillInputPowerSelect(`[data-cy="4062929467-powerselect-indAvalEstoq"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.clickIfExist(`[data-cy="2120985688-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.clickIfExist(`[data-cy="2120985688-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-novo`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-novo`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-power-search-button`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-excluir`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-excluir`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-carregar mais`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-power-search-button`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-power-search-button`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-visualizar/editar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-visualizar/editar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-visualizar/editar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-excluir`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-excluir`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-excluir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-carregar mais`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-carregar mais`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-carregar mais"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-visualizar/editar->428604447-remover item`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-visualizar/editar`, `428604447-remover item`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="428604447-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-visualizar/editar->428604447-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-visualizar/editar`, `428604447-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="428604447-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-visualizar/editar->428604447-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-visualizar/editar`, `428604447-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="428604447-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-visualizar/editar->428604447-powerselect-pfjInstituicao-428604447-powerselect-idPais-428604447-powerselect-idTipoMoeda and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-visualizar/editar`, `428604447-powerselect-pfjInstituicao-428604447-powerselect-idPais-428604447-powerselect-idTipoMoeda`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="428604447-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="428604447-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="428604447-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-novo->1604691892-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-novo`, `1604691892-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-novo"]`);
    cy.clickIfExist(`[data-cy="1604691892-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-novo->1604691892-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-novo`, `1604691892-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-novo"]`);
    cy.clickIfExist(`[data-cy="1604691892-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-novo->1604691892-input-nomeMultinacional-1604691892-powerselect-indControladora-1604691892-input-nomeControladora-1604691892-powerselect-idJurisdControladora-1604691892-input-tinControladora-1604691892-powerselect-indEntrega-1604691892-powerselect-indModalidade and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-novo`, `1604691892-input-nomeMultinacional-1604691892-powerselect-indControladora-1604691892-input-nomeControladora-1604691892-powerselect-idJurisdControladora-1604691892-input-tinControladora-1604691892-powerselect-indEntrega-1604691892-powerselect-indModalidade`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-novo"]`);
    cy.fillInput(`[data-cy="1604691892-input-nomeMultinacional"] textarea`, `deposit`);
    cy.fillInputPowerSelect(`[data-cy="1604691892-powerselect-indControladora"] input`);
    cy.fillInput(`[data-cy="1604691892-input-nomeControladora"] textarea`, `visualize`);
    cy.fillInputPowerSelect(`[data-cy="1604691892-powerselect-idJurisdControladora"] input`);
    cy.fillInput(`[data-cy="1604691892-input-tinControladora"] textarea`, `compress`);
    cy.fillInputPowerSelect(`[data-cy="1604691892-powerselect-indEntrega"] input`);
    cy.fillInputPowerSelect(`[data-cy="1604691892-powerselect-indModalidade"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-visualizar/editar->718996091-remover item`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-visualizar/editar`, `718996091-remover item`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="718996091-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-visualizar/editar->718996091-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-visualizar/editar`, `718996091-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="718996091-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-visualizar/editar->718996091-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-visualizar/editar`, `718996091-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="718996091-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/grupo-multinacional-w100-view->734266965-visualizar/editar->718996091-input-nomeMultinacional-718996091-powerselect-indControladora-718996091-input-nomeControladora-718996091-powerselect-idJurisdControladora-718996091-input-tinControladora-718996091-powerselect-indEntrega and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/grupo-multinacional-w100-view`, `734266965-visualizar/editar`, `718996091-input-nomeMultinacional-718996091-powerselect-indControladora-718996091-input-nomeControladora-718996091-powerselect-idJurisdControladora-718996091-input-tinControladora-718996091-powerselect-indEntrega`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/grupo-multinacional-w100-view"]`);
    cy.clickIfExist(`[data-cy="734266965-visualizar/editar"]`);
    cy.fillInput(`[data-cy="718996091-input-nomeMultinacional"] textarea`, `Travessa`);
    cy.fillInputPowerSelect(`[data-cy="718996091-powerselect-indControladora"] input`);
    cy.fillInput(`[data-cy="718996091-input-nomeControladora"] textarea`, `Codes specifically reserved for testing purposes`);
    cy.fillInputPowerSelect(`[data-cy="718996091-powerselect-idJurisdControladora"] input`);
    cy.fillInput(`[data-cy="718996091-input-tinControladora"] textarea`, `Crossgroup`);
    cy.fillInputPowerSelect(`[data-cy="718996091-powerselect-indEntrega"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-novo->2100922122-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-novo`, `2100922122-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-novo"]`);
    cy.clickIfExist(`[data-cy="2100922122-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-novo->2100922122-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-novo`, `2100922122-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-novo"]`);
    cy.clickIfExist(`[data-cy="2100922122-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/operacao-exterior-x291->1691052351-novo->2100922122-powerselect-idOperacaoExterior-2100922122-input-monetary-vlOperacaoExterior and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-novo`, `2100922122-powerselect-idOperacaoExterior-2100922122-input-monetary-vlOperacaoExterior`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2100922122-powerselect-idOperacaoExterior"] input`);
    cy.fillInput(`[data-cy="2100922122-input-monetary-vlOperacaoExterior"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-visualizar/editar->3777202513-remover item`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-visualizar/editar`, `3777202513-remover item`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3777202513-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-visualizar/editar->3777202513-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-visualizar/editar`, `3777202513-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3777202513-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/operacao-exterior-x291->1691052351-visualizar/editar->3777202513-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-visualizar/editar`, `3777202513-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3777202513-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/operacao-exterior-x291->1691052351-visualizar/editar->3777202513-powerselect-idOperacaoExterior-3777202513-input-monetary-vlOperacaoExterior and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/operacao-exterior-x291`, `1691052351-visualizar/editar`, `3777202513-powerselect-idOperacaoExterior-3777202513-input-monetary-vlOperacaoExterior`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickIfExist(`[data-cy="1691052351-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="3777202513-powerselect-idOperacaoExterior"] input`);
    cy.fillInput(`[data-cy="3777202513-input-monetary-vlOperacaoExterior"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/exportacoes-x300->469132206-novo->155499003-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/exportacoes-x300`, `469132206-novo`, `155499003-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/exportacoes-x300"]`);
    cy.clickIfExist(`[data-cy="469132206-novo"]`);
    cy.clickIfExist(`[data-cy="155499003-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/exportacoes-x300->469132206-novo->155499003-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/exportacoes-x300`, `469132206-novo`, `155499003-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/exportacoes-x300"]`);
    cy.clickIfExist(`[data-cy="469132206-novo"]`);
    cy.clickIfExist(`[data-cy="155499003-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/exportacoes-x300->469132206-novo->155499003-input-number-ordemOperacoes-155499003-powerselect-tipoExportacao-155499003-powerselect-tipoItem and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/exportacoes-x300`, `469132206-novo`, `155499003-input-number-ordemOperacoes-155499003-powerselect-tipoExportacao-155499003-powerselect-tipoItem`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/exportacoes-x300"]`);
    cy.clickIfExist(`[data-cy="469132206-novo"]`);
    cy.fillInput(`[data-cy="155499003-input-number-ordemOperacoes"] textarea`, `1`);
    cy.fillInputPowerSelect(`[data-cy="155499003-powerselect-tipoExportacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="155499003-powerselect-tipoItem"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/importacoes-x320->2176193540-novo->1251200869-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/importacoes-x320`, `2176193540-novo`, `1251200869-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/importacoes-x320"]`);
    cy.clickIfExist(`[data-cy="2176193540-novo"]`);
    cy.clickIfExist(`[data-cy="1251200869-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/importacoes-x320->2176193540-novo->1251200869-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/importacoes-x320`, `2176193540-novo`, `1251200869-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/importacoes-x320"]`);
    cy.clickIfExist(`[data-cy="2176193540-novo"]`);
    cy.clickIfExist(`[data-cy="1251200869-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/importacoes-x320->2176193540-novo->1251200869-input-number-numOrdemOperacao-1251200869-powerselect-tipoImportacao-1251200869-powerselect-indTipoOperacao-1251200869-powerselect-indTipoItem-1251200869-powerselect-indDtCalcPrecoParam-1251200869-textarea-criterioDeterPreco-1251200869-powerselect-idFonteCotacao-1251200869-powerselect-undefined-1251200869-input-monetary-vlAjusteAdc-1251200869-input-monetary-vlMedioDiarioCot-1251200869-input-numDeclaracaoUni and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/importacoes-x320`, `2176193540-novo`, `1251200869-input-number-numOrdemOperacao-1251200869-powerselect-tipoImportacao-1251200869-powerselect-indTipoOperacao-1251200869-powerselect-indTipoItem-1251200869-powerselect-indDtCalcPrecoParam-1251200869-textarea-criterioDeterPreco-1251200869-powerselect-idFonteCotacao-1251200869-powerselect-undefined-1251200869-input-monetary-vlAjusteAdc-1251200869-input-monetary-vlMedioDiarioCot-1251200869-input-numDeclaracaoUni`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/importacoes-x320"]`);
    cy.clickIfExist(`[data-cy="2176193540-novo"]`);
    cy.fillInput(`[data-cy="1251200869-input-number-numOrdemOperacao"] textarea`, `7`);
    cy.fillInputPowerSelect(`[data-cy="1251200869-powerselect-tipoImportacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="1251200869-powerselect-indTipoOperacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="1251200869-powerselect-indTipoItem"] input`);
    cy.fillInputPowerSelect(`[data-cy="1251200869-powerselect-indDtCalcPrecoParam"] input`);
    cy.fillInput(`[data-cy="1251200869-textarea-criterioDeterPreco"] input`, `Generic Concrete Shoes`);
    cy.fillInputPowerSelect(`[data-cy="1251200869-powerselect-idFonteCotacao"] input`);
    cy.fillInputPowerSelect(`[data-cy="1251200869-powerselect-undefined"] input`);
    cy.fillInput(`[data-cy="1251200869-input-monetary-vlAjusteAdc"] textarea`, `6`);
    cy.fillInput(`[data-cy="1251200869-input-monetary-vlMedioDiarioCot"] textarea`, `6`);
    cy.fillInput(`[data-cy="1251200869-input-numDeclaracaoUni"] textarea`, `deliverables`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.clickIfExist(`[data-cy="1212937951-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.clickIfExist(`[data-cy="1212937951-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indConsolid"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-motivoNaoConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-moreoutlined->1192077386-item-`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-moreoutlined`, `1192077386-item-`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-moreoutlined"]`);
    cy.clickIfExist(`[data-cy="1192077386-item-"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-visualizar/editar->1844839782-remover item`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-visualizar/editar`, `1844839782-remover item`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1844839782-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-visualizar/editar->1844839782-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-visualizar/editar`, `1844839782-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1844839782-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-visualizar/editar->1844839782-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-visualizar/editar`, `1844839782-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1844839782-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/participacao-exterior-x340->1192077386-visualizar/editar->1844839782-powerselect-pfjEmpresarial-1844839782-powerselect-paisId-1844839782-powerselect-indControle-1844839782-powerselect-idTipoMoeda-1844839782-radio-indExploPetr-1844839782-radio-indConsolid and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-visualizar/editar`, `1844839782-powerselect-pfjEmpresarial-1844839782-powerselect-paisId-1844839782-powerselect-indControle-1844839782-powerselect-idTipoMoeda-1844839782-radio-indExploPetr-1844839782-radio-indConsolid`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1844839782-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1844839782-radio-indConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-novo->2768753498-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-novo`, `2768753498-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-novo"]`);
    cy.clickIfExist(`[data-cy="2768753498-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-novo->2768753498-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-novo`, `2768753498-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-novo"]`);
    cy.clickIfExist(`[data-cy="2768753498-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-novo->2768753498-powerselect-paisId-2768753498-input-monetary-vlServAssist-2768753498-input-monetary-vlServSemTransf-2768753498-input-monetary-vlServSemTransfExt-2768753498-input-monetary-vlDividendos-2768753498-input-monetary-vlJuros-2768753498-input-monetary-vlDemaisJuros and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-novo`, `2768753498-powerselect-paisId-2768753498-input-monetary-vlServAssist-2768753498-input-monetary-vlServSemTransf-2768753498-input-monetary-vlServSemTransfExt-2768753498-input-monetary-vlDividendos-2768753498-input-monetary-vlJuros-2768753498-input-monetary-vlDemaisJuros`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2768753498-powerselect-paisId"] input`);
    cy.fillInput(`[data-cy="2768753498-input-monetary-vlServAssist"] textarea`, `1`);
    cy.fillInput(`[data-cy="2768753498-input-monetary-vlServSemTransf"] textarea`, `7`);
    cy.fillInput(`[data-cy="2768753498-input-monetary-vlServSemTransfExt"] textarea`, `6`);
    cy.fillInput(`[data-cy="2768753498-input-monetary-vlDividendos"] textarea`, `2`);
    cy.fillInput(`[data-cy="2768753498-input-monetary-vlJuros"] textarea`, `5`);
    cy.fillInput(`[data-cy="2768753498-input-monetary-vlDemaisJuros"] textarea`, `6`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-visualizar/editar->1476090273-remover item`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-visualizar/editar`, `1476090273-remover item`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1476090273-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-visualizar/editar->1476090273-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-visualizar/editar`, `1476090273-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1476090273-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-visualizar/editar->1476090273-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-visualizar/editar`, `1476090273-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1476090273-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/rendimento-brasil-exterior-x430->749906671-visualizar/editar->1476090273-powerselect-paisId-1476090273-input-monetary-vlServAssist-1476090273-input-monetary-vlServSemTransf-1476090273-input-monetary-vlServSemTransfExt-1476090273-input-monetary-vlDividendos-1476090273-input-monetary-vlJuros-1476090273-input-monetary-vlDemaisJuros and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/rendimento-brasil-exterior-x430`, `749906671-visualizar/editar`, `1476090273-powerselect-paisId-1476090273-input-monetary-vlServAssist-1476090273-input-monetary-vlServSemTransf-1476090273-input-monetary-vlServSemTransfExt-1476090273-input-monetary-vlDividendos-1476090273-input-monetary-vlJuros-1476090273-input-monetary-vlDemaisJuros`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/rendimento-brasil-exterior-x430"]`);
    cy.clickIfExist(`[data-cy="749906671-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1476090273-powerselect-paisId"] input`);
    cy.fillInput(`[data-cy="1476090273-input-monetary-vlServAssist"] textarea`, `5`);
    cy.fillInput(`[data-cy="1476090273-input-monetary-vlServSemTransf"] textarea`, `9`);
    cy.fillInput(`[data-cy="1476090273-input-monetary-vlServSemTransfExt"] textarea`, `5`);
    cy.fillInput(`[data-cy="1476090273-input-monetary-vlDividendos"] textarea`, `10`);
    cy.fillInput(`[data-cy="1476090273-input-monetary-vlJuros"] textarea`, `2`);
    cy.fillInput(`[data-cy="1476090273-input-monetary-vlDemaisJuros"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-novo->2526523095-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-novo`, `2526523095-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-novo"]`);
    cy.clickIfExist(`[data-cy="2526523095-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-novo->2526523095-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-novo`, `2526523095-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-novo"]`);
    cy.clickIfExist(`[data-cy="2526523095-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-novo->2526523095-powerselect-paisId-2526523095-input-monetary-vlServAssist-2526523095-input-monetary-vlJurosPf-2526523095-input-monetary-vlServSemTransf-2526523095-input-monetary-vlDividendosPf-2526523095-input-monetary-vlJurosPj-2526523095-input-monetary-vlServSemTransfExt-2526523095-input-monetary-vlDividendosPj-2526523095-input-monetary-vlDemaisJuros and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-novo`, `2526523095-powerselect-paisId-2526523095-input-monetary-vlServAssist-2526523095-input-monetary-vlJurosPf-2526523095-input-monetary-vlServSemTransf-2526523095-input-monetary-vlDividendosPf-2526523095-input-monetary-vlJurosPj-2526523095-input-monetary-vlServSemTransfExt-2526523095-input-monetary-vlDividendosPj-2526523095-input-monetary-vlDemaisJuros`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2526523095-powerselect-paisId"] input`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlServAssist"] textarea`, `6`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlJurosPf"] textarea`, `10`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlServSemTransf"] textarea`, `9`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlDividendosPf"] textarea`, `7`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlJurosPj"] textarea`, `1`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlServSemTransfExt"] textarea`, `10`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlDividendosPj"] textarea`, `8`);
    cy.fillInput(`[data-cy="2526523095-input-monetary-vlDemaisJuros"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-visualizar/editar->1801839454-remover item`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-visualizar/editar`, `1801839454-remover item`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1801839454-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-visualizar/editar->1801839454-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-visualizar/editar`, `1801839454-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1801839454-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-visualizar/editar->1801839454-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-visualizar/editar`, `1801839454-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="1801839454-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/pagamento-brasil-exterior-x450->970773842-visualizar/editar->1801839454-powerselect-paisId-1801839454-input-monetary-vlServAssist-1801839454-input-monetary-vlJurosPf-1801839454-input-monetary-vlServSemTransf-1801839454-input-monetary-vlDividendosPf-1801839454-input-monetary-vlJurosPj-1801839454-input-monetary-vlServSemTransfExt-1801839454-input-monetary-vlDividendosPj-1801839454-input-monetary-vlDemaisJuros and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/pagamento-brasil-exterior-x450`, `970773842-visualizar/editar`, `1801839454-powerselect-paisId-1801839454-input-monetary-vlServAssist-1801839454-input-monetary-vlJurosPf-1801839454-input-monetary-vlServSemTransf-1801839454-input-monetary-vlDividendosPf-1801839454-input-monetary-vlJurosPj-1801839454-input-monetary-vlServSemTransfExt-1801839454-input-monetary-vlDividendosPj-1801839454-input-monetary-vlDemaisJuros`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/pagamento-brasil-exterior-x450"]`);
    cy.clickIfExist(`[data-cy="970773842-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="1801839454-powerselect-paisId"] input`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlServAssist"] textarea`, `5`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlJurosPf"] textarea`, `2`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlServSemTransf"] textarea`, `6`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlDividendosPf"] textarea`, `2`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlJurosPj"] textarea`, `6`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlServSemTransfExt"] textarea`, `5`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlDividendosPj"] textarea`, `6`);
    cy.fillInput(`[data-cy="1801839454-input-monetary-vlDemaisJuros"] textarea`, `3`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/pagam-receb-ext-Y520->1335457661-novo->1892724620-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/pagam-receb-ext-Y520`, `1335457661-novo`, `1892724620-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/pagam-receb-ext-Y520"]`);
    cy.clickIfExist(`[data-cy="1335457661-novo"]`);
    cy.clickIfExist(`[data-cy="1892724620-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/pagam-receb-ext-Y520->1335457661-novo->1892724620-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/pagam-receb-ext-Y520`, `1335457661-novo`, `1892724620-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/pagam-receb-ext-Y520"]`);
    cy.clickIfExist(`[data-cy="1335457661-novo"]`);
    cy.clickIfExist(`[data-cy="1892724620-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/pagam-receb-ext-Y520->1335457661-novo->1892724620-powerselect-paisId-1892724620-powerselect-tipoExterior-1892724620-powerselect-naturezaOperacao-1892724620-input-monetary-vlPeriodo and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/pagam-receb-ext-Y520`, `1335457661-novo`, `1892724620-powerselect-paisId-1892724620-powerselect-tipoExterior-1892724620-powerselect-naturezaOperacao-1892724620-input-monetary-vlPeriodo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/pagam-receb-ext-Y520"]`);
    cy.clickIfExist(`[data-cy="1335457661-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1892724620-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1892724620-powerselect-tipoExterior"] input`);
    cy.fillInputPowerSelect(`[data-cy="1892724620-powerselect-naturezaOperacao"] input`);
    cy.fillInput(`[data-cy="1892724620-input-monetary-vlPeriodo"] textarea`, `3`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/retidos-na-fonte->355170916-novo->1247765765-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/retidos-na-fonte`, `355170916-novo`, `1247765765-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/retidos-na-fonte"]`);
    cy.clickIfExist(`[data-cy="355170916-novo"]`);
    cy.clickIfExist(`[data-cy="1247765765-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/retidos-na-fonte->355170916-novo->1247765765-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/retidos-na-fonte`, `355170916-novo`, `1247765765-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/retidos-na-fonte"]`);
    cy.clickIfExist(`[data-cy="355170916-novo"]`);
    cy.clickIfExist(`[data-cy="1247765765-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/retidos-na-fonte->355170916-novo->1247765765-radio-indOrgPublico-1247765765-powerselect-idCodReceita-1247765765-powerselect-pfjFontePagadora-1247765765-input-monetary-vlRendBruto-1247765765-input-monetary-vlIrRetidoFonte-1247765765-input-monetary-vlCsRetidaFonte and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/retidos-na-fonte`, `355170916-novo`, `1247765765-radio-indOrgPublico-1247765765-powerselect-idCodReceita-1247765765-powerselect-pfjFontePagadora-1247765765-input-monetary-vlRendBruto-1247765765-input-monetary-vlIrRetidoFonte-1247765765-input-monetary-vlCsRetidaFonte`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/retidos-na-fonte"]`);
    cy.clickIfExist(`[data-cy="355170916-novo"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1247765765-radio-indOrgPublico"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247765765-powerselect-idCodReceita"] input`);
    cy.fillInputPowerSelect(`[data-cy="1247765765-powerselect-pfjFontePagadora"] input`);
    cy.fillInput(`[data-cy="1247765765-input-monetary-vlRendBruto"] textarea`, `6`);
    cy.fillInput(`[data-cy="1247765765-input-monetary-vlIrRetidoFonte"] textarea`, `3`);
    cy.fillInput(`[data-cy="1247765765-input-monetary-vlCsRetidaFonte"] textarea`, `7`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-novo->229484644-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-novo`, `229484644-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-novo"]`);
    cy.clickIfExist(`[data-cy="229484644-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-novo->229484644-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-novo`, `229484644-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-novo"]`);
    cy.clickIfExist(`[data-cy="229484644-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/A1s-exterior-y-590->2324322213-novo->229484644-powerselect-idTipoAtivo-229484644-powerselect-idPais-229484644-textarea-discriminacao-229484644-input-monetary-vlAnterior-229484644-input-monetary-vlAtual and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-novo`, `229484644-powerselect-idTipoAtivo-229484644-powerselect-idPais-229484644-textarea-discriminacao-229484644-input-monetary-vlAnterior-229484644-input-monetary-vlAtual`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="229484644-powerselect-idTipoAtivo"] input`);
    cy.fillInputPowerSelect(`[data-cy="229484644-powerselect-idPais"] input`);
    cy.fillInput(`[data-cy="229484644-textarea-discriminacao"] input`, `access`);
    cy.fillInput(`[data-cy="229484644-input-monetary-vlAnterior"] textarea`, `9`);
    cy.fillInput(`[data-cy="229484644-input-monetary-vlAtual"] textarea`, `1`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-visualizar/editar->2435563307-remover item`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-visualizar/editar`, `2435563307-remover item`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2435563307-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-visualizar/editar->2435563307-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-visualizar/editar`, `2435563307-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2435563307-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/A1s-exterior-y-590->2324322213-visualizar/editar->2435563307-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-visualizar/editar`, `2435563307-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2435563307-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/A1s-exterior-y-590->2324322213-visualizar/editar->2435563307-powerselect-idTipoAtivo-2435563307-powerselect-idPais-2435563307-textarea-discriminacao-2435563307-input-monetary-vlAnterior-2435563307-input-monetary-vlAtual and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/A1s-exterior-y-590`, `2324322213-visualizar/editar`, `2435563307-powerselect-idTipoAtivo-2435563307-powerselect-idPais-2435563307-textarea-discriminacao-2435563307-input-monetary-vlAnterior-2435563307-input-monetary-vlAtual`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/A1s-exterior-y-590"]`);
    cy.clickIfExist(`[data-cy="2324322213-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="2435563307-powerselect-idTipoAtivo"] input`);
    cy.fillInputPowerSelect(`[data-cy="2435563307-powerselect-idPais"] input`);
    cy.fillInput(`[data-cy="2435563307-textarea-discriminacao"] input`, `TCP`);
    cy.fillInput(`[data-cy="2435563307-input-monetary-vlAnterior"] textarea`, `7`);
    cy.fillInput(`[data-cy="2435563307-input-monetary-vlAtual"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/rendimento-socio-titular->1231882239-novo->982327370-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/rendimento-socio-titular`, `1231882239-novo`, `982327370-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/rendimento-socio-titular"]`);
    cy.clickIfExist(`[data-cy="1231882239-novo"]`);
    cy.clickIfExist(`[data-cy="982327370-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/rendimento-socio-titular->1231882239-novo->982327370-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/rendimento-socio-titular`, `1231882239-novo`, `982327370-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/rendimento-socio-titular"]`);
    cy.clickIfExist(`[data-cy="1231882239-novo"]`);
    cy.clickIfExist(`[data-cy="982327370-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/rendimento-socio-titular->1231882239-novo->982327370-powerselect-nomeEmpresarial-982327370-powerselect-indQualificacaoSocTitu-982327370-powerselect-qualificacaoSocTitu-982327370-powerselect-pfjCodigoRepresentanteLegal-982327370-powerselect-qualificacaoRepLegal-982327370-powerselect-paisCodigo-982327370-powerselect-pfjSocioTitular-982327370-powerselect-pessoaFisicaJuridica-982327370-input-monetary-vlRemuneracaoTrabalho-982327370-input-monetary-vlLucroDividendos-982327370-input-monetary-vlJurosCapitalProprio-982327370-input-monetary-vlDemaisRendimentos-982327370-input-monetary-vlIrRetido-982327370-input-monetary-porcentagemCapitalTotal-982327370-input-monetary-porcentagemCapitalVotante and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/rendimento-socio-titular`, `1231882239-novo`, `982327370-powerselect-nomeEmpresarial-982327370-powerselect-indQualificacaoSocTitu-982327370-powerselect-qualificacaoSocTitu-982327370-powerselect-pfjCodigoRepresentanteLegal-982327370-powerselect-qualificacaoRepLegal-982327370-powerselect-paisCodigo-982327370-powerselect-pfjSocioTitular-982327370-powerselect-pessoaFisicaJuridica-982327370-input-monetary-vlRemuneracaoTrabalho-982327370-input-monetary-vlLucroDividendos-982327370-input-monetary-vlJurosCapitalProprio-982327370-input-monetary-vlDemaisRendimentos-982327370-input-monetary-vlIrRetido-982327370-input-monetary-porcentagemCapitalTotal-982327370-input-monetary-porcentagemCapitalVotante`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/rendimento-socio-titular"]`);
    cy.clickIfExist(`[data-cy="1231882239-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-nomeEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-indQualificacaoSocTitu"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-qualificacaoSocTitu"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-pfjCodigoRepresentanteLegal"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-qualificacaoRepLegal"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-paisCodigo"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-pfjSocioTitular"] input`);
    cy.fillInputPowerSelect(`[data-cy="982327370-powerselect-pessoaFisicaJuridica"] input`);
    cy.fillInput(`[data-cy="982327370-input-monetary-vlRemuneracaoTrabalho"] textarea`, `6`);
    cy.fillInput(`[data-cy="982327370-input-monetary-vlLucroDividendos"] textarea`, `10`);
    cy.fillInput(`[data-cy="982327370-input-monetary-vlJurosCapitalProprio"] textarea`, `2`);
    cy.fillInput(`[data-cy="982327370-input-monetary-vlDemaisRendimentos"] textarea`, `10`);
    cy.fillInput(`[data-cy="982327370-input-monetary-vlIrRetido"] textarea`, `1`);
    cy.fillInput(`[data-cy="982327370-input-monetary-porcentagemCapitalTotal"] textarea`, `4`);
    cy.fillInput(`[data-cy="982327370-input-monetary-porcentagemCapitalVotante"] textarea`, `3`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/equivalencia-patrimonial-y620->425823894-novo->3995607059-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/equivalencia-patrimonial-y620`, `425823894-novo`, `3995607059-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/equivalencia-patrimonial-y620"]`);
    cy.clickIfExist(`[data-cy="425823894-novo"]`);
    cy.clickIfExist(`[data-cy="3995607059-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/equivalencia-patrimonial-y620->425823894-novo->3995607059-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/equivalencia-patrimonial-y620`, `425823894-novo`, `3995607059-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/equivalencia-patrimonial-y620"]`);
    cy.clickIfExist(`[data-cy="425823894-novo"]`);
    cy.clickIfExist(`[data-cy="3995607059-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/equivalencia-patrimonial-y620->425823894-novo->3995607059-powerselect-paisId-3995607059-powerselect-indTpRelacionamento-3995607059-powerselect-pfjEmpresaColiCtrl-3995607059-input-monetary-vlTotalParticipacaoReais-3995607059-input-monetary-vlTotalPartMoedaOrigPais-3995607059-input-monetary-vlResultEquivPatrimonial-3995607059-input-monetary-percentualPartCapitalTotal-3995607059-input-monetary-percentualPartCapitalVotante-3995607059-radio-indSumarioRegistradoCartorio-3995607059-input-nomeCartorio-3995607059-radio-indProcessoRfb and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/equivalencia-patrimonial-y620`, `425823894-novo`, `3995607059-powerselect-paisId-3995607059-powerselect-indTpRelacionamento-3995607059-powerselect-pfjEmpresaColiCtrl-3995607059-input-monetary-vlTotalParticipacaoReais-3995607059-input-monetary-vlTotalPartMoedaOrigPais-3995607059-input-monetary-vlResultEquivPatrimonial-3995607059-input-monetary-percentualPartCapitalTotal-3995607059-input-monetary-percentualPartCapitalVotante-3995607059-radio-indSumarioRegistradoCartorio-3995607059-input-nomeCartorio-3995607059-radio-indProcessoRfb`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/equivalencia-patrimonial-y620"]`);
    cy.clickIfExist(`[data-cy="425823894-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="3995607059-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="3995607059-powerselect-indTpRelacionamento"] input`);
    cy.fillInputPowerSelect(`[data-cy="3995607059-powerselect-pfjEmpresaColiCtrl"] input`);
    cy.fillInput(`[data-cy="3995607059-input-monetary-vlTotalParticipacaoReais"] textarea`, `8`);
    cy.fillInput(`[data-cy="3995607059-input-monetary-vlTotalPartMoedaOrigPais"] textarea`, `8`);
    cy.fillInput(`[data-cy="3995607059-input-monetary-vlResultEquivPatrimonial"] textarea`, `6`);
    cy.fillInput(`[data-cy="3995607059-input-monetary-percentualPartCapitalTotal"] textarea`, `2`);
    cy.fillInput(`[data-cy="3995607059-input-monetary-percentualPartCapitalVotante"] textarea`, `2`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3995607059-radio-indSumarioRegistradoCartorio"] input`);
    cy.fillInput(`[data-cy="3995607059-input-nomeCartorio"] textarea`, `Checking Account`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3995607059-radio-indProcessoRfb"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/fundos-clubes-investimento-y630->1064065977-novo->193925328-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/fundos-clubes-investimento-y630`, `1064065977-novo`, `193925328-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/fundos-clubes-investimento-y630"]`);
    cy.clickIfExist(`[data-cy="1064065977-novo"]`);
    cy.clickIfExist(`[data-cy="193925328-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/fundos-clubes-investimento-y630->1064065977-novo->193925328-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/fundos-clubes-investimento-y630`, `1064065977-novo`, `193925328-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/fundos-clubes-investimento-y630"]`);
    cy.clickIfExist(`[data-cy="1064065977-novo"]`);
    cy.clickIfExist(`[data-cy="193925328-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/fundos-clubes-investimento-y630->1064065977-novo->193925328-powerselect-pfjFundoClbInvest-193925328-input-monetary-vlPatrimonioFinalPeriodo-193925328-input-number-qtdQuotistasFinalPeriodo-193925328-input-number-qtdQuotasFinalPeriodo and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/fundos-clubes-investimento-y630`, `1064065977-novo`, `193925328-powerselect-pfjFundoClbInvest-193925328-input-monetary-vlPatrimonioFinalPeriodo-193925328-input-number-qtdQuotistasFinalPeriodo-193925328-input-number-qtdQuotasFinalPeriodo`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/fundos-clubes-investimento-y630"]`);
    cy.clickIfExist(`[data-cy="1064065977-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="193925328-powerselect-pfjFundoClbInvest"] input`);
    cy.fillInput(`[data-cy="193925328-input-monetary-vlPatrimonioFinalPeriodo"] textarea`, `9`);
    cy.fillInput(`[data-cy="193925328-input-number-qtdQuotistasFinalPeriodo"] textarea`, `8`);
    cy.fillInput(`[data-cy="193925328-input-number-qtdQuotasFinalPeriodo"] textarea`, `6`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participacoes-consorcios-empresas-Y640->3014860639-novo->793683178-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participacoes-consorcios-empresas-Y640`, `3014860639-novo`, `793683178-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participacoes-consorcios-empresas-Y640"]`);
    cy.clickIfExist(`[data-cy="3014860639-novo"]`);
    cy.clickIfExist(`[data-cy="793683178-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/participacoes-consorcios-empresas-Y640->3014860639-novo->793683178-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participacoes-consorcios-empresas-Y640`, `3014860639-novo`, `793683178-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participacoes-consorcios-empresas-Y640"]`);
    cy.clickIfExist(`[data-cy="3014860639-novo"]`);
    cy.clickIfExist(`[data-cy="793683178-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/participacoes-consorcios-empresas-Y640->3014860639-novo->793683178-powerselect-pfjConsorcio-793683178-powerselect-condicaoDeclarante-793683178-powerselect-pfjEmpresaLider-793683178-input-monetary-vlReceitaDeclarante and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/participacoes-consorcios-empresas-Y640`, `3014860639-novo`, `793683178-powerselect-pfjConsorcio-793683178-powerselect-condicaoDeclarante-793683178-powerselect-pfjEmpresaLider-793683178-input-monetary-vlReceitaDeclarante`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/participacoes-consorcios-empresas-Y640"]`);
    cy.clickIfExist(`[data-cy="3014860639-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="793683178-powerselect-pfjConsorcio"] input`);
    cy.fillInputPowerSelect(`[data-cy="793683178-powerselect-condicaoDeclarante"] input`);
    cy.fillInputPowerSelect(`[data-cy="793683178-powerselect-pfjEmpresaLider"] input`);
    cy.fillInput(`[data-cy="793683178-input-monetary-vlReceitaDeclarante"] textarea`, `6`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/periodos-anteriores->4126259043-novo->188903270-salvar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/periodos-anteriores`, `4126259043-novo`, `188903270-salvar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/periodos-anteriores"]`);
    cy.clickIfExist(`[data-cy="4126259043-novo"]`);
    cy.clickIfExist(`[data-cy="188903270-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-y->bloco-y/periodos-anteriores->4126259043-novo->188903270-voltar`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/periodos-anteriores`, `4126259043-novo`, `188903270-voltar`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/periodos-anteriores"]`);
    cy.clickIfExist(`[data-cy="4126259043-novo"]`);
    cy.clickIfExist(`[data-cy="188903270-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-y->bloco-y/periodos-anteriores->4126259043-novo->188903270-input-monetary-vlLucroLiquido-188903270-input-monetary-vlReceitaBruta-188903270-powerselect-indTermoIntimacaoRfb and submit`, () => {
    const actualId = [`root`, `bloco-y`, `bloco-y/periodos-anteriores`, `4126259043-novo`, `188903270-input-monetary-vlLucroLiquido-188903270-input-monetary-vlReceitaBruta-188903270-powerselect-indTermoIntimacaoRfb`];
    cy.clickIfExist(`[data-cy="bloco-y"]`);
    cy.clickIfExist(`[data-cy="bloco-y/periodos-anteriores"]`);
    cy.clickIfExist(`[data-cy="4126259043-novo"]`);
    cy.fillInput(`[data-cy="188903270-input-monetary-vlLucroLiquido"] textarea`, `4`);
    cy.fillInput(`[data-cy="188903270-input-monetary-vlReceitaBruta"] textarea`, `5`);
    cy.fillInputPowerSelect(`[data-cy="188903270-powerselect-indTermoIntimacaoRfb"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-gerenciar labels->1210238629-fechar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-gerenciar labels`, `1210238629-fechar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-gerenciar labels"]`);
    cy.clickIfExist(`[data-cy="1210238629-fechar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-visualizar/editar->2212087746-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-visualizar/editar`, `2212087746-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2212087746-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-obrigacao-fiscal->1210238629-visualizar/editar->2212087746-voltar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-obrigacao-fiscal`, `1210238629-visualizar/editar`, `2212087746-voltar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-obrigacao-fiscal"]`);
    cy.clickIfExist(`[data-cy="1210238629-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="2212087746-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-voltar às obrigações do módulo`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-voltar às obrigações do módulo`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-voltar às obrigações do módulo"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-nova solicitação`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-nova solicitação`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-nova solicitação"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-agendamentos`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-agendamentos`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-agendamentos"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-atualizar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-atualizar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-atualizar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-visualização->1306894685-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-visualização`, `1306894685-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1306894685-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-abrir visualização->1306894685-aumentar o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-abrir visualização`, `1306894685-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1306894685-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-abrir visualização->1306894685-diminuir o zoom`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-abrir visualização`, `1306894685-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1306894685-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-abrir visualização->1306894685-expandir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-abrir visualização`, `1306894685-expandir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1306894685-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-abrir visualização->1306894685-download`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-abrir visualização`, `1306894685-download`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1306894685-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-visualizar->1306894685-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-visualizar`, `1306894685-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-visualizar"]`);
    cy.clickIfExist(`[data-cy="1306894685-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-novo->84255445-criar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-novo`, `84255445-criar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-novo"]`);
    cy.clickIfExist(`[data-cy="84255445-criar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-novo->84255445-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-novo`, `84255445-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-novo"]`);
    cy.clickIfExist(`[data-cy="84255445-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values obrigacoes->obrigacoes/periodicidade->84255445-novo->84255445-input-number-ano and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-novo`, `84255445-input-number-ano`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-novo"]`);
    cy.fillInput(`[data-cy="84255445-input-number-ano"] textarea`, `4`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-editar->84255445-remover item`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-editar`, `84255445-remover item`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-editar"]`);
    cy.clickIfExist(`[data-cy="84255445-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/periodicidade->84255445-editar->84255445-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/periodicidade`, `84255445-editar`, `84255445-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/periodicidade"]`);
    cy.clickIfExist(`[data-cy="84255445-editar"]`);
    cy.clickIfExist(`[data-cy="84255445-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/configuracao-estabelecimento->2480925693-novo->2480925693-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/configuracao-estabelecimento`, `2480925693-novo`, `2480925693-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/configuracao-estabelecimento"]`);
    cy.clickIfExist(`[data-cy="2480925693-novo"]`);
    cy.clickIfExist(`[data-cy="2480925693-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-executar->3414944032-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-executar`, `3414944032-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-executar"]`);
    cy.clickIfExist(`[data-cy="3414944032-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-executar->3414944032-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-executar`, `3414944032-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-executar"]`);
    cy.clickIfExist(`[data-cy="3414944032-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-agendamentos->3414944032-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-agendamentos`, `3414944032-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-agendamentos"]`);
    cy.clickIfExist(`[data-cy="3414944032-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/saldo-referencial->3414944032-visualização->3414944032-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-visualização`, `3414944032-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3414944032-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-detalhes->3414944032-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-detalhes`, `3414944032-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-detalhes"]`);
    cy.clickIfExist(`[data-cy="3414944032-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-abrir visualização->3414944032-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-abrir visualização`, `3414944032-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3414944032-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-abrir visualização->3414944032-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-abrir visualização`, `3414944032-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3414944032-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-abrir visualização->3414944032-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-abrir visualização`, `3414944032-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3414944032-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-abrir visualização->3414944032-download`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-abrir visualização`, `3414944032-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3414944032-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-executar->3193724833-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-executar`, `3193724833-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-executar"]`);
    cy.clickIfExist(`[data-cy="3193724833-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-executar->3193724833-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-executar`, `3193724833-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-executar"]`);
    cy.clickIfExist(`[data-cy="3193724833-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-agendamentos->3193724833-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-agendamentos`, `3193724833-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-agendamentos"]`);
    cy.clickIfExist(`[data-cy="3193724833-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/saldo-acumulado->3193724833-visualização->3193724833-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-visualização`, `3193724833-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3193724833-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-executar->1536113708-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-executar`, `1536113708-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-executar"]`);
    cy.clickIfExist(`[data-cy="1536113708-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-executar->1536113708-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-executar`, `1536113708-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-executar"]`);
    cy.clickIfExist(`[data-cy="1536113708-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-agendamentos->1536113708-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-agendamentos`, `1536113708-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-agendamentos"]`);
    cy.clickIfExist(`[data-cy="1536113708-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/limpa-tabelas->1536113708-visualização->1536113708-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-visualização`, `1536113708-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1536113708-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-detalhes->1536113708-dados disponíveis para impressão`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-detalhes`, `1536113708-dados disponíveis para impressão`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-detalhes"]`);
    cy.clickIfExist(`[data-cy="1536113708-dados disponíveis para impressão"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-abrir visualização->1536113708-aumentar o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-abrir visualização`, `1536113708-aumentar o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1536113708-aumentar o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-abrir visualização->1536113708-diminuir o zoom`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-abrir visualização`, `1536113708-diminuir o zoom`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1536113708-diminuir o zoom"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-abrir visualização->1536113708-expandir`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-abrir visualização`, `1536113708-expandir`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1536113708-expandir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-abrir visualização->1536113708-download`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-abrir visualização`, `1536113708-download`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1536113708-download"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-executar->409460278-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-executar`, `409460278-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-executar"]`);
    cy.clickIfExist(`[data-cy="409460278-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-executar->409460278-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-executar`, `409460278-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-executar"]`);
    cy.clickIfExist(`[data-cy="409460278-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-agendamentos->409460278-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-agendamentos`, `409460278-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-agendamentos"]`);
    cy.clickIfExist(`[data-cy="409460278-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/gera-saldo-mensal->409460278-visualização->409460278-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-visualização`, `409460278-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="409460278-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-executar->3143174073-múltipla seleção`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-executar`, `3143174073-múltipla seleção`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-executar"]`);
    cy.clickIfExist(`[data-cy="3143174073-múltipla seleção"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-executar->3143174073-agendar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-executar`, `3143174073-agendar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-executar"]`);
    cy.clickIfExist(`[data-cy="3143174073-agendar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-agendamentos->3143174073-voltar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-agendamentos`, `3143174073-voltar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-agendamentos"]`);
    cy.clickIfExist(`[data-cy="3143174073-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values processos->processos/gera-lancamentos-encerramento->3143174073-visualização->3143174073-item- and submit`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-visualização`, `3143174073-item-`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="3143174073-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-mais operações->3663702805-item-exportar balancete->3663702805-baixar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-mais operações`, `3663702805-item-exportar balancete`, `3663702805-baixar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-mais operações"]`);
    cy.clickIfExist(`[data-cy="3663702805-item-exportar balancete"]`);
    cy.clickIfExist(`[data-cy="3663702805-baixar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-mensal->3663702805-mais operações->3663702805-item-erros de importação->3663702805-cancelar`, () => {
    const actualId = [`root`, `movimentacao-contabil`, `movimentacao-contabil/saldo-mensal`, `3663702805-mais operações`, `3663702805-item-erros de importação`, `3663702805-cancelar`];
    cy.clickIfExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickIfExist(`[data-cy="movimentacao-contabil/saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="3663702805-mais operações"]`);
    cy.clickIfExist(`[data-cy="3663702805-item-erros de importação"]`);
    cy.clickIfExist(`[data-cy="3663702805-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda->428604447-remover item`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda`, `428604447-remover item`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="428604447-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda->428604447-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda`, `428604447-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="428604447-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda->428604447-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda`, `428604447-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="428604447-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-novo->2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda->428604447-powerselect-pfjInstituicao-428604447-powerselect-idPais-428604447-powerselect-idTipoMoeda and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-novo`, `2120985688-powerselect-pfjInstituicao-2120985688-powerselect-idPais-2120985688-powerselect-idTipoMoeda`, `428604447-powerselect-pfjInstituicao-428604447-powerselect-idPais-428604447-powerselect-idTipoMoeda`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="2120985688-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="428604447-powerselect-pfjInstituicao"] input`);
    cy.fillInputPowerSelect(`[data-cy="428604447-powerselect-idPais"] input`);
    cy.fillInputPowerSelect(`[data-cy="428604447-powerselect-idTipoMoeda"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-novo->2368291781-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-novo`, `2368291781-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-novo"]`);
    cy.clickIfExist(`[data-cy="2368291781-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-novo->2368291781-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-novo`, `2368291781-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-novo"]`);
    cy.clickIfExist(`[data-cy="2368291781-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-novo->2368291781-powerselect-tpDocumentoResponsavel-2368291781-input-number-numeroDocumentoResponsavel-2368291781-powerselect-pfjCodResponsavel-2368291781-input-identificacaoConta and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-novo`, `2368291781-powerselect-tpDocumentoResponsavel-2368291781-input-number-numeroDocumentoResponsavel-2368291781-powerselect-pfjCodResponsavel-2368291781-input-identificacaoConta`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2368291781-powerselect-tpDocumentoResponsavel"] input`);
    cy.fillInput(`[data-cy="2368291781-input-number-numeroDocumentoResponsavel"] textarea`, `6`);
    cy.fillInputPowerSelect(`[data-cy="2368291781-powerselect-pfjCodResponsavel"] input`);
    cy.fillInput(`[data-cy="2368291781-input-identificacaoConta"] textarea`, `Auto Loan Account`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-visualizar/editar->3112406476-remover item`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-visualizar/editar`, `3112406476-remover item`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3112406476-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-visualizar/editar->3112406476-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-visualizar/editar`, `3112406476-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3112406476-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-visualizar/editar->3112406476-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-visualizar/editar`, `3112406476-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3112406476-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-responsável->2179031972-visualizar/editar->3112406476-powerselect-tpDocumentoResponsavel-3112406476-input-number-numeroDocumentoResponsavel-3112406476-powerselect-pfjCodResponsavel-3112406476-input-identificacaoConta and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-responsável`, `2179031972-visualizar/editar`, `3112406476-powerselect-tpDocumentoResponsavel-3112406476-input-number-numeroDocumentoResponsavel-3112406476-powerselect-pfjCodResponsavel-3112406476-input-identificacaoConta`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-responsável"]`);
    cy.clickIfExist(`[data-cy="2179031972-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="3112406476-powerselect-tpDocumentoResponsavel"] input`);
    cy.fillInput(`[data-cy="3112406476-input-number-numeroDocumentoResponsavel"] textarea`, `6`);
    cy.fillInputPowerSelect(`[data-cy="3112406476-powerselect-pfjCodResponsavel"] input`);
    cy.fillInput(`[data-cy="3112406476-input-identificacaoConta"] textarea`, `hacking`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.clickIfExist(`[data-cy="1804448316-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.clickIfExist(`[data-cy="1804448316-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="1804448316-input-monetary-valor"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-visualizar/editar->3244492547-remover item`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-visualizar/editar`, `3244492547-remover item`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3244492547-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-visualizar/editar->3244492547-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-visualizar/editar`, `3244492547-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3244492547-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-visualizar/editar->3244492547-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-visualizar/editar`, `3244492547-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-visualizar/editar"]`);
    cy.clickIfExist(`[data-cy="3244492547-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-visualizar/editar->3244492547-powerselect-mes-3244492547-powerselect-codLinhaRfb-3244492547-input-monetary-valor and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-visualizar/editar`, `3244492547-powerselect-mes-3244492547-powerselect-codLinhaRfb-3244492547-input-monetary-valor`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-visualizar/editar"]`);
    cy.fillInputPowerSelect(`[data-cy="3244492547-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="3244492547-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="3244492547-input-monetary-valor"] textarea`, `9`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid->1844839782-remover item`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid`, `1844839782-remover item`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indConsolid"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-motivoNaoConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="1844839782-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid->1844839782-salvar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid`, `1844839782-salvar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indConsolid"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-motivoNaoConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="1844839782-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid->1844839782-voltar`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid`, `1844839782-voltar`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indConsolid"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-motivoNaoConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="1844839782-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Filling values bloco-x->bloco-x/participacao-exterior-x340->1192077386-novo->1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid->1844839782-powerselect-pfjEmpresarial-1844839782-powerselect-paisId-1844839782-powerselect-indControle-1844839782-powerselect-idTipoMoeda-1844839782-radio-indExploPetr-1844839782-radio-indConsolid and submit`, () => {
    const actualId = [`root`, `bloco-x`, `bloco-x/participacao-exterior-x340`, `1192077386-novo`, `1212937951-powerselect-pfjEmpresarial-1212937951-powerselect-paisId-1212937951-powerselect-indControle-1212937951-powerselect-idTipoMoeda-1212937951-radio-indExploPetr-1212937951-radio-indConsolid-1212937951-powerselect-motivoNaoConsolid`, `1844839782-powerselect-pfjEmpresarial-1844839782-powerselect-paisId-1844839782-powerselect-indControle-1844839782-powerselect-idTipoMoeda-1844839782-radio-indExploPetr-1844839782-radio-indConsolid`];
    cy.clickIfExist(`[data-cy="bloco-x"]`);
    cy.clickIfExist(`[data-cy="bloco-x/participacao-exterior-x340"]`);
    cy.clickIfExist(`[data-cy="1192077386-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1212937951-radio-indConsolid"] input`);
    cy.fillInputPowerSelect(`[data-cy="1212937951-powerselect-motivoNaoConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-pfjEmpresarial"] input`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-paisId"] input`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-indControle"] input`);
    cy.fillInputPowerSelect(`[data-cy="1844839782-powerselect-idTipoMoeda"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1844839782-radio-indExploPetr"] input`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1844839782-radio-indConsolid"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-voltar às obrigações do módulo->3614521972-ir para todas as obrigações`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-voltar às obrigações do módulo`, `3614521972-ir para todas as obrigações`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-voltar às obrigações do módulo"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-nova solicitação->3614521972-salvar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-nova solicitação`, `3614521972-salvar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="3614521972-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-nova solicitação->3614521972-cancelar`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-nova solicitação`, `3614521972-cancelar`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-nova solicitação"]`);
    cy.clickIfExist(`[data-cy="3614521972-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-agendamentos->1306894685-power-search-button`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-agendamentos`, `1306894685-power-search-button`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-agendamentos"]`);
    cy.clickIfExist(`[data-cy="1306894685-power-search-button"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-agendamentos->1306894685-visualização`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-agendamentos`, `1306894685-visualização`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-agendamentos"]`);
    cy.clickIfExist(`[data-cy="1306894685-visualização"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element obrigacoes->obrigacoes/obrigacoes-executadas->1306894685-abrir visualização->1306894685-expandir->1306894685-diminuir`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/obrigacoes-executadas`, `1306894685-abrir visualização`, `1306894685-expandir`, `1306894685-diminuir`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/obrigacoes-executadas"]`);
    cy.clickIfExist(`[data-cy="1306894685-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1306894685-expandir"]`);
    cy.clickIfExist(`[data-cy="1306894685-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-executar->3414944032-múltipla seleção->3414944032-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-executar`, `3414944032-múltipla seleção`, `3414944032-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-executar"]`);
    cy.clickIfExist(`[data-cy="3414944032-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3414944032-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-referencial->3414944032-abrir visualização->3414944032-expandir->3414944032-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-referencial`, `3414944032-abrir visualização`, `3414944032-expandir`, `3414944032-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-referencial"]`);
    cy.clickIfExist(`[data-cy="3414944032-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="3414944032-expandir"]`);
    cy.clickIfExist(`[data-cy="3414944032-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/saldo-acumulado->3193724833-executar->3193724833-múltipla seleção->3193724833-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/saldo-acumulado`, `3193724833-executar`, `3193724833-múltipla seleção`, `3193724833-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/saldo-acumulado"]`);
    cy.clickIfExist(`[data-cy="3193724833-executar"]`);
    cy.clickIfExist(`[data-cy="3193724833-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3193724833-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-executar->1536113708-múltipla seleção->1536113708-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-executar`, `1536113708-múltipla seleção`, `1536113708-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-executar"]`);
    cy.clickIfExist(`[data-cy="1536113708-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="1536113708-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/limpa-tabelas->1536113708-abrir visualização->1536113708-expandir->1536113708-diminuir`, () => {
    const actualId = [`root`, `processos`, `processos/limpa-tabelas`, `1536113708-abrir visualização`, `1536113708-expandir`, `1536113708-diminuir`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/limpa-tabelas"]`);
    cy.clickIfExist(`[data-cy="1536113708-abrir visualização"]`);
    cy.clickIfExist(`[data-cy="1536113708-expandir"]`);
    cy.clickIfExist(`[data-cy="1536113708-diminuir"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-saldo-mensal->409460278-executar->409460278-múltipla seleção->409460278-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-saldo-mensal`, `409460278-executar`, `409460278-múltipla seleção`, `409460278-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-saldo-mensal"]`);
    cy.clickIfExist(`[data-cy="409460278-executar"]`);
    cy.clickIfExist(`[data-cy="409460278-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="409460278-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it.skip(`Click on element processos->processos/gera-lancamentos-encerramento->3143174073-executar->3143174073-múltipla seleção->3143174073-cancelar`, () => {
    const actualId = [`root`, `processos`, `processos/gera-lancamentos-encerramento`, `3143174073-executar`, `3143174073-múltipla seleção`, `3143174073-cancelar`];
    cy.clickIfExist(`[data-cy="processos"]`);
    cy.clickIfExist(`[data-cy="processos/gera-lancamentos-encerramento"]`);
    cy.clickIfExist(`[data-cy="3143174073-executar"]`);
    cy.clickIfExist(`[data-cy="3143174073-múltipla seleção"]`);
    cy.clickIfExist(`[data-cy="3143174073-cancelar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor->3244492547-remover item`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor`, `3244492547-remover item`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="1804448316-input-monetary-valor"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3244492547-remover item"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor->3244492547-salvar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor`, `3244492547-salvar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="1804448316-input-monetary-valor"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3244492547-salvar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Click on element bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor->3244492547-voltar`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor`, `3244492547-voltar`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="1804448316-input-monetary-valor"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.clickIfExist(`[data-cy="3244492547-voltar"]`);
    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values bloco-v-w->bloco-v-w/derex-v010->1500425009-demonstrativo->2702280397-novo->1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor->3244492547-powerselect-mes-3244492547-powerselect-codLinhaRfb-3244492547-input-monetary-valor and submit`, () => {
    const actualId = [`root`, `bloco-v-w`, `bloco-v-w/derex-v010`, `1500425009-demonstrativo`, `2702280397-novo`, `1804448316-powerselect-mes-1804448316-powerselect-codLinhaRfb-1804448316-input-monetary-valor`, `3244492547-powerselect-mes-3244492547-powerselect-codLinhaRfb-3244492547-input-monetary-valor`];
    cy.clickIfExist(`[data-cy="bloco-v-w"]`);
    cy.clickIfExist(`[data-cy="bloco-v-w/derex-v010"]`);
    cy.clickIfExist(`[data-cy="1500425009-demonstrativo"]`);
    cy.clickIfExist(`[data-cy="2702280397-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="1804448316-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="1804448316-input-monetary-valor"] textarea`, `10`);
    cy.submitIfExist(`.ant-form`);

    cy.fillInputPowerSelect(`[data-cy="3244492547-powerselect-mes"] input`);
    cy.fillInputPowerSelect(`[data-cy="3244492547-powerselect-codLinhaRfb"] input`);
    cy.fillInput(`[data-cy="3244492547-input-monetary-valor"] textarea`, `5`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
  it(`Filling values obrigacoes->obrigacoes/solicitacoes-resultados->3614521972-ir para todas as obrigações->3614521972-agendamentos->1306894685-visualização->1306894685-item- and submit`, () => {
    const actualId = [`root`, `obrigacoes`, `obrigacoes/solicitacoes-resultados`, `3614521972-ir para todas as obrigações`, `3614521972-agendamentos`, `1306894685-visualização`, `1306894685-item-`];
    cy.clickIfExist(`[data-cy="obrigacoes"]`);
    cy.clickIfExist(`[data-cy="obrigacoes/solicitacoes-resultados"]`);
    cy.clickIfExist(`[data-cy="3614521972-ir para todas as obrigações"]`);
    cy.clickIfExist(`[data-cy="3614521972-agendamentos"]`);
    cy.clickIfExist(`[data-cy="1306894685-visualização"]`);
    cy.fillInputCheckboxOrRadio(`[data-cy="1306894685-item-"] input`);
    cy.submitIfExist(`.ant-form`);

    cy.checkErrorsWereDetected();
    cy.writeContent(actualId);
  });
  //--CODE--
});
