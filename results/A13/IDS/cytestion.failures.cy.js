describe('Cytestion', () => {
  beforeEach(() => {
    cy.loginKeycloak();
    cy.visit('/');
    cy.skipLoadingPage();
    cy.waitNetworkFinished();
  });
  it(`Click on element 1564411349-verificar dados`, () => {
    cy.clickCauseExist(`[data-cy="1564411349-verificar dados"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element movimentacao-contabil->movimentacao-contabil/conta-contabil-referencial-estab->2872373034-novo`, () => {
    cy.clickCauseExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickCauseExist(`[data-cy="movimentacao-contabil/conta-contabil-referencial-estab"]`);
    cy.clickCauseExist(`[data-cy="2872373034-novo"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Click on element movimentacao-contabil->movimentacao-contabil/saldo-referencial-contabil->247807478-novo`, () => {
    cy.clickCauseExist(`[data-cy="movimentacao-contabil"]`);
    cy.clickCauseExist(`[data-cy="movimentacao-contabil/saldo-referencial-contabil"]`);
    cy.clickCauseExist(`[data-cy="247807478-novo"]`);
    cy.checkErrorsWereDetected();
  });
  it(`Filling values bloco-x->bloco-x/operacao-exterior-x291->1691052351-novo->2100922122-powerselect-idOperacaoExterior-2100922122-input-monetary-vlOperacaoExterior and submit`, () => {
    cy.clickCauseExist(`[data-cy="bloco-x"]`);
    cy.clickCauseExist(`[data-cy="bloco-x/operacao-exterior-x291"]`);
    cy.clickCauseExist(`[data-cy="1691052351-novo"]`);
    cy.fillInputPowerSelect(`[data-cy="2100922122-powerselect-idOperacaoExterior"] input`);
    cy.fillInput(`[data-cy="2100922122-input-monetary-vlOperacaoExterior"] textarea`, `2`);
    cy.submitIfExist(`.ant-form`);
    cy.checkErrorsWereDetected();
  });
});
